class CreateTerritoryCountries < ActiveRecord::Migration
  def change
    create_table :territory_countries do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
