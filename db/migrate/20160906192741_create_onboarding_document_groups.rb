class CreateOnboardingDocumentGroups < ActiveRecord::Migration
  def change
    create_table :onboarding_document_groups do |t|
      t.references :onboarding_stage, index: true
      t.references :document_group, index: true

      t.timestamps null: false
    end

    add_foreign_key :onboarding_document_groups, :stage_bases, column: :onboarding_stage_id
    add_foreign_key :onboarding_document_groups, :document_groups, column: :document_group_id
  end
end
