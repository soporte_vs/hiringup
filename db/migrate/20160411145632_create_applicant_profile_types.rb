class CreateApplicantProfileTypes < ActiveRecord::Migration
  def change
    create_table :applicant_profile_types do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
