class CreateTrabajandoStates < ActiveRecord::Migration
  def change
    create_table :trabajando_states do |t|
      t.string :name
      t.integer :trabajando_id
      t.references :hupe_state, index: true

      t.timestamps null: false
    end
    add_foreign_key :trabajando_states, :territory_states, column: :hupe_state_id

  end

  def down
    Trabajando::State.destroy_all
    drop_table :trabajando_states
  end
end
