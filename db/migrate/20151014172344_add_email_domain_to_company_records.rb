class AddEmailDomainToCompanyRecords < ActiveRecord::Migration
  def change
    add_column :company_records, :email_domain, :string
  end
end
