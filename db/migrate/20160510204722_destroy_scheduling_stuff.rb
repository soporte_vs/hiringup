class DestroySchedulingStuff < ActiveRecord::Migration
  def up
    drop_table :scheduling_appointments
    drop_table :diary_appointments
    drop_table :diary_events
  end

  def down
    fail ActiveRecord::IrreversibleMigration
  end
end
