class CreateVideoInterviewQuestions < ActiveRecord::Migration
  def change
    create_table :video_interview_questions do |t|
      t.string :content

      t.timestamps null: false
    end
  end
end
