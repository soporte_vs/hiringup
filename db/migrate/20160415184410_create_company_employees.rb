class CreateCompanyEmployees < ActiveRecord::Migration
  def change
    create_table :company_employees do |t|
      t.references :user, index: true
      t.timestamps null: false
    end
    add_foreign_key :company_employees, :users
  end
end
