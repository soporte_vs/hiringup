class RenameUserIdToApplicantIdInDocumentRecord < ActiveRecord::Migration
  def change
    rename_column :document_records, :user_id, :applicant_id
  end
end
