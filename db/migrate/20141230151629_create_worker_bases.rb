class CreateWorkerBases < ActiveRecord::Migration
  def change
    create_table :worker_bases do |t|
      t.string :type
      t.references :user, index: true

      t.timestamps
    end
  end
end
