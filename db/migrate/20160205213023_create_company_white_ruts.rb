class CreateCompanyWhiteRuts < ActiveRecord::Migration
  def change
    create_table :company_white_ruts do |t|
      t.string :rut

      t.timestamps null: false
    end
  end
end
