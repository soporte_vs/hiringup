class CreateTrabajandoCountries < ActiveRecord::Migration
  def up
    create_table :trabajando_countries do |t|
      t.string :name
      t.integer :trabajando_id
      t.references :country, index: true

      t.timestamps null: false
    end
    add_foreign_key :trabajando_countries, :territory_countries, column: :country_id

    errors = []
    data = [
      { trabajando_id: 1, name: 'Chile'}
    ].each do |attrs|
      trabajando_country = Trabajando::Country.new(attrs)
      unless trabajando_country.save
        errors << trabajando_country
      end
    end
    puts "Se han cargado #{data.size - errors.size} de #{data.size} paises de trabajando"
  end

  def down
    Trabajando::Country.destroy_all
    drop_table :trabajando_countries
  end
end
