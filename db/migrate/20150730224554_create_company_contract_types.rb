class CreateCompanyContractTypes < ActiveRecord::Migration
  def change
    create_table :company_contract_types do |t|
      t.string :name
      t.text :description

      t.timestamps null: false
    end
  end
end
