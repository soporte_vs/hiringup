class CreateTagBaseTags < ActiveRecord::Migration
  def change
    create_table :tag_base_tags do |t|
      t.string :name
      t.string :type

      t.timestamps null: false
    end
  end
end
