class CreateCompanyMaritalStatuses < ActiveRecord::Migration
  def change
    create_table :company_marital_statuses do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
