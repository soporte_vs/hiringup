class RenameExistingSkillInterviews < ActiveRecord::Migration
  def up
    SkillInterview::Stage.find_each do |stage|
      stage.update_attributes(name: "Entrevista por Competencia y/o Entrevista para Informe Social")
    end
  end

  def down
    SkillInterview::Stage.find_each do |stage|
      stage.update_attributes(name: "Resultados de Entrevistas")
    end
  end
end
