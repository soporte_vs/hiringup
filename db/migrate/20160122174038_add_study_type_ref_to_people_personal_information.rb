class AddStudyTypeRefToPeoplePersonalInformation < ActiveRecord::Migration
  def change
    add_reference :people_personal_informations, :education_study_type, index: true
    add_foreign_key :people_personal_informations, :education_study_types
  end
end
