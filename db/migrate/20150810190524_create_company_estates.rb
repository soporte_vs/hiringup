class CreateCompanyEstates < ActiveRecord::Migration
  def change
    create_table :company_estates do |t|
      t.string :name

      t.timestamps null: false
    end
    add_index :company_estates, :name, unique: true
  end
end
