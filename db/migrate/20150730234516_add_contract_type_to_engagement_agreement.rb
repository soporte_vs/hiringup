class AddContractTypeToEngagementAgreement < ActiveRecord::Migration
  def change
    add_reference :engagement_agreements, :company_contract_type, index: true
    add_foreign_key :engagement_agreements, :company_contract_types
  end
end
