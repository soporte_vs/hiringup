class CreatePeopleLanguangeSkills < ActiveRecord::Migration
  def change
    create_table :people_languange_skills do |t|
      t.string :level
      t.references :education_language, index: true
      t.references :professional_information

      t.timestamps null: false
    end
    add_foreign_key :people_languange_skills, :education_languages
    add_foreign_key :people_languange_skills, :people_professional_informations, column: :professional_information_id
  end
end
