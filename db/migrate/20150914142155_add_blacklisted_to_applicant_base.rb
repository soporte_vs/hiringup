class AddBlacklistedToApplicantBase < ActiveRecord::Migration
  def change
    add_column :applicant_bases, :blacklisted, :boolean
  end
end
