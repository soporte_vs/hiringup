class MigrateDocumentsTechnicalTest < ActiveRecord::Migration

class TechnicalTest::Report < ActiveRecord::Base
  mount_uploader :file, GenericReportStageUploader
  belongs_to :technical_test_stage,
             class_name: TechnicalTest::Stage,
             foreign_key: :technical_test_stage_id

  has_many :documents,
            class_name: Stage::Document,
            as: :resource,
            dependent: :destroy

    validates :observations, presence: true
    validates :notified_to, presence: true, email: true
    validates :technical_test_stage, presence: true
  end

  def up
    TechnicalTest::Report.all.each do |report|
      report.documents.create(document: report.file)
    end
    remove_column :technical_test_reports, :file
  end

  def down
    add_column :technical_test_reports, :file, :string
  end

end
