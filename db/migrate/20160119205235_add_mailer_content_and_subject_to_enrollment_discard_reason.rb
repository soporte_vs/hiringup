class AddMailerContentAndSubjectToEnrollmentDiscardReason < ActiveRecord::Migration
  def change
    add_column :enrollment_discard_reasons, :mailer_content, :text
    add_column :enrollment_discard_reasons, :subject, :text
  end
end
