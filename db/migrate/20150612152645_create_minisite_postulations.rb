class CreateMinisitePostulations < ActiveRecord::Migration
  def change
    create_table :minisite_postulations do |t|
      t.references :minisite_publication, index: true
      t.references :applicant, index: true

      t.timestamps null: false
    end
    add_foreign_key :minisite_postulations, :minisite_publications
    add_foreign_key :minisite_postulations, :applicant_bases, column: :applicant_id
  end
end
