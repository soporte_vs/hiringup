class AddStageReferenceToEnrollmentBase < ActiveRecord::Migration
  def change
    add_reference :enrollment_bases, :stage, index: true
  end
end
