class CreateMinisiteQuestions < ActiveRecord::Migration
  def change
    create_table :minisite_questions do |t|
      t.references :minisite_publication, index: true
      t.text :description

      t.timestamps null: false
    end
    add_foreign_key :minisite_questions, :minisite_publications
  end
end
