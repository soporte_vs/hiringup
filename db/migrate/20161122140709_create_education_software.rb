class CreateEducationSoftware < ActiveRecord::Migration
  def change
    create_table :education_softwares do |t|
      t.string :name
      t.timestamps null: false
    end
  end
end
