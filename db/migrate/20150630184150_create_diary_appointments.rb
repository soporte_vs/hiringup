class CreateDiaryAppointments < ActiveRecord::Migration
  def change
    create_table :diary_appointments do |t|
      t.references :user, index: true
      t.datetime :starts_at
      t.datetime :ends_at
      t.references :resource, polymorphic: true, index: true

      t.timestamps null: false
    end
    add_foreign_key :diary_appointments, :users
  end
end
