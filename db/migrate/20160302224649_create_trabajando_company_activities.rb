class CreateTrabajandoCompanyActivities < ActiveRecord::Migration
  def up
    create_table :trabajando_company_activities do |t|
      t.string :name
      t.integer :trabajando_id

      t.timestamps null: false
    end

    errors = []
    data = [
      { trabajando_id:26, name: 'Administración Pública' },
      { trabajando_id:1, name: 'Aeronaves / Astilleros' },
      { trabajando_id:75, name: 'Afore' },
      { trabajando_id:59, name: 'Agencia de Aduanas' },
      { trabajando_id:2, name: 'Agrícola / Ganadera' },
      { trabajando_id:76, name: 'Agroindustria' },
      { trabajando_id:3, name: 'Agropecuaria ' },
      { trabajando_id:4, name: 'Agua / Obras Sanitarias' },
      { trabajando_id:60, name: 'Alimentos' },
      { trabajando_id:77, name: 'Ambiental' },
      { trabajando_id:5, name: 'Arquitectura / Diseño / Decoración' },
      { trabajando_id:71, name: 'Atelier de diseño' },
      { trabajando_id:6, name: 'Automotriz ' },
      { trabajando_id:7, name: 'Banca / Financiera ' },
      { trabajando_id:78, name: 'Bebidas' },
      { trabajando_id:8, name: 'Biotecnologia' },
      { trabajando_id:72, name: 'Boutique' },
      { trabajando_id:9, name: 'Carpintería / Muebles' },
      { trabajando_id:79, name: 'Cemento y Materiales' },
      { trabajando_id:10, name: 'Científica' },
      { trabajando_id:11, name: 'Combustibles (Gas / Petróleo)' },
      { trabajando_id:65, name: 'Comercial' },
      { trabajando_id:64, name: 'Comercio Electrónico' },
      { trabajando_id:58, name: 'Comercio Exterior' },
      { trabajando_id:12, name: 'Comercio Mayorista' },
      { trabajando_id:13, name: 'Comercio Minorista' },
      { trabajando_id:14, name: 'Confecciones' },
      { trabajando_id:15, name: 'Construcción' },
      { trabajando_id:16, name: 'Consultoria / Asesoría' },
      { trabajando_id:17, name: 'Consumo masivo' },
      { trabajando_id:80, name: 'Cultura' },
      { trabajando_id:18, name: 'Defensa' },
      { trabajando_id:81, name: 'Departamentales' },
      { trabajando_id:82, name: 'Despachos de Abogados' },
      { trabajando_id:57, name: 'Distribuidora' },
      { trabajando_id:84, name: 'Editorial e Imprenta' },
      { trabajando_id:19, name: 'Educación / Capacitación' },
      { trabajando_id:85, name: 'Electrónica de Consumo' },
      { trabajando_id:20, name: 'Energía / Electricidad / Electrónica' },
      { trabajando_id:21, name: 'Entretenimiento' },
      { trabajando_id:22, name: 'Estudios Jurídicos' },
      { trabajando_id:23, name: 'Exportación / Importación' },
      { trabajando_id:24, name: 'Farmacéutica' },
      { trabajando_id:25, name: 'Forestal / Papel / Celulosa' },
      { trabajando_id:73, name: 'Grandes Almacenes' },
      { trabajando_id:63, name: 'Grandes Tiendas' },
      { trabajando_id:97, name: 'Hipermercados' },
      { trabajando_id:27, name: 'Hotelería / Restaurantes' },
      { trabajando_id:28, name: 'Imprenta / Editoriales' },
      { trabajando_id:67, name: 'Industrial' },
      { trabajando_id:29, name: 'Informatica / Tecnologia ' },
      { trabajando_id:30, name: 'Ingeniería' },
      { trabajando_id:31, name: 'Inmobiliaria/Propiedades' },
      { trabajando_id:32, name: 'Internet' },
      { trabajando_id:33, name: 'Inversiones (Soc / Cías / Holding)' },
      { trabajando_id:86, name: 'Investigación' },
      { trabajando_id:34, name: 'Logística / Distribución' },
      { trabajando_id:35, name: 'Manufacturas Varias' },
      { trabajando_id:87, name: 'Maquinaria y Equipo' },
      { trabajando_id:36, name: 'Medicina / Salud' },
      { trabajando_id:37, name: 'Medios de Comunicación' },
      { trabajando_id:38, name: 'Metalmecánica' },
      { trabajando_id:39, name: 'Minería' },
      { trabajando_id:41, name: 'Naviera' },
      { trabajando_id:42, name: 'Organizaciones sin Fines de Lucro' },
      { trabajando_id:55, name: 'Otra Actividad' },
      { trabajando_id:88, name: 'Papel y Cartón' },
      { trabajando_id:69, name: 'Pesquera' },
      { trabajando_id:43, name: 'Pesquera / Cultivos Marinos' },
      { trabajando_id:40, name: 'Petroleo / Gas / Combustibles' },
      { trabajando_id:89, name: 'Poder ejecutivo y administración pública' },
      { trabajando_id:90, name: 'Poder judicial ' },
      { trabajando_id:91, name: 'Poder legislativo' },
      { trabajando_id:66, name: 'Productora de Cine y Tv' },
      { trabajando_id:44, name: 'Publicidad / Marketing / RRPP ' },
      { trabajando_id:45, name: 'Química' },
      { trabajando_id:98, name: 'Recursos Humanos' },
      { trabajando_id:61, name: 'Retail' },
      { trabajando_id:68, name: 'Salmonera' },
      { trabajando_id:92, name: 'Sector energético' },
      { trabajando_id:70, name: 'Seguridad' },
      { trabajando_id:46, name: 'Seguros / Previsión' },
      { trabajando_id:93, name: 'Servicios de Salud' },
      { trabajando_id:47, name: 'Servicios Financieros Varios' },
      { trabajando_id:74, name: 'Servicios funerarios' },
      { trabajando_id:48, name: 'Servicios Varios' },
      { trabajando_id:49, name: 'Siderurgia' },
      { trabajando_id:94, name: 'Siderurgía y Metalurgía' },
      { trabajando_id:95, name: 'Tabaco' },
      { trabajando_id:50, name: 'Tecnologías de Información' },
      { trabajando_id:51, name: 'Telecomunicaciones' },
      { trabajando_id:52, name: 'Textil' },
      { trabajando_id:53, name: 'Transporte' },
      { trabajando_id:54, name: 'Turismo' },
      { trabajando_id:56, name: 'Ventas' },
      { trabajando_id:96, name: 'Vidrio y envases' }
    ].each do |attrs|
      trabajando_company_activity = Trabajando::CompanyActivity.new(attrs)
      unless trabajando_company_activity.save
        errors << trabajando_company_activity
      end
    end
    puts "Se han cargado #{data.size - errors.size} de #{data.size} tipos de actividad de empresa"
  end

  def down
    Trabajando::CompanyActivity.destroy_all
    drop_table :trabajando_company_activities
  end
end
