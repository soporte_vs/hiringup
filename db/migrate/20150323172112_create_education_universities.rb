class CreateEducationUniversities < ActiveRecord::Migration
  def change
    create_table :education_universities do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
