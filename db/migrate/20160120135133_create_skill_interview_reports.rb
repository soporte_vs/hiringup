class CreateSkillInterviewReports < ActiveRecord::Migration
  def change
    create_table :skill_interview_reports do |t|
      t.text :observations
      t.string :document
      t.references :skill_interview_stage, index: true

      t.timestamps null: false
    end
    add_foreign_key :skill_interview_reports, :stage_bases, column: :skill_interview_stage_id
  end
end
