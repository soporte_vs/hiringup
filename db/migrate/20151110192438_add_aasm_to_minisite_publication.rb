class AddAasmToMinisitePublication < ActiveRecord::Migration
  def change
    add_column :minisite_publications, :aasm_state, :string
  end
end
