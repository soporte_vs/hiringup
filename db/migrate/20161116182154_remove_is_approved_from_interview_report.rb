class RemoveIsApprovedFromInterviewReport < ActiveRecord::Migration
  def change
    remove_column :interview_reports, :is_approved, :boolean
  end
end
