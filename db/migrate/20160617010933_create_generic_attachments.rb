class CreateGenericAttachments < ActiveRecord::Migration
  def change
    create_table :generic_attachments do |t|
      t.string :attachment
      t.string :name
      t.references :resourceable, polymorphic: true, index: {name: :attachment_index}

      t.timestamps null: false
    end
  end
end
