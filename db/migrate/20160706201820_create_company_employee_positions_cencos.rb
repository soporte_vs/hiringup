class CreateCompanyEmployeePositionsCencos < ActiveRecord::Migration
  def change
    create_table :company_employee_positions_cencos do |t|
      t.references :company_employee, index: true
      t.references :company_positions_cenco, index: {name: :index_company_positions_cenco_id}
      t.boolean :boss

      t.timestamps null: false
    end
    add_foreign_key :company_employee_positions_cencos, :company_employees
    add_foreign_key :company_employee_positions_cencos, :company_positions_cencos
  end
end
