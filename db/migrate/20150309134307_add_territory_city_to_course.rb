class AddTerritoryCityToCourse < ActiveRecord::Migration
  def change
    add_reference :courses, :territory_city, index: true
    add_foreign_key :courses, :territory_cities
  end
end
