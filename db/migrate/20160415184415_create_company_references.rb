class CreateCompanyReferences < ActiveRecord::Migration
  def change
    create_table :company_references do |t|
      t.references :company_employee, index: true
      t.references :applicant, index: true

      t.timestamps null: false
    end
    add_foreign_key :company_references, :company_employees
    add_foreign_key :company_references, :applicant_bases, column: :applicant_id
  end
end
