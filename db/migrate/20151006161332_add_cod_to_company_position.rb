class AddCodToCompanyPosition < ActiveRecord::Migration
  def change
    add_column :company_positions, :cod, :string
  end
end
