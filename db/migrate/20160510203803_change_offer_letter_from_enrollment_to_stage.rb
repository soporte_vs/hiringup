class ChangeOfferLetterFromEnrollmentToStage < ActiveRecord::Migration
  def up
    add_reference :offer_letters, :stage, index: true, foreign_key: true

    Offer::Letter.connection.schema_cache.clear!
    Offer::Letter.reset_column_information

    Offer::Letter.all.each do |offer_letter|
      offer_letter.stage_id = Offer::Stage.find_by(enrollment_id: offer_letter.enrollment_id).try(:id)
      offer_letter.save
    end

    remove_column :offer_letters, :enrollment_id
  end

  def down
    add_reference :offer_letters, :enrollment, index: true, foreign_key: true

    Offer::Letter.connection.schema_cache.clear!
    Offer::Letter.reset_column_information

    Offer::Letter.all.each do |offer_letter|
      offer_letter.enrollment_id = Offer::Stage.find_by(id: offer_letter.stage_id).try(:enrollment_id)
      offer_letter.save
    end

    remove_column :offer_letters, :stage_id
  end
end
