class AddDesactivateAtToLaborumPublications < ActiveRecord::Migration
  def change
    add_column :laborum_publications, :desactivate_at, :datetime
  end
end
