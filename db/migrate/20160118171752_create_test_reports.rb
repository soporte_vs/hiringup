class CreateTestReports < ActiveRecord::Migration
  def change
    create_table :test_reports do |t|
      t.text :observations
      t.string :document
      t.references :test_stage, index: true

      t.timestamps null: false
    end
    add_foreign_key :test_reports, :stage_bases, column: :test_stage_id
  end
end
