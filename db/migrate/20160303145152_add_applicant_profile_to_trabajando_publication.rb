class AddApplicantProfileToTrabajandoPublication < ActiveRecord::Migration
  def change
    add_reference :trabajando_publications, :applicant_profile, index: true
    add_foreign_key :trabajando_publications, :trabajando_applicant_profiles, column: :applicant_profile_id
  end
end
