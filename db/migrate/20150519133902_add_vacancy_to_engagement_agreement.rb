class AddVacancyToEngagementAgreement < ActiveRecord::Migration
  def change
      add_reference :engagement_agreements, :vacancy, index: true
      add_foreign_key :engagement_agreements, :vacancies
  end
end
