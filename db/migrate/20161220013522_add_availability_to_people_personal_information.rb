class AddAvailabilityToPeoplePersonalInformation < ActiveRecord::Migration
  def change
    add_column :people_personal_informations, :availability_replacements, :boolean
    add_column :people_personal_informations, :availability_work_other_residence, :boolean
  end
end
