class ChangeNamePsycholaboralStage < ActiveRecord::Migration
  def up
    PsycholaboralEvaluation::Stage.update_all(name: 'Evaluación por competencias / Psicolaboral')
    PsycholaboralEvaluation::Stage.all.map(&:enrollment).map(&:index_document)
  end

  def down
    PsycholaboralEvaluation::Stage.update_all(name: 'Evaluación psicolaboral')
    PsycholaboralEvaluation::Stage.all.map(&:enrollment).map(&:index_document)
  end
end
