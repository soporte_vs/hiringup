class AddObservationsToCalendarEvent < ActiveRecord::Migration
  def change
    add_column :calendar_events, :observations, :text
  end
end
