class AddHupeStudyLevelToTrabajandoEducationState < ActiveRecord::Migration
  def change
    add_reference :trabajando_education_states, :hupe_study_level, index: true
    add_foreign_key :trabajando_education_states, :education_study_levels, column: :hupe_study_level_id
  end
end
