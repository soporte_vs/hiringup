class AddIntegraHupAttrToCourse < ActiveRecord::Migration
  def change
    add_column :courses, :start, :date
    add_column :courses, :finish, :date
    add_column :courses, :experience_year, :integer
    add_column :courses, :is_supervisor, :boolean

    add_reference :courses, :contract_type, index: true
    add_foreign_key :courses, :company_contract_types, column: :contract_type_id

    add_reference :courses, :request_reason, index: true
    add_foreign_key :courses, :company_vacancy_request_reasons, column: :request_reason_id

    add_reference :courses, :timetable, index: true
    add_foreign_key :courses, :company_timetables, column: :timetable_id

    add_reference :courses, :study_type, index: true
    add_foreign_key :courses, :education_study_types, column: :study_type_id

    add_reference :courses, :study_level, index: true
    add_foreign_key :courses, :education_study_levels, column: :study_level_id

    add_reference :courses, :career, index: true
    add_foreign_key :courses, :education_careers, column: :career_id
  end
end
