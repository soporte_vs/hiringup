class CreateOfferLetters < ActiveRecord::Migration
  def change
    create_table :offer_letters do |t|
      t.references :company_position, index: true
      t.references :company_contract_type, index: true
      t.references :vacancy, index: true
      t.references :enrollment, index: true
      t.string :direct_boss
      t.integer :net_remuneration
      t.date :contract_start_date
      t.boolean :accepted
      t.string :token

      t.timestamps null: false
    end
    add_foreign_key :offer_letters, :company_positions
    add_foreign_key :offer_letters, :company_contract_types
    add_foreign_key :offer_letters, :vacancies
    add_foreign_key :offer_letters, :enrollment_bases, column: :enrollment_id
  end
end
