class CreateEducationCareers < ActiveRecord::Migration
  def change
    create_table :education_careers do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
