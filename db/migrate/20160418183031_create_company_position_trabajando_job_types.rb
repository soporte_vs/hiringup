class CreateCompanyPositionTrabajandoJobTypes < ActiveRecord::Migration
  def change
    create_table :company_position_trabajando_job_types do |t|
      t.references :company_position, index: { name: :position }
      t.references :trabajando_job_type, index: { name: :job_type }

      t.timestamps null: false
    end
    add_foreign_key :company_position_trabajando_job_types, :company_positions
    add_foreign_key :company_position_trabajando_job_types, :trabajando_job_types
  end
end
