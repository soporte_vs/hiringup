class CreateTagUsedTags < ActiveRecord::Migration
  def change
    create_table :tag_used_tags do |t|
      t.references :tag, index: true
      t.references :taggable, polymorphic: true, index: true

      t.timestamps null: false
    end
    add_foreign_key :tag_used_tags, :tag_base_tags, column: :tag_id
  end
end
