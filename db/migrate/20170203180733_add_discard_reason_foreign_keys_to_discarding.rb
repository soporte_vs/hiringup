# coding: utf-8
class AddDiscardReasonForeignKeysToDiscarding < ActiveRecord::Migration
  def up
    add_foreign_key :enrollment_discardings, :enrollment_discard_reasons, column: :discard_reason_id
  end

  def down
    remove_foreign_key :enrollment_discardings, column: :discard_reason_id
  end
end
