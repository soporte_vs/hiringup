class CreateTrabajandoWorkDays < ActiveRecord::Migration
  def up
    create_table :trabajando_work_days do |t|
      t.string :name
      t.references :hupe_timetable, index: true
      t.integer :trabajando_id

      t.timestamps null: false
    end
    add_foreign_key :trabajando_work_days, :company_timetables, column: :hupe_timetable_id

    errors = []
    data = [
      { trabajando_id:1, name: 'Jornada Completa' },
      { trabajando_id:2, name: 'Media Jornada' },
      { trabajando_id:3, name: 'Part Time' },
      { trabajando_id:4, name: 'Comisionista' },
      { trabajando_id:5, name: 'Reemplazo' },
      { trabajando_id:6, name: 'Práctica Profesional' },
      { trabajando_id:7, name: 'Por Turnos' },
      { trabajando_id:8, name: 'Free Lance' }
    ].each do |attrs|
      trabajando_work_day = Trabajando::WorkDay.new(attrs)
      unless trabajando_work_day.save
        errors << trabajando_work_day
      end
    end
    puts "Se han cargado #{data.size - errors.size} de #{data.size} estado civil"
  end

  def down
    Trabajando::WorkDay.destroy_all
    drop_table :trabajando_work_days
  end
end
