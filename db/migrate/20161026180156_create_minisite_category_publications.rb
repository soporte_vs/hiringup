class CreateMinisiteCategoryPublications < ActiveRecord::Migration
  def change
    create_table :minisite_category_publications do |t|

      t.references :minisite_category, index: true
      t.references :minisite_publication, index: true

      t.timestamps null: false
    end
    add_foreign_key :minisite_category_publications, :minisite_categories
    add_foreign_key :minisite_category_publications, :minisite_publications
  end
end

