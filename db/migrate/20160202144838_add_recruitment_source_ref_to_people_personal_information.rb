class AddRecruitmentSourceRefToPeoplePersonalInformation < ActiveRecord::Migration
  def change
    add_reference :people_personal_informations, :recruitment_source, index: true
    add_foreign_key :people_personal_informations, :company_recruitment_sources, column: :recruitment_source_id
  end
end
