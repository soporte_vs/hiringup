class CreateTechnicalTestReports < ActiveRecord::Migration
  def change
    create_table :technical_test_reports do |t|
      t.text :observations
      t.references :technical_test_stage, index: true

      t.timestamps null: false
    end
    add_foreign_key :technical_test_reports, :stage_bases, column: :technical_test_stage_id
  end
end