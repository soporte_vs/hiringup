class CreateCalendarEventGuests < ActiveRecord::Migration
  def change
    create_table :calendar_event_guests do |t|
      t.references :event, index: true
      t.string :name
      t.string :email

      t.timestamps null: false
    end
    add_foreign_key :calendar_event_guests, :calendar_events, column: :event_id
  end
end
