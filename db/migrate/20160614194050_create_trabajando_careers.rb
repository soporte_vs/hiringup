class CreateTrabajandoCareers < ActiveRecord::Migration
  def change
    create_table :trabajando_careers do |t|
      t.string :name
      t.integer :trabajando_id
      t.references :hupe_career, index: true

      t.timestamps null: false
    end
    add_foreign_key :trabajando_careers, :education_careers, column: :hupe_career_id
  end
end
