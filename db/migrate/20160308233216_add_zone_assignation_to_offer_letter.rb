class AddZoneAssignationToOfferLetter < ActiveRecord::Migration
  def change
    add_column :offer_letters, :zone_assignation, :integer
  end
end
