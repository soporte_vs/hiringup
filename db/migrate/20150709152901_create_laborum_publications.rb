class CreateLaborumPublications < ActiveRecord::Migration
  def change
    create_table :laborum_publications do |t|
      t.integer :service_id
      t.string :title
      t.text :description
      t.integer :state_id
      t.integer :city_id
      t.integer :area_id
      t.integer :subarea_id
      t.integer :pay_id
      t.integer :language_id
      t.integer :language_level_id
      t.integer :type_job_id
      t.integer :study_area_id
      t.integer :study_type_id
      t.integer :study_status_id
      t.integer :salary
      t.integer :experience
      t.string :question1
      t.string :question2
      t.string :question3
      t.string :question4
      t.string :question5
      t.references :course, index: true
      t.string :status

      t.timestamps null: false
    end
    add_foreign_key :laborum_publications, :courses
  end
end
