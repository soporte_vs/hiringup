class CreateCompanyManagements < ActiveRecord::Migration
  def change
    create_table :company_managements do |t|
      t.references :company_record, index: true
      t.references :company_business_unit, index: true

      t.timestamps null: false
    end
    add_foreign_key :company_managements, :company_records
    add_foreign_key :company_managements, :company_business_units
  end
end
