class MigrateInternalPostulation < ActiveRecord::Migration
  class InternalPostulation::Stage < Stage::Base
  end

  def up
    drop_table :internal_postulation_records
    Course.update_all(work_flow: Enrollment::Internal::WORK_FLOW)
    InternalPostulation::Stage.destroy_all
    # Adding Document Descriptor Internal Postulation
    document = Document::Descriptor.new(name: 'internal_postulation_template', details: 'Planilla de postulación interna')
    document.save
  end

  def down
    create_table :internal_postulation_records
  end
end
