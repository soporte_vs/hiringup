class CreateDocumentDescriptorsAndDocumentGroups < ActiveRecord::Migration
  def change
    create_table :document_descriptors_groups do |t|
      t.belongs_to :descriptor
      t.belongs_to :group
    end
    add_index :document_descriptors_groups, ["descriptor_id", "group_id"], :unique => true, :name => 'descriptors_groups_index'
  end
end
