class CreateGroupInterviewReports < ActiveRecord::Migration
  def change
    create_table :group_interview_reports do |t|
      t.text :observations
      t.references :group_interview_stage, index: true

      t.timestamps null: false
    end
    add_foreign_key :group_interview_reports, :stage_bases, column: :group_interview_stage_id
  end
end