class CreateCalendarEvents < ActiveRecord::Migration
  def change
    create_table :calendar_events do |t|
      t.datetime :starts_at
      t.datetime :ends_at
      t.string :address
      t.float :lat
      t.float :lng
      t.text :guests
      t.references :created_by, index: true

      t.timestamps null: false
    end
    add_foreign_key :calendar_events, :users, column: :created_by_id
  end
end
