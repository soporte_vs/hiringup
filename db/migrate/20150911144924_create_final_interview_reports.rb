class CreateFinalInterviewReports < ActiveRecord::Migration
  def change
    create_table :final_interview_reports do |t|
      t.text :observations
      t.references :final_interview_stage, index: true

      t.timestamps null: false
    end
    add_foreign_key :final_interview_reports, :stage_bases, column: :final_interview_stage_id
  end
end