class AddNationalityToPeoplePersonalInformation < ActiveRecord::Migration
  def change
    add_reference :people_personal_informations, :nationality, index: true
    add_foreign_key :people_personal_informations, :territory_countries, column: :nationality_id
  end
end
