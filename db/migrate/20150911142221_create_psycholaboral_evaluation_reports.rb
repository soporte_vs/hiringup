class CreatePsycholaboralEvaluationReports < ActiveRecord::Migration
  def change
    create_table :psycholaboral_evaluation_reports do |t|
      t.text :observations
      t.references :psycholaboral_evaluation_stage, index: { name: :psycolb_eva }

      t.timestamps null: false
    end
    add_foreign_key :psycholaboral_evaluation_reports, :stage_bases, column: :psycholaboral_evaluation_stage_id
  end
end