class CreateCompanyPositions < ActiveRecord::Migration
  def change
    create_table :company_positions do |t|
      t.string :name
      t.text :description

      t.timestamps null: false
    end
  end
end
