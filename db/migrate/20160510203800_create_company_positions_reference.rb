class CreateCompanyPositionsReference < ActiveRecord::Migration
  def change
    create_table :company_positions_references do |t|
      t.references :company_reference, index: true
      t.references :company_position, index: true

      t.timestamps null: false
    end
    add_foreign_key :company_positions_references, :company_references, column: :company_reference_id
    add_foreign_key :company_positions_references, :company_positions
  end
end
