class AddUserIdToVideoInterviewAnswer < ActiveRecord::Migration
  def change
    add_column :video_interview_answers, :user_id, :integer
  end
end
