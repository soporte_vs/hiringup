class AddAttendedToCalendarEventResourceables < ActiveRecord::Migration
  def change
    add_column :calendar_event_resourceables, :attended, :boolean
  end
end
