class ChangeExperienceYearsToCourse < ActiveRecord::Migration
  def change
    rename_column :courses, :experience_year, :experience_years
  end
end
