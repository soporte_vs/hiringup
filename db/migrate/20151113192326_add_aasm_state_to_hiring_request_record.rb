class AddAasmStateToHiringRequestRecord < ActiveRecord::Migration
  def change
    add_column :hiring_request_records, :aasm_state, :string
  end
end
