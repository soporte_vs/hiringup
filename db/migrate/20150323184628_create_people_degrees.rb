class CreatePeopleDegrees < ActiveRecord::Migration
  def change
    create_table :people_degrees do |t|
      t.references :career, index: true
      t.references :professional_information, index: true
      t.string :condition
      t.references :university, index: true
      t.date :culmination_date

      t.timestamps null: false
    end
    add_foreign_key :people_degrees, :education_careers, column: :career_id
    add_foreign_key :people_degrees, :people_professional_informations, column: :professional_information_id
    add_foreign_key :people_degrees, :education_universities, column: :university_id
  end
end
