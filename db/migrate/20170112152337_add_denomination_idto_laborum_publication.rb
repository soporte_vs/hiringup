class AddDenominationIdtoLaborumPublication < ActiveRecord::Migration
  def change
    add_column :laborum_publications, :denomination_id, :integer, default: 0
  end
end
