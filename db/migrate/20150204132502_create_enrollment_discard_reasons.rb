class CreateEnrollmentDiscardReasons < ActiveRecord::Migration
  def change
    create_table :enrollment_discard_reasons do |t|
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end
