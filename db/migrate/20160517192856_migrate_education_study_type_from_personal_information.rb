module Education
  class StudyType < ActiveRecord::Base
  end
end

class MigrateEducationStudyTypeFromPersonalInformation < ActiveRecord::Migration
  def up
    add_column :people_personal_informations, :study_type, :string
    add_column :courses, :study_type, :string
    add_column :trabajando_education_levels, :hupe_study_type, :string

    Education::StudyType.all.each do |education_study_type|
      case education_study_type.name
      when "Doctorado"
        People::PersonalInformation.where(education_study_type_id: education_study_type.id).update_all(study_type: People::PhdDegree)
        Course.where(study_type_id: education_study_type.id).update_all(study_type: People::PhdDegree)
        Trabajando::EducationLevel.where(hupe_study_type_id: education_study_type.id).update_all(hupe_study_type: People::PhdDegree)
      when "Enseñanza Media"
        People::PersonalInformation.where(education_study_type_id: education_study_type.id).update_all(study_type: People::HighSchoolDegree)
        Course.where(study_type_id: education_study_type.id).update_all(study_type: People::HighSchoolDegree)
        Trabajando::EducationLevel.where(hupe_study_type_id: education_study_type.id).update_all(hupe_study_type: People::HighSchoolDegree)
      when "Liceo Técnico"
        People::PersonalInformation.where(education_study_type_id: education_study_type.id).update_all(study_type: People::TechnicalDegree)
        Course.where(study_type_id: education_study_type.id).update_all(study_type: People::TechnicalDegree)
        Trabajando::EducationLevel.where(hupe_study_type_id: education_study_type.id).update_all(hupe_study_type: People::TechnicalDegree)
      when "Magister"
        People::PersonalInformation.where(education_study_type_id: education_study_type.id).update_all(study_type: People::MagisterDegree)
        Course.where(study_type_id: education_study_type.id).update_all(study_type: People::MagisterDegree)
        Trabajando::EducationLevel.where(hupe_study_type_id: education_study_type.id).update_all(hupe_study_type: People::MagisterDegree)
      when "Postgrado"
        People::PersonalInformation.where(education_study_type_id: education_study_type.id).update_all(study_type: People::PostgraduateDegree)
        Course.where(study_type_id: education_study_type.id).update_all(study_type: People::PostgraduateDegree)
        Trabajando::EducationLevel.where(hupe_study_type_id: education_study_type.id).update_all(hupe_study_type: People::PostgraduateDegree)
      when "Técnico Profesional"
        People::PersonalInformation.where(education_study_type_id: education_study_type.id).update_all(study_type: People::TechnicalProfessionalDegree)
        Course.where(study_type_id: education_study_type.id).update_all(study_type: People::TechnicalProfessionalDegree)
        Trabajando::EducationLevel.where(hupe_study_type_id: education_study_type.id).update_all(hupe_study_type: People::TechnicalProfessionalDegree)
      when "Universitaria"
        People::PersonalInformation.where(education_study_type_id: education_study_type.id).update_all(study_type: People::ProfessionalDegree)
        Course.where(study_type_id: education_study_type.id).update_all(study_type: People::ProfessionalDegree)
        Trabajando::EducationLevel.where(hupe_study_type_id: education_study_type.id).update_all(hupe_study_type: People::ProfessionalDegree)
      else
      end
    end

    remove_reference :people_personal_informations, :education_study_type, index: true
    remove_reference :courses, :study_type, index: true
    remove_reference :trabajando_education_levels, :hupe_study_type, index: true

    drop_table :education_study_types
  end

  def down
    create_table :education_study_types do |t|
      t.string :name

      t.timestamps null: false
    end
    add_reference :people_personal_informations, :education_study_type, index: true
    add_reference :courses, :study_type, index: true
    add_reference :trabajando_education_levels, :hupe_study_type, index: true

    [
      'People::PrimarySchoolDegree',
      'People::HighSchoolDegree',
      'People::TechnicalDegree',
      'People::TechnicalProfessionalDegree',
      'People::ProfessionalDegree',
      'People::PostgraduateDegree',
      'People::MagisterDegree',
      'People::PhdDegree'
    ].each do |study_type|
      case study_type
      when "People::PrimarySchoolDegree"
        education_study_type = Education::StudyType.find_or_create_by(name: "Enseñanza Media")
      when "People::HighSchoolDegree"
        education_study_type = Education::StudyType.find_or_create_by(name: "Enseñanza Media")
      when "People::TechnicalDegree"
        education_study_type = Education::StudyType.find_or_create_by(name: "Liceo Técnico")
      when "People::TechnicalProfessionalDegree"
        education_study_type = Education::StudyType.find_or_create_by(name: "Técnico Profesional")
      when "People::ProfessionalDegree"
        education_study_type = Education::StudyType.find_or_create_by(name: "Universitaria")
      when "People::PostgraduateDegree"
        education_study_type = Education::StudyType.find_or_create_by(name: "Postgrado")
      when "People::MagisterDegree"
        education_study_type = Education::StudyType.find_or_create_by(name: "Magister")
      when "People::PhdDegree"
        education_study_type = Education::StudyType.find_or_create_by(name: "Doctorado")
      else
      end
      People::PersonalInformation.where(
        study_type: study_type
      ).update_all(education_study_type_id: education_study_type.id)
      Course.where(
        study_type: study_type
      ).update_all(study_type_id: education_study_type.id)
      Trabajando::EducationLevel.where(
        hupe_study_type: study_type
      ).update_all(hupe_study_type_id: education_study_type.id)
    end
    remove_column :people_personal_informations, :study_type, :string
    remove_column :courses, :study_type, :string
    remove_column :trabajando_education_levels, :hupe_study_type, :string
  end
end
