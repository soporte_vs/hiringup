class CreateTrabajandoJobTypes < ActiveRecord::Migration
  def up
    create_table :trabajando_job_types do |t|
      t.string :name
      t.integer :trabajando_id
      t.references :hupe_position, index: true

      t.timestamps null: false
    end
    add_foreign_key :trabajando_job_types, :company_positions, column: :hupe_position_id

    errors = []
    data = [
      { trabajando_id:28, name: 'Abogado' },
      { trabajando_id:29, name: 'Abogado Independiente' },
      { trabajando_id:30, name: 'Administrador' },
      { trabajando_id:17, name: 'Administrativo' },
      { trabajando_id:26, name: 'Agente' },
      { trabajando_id:31, name: 'Ama de casa' },
      { trabajando_id:2, name: 'Analista' },
      { trabajando_id:32, name: 'Asesor' },
      { trabajando_id:3, name: 'Asistente' },
      { trabajando_id:33, name: 'Asociado' },
      { trabajando_id:34, name: 'Auditor' },
      { trabajando_id:35, name: 'Auxiliar' },
      { trabajando_id:36, name: 'Ayudante' },
      { trabajando_id:37, name: 'Becario' },
      { trabajando_id:62, name: 'Cajero' },
      { trabajando_id:27, name: 'Cobrador' },
      { trabajando_id:63, name: 'Comercial' },
      { trabajando_id:4, name: 'Consultor/Asesor' },
      { trabajando_id:38, name: 'Contador' },
      { trabajando_id:39, name: 'Contralor' },
      { trabajando_id:40, name: 'Coordinador' },
      { trabajando_id:41, name: 'Desarrollador' },
      { trabajando_id:42, name: 'Director' },
      { trabajando_id:25, name: 'Diseñador' },
      { trabajando_id:43, name: 'Dueño' },
      { trabajando_id:67, name: 'Educador/Docente' },
      { trabajando_id:44, name: 'Ejecutivo Cuenta' },
      { trabajando_id:5, name: 'Ejecutivo Junior' },
      { trabajando_id:6, name: 'Ejecutivo Senior' },
      { trabajando_id:45, name: 'Encargado' },
      { trabajando_id:46, name: 'Especialista' },
      { trabajando_id:16, name: 'Estudiante' },
      { trabajando_id:47, name: 'Estudiante de Posgrado' },
      { trabajando_id:22, name: 'Estudiante para tesis' },
      { trabajando_id:48, name: 'Gerente' },
      { trabajando_id:10, name: 'Gerente/Director de Área' },
      { trabajando_id:11, name: 'Gerente/Director de División' },
      { trabajando_id:13, name: 'Gerente/Director General' },
      { trabajando_id:49, name: 'Ingeniero' },
      { trabajando_id:50, name: 'Inspector' },
      { trabajando_id:8, name: 'Jefe Área/Sección/Depto./Local' },
      { trabajando_id:51, name: 'Líder de Proyecto' },
      { trabajando_id:60, name: 'Maestro de cocina' },
      { trabajando_id:66, name: 'Médico' },
      { trabajando_id:15, name: 'Miembro de Directorio' },
      { trabajando_id:52, name: 'Notario' },
      { trabajando_id:19, name: 'Operario' },
      { trabajando_id:20, name: 'Operativo' },
      { trabajando_id:1, name: 'Otro Profesional' },
      { trabajando_id:54, name: 'Planeador' },
      { trabajando_id:21, name: 'Practica profesional' },
      { trabajando_id:14, name: 'Presidente/Director Ejecutivo' },
      { trabajando_id:64, name: 'Profesional Independiente' },
      { trabajando_id:55, name: 'Profesor / Investigador' },
      { trabajando_id:56, name: 'Promotor' },
      { trabajando_id:61, name: 'Reponedor / Repositor' },
      { trabajando_id:57, name: 'Representante' },
      { trabajando_id:58, name: 'Socio' },
      { trabajando_id:9, name: 'Subgerente de Área' },
      { trabajando_id:12, name: 'Subgerente/Subdirector General' },
      { trabajando_id:7, name: 'Supervisor' },
      { trabajando_id:18, name: 'Técnico' },
      { trabajando_id:24, name: 'Técnico Superior Universitario' },
      { trabajando_id:59, name: 'Tesorero' },
      { trabajando_id:65, name: 'Vendedor' }
    ].each do |attrs|
      trabajando_job_type = Trabajando::JobType.new(attrs)
      unless trabajando_job_type.save
        errors << trabajando_job_type
      end
    end
    puts "Se han cargado #{data.size - errors.size} de #{data.size} Cargos"
  end

  def down

  end
end
