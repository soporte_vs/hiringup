class CreateTerritoryStates < ActiveRecord::Migration
  def change
    create_table :territory_states do |t|
      t.string :name
      t.references :territory_country, index: true

      t.timestamps null: false
    end
    add_foreign_key :territory_states, :territory_countries
  end
end
