class AddDiaryEventToDiaryAppointment < ActiveRecord::Migration
  def change
      add_reference :diary_appointments, :diary_event, index: true
      add_foreign_key :diary_appointments, :diary_events
  end
end
