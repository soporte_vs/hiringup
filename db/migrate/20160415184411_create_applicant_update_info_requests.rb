class CreateApplicantUpdateInfoRequests < ActiveRecord::Migration
  def change
    create_table :applicant_update_info_requests do |t|
      t.references :applicant, index: true

      t.timestamps null: false
    end
    add_foreign_key :applicant_update_info_requests, :applicant_bases, column: :applicant_id
  end
end
