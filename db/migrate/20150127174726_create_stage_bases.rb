class CreateStageBases < ActiveRecord::Migration
  def change
    create_table :stage_bases do |t|
      t.references :enrollment, index: true
      t.string :type
      
      t.timestamps
    end
  end
end
