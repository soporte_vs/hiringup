class AddHupeAreaToTrabajandoArea < ActiveRecord::Migration
  def change
    add_reference :trabajando_areas, :hupe_area, index: true
    add_foreign_key :trabajando_areas, :company_areas, column: :hupe_area_id
  end
end
