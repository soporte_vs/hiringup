class MigrateDocumentsTestReport < ActiveRecord::Migration

class Test::Report < ActiveRecord::Base
  mount_uploader :document, GenericReportStageUploader
  belongs_to :test_stage,
             class_name: Test::Stage,
             foreign_key: :test_stage_id

  has_many :documents,
            class_name: Stage::Document,
            as: :resource,
            dependent: :destroy

    validates :observations, presence: true
    validates :test_stage, presence: true
  end

  def up
    Test::Report.all.each do |report|
      report.documents.create(document: report.document)
    end
    remove_column :test_reports, :document
  end

  def down
    add_column :test_reports, :document, :string
  end

end
