class CreateCompanyCencos < ActiveRecord::Migration
  def change
    create_table :company_cencos do |t|
      t.references :company_business_unit, index: true
      t.string :name
      t.string :address

      t.timestamps null: false
    end
    add_foreign_key :company_cencos, :company_business_units
  end
end
