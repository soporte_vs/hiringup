class CreateCompanyEngagementOrigins < ActiveRecord::Migration
  def change
    create_table :company_engagement_origins do |t|
      t.string :name
      t.text :description

      t.timestamps null: false
    end
  end
end
