class CreateDocumentDescriptors < ActiveRecord::Migration
  def change
    create_table :document_descriptors do |t|
      t.string :name, :null => false
      t.boolean :required, :default => false
      t.string :details
      t.string :allowed_extensions

      t.timestamps null: false
    end
  end
end
