class CreateEnrollmentBases < ActiveRecord::Migration
  def change
    create_table :enrollment_bases do |t|
      t.string :type
      t.integer :course_id

      t.timestamps
    end
  end
end
