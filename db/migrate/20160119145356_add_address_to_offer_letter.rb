class AddAddressToOfferLetter < ActiveRecord::Migration
  def change
    add_column :offer_letters, :address, :string
  end
end
