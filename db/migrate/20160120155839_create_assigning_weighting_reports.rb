class CreateAssigningWeightingReports < ActiveRecord::Migration
  def change
    create_table :assigning_weighting_reports do |t|
      t.string :document
      t.references :assigning_weighting_stage, index: {name: :assigning_weighting_stage}
      t.timestamps null: false
    end
    add_foreign_key :assigning_weighting_reports, :stage_bases, column: :assigning_weighting_stage_id
  end
end
