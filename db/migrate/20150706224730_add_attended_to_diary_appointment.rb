class AddAttendedToDiaryAppointment < ActiveRecord::Migration
  def change
    add_column :diary_appointments, :attended, :boolean
  end
end
