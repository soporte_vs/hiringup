class CreateTriggerBases < ActiveRecord::Migration
  def change
    create_table :trigger_bases do |t|
      t.string :type
      t.references :resourceable, polymorphic: true, index: true

      t.timestamps null: false
    end
  end
end
