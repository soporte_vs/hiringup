class CreateInternalPostulationRecords < ActiveRecord::Migration
  def change
    create_table :internal_postulation_records do |t|
      t.string :template_file
      t.references :internal_postulation_stage, index: { name: :inter_postu_stage }
      t.references :created_by, index: true

      t.timestamps null: false
    end
    add_foreign_key :internal_postulation_records, :stage_bases, column: :internal_postulation_stage_id
    add_foreign_key :internal_postulation_records, :users, column: :created_by_id
  end
end
