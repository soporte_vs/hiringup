class MigrateDocumentsPsycholaboralEvaluationReport < ActiveRecord::Migration

  class PsycholaboralEvaluation::Report < ActiveRecord::Base
    mount_uploader :file, GenericReportStageUploader
    belongs_to :psycholaboral_evaluation_stage,
               class_name: PsycholaboralEvaluation::Stage,
               foreign_key: :psycholaboral_evaluation_stage_id

  has_many :documents,
            class_name: Stage::Document,
            as: :resource,
            dependent: :destroy

    validates :observations, presence: true
    validates :notified_to, presence: true, email: true
    validates :psycholaboral_evaluation_stage, presence: true
  end

  def up
    PsycholaboralEvaluation::Report.all.each do |report|
      report.documents.create(document: report.file)
    end
    remove_column :psycholaboral_evaluation_reports, :file
  end

  def down
    add_column :psycholaboral_evaluation_reports, :file, :string
  end

end
