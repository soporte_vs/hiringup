class CreateStageNotApplyings < ActiveRecord::Migration
  def change
    create_table :stage_not_applyings do |t|
      t.references :stage, index: true
      t.references :not_apply_reason, index: true
      t.text :observations

      t.timestamps null: false
    end
    add_foreign_key :stage_not_applyings, :stage_bases, column: :stage_id
    add_foreign_key :stage_not_applyings, :stage_not_apply_reasons, column: :not_apply_reason_id
  end
end
