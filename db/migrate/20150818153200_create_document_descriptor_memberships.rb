class CreateDocumentDescriptorMemberships < ActiveRecord::Migration
  def change
    create_table :document_descriptor_memberships do |t|
      t.integer :document_descriptor_id
      t.integer :document_group_id
      t.boolean :document_required

      t.timestamps null: false
    end
  end
end
