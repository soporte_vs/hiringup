class CreateMinisiteBanners < ActiveRecord::Migration
  def change
    create_table :minisite_banners do |t|
      t.string :title
      t.string :image
      t.boolean :active_image

      t.timestamps null: false
    end
  end
end
