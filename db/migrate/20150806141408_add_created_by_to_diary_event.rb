class AddCreatedByToDiaryEvent < ActiveRecord::Migration
  def change
    add_reference :diary_events, :created_by, index: true
    add_foreign_key :diary_events, :users
  end
end
