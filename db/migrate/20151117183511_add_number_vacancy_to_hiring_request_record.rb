class AddNumberVacancyToHiringRequestRecord < ActiveRecord::Migration
  def change
    add_column :hiring_request_records, :number_vacancies, :integer, default: 1
  end
end
