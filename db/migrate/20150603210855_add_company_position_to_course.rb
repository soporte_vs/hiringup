class AddCompanyPositionToCourse < ActiveRecord::Migration
  def change
    add_reference :courses, :company_position, index: true
    add_foreign_key :courses, :company_positions
  end
end
