module Diary
  class Appointment < ActiveRecord::Base
    self.table_name = 'diary_appointments'
    belongs_to :user
    belongs_to :created_by, class_name: User
    belongs_to :manager, class_name: User
    belongs_to :resource, polymorphic: true
  end
end

module Scheduling
  class Appointment < ActiveRecord::Base
    self.table_name = 'scheduling_appointments'
    belongs_to :diary_appointment,
               class_name: Diary::Appointment,
               foreign_key: :diary_appointment_id,
               dependent: :destroy
  end

  class Stage < ::Stage::Base
    has_one :scheduling_appointment,
            class_name: Scheduling::Appointment,
            foreign_key: :scheduling_stage_id,
            dependent: :destroy

    has_many :not_applyings, class_name: ::Stage::NotApplying, dependent: :destroy
  end
end

class CreateEventsFromDiaryAppointments < ActiveRecord::Migration
  def up
    begin

      Calendar::Event.skip_callback(:create, :after, :notify_creation)
      Calendar::Event.skip_callback(:update, :after, :notify_updates)

      Calendar::EventGuest.Calendar::EventGuest.skip_callback(:create, :after, :send_create_notification)
      Calendar::EventGuest.Calendar::EventGuest.skip_callback(:destroy, :before, :send_remove_notification)
      Calendar::EventGuest.Calendar::EventGuest.skip_callback(:touch, :after, :send_update_notification)

      Calendar::EventManager.skip_callback(:create, :after, :send_create_notification)
      Calendar::EventManager.skip_callback(:destroy, :after, :send_remove_notification)
      Calendar::EventManager.skip_callback(:touch, :after, :send_update_notification)

      Calendar::EventResourceable.skip_callback(:create, :after, :send_create_notification)
      Calendar::EventResourceable.skip_callback(:destroy, :after, :send_remove_notification)
      Calendar::EventResourceable.skip_callback(:touch, :after, :send_update_notification)

      Diary::Appointment.all.each do |diary_appointment|
        event = Calendar::Event.new
        event.manager_attributes = {
          name: "#{diary_appointment.manager.first_name} #{diary_appointment.manager.last_name}",
          email: diary_appointment.manager.email
        }
        event.starts_at = diary_appointment.starts_at
        event.ends_at = diary_appointment.ends_at
        event.address = diary_appointment.address
        event.created_by = diary_appointment.created_by
        event.stages << diary_appointment.resource if diary_appointment.resource.present?
        event.save if event.stages.any?
      end
    rescue
      false
    end

    begin
      Stage::Base.where(type: 'Scheduling::Stage').each do |scheduling_stage|
        scheduling_stage.destroy
      end
    rescue
      false
    end
  end

  def down
    Calendar::Event.destroy_all
  end
end
