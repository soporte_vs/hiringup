class AddFileToGroupInterviewReport < ActiveRecord::Migration
  def change
    add_column :group_interview_reports, :file, :string
    add_column :group_interview_reports, :notified_to, :string
  end
end
