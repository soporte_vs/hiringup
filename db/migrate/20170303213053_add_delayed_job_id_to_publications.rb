class AddDelayedJobIdToPublications < ActiveRecord::Migration
  def change
    add_column :minisite_publications, :delayed_job_id, :integer
    add_index  :minisite_publications, :delayed_job_id
    add_column :laborum_publications, :delayed_job_id, :integer
    add_index  :laborum_publications, :delayed_job_id
    add_column :trabajando_publications, :delayed_job_id, :integer
    add_index  :trabajando_publications, :delayed_job_id
  end
end
