class AddHiringRequestRecordToVacancy < ActiveRecord::Migration
  def change
    add_reference :vacancies, :hiring_request_record, index: true
    add_foreign_key :vacancies, :hiring_request_records
  end
end
