class AddCompanyActivityToTrabajandoPublication < ActiveRecord::Migration
  def change
    add_reference :trabajando_publications, :company_activity, index: true
    add_foreign_key :trabajando_publications, :trabajando_company_activities, column: :company_activity_id
  end
end
