class AddTerritoryCityToPersonalInformation < ActiveRecord::Migration
  def change
    add_reference :people_personal_informations , :territory_city, index: true
    add_foreign_key :people_personal_informations , :territory_cities
  end
end
