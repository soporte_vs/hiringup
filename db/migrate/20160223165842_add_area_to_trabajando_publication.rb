class AddAreaToTrabajandoPublication < ActiveRecord::Migration
  def change
    add_reference :trabajando_publications, :area, index: true
    add_foreign_key :trabajando_publications, :trabajando_areas, column: :area_id
  end
end
