class AddDescriptionToLaboralExperience < ActiveRecord::Migration
  def change
    add_column :people_laboral_experiences, :description, :text
  end
end
