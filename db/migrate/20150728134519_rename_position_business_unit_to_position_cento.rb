class RenamePositionBusinessUnitToPositionCento < ActiveRecord::Migration
  def change
    remove_reference(:company_positions_business_units, :company_business_unit, index: { name: :cp_business_units }, foreign_key: true)

    rename_table :company_positions_business_units, :company_positions_cencos
    add_reference(:company_positions_cencos, :company_cenco, index: true)

    add_foreign_key :company_positions_cencos, :company_positions
    add_foreign_key :company_positions_cencos, :company_cencos

    rename_column :vacancies, :company_positions_business_unit_id, :company_positions_cenco_id
    add_foreign_key :vacancies, :company_positions_cencos
  end
end
