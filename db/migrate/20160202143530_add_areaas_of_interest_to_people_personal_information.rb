class AddAreaasOfInterestToPeoplePersonalInformation < ActiveRecord::Migration
  def change
    add_column :people_personal_informations, :areas_of_interest, :text
  end
end
