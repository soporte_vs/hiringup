class AddEnrollmentIdToDocumentRecord < ActiveRecord::Migration
  def change
    add_column :document_records, :enrollment_id, :integer
  end
end
