class AddNameToPeopleDegree < ActiveRecord::Migration
  def change
    add_column :people_degrees, :name, :string
  end
end
