# This migration comes from abstract_hiringup (originally 20150106133202)
class CreateAbstractHiringupStages < ActiveRecord::Migration
  def change
    create_table :abstract_hiringup_stages do |t|
      t.string :type
      t.string :name
      t.references :enrollment, index: true

      t.timestamps
    end
  end
end
