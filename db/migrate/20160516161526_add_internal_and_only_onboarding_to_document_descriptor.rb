class AddInternalAndOnlyOnboardingToDocumentDescriptor < ActiveRecord::Migration
  def change
    add_column :document_descriptors, :internal, :boolean, default: false
    add_column :document_descriptors, :only_onboarding, :boolean, default: false
  end
end
