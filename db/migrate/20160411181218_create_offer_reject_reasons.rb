class CreateOfferRejectReasons < ActiveRecord::Migration
  def change
    create_table :offer_reject_reasons do |t|
      t.string :name
      t.text :description

      t.timestamps null: false
    end
  end
end
