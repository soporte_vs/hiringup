class CreatePeoplePersonalInformations < ActiveRecord::Migration
  def change
    create_table :people_personal_informations do |t|
      t.string :first_name
      t.string :last_name
      t.date :birth_date
      t.string :landline
      t.string :cellphone
      t.string :sex
      t.references :user, index: true

      t.timestamps null: false
    end
    add_foreign_key :people_personal_informations, :users
  end
end
