class CreateTrabajandoPublications < ActiveRecord::Migration
  def change
    create_table :trabajando_publications do |t|
      t.string :title
      t.string :description
      t.text :q1
      t.text :q2
      t.text :q3
      t.text :q4
      t.text :q5
      t.integer :number_vacancies
      t.float :salary
      t.references :course, index: true

      t.timestamps null: false
    end
    add_foreign_key :trabajando_publications, :courses
  end
end
