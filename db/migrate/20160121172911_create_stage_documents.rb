class CreateStageDocuments < ActiveRecord::Migration
  def change
    create_table :stage_documents do |t|
      t.references :resource, polymorphic: true, index: true
      t.string :document
    end
  end
end
