class CreateTrabajandoCities < ActiveRecord::Migration
  def up
    create_table :trabajando_cities do |t|
      t.string :name
      t.integer :trabajando_id
      t.references :hupe_city, index: true

      t.timestamps null: false
    end
    add_foreign_key :trabajando_cities, :territory_cities, column: :hupe_city_id

    errors = []
    data = [
      { trabajando_id:1, name: 'Arica' },
      { trabajando_id:2, name: 'Camarones' },
      { trabajando_id:3, name: 'Putre' },
      { trabajando_id:4, name: 'General Lagos' },
      { trabajando_id:5, name: 'Iquique' },
      { trabajando_id:6, name: 'Huara' },
      { trabajando_id:7, name: 'Camiña' },
      { trabajando_id:8, name: 'Colchane' },
      { trabajando_id:9, name: 'Pica' },
      { trabajando_id:10, name: 'Pozo Almonte' },
      { trabajando_id:11, name: 'Tocopilla' },
      { trabajando_id:12, name: 'María Elena' },
      { trabajando_id:13, name: 'Calama' },
      { trabajando_id:14, name: 'Ollague' },
      { trabajando_id:15, name: 'San Pedro de Atacama' },
      { trabajando_id:16, name: 'Antofagasta' },
      { trabajando_id:17, name: 'Mejillones' },
      { trabajando_id:18, name: 'Sierra Gorda' },
      { trabajando_id:19, name: 'Taltal' },
      { trabajando_id:20, name: 'Chañaral' },
      { trabajando_id:21, name: 'Diego de Almagro' },
      { trabajando_id:22, name: 'Copiapó' },
      { trabajando_id:23, name: 'Caldera' },
      { trabajando_id:24, name: 'Tierra Amarilla' },
      { trabajando_id:25, name: 'Vallenar' },
      { trabajando_id:26, name: 'Freirina' },
      { trabajando_id:27, name: 'Huasco' },
      { trabajando_id:28, name: 'Alto del Carmen' },
      { trabajando_id:29, name: 'La Serena' },
      { trabajando_id:30, name: 'La Higuera' },
      { trabajando_id:31, name: 'Coquimbo' },
      { trabajando_id:32, name: 'Andacollo' },
      { trabajando_id:33, name: 'Vicuña' },
      { trabajando_id:34, name: 'Paihuano' },
      { trabajando_id:35, name: 'Ovalle' },
      { trabajando_id:36, name: 'Río Hurtado' },
      { trabajando_id:37, name: 'Monte Patria' },
      { trabajando_id:38, name: 'Combarbalá' },
      { trabajando_id:39, name: 'Punitaqui' },
      { trabajando_id:40, name: 'Illapel' },
      { trabajando_id:41, name: 'Salamanca' },
      { trabajando_id:42, name: 'Los Vilos' },
      { trabajando_id:43, name: 'Canela o Mincha' },
      { trabajando_id:44, name: 'Valparaíso' },
      { trabajando_id:45, name: 'Casablanca' },
      { trabajando_id:46, name: 'Concon' },
      { trabajando_id:47, name: 'Juan Fernández' },
      { trabajando_id:48, name: 'Puchuncaví' },
      { trabajando_id:49, name: 'Quilpué' },
      { trabajando_id:50, name: 'Quintero' },
      { trabajando_id:51, name: 'Villa Alemana' },
      { trabajando_id:52, name: 'Viña del Mar' },
      { trabajando_id:53, name: 'Petorca' },
      { trabajando_id:54, name: 'La Ligua' },
      { trabajando_id:55, name: 'Cabildo' },
      { trabajando_id:56, name: 'Papudo' },
      { trabajando_id:57, name: 'Zapallar' },
      { trabajando_id:58, name: 'Los Andes' },
      { trabajando_id:59, name: 'San Esteban' },
      { trabajando_id:60, name: 'Calle Larga' },
      { trabajando_id:61, name: 'Rinconada' },
      { trabajando_id:62, name: 'San Felipe' },
      { trabajando_id:63, name: 'Catemu' },
      { trabajando_id:64, name: 'Llayllay' },
      { trabajando_id:65, name: 'Panquehue' },
      { trabajando_id:66, name: 'Putaendo' },
      { trabajando_id:67, name: 'Santa María' },
      { trabajando_id:68, name: 'Quillota' },
      { trabajando_id:69, name: 'Calera' },
      { trabajando_id:70, name: 'Hijuelas' },
      { trabajando_id:71, name: 'Limache' },
      { trabajando_id:72, name: 'La Cruz' },
      { trabajando_id:73, name: 'Nogales' },
      { trabajando_id:74, name: 'Olmué' },
      { trabajando_id:75, name: 'San Antonio' },
      { trabajando_id:76, name: 'Algarrobo' },
      { trabajando_id:77, name: 'Cartagena' },
      { trabajando_id:78, name: 'El Quisco' },
      { trabajando_id:79, name: 'El Tabo' },
      { trabajando_id:80, name: 'Santo Domingo' },
      { trabajando_id:81, name: 'Isla de Pascua' },
      { trabajando_id:82, name: 'Rancagua' },
      { trabajando_id:83, name: 'Codegua' },
      { trabajando_id:84, name: 'Coinco' },
      { trabajando_id:85, name: 'Coltauco' },
      { trabajando_id:86, name: 'Doñihue' },
      { trabajando_id:87, name: 'Graneros' },
      { trabajando_id:88, name: 'Las Cabras' },
      { trabajando_id:89, name: 'Mostazal' },
      { trabajando_id:90, name: 'Machalí' },
      { trabajando_id:91, name: 'Malloa' },
      { trabajando_id:92, name: 'Olivar' },
      { trabajando_id:93, name: 'Peumo' },
      { trabajando_id:94, name: 'Pichidegua' },
      { trabajando_id:95, name: 'Quinta de Tilcoco' },
      { trabajando_id:96, name: 'Rengo' },
      { trabajando_id:97, name: 'Requinoa' },
      { trabajando_id:98, name: 'San Vicente' },
      { trabajando_id:99, name: 'San Fernando' },
      { trabajando_id:100, name: 'Chépica' },
      { trabajando_id:101, name: 'Chimbarongo' },
      { trabajando_id:102, name: 'Lolol' },
      { trabajando_id:103, name: 'Nancagua' },
      { trabajando_id:104, name: 'Palmilla' },
      { trabajando_id:105, name: 'Peralillo' },
      { trabajando_id:106, name: 'Placilla' },
      { trabajando_id:107, name: 'Pumanque' },
      { trabajando_id:108, name: 'Santa Cruz' },
      { trabajando_id:109, name: 'Pichilemu' },
      { trabajando_id:110, name: 'La Estrella' },
      { trabajando_id:111, name: 'Litueche' },
      { trabajando_id:112, name: 'Marchigue' },
      { trabajando_id:113, name: 'Navidad' },
      { trabajando_id:114, name: 'Paredones' },
      { trabajando_id:115, name: 'Curicó' },
      { trabajando_id:116, name: 'Teno' },
      { trabajando_id:117, name: 'Romeral' },
      { trabajando_id:118, name: 'Molina' },
      { trabajando_id:119, name: 'Sagrada Familia' },
      { trabajando_id:120, name: 'Hualañé' },
      { trabajando_id:121, name: 'Licantén' },
      { trabajando_id:122, name: 'Vichuquén' },
      { trabajando_id:123, name: 'Rauco' },
      { trabajando_id:124, name: 'Talca' },
      { trabajando_id:125, name: 'Pelarco' },
      { trabajando_id:126, name: 'Río Claro' },
      { trabajando_id:127, name: 'San Clemente' },
      { trabajando_id:128, name: 'Maule' },
      { trabajando_id:129, name: 'San Rafael' },
      { trabajando_id:130, name: 'Empedrado' },
      { trabajando_id:131, name: 'Pencahue' },
      { trabajando_id:132, name: 'Constitución' },
      { trabajando_id:133, name: 'Curepto' },
      { trabajando_id:134, name: 'Linares' },
      { trabajando_id:135, name: 'Yerbas Buenas' },
      { trabajando_id:136, name: 'Colbún' },
      { trabajando_id:137, name: 'Longaví' },
      { trabajando_id:138, name: 'Parral' },
      { trabajando_id:139, name: 'Retiro' },
      { trabajando_id:140, name: 'Villa Alegre' },
      { trabajando_id:141, name: 'San Javier' },
      { trabajando_id:142, name: 'Cauquenes' },
      { trabajando_id:143, name: 'Pelluhue' },
      { trabajando_id:144, name: 'Chanco' },
      { trabajando_id:145, name: 'Chillán' },
      { trabajando_id:146, name: 'San Carlos' },
      { trabajando_id:147, name: 'Ñiquén' },
      { trabajando_id:148, name: 'San Fabián' },
      { trabajando_id:149, name: 'Coihueco' },
      { trabajando_id:150, name: 'Pinto' },
      { trabajando_id:151, name: 'San Ignacio' },
      { trabajando_id:152, name: 'El Carmen' },
      { trabajando_id:153, name: 'Yungay' },
      { trabajando_id:154, name: 'Pemuco' },
      { trabajando_id:155, name: 'Bulnes' },
      { trabajando_id:156, name: 'Quillón' },
      { trabajando_id:157, name: 'Ranquil' },
      { trabajando_id:158, name: 'Portezuelo' },
      { trabajando_id:159, name: 'Coelemu' },
      { trabajando_id:160, name: 'Trehuaco' },
      { trabajando_id:161, name: 'Cobquecura' },
      { trabajando_id:162, name: 'Quirihue' },
      { trabajando_id:163, name: 'Ninhue' },
      { trabajando_id:164, name: 'San Nicolás' },
      { trabajando_id:165, name: 'Chillan Viejo' },
      { trabajando_id:166, name: 'Los Angeles' },
      { trabajando_id:167, name: 'Cabrero' },
      { trabajando_id:168, name: 'Tucapel' },
      { trabajando_id:169, name: 'Antuco' },
      { trabajando_id:170, name: 'Quilleco' },
      { trabajando_id:171, name: 'Santa Bárbara' },
      { trabajando_id:172, name: 'Quilaco' },
      { trabajando_id:173, name: 'Mulchén' },
      { trabajando_id:174, name: 'Negrete' },
      { trabajando_id:175, name: 'Nacimiento' },
      { trabajando_id:176, name: 'Laja' },
      { trabajando_id:177, name: 'San Rosendo' },
      { trabajando_id:178, name: 'Yumbel' },
      { trabajando_id:179, name: 'Concepción' },
      { trabajando_id:180, name: 'Talcahuano' },
      { trabajando_id:181, name: 'Penco' },
      { trabajando_id:182, name: 'Tomé' },
      { trabajando_id:183, name: 'Florida' },
      { trabajando_id:184, name: 'Hualqui' },
      { trabajando_id:185, name: 'Santa Juana' },
      { trabajando_id:186, name: 'Lota' },
      { trabajando_id:187, name: 'Coronel' },
      { trabajando_id:188, name: 'San Pedro de la Paz' },
      { trabajando_id:189, name: 'Chiguayante' },
      { trabajando_id:190, name: 'Lebu' },
      { trabajando_id:191, name: 'Arauco' },
      { trabajando_id:192, name: 'Curanilahue' },
      { trabajando_id:193, name: 'Los Alamos' },
      { trabajando_id:194, name: 'Cañete' },
      { trabajando_id:195, name: 'Contulmo' },
      { trabajando_id:196, name: 'Tirua' },
      { trabajando_id:197, name: 'Angol' },
      { trabajando_id:198, name: 'Collipulli' },
      { trabajando_id:199, name: 'Curacautín' },
      { trabajando_id:200, name: 'Ercilla' },
      { trabajando_id:201, name: 'Lonquimay' },
      { trabajando_id:202, name: 'Los Sauces' },
      { trabajando_id:203, name: 'Lumaco' },
      { trabajando_id:204, name: 'Purén' },
      { trabajando_id:205, name: 'Renaico' },
      { trabajando_id:206, name: 'Traiguén' },
      { trabajando_id:207, name: 'Victoria' },
      { trabajando_id:208, name: 'Temuco' },
      { trabajando_id:209, name: 'Carahue' },
      { trabajando_id:210, name: 'Cunco' },
      { trabajando_id:211, name: 'Curarrehue' },
      { trabajando_id:212, name: 'Freire' },
      { trabajando_id:213, name: 'Galvarino' },
      { trabajando_id:214, name: 'Gorbea' },
      { trabajando_id:215, name: 'Lautaro' },
      { trabajando_id:216, name: 'Loncoche' },
      { trabajando_id:217, name: 'Melipeuco' },
      { trabajando_id:218, name: 'Nueva Imperial' },
      { trabajando_id:219, name: 'Padre las Casas' },
      { trabajando_id:220, name: 'Perquenco' },
      { trabajando_id:221, name: 'Pitrufquén' },
      { trabajando_id:222, name: 'Pucón' },
      { trabajando_id:223, name: 'Saavedra' },
      { trabajando_id:224, name: 'Teodoro Schmidt' },
      { trabajando_id:225, name: 'Toltén' },
      { trabajando_id:226, name: 'Vilcún' },
      { trabajando_id:227, name: 'Villarrica' },
      { trabajando_id:228, name: 'Valdivia' },
      { trabajando_id:229, name: 'Corral' },
      { trabajando_id:230, name: 'Futrono' },
      { trabajando_id:231, name: 'La Unión' },
      { trabajando_id:232, name: 'Lago Ranco' },
      { trabajando_id:233, name: 'Lanco' },
      { trabajando_id:234, name: 'Los Lagos' },
      { trabajando_id:235, name: 'Máfil' },
      { trabajando_id:236, name: 'Mariquina' },
      { trabajando_id:237, name: 'Paillaco' },
      { trabajando_id:238, name: 'Panguipulli' },
      { trabajando_id:239, name: 'Río Bueno' },
      { trabajando_id:240, name: 'Osorno' },
      { trabajando_id:241, name: 'Puerto Octay' },
      { trabajando_id:242, name: 'Purranque' },
      { trabajando_id:243, name: 'Puyehue' },
      { trabajando_id:244, name: 'Río Negro' },
      { trabajando_id:245, name: 'San Juan de la Costa' },
      { trabajando_id:246, name: 'San Pablo' },
      { trabajando_id:247, name: 'Calbuco' },
      { trabajando_id:248, name: 'Cochamó' },
      { trabajando_id:249, name: 'Fresia' },
      { trabajando_id:250, name: 'Frutillar' },
      { trabajando_id:251, name: 'Los Muermos' },
      { trabajando_id:252, name: 'Llanquihue' },
      { trabajando_id:253, name: 'Maullín' },
      { trabajando_id:254, name: 'Puerto Montt' },
      { trabajando_id:255, name: 'Puerto Varas' },
      { trabajando_id:256, name: 'Ancud' },
      { trabajando_id:257, name: 'Castro' },
      { trabajando_id:258, name: 'Curaco de Vélez' },
      { trabajando_id:259, name: 'Chonchi' },
      { trabajando_id:260, name: 'Dalcahue' },
      { trabajando_id:261, name: 'Puqueldón' },
      { trabajando_id:262, name: 'Queilén' },
      { trabajando_id:263, name: 'Quellón' },
      { trabajando_id:264, name: 'Quemchi' },
      { trabajando_id:265, name: 'Quinchao' },
      { trabajando_id:267, name: 'Chaitén' },
      { trabajando_id:268, name: 'Futaleufú' },
      { trabajando_id:269, name: 'Hualaihue' },
      { trabajando_id:270, name: 'Palena' },
      { trabajando_id:271, name: 'Cochrane' },
      { trabajando_id:272, name: 'O\'Higgins' },
      { trabajando_id:273, name: 'Tortel' },
      { trabajando_id:274, name: 'Aysén' },
      { trabajando_id:275, name: 'Cisnes' },
      { trabajando_id:276, name: 'Guaitecas' },
      { trabajando_id:277, name: 'Coihaique' },
      { trabajando_id:278, name: 'Lago Verde' },
      { trabajando_id:279, name: 'Chile Chico' },
      { trabajando_id:280, name: 'Río Ibañez' },
      { trabajando_id:281, name: 'Natales' },
      { trabajando_id:282, name: 'Torres del Paine' },
      { trabajando_id:283, name: 'Punta Arenas' },
      { trabajando_id:284, name: 'Río Verde' },
      { trabajando_id:285, name: 'Laguna Blanca' },
      { trabajando_id:286, name: 'San Gregorio' },
      { trabajando_id:287, name: 'Porvenir' },
      { trabajando_id:288, name: 'Primavera' },
      { trabajando_id:289, name: 'Timaukel' },
      { trabajando_id:290, name: 'Cabo de Hornos' },
      { trabajando_id:291, name: 'Antártica' },
      { trabajando_id:292, name: 'Puente Alto' },
      { trabajando_id:293, name: 'Pirque' },
      { trabajando_id:294, name: 'San José de Maipo' },
      { trabajando_id:295, name: 'Melipilla' },
      { trabajando_id:296, name: 'María Pinto' },
      { trabajando_id:297, name: 'Curacaví' },
      { trabajando_id:298, name: 'Alhué' },
      { trabajando_id:299, name: 'San Pedro' },
      { trabajando_id:300, name: 'Talagante' },
      { trabajando_id:301, name: 'El Monte' },
      { trabajando_id:302, name: 'Isla de Maipo' },
      { trabajando_id:303, name: 'Padre Hurtado' },
      { trabajando_id:304, name: 'Peñaflor' },
      { trabajando_id:305, name: 'Buin' },
      { trabajando_id:306, name: 'Calera de Tango' },
      { trabajando_id:307, name: 'Paine' },
      { trabajando_id:308, name: 'San Bernardo' },
      { trabajando_id:309, name: 'Colina' },
      { trabajando_id:310, name: 'Lampa' },
      { trabajando_id:311, name: 'Tiltil' },
      { trabajando_id:312, name: 'Cerrillos' },
      { trabajando_id:313, name: 'Cerro Navia' },
      { trabajando_id:314, name: 'Conchalí' },
      { trabajando_id:315, name: 'El Bosque' },
      { trabajando_id:316, name: 'Estación Central' },
      { trabajando_id:317, name: 'Huechuraba' },
      { trabajando_id:318, name: 'Independencia' },
      { trabajando_id:319, name: 'La Cisterna' },
      { trabajando_id:320, name: 'La Florida' },
      { trabajando_id:321, name: 'La Granja' },
      { trabajando_id:322, name: 'La Pintana' },
      { trabajando_id:323, name: 'La Reina' },
      { trabajando_id:324, name: 'Las Condes' },
      { trabajando_id:325, name: 'Lo Barnechea' },
      { trabajando_id:326, name: 'Lo Espejo' },
      { trabajando_id:327, name: 'Lo Prado' },
      { trabajando_id:328, name: 'Macul' },
      { trabajando_id:329, name: 'Maipú' },
      { trabajando_id:330, name: 'Ñuñoa' },
      { trabajando_id:331, name: 'Pedro Aguirre Cerda' },
      { trabajando_id:332, name: 'Peñalolén' },
      { trabajando_id:333, name: 'Providencia' },
      { trabajando_id:334, name: 'Pudahuel' },
      { trabajando_id:335, name: 'Quilicura' },
      { trabajando_id:336, name: 'Quinta Normal' },
      { trabajando_id:337, name: 'Recoleta' },
      { trabajando_id:338, name: 'Renca' },
      { trabajando_id:339, name: 'San Joaquín' },
      { trabajando_id:340, name: 'San Miguel' },
      { trabajando_id:341, name: 'San Ramón' },
      { trabajando_id:342, name: 'Santiago' },
      { trabajando_id:343, name: 'Vitacura' },
      { trabajando_id:344, name: 'Alto Hospicio' },
      { trabajando_id:1263, name: 'Hualpén' },
      { trabajando_id:1264, name: 'Alto Bío Bío' },
      { trabajando_id:6084, name: 'Chol Chol' }
    ].each do |attrs|
      trabajando_city = Trabajando::City.new(attrs)
      unless trabajando_city.save
        errors << trabajando_city
      end
    end
    puts "Se han cargado #{data.size - errors.size} de #{data.size} ciudades de trabajando"
  end

  def down
    Trabajando::City.destroy_all
    drop_table :trabajando_cities
  end
end
