class ChangeTypeContractStartDateToOfferLetter < ActiveRecord::Migration
  def change
    change_column :offer_letters, :contract_start_date, :datetime
  end
end
