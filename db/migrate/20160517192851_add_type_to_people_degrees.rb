class AddTypeToPeopleDegrees < ActiveRecord::Migration
  def change
    add_column :people_degrees, :type, :string
  end
end
