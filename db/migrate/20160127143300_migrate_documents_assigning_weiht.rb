class MigrateDocumentsAssigningWeiht < ActiveRecord::Migration
  class AssigningWeighting::Report < ActiveRecord::Base
    mount_uploader :document, GenericReportStageUploader

    belongs_to :assigning_weighting_stage,
               class_name: AssigningWeighting::Stage,
               foreign_key: :assigning_weighting_stage_id

    has_many :documents,
           class_name: Stage::Document,
           as: :resource,
           dependent: :destroy

    validates :assigning_weighting_stage, presence: true
  end


  def up
    AssigningWeighting::Report.all.each do |report|
      report.documents.create(document: report.document)
    end
    remove_column :assigning_weighting_reports, :document
  end

  def down
    add_column :assigning_weighting_reports, :document, :string
  end

end
