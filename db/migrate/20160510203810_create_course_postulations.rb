class CreateCoursePostulations < ActiveRecord::Migration
  def change
    create_table :course_postulations do |t|
      t.references :course, index: true
      t.references :postulationable, polymorphic: true, index: { name: :cp_index }
      t.references :applicant, index: true

      t.timestamps null: false
    end
    add_foreign_key :course_postulations, :courses
    add_foreign_key :course_postulations, :applicant_bases, column: :applicant_id

    Trabajando::Postulation.find_each do |trabajando_postulation|
      trabajando_postulation.generate_course_postulation
    end

    Minisite::Postulation.find_each do |minisite_postulation|
      minisite_postulation.generate_course_postulation
    end

    Laborum::Postulation.find_each do |laborum_postualtion|
      laborum_postualtion.generate_course_postulation
    end
  end
end
