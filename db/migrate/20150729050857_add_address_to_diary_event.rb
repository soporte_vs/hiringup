class AddAddressToDiaryEvent < ActiveRecord::Migration
  def change
    add_column :diary_events, :address, :string
  end
end
