module Education
  class StudyType < ActiveRecord::Base
  end
end

class RemoveEducationStudyTypeFromPeopleDegree < ActiveRecord::Migration
  def up
    Education::StudyType.all.each do |education_study_type|
      case education_study_type.name
      when "Doctorado"
        People::Degree.where(education_study_type_id: education_study_type.id).update_all(type: People::PhdDegree)
      when "Enseñanza Media"
        People::Degree.where(education_study_type_id: education_study_type.id).update_all(type: People::HighSchoolDegree)
      when "Liceo Técnico"
        People::Degree.where(education_study_type_id: education_study_type.id).update_all(type: People::TechnicalDegree)
      when "Magister"
        People::Degree.where(education_study_type_id: education_study_type.id).update_all(type: People::MagisterDegree)
      when "Postgrado"
        People::Degree.where(education_study_type_id: education_study_type.id).update_all(type: People::PostgraduateDegree)
      when "Técnico Profesional"
        People::Degree.where(education_study_type_id: education_study_type.id).update_all(type: People::TechnicalProfessionalDegree)
      when "Universitaria"
        People::Degree.where(education_study_type_id: education_study_type.id).update_all(type: People::ProfessionalDegree)
      else
      end
    end

    remove_reference :people_degrees, :education_study_type, index: true
  end

  def down
    add_reference :people_degrees, :education_study_type, index: true
    [
      'People::PrimarySchoolDegree',
      'People::HighSchoolDegree',
      'People::TechnicalDegree',
      'People::TechnicalProfessionalDegree',
      'People::ProfessionalDegree',
      'People::PostgraduateDegree',
      'People::MagisterDegree',
      'People::PhdDegree'
    ].each do |degree_type|
      case degree_type
      when "People::PrimarySchoolDegree"
        education_study_type = Education::StudyType.find_by(name: "Enseñanza Media")
      when "People::HighSchoolDegree"
        education_study_type = Education::StudyType.find_by(name: "Enseñanza Media")
      when "People::TechnicalDegree"
        education_study_type = Education::StudyType.find_by(name: "Liceo Técnico")
      when "People::TechnicalProfessionalDegree"
        education_study_type = Education::StudyType.find_by(name: "Técnico Profesional")
      when "People::ProfessionalDegree"
        education_study_type = Education::StudyType.find_by(name: "Universitaria")
      when "People::PostgraduateDegree"
        education_study_type = Education::StudyType.find_by(name: "Postgrado")
      when "People::MagisterDegree"
        education_study_type = Education::StudyType.find_by(name: "Magister")
      when "People::PhdDegree"
        education_study_type = Education::StudyType.find_by(name: "Doctorado")
      else
      end
      degree_type.constantize.update_all(education_study_type_id: education_study_type.try(:id))
    end
  end
end
