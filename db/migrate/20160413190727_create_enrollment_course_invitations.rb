class CreateEnrollmentCourseInvitations < ActiveRecord::Migration
  def change
    create_table :enrollment_course_invitations do |t|
      t.references :enrollment, index: true
      t.boolean :accepted

      t.timestamps null: false
    end
    add_foreign_key :enrollment_course_invitations, :enrollment_bases, column: :enrollment_id
  end
end
