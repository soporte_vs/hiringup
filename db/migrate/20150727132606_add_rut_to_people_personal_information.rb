class AddRutToPeoplePersonalInformation < ActiveRecord::Migration
  def change
    add_column :people_personal_informations, :rut, :string
  end
end
