class AddEducationLevelToTrabajandoPublication < ActiveRecord::Migration
  def change
    add_reference :trabajando_publications, :education_level, index: true
    add_foreign_key :trabajando_publications, :trabajando_education_levels, column: :education_level_id
  end
end
