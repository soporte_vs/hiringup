class AddWorkDayToTrabajandoPublication < ActiveRecord::Migration
  def change
    add_reference :trabajando_publications, :work_day, index: true
    add_foreign_key :trabajando_publications, :trabajando_work_days, column: :work_day_id
  end
end
