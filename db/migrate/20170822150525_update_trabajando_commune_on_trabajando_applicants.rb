class UpdateTrabajandoCommuneOnTrabajandoApplicants < ActiveRecord::Migration
  def up
    trabajando_applicants = Trabajando::Applicant.where.not(integration_data: nil)
    equal_cities = 0
    trabajando_applicants.find_in_batches do |trabajando_applicant_group|
      trabajando_applicant_group.each do |trabajando_applicant|
        integration_data = trabajando_applicant.integration_data.is_a?(Array) ? trabajando_applicant.integration_data.last : trabajando_applicant.integration_data
        applicant_data = integration_data[:data]['data']
        personal_info = applicant_data['personalInfo']
        commune_data = personal_info['commune']
        territory_city = Trabajando::Applicant.get_city commune_data
        previous_applicant_city = trabajando_applicant.user.personal_information.city
        if territory_city.nil?
          puts "No pudo ser encontrada la ciudad a la cual se debe migrar en el candidato: #{trabajando_applicant.id} #{commune_data}"
        elsif  previous_applicant_city == territory_city
          equal_cities += 1
        elsif trabajando_applicant.user.personal_information.update_attribute(:territory_city_id, territory_city.id)
          trabajando_applicant.index_document
          puts "Se ha migrado la ciudad del candidato #{trabajando_applicant.email} de #{previous_applicant_city.try :name} a #{territory_city.name}"
        else
          puts "Error(es) al migrar: [#{trabajando_applicant.id}]#{trabajando_applicant.email} -> #{trabajando_applicant.user.personal_information.errors.full_messages}"
        end
      end
    end
    puts "#{equal_cities} candidatos poseían la comuna correcta"
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
