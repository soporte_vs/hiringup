class AddFileToPsycholaboralEvaluationReport < ActiveRecord::Migration
  def change
    add_column :psycholaboral_evaluation_reports, :file, :string
    add_column :psycholaboral_evaluation_reports, :notified_to, :string
  end
end
