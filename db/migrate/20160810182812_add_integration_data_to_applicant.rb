class AddIntegrationDataToApplicant < ActiveRecord::Migration
  def change
    add_column :applicant_bases, :integration_data, :text
  end
end
