class OnboardingDocumentGroups < ActiveRecord::Migration
  def up
    onboarding_stages = Onboarding::Stage.all
    onboarding_stages.each do |onboarding_stage|
      if onboarding_stage.onboarding_document_group.nil?
        document_group = onboarding_stage.enrollment.course.document_group
        Onboarding::DocumentGroup.create(onboarding_stage: onboarding_stage, document_group: document_group)
      end
    end
  end

  def down
  end
end
