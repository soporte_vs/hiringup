class AddAddressToPersonalInformation < ActiveRecord::Migration
  def change
    add_column :people_personal_informations, :address, :string
  end
end
