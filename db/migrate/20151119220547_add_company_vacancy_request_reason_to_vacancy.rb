class AddCompanyVacancyRequestReasonToVacancy < ActiveRecord::Migration
  def change
    add_reference :vacancies, :company_vacancy_request_reason, index: true
    add_foreign_key :vacancies, :company_vacancy_request_reasons
  end
end
