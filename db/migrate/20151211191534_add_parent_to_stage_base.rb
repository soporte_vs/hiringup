class AddParentToStageBase < ActiveRecord::Migration
  def change
    add_reference :stage_bases, :parent_stage, index: true
    add_foreign_key :stage_bases, :stage_bases, column: :parent_stage_id
  end
end
