class AddEmailToCompanyReference < ActiveRecord::Migration
  def change
    add_column :company_references, :email, :string
    add_column :company_references, :first_name, :string
    add_column :company_references, :last_name, :string
    add_column :company_references, :position, :string
    add_column :company_references, :observations, :string
    add_column :company_references, :confirmed, :boolean, default:false
  end
end
