class AddHupeSoftwareLevelToTrabajandoSoftwareLevel < ActiveRecord::Migration
  def change
    add_reference :trabajando_software_levels, :hupe_software_level, index: true
    add_foreign_key :trabajando_software_levels, :applicant_software_levels, column: :hupe_software_level_id
  end
end
