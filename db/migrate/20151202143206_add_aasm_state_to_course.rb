class AddAasmStateToCourse < ActiveRecord::Migration
  def change
    add_column :courses, :aasm_state, :string
  end
end
