class CreateStageNotApplyReasons < ActiveRecord::Migration
  def change
    create_table :stage_not_apply_reasons do |t|
      t.string :name
      t.text :description

      t.timestamps null: false
    end
  end
end
