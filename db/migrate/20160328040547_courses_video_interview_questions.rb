class CoursesVideoInterviewQuestions < ActiveRecord::Migration
  def change
    create_table :courses_video_interview_questions, id: false do |t|
      t.belongs_to :question, index: true
      t.belongs_to :course, index: true
    end
  end
end
