class CreateEvaluarCourses < ActiveRecord::Migration
  def change
    create_table :evaluar_courses do |t|
      t.belongs_to :course, index: true
      t.string :agency_id
      t.boolean :confidential, default: true
      t.text :cc
      t.boolean  :notification_at_the_end, default: true
      t.boolean :notification_per_evaluated, default: true
      t.string :environment_type
      t.string :name
      t.string :product_type
      t.datetime :start_date
      t.datetime :end_date
      t.string :status
      t.string :profile_id
      t.timestamps null: false
    end
  end
end
