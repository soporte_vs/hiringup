class CreateDocumentRecords < ActiveRecord::Migration
  def change
    create_table :document_records do |t|
      t.integer :user_id
      t.integer :document_descriptor_id
      t.string :document

      t.timestamps null: false
    end
  end
end
