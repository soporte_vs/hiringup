class AddStudyTipeRefToDegree < ActiveRecord::Migration
  def change
    add_reference :people_degrees, :education_study_type, index: true
    add_foreign_key :people_degrees, :education_study_types
  end
end
