class CreateSchedulingAppointments < ActiveRecord::Migration
  def change
    create_table :scheduling_appointments do |t|
      t.references :scheduling_stage, index: true
      t.references :diary_appointment
      
      t.timestamps null: false
    end
    add_foreign_key :scheduling_appointments, :stage_bases, column: :scheduling_stage_id
    add_foreign_key :scheduling_appointments, :diary_appointments, column: :diary_appointment_id
  end
end
