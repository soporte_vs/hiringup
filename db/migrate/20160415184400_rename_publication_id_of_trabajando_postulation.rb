class RenamePublicationIdOfTrabajandoPostulation < ActiveRecord::Migration
  def change
    # Se hace rename de la coumna publication_id para que se acople a como está en el core
    rename_column :trabajando_postulations, :publication_id, :trabajando_publication_id
  end
end
