class AddRejectReasonToOfferLetter < ActiveRecord::Migration
  def change
    add_reference :offer_letters, :offer_reject_reason, index: true
    add_foreign_key :offer_letters, :offer_reject_reasons
  end
end
