class AddIsAcceptedToMinisitePostulation < ActiveRecord::Migration
  def change
    add_column :minisite_postulations, :is_accepted, :boolean
    add_column :minisite_postulations, :accepted_at, :date
  end
end
