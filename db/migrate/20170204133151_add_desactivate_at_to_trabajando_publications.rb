class AddDesactivateAtToTrabajandoPublications < ActiveRecord::Migration
  def change
    add_column :trabajando_publications, :desactivate_at, :datetime
  end
end
