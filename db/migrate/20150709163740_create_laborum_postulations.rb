class CreateLaborumPostulations < ActiveRecord::Migration
  def change
    create_table :laborum_postulations do |t|
      t.references :laborum_publication, index: true
      t.references :applicant, index: true
      t.integer :user_portal_id, limit: 8
      t.string :answer1
      t.string :answer2
      t.string :answer3
      t.string :answer4
      t.string :answer5
      t.text :information
      t.string :cv

      t.timestamps null: false
    end
    add_foreign_key :laborum_postulations, :laborum_publications
    add_foreign_key :laborum_postulations, :applicant_bases, column: :applicant_id
  end
end
