class AddCompanyPositionBusinessUnitToVacancy < ActiveRecord::Migration
  def change
    add_reference :vacancies, :company_positions_business_unit, index: true
    add_foreign_key :vacancies, :company_positions_business_units
  end
end
