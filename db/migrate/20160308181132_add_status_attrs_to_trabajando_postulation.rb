class AddStatusAttrsToTrabajandoPostulation < ActiveRecord::Migration
  def change
    add_column :trabajando_postulations, :is_accepted, :boolean
    add_column :trabajando_postulations, :accepted_at, :datetime
  end
end
