class AddCompanyEngagementOriginToCourse < ActiveRecord::Migration
  def change
    add_reference :courses, :engagement_origin, index: true
    add_foreign_key :courses, :company_engagement_origins, column: :engagement_origin_id
  end
end
