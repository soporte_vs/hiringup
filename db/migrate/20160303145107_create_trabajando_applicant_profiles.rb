class CreateTrabajandoApplicantProfiles < ActiveRecord::Migration
  def up
    create_table :trabajando_applicant_profiles do |t|
      t.string :name
      t.integer :trabajando_id

      t.timestamps null: false
    end

    errors = []
    data = [
      { trabajando_id:1, name: 'Estudios superiores (Profesional, técnico-profesional, técnico)' },
      { trabajando_id:2, name: 'Estudiante' },
      { trabajando_id:3, name: 'Oficio (no requiere estudios superiores)' },
      { trabajando_id:4, name: 'Indiferente' }
    ].each do |attrs|
      trabajando_applicant_profile = Trabajando::ApplicantProfile.new(attrs)
      unless trabajando_applicant_profile.save
        errors << trabajando_applicant_profile
      end
    end
    puts "Se han cargado #{data.size - errors.size} de #{data.size} perfiles de candidato"
  end


  def down
    Trabajando::ApplicantProfile.destroy_all
    drop_table :trabajando_applicant_profilees
  end
end
