class CreatePeoplePersonalReferrals < ActiveRecord::Migration
  def change
    create_table :people_personal_referrals do |t|
      t.string :full_name
      t.string :email
      t.string :phone
      t.references :personal_information, index: true

      t.timestamps null: false
    end
    add_foreign_key :people_personal_referrals, :people_personal_informations, column: :personal_information_id
  end
end
