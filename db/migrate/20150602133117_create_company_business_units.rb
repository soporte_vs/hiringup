class CreateCompanyBusinessUnits < ActiveRecord::Migration
  def change
    create_table :company_business_units do |t|
      t.string :name
      t.text :description

      t.timestamps null: false
    end
  end
end
