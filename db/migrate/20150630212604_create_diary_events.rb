class CreateDiaryEvents < ActiveRecord::Migration
  def change
    create_table :diary_events do |t|
      t.references :user, index: true
      t.datetime :starts_at
      t.datetime :ends_at
      t.timestamps null: false
    end
    add_foreign_key :diary_events, :users
  end
end
