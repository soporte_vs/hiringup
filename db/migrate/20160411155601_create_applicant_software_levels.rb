class CreateApplicantSoftwareLevels < ActiveRecord::Migration
  def change
    create_table :applicant_software_levels do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
