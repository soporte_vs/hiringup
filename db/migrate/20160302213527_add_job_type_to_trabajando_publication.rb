class AddJobTypeToTrabajandoPublication < ActiveRecord::Migration
  def change
    add_reference :trabajando_publications, :job_type, index: true
    add_foreign_key :trabajando_publications, :trabajando_job_types, column: :job_type_id
  end
end
