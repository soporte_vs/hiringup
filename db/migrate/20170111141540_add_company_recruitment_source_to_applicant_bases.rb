class AddCompanyRecruitmentSourceToApplicantBases < ActiveRecord::Migration
  def change
    add_reference :applicant_bases, :company_recruitment_source, index: true
    add_foreign_key :applicant_bases, :company_recruitment_sources, column: :company_recruitment_source_id
  end
end
