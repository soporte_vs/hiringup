class AddDocumentGroupIdToCourse < ActiveRecord::Migration
  def change
    add_column :courses, :document_group_id, :integer
  end
end
