class AddCurrentEmployeePositionsCencoToCompanyEmployee < ActiveRecord::Migration
  def change
    add_reference :company_employees, :current_employee_position_cenco, index: true
    add_foreign_key :company_employees, :company_employee_positions_cencos, column: :current_employee_position_cenco_id
    add_column :company_employees, :is_boss, :boolean
  end
end
