class CreateEngagementAgreements < ActiveRecord::Migration
  def change
    create_table :engagement_agreements do |t|
      t.string :revenue
      t.text :observations
      t.references :engagement_stage, index: true

      t.timestamps
    end
  end
end
