# coding: utf-8
module Trabajando
  class WorkDay < ActiveRecord::Base
    belongs_to :hupe_timetable, class: Company::Timetable
  end
end

class CompanyTimetableTrabajandoWorkday < ActiveRecord::Base
  belongs_to :company_timetable, class_name: Company::Timetable
  belongs_to :trabajando_work_day, class_name: Trabajando::WorkDay
end

class CreateCompanyTimetableTrabajandoWorkdays < ActiveRecord::Migration
  def up
    create_table :company_timetable_trabajando_workdays do |t|
      t.references :company_timetable, index: { name: :timetable }
      t.references :trabajando_work_day, index: { name: :work_day }

      t.timestamps null: false
    end
    add_foreign_key :company_timetable_trabajando_workdays, :company_timetables
    add_foreign_key :company_timetable_trabajando_workdays, :trabajando_work_days

    Trabajando::WorkDay.all.each do |workday|
      if workday.hupe_timetable.present?
        Company::TimetableTrabajandoWorkday.where(
          trabajando_work_day: workday,
          company_timetable: workday.hupe_timetable
        ).first_or_create
      end

      # casos particulares
      if workday.name == "Media Jornada"
        hupe_media_jornada = Company::Timetable.find_by(name: "Media Jornada")
        Company::TimetableTrabajandoWorkday.where(
          trabajando_work_day: workday,
          company_timetable: hupe_media_jornada
        ).first_or_create

        hupe_extension_horaria = Company::Timetable.find_by(name: "Extensión Horaria")
        Company::TimetableTrabajandoWorkday.where(
          trabajando_work_day: workday,
          company_timetable: hupe_extension_horaria
        ).first_or_create
      end

      if workday.name == "Part Time"
        Company::TimetableTrabajandoWorkday.where(
          trabajando_work_day: workday,
          company_timetable: workday.hupe_timetable
        ).destroy_all
      end
    end

    remove_column :trabajando_work_days, :hupe_timetable_id
  end

  def down
    add_reference :trabajando_work_days, :hupe_timetable, index: true
    add_foreign_key :trabajando_work_days, :company_timetables, column: :hupe_timetable_id

    Company::TimetableTrabajandoWorkday.all.each do |c|
      timetable_id = c.company_timetable_id
      workday = Trabajando::WorkDay.find(c.trabajando_work_day_id)
      if Company::Timetable.find(timetable_id).present?
        workday.hupe_timetable_id = timetable_id
        workday.save
      end

      # casos particulares
      hupe_media_jornada = Company::Timetable.where(name: "Media Jornada").first_or_create
      part_time_workday = Trabajando::WorkDay.where(name: "Part Time").first_or_create
      part_time_workday.hupe_timetable_id = hupe_media_jornada.id
      part_time_workday.save

      hupe_extension_horaria = Company::Timetable.where(name: "Extensión Horaria").first_or_create
      media_jornada_workday = Trabajando::WorkDay.where(name: "Media Jornada").first_or_create
      media_jornada_workday.hupe_timetable_id = hupe_extension_horaria.id
      media_jornada_workday.save
    end

    drop_table :company_timetable_trabajando_workdays
  end
end
