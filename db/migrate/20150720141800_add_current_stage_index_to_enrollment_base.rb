class AddCurrentStageIndexToEnrollmentBase < ActiveRecord::Migration
  def change
    add_column :enrollment_bases, :current_stage_index, :integer
  end
end
