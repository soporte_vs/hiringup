class CreateCalendarEventResourceables < ActiveRecord::Migration
  def change
    create_table :calendar_event_resourceables do |t|
      t.references :resourceable, polymorphic: true, index: { name: :event_resourceable_index }
      t.references :event, index: true
      t.string :status

      t.timestamps null: false
    end
    add_foreign_key :calendar_event_resourceables, :calendar_events, column: :event_id
  end
end
