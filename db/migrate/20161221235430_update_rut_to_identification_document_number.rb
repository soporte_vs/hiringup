# coding: utf-8
# Esta migración sólo toma todos los rut y los pone en la nueva columna
# Si no existe la columna rut no hace nada
# Si existe, migra los ruts, y elimina la columna
# Al deshacer, sólo pone los rut en la columna rut, pero no elimina la columna nueva porque eso lo hace otra migración
class UpdateRutToIdentificationDocumentNumber < ActiveRecord::Migration
  def up
    if column_exists?(:people_personal_informations, :rut)
      chile = Territory::Country.where(name: "Chile").first_or_create
      if chile.present?
        chile_rut = People::IdentificationDocumentType.where(
          country: chile,
          name: "RUT",
          validation_regex: "^([0-9]+-[0-9Kk])$"
        ).first_or_create
      end

      People::PersonalInformation.find_each do |pi|
        if pi.rut.present?
          pi.identification_document_number = pi.rut
          pi.identification_document_type = chile_rut
          pi.save(validate: false)
        end
      end
      remove_column :people_personal_informations, :rut
      People::PersonalInformation.reset_column_information
    end
  end

  def down
    unless column_exists?(:people_personal_informations, :rut)
      add_column :people_personal_informations, :rut, :string
      People::PersonalInformation.find_each do |pi|
        if pi.identification_document_type.present? && pi.identification_document_type.codename == "rut"
          pi.rut = pi.identification_document_number
          pi.save(validate: false)
        end
      end
    end
  end
end
