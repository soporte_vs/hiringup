class CreateTrabajandoPostulations < ActiveRecord::Migration
  def change
    create_table :trabajando_postulations do |t|
      t.references :publication, index: true
      t.references :applicant, index: true
      t.datetime :postulation_date

      t.timestamps null: false
    end
    add_foreign_key :trabajando_postulations, :trabajando_publications, column: :publication_id
    add_foreign_key :trabajando_postulations, :applicant_bases, column: :applicant_id
  end
end
