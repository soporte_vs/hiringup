class AddAvatarToPeoplePersonalInformation < ActiveRecord::Migration
  def change
    add_column :people_personal_informations, :avatar, :string
  end
end
