class AddCallbackUrlToEvaluarCourse < ActiveRecord::Migration
  def change
    add_column :evaluar_courses, :callback_url, :string
  end
end
