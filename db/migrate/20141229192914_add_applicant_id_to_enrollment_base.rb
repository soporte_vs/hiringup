class AddApplicantIdToEnrollmentBase < ActiveRecord::Migration
  def change
    add_column :enrollment_bases, :applicant_id, :integer
    add_index :enrollment_bases, :applicant_id
  end
end
