class CreatePeopleProfessionalInformations < ActiveRecord::Migration
  def change
    create_table :people_professional_informations do |t|
      t.references :user, index: true

      t.timestamps null: false
    end
    add_foreign_key :people_professional_informations, :users
  end
end
