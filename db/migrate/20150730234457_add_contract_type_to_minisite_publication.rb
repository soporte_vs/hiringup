class AddContractTypeToMinisitePublication < ActiveRecord::Migration
  def change
    add_reference :minisite_publications, :company_contract_type, index: true
    add_foreign_key :minisite_publications, :company_contract_types
  end
end
