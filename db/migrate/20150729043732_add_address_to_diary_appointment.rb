class AddAddressToDiaryAppointment < ActiveRecord::Migration
  def change
    add_column :diary_appointments, :address, :string
  end
end
