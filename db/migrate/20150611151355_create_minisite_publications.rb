class CreateMinisitePublications < ActiveRecord::Migration
  def change
    create_table :minisite_publications do |t|
      t.references :course, index: true
      t.text :description
      t.string :title

      t.timestamps null: false
    end
    add_foreign_key :minisite_publications, :courses
  end
end
