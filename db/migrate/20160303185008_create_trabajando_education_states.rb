class CreateTrabajandoEducationStates < ActiveRecord::Migration
  def change
    create_table :trabajando_education_states do |t|
      t.string :name
      t.integer :trabajando_id

      t.timestamps null: false
    end

    errors = []
    data = [
      { trabajando_id:0, name: 'Incompleto' },
      { trabajando_id:1, name: 'Completo' },
      { trabajando_id:2, name: 'Estudiando' },
      { trabajando_id:3, name: 'Licenciado' },
      { trabajando_id:4, name: 'Egresado' },
      { trabajando_id:5, name: 'Titulado' }
    ].each do |attrs|
      trabajando_education_state = Trabajando::EducationState.new(attrs)
      unless trabajando_education_state.save
        errors << trabajando_education_state
      end
    end
    puts "Se han cargado #{data.size - errors.size} de #{data.size} estado de estudios"
  end


  def down
    Trabajando::EducationState.destroy_all
    drop_table :trabajando_education_states
  end
end
