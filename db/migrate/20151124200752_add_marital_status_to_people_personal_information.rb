class AddMaritalStatusToPeoplePersonalInformation < ActiveRecord::Migration
  def change
    add_reference :people_personal_informations, :company_marital_status, index: true
    add_foreign_key :people_personal_informations, :company_marital_statuses
  end
end
