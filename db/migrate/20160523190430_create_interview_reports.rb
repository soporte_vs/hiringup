class CreateInterviewReports < ActiveRecord::Migration
  def change
    create_table :interview_reports do |t|
      t.text :observations
      t.string :document
      t.references :interview_stage, index: true

      t.timestamps null: false
    end
    add_foreign_key :interview_reports, :stage_bases, column: :interview_stage_id
  end
end
