class RemoveCompanyBusinessUnitToCompanyCenco < ActiveRecord::Migration
  def up
    company = Company::Record.create(name: $COMPANY[:name])
    company.company_business_units << Company::BusinessUnit.all

    add_reference :company_cencos, :company_management, index: true
    add_foreign_key :company_cencos, :company_managements
    Company::Cenco.all.each do |cenco|
      managements = Company::Management.where(company_business_unit_id: cenco.company_business_unit_id)
      cenco.update(company_management: managements.first)
      if managements.count > 1
        puts "EXISTE UNA UNIDAD DE NEGOCIO ASOCIADA A MAS DE UNA EMPRESA #{cenco.company_business_unit_id}"
      end
    end
    remove_foreign_key :company_cencos, :company_business_units
    remove_reference :company_cencos, :company_business_unit, index: true
  end

  def down
    remove_foreign_key :company_cencos, :company_managements
    remove_reference :company_cencos, :company_management, index: true
    company = Company::Record.first.destroy
    company.destroy
  end
end
