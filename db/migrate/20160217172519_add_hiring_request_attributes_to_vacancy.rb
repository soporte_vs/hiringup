class AddHiringRequestAttributesToVacancy < ActiveRecord::Migration
  def change
    add_reference :vacancies, :created_by, references: :users
    add_foreign_key :vacancies, :users, column: :created_by_id
    add_column :vacancies, :required_by_email, :string
    add_column :vacancies, :required_by_name, :string
  end
end
