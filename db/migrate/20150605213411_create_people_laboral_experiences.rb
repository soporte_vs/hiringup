class CreatePeopleLaboralExperiences < ActiveRecord::Migration
  def change
    create_table :people_laboral_experiences do |t|
      t.string :position
      t.string :company
      t.date :since_date
      t.date :until_date
      t.references :professional_information, index: true

      t.timestamps null: false
    end
    add_foreign_key :people_laboral_experiences, :people_professional_informations, column: :professional_information_id
  end
end
