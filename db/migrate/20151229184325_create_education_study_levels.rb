class CreateEducationStudyLevels < ActiveRecord::Migration
  def change
    create_table :education_study_levels do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
