class AddObservationsClosingToCourses < ActiveRecord::Migration
  def change
    add_column :courses, :observations_closing, :string
  end
end
