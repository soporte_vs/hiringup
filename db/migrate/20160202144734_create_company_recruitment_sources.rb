class CreateCompanyRecruitmentSources < ActiveRecord::Migration
  def change
    create_table :company_recruitment_sources do |t|
      t.string :name
      t.text :description

      t.timestamps null: false
    end
  end
end
