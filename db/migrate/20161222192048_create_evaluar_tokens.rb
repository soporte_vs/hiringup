class CreateEvaluarTokens < ActiveRecord::Migration
  def change
    create_table :evaluar_tokens do |t|
      t.string :access_token
      t.integer :expires_in
      t.integer :refresh_expires_in
      t.string :refresh_token
      t.string :token_type
      t.string :id_token
      t.integer :not_before_policy
      t.string :session_state

      t.timestamps null: false
    end
  end
end
