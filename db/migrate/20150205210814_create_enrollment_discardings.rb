class CreateEnrollmentDiscardings < ActiveRecord::Migration
  def change
    create_table :enrollment_discardings do |t|
      t.references :enrollment, index: true
      t.references :discard_reason, index: true
      t.text :observations

      t.timestamps
    end
  end
end
