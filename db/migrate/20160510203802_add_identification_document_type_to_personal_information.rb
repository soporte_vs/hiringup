class AddIdentificationDocumentTypeToPersonalInformation < ActiveRecord::Migration
  def change
    add_column :people_personal_informations, :identification_document_number, :string
    add_reference :people_personal_informations, :identification_document_type
    add_foreign_key :people_personal_informations, :people_identification_document_types, column: :identification_document_type_id
  end
end
