class AddBossToCompanyEmployee < ActiveRecord::Migration
  def change
  	add_column :company_employees, :boss_id, :integer
    add_foreign_key :company_employees, :company_employees, column: :boss_id
  end
end
