class AddEducationStateToTrabajandoPublication < ActiveRecord::Migration
  def change
    add_reference :trabajando_publications, :education_state, index: true
    add_foreign_key :trabajando_publications, :trabajando_education_states, column: :education_state_id
  end
end
