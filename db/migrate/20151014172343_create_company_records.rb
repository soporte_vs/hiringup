class CreateCompanyRecords < ActiveRecord::Migration
  def change
    create_table :company_records do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
