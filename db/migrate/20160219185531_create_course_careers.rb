class CreateCourseCareers < ActiveRecord::Migration
  def change
    create_table :course_careers do |t|
      t.references :course, index: true
      t.references :education_career, index: true

      t.timestamps null: false
    end
    add_foreign_key :course_careers, :courses
    add_foreign_key :course_careers, :education_careers
  end
end
