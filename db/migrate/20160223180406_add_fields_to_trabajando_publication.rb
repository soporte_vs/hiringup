class AddFieldsToTrabajandoPublication < ActiveRecord::Migration
  def change
    add_column :trabajando_publications, :contract_time, :string
    add_column :trabajando_publications, :show_salary, :boolean, default: true
    add_column :trabajando_publications, :salary_comment, :string
    add_column :trabajando_publications, :work_time, :string
    add_column :trabajando_publications, :workplace, :string
    add_column :trabajando_publications, :minimum_requirements, :text
    add_column :trabajando_publications, :work_experience, :integer
    add_column :trabajando_publications, :own_transport, :boolean, default: false
    add_column :trabajando_publications, :trabajando_id, :integer
    add_column :trabajando_publications, :aasm_state, :string
  end
end
