class CreateTrabajandoMaritalStatuses < ActiveRecord::Migration
  def up
    create_table :trabajando_marital_statuses do |t|
      t.string :name
      t.integer :trabajando_id
      t.references :company_marital_status, index: true

      t.timestamps null: false
    end
    add_foreign_key :trabajando_marital_statuses, :company_marital_statuses

    errors = []
    data = [
      { trabajando_id: 0, name: 'Soltero(a)'},
      { trabajando_id: 1, name: 'Casado(a)'},
      { trabajando_id: 2, name: 'Viudo(a)'},
      { trabajando_id: 3, name: 'Separado(a)'},
      { trabajando_id: 4, name: 'Convive'},
      { trabajando_id: 5, name: 'Divorciado(a)'}
    ].each do |attrs|
      trabajando_marital_status = Trabajando::MaritalStatus.new(attrs)
      unless trabajando_marital_status.save
        errors << trabajando_marital_status
      end
    end
    puts "Se han cargado #{data.size - errors.size} de #{data.size} estado civil"
  end


  def down
    Trabajando::MaritalStatus.destroy_all
    drop_table :trabajando_marital_statuses
  end
end
