class AddTrbajandoFieldsToCourse < ActiveRecord::Migration
  def change
    add_reference :courses, :applicant_software_level, index: true
    add_foreign_key :courses, :applicant_software_levels
    add_reference :courses, :applicant_profile_type, index: true
    add_foreign_key :courses, :applicant_profile_types
    add_column :courses, :minimun_requirements, :text
  end
end
