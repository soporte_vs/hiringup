class CreateTrabajandoEducationLevels < ActiveRecord::Migration
  def change
    create_table :trabajando_education_levels do |t|
      t.string :name
      t.integer :trabajando_id

      t.timestamps null: false
    end

    errors = []
    data = [
      { trabajando_id:148, name: 'Doctorado' },
      { trabajando_id:2, name: 'Media' },
      { trabajando_id:3, name: 'Técnico medio/ colegio técnico' },
      { trabajando_id:147, name: 'Magíster' },
      { trabajando_id:6, name: 'Postgrado' },
      { trabajando_id:4, name: 'Técnico profesional superior' },
      { trabajando_id:5, name: 'Universitaria' },
      { trabajando_id:1, name: 'Básica' }
    ].each do |attrs|
      trabajando_education_level = Trabajando::EducationLevel.new(attrs)
      unless trabajando_education_level.save
        errors << trabajando_education_level
      end
    end
    puts "Se han cargado #{data.size - errors.size} de #{data.size} niveles de educación"
  end


  def down
    Trabajando::EducationLevel.destroy_all
    drop_table :trabajando_education_leveles
  end
end
