class CreateHiringRequestRecords < ActiveRecord::Migration
  def change
    create_table :hiring_request_records do |t|
      t.references :company_positions_cenco, index: true
      t.date :request_date
      t.references :applicant, index: true
      t.float :revenue_expected
      t.string :workplace
      t.text :request_reason
      t.references :company_contract_type, index: true
      t.references :company_estate, index: true
      t.references :company_timetable, index: true
      t.date :entry_date

      t.timestamps null: false
    end
    add_foreign_key :hiring_request_records, :company_positions_cencos
    add_foreign_key :hiring_request_records, :applicant_bases, column: :applicant_id
    add_foreign_key :hiring_request_records, :company_contract_types
    add_foreign_key :hiring_request_records, :company_estates
    add_foreign_key :hiring_request_records, :company_timetables
  end
end
