class RemoveSchedulingWorkFlowInCourse < ActiveRecord::Migration
  def up
  	Course.all.each do |course|
  		array_work_flow = []
  		course.work_flow.each do |workflow|
  			array_work_flow << workflow unless workflow[:class_name].eql?("Scheduling::Stage")
  		end
  		course.update(work_flow: array_work_flow)
  	end
  end

  def down
    fail ActiveRecord::IrreversibleMigration
  end
end
