class CreateManagerInterviewReports < ActiveRecord::Migration
  def change
    create_table :manager_interview_reports do |t|
      t.text :observations
      t.string :document
      t.references :manager_interview_stage, index: true

      t.timestamps null: false
    end
    add_foreign_key :manager_interview_reports, :stage_bases, column: :manager_interview_stage_id
  end
end
