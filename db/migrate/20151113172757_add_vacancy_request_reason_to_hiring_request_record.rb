class AddVacancyRequestReasonToHiringRequestRecord < ActiveRecord::Migration
  def change
    rename_column :hiring_request_records, :request_reason, :observations

    add_reference :hiring_request_records, :company_vacancy_request_reason, index: { name: :hr_cp_vacancy_request_reason }
    add_foreign_key :hiring_request_records, :company_vacancy_request_reasons
  end
end
