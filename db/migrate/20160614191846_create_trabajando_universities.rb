# coding: utf-8
class CreateTrabajandoUniversities < ActiveRecord::Migration
  def change
    create_table :trabajando_universities do |t|
      t.string :name
      t.integer :trabajando_id
      t.references :hupe_university, index: true

      t.timestamps null: false
    end
    add_foreign_key :trabajando_universities, :education_universities, column: :hupe_university_id
  end
end
