class CreateVideoInterviewAnswers < ActiveRecord::Migration
  def change
    create_table :video_interview_answers do |t|
      t.integer :time_limit
      t.string :question
      t.integer :enrollment_id

      t.timestamps null: false
    end
  end
end
