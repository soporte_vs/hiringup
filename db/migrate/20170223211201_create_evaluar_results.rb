class CreateEvaluarResults < ActiveRecord::Migration
  def change
    create_table :evaluar_results do |t|
      t.text :data
      t.string :process_id
      t.string :identification
      t.references :applicant, index: true
      t.references :evaluar_course, index: true

      t.timestamps null: false
    end
    add_foreign_key :evaluar_results, :applicant_bases, column: :applicant_id
    add_foreign_key :evaluar_results, :evaluar_courses
  end
end
