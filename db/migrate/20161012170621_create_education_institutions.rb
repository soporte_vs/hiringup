class CreateEducationInstitutions < ActiveRecord::Migration
  def up
    rename_table :education_universities, :education_institutions
    add_column :education_institutions, :type, :string
    add_column :education_institutions, :custom, :boolean, default: false # true when created by user, not in list
    add_reference :education_institutions, :country, index: true
    add_foreign_key :education_institutions, :territory_countries, column: :country_id

    # at this moment, every record in this table is a University,
    # so, lets update the type for each one
    Education::Institution.update_all(type: "Education::University")

    rename_column :people_degrees, :university_id, :institution_id # this updates the index too
  end

  def down
    # destroy all and reseed?

    rename_column :people_degrees, :institution_id, :university_id

    remove_foreign_key :education_institutions, column: :country_id
    remove_reference :education_institutions, :country, index: true
    remove_column :education_institutions, :custom, :boolean
    remove_column :education_institutions, :type, :string
    rename_table :education_institutions, :education_universities
  end
end
