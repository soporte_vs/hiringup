class CreateTrabajandoSoftwareLevels < ActiveRecord::Migration
  def change
    create_table :trabajando_software_levels do |t|
      t.string :name
      t.integer :trabajando_id

      t.timestamps null: false
    end
  end


  def down
    Trabajando::SoftwareLevel.destroy_all
    drop_table :trabajando_software_levels
  end
end
