class CreateApplicantReports < ActiveRecord::Migration
  def change
    create_table :applicant_reports do |t|
      t.references :user, index: true
      t.text :params
      t.string :report_file

      t.timestamps null: false
    end
    add_foreign_key :applicant_reports, :users
  end
end
