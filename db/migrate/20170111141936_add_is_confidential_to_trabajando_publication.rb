class AddIsConfidentialToTrabajandoPublication < ActiveRecord::Migration
  def change
    add_column :trabajando_publications, :is_confidential, :boolean, default: false
  end
end
