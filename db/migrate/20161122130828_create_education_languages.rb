class CreateEducationLanguages < ActiveRecord::Migration
  def change
    create_table :education_languages do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
