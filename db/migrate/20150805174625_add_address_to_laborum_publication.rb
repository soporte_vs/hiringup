class AddAddressToLaborumPublication < ActiveRecord::Migration
  def change
    add_column :laborum_publications, :address, :string
  end
end
