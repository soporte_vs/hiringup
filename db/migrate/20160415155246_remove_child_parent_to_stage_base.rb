class RemoveChildParentToStageBase < ActiveRecord::Migration
  def up
    remove_foreign_key :stage_bases, column: :parent_stage_id
    remove_reference :stage_bases, :parent_stage, index: true
    remove_column :stage_bases, :child_types, :text
  end

  def down
    add_reference :stage_bases, :parent_stage, index: true
    add_foreign_key :stage_bases, :stage_bases, column: :parent_stage_id
    add_column :stage_bases, :child_types, :text
  end
end
