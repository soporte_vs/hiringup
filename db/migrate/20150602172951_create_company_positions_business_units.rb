class CreateCompanyPositionsBusinessUnits < ActiveRecord::Migration
  def change
    create_table :company_positions_business_units do |t|
      t.references :company_position, index: true
      t.references :company_business_unit, index: { name: :cp_business_units}

      t.timestamps null: false
    end
    add_foreign_key :company_positions_business_units, :company_positions
    add_foreign_key :company_positions_business_units, :company_business_units
  end
end
