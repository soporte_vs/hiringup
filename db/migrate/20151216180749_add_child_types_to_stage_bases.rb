class AddChildTypesToStageBases < ActiveRecord::Migration
  def change
    add_column :stage_bases, :child_types, :text
  end
end
