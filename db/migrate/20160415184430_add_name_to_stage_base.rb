module Scheduling
  class Stage < Stage::Base
    self.table_name = 'stage_bases'
  end
end

module Enrollment
  class Base < ActiveRecord::Base
    def index_document
    end

    def delete_document
    end
  end
end

class AddNameToStageBase < ActiveRecord::Migration
  class Stage::Base < ActiveRecord::Base
    def to_s
      if self.step.present?
        "#{enrollment.course.work_flow[self.step][:name]}"
      end
    end
  end

  def change
    add_column :stage_bases, :name, :string

    Stage::Base.find_each do |stage|
      stage.update(name: stage.to_s)
    end
  end
end
