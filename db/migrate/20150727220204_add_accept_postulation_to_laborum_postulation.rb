class AddAcceptPostulationToLaborumPostulation < ActiveRecord::Migration
  def change
    add_column :laborum_postulations, :is_accepted, :boolean
    add_column :laborum_postulations, :accepted_at, :date
  end
end
