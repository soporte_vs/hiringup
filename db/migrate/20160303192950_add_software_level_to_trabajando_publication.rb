class AddSoftwareLevelToTrabajandoPublication < ActiveRecord::Migration
  def change
    add_reference :trabajando_publications, :software_level, index: true
    add_foreign_key :trabajando_publications, :trabajando_software_levels, column: :software_level_id
  end
end
