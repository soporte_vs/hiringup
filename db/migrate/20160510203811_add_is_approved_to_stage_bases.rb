class AddIsApprovedToStageBases < ActiveRecord::Migration
  def change
    add_column :stage_bases, :is_approved, :boolean
  end
end
