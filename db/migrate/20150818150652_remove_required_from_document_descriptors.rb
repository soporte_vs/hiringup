class RemoveRequiredFromDocumentDescriptors < ActiveRecord::Migration
  def change
    remove_column :document_descriptors, :required, :boolean
  end
end
