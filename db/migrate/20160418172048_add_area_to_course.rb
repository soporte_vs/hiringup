class AddAreaToCourse < ActiveRecord::Migration
  def change
    add_reference :courses, :area, index: true
    add_foreign_key :courses, :company_areas, column: :area_id
  end
end
