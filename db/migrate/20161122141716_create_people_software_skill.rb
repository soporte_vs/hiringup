class CreatePeopleSoftwareSkill < ActiveRecord::Migration
  def change
    create_table :people_software_skills do |t|
      t.string :level
      t.references :professional_information, index: true
      t.references :education_software, index: true

      t.timestamps null: false
    end
    add_foreign_key :people_software_skills, :education_softwares, column: :education_software_id
    add_foreign_key :people_software_skills, :people_professional_informations, column: :professional_information_id
  end
end
