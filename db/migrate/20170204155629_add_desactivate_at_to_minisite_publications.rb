class AddDesactivateAtToMinisitePublications < ActiveRecord::Migration
  def change
    add_column :minisite_publications, :desactivate_at, :datetime
  end
end
