class AddFileToFinalInterviewReport < ActiveRecord::Migration
  def change
    add_column :final_interview_reports, :file, :string
    add_column :final_interview_reports, :notified_to, :string
  end
end
