class CreateVacancies < ActiveRecord::Migration
  def change
    create_table :vacancies do |t|
      t.references :course, index: true
      t.datetime :closed_at
      t.text :observations
      
      t.timestamps null: false
    end
    add_foreign_key :vacancies, :courses
  end
end
