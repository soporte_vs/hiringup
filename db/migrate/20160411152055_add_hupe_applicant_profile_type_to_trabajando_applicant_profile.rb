class AddHupeApplicantProfileTypeToTrabajandoApplicantProfile < ActiveRecord::Migration
  def change
    add_reference :trabajando_applicant_profiles, :hupe_profile_type, index: true
    add_foreign_key :trabajando_applicant_profiles, :applicant_profile_types, column: :hupe_profile_type_id
  end
end
