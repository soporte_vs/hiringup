class AddWillAttendToEventResourceable < ActiveRecord::Migration
  def change
    add_column :calendar_event_resourceables, :will_attend, :boolean
  end
end
