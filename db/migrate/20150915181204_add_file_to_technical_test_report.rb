class AddFileToTechnicalTestReport < ActiveRecord::Migration
  def change
    add_column :technical_test_reports, :file, :string
    add_column :technical_test_reports, :notified_to, :string
  end
end
