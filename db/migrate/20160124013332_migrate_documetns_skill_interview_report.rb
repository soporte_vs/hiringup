class MigrateDocumetnsSkillInterviewReport < ActiveRecord::Migration

  class SkillInterview::Report < ActiveRecord::Base
    mount_uploader :document, GenericReportStageUploader
    belongs_to :skill_interview_stage,
               class_name: SkillInterview::Stage,
               foreign_key: :skill_interview_stage_id

  has_many :documents,
            class_name: Stage::Document,
            as: :resource,
            dependent: :destroy

    validates :observations, presence: true
    validates :notified_to, presence: true, email: true
    validates :skill_interview_stage, presence: true
  end

  def up
    SkillInterview::Report.all.each do |report|
      report.documents.create(document: report.document)
    end
    remove_column :skill_interview_reports, :document
  end

  def down
    add_column :skill_interview_reports, :document, :string
  end

end
