class AddIsApprovedToInterviewReport < ActiveRecord::Migration
  def change
    add_column :interview_reports, :is_approved, :boolean
  end
end
