class AddStepToStageBases < ActiveRecord::Migration
  def change
    add_column :stage_bases, :step, :integer
  end
end
