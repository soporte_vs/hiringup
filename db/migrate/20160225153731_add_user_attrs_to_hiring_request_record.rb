class AddUserAttrsToHiringRequestRecord < ActiveRecord::Migration
 def change
   add_reference :hiring_request_records, :created_by, references: :users
   add_foreign_key :hiring_request_records, :users, column: :created_by_id
   add_column :hiring_request_records, :required_by_name, :string
   add_column :hiring_request_records, :required_by_email, :string
 end
end
