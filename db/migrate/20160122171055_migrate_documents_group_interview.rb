class MigrateDocumentsGroupInterview < ActiveRecord::Migration

  class GroupInterview::Report < ActiveRecord::Base
    mount_uploader :file, GenericReportStageUploader
    belongs_to :group_interview_stage,
             class_name: GroupInterview::Stage,
             foreign_key: :group_interview_stage_id

    has_many :documents,
            class_name: Stage::Document,
            as: :resource,
            dependent: :destroy

    validates :observations, presence: true
    validates :notified_to, presence: true, email: true
    validates :group_interview_stage, presence: true
  end

  def up
    GroupInterview::Report.all.each do |report|
      report.documents.create(document: report.file)
    end
    remove_column :group_interview_reports, :file
  end

  def down
    add_column :group_interview_reports, :file, :string
  end
end
