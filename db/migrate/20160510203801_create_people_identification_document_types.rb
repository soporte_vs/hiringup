class CreatePeopleIdentificationDocumentTypes < ActiveRecord::Migration
  def change
    create_table :people_identification_document_types do |t|
      t.string :name
      t.string :codename
      t.references :country, index: true
      t.text :validation_regex
      t.boolean :validate_uniqueness, default: true

      t.timestamps null: false
    end
    add_foreign_key :people_identification_document_types, :territory_countries, column: :country_id
  end
end
