class RemoveCareerToCourse < ActiveRecord::Migration
  def change
    Course.where.not(career_id: nil).each do |c|
      c.careers << Education::Career.find(c.career_id)
    end
    remove_column :courses, :career_id, :integer
  end
end
