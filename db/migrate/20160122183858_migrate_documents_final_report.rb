class MigrateDocumentsFinalReport < ActiveRecord::Migration

  class FinalInterview::Report < ActiveRecord::Base
    mount_uploader :file, GenericReportStageUploader
    belongs_to :final_interview_stage,
               class_name: FinalInterview::Stage,
               foreign_key: :final_interview_stage_id

    has_many :documents,
            class_name: Stage::Document,
            as: :resource,
            dependent: :destroy

    validates :observations, presence: true
    validates :notified_to, presence: true, email: true
    validates :final_interview_stage, presence: true
  end

  def up
    FinalInterview::Report.all.each do |report|
      report.documents.create(document: report.file)
    end
    remove_column :final_interview_reports, :file
  end

  def down
    add_column :final_interview_reports, :file, :string
  end
end
