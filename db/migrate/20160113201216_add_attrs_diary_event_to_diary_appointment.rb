module Diary
  class Event < ActiveRecord::Base
    self.table_name = 'diary_events'
  end

  class Appointment < ActiveRecord::Base
    self.table_name = 'diary_appointments'
  end
end

class AddAttrsDiaryEventToDiaryAppointment < ActiveRecord::Migration


  def up
    add_reference :diary_appointments, :created_by, index: true
    add_foreign_key :diary_appointments, :users, column: :created_by_id

    add_reference :diary_appointments, :manager, index: true
    add_foreign_key :diary_appointments, :users, column: :manager_id

    Diary::Appointment.all.each do |diary_appointment|
      diary_event = Diary::Event.find(diary_appointment[:diary_event_id])
      diary_appointment.update(created_by_id: diary_event[:created_by_id], manager_id: diary_event[:user_id])
    end
  end

  def down
    remove_foreign_key :diary_appointments, :created_by
    remove_reference :diary_appointments, :created_by, index: true

    remove_foreign_key :diary_appointments, :manager
    remove_reference :diary_appointments, :manager, index: true
  end
end
