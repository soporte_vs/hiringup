class CreateTerritoryCities < ActiveRecord::Migration
  def change
    create_table :territory_cities do |t|
      t.string :name
      t.references :territory_state, index: true

      t.timestamps null: false
    end
    add_foreign_key :territory_cities, :territory_states
  end
end
