class CreateMinisiteAnswers < ActiveRecord::Migration
  def change
    create_table :minisite_answers do |t|
      t.string :description
      t.references :minisite_question, index: true
      t.references :minisite_postulation, index: true

      t.timestamps null: false
    end
    add_foreign_key :minisite_answers, :minisite_questions
    add_foreign_key :minisite_answers, :minisite_postulations
  end
end
