class AddHupeStudyTypeToTrabajandoEducationLevel < ActiveRecord::Migration
  def change
    add_reference :trabajando_education_levels, :hupe_study_type, index: true
    add_foreign_key :trabajando_education_levels, :education_study_types, column: :hupe_study_type_id
  end
end
