class AddGeoDataToTrabajandoPublications < ActiveRecord::Migration
  def change
    add_reference :trabajando_publications, :region, index: true
    add_foreign_key :trabajando_publications, :trabajando_states, column: :region_id

    add_reference :trabajando_publications, :city, index: true
    add_foreign_key :trabajando_publications, :trabajando_cities, column: :city_id
  end
end
