# coding: utf-8
class AddTrabajandoEducationStates < SeedMigration::Migration
  def up
    Trabajando::EducationState.reset_column_information
    [{:name=>"Incompleto", :trabajando_id=>0, :hupe_study_level_id=>4},
     {:name=>"Estudiando", :trabajando_id=>2, :hupe_study_level_id=>3},
     {:name=>"Graduado", :trabajando_id=>3, :hupe_study_level_id=>5},
     {:name=>"En curso", :trabajando_id=>1, :hupe_study_level_id=>3},
     {:name=>"Indiferente", :trabajando_id=>5, :hupe_study_level_id=>nil},
     {:name=>"Próximo a graduarse", :trabajando_id=>4, :hupe_study_level_id=>3}].each do |state|
      trabajando_education_state = Trabajando::EducationState.find_or_create_by(state.except(:hupe_study_level_id))
      trabajando_education_state.update(hupe_study_level_id: state[:hupe_study_level_id])
    end
  end

  def down
    Trabajando::EducationState.destroy_all
  end
end
