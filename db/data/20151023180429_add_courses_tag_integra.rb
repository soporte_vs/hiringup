class AddCoursesTagIntegra < SeedMigration::Migration
  def up
    [
      {name: 'Jardines Infantiles'},
      {name: 'Oficinas Regionales'},
      {name: 'Casa Central'},
      {name: 'Prácticas'},
      {name: 'Promociones'}
    ].each do |tag|
      Tag::CourseTag.create(tag)
    end
  end

  def down
    Tag::CourseTag.destroy_all
  end
end
