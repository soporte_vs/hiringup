class AddBoliviaStatesAndCities < SeedMigration::Migration
  def up
    #Division politica de Bolivia, está estructurado en 9 departamentos, 112 provincias,
    bolivia = Territory::Country.find_by(name: 'Bolivia')

    d01 = Territory::State.create(name: "Beni", territory_country: bolivia)
    d02 = Territory::State.create(name: "Chuquisaca", territory_country: bolivia)
    d03 = Territory::State.create(name: "Cochabamba", territory_country: bolivia)
    d04 = Territory::State.create(name: "La Paz", territory_country: bolivia)
    d05 = Territory::State.create(name: "Oruro", territory_country: bolivia)
    d06 = Territory::State.create(name: "Pando", territory_country: bolivia)
    d07 = Territory::State.create(name: "Potosí", territory_country: bolivia)
    d08 = Territory::State.create(name: "Santa Cruz", territory_country: bolivia)
    d09 = Territory::State.create(name: "Tarija", territory_country: bolivia)
    [

        # d01 Beni 08
        {territory_state_id: d01.id, name: "Cercado" },
        {territory_state_id: d01.id, name: "Antonio Vaca Díez" },
        {territory_state_id: d01.id, name: "Mariscal José Ballivián Segurola" },
        {territory_state_id: d01.id, name: "Yacuma" },
        {territory_state_id: d01.id, name: "Moxos" },
        {territory_state_id: d01.id, name: "Marbán" },
        {territory_state_id: d01.id, name: "Mamoré" },
        {territory_state_id: d01.id, name: "Iténez" },

        # d02 Chuquisaca 10
        {territory_state_id: d02.id, name: "Oropeza" },
        {territory_state_id: d02.id, name: "Juana Azurduy de Padilla" },
        {territory_state_id: d02.id, name: "Jaime Zudáñez" },
        {territory_state_id: d02.id, name: "Tomina" },
        {territory_state_id: d02.id, name: "Hernando Siles" },
        {territory_state_id: d02.id, name: "Yamparáez" },
        {territory_state_id: d02.id, name: "Nor Cinti" },
        {territory_state_id: d02.id, name: "Sud Cinti" },
        {territory_state_id: d02.id, name: "Belisario Boeto" },
        {territory_state_id: d02.id, name: "Luis Calvo" },

        # d03 Cochabamba 16
        {territory_state_id: d03.id, name: "Arani" },
        {territory_state_id: d03.id, name: "Esteban Arce" },
        {territory_state_id: d03.id, name: "Arque" },
        {territory_state_id: d03.id, name: "Ayopaya" },
        {territory_state_id: d03.id, name: "Campero" },
        {territory_state_id: d03.id, name: "Capinota" },
        {territory_state_id: d03.id, name: "Cercado" },
        {territory_state_id: d03.id, name: "Carrasco" },
        {territory_state_id: d03.id, name: "Chapare" },
        {territory_state_id: d03.id, name: "Germán Jordán" },
        {territory_state_id: d03.id, name: "Mizque" },
        {territory_state_id: d03.id, name: "Punata" },
        {territory_state_id: d03.id, name: "Quillacollo" },
        {territory_state_id: d03.id, name: "Tapacarí" },
        {territory_state_id: d03.id, name: "Bolívar" },
        {territory_state_id: d03.id, name: "Tiraque" },

        # d04 La Paz 20
        {territory_state_id: d04.id, name: "Aroma" },
        {territory_state_id: d04.id, name: "Bautista Saavedra" },
        {territory_state_id: d04.id, name: "Abel Iturralde" },
        {territory_state_id: d04.id, name: "Caranavi" },
        {territory_state_id: d04.id, name: "Eliodoro Camacho" },
        {territory_state_id: d04.id, name: "Franz Tamayo" },
        {territory_state_id: d04.id, name: "Gualberto Villaroel" },
        {territory_state_id: d04.id, name: "Ingavi" },
        {territory_state_id: d04.id, name: "Inquisivi" },
        {territory_state_id: d04.id, name: "General José Manuel Pando" },
        {territory_state_id: d04.id, name: "Larecaja" },
        {territory_state_id: d04.id, name: "Loayza" },
        {territory_state_id: d04.id, name: "Los Andes" },
        {territory_state_id: d04.id, name: "Manco Kapac" },
        {territory_state_id: d04.id, name: "Muñecas" },
        {territory_state_id: d04.id, name: "Nor Yungas" },
        {territory_state_id: d04.id, name: "Omasuyos" },
        {territory_state_id: d04.id, name: "Pacajes" },
        {territory_state_id: d04.id, name: "Pedro Domingo Murillo" },
        {territory_state_id: d04.id, name: "Sud Yungas" },

        # d05 Oruro 16
        {territory_state_id: d05.id, name: "Sabaya" },
        {territory_state_id: d05.id, name: "Carangas" },
        {territory_state_id: d05.id, name: "Cercado" },
        {territory_state_id: d05.id, name: "Eduardo Avaroa" },
        {territory_state_id: d05.id, name: "Ladislao Cabrera" },
        {territory_state_id: d05.id, name: "Litoral" },
        {territory_state_id: d05.id, name: "Mejillones" },
        {territory_state_id: d05.id, name: "Nor Carangas" },
        {territory_state_id: d05.id, name: "Pantaleón Dalence" },
        {territory_state_id: d05.id, name: "Poopó" },
        {territory_state_id: d05.id, name: "Sajama" },
        {territory_state_id: d05.id, name: "San Pedro de Totora" },
        {territory_state_id: d05.id, name: "Saucarí" },
        {territory_state_id: d05.id, name: "Sebastian Pagador" },
        {territory_state_id: d05.id, name: "Sud Carangas" },
        {territory_state_id: d05.id, name: "Tomas Barrón" },

        # d06 Pando 05
        {territory_state_id: d06.id, name: "Abuná" },
        {territory_state_id: d06.id, name: "Federico Román" },
        {territory_state_id: d06.id, name: "Madre de Dios" },
        {territory_state_id: d06.id, name: "Manuripi" },
        {territory_state_id: d06.id, name: "Nicolás Suárez" },

        # d07 Potosí 16
        {territory_state_id: d07.id, name: "Alonzo de Ibáñez" },
        {territory_state_id: d07.id, name: "Antonio Quijarro" },
        {territory_state_id: d07.id, name: "Bernardino Bilbao" },
        {territory_state_id: d07.id, name: "Charcas" },
        {territory_state_id: d07.id, name: "Chayanta" },
        {territory_state_id: d07.id, name: "Cornelio Saavedra" },
        {territory_state_id: d07.id, name: "Daniel Saavedra" },
        {territory_state_id: d07.id, name: "Enrique Baldivieso" },
        {territory_state_id: d07.id, name: "José María Linares" },
        {territory_state_id: d07.id, name: "Modesto Omiste" },
        {territory_state_id: d07.id, name: "Nor Chichas" },
        {territory_state_id: d07.id, name: "Nor Lípez" },
        {territory_state_id: d07.id, name: "Rafael Bustillo" },
        {territory_state_id: d07.id, name: "Sud Chichas" },
        {territory_state_id: d07.id, name: "Sud Lipez" },
        {territory_state_id: d07.id, name: "Tomás Frías" },

        #  d08 Santa Cruz 15
        {territory_state_id: d08.id, name: "Andrés Ibáñez" },
        {territory_state_id: d08.id, name: "Ignacio Warnes" },
        {territory_state_id: d08.id, name: "José Miguel de Velasco" },
        {territory_state_id: d08.id, name: "Ichilo" },
        {territory_state_id: d08.id, name: "Chiquitos" },
        {territory_state_id: d08.id, name: "Sara" },
        {territory_state_id: d08.id, name: "Cordillera" },
        {territory_state_id: d08.id, name: "Vallegrande" },
        {territory_state_id: d08.id, name: "Florida" },
        {territory_state_id: d08.id, name: "Santistevan" },
        {territory_state_id: d08.id, name: "Ñuflo de Chávez" },
        {territory_state_id: d08.id, name: "Ángel Sandoval" },
        {territory_state_id: d08.id, name: "Caballero" },
        {territory_state_id: d08.id, name: "Germán Busch" },
        {territory_state_id: d08.id, name: "Guarayos" },

        # d09 Tarija 06
        {territory_state_id: d09.id, name: "Aniceto Arce" },
        {territory_state_id: d09.id, name: "Burdet O'Connor" },
        {territory_state_id: d09.id, name: "Cercado" },
        {territory_state_id: d09.id, name: "Eustaquio Méndez" },
        {territory_state_id: d09.id, name: "Gran Chaco" },
        {territory_state_id: d09.id, name: "José María Avilés" }


    ].each do |state_and_cities|
      Territory::City.create(state_and_cities)
    end
  end

  def down
    bolivia = Territory::Country.find_by(name: 'Bolivia')
    bolivia.territory_states.destroy_all
  end
end
