class AddTrabajandoSoftwareLevel < SeedMigration::Migration
  def up
    Trabajando::SoftwareLevel.reset_column_information
    [
      {name: "Ninguno", trabajando_id: 0, hupe_software_level_id: 1},
      {name: "Nivel bajo", trabajando_id: 1, hupe_software_level_id: 2},
      {name: "Nivel usuario", trabajando_id: 2, hupe_software_level_id: 3},
      {name: "Nivel usuario avanzado", trabajando_id: 3, hupe_software_level_id: 4},
      {name: "Nivel técnico", trabajando_id: 4, hupe_software_level_id: 5},
      {name: "Nivel profesional", trabajando_id: 5, hupe_software_level_id: 6},
      {name: "Nivel experto", trabajando_id: 6, hupe_software_level_id: 7}
    ].each do |sl|
      trabajando_software_level = Trabajando::SoftwareLevel.find_or_create_by(sl.except(:hupe_software_level_id))
      trabajando_software_level.update(hupe_software_level_id: sl[:hupe_software_level_id])
    end
  end

  def down
    Trabajando::SoftwareLevel.destroy_all
  end
end
