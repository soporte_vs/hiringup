class UpdateCoursesTagIntegra < SeedMigration::Migration
  def up
      find_tag = Tag::CourseTag.find_by(name: "Promociones")
      find_tag.update(name: "Promociones y Traslados") if find_tag.present?
  end

  def down
    find_tag = Tag::CourseTag.find_by(name: "Promociones y Traslados")
    find_tag.update(name: "Promociones") if find_tag.present?
  end
end
