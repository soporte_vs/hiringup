class ModifyDescriptors < SeedMigration::Migration
  def up
    criminal_record = Document::Descriptor.find_by(name: 'certificado_antecedentes')
    if criminal_record.present?
      criminal_record.update_attributes(details: "Certificado de antecedentes original y de preferencia de emisión inferior a 30 días (*)(**)")
    end

    pics = Document::Descriptor.find_by(name: "fotos_tamaño_carne")
    if pics.present?
      pics.update_attributes(details: "Dos fotos tamaño carnet")
    end

    studies = Document::Descriptor.find_by(name: "certificado_de_estudios")
    if studies.present?
      studies.update_attributes(details: "Certificado de estudios o título profesional, original o fotocopia legalizada (*)(**)")
    end
  end

  def down
  end
end
