class AddDiscardReasons < SeedMigration::Migration
  def up
[{"name"=>"Descartado por mejor candidato (Flujo R&S)",
  "description"=>"Descartado por mejor candidato",
  "mailer_content"=>"",
  "subject"=>""},
 {"name"=>"Descarte  por cierre de proceso (Flujo R&S)",
  "description"=>"Descarte",
  "mailer_content"=>"",
  "subject"=>""},
 {"name"=>
   "Rechazo  postulación Promoción y/o Traslado (Flujo de Promociones y/o Traslado)",
  "description"=>"Rechazo  postulación Promoción y/o Traslado",
  "mailer_content"=>
   "<span>\r\n <span><span>\r\n  <span>\r\n  <p>Estimado(a)&nbsp;<span>[[candidato]]<br></span></p><div>Junto con saludar, confirmarte que hemos\r\nrecibido tu &nbsp;ficha de promoción y\r\nantecedentes, habiendo verificado que lamentablemente no cumples con la\r\ntotalidad de los requisitos exigidos, encontrándote por lo tanto inhabilitado(a)\r\nde continuar con el proceso, de acuerdo a lo establecido en las orientaciones\r\nvigentes sobre movilidad interna.&nbsp;<br></div><p><br></p><p>Atte.<br>Fundación Integra<br></p>\r\n  </span>\r\n </span>\r\n</span></span>",
  "subject"=>"Proceso de postulación  Fundación Integra"},
 {"name"=>"Desiste del proceso (Flujo R&S)",
  "description"=>"Desiste del proceso",
  "mailer_content"=>"",
  "subject"=>""},
 {"name"=>"Descartado por Inhabilidades (Flujo R&S)",
  "description"=>"Descartado por BlackList",
  "mailer_content"=>"",
  "subject"=>""},
 {"name"=>"Descartado por referencias (Flujo R&S)",
  "description"=>"Descartado por referencias",
  "mailer_content"=>"",
  "subject"=>""},
 {"name"=>"Filtro Curriculuar (Flujo R&S)",
  "description"=>"Filtro Curriculuar",
  "mailer_content"=>"",
  "subject"=>""},
 {"name"=>"Evaluación Grupal (Flujo R&S)",
  "description"=>"Evaluación Grupal",
  "mailer_content"=>"",
  "subject"=>""},
 {"name"=>"Prueba Técnica (Flujo R&S)",
  "description"=>"Prueba Técnica",
  "mailer_content"=>"",
  "subject"=>""},
 {"name"=>"Evaluación Psicolaboral (Flujo R&S)",
  "description"=>"Evaluación Psicolaboral",
  "mailer_content"=>"",
  "subject"=>""},
 {"name"=>"Entrevista con Jefatura (Flujo R&S)",
  "description"=>"Entrevista con Jefatura",
  "mailer_content"=>"",
  "subject"=>""},
 {"name"=>"Descarte práctica (Flujo Práctica)",
  "description"=>
   "Al descartar en el proceso de práctica debe elegir esta opción.",
  "mailer_content"=>
   "<p><b>&nbsp;</b></p>\r\n\r\n<p>Estimado (a)&nbsp;[[candidato]]:</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Agradecemos tu interés de participar del proceso de\r\nReclutamiento y Selección de\r\nPrácticas Profesionales en Integra. Sin embargo, para este proceso en\r\nparticular, no has sido seleccionado(a).</p>\r\n\r\n<p>Tus\r\nantecedentes quedarán en nuestra base de datos y, en caso que iniciemos un\r\nnuevo proceso en que tanto tus conocimientos, como tu perfil se adecuen a\r\nnuestros requerimientos, tomaremos contacto contigo.</p>\r\n\r\n<p>Te recomendamos que mantengas actualizado tu perfil\r\nen nuestra plataforma.\r\n</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Cordialmente,</p>\r\n\r\n<p>Equipo de Reclutamiento y Selección</p>\r\n\r\nFundación Integra",
  "subject"=>
   "Resultado del proceso de postulación Práctica en Fundación Integra "},
 {"name"=>
   "Descarte por promoción y/o Traslado de otro trabajador (Flujo Promociones y Traslados)",
  "description"=>"Descarte por promoción y/o Traslado  de otro trabajador",
  "mailer_content"=>
   "Estimado(a)&nbsp;<span>[[candidato]]<br></span><p>Junto con saludar, confirmarte que luego de revisar y&nbsp;analizar las variables ponderadas según la tabla de factores vigentes que contemplan las orientaciones vigentes sobre movilidad interna, no has obtenido la mayor puntuación en comparación a los otros postulantes. Agradecemos tu interés, informándote que mantendremos vigentes tus antecedentes comunicándonos contigo al abrirse otra postulación&nbsp;&nbsp;</p>Atte.<br>Fundación Integra",
  "subject"=>"Resultado proceso de Promoción y/o Traslado Integra"},
 {"name"=>"Descarte antes de invitar (Flujo R&S)",
  "description"=>
   "Se descarta al candidato una vez que es aceptada su postulación",
  "mailer_content"=>
   "<p>Estimado (a)&nbsp;[[candidato]]:</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Agradecemos tu interés por pertenecer a Integra y\r\nparticipar de nuestro proceso de Reclutamiento y Selección, para el cargo de&nbsp;[[cargo]]. Sin embargo, para este proceso en&nbsp;particular, no has sido\r\nseleccionado(a).</p>\r\n\r\n<p>Tus antecedentes quedarán en nuestra base de datos\r\ny, en caso que iniciemos un nuevo proceso en que tanto tus conocimientos, como\r\ntu perfil se adecuen a nuestros requerimientos, tomaremos contacto contigo.</p>\r\n\r\n<p>Te recomendamos que mantengas actualizado tu perfil\r\nen nuestra plataforma. </p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Cordialmente,</p>\r\n\r\n<p>Equipo de Reclutamiento y Selección</p>\r\n\r\nFundación Integra",
  "subject"=>"Resultado del proceso de postulación en Fundación Integra"},
 {"name"=>"Rechaza carta oferta (Flujo R&S)",
  "description"=>"Rechaza carta oferta",
  "mailer_content"=>
   "<span>Estimado(a)&nbsp;[[candidato]]:<br><br></span>Agradecemos tu participación en nuestro proceso de Reclutamiento y Selección del cargo&nbsp;[[cargo]].<br><p><br></p><p>Cordialmente,<br></p><p>Área Desarrollo de Personas y Equipos</p><p>Fundación Integra</p>",
  "subject"=>""}].each do |dr|
      Enrollment::DiscardReason.where(dr).first_or_create
    end
  end

  def down
    Enrollment::DiscardReason.destroy_all
  end
end
