class AddNewsRecluitments < SeedMigration::Migration
  def up
    # Se cargan Perfiles regionales a petición de integra
    all_data = [
      { region: "Arica", name: "Elizabeth Andrea" , last_name: "Cutipa González", rut: "13968427-6",  email: "ecutipa@integra.cl" },
      { region: "Antofagasta", name: "María Albertina", last_name: "Ordenes Mundaca", rut: "11219504-1",  email: "mordenes@integra.cl" },
      { region: "Aysén", name: "Yasna Cristina", last_name: "Oyarzún Carrasco", rut: "9323517-7",  email: "yoyarzun@integra.cl" },
      { region: "Magallanes", name: "Eduardo Javier", last_name: "Brahona Kompatzki", rut: "13257246-1",  email: "ebarahona@integra.cl" }
    ]

    all_data.each do |data|
      region = data[:region]
      rut_type = People::IdentificationDocumentType.find_by(name: 'RUT')
      rut = data[:rut]
      email = data[:email].downcase
      user_name = data[:name].capitalize
      user_last_name = data[:last_name].capitalize
      # Crear grupo
      group = Group.find_by(name: region)

      # Crear user, password es el RUT
      user = User.find_by(email: email)
      if user.nil?# Si user no existe, crear user
        user = User.create(email: email, password: 'integra.2016')

        peopple = People::PersonalInformation.find_or_create_by(first_name: user_name, 
        last_name: user_last_name, user_id: user.id, 
        identification_document_type_id: rut_type.id, 
        identification_document_number: rut)
        unless peopple.persisted?
          error = peopple.errors.messages
          puts "RUT: #{rut} Errores: #{error}"
        end
      end

      user.add_role :admin_reports
      user.add_role :admin, Applicant::Catched
      user.add_role :create, Applicant::Catched
      user.add_role :new, Applicant::Catched
      user.add_role :search, Applicant::Base
      user.add_role :update, Applicant::Base
      user.add_role :new, Course
      user.add_role :create, Course
      user.add_role :search, Course

      # Agregar personal_information al user
      if user.persisted? && group.present?
        user_group = UserGroup.find_or_create_by(user_id: user.id, group_id: group.id)
        puts "[UserGroup - #{user_group.id}] ---> Agregando a #{user.email} al groupo #{user_group.group.name}"
      end
    end
  end

  def down

  end
end
