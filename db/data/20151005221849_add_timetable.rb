class AddTimetable < SeedMigration::Migration
  def up
    [
      {:name=>"Jornada completa", :description=> "Trabajo de tiempo completo, jornada diaria debe ser continua y no puede exceder de 10 horas"},
      {:name=>"Extensión Horaria", :description=>nil},
      {:name=>"Freelance (sin horario)", :description=>""},
      {:name=>"Turno rotativo", :description=>nil},
      {:name=>"Media Jornada", :description=>"Jornada de trabajo no superior a 30 horas semanales"}
    ].each do |ct|
      Company::Timetable.where(ct).first_or_create
    end
  end

  def down
    Company::Timetable.destroy_all
  end
end
