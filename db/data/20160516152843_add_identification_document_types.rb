# coding: utf-8
class AddIdentificationDocumentTypes < SeedMigration::Migration
  def up
    # Pasaporte para todos los paises
    countries = Territory::Country.all
    countries.each do |country|
      idt = People::IdentificationDocumentType.create(
        country: country,
        name: "Pasaporte",
        validation_regex: "^[0-9a-zA-Z]+$"
      )
    end

    # RUT
    chile = Territory::Country.find_by(name: "Chile")
    if chile.present?
      People::IdentificationDocumentType.create(
        country: chile,
        name: "RUT",
        validation_regex: "^([0-9]+-[0-9Kk])$"
      )
    end

    # DNI
    peru = Territory::Country.find_by(name: "Perú")
    if peru.present?
      People::IdentificationDocumentType.create(
        country: peru,
        name: "DNI"
      )
    end
  end

  def down
    People::IdentificationDocumentType.destroy_all
  end
end
