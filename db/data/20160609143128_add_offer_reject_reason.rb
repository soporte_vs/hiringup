class AddOfferRejectReason < SeedMigration::Migration
  def up
    Offer::RejectReason.destroy_all
    [
      {name: "Renta no es la que esperaba.", description: ""},
      {name: "Encontré otro trabajo.", description: ""},
      {name: "Me arrepentí, me encuentro trabajando en un lugar estable.",  description: ""},
      {name: "Otra razón.", description: ""}
    ].each do |reason|
      reject_reason = Offer::RejectReason.create_with(description: reason[:name]).find_or_create_by(name: reason[:name])
    end
  end

  def down
    Offer::RejectReason.destroy_all
  end
end
