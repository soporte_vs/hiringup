class CompanyVacancyRequestReasons < SeedMigration::Migration
  def up
    object_with_errors = []

    all_data = [
      {name: "Aumento dotación"},
      {name: "Planta por desvinculación"},
      {name: "Planta por renuncia"},
      {name: "Práctica profesional"},
      {name: "Proyecto Especial"},
      {name: "Reemplazo"},
      {name: "Vacaciones en mi Jardín"},
      {name: "Promoción y/o Traslado"}
    ]
    all_data.each do |data|
      vacancy_request_reason = Company::VacancyRequestReason.new(data)
      unless vacancy_request_reason.save
        object_with_errors.push(vacancy_request_reason.clone)
      end
    end
    puts "\033[31mSe han encontrado #{object_with_errors.length} al tratar de crear #{Company::VacancyRequestReason.model_name.human}:\033[0m \n" unless object_with_errors.empty?
    object_with_errors.each do |object|
      puts "\t#{object.to_s}"

      object.errors.full_messages.each do |error|
        puts "\t\tErrors => \033[31m#{error}\033[0m"
      end
    end
    puts "\033[32mSe han creado #{all_data.length - object_with_errors.length} de #{all_data.length} #{Company::VacancyRequestReason.model_name.human}\033[0m \n"
  end

  def down
    Company::VacancyRequestReason.destroy_all
  end
end
