class AddStageNotApplyReason < SeedMigration::Migration
  def up
    Stage::NotApplyReason.delete_all
    [
      {name: "Etapa no corresponde se considerada en este proceso",description: "Etapa no corresponde se considerada en este proceso"},
      {name: "No se requiere realizar agendamiento", description: "No se requiere realizar agendamiento"}
    ].each do |reason|
      not_apply_reason = Stage::NotApplyReason.create_with(description: reason[:description]).find_or_create_by(name: reason[:name])
    end

  end

  def down
    Stage::NotApplyReason.delete_all
  end
end
