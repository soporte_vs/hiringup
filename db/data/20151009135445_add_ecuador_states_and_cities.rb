class AddEcuadorStatesAndCities < SeedMigration::Migration
  def up
      #Ecuador se dicvide en 24 provincias, las cuales se dividen en cantones.
      ecuador = Territory::Country.find_by(name: 'Ecuador')

      p01 = Territory::State.create(name: "Azuay", territory_country: ecuador)
      p02 = Territory::State.create(name: "Bolívar", territory_country: ecuador)
      p03 = Territory::State.create(name: "Cañar", territory_country: ecuador)
      p04 = Territory::State.create(name: "Carchi", territory_country: ecuador)
      p05 = Territory::State.create(name: "Chimborazo", territory_country: ecuador)
      p06 = Territory::State.create(name: "Cotopaxi", territory_country: ecuador)
      p07 = Territory::State.create(name: "El Oro", territory_country: ecuador)
      p08 = Territory::State.create(name: "Esmeraldas", territory_country: ecuador)
      p09 = Territory::State.create(name: "Galápagos", territory_country: ecuador)
      p10 = Territory::State.create(name: "Guayas", territory_country: ecuador)
      p11 = Territory::State.create(name: "Imbabura", territory_country: ecuador)
      p12 = Territory::State.create(name: "Loja", territory_country: ecuador)
      p13 = Territory::State.create(name: "Los Ríos", territory_country: ecuador)
      p14 = Territory::State.create(name: "Manabí", territory_country: ecuador)
      p15 = Territory::State.create(name: "Morona Santiago", territory_country: ecuador)
      p16 = Territory::State.create(name: "Napo", territory_country: ecuador)
      p17 = Territory::State.create(name: "Orellana", territory_country: ecuador) #Falta
      p18 = Territory::State.create(name: "Pastaza", territory_country: ecuador)
      p19 = Territory::State.create(name: "Pichincha", territory_country: ecuador)
      p20 = Territory::State.create(name: "Santa Elena", territory_country: ecuador) #Falta
      p21 = Territory::State.create(name: "Santo Domingo de los Tsáchichilas", territory_country: ecuador) #Falta
      p22 = Territory::State.create(name: "Sucumbíos", territory_country: ecuador)
      p23 = Territory::State.create(name: "Tungurahua", territory_country: ecuador)
      p24 = Territory::State.create(name: "Zamora Chinchipe", territory_country: ecuador)

      [
        #Provincia Azuay p01
        {territory_state_id: p01.id, name: "Cuenca" },
        {territory_state_id: p01.id, name: "Girón" },
        {territory_state_id: p01.id, name: "Gualaceo" },
        {territory_state_id: p01.id, name: "Nabón" },
        {territory_state_id: p01.id, name: "Paute" },
        {territory_state_id: p01.id, name: "Pucará" },
        {territory_state_id: p01.id, name: "San Fernando" },
        {territory_state_id: p01.id, name: "Santa Isabel" },
        {territory_state_id: p01.id, name: "Sigsig" },
        {territory_state_id: p01.id, name: "Oña" },
        {territory_state_id: p01.id, name: "Chordeleg" },
        {territory_state_id: p01.id, name: "Pan" },
        {territory_state_id: p01.id, name: "Sevilla de Oro" },
        {territory_state_id: p01.id, name: "Guachapala" },
        #Provincia Bolívar p02
        {territory_state_id: p02.id, name: "Guaranda" },
        {territory_state_id: p02.id, name: "Chillanes" },
        {territory_state_id: p02.id, name: "Chimbo" },
        {territory_state_id: p02.id, name: "Echeandía" },
        {territory_state_id: p02.id, name: "Caluma" },
        {territory_state_id: p02.id, name: "Las Naves" },
        {territory_state_id: p02.id, name: "San Miguel" },
        #Provincia Cañar p03
        {territory_state_id: p03.id, name: "Azogues" },
        {territory_state_id: p03.id, name: "Biblián" },
        {territory_state_id: p03.id, name: "Cañar" },
        {territory_state_id: p03.id, name: "La Troncal" },
        {territory_state_id: p03.id, name: "Déleg" },
        {territory_state_id: p03.id, name: "El Tambo" },
        {territory_state_id: p03.id, name: "Suscal" },
        #Provincia Carchi p04
        {territory_state_id: p04.id, name: "Tulcán" },
        {territory_state_id: p04.id, name: "Bolívar" },
        {territory_state_id: p04.id, name: "Espejo" },
        {territory_state_id: p04.id, name: "Mira" },
        {territory_state_id: p04.id, name: "Montufar" },
        {territory_state_id: p04.id, name: "San Pedro de Huaca" },
        #Provincia Chimborazo p05
        {territory_state_id: p05.id, name: "Riobamba" },
        {territory_state_id: p05.id, name: "Alusí" },
        {territory_state_id: p05.id, name: "Colta" },
        {territory_state_id: p05.id, name: "Chambo" },
        {territory_state_id: p05.id, name: "Chunchi" },
        {territory_state_id: p05.id, name: "Guamote" },
        {territory_state_id: p05.id, name: "Guano" },
        {territory_state_id: p05.id, name: "Pallatanga" },
        {territory_state_id: p05.id, name: "Penipe" },
        {territory_state_id: p05.id, name: "Cumanda" },
        #Provincia Cotopaxi p06
        {territory_state_id: p06.id, name: "Latacunga" },
        {territory_state_id: p06.id, name: "La Mana" },
        {territory_state_id: p06.id, name: "Pangua" },
        {territory_state_id: p06.id, name: "Pujilí" },
        {territory_state_id: p06.id, name: "Salcedo" },
        {territory_state_id: p06.id, name: "Saquislí" },
        {territory_state_id: p06.id, name: "Sigchos" },
        #Provincia El Oro p07
        {territory_state_id: p07.id, name: "Machala" },
        {territory_state_id: p07.id, name: "Arenillas" },
        {territory_state_id: p07.id, name: "Atahualpa" },
        {territory_state_id: p07.id, name: "Balsas" },
        {territory_state_id: p07.id, name: "Chilla" },
        {territory_state_id: p07.id, name: "Guabo" },
        {territory_state_id: p07.id, name: "Huaquillas" },
        {territory_state_id: p07.id, name: "Marcabelí" },
        {territory_state_id: p07.id, name: "Pasaje" },
        {territory_state_id: p07.id, name: "Piñas" },
        {territory_state_id: p07.id, name: "Portovelo" },
        {territory_state_id: p07.id, name: "Santa Rosa" },
        {territory_state_id: p07.id, name: "Zaruma" },
        {territory_state_id: p07.id, name: "Las Lajas" },
        #Provincia Esmeraldas p08
        {territory_state_id: p08.id, name: "Esmeraldas" },
        {territory_state_id: p08.id, name: "Eloy Alfaro" },
        {territory_state_id: p08.id, name: "Muisne" },
        {territory_state_id: p08.id, name: "Quininde" },
        {territory_state_id: p08.id, name: "San Lorenzo" },
        {territory_state_id: p08.id, name: "Atacames" },
        {territory_state_id: p08.id, name: "Río Verde" },
        #Provincia Galapagos p09
        {territory_state_id: p09.id, name: "San Cristóbal" },
        {territory_state_id: p09.id, name: "Isabela" },
        {territory_state_id: p09.id, name: "Santa Cruz" },
        #Provincia Guayas p10
        {territory_state_id: p10.id, name: "Guayaquil" },
        {territory_state_id: p10.id, name: "Alfredo Baquerizo Moreno" },
        {territory_state_id: p10.id, name: "Balao" },
        {territory_state_id: p10.id, name: "Balzar" },
        {territory_state_id: p10.id, name: "Colimes" },
        {territory_state_id: p10.id, name: "Daule" },
        {territory_state_id: p10.id, name: "Durán" },
        {territory_state_id: p10.id, name: "El Empalme" },
        {territory_state_id: p10.id, name: "Triunfo" },
        {territory_state_id: p10.id, name: "Milagro" },
        {territory_state_id: p10.id, name: "Naranjal" },
        {territory_state_id: p10.id, name: "Naranjito" },
        {territory_state_id: p10.id, name: "Palestina" },
        {territory_state_id: p10.id, name: "Pedro Carbo" },
        {territory_state_id: p10.id, name: "Samborondon" },
        {territory_state_id: p10.id, name: "Santa Lucia" },
        {territory_state_id: p10.id, name: "Salitre" },
        {territory_state_id: p10.id, name: "Yaguachi" },
        {territory_state_id: p10.id, name: "Playas" },
        {territory_state_id: p10.id, name: "Simón Bolívar" },
        {territory_state_id: p10.id, name: "Coronel Marcelino Maridueñas" },
        {territory_state_id: p10.id, name: "Sargentillo" },
        {territory_state_id: p10.id, name: "Nobol" },
        {territory_state_id: p10.id, name: "General Antonio Elizarde (Bucay)" },
        {territory_state_id: p10.id, name: "Isidro Ayora" },
        #Provincia Imbabura p11
        {territory_state_id: p11.id, name: "Ibarra" },
        {territory_state_id: p11.id, name: "Antonio Ante" },
        {territory_state_id: p11.id, name: "Cotacachi" },
        {territory_state_id: p11.id, name: "Otavalo" },
        {territory_state_id: p11.id, name: "Pimampiro" },
        {territory_state_id: p11.id, name: "San Miguel de Urcuquí" },
        #Provincia Loja p12
        {territory_state_id: p12.id, name: "Loja" },
        {territory_state_id: p12.id, name: "Calvas" },
        {territory_state_id: p12.id, name: "Catamayo" },
        {territory_state_id: p12.id, name: "Celica" },
        {territory_state_id: p12.id, name: "Chaguarpamba" },
        {territory_state_id: p12.id, name: "Espíndola" },
        {territory_state_id: p12.id, name: "Gonzanamá" },
        {territory_state_id: p12.id, name: "Macará" },
        {territory_state_id: p12.id, name: "Paltas" },
        {territory_state_id: p12.id, name: "Puyango" },
        {territory_state_id: p12.id, name: "Saraguro" },
        {territory_state_id: p12.id, name: "Sozaranga" },
        {territory_state_id: p12.id, name: "Zapotillo" },
        {territory_state_id: p12.id, name: "Pindal" },
        {territory_state_id: p12.id, name: "Quillanga" },
        {territory_state_id: p12.id, name: "Olmedo" },
        #Provincia Los Rios p13
        {territory_state_id: p13.id, name: "Babahoyo" },
        {territory_state_id: p13.id, name: "Baba" },
        {territory_state_id: p13.id, name: "Montalvo" },
        {territory_state_id: p13.id, name: "Puebloviejo" },
        {territory_state_id: p13.id, name: "Quevedo" },
        {territory_state_id: p13.id, name: "Urdaneta" },
        {territory_state_id: p13.id, name: "Ventanas" },
        {territory_state_id: p13.id, name: "Vinces" },
        {territory_state_id: p13.id, name: "Palenque" },
        {territory_state_id: p13.id, name: "Buena Fe" },
        {territory_state_id: p13.id, name: "Valencia" },
        {territory_state_id: p13.id, name: "Mocache" },
        #Provincia Manabí p14
        {territory_state_id: p14.id, name: "Portoviejo" },
        {territory_state_id: p14.id, name: "Bolívar" },
        {territory_state_id: p14.id, name: "Chone" },
        {territory_state_id: p14.id, name: "El Carmen" },
        {territory_state_id: p14.id, name: "Flavio Alfaro" },
        {territory_state_id: p14.id, name: "Jipijapa" },
        {territory_state_id: p14.id, name: "Junín" },
        {territory_state_id: p14.id, name: "Manta" },
        {territory_state_id: p14.id, name: "Jaramijó" },
        {territory_state_id: p14.id, name: "San Vicente" },
        {territory_state_id: p14.id, name: "Montecristi" },
        {territory_state_id: p14.id, name: "Paján" },
        {territory_state_id: p14.id, name: "Pichincha" },
        {territory_state_id: p14.id, name: "Rocafuerte" },
        {territory_state_id: p14.id, name: "Santa Ana" },
        {territory_state_id: p14.id, name: "Sucre" },
        {territory_state_id: p14.id, name: "Tosagua" },
        {territory_state_id: p14.id, name: "24 de Mayo" },
        {territory_state_id: p14.id, name: "Pedernales" },
        {territory_state_id: p14.id, name: "Olmedo" },
        {territory_state_id: p14.id, name: "Puerto López" },
        {territory_state_id: p14.id, name: "Jama" },
        #Provincia Morona Santiago  p15
        {territory_state_id: p15.id, name: "Morona" },
        {territory_state_id: p15.id, name: "Gualaquiza" },
        {territory_state_id: p15.id, name: "Limón Indaza" },
        {territory_state_id: p15.id, name: "Palora" },
        {territory_state_id: p15.id, name: "Santiago" },
        {territory_state_id: p15.id, name: "Sucúa" },
        {territory_state_id: p15.id, name: "Huamboya" },
        {territory_state_id: p15.id, name: "San Juan Bosco" },
        {territory_state_id: p15.id, name: "Taisha" },
        {territory_state_id: p15.id, name: "Logroño" },
        #Provincia Santa Napo p16
        {territory_state_id: p16.id, name: "Tena" },
        {territory_state_id: p16.id, name: "Archidona" },
        {territory_state_id: p16.id, name: "El Chaco" },
        {territory_state_id: p16.id, name: "Quijos" },
        {territory_state_id: p16.id, name: "Carlos Julio Arosemena" },
        {territory_state_id: p16.id, name: "Tola" },
        #Provincia Orellana  p17
        {territory_state_id: p17.id, name: "Orellana" },
        {territory_state_id: p17.id, name: "Aguarico" },
        {territory_state_id: p17.id, name: "La Joya de los Sachas" },
        {territory_state_id: p17.id, name: "Loreto" },
        #Provincia Pastaza  p18
        {territory_state_id: p18.id, name: "Pastaza" },
        {territory_state_id: p18.id, name: "Mera" },
        {territory_state_id: p18.id, name: "Santa Clara" },
        {territory_state_id: p18.id, name: "Arajuno" },
        #Provincia Pichincha p19
        {territory_state_id: p19.id, name: "Quito" },
        {territory_state_id: p19.id, name: "Cayambe" },
        {territory_state_id: p19.id, name: "Mejía" },
        {territory_state_id: p19.id, name: "Pedro Moncayo" },
        {territory_state_id: p19.id, name: "Rumiñahui" },
        {territory_state_id: p19.id, name: "San Miguel de los Bancos" },
        {territory_state_id: p19.id, name: "Pedro Vicente Maldonado" },
        {territory_state_id: p19.id, name: "Puerto Quito" },
        #Provincia Santa Elena p20
        {territory_state_id: p20.id, name: "La Libertad" },
        {territory_state_id: p20.id, name: "Salinas" },
        {territory_state_id: p20.id, name: "Santa Elena" },
        #Provincia Santo Domingo de los Tsachilas p21
        {territory_state_id: p21.id, name: "Santo Domingo" },
        {territory_state_id: p21.id, name: "Tandapi" },
        {territory_state_id: p21.id, name: "La Concordia" },
        {territory_state_id: p21.id, name: "Pto. Limones" },
        {territory_state_id: p21.id, name: "Luz de America" },
        {territory_state_id: p21.id, name: "Santa Clara de Toachi" },
        #Provincia Sucumbíos p22
        {territory_state_id: p22.id, name: "Lago Agrio"},
        {territory_state_id: p22.id, name: "Gonzalo Pizarro"},
        {territory_state_id: p22.id, name: "Putumayo"},
        {territory_state_id: p22.id, name: "Shushufindi"},
        {territory_state_id: p22.id, name: "Sucumbio"},
        {territory_state_id: p22.id, name: "Cascales"},
        {territory_state_id: p22.id, name: "Cuyabeno"},
        #Provincia Tungurahua p23
        {territory_state_id: p23.id, name: "Ambato" },
        {territory_state_id: p23.id, name: "Baños" },
        {territory_state_id: p23.id, name: "Cevallos" },
        {territory_state_id: p23.id, name: "Mocha" },
        {territory_state_id: p23.id, name: "Patate" },
        {territory_state_id: p23.id, name: "Quero" },
        {territory_state_id: p23.id, name: "San Pedro de Pelileo" },
        {territory_state_id: p23.id, name: "Santiago de Píllaro" },
        {territory_state_id: p23.id, name: "Tisaleo" },
        #Provincia Zamora Chinchipe p24
        {territory_state_id: p24.id, name: "Zamora" },
        {territory_state_id: p24.id, name: "Chinchipe" },
        {territory_state_id: p24.id, name: "Nangaritza" },
        {territory_state_id: p24.id, name: "Yacuambi" },
        {territory_state_id: p24.id, name: "Yantzaza" },
        {territory_state_id: p24.id, name: "El Pangui" },
        {territory_state_id: p24.id, name: "Centinela del Cóndor" },
        {territory_state_id: p24.id, name: "Palanda" }
      ].each do |state_and_cities|
        Territory::City.create(state_and_cities)
      end
    end

    def down
      ecuador = Territory::Country.find_by(name: 'Ecuador')
      ecuador.territory_states.destroy_all
    end
  end
