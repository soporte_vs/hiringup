class AddParaguayStatesAndCities < SeedMigration::Migration
  def up
    #Division politica de Paraguay
    paraguay = Territory::Country.find_by(name: 'Paraguay')

    d01 = Territory::State.create(name: "Distrito Capital", territory_country: paraguay)
    d02 = Territory::State.create(name: "Alto Paraguay", territory_country: paraguay)
    d03 = Territory::State.create(name: "Alto Paraná", territory_country: paraguay)
    d04 = Territory::State.create(name: "Amambay", territory_country: paraguay)
    d05 = Territory::State.create(name: "Boquerón", territory_country: paraguay)
    d06 = Territory::State.create(name: "Caaguazú", territory_country: paraguay)
    d07 = Territory::State.create(name: "Caazapá", territory_country: paraguay)
    d08 = Territory::State.create(name: "Canindeyú", territory_country: paraguay)
    d09 = Territory::State.create(name: "Central", territory_country: paraguay)
    d10 = Territory::State.create(name: "Concepción", territory_country: paraguay)
    d11 = Territory::State.create(name: "Cordillera", territory_country: paraguay)
    d12 = Territory::State.create(name: "Guairá", territory_country: paraguay)
    d13 = Territory::State.create(name: "Itapúa", territory_country: paraguay)
    d14 = Territory::State.create(name: "Misiones", territory_country: paraguay)
    d15 = Territory::State.create(name: "Ñeembucú", territory_country: paraguay)
    d16 = Territory::State.create(name: "Paraguarí", territory_country: paraguay)
    d17 = Territory::State.create(name: "Presidente Hayes", territory_country: paraguay)
    d18 = Territory::State.create(name: "San Pedro", territory_country: paraguay)


    [
        # 1	Distrito Capital	1
        {territory_state_id: d01.id, name: "Asunción" },

        # 2	Alto Paraguay	4
        {territory_state_id: d02.id, name: "Bahía Negra" },
        {territory_state_id: d02.id, name: "Carmelo Peralta" },
        {territory_state_id: d02.id, name: "Puerto Casado" },
        {territory_state_id: d02.id, name: "Fuerte Olimpo" },

        # 3	Alto Paraná	22
        {territory_state_id: d03.id, name: "Ciudad del Este" },
        {territory_state_id: d03.id, name: "Doctor Juan León Mallorquín" },
        {territory_state_id: d03.id, name: "Doctor Raúl Peña" },
        {territory_state_id: d03.id, name: "Domingo Martínez de Irala" },
        {territory_state_id: d03.id, name: "Hernandarias" },
        {territory_state_id: d03.id, name: "Iruña" },
        {territory_state_id: d03.id, name: "Itakyry" },
        {territory_state_id: d03.id, name: "Juan Emilio O'Leary" },
        {territory_state_id: d03.id, name: "Los Cedrales" },
        {territory_state_id: d03.id, name: "Mbaracayú" },
        {territory_state_id: d03.id, name: "Minga Guazú" },
        {territory_state_id: d03.id, name: "Minga Porá" },
        {territory_state_id: d03.id, name: "Naranjal" },
        {territory_state_id: d03.id, name: "Ñacunday" },
        {territory_state_id: d03.id, name: "Presidente Franco" },
        {territory_state_id: d03.id, name: "San Alberto" },
        {territory_state_id: d03.id, name: "San Cristóbal" },
        {territory_state_id: d03.id, name: "Santa Fe del Paraná" },
        {territory_state_id: d03.id, name: "Santa Rita" },
        {territory_state_id: d03.id, name: "Santa Rosa del Monday" },
        {territory_state_id: d03.id, name: "Tavapy" },
        {territory_state_id: d03.id, name: "Yguazú" },

        # 4	Amambay	5
        {territory_state_id: d04.id, name: "Bella Vista" },
        {territory_state_id: d04.id, name: "Capitán Bado" },
        {territory_state_id: d04.id, name: "Pedro Juan Caballero" },
        {territory_state_id: d04.id, name: "Zanja Pytá" },
        {territory_state_id: d04.id, name: "Karapaí" },

        #5	Boquerón	3
        {territory_state_id: d05.id, name: "Filadelfia" },
        {territory_state_id: d05.id, name: "Loma Plata" },
        {territory_state_id: d05.id, name: "Mariscal Estigarribia" },



        # 6	Caaguazú	22
        {territory_state_id: d06.id, name: "Caaguazú" },
        {territory_state_id: d06.id, name: "Carayaó" },
        {territory_state_id: d06.id, name: "Coronel Oviedo" },
        {territory_state_id: d06.id, name: "Doctor Cecilio Báez" },
        {territory_state_id: d06.id, name: "Doctor J. Eulogio Estigarribia" },
        {territory_state_id: d06.id, name: "Doctor Juan Manuel Frutos" },
        {territory_state_id: d06.id, name: "José Domingo Ocampos" },
        {territory_state_id: d06.id, name: "La Pastora" },
        {territory_state_id: d06.id, name: "Mcal. Francisco S. López" },
        {territory_state_id: d06.id, name: "Nueva Londres" },
        {territory_state_id: d06.id, name: "Nueva Toledo" },
        {territory_state_id: d06.id, name: "Raúl Arsenio Oviedo" },
        {territory_state_id: d06.id, name: "Repatriación" },
        {territory_state_id: d06.id, name: "R. I. Tres Corrales" },
        {territory_state_id: d06.id, name: "San Joaquín" },
        {territory_state_id: d06.id, name: "San José de los Arroyos" },
        {territory_state_id: d06.id, name: "Santa Rosa del Mbutuy" },
        {territory_state_id: d06.id, name: "Simón Bolívar" },
        {territory_state_id: d06.id, name: "Tembiaporá" },
        {territory_state_id: d06.id, name: "Tres de Febrero" },
        {territory_state_id: d06.id, name: "Vaquería" },
        {territory_state_id: d06.id, name: "Yhú" },

        # 7	Caazapá	11
        {territory_state_id: d07.id, name: "3 de Mayo" },
        {territory_state_id: d07.id, name: "Abaí" },
        {territory_state_id: d07.id, name: "Buena Vista" },
        {territory_state_id: d07.id, name: "Caazapá" },
        {territory_state_id: d07.id, name: "Doctor Moisés S. Bertoni" },
        {territory_state_id: d07.id, name: "Fulgencio Yegros" },
        {territory_state_id: d07.id, name: "General Higinio Morínigo" },
        {territory_state_id: d07.id, name: "Maciel" },
        {territory_state_id: d07.id, name: "San Juan Nepomuceno" },
        {territory_state_id: d07.id, name: "Tavaí" },
        {territory_state_id: d07.id, name: "Yuty" },

        # 8	Canindeyú	13
        {territory_state_id: d08.id, name: "Corpus Christi" },
        {territory_state_id: d08.id, name: "Curuguaty" },
        {territory_state_id: d08.id, name: "Gral. Francisco Caballero Álvarez" },
        {territory_state_id: d08.id, name: "Itanará" },
        {territory_state_id: d08.id, name: "Katueté" },
        {territory_state_id: d08.id, name: "La Paloma" },
        {territory_state_id: d08.id, name: "Nueva Esperanza" },
        {territory_state_id: d08.id, name: "Salto del Guairá" },
        {territory_state_id: d08.id, name: "Villa Ygatimí" },
        {territory_state_id: d08.id, name: "Yasy Cañy" },
        {territory_state_id: d08.id, name: "Ybyrarovaná" },
        {territory_state_id: d08.id, name: "Ypejhú" },
        {territory_state_id: d08.id, name: "Yby Pytá" },

        # 9	Central	19
        {territory_state_id: d09.id, name: "Areguá" },
        {territory_state_id: d09.id, name: "Capiatá" },
        {territory_state_id: d09.id, name: "Fernando de la Mora" },
        {territory_state_id: d09.id, name: "Guarambaré" },
        {territory_state_id: d09.id, name: "Itá" },
        {territory_state_id: d09.id, name: "Itauguá" },
        {territory_state_id: d09.id, name: "Julián Augusto Saldívar" },
        {territory_state_id: d09.id, name: "Lambaré" },
        {territory_state_id: d09.id, name: "Limpio" },
        {territory_state_id: d09.id, name: "Luque" },
        {territory_state_id: d09.id, name: "Mariano Roque Alonso" },
        {territory_state_id: d09.id, name: "Ñemby" },
        {territory_state_id: d09.id, name: "Nueva Italia" },
        {territory_state_id: d09.id, name: "San Antonio" },
        {territory_state_id: d09.id, name: "San Lorenzo" },
        {territory_state_id: d09.id, name: "Villa Elisa" },
        {territory_state_id: d09.id, name: "Villeta" },
        {territory_state_id: d09.id, name: "Ypacaraí" },
        {territory_state_id: d09.id, name: "Ypané" },

        # 10	Concepción	11
        {territory_state_id: d10.id, name: "Azotey" },
        {territory_state_id: d10.id, name: "Belén" },
        {territory_state_id: d10.id, name: "Concepción" },
        {territory_state_id: d10.id, name: "Horqueta" },
        {territory_state_id: d10.id, name: "Loreto" },
        {territory_state_id: d10.id, name: "San Carlos del Apa" },
        {territory_state_id: d10.id, name: "San Lázaro" },
        {territory_state_id: d10.id, name: "Yby Yaú" },
        {territory_state_id: d10.id, name: "Sargento José Félix López" },
        {territory_state_id: d10.id, name: "San Alfredo" },
        {territory_state_id: d10.id, name: "Paso Barreto" },

        # 11	Cordillera	20
        {territory_state_id: d11.id, name: "Altos" },
        {territory_state_id: d11.id, name: "Arroyos y Esteros" },
        {territory_state_id: d11.id, name: "Atyrá" },
        {territory_state_id: d11.id, name: "Caacupé" },
        {territory_state_id: d11.id, name: "Caraguatay" },
        {territory_state_id: d11.id, name: "Emboscada" },
        {territory_state_id: d11.id, name: "Eusebio Ayala" },
        {territory_state_id: d11.id, name: "Isla Pucú" },
        {territory_state_id: d11.id, name: "Itacurubí de la Cordillera" },
        {territory_state_id: d11.id, name: "Juan de Mena" },
        {territory_state_id: d11.id, name: "Loma Grande" },
        {territory_state_id: d11.id, name: "Mbocayaty del Yhaguy" },
        {territory_state_id: d11.id, name: "Nueva Colombia" },
        {territory_state_id: d11.id, name: "Piribebuy" },
        {territory_state_id: d11.id, name: "Primero de Marzo" },
        {territory_state_id: d11.id, name: "San Bernardino" },
        {territory_state_id: d11.id, name: "San José Obrero" },
        {territory_state_id: d11.id, name: "Santa Elena" },
        {territory_state_id: d11.id, name: "Tobatí" },
        {territory_state_id: d11.id, name: "Valenzuela" },

        # 12	Guairá	18
        {territory_state_id: d12.id, name: "Borja" },
        {territory_state_id: d12.id, name: "Colonia Independencia" },
        {territory_state_id: d12.id, name: "Coronel Martínez" },
        {territory_state_id: d12.id, name: "Dr. Bottrell" },
        {territory_state_id: d12.id, name: "Fassardi" },
        {territory_state_id: d12.id, name: "Félix Pérez Cardozo" },
        {territory_state_id: d12.id, name: "Garay" },
        {territory_state_id: d12.id, name: "Itapé" },
        {territory_state_id: d12.id, name: "Iturbe" },
        {territory_state_id: d12.id, name: "Mbocayaty" },
        {territory_state_id: d12.id, name: "Natalicio Talavera" },
        {territory_state_id: d12.id, name: "Ñumí" },
        {territory_state_id: d12.id, name: "Paso Yobái" },
        {territory_state_id: d12.id, name: "San Salvador" },
        {territory_state_id: d12.id, name: "Tebicuary" },
        {territory_state_id: d12.id, name: "Troche" },
        {territory_state_id: d12.id, name: "Villarrica" },
        {territory_state_id: d12.id, name: "Yataity" },

        # 13	Itapúa	30
        {territory_state_id: d13.id, name: "Alto Verá" },
        {territory_state_id: d13.id, name: "Bella Vista" },
        {territory_state_id: d13.id, name: "Cambyretá" },
        {territory_state_id: d13.id, name: "Capitán Meza" },
        {territory_state_id: d13.id, name: "Capitán Miranda" },
        {territory_state_id: d13.id, name: "Carlos Antonio López" },
        {territory_state_id: d13.id, name: "Carmen del Paraná" },
        {territory_state_id: d13.id, name: "Coronel Bogado" },
        {territory_state_id: d13.id, name: "Edelira" },
        {territory_state_id: d13.id, name: "Encarnación" },
        {territory_state_id: d13.id, name: "Fram" },
        {territory_state_id: d13.id, name: "General Artigas" },
        {territory_state_id: d13.id, name: "General Delgado" },
        {territory_state_id: d13.id, name: "Hohenau" },
        {territory_state_id: d13.id, name: "Itapúa Poty" },
        {territory_state_id: d13.id, name: "Jesús" },
        {territory_state_id: d13.id, name: "La Paz" },
        {territory_state_id: d13.id, name: "José Leandro Oviedo" },
        {territory_state_id: d13.id, name: "Mayor Otaño" },
        {territory_state_id: d13.id, name: "Natalio" },
        {territory_state_id: d13.id, name: "Nueva Alborada" },
        {territory_state_id: d13.id, name: "Obligado" },
        {territory_state_id: d13.id, name: "Pirapó" },
        {territory_state_id: d13.id, name: "San Cosme y Damián" },
        {territory_state_id: d13.id, name: "San Juan del Paraná" },
        {territory_state_id: d13.id, name: "San Pedro del Paraná" },
        {territory_state_id: d13.id, name: "San Rafael del Paraná" },
        {territory_state_id: d13.id, name: "Tomás Romero Pereira" },
        {territory_state_id: d13.id, name: "Trinidad" },
        {territory_state_id: d13.id, name: "Yatytay" },

        # 14	Misiones	10
        {territory_state_id: d14.id, name: "Ayolas" },
        {territory_state_id: d14.id, name: "San Ignacio" },
        {territory_state_id: d14.id, name: "San Juan Bautista." },
        {territory_state_id: d14.id, name: "San Miguel" },
        {territory_state_id: d14.id, name: "San Patricio" },
        {territory_state_id: d14.id, name: "Santa María" },
        {territory_state_id: d14.id, name: "Santa Rosa" },
        {territory_state_id: d14.id, name: "Santiago" },
        {territory_state_id: d14.id, name: "Villa Florida" },
        {territory_state_id: d14.id, name: "Yabebyry" },

        # 15	Ñeembucú	16
        {territory_state_id: d15.id, name: "Alberdi" },
        {territory_state_id: d15.id, name: "Cerrito" },
        {territory_state_id: d15.id, name: "Desmochados" },
        {territory_state_id: d15.id, name: "General José Eduvigis Díaz" },
        {territory_state_id: d15.id, name: "Guazú Cuá" },
        {territory_state_id: d15.id, name: "Humaitá" },
        {territory_state_id: d15.id, name: "Isla Umbú" },
        {territory_state_id: d15.id, name: "Laureles" },
        {territory_state_id: d15.id, name: "Mayor José J. Martínez" },
        {territory_state_id: d15.id, name: "Paso de Patria" },
        {territory_state_id: d15.id, name: "Pilar" },
        {territory_state_id: d15.id, name: "San Juan Bautista del Ñeembucú" },
        {territory_state_id: d15.id, name: "Tacuaras" },
        {territory_state_id: d15.id, name: "Villa Franca" },
        {territory_state_id: d15.id, name: "Villalbín" },
        {territory_state_id: d15.id, name: "Villa Oliva" },

        # 16	Paraguarí	17
        {territory_state_id: d16.id, name: "Acahay" },
        {territory_state_id: d16.id, name: "Caapucú" },
        {territory_state_id: d16.id, name: "Carapeguá" },
        {territory_state_id: d16.id, name: "Escobar" },
        {territory_state_id: d16.id, name: "Gral. Bernardino Caballero" },
        {territory_state_id: d16.id, name: "La Colmena" },
        {territory_state_id: d16.id, name: "Mbuyapey" },
        {territory_state_id: d16.id, name: "Paraguarí" },
        {territory_state_id: d16.id, name: "Pirayú" },
        {territory_state_id: d16.id, name: "Quiindy" },
        {territory_state_id: d16.id, name: "Quyquyhó" },
        {territory_state_id: d16.id, name: "San Roque González de Santa Cruz" },
        {territory_state_id: d16.id, name: "Sapucai" },
        {territory_state_id: d16.id, name: "Tebicuarymí" },
        {territory_state_id: d16.id, name: "Yaguarón" },
        {territory_state_id: d16.id, name: "Ybycuí" },
        {territory_state_id: d16.id, name: "Ybytymí" },

        # 17	Presidente Hayes	8
        {territory_state_id: d17.id, name: "Benjamín Aceval" },
        {territory_state_id: d17.id, name: "Dr. José Falcón" },
        {territory_state_id: d17.id, name: "General José María Bruguez" },
        {territory_state_id: d17.id, name: "Nanawa" },
        {territory_state_id: d17.id, name: "Puerto Pinasco" },
        {territory_state_id: d17.id, name: "Tte. Irala Fernández" },
        {territory_state_id: d17.id, name: "Tte. Esteban Martínez" },
        {territory_state_id: d17.id, name: "Villa Hayes" },

        # 18	San Pedro	20
        {territory_state_id: d18.id, name: "Antequera" },
        {territory_state_id: d18.id, name: "Capiibary" },
        {territory_state_id: d18.id, name: "Choré" },
        {territory_state_id: d18.id, name: "General Elizardo Aquino" },
        {territory_state_id: d18.id, name: "General Isidoro Resquín" },
        {territory_state_id: d18.id, name: "Guayaibí" },
        {territory_state_id: d18.id, name: "Itacurubí del Rosario" },
        {territory_state_id: d18.id, name: "Liberación" },
        {territory_state_id: d18.id, name: "Lima" },
        {territory_state_id: d18.id, name: "Nueva Germania" },
        {territory_state_id: d18.id, name: "San Estanislao" },
        {territory_state_id: d18.id, name: "San Pablo" },
        {territory_state_id: d18.id, name: "San Pedro" },
        {territory_state_id: d18.id, name: "Santa Rosa del Aguaray" },
        {territory_state_id: d18.id, name: "Tacuatí" },
        {territory_state_id: d18.id, name: "Unión" },
        {territory_state_id: d18.id, name: "Veinticinco de Diciembre" },
        {territory_state_id: d18.id, name: "Villa del Rosario" },
        {territory_state_id: d18.id, name: "Yataity del Norte" },
        {territory_state_id: d18.id, name: "Yrybucuá" }
    ].each do |state_and_cities|
      Territory::City.create(state_and_cities)
    end
  end

  def down
    paraguay = Territory::Country.find_by(name: 'Paraguay')
    paraguay.territory_states.destroy_all
  end
end
