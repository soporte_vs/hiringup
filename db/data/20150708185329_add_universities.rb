# coding: utf-8
class AddUniversities < SeedMigration::Migration
  def up
    [
      { name: "Pontificia Universidad Católica de Chile" },
      { name: "Pontificia Universidad Católica de Valparaíso" },
      { name: "Universidad Adolfo Ibáñez" },
      { name: "Universidad Academia de Humanismo Cristiano" },
      { name: "Universidad Adventista de Chile" },
      { name: "Universidad Alberto Hurtado" },
      { name: "Universidad Andrés Bello" },
      { name: "Universidad Arturo Prat" },
      { name: "Universidad Austral de Chile" },
      { name: "Universidad Autonóma de Chile (ex. Universidad Autónoma del Sur)" },
      { name: "Universidad Bernardo O'Higgins" },
      { name: "Universidad Bolivariana" },
      { name: "Universidad Católica Cardenal Raúl Silva Henríquez" },
      { name: "Universidad Católica de la Santísima Concepción" },
      { name: "Universidad Católica de Temuco" },
      { name: "Universidad Católica del Maule" },
      { name: "Universidad Católica del Norte Ex U. del Norte (1956)" },
      { name: "Universidad Central de Chile" },
      { name: "Universidad Chileno Británica de Cultura" },
      { name: "Universidad de Aconcagua" },
      { name: "Universidad de Antofagasta" },
      { name: "Universidad de Arte y Ciencias Sociales Arcis" },
      { name: "Universidad de Artes, Ciencias y Comunicación Uniacc" },
      { name: "Universidad de Atacama" },
      { name: "Universidad de Chile" },
      { name: "Universidad de Ciencias de la Informática UCINF" },
      { name: "Universidad de Concepción" },
      { name: "Universidad de La Frontera" },
      { name: "Universidad de La Serena" },
      { name: "Universidad de Las Américas" },
      { name: "Universidad de Los Andes" },
      { name: "Universidad de Los Lagos" },
      { name: "Universidad de Magallanes" },
      { name: "Universidad de Playa Ancha de Ciencias de la Educación" },
      { name: "Universidad de Santiago de Chile Ex U. Técnica del Estado (1947)" },
      { name: "Universidad de Talca" },
      { name: "Universidad de Tarapacá" },
      { name: "Universidad de Valparaíso" },
      { name: "Universidad de Viña del Mar" },
      { name: "Universidad del Bío-Bío" },
      { name: "Universidad del Desarrollo" },
      { name: "Universidad del Pacífico" },
      { name: "Universidad Diego Portales" },
      { name: "Universidad Finis Terrae" },
      { name: "Universidad Gabriela Mistral" },
      { name: "Universidad Iberoamericana de Ciencias y Tecnología, UNICYT" },
      { name: "Universidad Internacional Sek" },
      { name: "Universidad La Araucana" },
      { name: "Universidad La República" },
      { name: "Universidad Los Leones (ex Universidad Marítima de Chile)" },
      { name: "Universidad Mayor" },
      { name: "Universidad Metropolitana de Ciencias de la Educación" },
      { name: "Universidad Miguel de Cervantes" },
      { name: "Universidad Pedro de Valdivia (ex Mariano Egaña)" },
      { name: "Universidad San Sebastián" },
      { name: "Universidad Santo Tomás" },
      { name: "Universidad Técnica Federico Santa María" },
      { name: "Universidad Técnológica de Chile INACAP (ex U. Tecnológica V.P.R.)" },
      { name: "Universidad Tecnológica Metropolitana Ex I.P. de Santiago (1981)" }
    ].each do|uni|
      Education::University.create(uni)
    end
  end

  def down
    Education::University.destroy_all
  end
end
