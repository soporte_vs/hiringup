# coding: utf-8
class AddProfessionalInstitutes < SeedMigration::Migration
  def up
    [
      { name: "Instituto Profesional Adventista" },
      { name: "Instituto Profesional Agrario Adolfo Matthei" },
      { name: "Instituto Profesional Aiep" },
      { name: "Instituto Profesional Alemán Wilhelm Von Humboldt" },
      { name: "Instituto Profesional Alpes" },
      { name: "Instituto Profesional Arturo Prat (ex ECACEC)" },
      { name: "Instituto Profesional CAMPUS" },
      { name: "Instituto Profesional Carlos Casanueva" },
      { name: "Instituto Profesional CENAFOM" },
      { name: "Instituto Profesional Chileno Norteamericano" },
      { name: "Instituto Profesional Chileno-Britanico de Cultura" },
      { name: "Instituto Profesional CIISA" },
      { name: "Instituto Profesional de Arte y Comunicación ARCOS" },
      { name: "Instituto Profesional de Chile (ex San Bartolomé)" },
      { name: "Instituto Profesional de Ciencias de la Computación Acuario Data" },
      { name: "Instituto Profesional de Ciencias y Artes INCACEA" },
      { name: "Instituto Profesional de Ciencias y Educación Helen Keller" },
      { name: "Instituto Profesional de ENAC" },
      { name: "Instituto Profesional de Los Ángeles" },
      { name: "Instituto Profesional de Providencia" },
      { name: "Instituto Profesional del Valle Central (ex I.P. del Maule)" },
      { name: "Instituto Profesional Diego Portales" },
      { name: "Instituto Profesional Dr. Virginio Gómez" },
      { name: "Instituto Profesional DUOC UC" },
      { name: "Instituto Profesional EATRI" },
      { name: "Instituto Profesional Escuela de Cine de Chile" },
      { name: "Instituto Profesional Escuela de Contadores Auditores de Santiago" },
      { name: "Instituto Profesional Escuela Moderna de Música" },
      { name: "Instituto Profesional Escuela Nacional de Relaciones Públicas" },
      { name: "Instituto Profesional Esucomex" },
      { name: "Instituto Profesional Hogar Catequístico" },
      { name: "Instituto Profesional Inacap" },
      { name: "Instituto Profesional Instituto de Estudios Bancarios Guillermo Subercaseaux" },
      { name: "Instituto Profesional Instituto Internacional de Artes Culinarias y Servicios" },
      { name: "Instituto Profesional Instituto Nacional del Fútbol" },
      { name: "Instituto Profesional Instituto Superior de Artes y Ciencias de la Comunicación" },
      { name: "Instituto Profesional La Araucana" },
      { name: "Instituto Profesional Latinoamericano de Comercio Exterior" },
      { name: "Instituto Profesional Libertador de Los Andes" },
      { name: "Instituto Profesional Los Lagos (ex IP de Concepción)" },
      { name: "Instituto Profesional Los Leones" },
      { name: "Instituto Profesional Luis Galdames" },
      { name: "Instituto Profesional Mar Futuro" },
      { name: "Instituto Profesional PROJAZZ" },
      { name: "Instituto Profesional Santo Tomás" },
      { name: "Instituto Profesional Teatro la Casa" },
      { name: "Instituto Profesional Vertical Instituto Profesional" }
    ].each do|pi|
      Education::ProfessionalInstitute.create(pi)
    end
  end

  def down
    Education::ProfessionalInstitute.destroy_all
  end
end
