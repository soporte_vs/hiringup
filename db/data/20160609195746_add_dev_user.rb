class AddDevUser < SeedMigration::Migration
  def up
    user = User.new(email: "integraerror@valposystems.cl", password: "hdDDZ*n8jX")

    if user.save
      puts "\033[32m#{User.model_name.human} #{user.email} creado con exito\033[0m"
      user.add_role :admin
      puts "\033[32m#{User.model_name.human} #{user.email} tiene el rol admin\033[0m" if user.has_role? :admin
    else
      puts "\tErrors => \033[31m#{user.errors.full_messages}\033[0m"
    end
  end

  def down
    user = User.find_by(email: "integraerror@valposystems.cl")
    if user.present?
      user.delete
    end
  end
end
