class AddRecruitmentSources < SeedMigration::Migration
  def up
    recruitment_sources = [
      {name: "Facebook"},
      {name: "Afiches y Folletos"},
      {name: "Diarios"},
      {name: "Radios"},
      {name: "Trabajando.com"},
      {name: "Página Web Integra"},
      {name: "Linkedin"},
      {name: "Me lo comentó un amigo/a, familiar o colega"}
    ]


    recruitment_sources.each do |recruitment|
      source = Company::RecruitmentSource.find_or_create_by(recruitment)
    end
  end

  def down
    Company::RecruitmentSource.delete_all
  end
end
