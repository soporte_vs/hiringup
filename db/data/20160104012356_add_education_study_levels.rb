class AddEducationStudyLevels < SeedMigration::Migration
  def up
    [
      { name: "Congelado"},
      { name: "Egresado"},
      { name: "En curso"},
      { name: "Incompleta"},
      { name: "Titulado"}
    ].each do |types|
      Education::StudyLevel.where(types).first_or_create
    end
  end

  def down
    Education::StudyLevel.destroy_all
  end
end
