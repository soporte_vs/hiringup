class AddDocumentDescriptorAndGroup < SeedMigration::Migration
  def up
    Document::Descriptor.reset_column_information
    [
      {name:"otro_documento",	details:"Otro documento",	allowed_extensions: nil,	internal: nil,	only_onboarding: false},
      {name:"cv",	details:"Currículo Vitae actualizado",	allowed_extensions: "pdf",	internal: nil,	only_onboarding: false},
      {name:"situación_militar",	details:"Certificado de situación militar (varones mayores de 18 años)",	allowed_extensions: "",	internal: false,	only_onboarding: true},
      {name:"Último_finiquito",	details:   "Finiquito del último empleador (fotocopia), si no cuenta con uno, reemplazar por declaración jurada",	allowed_extensions: "",	internal: false,	only_onboarding: true},
      {name:"isapre_o_fonasa",	details:"Certificado de afiliación a Fonasa o Isapre ",	allowed_extensions: "",	internal: false,	only_onboarding: true},
      {name:"certificado_de_inhabilidades",	details:"Certificado de Inhabilidades",	allowed_extensions: "",	internal: true,	only_onboarding: false},
      {name:"carta_de_solicitud_de_pr_ctica_y_seguro_escolar",	details:"Carta de solicitud de práctica y Seguro escolar.",	allowed_extensions: "",	internal: false,	only_onboarding: true},
      {name:"programa_de_pr_ctica__debe_incluir_como_m_nimo_objetivo_general__objetivos_espec_ficos__duraci_n__metodolog_a__reportes_y_evaluaci_n_",	details:   "Programa de práctica (debe incluir como mínimo objetivo general, objetivos específicos, duración, metodología, reportes y evaluación)",	allowed_extensions: "",	internal: false,	only_onboarding: true},
      {name:"afp",	details:"Certificado de afiliación AFP.",	allowed_extensions: "",	internal: false,	only_onboarding: true},
      {name:"ci",	details:"Cedula de identidad (copia por ambos lados)",	allowed_extensions: "",	internal: false,	only_onboarding: true},
      {name:"certificado_antecedentes",	details:   "Certificado de antecedentes original y de preferencia de emisión inferior a 30 días (*)",	allowed_extensions: "",	internal: false,	only_onboarding: true},
      {name:"número_cuenta_banco",	details:"Fotocopia de nº cuenta de banco  para incorporación a convenio",	allowed_extensions: "",	internal: false,	only_onboarding: true},
      {name:"ficha_personal_estudiante_en_pr_ctica",	details:"Ficha Personal estudiante en práctica",	allowed_extensions: "",	internal: false,	only_onboarding: true},
      {name:"fotos_tamaño_carne",	details:"Dos fotos tamaño carnet (*)",	allowed_extensions: "",	internal: false,	only_onboarding: true},
      {name:"certificado_residencia",	details:"Certificado de residencia original y de emisión reciente (*) ",	allowed_extensions: "",	internal: false,	only_onboarding: true},
      {name:"ficha_de_antecedentes_personales",	details:"Ficha de antecedentes personales",	allowed_extensions: "",	internal: false,	only_onboarding: true},
      {name:"certificado_de_estudios",	details:   "Certificado de estudios o título profesional, original o fotocopia legalizada (*)",	allowed_extensions: "",	internal: false,	only_onboarding: false},
      {name:"informe_social__traslado_",	details:"Informe Social Traslado (Solo para trabajadores de Integra)",	allowed_extensions: "",	internal: false,	only_onboarding: false}
      #{name:"internal_postulation_template",	details:   "Ficha de postulación Promoción y Traslado (Solo para trabajadores de Integra)",	allowed_extensions: "",	internal: false,	only_onboarding: false} #Se crea en la migración db/migrate/20160129152815_migrate_internal_postulation.rb
    ].each do |descriptor|
      document_descriptor = Document::Descriptor.new(descriptor)
      unless document_descriptor.save
        puts "\tErrors => \033[31m#{document_descriptor.errors.full_messages} #{document_descriptor.name}\033[0m"
      end
    end

    [
      { name: "Prácticas",
        descriptors:
          [
            "cv",
            "carta_de_solicitud_de_pr_ctica_y_seguro_escolar",
            "programa_de_pr_ctica__debe_incluir_como_m_nimo_objetivo_general__objetivos_espec_ficos__duraci_n__metodolog_a__reportes_y_evaluaci_n_",
            "ci",
            "certificado_antecedentes",
            "ficha_personal_estudiante_en_pr_ctica",
            "certificado_residencia"
          ]
      },
      { name: "Estandar",
        descriptors:
          [
            "cv",
            "situación_militar",
            "Último_finiquito",
            "isapre_o_fonasa",
            "afp",
            "ci",
            "certificado_antecedentes",
            "número_cuenta_banco",
            "fotos_tamaño_carne",
            "certificado_residencia",
            "ficha_de_antecedentes_personales",
            "certificado_de_estudios"
          ]
      },
      { name: "Promociones y Traslados",
        descriptors:
          [
            "certificado_de_estudios",
            "informe_social__traslado_",
            "internal_postulation_template"
          ]
      }
    ].each do |group|
      document_group = Document::Group.new(name: group[:name])

      if document_group.save
       descriptors = Document::Descriptor.where(name: group[:descriptors])

       descriptors.each do |desc|
        Document::DescriptorMembership.create(document_descriptor: desc, document_group: document_group)
       end
      end
    end


  end

  def down
    Document::DescriptorMembership.destroy_all
    Document::Group.destroy_all
    Document::Descriptor.destroy_all
  end
end
