class AddGroupsToGroup < SeedMigration::Migration
  def up
    all_data = [
      { region: "Tarapaca", name: "Cindy Michell Ithal Alvarez", rut: "13887599-7",  email: "cithal@integra.cl" },
      { region: "Tarapaca", name: "Juan Pablo Araneda Inostroza", rut: "13866784-7",  email: "jaraneda@integra.cl" },
      { region: "Tarapaca", name: "Carolina Andrea Melin Navarro", rut: "13172319-9",  email: "cmelin@integra.cl" },
      { region: "Antofagasta", name: "Camila Beatriz Rey Ramírez", rut: "19104884-9",  email: "crey@integra.cl" },
      { region: "Antofagasta", name: "América Gabriela Lillo Olivares", rut: "16134361-7",  email: "alillo@integra.cl" },
      { region: "Antofagasta", name: "Daniela Paz Monsalve Olivares", rut: "15357924-5",  email: "dmonsalve@integra.cl" },
      { region: "Antofagasta", name: "Yinia Fritis Peña", rut: "13217704-k",  email: "yfritis@integra.cl" },
      { region: "Antofagasta", name: "Ana Veronica Degioanni", rut: "14552711-2",  email: "adegioanni@integra.cl" },
      { region: "Atacama", name: "FLAVIA PAULETTE BLANCO DIAZ", rut: "16834255-1",  email: "fblanco@integra.cl" },
      { region: "Atacama", name: "TANIA ALEJANDRA MARTINEZ HIDALGO", rut: "17133603-1",  email: "tmartinez@integra.cl" },
      { region: "Atacama", name: "CLARA SIDNEY MURCIA VARGAS", rut: "24976765-4",  email: "cmurcia@integra.cl" },
      { region: "Coquimbo", name: "Camila Isabel Varas Castillo", rut: "15501730-9",  email: "cvaras@integra.cl" },
      { region: "Coquimbo", name: "Sarah Angela Bravo Muñoz", rut: "12874324-3",  email: "sbravo@integra.cl" },
      { region: "Coquimbo", name: "Hugo Enrique Rojas Rodriguez", rut: "16580200-4",  email: "hrojas@integra.cl" },
      { region: "Coquimbo", name: "Oriana Yamile Rojo Ardiles", rut: "8102701-3",  email: "orojo@integra.cl" },
      { region: "Coquimbo", name: "Paulina Maribel Muñoz Muñoz", rut: "16054413-9",  email: "pmunozm@integra.cl" },
      { region: "Coquimbo", name: "Susana Gumucio Sepulveda ", rut: "9683996-0",  email: "sgumucio@integra.cl" },
      { region: "Valparaiso", name: "Maria José Fuentes Lopez", rut: "16775542-9",  email: "mfuentes@integra.cl" },
      { region: "Valparaiso", name: "Maria José Varela Quiroz", rut: "17624542-5",  email: "mvarela@integra.cl" },
      { region: "Valparaiso", name: "Anita Monserrat Santa Maria Castro", rut: "18781561-4",  email: "asantamaria@integra.cl" },
      { region: "Valparaiso", name: "Karen Judith Alvarez Toro", rut: "15740269-2",  email: "kalvarez@integra.cl" },
      { region: "Valparaiso", name: "Maritza Margarita Nuñez Daza", rut: "12159048-4",  email: "mnunezd@integra.cl" },
      { region: "Valparaiso", name: "Karina Cecilia Muñoz Beck", rut: "15285594-k",  email: "kcmunoz@integra.cl" },
      { region: "Valparaiso", name: "Roberta Donoso Cortázar", rut: "15491208-8",  email: "rdonoso@integra.cl" },
      { region: "O Higgins", name: "María Fernanda Madrid Arredondo", rut: "16554421-8",  email: "mmadrid@integra.cl" },
      { region: "O Higgins", name: "Carlos Fernando  Riquelme Videla  ", rut: "16322439-9",  email: "criquelmev@integra.cl" },
      { region: "O Higgins", name: "Pamela Alejandra Leal Faundez", rut: "17685049-3",  email: "pleal@integra.cl" },
      { region: "O Higgins", name: "Melissa del Carmen Flores Muñoz", rut: "16298361-k",  email: "mfloresm@integra.cl" },
      { region: "O Higgins", name: "Karina Nelly Sabelle Fuentes", rut: "17502800-5",  email: "ksabelle@integra.cl" },
      { region: "O Higgins", name: "Karina Elizabeth Muñoz Silva", rut: "15523943-3",  email: "kmunoz@integra.cl" },
      { region: "Maule", name: "Paulina Andrea Medina Hurtado", rut: "16024590-5",  email: "pmedinah@integra.cl" },
      { region: "Maule", name: "Fernando Sebastián Bochard Bustos", rut: "16998628-2",  email: "fbochard@integra.cl" },
      { region: "Maule", name: "Fernando Andrés Carreño Morales", rut: "16455283-7",  email: "fcarreno@integra.cl" },
      { region: "Maule", name: "Valeska Ayala Nuñez", rut: "15352144-1",  email: "vayala@integra.cl" },
      { region: "Maule", name: "Veronica Leal Faundez ", rut: "16298789-5",  email: "vleal@integra.cl " },
      { region: "Bio Bio", name: "Alejandra Elena Meriño Tapia", rut: "15069520-1",  email: "amerino@integra.cl" },
      { region: "Bio Bio", name: "Camilo Andrés Herrera Bustos       ", rut: "17614952-3",  email: "cherrerab@integra.cl" },
      { region: "Bio Bio", name: "Vanessa Alejandra Espinoza Sobarzo", rut: "15179679-6",  email: "vespinoza@integra.cl" },
      { region: "Bio Bio", name: "Diego Andrés García Alquinta", rut: "15616041-5",  email: "dagarcia@integra.cl" },
      { region: "Bio Bio", name: "Paola Soledad Victoriano Araneda", rut: "15626868-2",  email: "pvictoriano@integra.cl" },
      { region: "Bio Bio", name: "Nicole Jeanette Ruiz Cúneo", rut: "16726794-7",  email: "nruiz@integra.cl" },
      { region: "Bio Bio", name: "Nicolás Alejandro Rocha Rencoret", rut: "15641661-4",  email: "nrocha@integra.cl" },
      { region: "Araucanía", name: "KAREN MABEL LAURIE FRIZ", rut: "16996606-0",  email: "klaurie@integra.cl" },
      { region: "Araucanía", name: "EMA IVETTE CASTILLO SANHUEZA", rut: "16950443-1",  email: "ecastillos@integra.cl" },
      { region: "Araucanía", name: "EDUARDO MUÑOZ NAVARRETE", rut: "16364645-5",  email: "emunoz@integra.cl" },
      { region: "Araucanía", name: "JUAN GONZALO PINTO ALLER", rut: "17726571-3",  email: "jpintoa@integra.cl" },
      { region: "Araucanía", name: "ANA LUISA CARDENAS SEPULVEDA", rut: "11687106-8",  email: "acardenas@integra.cl" },
      { region: "Araucanía", name: "CAMILO IGNACIO WAGHORN SANHUEZA", rut: "16637470-7",  email: "cwaghorn@integra.cl" },
      { region: "Araucanía", name: "MARCO ANTONIO SILVA CORNEJO", rut: "9838742-0",  email: "msilvac@integra.cl" },
      { region: "Los Lagos", name: "Silvia Rosemarie Robles Delgado ", rut: "16994406-7",  email: "srobles@integra.cl" },
      { region: "Los Lagos", name: "Maria Carolina Vivanco Borquez", rut: "13587676-3",  email: "mvivanco@integra.cl" },
      { region: "Los Lagos", name: "Gladys Carolina Recabal Lara", rut: "15677298-4",  email: "grecabal@integra.cl" },
      { region: "Los Lagos", name: "Carlos Andres Sepulveda Urrutia", rut: "15162756-0",  email: "csepulvedau@integra.cl" },
      { region: "Los Lagos", name: "Claudia Miriam  Almonacid Nicloux ", rut: "13968103-7",  email: "calmonacid@integra.cl" },
      { region: "Los Lagos", name: "Viviana de Lourdes Gonzalez Guaiquil", rut: "16237115-0",  email: "vgonzalezg@integra.cl" },
      { region: "Los Lagos", name: "Victor Hugo Riquelme Mellado", rut: "15592065-3",  email: "vriquelme@integra.cl" },
      { region: "Aysén", name: "Natalia Trinidad Bravo Rebolledo", rut: "16751595-9",  email: "nbravor@integra.cl" },
      { region: "Aysén", name: "Paola Roxana Medina Garcia", rut: "9238169-2",  email: "pmedina@integra.cl" },
      { region: "Magallanes", name: "Marguiana Paz Vidal Inostroza", rut: "10601562-7",  email: "mvidal@integra.cl" },
      { region: "Magallanes", name: "Carol Vladilo Salinas", rut: "14228604-1",  email: "cvladilo@integra.cl" },
      { region: "SurOriente", name: "Roxana Francesca Aránguiz Palacios", rut: "16418609-1",  email: "raranguiz@integra.cl" },
      { region: "SurOriente", name: "Ruben Arturo Reyes Sepulveda", rut: "16532783-7",  email: "rreyes@integra.cl" },
      { region: "SurOriente", name: "Evelyn Catalina Vergara Gutierrez", rut: "16719992-5",  email: "evergara@integra.cl" },
      { region: "SurOriente", name: "Juan Andres Contreras Lisboa", rut: "16913127-9",  email: "jcontrerasl@integra.cl" },
      { region: "SurOriente", name: "Carolina Andrea Salas Salas", rut: "17059173-9",  email: "csalas@integra.cl" },
      { region: "SurOriente", name: "Diego Agustin Campos Villablanca", rut: "16880548-9",  email: "dcampos@integra.cl" },
      { region: "SurOriente", name: "Consuelo Ferrada Nuñez", rut: "13935739-6",  email: "cferradan@integra.cl" },
      { region: "NorPoniente", name: "Carmen Mónica Lagos Letelier", rut: "10929058-0",  email: "clagos@integra.cl" },
      { region: "NorPoniente", name: "Jorge Andres Salas Rodriguez", rut: "13472680-6",  email: "jsalas@integra.cl" },
      { region: "NorPoniente", name: "Carolina Andrea Rivas Salgado", rut: "14123695-4",  email: "carivas@integra.cl" },
      { region: "NorPoniente", name: "Patricio Alejandro Orellana Toloza", rut: "15633550-9",  email: "porellana@integra.cl" },
      { region: "NorPoniente", name: "Adela del Carmen Trigo Olivares", rut: "9920299-8",  email: "atrigo@integra.cl" },
      { region: "NorPoniente", name: "Nataly Victoria Guzmán Pinto", rut: "16714872-7",  email: "nguzman@integra.cl" },
      { region: "NorPoniente", name: "María Belen Olivares Martinez", rut: "17078785-4",  email: "molivares@integra.cl" },
      { region: "NorPoniente", name: "Mauricio Alejandro Zarzar Encina", rut: "16475928-8",  email: "mzarzar@integra.cl" },
      { region: "Arica", name: "Javiera Fernanda Rivera Perez", rut: "15009248-5",  email: "jrivera@integra.cl" },
      { region: "Arica", name: "Carolina Andrea Riveros Osorio", rut: "13076184-4",  email: "cariveros@integra.cl" },
      { region: "Arica", name: "Ana Luisa Rocco Troquian", rut: "13412842-9",  email: "arocco@integra.cl" },
      { region: "Los Rios", name: "KAREN VIVIANA QUINTEROS FUENTEALBA ", rut: "16806826-3",  email: "KQUINTEROS@INTEGRA.CL" },
      { region: "Los Rios", name: "VIVIANA MAGDALENA QUINTUPRAI RIQUELME ", rut: "15576770-7",  email: "VQUINTUPRAI@INTEGRA.CL" },
      { region: "Los Rios", name: "ROCIO VALESKA VALDES GUTIERREZ ", rut: "17511503-K",  email: "RVALDES@INTEGRA.CL" },
      { region: "Los Rios", name: "CAROLINA ANDREA PARADA SEPULVEDA ", rut: "15351248-5",  email: "CPARADA@INTEGRA.CL" }
    ]
    all_data.each do |data|
      region = data[:region]
      name = data[:name]
      rut_type = People::IdentificationDocumentType.find_by(name: 'RUT')
      rut = data[:rut]
      email = data[:email].downcase
      split_name = name.split
      user_name = "#{split_name[0]} #{split_name[1]}"
      user_name = user_name.capitalize
      user_last_name = "#{split_name[2]} #{split_name[3]}"
      user_last_name = user_last_name.capitalize
      # Crear grupo
      group = Group.find_or_create_by(name: region, description: "Descripción #{region}")

      # Crear user, password es el RUT
      user = User.find_by(email: email)
      if user.nil? # Si user no existe, crear user
        user = User.create(email: email, password: 'integra.2016')

        user.add_role :admin, Applicant::Catched
        user.add_role :create, Applicant::Catched
        user.add_role :new, Applicant::Catched
        user.add_role :search, Applicant::Base
        user.add_role :update, Applicant::Base
        user.add_role :new, Course
        user.add_role :create, Course
        user.add_role :search, Course
      end

      # Agregar personal_information al user
      peopple = People::PersonalInformation.find_or_create_by(first_name: user_name, 
        last_name: user_last_name, user_id: user.id, 
        identification_document_type_id: rut_type.id, 
        identification_document_number: rut)
      unless peopple.persisted?
        error = peopple.errors.messages
        puts "RUT: #{rut} Errores: #{error}"
      end

      # Crear user group
      UserGroup.find_or_create_by(user_id: user.id, group_id: group.id)
    end
  end

  def down
    Group.destroy_all
    UserGroup.destroy_all
  end
end
