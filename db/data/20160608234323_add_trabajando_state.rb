# coding: utf-8
class AddTrabajandoState < SeedMigration::Migration
  def up
    [{:name=>"Extranjero", :hupe_state_id=>nil, :trabajando_id=>14},
     {:name=>"Metropolitana", :hupe_state_id=>1, :trabajando_id=>1},
     {:name=>"I Tarapacá ", :hupe_state_id=>3, :trabajando_id=>2},
     {:name=>"II Antofagasta ", :hupe_state_id=>4, :trabajando_id=>3},
     {:name=>"III Atacama", :hupe_state_id=>5, :trabajando_id=>4},
     {:name=>"IV Coquimbo", :hupe_state_id=>6, :trabajando_id=>5},
     {:name=>"V Valparaíso", :hupe_state_id=>7, :trabajando_id=>6},
     {:name=>"VI O'Higgins", :hupe_state_id=>8, :trabajando_id=>7},
     {:name=>"VII Maule", :hupe_state_id=>9, :trabajando_id=>8},
     {:name=>"VIII Bío Bío ", :hupe_state_id=>10, :trabajando_id=>9},
     {:name=>"IX La Araucanía", :hupe_state_id=>11, :trabajando_id=>10},
     {:name=>"X Los Lagos", :hupe_state_id=>13, :trabajando_id=>11},
     {:name=>"XI Aysén", :hupe_state_id=>14, :trabajando_id=>12},
     {:name=>"XII Magallanes y Antártica Chilena",
      :hupe_state_id=>15,
      :trabajando_id=>13},
     {:name=>"XIV Los Ríos", :hupe_state_id=>12, :trabajando_id=>472},
     {:name=>"XV Arica y Parinacota", :hupe_state_id=>2, :trabajando_id=>473}].each do |state|
      Trabajando::State.create(state)
    end
  end

  def down
    Trabajando::State.destroy_all
  end
end
