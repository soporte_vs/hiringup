class DeleteUsersWithoutInformation < SeedMigration::Migration
  require 'csv'
  def up
    ###ELIMINAR USUARIOS###
    contador = 1
    ruta_csv = 'db/data/eliminar.csv'
    CSV.foreach(ruta_csv, headers: true) do |row|
      if row.length <= 1  #Ignora posibles espacios en blancos
        user = User.find_by(email:row[0])
        if user.present?
          applicant = user.applicant
          minisite_postulations = Minisite::Postulation.where(applicant:applicant)
          trabajando_postulations = Trabajando::Postulation.where(applicant:applicant)
          course_postulations =  CoursePostulation.where(applicant:applicant)
          if minisite_postulations.any?
            minisite_postulations.each do |minisite_postulation|
              minisite_postulation.destroy
            end
          end
          if trabajando_postulations.any?
            trabajando_postulations.each do |trabajando_postulation|
              trabajando_postulation.destroy
            end
          end
          if course_postulations.any?
            course_postulations.each do |course_postulation|
              course_postulation.destroy
            end
          end
          if applicant.enrollments.any?
            applicant.enrollments.each do |enrollment|
              enrollment.destroy
            end
          end
          user.destroy
          puts "Eliminando usuario #{contador} - #{row[0]} - #{applicant.type}"
          contador +=1
        end
      end
    end
  end

  def down

  end
end
