# coding: utf-8
class AddTrabajandoApplicantProfile < SeedMigration::Migration
  def up
    Applicant::ProfileType.reset_column_information
    Trabajando::ApplicantProfile.reset_column_information

    # Primero crea los Applicant::ProfileType
    [
      "Estudios superiores (Profesional, técnico-profesional, técnico)",
      "Estudiante",
      "Oficio (no requiere estudios superiores)",
      "Indiferente"
    ].each do |pt|
      Applicant::ProfileType.find_or_create_by(name: pt)
    end

    [
      {:name=>"Estudios superiores (Profesional, técnico-profesional, técnico)",
      :trabajando_id=>1,
      :hupe_profile_type=>
       "Estudios superiores (Profesional, técnico-profesional, técnico)"},
     {:name=>"Indiferente", :trabajando_id=>4, :hupe_profile_type=>"Indiferente"},
     {:name=>"Oficio (no requiere estudios superiores)",
      :trabajando_id=>3,
      :hupe_profile_type=>"Oficio (no requiere estudios superiores)"},
     {:name=>"Estudiante", :trabajando_id=>2, :hupe_profile_type=>"Estudiante"}
    ].each do |apt|
      profile_type = Applicant::ProfileType.find_by(name: apt[:hupe_profile_type])
      trabajando_applicant_profile = Trabajando::ApplicantProfile.find_by(name: apt[:name], trabajando_id: apt[:trabajando_id])
      trabajando_applicant_profile.update(hupe_profile_type_id: profile_type.id)
    end
  end

  def down
    Trabajando::ApplicantProfile.destroy_all
    Applicant::ProfileType.destroy_all
  end
end
