class AddCostaRicaStatesAndCities < SeedMigration::Migration
  def up
    #Division politica de Bolivia, está estructurado en 9 departamentos, 112 provincias,
    costarica = Territory::Country.find_by(name: 'Costa Rica')

    d01 = Territory::State.create(name: "Alajuela", territory_country: costarica)
    d02 = Territory::State.create(name: "Cartago", territory_country: costarica)
    d03 = Territory::State.create(name: "Guanacaste", territory_country: costarica)
    d04 = Territory::State.create(name: "Heredia", territory_country: costarica)
    d05 = Territory::State.create(name: "Limón", territory_country: costarica)
    d06 = Territory::State.create(name: "Puntarenas", territory_country: costarica)
    d07 = Territory::State.create(name: "San José", territory_country: costarica)


    [
        # d01 Alajuela 15
        {territory_state_id: d01.id, name: "Alajuela" },
        {territory_state_id: d01.id, name: "San Ramón" },
        {territory_state_id: d01.id, name: "Grecia" },
        {territory_state_id: d01.id, name: "San Mateo" },
        {territory_state_id: d01.id, name: "Atenas" },
        {territory_state_id: d01.id, name: "Naranjo" },
        {territory_state_id: d01.id, name: "Palmares" },
        {territory_state_id: d01.id, name: "Poás" },
        {territory_state_id: d01.id, name: "Orotina" },
        {territory_state_id: d01.id, name: "San Carlos " },
        {territory_state_id: d01.id, name: "Zarcero" },
        {territory_state_id: d01.id, name: "Valverde Vega" },
        {territory_state_id: d01.id, name: "Upala " },
        {territory_state_id: d01.id, name: "Los Chiles " },
        {territory_state_id: d01.id, name: "Guatuso" },

        # d02  Cartago 08
        {territory_state_id: d02.id, name: "Cartago" },
        {territory_state_id: d02.id, name: "Paraíso" },
        {territory_state_id: d02.id, name: "La Unión" },
        {territory_state_id: d02.id, name: "Jiménez" },
        {territory_state_id: d02.id, name: "Turrialba " },
        {territory_state_id: d02.id, name: "Alvarado" },
        {territory_state_id: d02.id, name: "Oreamuno" },
        {territory_state_id: d02.id, name: "El Guarco" },

        # d03 Guanacaste 11
        {territory_state_id: d03.id, name: "Liberia" },
        {territory_state_id: d03.id, name: "Nicoya" },
        {territory_state_id: d03.id, name: "Santa Cruz" },
        {territory_state_id: d03.id, name: "Bagaces" },
        {territory_state_id: d03.id, name: "Carrillo" },
        {territory_state_id: d03.id, name: "Cañas" },
        {territory_state_id: d03.id, name: "Abangares " },
        {territory_state_id: d03.id, name: "Tilarán" },
        {territory_state_id: d03.id, name: "Nandayure" },
        {territory_state_id: d03.id, name: "La Cruz" },
        {territory_state_id: d03.id, name: "Hojancha" },

        # d04  Heredia 10
        {territory_state_id: d04.id, name: "Heredia" },
        {territory_state_id: d04.id, name: "Barva" },
        {territory_state_id: d04.id, name: "Santo Domingo" },
        {territory_state_id: d04.id, name: "Santa Bárbara" },
        {territory_state_id: d04.id, name: "San Rafael" },
        {territory_state_id: d04.id, name: "San Isidro" },
        {territory_state_id: d04.id, name: "Belén " },
        {territory_state_id: d04.id, name: "Flores" },
        {territory_state_id: d04.id, name: "San Pablo" },
        {territory_state_id: d04.id, name: "Sarapiquí" },

        # d05 Limón 06
        {territory_state_id: d05.id, name: "Limón" },
        {territory_state_id: d05.id, name: "Pococí" },
        {territory_state_id: d05.id, name: "Siquirres" },
        {territory_state_id: d05.id, name: "Talamanca" },
        {territory_state_id: d05.id, name: "Matina" },
        {territory_state_id: d05.id, name: "Guácimo" },

        # d06 Puntarenas 11
        {territory_state_id: d06.id, name: "Puntarenas" },
        {territory_state_id: d06.id, name: "Esparza" },
        {territory_state_id: d06.id, name: "Buenos Aires " },
        {territory_state_id: d06.id, name: "Montes de Oro" },
        {territory_state_id: d06.id, name: "Osa" },
        {territory_state_id: d06.id, name: "Quepos" },
        {territory_state_id: d06.id, name: "Golfito" },
        {territory_state_id: d06.id, name: "Coto Brus" },
        {territory_state_id: d06.id, name: "Parrita" },
        {territory_state_id: d06.id, name: "Corredores" },
        {territory_state_id: d06.id, name: "Garabito" },

        # d07 San José 19
        {territory_state_id: d07.id, name: "San José" },
        {territory_state_id: d07.id, name: "Escazú" },
        {territory_state_id: d07.id, name: "Desamparados" },
        {territory_state_id: d07.id, name: "Puriscal" },
        {territory_state_id: d07.id, name: "Tarrazú" },
        {territory_state_id: d07.id, name: "Aserrí" },
        {territory_state_id: d07.id, name: "Mora" },
        {territory_state_id: d07.id, name: "Goicoechea" },
        {territory_state_id: d07.id, name: "Santa Ana" },
        {territory_state_id: d07.id, name: "Alajuelita" },
        {territory_state_id: d07.id, name: "Acosta " },
        {territory_state_id: d07.id, name: "Tibás" },
        {territory_state_id: d07.id, name: "Moravia" },
        {territory_state_id: d07.id, name: "Montes de Oca" },
        {territory_state_id: d07.id, name: "Turrubares " },
        {territory_state_id: d07.id, name: "Dota " },
        {territory_state_id: d07.id, name: "Curridabat" },
        {territory_state_id: d07.id, name: "Pérez Zeledón" },
        {territory_state_id: d07.id, name: "León Cortés " }

    ].each do |state_and_cities|
      Territory::City.create(state_and_cities)
    end
  end

  def down
    costarica = Territory::Country.find_by(name: 'Costa Rica')
    costarica.territory_states.destroy_all
  end
end
