class AddPeruStatesAndCities < SeedMigration::Migration
  def up
    peru = Territory::Country.find_by(name: 'Perú')
     # Perú 24 departamentos, 1 provincia constitucional, 1 provincia
     d01 = Territory::State.create(name: "Amazonas", territory_country: peru)
     d02 = Territory::State.create(name: "Áncash", territory_country: peru)
     d03 = Territory::State.create(name: "Apurímac", territory_country: peru)
     d04 = Territory::State.create(name: "Arequipa", territory_country: peru)
     d05 = Territory::State.create(name: "Ayacucho", territory_country: peru)
     d06 = Territory::State.create(name: "Cajamarca", territory_country: peru)
     d07 = Territory::State.create(name: "Callao", territory_country: peru)
     d08 = Territory::State.create(name: "Cusco", territory_country: peru)
     d09 = Territory::State.create(name: "Huancavelica", territory_country: peru)
     d10 = Territory::State.create(name: "Huánuco", territory_country: peru)
     d11 = Territory::State.create(name: "Ica", territory_country: peru)
     d12 = Territory::State.create(name: "Junín", territory_country: peru)
     d13 = Territory::State.create(name: "La Libertad", territory_country: peru)
     d14 = Territory::State.create(name: "Lambayeque", territory_country: peru)
     d15 = Territory::State.create(name: "Lima", territory_country: peru)
     d16 = Territory::State.create(name: "Loreto", territory_country: peru)
     d17 = Territory::State.create(name: "Madre de Dios", territory_country: peru)
     d18 = Territory::State.create(name: "Moquegua", territory_country: peru)
     d19 = Territory::State.create(name: "Pasco", territory_country: peru)
     d20 = Territory::State.create(name: "Piura", territory_country: peru)
     d21 = Territory::State.create(name: "Puno", territory_country: peru)
     d22 = Territory::State.create(name: "San Martín", territory_country: peru)
     d23 = Territory::State.create(name: "Tacna", territory_country: peru)
     d24 = Territory::State.create(name: "Tumbes", territory_country: peru)
     d25 = Territory::State.create(name: "Ucayali", territory_country: peru)

    [
      # Amazonas, 07 Provincias, d01
      {territory_state_id: d01.id, name: "Chachapoyas" },
      {territory_state_id: d01.id, name: "Bagua" },
      {territory_state_id: d01.id, name: "Bongará" },
      {territory_state_id: d01.id, name: "Luya" },
      {territory_state_id: d01.id, name: "Rodríguez de mendoza" },
      {territory_state_id: d01.id, name: "Condorcanqui" },
      {territory_state_id: d01.id, name: "Utcubamba" },

      # Ancash, 20 Provincias, d02
      {territory_state_id: d02.id, name: "Huaraz" },
      {territory_state_id: d02.id, name: "Aija" },
      {territory_state_id: d02.id, name: "Bolognesi" },
      {territory_state_id: d02.id, name: "Carhuaz" },
      {territory_state_id: d02.id, name: "Casma" },
      {territory_state_id: d02.id, name: "Corongo" },
      {territory_state_id: d02.id, name: "Huaylas" },
      {territory_state_id: d02.id, name: "Huari" },
      {territory_state_id: d02.id, name: "Mariscal luzuriaga" },
      {territory_state_id: d02.id, name: "Pallasca" },
      {territory_state_id: d02.id, name: "Pomabamba" },
      {territory_state_id: d02.id, name: "Recuay" },
      {territory_state_id: d02.id, name: "Santa" },
      {territory_state_id: d02.id, name: "Sihuas" },
      {territory_state_id: d02.id, name: "Yungay" },
      {territory_state_id: d02.id, name: "Antonio raimondi" },
      {territory_state_id: d02.id, name: "Carlos fermin fitzcarral" },
      {territory_state_id: d02.id, name: "Asunción" },
      {territory_state_id: d02.id, name: "Huarmey" },
      {territory_state_id: d02.id, name: "Ocros" },

      # Apurimac, 07 Provincias, d03
      {territory_state_id: d03.id, name: "Abancay" },
      {territory_state_id: d03.id, name: "Aymaraes" },
      {territory_state_id: d03.id, name: "Andahuaylas" },
      {territory_state_id: d03.id, name: "Antabamba" },
      {territory_state_id: d03.id, name: "Cotabambas" },
      {territory_state_id: d03.id, name: "Grau" },
      {territory_state_id: d03.id, name: "Chincheros" },

      # Arequipa, 08 Provincias, d04
      {territory_state_id: d04.id, name: "Arequipa" },
      {territory_state_id: d04.id, name: "Caylloma" },
      {territory_state_id: d04.id, name: "Camana" },
      {territory_state_id: d04.id, name: "Caraveli" },
      {territory_state_id: d04.id, name: "Castilla" },
      {territory_state_id: d04.id, name: "Condesuyos" },
      {territory_state_id: d04.id, name: "Islay" },
      {territory_state_id: d04.id, name: "La Union" },

      # Ayacucho, 11 Provincias, d05
      {territory_state_id: d05.id, name: "Huamanga" },
      {territory_state_id: d05.id, name: "Cangallo" },
      {territory_state_id: d05.id, name: "Huanta" },
      {territory_state_id: d05.id, name: "La Mar" },
      {territory_state_id: d05.id, name: "Lucanas" },
      {territory_state_id: d05.id, name: "Parinacochas" },
      {territory_state_id: d05.id, name: "Víctor Fajardo" },
      {territory_state_id: d05.id, name: "Huanca Sancos" },
      {territory_state_id: d05.id, name: "Vilcashuamán" },
      {territory_state_id: d05.id, name: "Páucar Del Sara Sara" },
      {territory_state_id: d05.id, name: "Sucre" },

      # Cajamarca, 13 Provincias, d06
      {territory_state_id: d06.id, name: "Cajamarca" },
      {territory_state_id: d06.id, name: "Cajabamba" },
      {territory_state_id: d06.id, name: "Celendín" },
      {territory_state_id: d06.id, name: "Contumazá" },
      {territory_state_id: d06.id, name: "Cutervo" },
      {territory_state_id: d06.id, name: "Chota" },
      {territory_state_id: d06.id, name: "Hualgayoc" },
      {territory_state_id: d06.id, name: "Jaén" },
      {territory_state_id: d06.id, name: "Santa Cruz" },
      {territory_state_id: d06.id, name: "San Miguel" },
      {territory_state_id: d06.id, name: "San Ignacio" },
      {territory_state_id: d06.id, name: "San Marcos" },
      {territory_state_id: d06.id, name: "San Pablo" },

      # Cusco, 13 Provincias, d07
      {territory_state_id: d07.id, name: "Cusco" },
      {territory_state_id: d07.id, name: "Acomayo" },
      {territory_state_id: d07.id, name: "Anta" },
      {territory_state_id: d07.id, name: "Calca" },
      {territory_state_id: d07.id, name: "Canas" },
      {territory_state_id: d07.id, name: "Canchis" },
      {territory_state_id: d07.id, name: "Chumbivilcas" },
      {territory_state_id: d07.id, name: "Espinar" },
      {territory_state_id: d07.id, name: "La Convencion" },
      {territory_state_id: d07.id, name: "Paruro" },
      {territory_state_id: d07.id, name: "Paucartambo" },
      {territory_state_id: d07.id, name: "Quispicanchi" },
      {territory_state_id: d07.id, name: "Urubamba" },


      #Callao, Provincias, d08
      {territory_state_id: d08.id, name: "El Callao" },
      {territory_state_id: d08.id, name: "Bellavista" },
      {territory_state_id: d08.id, name: "La Punta" },
      {territory_state_id: d08.id, name: "La Perla" },
      {territory_state_id: d08.id, name: "Carmen de la Legua" },
      {territory_state_id: d08.id, name: "Ventanilla" },


      # Huancavelica, 07 Provincias, d09
      {territory_state_id: d09.id, name: "Huancavelica" },
      {territory_state_id: d09.id, name: "Acobamba" },
      {territory_state_id: d09.id, name: "Angaraes" },
      {territory_state_id: d09.id, name: "Castrovirreyna" },
      {territory_state_id: d09.id, name: "Tayacaja" },
      {territory_state_id: d09.id, name: "Huaytará" },
      {territory_state_id: d09.id, name: "Churcampa" },

      # Huanuco, 11 Provincias, d10
      {territory_state_id: d10.id, name: "Huánuco" },
      {territory_state_id: d10.id, name: "Ambo" },
      {territory_state_id: d10.id, name: "Dos De Mayo" },
      {territory_state_id: d10.id, name: "Huamalíes" },
      {territory_state_id: d10.id, name: "Marañón" },
      {territory_state_id: d10.id, name: "Leoncio Prado" },
      {territory_state_id: d10.id, name: "Pachitea" },
      {territory_state_id: d10.id, name: "Puerto Inca" },
      {territory_state_id: d10.id, name: "Huacaybamba" },
      {territory_state_id: d10.id, name: "Lauricocha" },
      {territory_state_id: d10.id, name: "Yarowilca" },

      # Ica, 05 Provincias, d11
      {territory_state_id: d11.id, name: "Ica" },
      {territory_state_id: d11.id, name: "Chincha" },
      {territory_state_id: d11.id, name: "Nazca" },
      {territory_state_id: d11.id, name: "Pisco" },
      {territory_state_id: d11.id, name: "Palpa" },

      # Junin, 09 Provincias, d12
      {territory_state_id: d12.id, name: "Huancayo" },
      {territory_state_id: d12.id, name: "Concepcion" },
      {territory_state_id: d12.id, name: "Jauja" },
      {territory_state_id: d12.id, name: "Junin" },
      {territory_state_id: d12.id, name: "Tarma" },
      {territory_state_id: d12.id, name: "Yauli" },
      {territory_state_id: d12.id, name: "Satipo" },
      {territory_state_id: d12.id, name: "Chanchamayo" },
      {territory_state_id: d12.id, name: "Chupaca" },

      # La Libertad, 12 Provincias, d13
      {territory_state_id: d13.id, name: "Trujillo" },
      {territory_state_id: d13.id, name: "Bolívar" },
      {territory_state_id: d13.id, name: "Sánchez Carrión" },
      {territory_state_id: d13.id, name: "Otuzco" },
      {territory_state_id: d13.id, name: "Pacasmayo" },
      {territory_state_id: d13.id, name: "Pataz" },
      {territory_state_id: d13.id, name: "Santiago De Chuco" },
      {territory_state_id: d13.id, name: "Ascope" },
      {territory_state_id: d13.id, name: "Chepén" },
      {territory_state_id: d13.id, name: "Julcan" },
      {territory_state_id: d13.id, name: "Gran Chimú" },
      {territory_state_id: d13.id, name: "Virú" },

      # Lambayeque, 03 Provincias, d14
      {territory_state_id: d14.id, name: "Chiclayo" },
      {territory_state_id: d14.id, name: "Ferreñafe" },
      {territory_state_id: d14.id, name: "Lambayeque" },

      # Lima, 10 Provincias, d15
      {territory_state_id: d15.id, name: "Lima" },
      {territory_state_id: d15.id, name: "Cajatambo" },
      {territory_state_id: d15.id, name: "Canta" },
      {territory_state_id: d15.id, name: "Cañete" },
      {territory_state_id: d15.id, name: "Huaura" },
      {territory_state_id: d15.id, name: "Huarochirí" },
      {territory_state_id: d15.id, name: "Yauyos" },
      {territory_state_id: d15.id, name: "Huaral" },
      {territory_state_id: d15.id, name: "Barranca" },
      {territory_state_id: d15.id, name: "Oyón" },

      # Loreto, 07 Provincias, d16
      {territory_state_id: d16.id, name: "Maynas" },
      {territory_state_id: d16.id, name: "Alto Amazonas" },
      {territory_state_id: d16.id, name: "Loreto" },
      {territory_state_id: d16.id, name: "Requena" },
      {territory_state_id: d16.id, name: "Ucayali" },
      {territory_state_id: d16.id, name: "Mariscal Ramon Castilla" },
      {territory_state_id: d16.id, name: "Datem Del Marañon" },

      # Madre De Dios, 03 Provincias, d17
      {territory_state_id: d17.id, name: "Tambopata" },
      {territory_state_id: d17.id, name: "Manu" },
      {territory_state_id: d17.id, name: "Tahuamanu" },

      # Moquegua, 03 Provincias, d18
      {territory_state_id: d18.id, name: "Mariscal Nieto" },
      {territory_state_id: d18.id, name: "General Sanchez Cerro" },
      {territory_state_id: d18.id, name: "Ilo" },

      # Pasco, 03 Provincias, d19
      {territory_state_id: d19.id, name: "Pasco" },
      {territory_state_id: d19.id, name: "Daniel Alcides Carrión" },
      {territory_state_id: d19.id, name: "Oxapampa" },

      # Piura, 08 Provincias, d20
      {territory_state_id: d20.id, name: "Piura" },
      {territory_state_id: d20.id, name: "Ayabaca" },
      {territory_state_id: d20.id, name: "Huancabamba" },
      {territory_state_id: d20.id, name: "Morropón" },
      {territory_state_id: d20.id, name: "Paita" },
      {territory_state_id: d20.id, name: "Sullana" },
      {territory_state_id: d20.id, name: "Talara" },
      {territory_state_id: d20.id, name: "Sechura" },

      # Puno, 13 Provincias, d21
      {territory_state_id: d21.id, name: "Puno" },
      {territory_state_id: d21.id, name: "Azángaro" },
      {territory_state_id: d21.id, name: "Carabaya" },
      {territory_state_id: d21.id, name: "Chucuito" },
      {territory_state_id: d21.id, name: "Huancané" },
      {territory_state_id: d21.id, name: "Lampa" },
      {territory_state_id: d21.id, name: "Melgar" },
      {territory_state_id: d21.id, name: "Sandia" },
      {territory_state_id: d21.id, name: "San Román" },
      {territory_state_id: d21.id, name: "Yunguyo" },
      {territory_state_id: d21.id, name: "San Antonio De Putina" },
      {territory_state_id: d21.id, name: "El Collao" },
      {territory_state_id: d21.id, name: "Moho" },

      # San Martin, 10 Provincias, d22
      {territory_state_id: d22.id, name: "Moyobamba" },
      {territory_state_id: d22.id, name: "Huallaga" },
      {territory_state_id: d22.id, name: "Lamas" },
      {territory_state_id: d22.id, name: "Mariscal Cáceres" },
      {territory_state_id: d22.id, name: "Rioja" },
      {territory_state_id: d22.id, name: "San Martín" },
      {territory_state_id: d22.id, name: "Bellavista" },
      {territory_state_id: d22.id, name: "Tocache" },
      {territory_state_id: d22.id, name: "Picota" },
      {territory_state_id: d22.id, name: "El Dorado" },

      # Tacna, 04 Provincias, d23
      {territory_state_id: d23.id, name: "Tacna" },
      {territory_state_id: d23.id, name: "Tarata" },
      {territory_state_id: d23.id, name: "Jorge Basadre" },
      {territory_state_id: d23.id, name: "Candarave" },

      # Tumbes, 03 Provincias, d24
      {territory_state_id: d24.id, name: "Tumbes" },
      {territory_state_id: d24.id, name: "Contralmirante Villar" },
      {territory_state_id: d24.id, name: "Zarumilla" },

      # Ucayali, 04 Provincias, d25
      {territory_state_id: d25.id, name: "Coronel Portillo" },
      {territory_state_id: d25.id, name: "Padre Abad" },
      {territory_state_id: d25.id, name: "Atalaya" },
      {territory_state_id: d25.id, name: "Purús" }
    ].each do |state_and_cities|
      Territory::City.create(state_and_cities)
    end
  end

  def down
    peru = Territory::Country.find_by(name: 'Perú')
    peru.territory_states.destroy_all
  end
end
