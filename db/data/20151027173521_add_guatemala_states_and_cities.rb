class AddGuatemalaStatesAndCities < SeedMigration::Migration
  def up
    #Division politica de Guatemala
    guatemala = Territory::Country.find_by(name: 'Guatemala')

    d01 = Territory::State.create(name: "Guatemala", territory_country: guatemala)
    d02 = Territory::State.create(name: "El Progreso", territory_country: guatemala)
    d03 = Territory::State.create(name: "Sacatepéquez", territory_country: guatemala)
    d04 = Territory::State.create(name: "Chimaltenango", territory_country: guatemala)
    d05 = Territory::State.create(name: "Escuintla", territory_country: guatemala)
    d06 = Territory::State.create(name: "Santa Rosa", territory_country: guatemala)
    d07 = Territory::State.create(name: "Sololá", territory_country: guatemala)
    d08 = Territory::State.create(name: "Totonicapán", territory_country: guatemala)
    d09 = Territory::State.create(name: "Quetzaltenango", territory_country: guatemala)
    d10 = Territory::State.create(name: "Suchitepéquez", territory_country: guatemala)
    d11 = Territory::State.create(name: "Retalhuleu", territory_country: guatemala)
    d12 = Territory::State.create(name: "San Marcos", territory_country: guatemala)
    d13 = Territory::State.create(name: "Huehuetenango", territory_country: guatemala)
    d14 = Territory::State.create(name: "Quiché", territory_country: guatemala)
    d15 = Territory::State.create(name: "Baja Verapaz", territory_country: guatemala)
    d16 = Territory::State.create(name: "Alta Verapaz", territory_country: guatemala)
    d17 = Territory::State.create(name: "Petén", territory_country: guatemala)
    d18 = Territory::State.create(name: "Izabal", territory_country: guatemala)
    d19 = Territory::State.create(name: "Zacapa", territory_country: guatemala)
    d20 = Territory::State.create(name: "Chiquimula", territory_country: guatemala)
    d21 = Territory::State.create(name: "Jalapa", territory_country: guatemala)
    d22 = Territory::State.create(name: "Jutiapa", territory_country: guatemala)

    [
      # d01	Guatemala	17
      {territory_state_id: d01.id, name: "Guatemala" },
      {territory_state_id: d01.id, name: "Santa Catarina Pinula" },
      {territory_state_id: d01.id, name: "San José Pinula" },
      {territory_state_id: d01.id, name: "San José del Golfo" },
      {territory_state_id: d01.id, name: "Palencia" },
      {territory_state_id: d01.id, name: "Chinautla" },
      {territory_state_id: d01.id, name: "San Pedro Ayampuc" },
      {territory_state_id: d01.id, name: "Mixco" },
      {territory_state_id: d01.id, name: "San Pedro Sacatepéquez" },
      {territory_state_id: d01.id, name: "San Juan Sacatepéquez" },
      {territory_state_id: d01.id, name: "San Raimundo" },
      {territory_state_id: d01.id, name: "Chuarrancho" },
      {territory_state_id: d01.id, name: "Fraijanes" },
      {territory_state_id: d01.id, name: "Amatitlán" },
      {territory_state_id: d01.id, name: "Villa Nueva" },
      {territory_state_id: d01.id, name: "Villa Canales" },
      {territory_state_id: d01.id, name: "Petapa" },

      # d02	El Progreso	8
      {territory_state_id: d02.id, name: "Guastatoya" },
      {territory_state_id: d02.id, name: "Morazán" },
      {territory_state_id: d02.id, name: "San Agustín Acasaguastlán" },
      {territory_state_id: d02.id, name: "San Cristóbal Acasaguastlán" },
      {territory_state_id: d02.id, name: "El Jícaro" },
      {territory_state_id: d02.id, name: "Sansare" },
      {territory_state_id: d02.id, name: "Sanarate" },
      {territory_state_id: d02.id, name: "San Antonio La Paz" },

      # d03	Sacatepéquez	16
      {territory_state_id: d03.id, name: "Antigua Guatemala" },
      {territory_state_id: d03.id, name: "Jocotenango" },
      {territory_state_id: d03.id, name: "Pastores" },
      {territory_state_id: d03.id, name: "Sumpango" },
      {territory_state_id: d03.id, name: "Santo Domingo Xenacoj" },
      {territory_state_id: d03.id, name: "Santiago Sacatepéquez" },
      {territory_state_id: d03.id, name: "San Bartolomé Milpas Altas" },
      {territory_state_id: d03.id, name: "San Lucas Sacatepéquez" },
      {territory_state_id: d03.id, name: "Santa Lucía Milpas Altas" },
      {territory_state_id: d03.id, name: "Magdalena Milpas Altas" },
      {territory_state_id: d03.id, name: "Santa María de Jesús" },
      {territory_state_id: d03.id, name: "Ciudad Vieja" },
      {territory_state_id: d03.id, name: "San Miguel Dueñas" },
      {territory_state_id: d03.id, name: "Alotenango" },
      {territory_state_id: d03.id, name: "San Antonio Aguas Calientes" },
      {territory_state_id: d03.id, name: "Santa Catarina Barahona" },

      # d04	Chimaltenango	16
      {territory_state_id: d04.id, name: "Chimaltenango" },
      {territory_state_id: d04.id, name: "San José Poaquil" },
      {territory_state_id: d04.id, name: "San Martín Jilotepeque" },
      {territory_state_id: d04.id, name: "Comalapa" },
      {territory_state_id: d04.id, name: "Santa Apolonia" },
      {territory_state_id: d04.id, name: "Tecpán Guatemala" },
      {territory_state_id: d04.id, name: "Patzún" },
      {territory_state_id: d04.id, name: "Pochuta" },
      {territory_state_id: d04.id, name: "Patzicía" },
      {territory_state_id: d04.id, name: "Santa Cruz  Balanyá" },
      {territory_state_id: d04.id, name: "Acatenango" },
      {territory_state_id: d04.id, name: "Yepocapa" },
      {territory_state_id: d04.id, name: "San Andrés Itzapa" },
      {territory_state_id: d04.id, name: "Parramos" },
      {territory_state_id: d04.id, name: "Zaragoza" },
      {territory_state_id: d04.id, name: "El Tejar" },

      # d05	Escuintla	13
      {territory_state_id: d05.id, name: "Escuintla" },
      {territory_state_id: d05.id, name: "Santa Lucía Cotzumalguapa" },
      {territory_state_id: d05.id, name: "La Democracia" },
      {territory_state_id: d05.id, name: "Siquinalá" },
      {territory_state_id: d05.id, name: "Masagua" },
      {territory_state_id: d05.id, name: "Tiquisate" },
      {territory_state_id: d05.id, name: "La Gomera" },
      {territory_state_id: d05.id, name: "Guanagazapa" },
      {territory_state_id: d05.id, name: "San José" },
      {territory_state_id: d05.id, name: "Iztapa" },
      {territory_state_id: d05.id, name: "Palín" },
      {territory_state_id: d05.id, name: "San Vicente Pacaya" },
      {territory_state_id: d05.id, name: "Nueva Concepción" },

      # d06	Santa Rosa	14
      {territory_state_id: d06.id, name: "Cuilapa" },
      {territory_state_id: d06.id, name: "Barberena" },
      {territory_state_id: d06.id, name: "Santa Rosa de Lima" },
      {territory_state_id: d06.id, name: "Casillas" },
      {territory_state_id: d06.id, name: "San Rafael Las Flores" },
      {territory_state_id: d06.id, name: "Oratorio" },
      {territory_state_id: d06.id, name: "San Juan Tecuaco" },
      {territory_state_id: d06.id, name: "Chiquimulilla" },
      {territory_state_id: d06.id, name: "Taxisco" },
      {territory_state_id: d06.id, name: "Santa María Ixhuatán" },
      {territory_state_id: d06.id, name: "Guazacapán" },
      {territory_state_id: d06.id, name: "Santa Cruz  Naranjo" },
      {territory_state_id: d06.id, name: "Pueblo Nuevo Viñas" },
      {territory_state_id: d06.id, name: "Nueva Santa Rosa" },

      # d07	Sololá	19
      {territory_state_id: d07.id, name: "Sololá" },
      {territory_state_id: d07.id, name: "San José Chacayá" },
      {territory_state_id: d07.id, name: "Santa María Visitación" },
      {territory_state_id: d07.id, name: "Santa Lucía Utatlán" },
      {territory_state_id: d07.id, name: "Nahualá" },
      {territory_state_id: d07.id, name: "Santa Catarina Ixtahuacán" },
      {territory_state_id: d07.id, name: "Santa Clara La Laguna" },
      {territory_state_id: d07.id, name: "Concepción" },
      {territory_state_id: d07.id, name: "San Andrés Semetabaj" },
      {territory_state_id: d07.id, name: "Panajachel" },
      {territory_state_id: d07.id, name: "Santa Catarina Palopó" },
      {territory_state_id: d07.id, name: "San Antonio Palopó" },
      {territory_state_id: d07.id, name: "San Lucas Tolimán" },
      {territory_state_id: d07.id, name: "Santa Cruz  La Laguna" },
      {territory_state_id: d07.id, name: "San Pablo La Laguna" },
      {territory_state_id: d07.id, name: "San Marcos La Laguna" },
      {territory_state_id: d07.id, name: "San Juan La Laguna" },
      {territory_state_id: d07.id, name: "San Pedro La Laguna" },
      {territory_state_id: d07.id, name: "Santiago Atitlán" },

      # d08	Totonicapán	8
      {territory_state_id: d08.id, name: "Totonicapán" },
      {territory_state_id: d08.id, name: "San Cristóbal Totonicapán" },
      {territory_state_id: d08.id, name: "San Francisco El Alto" },
      {territory_state_id: d08.id, name: "San Andrés Xecul" },
      {territory_state_id: d08.id, name: "Momostenango" },
      {territory_state_id: d08.id, name: "Santa María Chiquimula" },
      {territory_state_id: d08.id, name: "Santa Lucía La Reforma" },

      # d09	Quetzaltenango	24
      {territory_state_id: d08.id, name: "San Bartolo Aguas Calientes" },
      {territory_state_id: d09.id, name: "Quetzaltenango" },
      {territory_state_id: d09.id, name: "Salcajá" },
      {territory_state_id: d09.id, name: "Olintepeque" },
      {territory_state_id: d09.id, name: "San Carlos Sija" },
      {territory_state_id: d09.id, name: "Sibilia" },
      {territory_state_id: d09.id, name: "Cabricán" },
      {territory_state_id: d09.id, name: "Cajolá" },
      {territory_state_id: d09.id, name: "San Miguel Sigüilá" },
      {territory_state_id: d09.id, name: "San Juan Ostuncalco" },
      {territory_state_id: d09.id, name: "San Mateo" },
      {territory_state_id: d09.id, name: "Concepción Chiquirichapa" },
      {territory_state_id: d09.id, name: "San Martín Sacatepéquez" },
      {territory_state_id: d09.id, name: "Almolonga" },
      {territory_state_id: d09.id, name: "Cantel" },
      {territory_state_id: d09.id, name: "Huitán" },
      {territory_state_id: d09.id, name: "Zunil" },
      {territory_state_id: d09.id, name: "Colomba" },
      {territory_state_id: d09.id, name: "San Francisco La Unión" },
      {territory_state_id: d09.id, name: "El Palmar" },
      {territory_state_id: d09.id, name: "Coatepeque" },
      {territory_state_id: d09.id, name: "Génova" },
      {territory_state_id: d09.id, name: "Flores  Costa Cuca" },
      {territory_state_id: d09.id, name: "La Esperanza" },
      {territory_state_id: d09.id, name: "Palestina de los Altos" },

      # d10	Suchitepéquez	20
      {territory_state_id: d10.id, name: "Mazatenango" },
      {territory_state_id: d10.id, name: "Cuyotenango" },
      {territory_state_id: d10.id, name: "San Francisco Zapotitlán" },
      {territory_state_id: d10.id, name: "San Bernardino" },
      {territory_state_id: d10.id, name: "San José El Ídolo" },
      {territory_state_id: d10.id, name: "Santo Domingo Suchitepéquez" },
      {territory_state_id: d10.id, name: "San Lorenzo" },
      {territory_state_id: d10.id, name: "Samayac" },
      {territory_state_id: d10.id, name: "San Pablo Jocopilas" },
      {territory_state_id: d10.id, name: "San Antonio Suchitepéquez" },
      {territory_state_id: d10.id, name: "San Miguel Panan" },
      {territory_state_id: d10.id, name: "San Gabriel" },
      {territory_state_id: d10.id, name: "Chicacao" },
      {territory_state_id: d10.id, name: "Patulul" },
      {territory_state_id: d10.id, name: "Santa Bárbara" },
      {territory_state_id: d10.id, name: "San Juan Bautista" },
      {territory_state_id: d10.id, name: "Santo Tomás La Unión" },
      {territory_state_id: d10.id, name: "Zunilito" },
      {territory_state_id: d10.id, name: "Pueblo Nuevo" },
      {territory_state_id: d10.id, name: "Río Bravo" },

      # d11	Retalhuleu	9
      {territory_state_id: d11.id, name: "Retalhuleu" },
      {territory_state_id: d11.id, name: "San Sebastián" },
      {territory_state_id: d11.id, name: "Santa Cruz  Muluá" },
      {territory_state_id: d11.id, name: "San Martín Zapotitlán" },
      {territory_state_id: d11.id, name: "San Felipe Retalhuleu" },
      {territory_state_id: d11.id, name: "San Andrés Villa Seca" },
      {territory_state_id: d11.id, name: "Champerico" },
      {territory_state_id: d11.id, name: "Nuevo San Carlos" },
      {territory_state_id: d11.id, name: "El Asintal" },

      # d12	San Marcos	30
      {territory_state_id: d12.id, name: "San Marcos" },
      {territory_state_id: d12.id, name: "San Pedro Sacatepéquez" },
      {territory_state_id: d12.id, name: "San Antonio Sacatepéquez" },
      {territory_state_id: d12.id, name: "Comitancillo" },
      {territory_state_id: d12.id, name: "San Miguel Ixtahuacán" },
      {territory_state_id: d12.id, name: "Concepción Tutuapa" },
      {territory_state_id: d12.id, name: "Tacaná" },
      {territory_state_id: d12.id, name: "Sibinal" },
      {territory_state_id: d12.id, name: "Tajumulco" },
      {territory_state_id: d12.id, name: "Tejutla" },
      {territory_state_id: d12.id, name: "San Rafael Pie de la Cuesta" },
      {territory_state_id: d12.id, name: "Nuevo Progreso" },
      {territory_state_id: d12.id, name: "El Tumbador" },
      {territory_state_id: d12.id, name: "El Rodeo" },
      {territory_state_id: d12.id, name: "Malacatán" },
      {territory_state_id: d12.id, name: "Catarina" },
      {territory_state_id: d12.id, name: "Ayutla" },
      {territory_state_id: d12.id, name: "Ocós" },
      {territory_state_id: d12.id, name: "San Pablo" },
      {territory_state_id: d12.id, name: "El Quetzal" },
      {territory_state_id: d12.id, name: "La Reforma" },
      {territory_state_id: d12.id, name: "Pajapita" },
      {territory_state_id: d12.id, name: "Ixchiguán" },
      {territory_state_id: d12.id, name: "San José Ojetenam" },
      {territory_state_id: d12.id, name: "San Cristóbal Cucho" },
      {territory_state_id: d12.id, name: "Sipacapa" },
      {territory_state_id: d12.id, name: "Esquipulas Palo Gordo" },
      {territory_state_id: d12.id, name: "Río Blanco" },
      {territory_state_id: d12.id, name: "San Lorenzo" },
      {territory_state_id: d12.id, name: "La Blanca" },

      # d13	Huehuetenango	32
      {territory_state_id: d13.id, name: "Huehuetenango" },
      {territory_state_id: d13.id, name: "Chiantla" },
      {territory_state_id: d13.id, name: "Malacatancito" },
      {territory_state_id: d13.id, name: "Cuilco" },
      {territory_state_id: d13.id, name: "Nentón" },
      {territory_state_id: d13.id, name: "San Pedro Necta" },
      {territory_state_id: d13.id, name: "Jacaltenango" },
      {territory_state_id: d13.id, name: "Soloma" },
      {territory_state_id: d13.id, name: "San Idelfonso Ixtahuacán" },
      {territory_state_id: d13.id, name: "Santa Bárbara" },
      {territory_state_id: d13.id, name: "La Libertad" },
      {territory_state_id: d13.id, name: "La Democracia" },
      {territory_state_id: d13.id, name: "San Miguel Acatán" },
      {territory_state_id: d13.id, name: "San Rafael La Independencia" },
      {territory_state_id: d13.id, name: "Todos Santos Cuchumatán" },
      {territory_state_id: d13.id, name: "San Juan Atitán" },
      {territory_state_id: d13.id, name: "Santa Eulalia" },
      {territory_state_id: d13.id, name: "San Mateo Ixtatán" },
      {territory_state_id: d13.id, name: "Colotenango" },
      {territory_state_id: d13.id, name: "San Sebastián Huehuetenango" },
      {territory_state_id: d13.id, name: "Tectitán" },
      {territory_state_id: d13.id, name: "Concepción Huista" },
      {territory_state_id: d13.id, name: "San Juan Ixcoy" },
      {territory_state_id: d13.id, name: "San Antonio Huista" },
      {territory_state_id: d13.id, name: "San Sebastián Coatán" },
      {territory_state_id: d13.id, name: "Barillas" },
      {territory_state_id: d13.id, name: "Aguacatán" },
      {territory_state_id: d13.id, name: "San Rafael Pétzal" },
      {territory_state_id: d13.id, name: "San Gaspar Ixchil" },
      {territory_state_id: d13.id, name: "Santiago Chimaltenango" },
      {territory_state_id: d13.id, name: "Santa Ana Huista" },
      {territory_state_id: d13.id, name: "Unión  Cantinil" },

      # d14	Quiché	21
      {territory_state_id: d14.id, name: "Santa Cruz  del Quiché" },
      {territory_state_id: d14.id, name: "Chiché" },
      {territory_state_id: d14.id, name: "Chinique" },
      {territory_state_id: d14.id, name: "Zacualpa" },
      {territory_state_id: d14.id, name: "Chajul" },
      {territory_state_id: d14.id, name: "Chichicastenango" },
      {territory_state_id: d14.id, name: "Patzité" },
      {territory_state_id: d14.id, name: "San Antonio Ilotenango" },
      {territory_state_id: d14.id, name: "San Pedro Jocopilas" },
      {territory_state_id: d14.id, name: "Cunén" },
      {territory_state_id: d14.id, name: "San Juan Cotzal" },
      {territory_state_id: d14.id, name: "Joyabaj" },
      {territory_state_id: d14.id, name: "Nebaj" },
      {territory_state_id: d14.id, name: "San Andrés Sajcabajá" },
      {territory_state_id: d14.id, name: "Uspantán" },
      {territory_state_id: d14.id, name: "Sacapulas" },
      {territory_state_id: d14.id, name: "San Bartolomé Jocotenango" },
      {territory_state_id: d14.id, name: "Canillá" },
      {territory_state_id: d14.id, name: "Chicamán" },
      {territory_state_id: d14.id, name: "Playa Grande-Ixcán" },
      {territory_state_id: d14.id, name: "Pachalum" },

      # d15	Baja Verapaz	8
      {territory_state_id: d15.id, name: "Salamá" },
      {territory_state_id: d15.id, name: "San Miguel Chicaj" },
      {territory_state_id: d15.id, name: "Rabinal" },
      {territory_state_id: d15.id, name: "Cubulco" },
      {territory_state_id: d15.id, name: "Granados" },
      {territory_state_id: d15.id, name: "El Chol" },
      {territory_state_id: d15.id, name: "San Jerónimo" },
      {territory_state_id: d15.id, name: "Purulhá" },

      # d16	Alta Verapaz	17
      {territory_state_id: d16.id, name: "Cobán" },
      {territory_state_id: d16.id, name: "Santa Cruz  Verapaz" },
      {territory_state_id: d16.id, name: "San Cristóbal Verapaz" },
      {territory_state_id: d16.id, name: "Tactic" },
      {territory_state_id: d16.id, name: "Tamahú" },
      {territory_state_id: d16.id, name: "Tucurú" },
      {territory_state_id: d16.id, name: "Panzós" },
      {territory_state_id: d16.id, name: "Senahú" },
      {territory_state_id: d16.id, name: "San Pedro Carchá" },
      {territory_state_id: d16.id, name: "San Juan Chamelco" },
      {territory_state_id: d16.id, name: "Lanquín" },
      {territory_state_id: d16.id, name: "Cahabón" },
      {territory_state_id: d16.id, name: "Chisec" },
      {territory_state_id: d16.id, name: "Chahal" },
      {territory_state_id: d16.id, name: "Fray Bartolomé de las Casas" },
      {territory_state_id: d16.id, name: "Santa Catalina La Tinta" },
      {territory_state_id: d16.id, name: "Raxruhá" },

      # d17	Petén	14
      {territory_state_id: d17.id, name: "Flores" },
      {territory_state_id: d17.id, name: "San José" },
      {territory_state_id: d17.id, name: "San Benito" },
      {territory_state_id: d17.id, name: "San Andrés" },
      {territory_state_id: d17.id, name: "La Libertad" },
      {territory_state_id: d17.id, name: "San Francisco" },
      {territory_state_id: d17.id, name: "Santa Ana" },
      {territory_state_id: d17.id, name: "Dolores" },
      {territory_state_id: d17.id, name: "San Luis" },
      {territory_state_id: d17.id, name: "Sayaxché" },
      {territory_state_id: d17.id, name: "Melchor de Mencos" },
      {territory_state_id: d17.id, name: "Poptún" },
      {territory_state_id: d17.id, name: "Las Cruces" },
      {territory_state_id: d17.id, name: "El Chal" },

      # d18	Izabal	5
      {territory_state_id: d18.id, name: "Puerto Barrios" },
      {territory_state_id: d18.id, name: "Lívingston" },
      {territory_state_id: d18.id, name: "El Estor" },
      {territory_state_id: d18.id, name: "Morales" },
      {territory_state_id: d18.id, name: "Los Amates" },

      # d19	Zacapa	11
      {territory_state_id: d19.id, name: "Zacapa" },
      {territory_state_id: d19.id, name: "Estanzuela" },
      {territory_state_id: d19.id, name: "Río Hondo" },
      {territory_state_id: d19.id, name: "Gualán" },
      {territory_state_id: d19.id, name: "Teculután" },
      {territory_state_id: d19.id, name: "Usumatlán" },
      {territory_state_id: d19.id, name: "Cabañas" },
      {territory_state_id: d19.id, name: "San Diego" },
      {territory_state_id: d19.id, name: "La Unión" },
      {territory_state_id: d19.id, name: "Huité" },
      {territory_state_id: d19.id, name: "San Jorge" },

      # d20	Chiquimula	11
      {territory_state_id: d20.id, name: "Chiquimula" },
      {territory_state_id: d20.id, name: "San José La Arada" },
      {territory_state_id: d20.id, name: "San Juan Ermita" },
      {territory_state_id: d20.id, name: "Jocotán" },
      {territory_state_id: d20.id, name: "Camotán" },
      {territory_state_id: d20.id, name: "Olopa" },
      {territory_state_id: d20.id, name: "Esquipulas" },
      {territory_state_id: d20.id, name: "Concepción Las Minas" },
      {territory_state_id: d20.id, name: "Quezaltepeque" },
      {territory_state_id: d20.id, name: "San Jacinto" },
      {territory_state_id: d20.id, name: "Ipala" },

      # d21	Jalapa	7
      {territory_state_id: d21.id, name: "Jalapa" },
      {territory_state_id: d21.id, name: "San Pedro Pinula" },
      {territory_state_id: d21.id, name: "San Luis Jilotepeque" },
      {territory_state_id: d21.id, name: "San Manuel Chaparrón" },
      {territory_state_id: d21.id, name: "San Carlos Alzatate" },
      {territory_state_id: d21.id, name: "Monjas" },
      {territory_state_id: d21.id, name: "Mataquescuintla" },

      # d22	Jutiapa	17
      {territory_state_id: d22.id, name: "Jutiapa" },
      {territory_state_id: d22.id, name: "El Progreso" },
      {territory_state_id: d22.id, name: "Santa Catarina Mita" },
      {territory_state_id: d22.id, name: "Agua Blanca" },
      {territory_state_id: d22.id, name: "Asunción Mita" },
      {territory_state_id: d22.id, name: "Yupiltepeque" },
      {territory_state_id: d22.id, name: "Atescatempa" },
      {territory_state_id: d22.id, name: "Jerez" },
      {territory_state_id: d22.id, name: "El Adelanto" },
      {territory_state_id: d22.id, name: "Zapotitlán" },
      {territory_state_id: d22.id, name: "Comapa" },
      {territory_state_id: d22.id, name: "Jalpatagua" },
      {territory_state_id: d22.id, name: "Conguaco" },
      {territory_state_id: d22.id, name: "Moyuta" },
      {territory_state_id: d22.id, name: "Pasaco" },
      {territory_state_id: d22.id, name: "San José Acatempa" },
      {territory_state_id: d22.id, name: "Quesada" }



    ].each do |state_and_cities|
      Territory::City.create(state_and_cities)
    end
  end

  def down
    guatemala = Territory::Country.find_by(name: 'Guatemala')
    guatemala.territory_states.destroy_all
  end
end
