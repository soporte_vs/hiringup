class UpdateTagCourseTagName < SeedMigration::Migration
  def up
    tag = Tag::CourseTag.find_by(name: 'Prácticas')
    tag.update(name: 'Movilidad Interna Oficinas Regionales y Casa Central') if tag.present?
  end

  def down
    tag = Tag::CourseTag.find_by(name: 'Movilidad Interna Oficinas Regionales y Casa Central')
    tag.update(name: 'Prácticas') if tag.present?
  end
end
