# coding: utf-8
class AddTrabajandoCity < SeedMigration::Migration
  def up
    cities_chile = Territory::City.joins(territory_state: :territory_country).where(territory_countries: { name: 'Chile'})
    Trabajando::City.all.each do |city|
      hupe_city_id = cities_chile.find_by_name(city.name).try(:id)
      city.update(hupe_city_id: hupe_city_id)
    end
  end

  def down
    Trabajando::City.destroy_all
  end
end
