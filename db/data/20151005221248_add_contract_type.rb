class AddContractType < SeedMigration::Migration
  def up
    [
      { name: 'Honorarios', description: 'Honorarios' },
      { name: 'Indefinido', description: 'Indefinido' },
      { name: 'Plazo Fijo', description: 'Plazo Fijo' },
      { name: 'Práctica Profesional', description: 'Práctica Profesional' },
      { name: 'Proyecto Especial', description: 'Proyecto Especial' }
    ].each do |ct|
      Company::ContractType.where(ct).first_or_create
    end

  end

  def down
    Company::ContractType.destroy_all
  end
end
