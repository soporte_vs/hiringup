class AddUsers < SeedMigration::Migration
  def up
    all_data = [
      { region: "Antofagasta", name: "Fabian" , last_name: "Tapia", rut: "16672044-3",  email: "ftapia@integra.cl" },
      { region: "Valparaiso", name: "Maria De los Ángeles", last_name: "Campaña", rut: "13990538-5",  email: "mcampana@integra.cl" },
      { region: "O Higgins", name: "Hugo", last_name: "Guerrero", rut: "17172617-4",  email: "hguerrero@integra.cl" },
      { region: "Los Rios", name: "Rosa", last_name: "Aguilera" , rut: "12747399-4",  email: "raguilera16@fundacion.integra.cl" },
      { region: "Los Rios", name: "Susana", last_name: "Aguillón" , rut: "12431695-2",  email: "saguillon16@fundacion.integra.cl" }
    ]

    all_data.each do |data|
      region = data[:region]
      rut_type = People::IdentificationDocumentType.find_by(name: 'RUT')
      rut = data[:rut]
      email = data[:email].downcase
      user_name = data[:name].capitalize
      user_last_name = data[:last_name].capitalize
      # Crear grupo
      group = Group.find_or_create_by(name: region, description: "Descripción #{region}")

      # Crear user, password es el RUT
      user = User.find_by(email: email)
      if user.nil? # Si user no existe, crear user
        user = User.create(email: email, password: 'integra.2016')

        user.add_role :admin, Applicant::Catched
        user.add_role :create, Applicant::Catched
        user.add_role :new, Applicant::Catched
        user.add_role :search, Applicant::Base
        user.add_role :update, Applicant::Base
        user.add_role :new, Course
        user.add_role :create, Course
        user.add_role :search, Course
      end

      # Agregar personal_information al user

      peopple = People::PersonalInformation.find_or_create_by(first_name: user_name, 
        last_name: user_last_name, user_id: user.id, 
        identification_document_type_id: rut_type.id, 
        identification_document_number: rut)
      unless peopple.persisted?
        error = peopple.errors.messages
        puts "RUT: #{rut} Errores: #{error}"
      end

      # Crear user group
      UserGroup.find_or_create_by(user_id: user.id, group_id: group.id)
    end
  end

  def down

  end
end
