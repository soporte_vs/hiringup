class UpdateAassmStateCourses < SeedMigration::Migration
  def up
    Course.update_all(aasm_state: :opened)
  end

  def down

  end
end
