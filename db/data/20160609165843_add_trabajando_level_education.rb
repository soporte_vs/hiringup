class AddTrabajandoLevelEducation < SeedMigration::Migration
  def up
    Trabajando::EducationLevel.reset_column_information
    Trabajando::EducationLevel.destroy_all
    all_data = [
      {:name=>"Básica", :trabajando_id=>1, :hupe_study_type=>'People::PrimarySchoolDegree'},
      {:name=>"Doctorado", :trabajando_id=>148, :hupe_study_type=>'People::PhdDegree'},
      {:name=>"Media", :trabajando_id=>2, :hupe_study_type=>'People::HighSchoolDegree'},
      {:name=>"Técnico medio/ colegio técnico", :trabajando_id=>3, :hupe_study_type=>'People::TechnicalDegree'},
      {:name=>"Magíster", :trabajando_id=>147, :hupe_study_type=>'People::MagisterDegree'},
      {:name=>"Postgrado", :trabajando_id=>6, :hupe_study_type=>'People::PostgraduateDegree'},
      {:name=>"Universitaria", :trabajando_id=>5, :hupe_study_type=>'People::ProfessionalDegree'},
      {:name=>"Técnico profesional superior", :trabajando_id=>4, :hupe_study_type=>'People::TechnicalProfessionalDegree'}
    ]
    all_data.each do |data|
      trabajando_education_level = Trabajando::EducationLevel.find_or_create_by(data.except(:hupe_study_type))
      trabajando_education_level.update(hupe_study_type: data[:hupe_study_type])
    end
  end

  def down
    Trabajando::EducationLevel.destroy_all
  end
end
