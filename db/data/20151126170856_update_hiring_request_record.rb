class UpdateHiringRequestRecord < SeedMigration::Migration
  def up
    HiringRequest::Record.where(aasm_state: nil).each do |hr|
      hr.update(aasm_state: :active)
    end
  end

  def down
  end
end
