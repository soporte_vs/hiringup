class AddNewPositionsToTrabajandoJobType < SeedMigration::Migration
  def up
    all_data = [
      { position_name: "Abogado", description: "Descripción Abogado", trabajando_id: "28", trabajando_name: "Abogado" },
      { position_name: "Administrativo", description: "Descripción Administrativo", trabajando_id: "17", trabajando_name: "Administrativo" },
      { position_name: "Analista", description: "Descripción Analista", trabajando_id: "2", trabajando_name: "Analista" },
      { position_name: "Analista Senior", description: "Descripción Analista Senior", trabajando_id: "6", trabajando_name: "Analista" },
      { position_name: "Asesor(a)", description: "Descripción Asesor(a)", trabajando_id: "32", trabajando_name: "Asesor" },
      { position_name: "Asistente", description: "Descripción Asistente", trabajando_id: "3", trabajando_name: "Asistente" },
      { position_name: "Asistente Administrativo", description: "Descripción Asistente Administrativo", trabajando_id: "17", trabajando_name: "Administrativo" },
      { position_name: "Asistente de Párvulos", description: "Descripción Asistente de Párvulos", trabajando_id: "67", trabajando_name: "Educador/Docente" },
      { position_name: "Asistente Extensión Horaria", description: "Descripción Asistente Extensión Horaria", trabajando_id: "67", trabajando_name: "Educador/Docente" },
      { position_name: "Auditor Interno", description: "Descripción Auditor Interno", trabajando_id: "34", trabajando_name: "Auditor" },
      { position_name: "Auxiliar de Servicios", description: "Descripción Auxiliar de Servicios", trabajando_id: "35", trabajando_name: "Auxiliar" },
      { position_name: "Conductor", description: "Descripción Conductor", trabajando_id: "19", trabajando_name: "Operario" },
      { position_name: "Contralor", description: "Descripción Contralor", trabajando_id: "39", trabajando_name: "Contralor" },
      { position_name: "Coordinador(a)", description: "Descripción Coordinador(a)", trabajando_id: "40", trabajando_name: "Coordinador" },
      { position_name: "Digitador(a)", description: "Descripción Digitador(a)", trabajando_id: "17", trabajando_name: "Administrativo" },
      { position_name: "Director(a)", description: "Descripción Director(a)", trabajando_id: "42", trabajando_name: "Director" },
      { position_name: "Educadora de Párvulos", description: "Descripción Educadora de Párvulos", trabajando_id: "67", trabajando_name: "Educador/Docente" },
      { position_name: "Educadora de Párvulos Mod. no convencional", description: "Descripción Educadora de Párvulos Mod. no convencional", trabajando_id: "67", trabajando_name: "Educador/Docente" },
      { position_name: "Encargado(a)", description: "Descripción Encargado(a)", trabajando_id: "45", trabajando_name: "Encargado" },
      { position_name: "Gestor", description: "Descripción Gestor", trabajando_id: "1", trabajando_name: "Otro Profesional" },
      { position_name: "Ingeniero", description: "Descripción Ingeniero", trabajando_id: "49", trabajando_name: "Ingeniero" },
      { position_name: "Jefe(a)", description: "Descripción Jefe(a)", trabajando_id: "8", trabajando_name: "Jefe Área/Sección/Depto./Local" },
      { position_name: "Junior", description: "Descripción Junior", trabajando_id: "3", trabajando_name: "Asistente" },
      { position_name: "Nutricionista", description: "Descripción Nutricionista", trabajando_id: "1", trabajando_name: "Otro Profesional" },
      { position_name: "Periodista", description: "Descripción Periodista", trabajando_id: "1", trabajando_name: "Otro Profesional" },
      { position_name: "Portero", description: "Descripción Portero", trabajando_id: "17", trabajando_name: "Administrativo" },
      { position_name: "Profesional de Apoyo", description: "Descripción Profesional de Apoyo", trabajando_id: "1", trabajando_name: "Otro Profesional" },
      { position_name: "Profesional Regional", description: "Descripción Profesional Regional", trabajando_id: "1", trabajando_name: "Otro Profesional" },
      { position_name: "Psicologo de Desarrollo", description: "Descripción Psicologo de Desarrollo", trabajando_id: "1", trabajando_name: "Otro Profesional" },
      { position_name: "Psicologo(A) Coordinador", description: "Descripción Psicologo(A) Coordinador", trabajando_id: "1", trabajando_name: "Otro Profesional" },
      { position_name: "Recepcionista", description: "Descripción Recepcionista", trabajando_id: "17", trabajando_name: "Administrativo" },
      { position_name: "Secretaria", description: "Descripción Secretaria", trabajando_id: "17", trabajando_name: "Administrativo" },
      { position_name: "Secretaria Administrativa", description: "Descripción Secretaria Administrativa", trabajando_id: "17", trabajando_name: "Administrativo" },
      { position_name: "Secretaria Atención de Público", description: "Descripción Secretaria Atención de Público", trabajando_id: "17", trabajando_name: "Administrativo" },
      { position_name: "Sub Director(a)", description: "Descripción Sub Director(a)", trabajando_id: "42", trabajando_name: "Director" },
      { position_name: "Supervisor(a)", description: "Descripción Supervisor(a)", trabajando_id: "7", trabajando_name: "Supervisor" },
      { position_name: "Técnico", description: "Descripción Técnico", trabajando_id: "18", trabajando_name: "Técnico" },
      { position_name: "Vigilante", description: "Descripción Vigilante", trabajando_id: "50", trabajando_name: "Inspector" }
    ]
    all_data.each do |data|
      position_name = data[:position_name]
      description = data[:description]
      trabajando_id = data[:trabajando_id]
      trabajando_name = data[:trabajando_name]

      trabajando_job_type = Trabajando::JobType.find_by(trabajando_id: trabajando_id)

      company_position = Company::Position.find_by(name: position_name)

      trabajando_job_type.company_positions << company_position

    end
  end

  def down
  end
end
