class AddContractTypes < SeedMigration::Migration
  def up
    [
     {:name=>"Honorarios", :description=>"test"},
     {:name=>"Indefinido", :description=>"Indefinido"},
     {:name=>"Plazo Fijo", :description=>"Plazo Fijo"},
     {:name=>"Práctica Profesional", :description=>"Práctica Profesional"},
     {:name=>"Proyecto Especial", :description=>"Proyecto Especial"}
    ].each do |contract|
      Company::ContractType.create(contract)
    end
  end

  def down
    Company::ContractType.destroy_all
  end
end
