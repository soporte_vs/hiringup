class UpdateTrabajandoEducationState < SeedMigration::Migration
  def up
    [
      { trabajando_id:0, name: '' },
      { trabajando_id:1, name: 'En curso' },
      { trabajando_id:2, name: 'Egresado' },
      { trabajando_id:3, name: 'Graduado' },
      { trabajando_id:4, name: 'Próximo a graduarse' },
      { trabajando_id:5, name: 'Indiferente' }
    ].each do |data|
      education_state = Trabajando::EducationState.find_by(trabajando_id: data[:trabajando_id])
      education_state.update(name: data[:name])
    end
  end

  def down
  end
end
