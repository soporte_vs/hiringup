class AddRoleAdminDescriptionToUser < SeedMigration::Migration
  def up
    all_data = [
      { rut: "13172319-9",  email: "cmelin@integra.cl" },
      { rut: "14552711-2",  email: "adegioanni@integra.cl" },
      { rut: "24976765-4",  email: "cmurcia@integra.cl" },
      { rut: "9683996-0",  email: "sgumucio@integra.cl" },
      { rut: "15491208-8",  email: "rdonoso@integra.cl" },
      { rut: "15523943-3",  email: "kmunoz@integra.cl" },
      { rut: "15352144-1",  email: "vayala@integra.cl" },
      { rut: "15069520-1",  email: "amerino@integra.cl" },
      { rut: "9838742-0",  email: "msilvac@integra.cl" },
      { rut: "15592065-3",  email: "vriquelme@integra.cl" },
      { rut: "9238169-2",  email: "pmedina@integra.cl" },
      { rut: "14228604-1",  email: "cvladilo@integra.cl" },
      { rut: "13935739-6",  email: "cferradan@integra.cl" },
      { rut: "10929058-0",  email: "clagos@integra.cl" },
      { rut: "13412842-9",  email: "arocco@integra.cl" },
      { rut: "15351248-5",  email: "CPARADA@INTEGRA.CL" }
    ]
    all_data.each do |data|
      rut = data[:rut]
      email = data[:email].downcase
      user = User.find_by(email: email)
      if user.present?
        user.add_role :admin_reports
      else
        puts "email no encontrado #{email}"
      end
      # Asignar rol admin_report
    end
  end

  def down
    # Remover rol user_admin_reports
    user = User.find_by(email: email)
    user.remove_role :admin_reports
  end
end
