class AddMatchCompanyAreaToTrabajandoArea < SeedMigration::Migration
  def up
    Trabajando::Area.all.each do |area|
      company_area = Company::Area.find_by(name: area.name)
      trabajando_area = Trabajando::Area.find_by(name: area.name)
      trabajando_area.update(hupe_area_id: company_area.id)
    end
  end

  def down
  end
end
