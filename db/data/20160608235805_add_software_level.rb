class AddSoftwareLevel < SeedMigration::Migration
  def up
    software_level_errors = []
    [
      "Ninguno",
      "Nivel bajo",
      "Nivel usuario",
      "Nivel usuario avanzado",
      "Nivel técnico",
      "Nivel profesional",
      "Nivel experto"
    ].each do |level|
      software_level = Applicant::SoftwareLevel.new(name: level)
      unless software_level.save
        software_level_errors << software_level
      end
    end

    if software_level_errors.length > 0
      puts "\033[31mSe han encontrado en Applicant::SoftwareLevel #{software_level_errors.length}. Errores: \033[0m" # Problems (Red message)
      software_level_errors.each do |object|
         puts "\tErrors => \033[31m#{object.errors.full_messages}\033[0m" # Problems (Red message)
      end
    end
  end

  def down
    Applicant::SoftwareLevel.destroy_all
  end
end
