class AddAreasToCompanyArea < SeedMigration::Migration
  def up
    trabajando_areas = Trabajando::Area.all
    company_areas = Company::Area
    trabajando_areas.each do |area|
      company_areas.find_or_create_by(name: area.name)
    end
  end

  def down
  end
end
