class AddEngagementOrigin < SeedMigration::Migration
  def up
    [
      { name: 'Continuidad', description: '' },
      { name: 'Meta aumento de cobertura', description: '' },
      { name: 'Protocolo de acuerdo', description: '' },
      { name: 'Práctica', description: '' },
      { name: 'Vacaciones en mi jardín', description: '' }
    ].each do |ea|
      Company::EngagementOrigin.where(ea).first_or_create
    end
  end

  def down
    Company::EngagementOrigin.destroy_all
  end
end
