# coding: utf-8
class AddTrabajandoWorkDay < SeedMigration::Migration
  def up
    Trabajando::WorkDay.destroy_all
    errors = []
    data= [
      {:name=>"Comisionista", :hupe_timetable_id=>nil, :trabajando_id=>4},
      {:name=>"Reemplazo", :hupe_timetable_id=>nil, :trabajando_id=>5},
      {:name=>"Práctica Profesional", :hupe_timetable_id=>nil, :trabajando_id=>6},
      {:name=>"Jornada Completa", :hupe_timetable_id=>1, :trabajando_id=>1},
      {:name=>"Media Jornada", :hupe_timetable_id=>2, :trabajando_id=>2},
      {:name=>"Free Lance", :hupe_timetable_id=>3, :trabajando_id=>8},
      {:name=>"Por Turnos", :hupe_timetable_id=>4, :trabajando_id=>7},
      {:name=>"Part Time", :hupe_timetable_id=>5, :trabajando_id=>3}
    ]
    if Trabajando::WorkDay.column_names.include? :hupe_timetable_id
      data.each do |attrs|
        trabajando_work_day = Trabajando::WorkDay.find_or_create_by(attrs.except(:hupe_timetable_id))
        trabajando_work_day.update(hupe_timetable_id: attrs[:hupe_timetable_id])
      end
    else
      data.each do |attrs|
        trabajando_work_day = Trabajando::WorkDay.where(
          name: attrs[:name],
          trabajando_id: attrs[:trabajando_id]
        ).first_or_create
        Company::TimetableTrabajandoWorkday.where(
          trabajando_work_day_id: trabajando_work_day.id,
          company_timetable_id: attrs[:hupe_timetable_id]
        ).first_or_create
      end
    end
  end

  def down
    Trabajando::WorkDay.destroy_all
  end
end
