class CourseUsers < SeedMigration::Migration
  def up
    Course.all.each do |course|
      created_by = course.activities.try(:first).try(:owner) || User.with_role(:admin).first || User.first
      course.update(user: created_by)
    end
  end

  def down
  end
end
