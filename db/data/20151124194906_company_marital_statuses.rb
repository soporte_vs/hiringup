class CompanyMaritalStatuses < SeedMigration::Migration
  def up
    all_data = [
      { name: 'Soltero'},
      { name: 'Casado'},
      { name: 'Divorciado'},
      { name: 'Viudo'}
    ]
    marital_statuses_error = []
    all_data.each do |data|
      marital_status = Company::MaritalStatus.new(data)
      unless marital_status.save
        marital_statuses_error.push(marital_status)
      end
    end

    total_created = all_data.length - marital_statuses_error.length
    if marital_statuses_error.length > 0
      puts "\033[31mSe han encontrado #{marital_statuses_error.length}. Errores: \033[0m" # Problems (Red message)
      marital_statuses_error.each do |object|
         puts "\tErrors => \033[31m#{object.errors.full_messages}\033[0m" # Problems (Red message)
      end
    end
    puts "\033[32mSe han creado #{total_created} de #{all_data.length}\033[0m" # No problems (Green message)
  end

  def down
    Company::MaritalStatus.destroy_all
  end
end
