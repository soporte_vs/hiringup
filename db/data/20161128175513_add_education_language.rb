class AddEducationLanguage < ActiveRecord::Migration
  def up
    [
      {name: 'Alemán'},
      {name: 'Español'},
      {name: 'Frances'},
      {name: 'Inglés'},
      {name: 'Italiano'},
      {name: 'Ruso'}
    ].each do |education_language|
      Education::Language.create(education_language)
    end
  end

  def down
    Education::Language.destroy_all
  end
end
