# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170908142556) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "abstract_hiringup_stages", force: :cascade do |t|
    t.string   "type"
    t.string   "name"
    t.integer  "enrollment_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "abstract_hiringup_stages", ["enrollment_id"], name: "index_abstract_hiringup_stages_on_enrollment_id", using: :btree

  create_table "activities", force: :cascade do |t|
    t.integer  "trackable_id"
    t.string   "trackable_type"
    t.integer  "owner_id"
    t.string   "owner_type"
    t.string   "key"
    t.text     "parameters"
    t.integer  "recipient_id"
    t.string   "recipient_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "activities", ["owner_id", "owner_type"], name: "index_activities_on_owner_id_and_owner_type", using: :btree
  add_index "activities", ["recipient_id", "recipient_type"], name: "index_activities_on_recipient_id_and_recipient_type", using: :btree
  add_index "activities", ["trackable_id", "trackable_type"], name: "index_activities_on_trackable_id_and_trackable_type", using: :btree

  create_table "applicant_bases", force: :cascade do |t|
    t.string   "type"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "blacklisted"
    t.text     "integration_data"
    t.integer  "company_recruitment_source_id"
  end

  add_index "applicant_bases", ["company_recruitment_source_id"], name: "index_applicant_bases_on_company_recruitment_source_id", using: :btree
  add_index "applicant_bases", ["user_id"], name: "index_applicant_bases_on_user_id", using: :btree

  create_table "applicant_profile_types", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "applicant_reports", force: :cascade do |t|
    t.integer  "user_id"
    t.text     "params"
    t.string   "report_file"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "applicant_reports", ["user_id"], name: "index_applicant_reports_on_user_id", using: :btree

  create_table "applicant_software_levels", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "applicant_update_info_requests", force: :cascade do |t|
    t.integer  "applicant_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "applicant_update_info_requests", ["applicant_id"], name: "index_applicant_update_info_requests_on_applicant_id", using: :btree

  create_table "assigning_weighting_reports", force: :cascade do |t|
    t.integer  "assigning_weighting_stage_id"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "assigning_weighting_reports", ["assigning_weighting_stage_id"], name: "assigning_weighting_stage", using: :btree

  create_table "bootsy_image_galleries", force: :cascade do |t|
    t.integer  "bootsy_resource_id"
    t.string   "bootsy_resource_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "bootsy_images", force: :cascade do |t|
    t.string   "image_file"
    t.integer  "image_gallery_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "calendar_event_guests", force: :cascade do |t|
    t.integer  "event_id"
    t.string   "name"
    t.string   "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "calendar_event_guests", ["event_id"], name: "index_calendar_event_guests_on_event_id", using: :btree

  create_table "calendar_event_managers", force: :cascade do |t|
    t.integer  "event_id"
    t.string   "name"
    t.string   "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "calendar_event_managers", ["event_id"], name: "index_calendar_event_managers_on_event_id", using: :btree

  create_table "calendar_event_resourceables", force: :cascade do |t|
    t.integer  "resourceable_id"
    t.string   "resourceable_type"
    t.integer  "event_id"
    t.string   "status"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.boolean  "attended"
    t.boolean  "will_attend"
  end

  add_index "calendar_event_resourceables", ["event_id"], name: "index_calendar_event_resourceables_on_event_id", using: :btree
  add_index "calendar_event_resourceables", ["resourceable_type", "resourceable_id"], name: "event_resourceable_index", using: :btree

  create_table "calendar_events", force: :cascade do |t|
    t.datetime "starts_at"
    t.datetime "ends_at"
    t.string   "address"
    t.float    "lat"
    t.float    "lng"
    t.text     "guests"
    t.integer  "created_by_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.text     "observations"
  end

  add_index "calendar_events", ["created_by_id"], name: "index_calendar_events_on_created_by_id", using: :btree

  create_table "comments", force: :cascade do |t|
    t.text     "content"
    t.integer  "user_id"
    t.integer  "object_id"
    t.string   "object_type"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "comments", ["object_type", "object_id"], name: "index_comments_on_object_type_and_object_id", using: :btree
  add_index "comments", ["user_id"], name: "index_comments_on_user_id", using: :btree

  create_table "company_areas", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "company_business_units", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "company_cencos", force: :cascade do |t|
    t.string   "name"
    t.string   "address"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.string   "cod"
    t.integer  "company_management_id"
  end

  add_index "company_cencos", ["company_management_id"], name: "index_company_cencos_on_company_management_id", using: :btree

  create_table "company_contract_types", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "company_employee_positions_cencos", force: :cascade do |t|
    t.integer  "company_employee_id"
    t.integer  "company_positions_cenco_id"
    t.boolean  "boss"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "company_employee_positions_cencos", ["company_employee_id"], name: "index_company_employee_positions_cencos_on_company_employee_id", using: :btree
  add_index "company_employee_positions_cencos", ["company_positions_cenco_id"], name: "index_company_positions_cenco_id", using: :btree

  create_table "company_employees", force: :cascade do |t|
    t.integer  "user_id"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.integer  "current_employee_position_cenco_id"
    t.boolean  "is_boss"
    t.integer  "boss_id"
  end

  add_index "company_employees", ["current_employee_position_cenco_id"], name: "index_company_employees_on_current_employee_position_cenco_id", using: :btree
  add_index "company_employees", ["user_id"], name: "index_company_employees_on_user_id", using: :btree

  create_table "company_engagement_origins", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "company_estates", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "company_estates", ["name"], name: "index_company_estates_on_name", unique: true, using: :btree

  create_table "company_managements", force: :cascade do |t|
    t.integer  "company_record_id"
    t.integer  "company_business_unit_id"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "company_managements", ["company_business_unit_id"], name: "index_company_managements_on_company_business_unit_id", using: :btree
  add_index "company_managements", ["company_record_id"], name: "index_company_managements_on_company_record_id", using: :btree

  create_table "company_marital_statuses", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "company_position_trabajando_job_types", force: :cascade do |t|
    t.integer  "company_position_id"
    t.integer  "trabajando_job_type_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "company_position_trabajando_job_types", ["company_position_id"], name: "position", using: :btree
  add_index "company_position_trabajando_job_types", ["trabajando_job_type_id"], name: "job_type", using: :btree

  create_table "company_positions", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "cod"
  end

  create_table "company_positions_cencos", force: :cascade do |t|
    t.integer  "company_position_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.integer  "company_cenco_id"
  end

  add_index "company_positions_cencos", ["company_cenco_id"], name: "index_company_positions_cencos_on_company_cenco_id", using: :btree
  add_index "company_positions_cencos", ["company_position_id"], name: "index_company_positions_cencos_on_company_position_id", using: :btree

  create_table "company_positions_references", force: :cascade do |t|
    t.integer  "company_reference_id"
    t.integer  "company_position_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "company_positions_references", ["company_position_id"], name: "index_company_positions_references_on_company_position_id", using: :btree
  add_index "company_positions_references", ["company_reference_id"], name: "index_company_positions_references_on_company_reference_id", using: :btree

  create_table "company_records", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "email_domain"
  end

  create_table "company_recruitment_sources", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "company_references", force: :cascade do |t|
    t.integer  "company_employee_id"
    t.integer  "applicant_id"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "email"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "position"
    t.string   "observations"
    t.boolean  "confirmed",           default: false
  end

  add_index "company_references", ["applicant_id"], name: "index_company_references_on_applicant_id", using: :btree
  add_index "company_references", ["company_employee_id"], name: "index_company_references_on_company_employee_id", using: :btree

  create_table "company_timetable_trabajando_workdays", force: :cascade do |t|
    t.integer  "company_timetable_id"
    t.integer  "trabajando_work_day_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "company_timetable_trabajando_workdays", ["company_timetable_id"], name: "timetable", using: :btree
  add_index "company_timetable_trabajando_workdays", ["trabajando_work_day_id"], name: "work_day", using: :btree

  create_table "company_timetables", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "company_vacancy_request_reasons", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "company_white_ruts", force: :cascade do |t|
    t.string   "rut"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "course_careers", force: :cascade do |t|
    t.integer  "course_id"
    t.integer  "education_career_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  add_index "course_careers", ["course_id"], name: "index_course_careers_on_course_id", using: :btree
  add_index "course_careers", ["education_career_id"], name: "index_course_careers_on_education_career_id", using: :btree

  create_table "course_postulations", force: :cascade do |t|
    t.integer  "course_id"
    t.integer  "postulationable_id"
    t.string   "postulationable_type"
    t.integer  "applicant_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "course_postulations", ["applicant_id"], name: "index_course_postulations_on_applicant_id", using: :btree
  add_index "course_postulations", ["course_id"], name: "index_course_postulations_on_course_id", using: :btree
  add_index "course_postulations", ["postulationable_type", "postulationable_id"], name: "cp_index", using: :btree

  create_table "courses", force: :cascade do |t|
    t.string   "type_enrollment"
    t.string   "title"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "work_flow"
    t.integer  "territory_city_id"
    t.integer  "company_position_id"
    t.integer  "document_group_id"
    t.string   "aasm_state"
    t.integer  "user_id"
    t.date     "start"
    t.date     "finish"
    t.integer  "experience_years"
    t.boolean  "is_supervisor"
    t.integer  "contract_type_id"
    t.integer  "request_reason_id"
    t.integer  "timetable_id"
    t.integer  "study_level_id"
    t.integer  "engagement_origin_id"
    t.integer  "applicant_software_level_id"
    t.integer  "applicant_profile_type_id"
    t.text     "minimun_requirements"
    t.integer  "area_id"
    t.string   "study_type"
    t.string   "observations_closing"
  end

  add_index "courses", ["applicant_profile_type_id"], name: "index_courses_on_applicant_profile_type_id", using: :btree
  add_index "courses", ["applicant_software_level_id"], name: "index_courses_on_applicant_software_level_id", using: :btree
  add_index "courses", ["area_id"], name: "index_courses_on_area_id", using: :btree
  add_index "courses", ["company_position_id"], name: "index_courses_on_company_position_id", using: :btree
  add_index "courses", ["contract_type_id"], name: "index_courses_on_contract_type_id", using: :btree
  add_index "courses", ["engagement_origin_id"], name: "index_courses_on_engagement_origin_id", using: :btree
  add_index "courses", ["request_reason_id"], name: "index_courses_on_request_reason_id", using: :btree
  add_index "courses", ["study_level_id"], name: "index_courses_on_study_level_id", using: :btree
  add_index "courses", ["territory_city_id"], name: "index_courses_on_territory_city_id", using: :btree
  add_index "courses", ["timetable_id"], name: "index_courses_on_timetable_id", using: :btree
  add_index "courses", ["user_id"], name: "index_courses_on_user_id", using: :btree

  create_table "courses_video_interview_questions", id: false, force: :cascade do |t|
    t.integer "question_id"
    t.integer "course_id"
  end

  add_index "courses_video_interview_questions", ["course_id"], name: "index_courses_video_interview_questions_on_course_id", using: :btree
  add_index "courses_video_interview_questions", ["question_id"], name: "index_courses_video_interview_questions_on_question_id", using: :btree

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "document_descriptor_memberships", force: :cascade do |t|
    t.integer  "document_descriptor_id"
    t.integer  "document_group_id"
    t.boolean  "document_required"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "document_descriptors", force: :cascade do |t|
    t.string   "name",                               null: false
    t.string   "details"
    t.string   "allowed_extensions"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.boolean  "internal",           default: false
    t.boolean  "only_onboarding",    default: false
  end

  create_table "document_groups", force: :cascade do |t|
    t.string   "name",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "document_records", force: :cascade do |t|
    t.integer  "applicant_id"
    t.integer  "document_descriptor_id"
    t.string   "document"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "enrollment_id"
  end

  create_table "education_careers", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "education_institutions", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.string   "type"
    t.boolean  "custom",     default: false
    t.integer  "country_id"
  end

  add_index "education_institutions", ["country_id"], name: "index_education_institutions_on_country_id", using: :btree

  create_table "education_languages", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "education_softwares", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "education_study_levels", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "engagement_agreements", force: :cascade do |t|
    t.string   "revenue"
    t.text     "observations"
    t.integer  "engagement_stage_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "vacancy_id"
    t.integer  "company_contract_type_id"
  end

  add_index "engagement_agreements", ["company_contract_type_id"], name: "index_engagement_agreements_on_company_contract_type_id", using: :btree
  add_index "engagement_agreements", ["engagement_stage_id"], name: "index_engagement_agreements_on_engagement_stage_id", using: :btree
  add_index "engagement_agreements", ["vacancy_id"], name: "index_engagement_agreements_on_vacancy_id", using: :btree

  create_table "enrollment_bases", force: :cascade do |t|
    t.string   "type"
    t.integer  "course_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "applicant_id"
    t.integer  "stage_id"
    t.integer  "current_stage_index"
  end

  add_index "enrollment_bases", ["applicant_id"], name: "index_enrollment_bases_on_applicant_id", using: :btree
  add_index "enrollment_bases", ["stage_id"], name: "index_enrollment_bases_on_stage_id", using: :btree

  create_table "enrollment_course_invitations", force: :cascade do |t|
    t.integer  "enrollment_id"
    t.boolean  "accepted"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "enrollment_course_invitations", ["enrollment_id"], name: "index_enrollment_course_invitations_on_enrollment_id", using: :btree

  create_table "enrollment_discard_reasons", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "mailer_content"
    t.text     "subject"
  end

  create_table "enrollment_discardings", force: :cascade do |t|
    t.integer  "enrollment_id"
    t.integer  "discard_reason_id"
    t.text     "observations"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "enrollment_discardings", ["discard_reason_id"], name: "index_enrollment_discardings_on_discard_reason_id", using: :btree
  add_index "enrollment_discardings", ["enrollment_id"], name: "index_enrollment_discardings_on_enrollment_id", using: :btree

  create_table "evaluar_courses", force: :cascade do |t|
    t.integer  "course_id"
    t.string   "agency_id"
    t.boolean  "confidential",               default: true
    t.text     "cc"
    t.boolean  "notification_at_the_end",    default: true
    t.boolean  "notification_per_evaluated", default: true
    t.string   "environment_type"
    t.string   "name"
    t.string   "product_type"
    t.datetime "start_date"
    t.datetime "end_date"
    t.string   "status"
    t.string   "profile_id"
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
    t.string   "evaluar_id"
    t.string   "callback_url"
  end

  add_index "evaluar_courses", ["course_id"], name: "index_evaluar_courses_on_course_id", using: :btree

  create_table "evaluar_results", force: :cascade do |t|
    t.text     "data"
    t.string   "process_id"
    t.string   "identification"
    t.integer  "applicant_id"
    t.integer  "evaluar_course_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "evaluar_results", ["applicant_id"], name: "index_evaluar_results_on_applicant_id", using: :btree
  add_index "evaluar_results", ["evaluar_course_id"], name: "index_evaluar_results_on_evaluar_course_id", using: :btree

  create_table "evaluar_tokens", force: :cascade do |t|
    t.string   "access_token"
    t.integer  "expires_in"
    t.integer  "refresh_expires_in"
    t.string   "refresh_token"
    t.string   "token_type"
    t.string   "id_token"
    t.integer  "not_before_policy"
    t.string   "session_state"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  create_table "faqs", force: :cascade do |t|
    t.string   "question"
    t.string   "answer"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "final_interview_reports", force: :cascade do |t|
    t.text     "observations"
    t.integer  "final_interview_stage_id"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.string   "notified_to"
  end

  add_index "final_interview_reports", ["final_interview_stage_id"], name: "index_final_interview_reports_on_final_interview_stage_id", using: :btree

  create_table "generic_attachments", force: :cascade do |t|
    t.string   "attachment"
    t.string   "name"
    t.integer  "resourceable_id"
    t.string   "resourceable_type"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "generic_attachments", ["resourceable_type", "resourceable_id"], name: "attachment_index", using: :btree

  create_table "group_interview_reports", force: :cascade do |t|
    t.text     "observations"
    t.integer  "group_interview_stage_id"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.string   "notified_to"
  end

  add_index "group_interview_reports", ["group_interview_stage_id"], name: "index_group_interview_reports_on_group_interview_stage_id", using: :btree

  create_table "groups", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "hiring_request_records", force: :cascade do |t|
    t.integer  "company_positions_cenco_id"
    t.date     "request_date"
    t.integer  "applicant_id"
    t.float    "revenue_expected"
    t.string   "workplace"
    t.text     "observations"
    t.integer  "company_contract_type_id"
    t.integer  "company_estate_id"
    t.integer  "company_timetable_id"
    t.date     "entry_date"
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
    t.integer  "company_vacancy_request_reason_id"
    t.string   "aasm_state"
    t.integer  "number_vacancies",                  default: 1
    t.integer  "created_by_id"
    t.string   "required_by_name"
    t.string   "required_by_email"
  end

  add_index "hiring_request_records", ["applicant_id"], name: "index_hiring_request_records_on_applicant_id", using: :btree
  add_index "hiring_request_records", ["company_contract_type_id"], name: "index_hiring_request_records_on_company_contract_type_id", using: :btree
  add_index "hiring_request_records", ["company_estate_id"], name: "index_hiring_request_records_on_company_estate_id", using: :btree
  add_index "hiring_request_records", ["company_positions_cenco_id"], name: "index_hiring_request_records_on_company_positions_cenco_id", using: :btree
  add_index "hiring_request_records", ["company_timetable_id"], name: "index_hiring_request_records_on_company_timetable_id", using: :btree
  add_index "hiring_request_records", ["company_vacancy_request_reason_id"], name: "hr_cp_vacancy_request_reason", using: :btree

  create_table "interview_reports", force: :cascade do |t|
    t.text     "observations"
    t.string   "document"
    t.integer  "interview_stage_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  add_index "interview_reports", ["interview_stage_id"], name: "index_interview_reports_on_interview_stage_id", using: :btree

  create_table "laborum_postulations", force: :cascade do |t|
    t.integer  "laborum_publication_id"
    t.integer  "applicant_id"
    t.integer  "user_portal_id",         limit: 8
    t.string   "answer1"
    t.string   "answer2"
    t.string   "answer3"
    t.string   "answer4"
    t.string   "answer5"
    t.text     "information"
    t.string   "cv"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.boolean  "is_accepted"
    t.date     "accepted_at"
  end

  add_index "laborum_postulations", ["applicant_id"], name: "index_laborum_postulations_on_applicant_id", using: :btree
  add_index "laborum_postulations", ["laborum_publication_id"], name: "index_laborum_postulations_on_laborum_publication_id", using: :btree

  create_table "laborum_publications", force: :cascade do |t|
    t.integer  "service_id"
    t.string   "title"
    t.text     "description"
    t.integer  "state_id"
    t.integer  "city_id"
    t.integer  "area_id"
    t.integer  "subarea_id"
    t.integer  "pay_id"
    t.integer  "language_id"
    t.integer  "language_level_id"
    t.integer  "type_job_id"
    t.integer  "study_area_id"
    t.integer  "study_type_id"
    t.integer  "study_status_id"
    t.integer  "salary"
    t.integer  "experience"
    t.string   "question1"
    t.string   "question2"
    t.string   "question3"
    t.string   "question4"
    t.string   "question5"
    t.integer  "course_id"
    t.string   "status"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.string   "address"
    t.integer  "denomination_id",   default: 0
    t.datetime "desactivate_at"
    t.integer  "delayed_job_id"
  end

  add_index "laborum_publications", ["course_id"], name: "index_laborum_publications_on_course_id", using: :btree
  add_index "laborum_publications", ["delayed_job_id"], name: "index_laborum_publications_on_delayed_job_id", using: :btree

  create_table "manager_interview_reports", force: :cascade do |t|
    t.text     "observations"
    t.string   "document"
    t.integer  "manager_interview_stage_id"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "manager_interview_reports", ["manager_interview_stage_id"], name: "index_manager_interview_reports_on_manager_interview_stage_id", using: :btree

  create_table "minisite_answers", force: :cascade do |t|
    t.string   "description"
    t.integer  "minisite_question_id"
    t.integer  "minisite_postulation_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "minisite_answers", ["minisite_postulation_id"], name: "index_minisite_answers_on_minisite_postulation_id", using: :btree
  add_index "minisite_answers", ["minisite_question_id"], name: "index_minisite_answers_on_minisite_question_id", using: :btree

  create_table "minisite_banners", force: :cascade do |t|
    t.string   "title"
    t.string   "image"
    t.boolean  "active_image"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "minisite_categories", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.string   "image"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "minisite_category_publications", force: :cascade do |t|
    t.integer  "minisite_category_id"
    t.integer  "minisite_publication_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "minisite_category_publications", ["minisite_category_id"], name: "index_minisite_category_publications_on_minisite_category_id", using: :btree
  add_index "minisite_category_publications", ["minisite_publication_id"], name: "index_minisite_category_publications_on_minisite_publication_id", using: :btree

  create_table "minisite_postulations", force: :cascade do |t|
    t.integer  "minisite_publication_id"
    t.integer  "applicant_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.boolean  "is_accepted"
    t.date     "accepted_at"
  end

  add_index "minisite_postulations", ["applicant_id"], name: "index_minisite_postulations_on_applicant_id", using: :btree
  add_index "minisite_postulations", ["minisite_publication_id"], name: "index_minisite_postulations_on_minisite_publication_id", using: :btree

  create_table "minisite_publications", force: :cascade do |t|
    t.integer  "course_id"
    t.text     "description"
    t.string   "title"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "company_contract_type_id"
    t.string   "aasm_state"
    t.datetime "desactivate_at"
    t.integer  "delayed_job_id"
  end

  add_index "minisite_publications", ["company_contract_type_id"], name: "index_minisite_publications_on_company_contract_type_id", using: :btree
  add_index "minisite_publications", ["course_id"], name: "index_minisite_publications_on_course_id", using: :btree
  add_index "minisite_publications", ["delayed_job_id"], name: "index_minisite_publications_on_delayed_job_id", using: :btree

  create_table "minisite_questions", force: :cascade do |t|
    t.integer  "minisite_publication_id"
    t.text     "description"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "minisite_questions", ["minisite_publication_id"], name: "index_minisite_questions_on_minisite_publication_id", using: :btree

  create_table "oauth_access_grants", force: :cascade do |t|
    t.integer  "resource_owner_id", null: false
    t.integer  "application_id",    null: false
    t.string   "token",             null: false
    t.integer  "expires_in",        null: false
    t.text     "redirect_uri",      null: false
    t.datetime "created_at",        null: false
    t.datetime "revoked_at"
    t.string   "scopes"
  end

  add_index "oauth_access_grants", ["token"], name: "index_oauth_access_grants_on_token", unique: true, using: :btree

  create_table "oauth_access_tokens", force: :cascade do |t|
    t.integer  "resource_owner_id"
    t.integer  "application_id"
    t.string   "token",             null: false
    t.string   "refresh_token"
    t.integer  "expires_in"
    t.datetime "revoked_at"
    t.datetime "created_at",        null: false
    t.string   "scopes"
  end

  add_index "oauth_access_tokens", ["refresh_token"], name: "index_oauth_access_tokens_on_refresh_token", unique: true, using: :btree
  add_index "oauth_access_tokens", ["resource_owner_id"], name: "index_oauth_access_tokens_on_resource_owner_id", using: :btree
  add_index "oauth_access_tokens", ["token"], name: "index_oauth_access_tokens_on_token", unique: true, using: :btree

  create_table "oauth_applications", force: :cascade do |t|
    t.string   "name",                      null: false
    t.string   "uid",                       null: false
    t.string   "secret",                    null: false
    t.text     "redirect_uri",              null: false
    t.string   "scopes",       default: "", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "oauth_applications", ["uid"], name: "index_oauth_applications_on_uid", unique: true, using: :btree

  create_table "offer_letters", force: :cascade do |t|
    t.integer  "company_position_id"
    t.integer  "company_contract_type_id"
    t.integer  "vacancy_id"
    t.string   "direct_boss"
    t.integer  "net_remuneration"
    t.datetime "contract_start_date"
    t.boolean  "accepted"
    t.string   "token"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.string   "address"
    t.integer  "zone_assignation"
    t.integer  "offer_reject_reason_id"
    t.integer  "stage_id"
  end

  add_index "offer_letters", ["company_contract_type_id"], name: "index_offer_letters_on_company_contract_type_id", using: :btree
  add_index "offer_letters", ["company_position_id"], name: "index_offer_letters_on_company_position_id", using: :btree
  add_index "offer_letters", ["offer_reject_reason_id"], name: "index_offer_letters_on_offer_reject_reason_id", using: :btree
  add_index "offer_letters", ["stage_id"], name: "index_offer_letters_on_stage_id", using: :btree
  add_index "offer_letters", ["vacancy_id"], name: "index_offer_letters_on_vacancy_id", using: :btree

  create_table "offer_reject_reasons", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "onboarding_document_groups", force: :cascade do |t|
    t.integer  "onboarding_stage_id"
    t.integer  "document_group_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  add_index "onboarding_document_groups", ["document_group_id"], name: "index_onboarding_document_groups_on_document_group_id", using: :btree
  add_index "onboarding_document_groups", ["onboarding_stage_id"], name: "index_onboarding_document_groups_on_onboarding_stage_id", using: :btree

  create_table "people_degrees", force: :cascade do |t|
    t.integer  "career_id"
    t.integer  "professional_information_id"
    t.string   "condition"
    t.integer  "institution_id"
    t.date     "culmination_date"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.string   "name"
    t.string   "type"
  end

  add_index "people_degrees", ["career_id"], name: "index_people_degrees_on_career_id", using: :btree
  add_index "people_degrees", ["institution_id"], name: "index_people_degrees_on_institution_id", using: :btree
  add_index "people_degrees", ["professional_information_id"], name: "index_people_degrees_on_professional_information_id", using: :btree

  create_table "people_identification_document_types", force: :cascade do |t|
    t.string   "name"
    t.string   "codename"
    t.integer  "country_id"
    t.text     "validation_regex"
    t.boolean  "validate_uniqueness", default: true
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
  end

  add_index "people_identification_document_types", ["country_id"], name: "index_people_identification_document_types_on_country_id", using: :btree

  create_table "people_laboral_experiences", force: :cascade do |t|
    t.string   "position"
    t.string   "company"
    t.date     "since_date"
    t.date     "until_date"
    t.integer  "professional_information_id"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.text     "description"
  end

  add_index "people_laboral_experiences", ["professional_information_id"], name: "index_people_laboral_experiences_on_professional_information_id", using: :btree

  create_table "people_language_skills", force: :cascade do |t|
    t.string   "level"
    t.integer  "education_language_id"
    t.integer  "professional_information_id"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "people_language_skills", ["education_language_id"], name: "index_people_language_skills_on_education_language_id", using: :btree

  create_table "people_personal_informations", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.date     "birth_date"
    t.string   "landline"
    t.string   "cellphone"
    t.string   "sex"
    t.integer  "user_id"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.integer  "territory_city_id"
    t.string   "avatar"
    t.string   "address"
    t.integer  "nationality_id"
    t.integer  "company_marital_status_id"
    t.text     "areas_of_interest"
    t.integer  "recruitment_source_id"
    t.string   "identification_document_number"
    t.integer  "identification_document_type_id"
    t.string   "study_type"
    t.boolean  "availability_replacements"
    t.boolean  "availability_work_other_residence"
  end

  add_index "people_personal_informations", ["company_marital_status_id"], name: "index_people_personal_informations_on_company_marital_status_id", using: :btree
  add_index "people_personal_informations", ["nationality_id"], name: "index_people_personal_informations_on_nationality_id", using: :btree
  add_index "people_personal_informations", ["recruitment_source_id"], name: "index_people_personal_informations_on_recruitment_source_id", using: :btree
  add_index "people_personal_informations", ["territory_city_id"], name: "index_people_personal_informations_on_territory_city_id", using: :btree
  add_index "people_personal_informations", ["user_id"], name: "index_people_personal_informations_on_user_id", using: :btree

  create_table "people_personal_referrals", force: :cascade do |t|
    t.string   "full_name"
    t.string   "email"
    t.string   "phone"
    t.integer  "personal_information_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "people_personal_referrals", ["personal_information_id"], name: "index_people_personal_referrals_on_personal_information_id", using: :btree

  create_table "people_professional_informations", force: :cascade do |t|
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "people_professional_informations", ["user_id"], name: "index_people_professional_informations_on_user_id", using: :btree

  create_table "people_software_skills", force: :cascade do |t|
    t.string   "level"
    t.integer  "professional_information_id"
    t.integer  "education_software_id"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "people_software_skills", ["education_software_id"], name: "index_people_software_skills_on_education_software_id", using: :btree
  add_index "people_software_skills", ["professional_information_id"], name: "index_people_software_skills_on_professional_information_id", using: :btree

  create_table "psycholaboral_evaluation_reports", force: :cascade do |t|
    t.text     "observations"
    t.integer  "psycholaboral_evaluation_stage_id"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.string   "notified_to"
  end

  add_index "psycholaboral_evaluation_reports", ["psycholaboral_evaluation_stage_id"], name: "psycolb_eva", using: :btree

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "roles", ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
  add_index "roles", ["name"], name: "index_roles_on_name", using: :btree

  create_table "seed_migration_data_migrations", force: :cascade do |t|
    t.string   "version"
    t.integer  "runtime"
    t.datetime "migrated_on"
  end

  create_table "shortened_urls", force: :cascade do |t|
    t.integer  "owner_id"
    t.string   "owner_type", limit: 20
    t.text     "url",                               null: false
    t.string   "unique_key", limit: 10,             null: false
    t.integer  "use_count",             default: 0, null: false
    t.datetime "expires_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "shortened_urls", ["owner_id", "owner_type"], name: "index_shortened_urls_on_owner_id_and_owner_type", using: :btree
  add_index "shortened_urls", ["unique_key"], name: "index_shortened_urls_on_unique_key", unique: true, using: :btree
  add_index "shortened_urls", ["url"], name: "index_shortened_urls_on_url", using: :btree

  create_table "skill_interview_reports", force: :cascade do |t|
    t.text     "observations"
    t.integer  "skill_interview_stage_id"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "skill_interview_reports", ["skill_interview_stage_id"], name: "index_skill_interview_reports_on_skill_interview_stage_id", using: :btree

  create_table "stage_bases", force: :cascade do |t|
    t.integer  "enrollment_id"
    t.string   "type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "step"
    t.string   "name"
    t.boolean  "is_approved"
  end

  add_index "stage_bases", ["enrollment_id"], name: "index_stage_bases_on_enrollment_id", using: :btree

  create_table "stage_documents", force: :cascade do |t|
    t.integer "resource_id"
    t.string  "resource_type"
    t.string  "document"
  end

  add_index "stage_documents", ["resource_type", "resource_id"], name: "index_stage_documents_on_resource_type_and_resource_id", using: :btree

  create_table "stage_not_apply_reasons", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "stage_not_applyings", force: :cascade do |t|
    t.integer  "stage_id"
    t.integer  "not_apply_reason_id"
    t.text     "observations"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  add_index "stage_not_applyings", ["not_apply_reason_id"], name: "index_stage_not_applyings_on_not_apply_reason_id", using: :btree
  add_index "stage_not_applyings", ["stage_id"], name: "index_stage_not_applyings_on_stage_id", using: :btree

  create_table "tag_base_tags", force: :cascade do |t|
    t.string   "name"
    t.string   "type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tag_used_tags", force: :cascade do |t|
    t.integer  "tag_id"
    t.integer  "taggable_id"
    t.string   "taggable_type"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "tag_used_tags", ["tag_id"], name: "index_tag_used_tags_on_tag_id", using: :btree
  add_index "tag_used_tags", ["taggable_type", "taggable_id"], name: "index_tag_used_tags_on_taggable_type_and_taggable_id", using: :btree

  create_table "technical_test_reports", force: :cascade do |t|
    t.text     "observations"
    t.integer  "technical_test_stage_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "notified_to"
  end

  add_index "technical_test_reports", ["technical_test_stage_id"], name: "index_technical_test_reports_on_technical_test_stage_id", using: :btree

  create_table "territory_cities", force: :cascade do |t|
    t.string   "name"
    t.integer  "territory_state_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  add_index "territory_cities", ["territory_state_id"], name: "index_territory_cities_on_territory_state_id", using: :btree

  create_table "territory_countries", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "territory_states", force: :cascade do |t|
    t.string   "name"
    t.integer  "territory_country_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "territory_states", ["territory_country_id"], name: "index_territory_states_on_territory_country_id", using: :btree

  create_table "test_reports", force: :cascade do |t|
    t.text     "observations"
    t.integer  "test_stage_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "test_reports", ["test_stage_id"], name: "index_test_reports_on_test_stage_id", using: :btree

  create_table "trabajando_applicant_profiles", force: :cascade do |t|
    t.string   "name"
    t.integer  "trabajando_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.integer  "hupe_profile_type_id"
  end

  add_index "trabajando_applicant_profiles", ["hupe_profile_type_id"], name: "index_trabajando_applicant_profiles_on_hupe_profile_type_id", using: :btree

  create_table "trabajando_areas", force: :cascade do |t|
    t.integer  "trabajando_id"
    t.string   "name"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "hupe_area_id"
  end

  add_index "trabajando_areas", ["hupe_area_id"], name: "index_trabajando_areas_on_hupe_area_id", using: :btree

  create_table "trabajando_careers", force: :cascade do |t|
    t.string   "name"
    t.integer  "trabajando_id"
    t.integer  "hupe_career_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "trabajando_careers", ["hupe_career_id"], name: "index_trabajando_careers_on_hupe_career_id", using: :btree

  create_table "trabajando_cities", force: :cascade do |t|
    t.string   "name"
    t.integer  "trabajando_id"
    t.integer  "hupe_city_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "trabajando_cities", ["hupe_city_id"], name: "index_trabajando_cities_on_hupe_city_id", using: :btree

  create_table "trabajando_company_activities", force: :cascade do |t|
    t.string   "name"
    t.integer  "trabajando_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "trabajando_countries", force: :cascade do |t|
    t.string   "name"
    t.integer  "trabajando_id"
    t.integer  "country_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "trabajando_countries", ["country_id"], name: "index_trabajando_countries_on_country_id", using: :btree

  create_table "trabajando_education_levels", force: :cascade do |t|
    t.string   "name"
    t.integer  "trabajando_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "hupe_study_type"
  end

  create_table "trabajando_education_states", force: :cascade do |t|
    t.string   "name"
    t.integer  "trabajando_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.integer  "hupe_study_level_id"
  end

  add_index "trabajando_education_states", ["hupe_study_level_id"], name: "index_trabajando_education_states_on_hupe_study_level_id", using: :btree

  create_table "trabajando_job_types", force: :cascade do |t|
    t.string   "name"
    t.integer  "trabajando_id"
    t.integer  "hupe_position_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "trabajando_job_types", ["hupe_position_id"], name: "index_trabajando_job_types_on_hupe_position_id", using: :btree

  create_table "trabajando_marital_statuses", force: :cascade do |t|
    t.string   "name"
    t.integer  "trabajando_id"
    t.integer  "company_marital_status_id"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "trabajando_marital_statuses", ["company_marital_status_id"], name: "index_trabajando_marital_statuses_on_company_marital_status_id", using: :btree

  create_table "trabajando_postulations", force: :cascade do |t|
    t.integer  "trabajando_publication_id"
    t.integer  "applicant_id"
    t.datetime "postulation_date"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.boolean  "is_accepted"
    t.datetime "accepted_at"
    t.text     "answers"
    t.string   "trabajando_applicant_id"
  end

  add_index "trabajando_postulations", ["applicant_id"], name: "index_trabajando_postulations_on_applicant_id", using: :btree
  add_index "trabajando_postulations", ["trabajando_publication_id"], name: "index_trabajando_postulations_on_trabajando_publication_id", using: :btree

  create_table "trabajando_publications", force: :cascade do |t|
    t.string   "title"
    t.string   "description"
    t.text     "q1"
    t.text     "q2"
    t.text     "q3"
    t.text     "q4"
    t.text     "q5"
    t.integer  "number_vacancies"
    t.float    "salary"
    t.integer  "course_id"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.integer  "area_id"
    t.string   "contract_time"
    t.boolean  "show_salary",          default: true
    t.string   "salary_comment"
    t.string   "work_time"
    t.string   "workplace"
    t.text     "minimum_requirements"
    t.integer  "work_experience"
    t.boolean  "own_transport",        default: false
    t.integer  "trabajando_id"
    t.string   "aasm_state"
    t.integer  "job_type_id"
    t.integer  "company_activity_id"
    t.integer  "work_day_id"
    t.integer  "applicant_profile_id"
    t.integer  "education_level_id"
    t.integer  "education_state_id"
    t.integer  "software_level_id"
    t.integer  "region_id"
    t.integer  "city_id"
    t.boolean  "is_confidential",      default: false
    t.datetime "desactivate_at"
    t.integer  "delayed_job_id"
  end

  add_index "trabajando_publications", ["applicant_profile_id"], name: "index_trabajando_publications_on_applicant_profile_id", using: :btree
  add_index "trabajando_publications", ["area_id"], name: "index_trabajando_publications_on_area_id", using: :btree
  add_index "trabajando_publications", ["city_id"], name: "index_trabajando_publications_on_city_id", using: :btree
  add_index "trabajando_publications", ["company_activity_id"], name: "index_trabajando_publications_on_company_activity_id", using: :btree
  add_index "trabajando_publications", ["course_id"], name: "index_trabajando_publications_on_course_id", using: :btree
  add_index "trabajando_publications", ["delayed_job_id"], name: "index_trabajando_publications_on_delayed_job_id", using: :btree
  add_index "trabajando_publications", ["education_level_id"], name: "index_trabajando_publications_on_education_level_id", using: :btree
  add_index "trabajando_publications", ["education_state_id"], name: "index_trabajando_publications_on_education_state_id", using: :btree
  add_index "trabajando_publications", ["job_type_id"], name: "index_trabajando_publications_on_job_type_id", using: :btree
  add_index "trabajando_publications", ["region_id"], name: "index_trabajando_publications_on_region_id", using: :btree
  add_index "trabajando_publications", ["software_level_id"], name: "index_trabajando_publications_on_software_level_id", using: :btree
  add_index "trabajando_publications", ["work_day_id"], name: "index_trabajando_publications_on_work_day_id", using: :btree

  create_table "trabajando_software_levels", force: :cascade do |t|
    t.string   "name"
    t.integer  "trabajando_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "hupe_software_level_id"
  end

  add_index "trabajando_software_levels", ["hupe_software_level_id"], name: "index_trabajando_software_levels_on_hupe_software_level_id", using: :btree

  create_table "trabajando_states", force: :cascade do |t|
    t.string   "name"
    t.integer  "trabajando_id"
    t.integer  "hupe_state_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "trabajando_states", ["hupe_state_id"], name: "index_trabajando_states_on_hupe_state_id", using: :btree

  create_table "trabajando_universities", force: :cascade do |t|
    t.string   "name"
    t.integer  "trabajando_id"
    t.integer  "hupe_university_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  add_index "trabajando_universities", ["hupe_university_id"], name: "index_trabajando_universities_on_hupe_university_id", using: :btree

  create_table "trabajando_work_days", force: :cascade do |t|
    t.string   "name"
    t.integer  "trabajando_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "trigger_bases", force: :cascade do |t|
    t.string   "type"
    t.integer  "resourceable_id"
    t.string   "resourceable_type"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "trigger_bases", ["resourceable_type", "resourceable_id"], name: "index_trigger_bases_on_resourceable_type_and_resourceable_id", using: :btree

  create_table "user_groups", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "group_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "user_groups", ["group_id"], name: "index_user_groups_on_group_id", using: :btree
  add_index "user_groups", ["user_id"], name: "index_user_groups_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",   null: false
    t.string   "encrypted_password",     default: ""
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,    null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "invitation_token"
    t.datetime "invitation_created_at"
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer  "invitation_limit"
    t.integer  "invited_by_id"
    t.string   "invited_by_type"
    t.integer  "invitations_count",      default: 0
    t.string   "authentication_token"
    t.string   "provider"
    t.string   "uid"
    t.boolean  "is_active",              default: true
    t.datetime "locked_at"
  end

  add_index "users", ["authentication_token"], name: "index_users_on_authentication_token", using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["invitation_token"], name: "index_users_on_invitation_token", unique: true, using: :btree
  add_index "users", ["invitations_count"], name: "index_users_on_invitations_count", using: :btree
  add_index "users", ["invited_by_id"], name: "index_users_on_invited_by_id", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "users_roles", force: :cascade do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "users_roles", ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree

  create_table "vacancies", force: :cascade do |t|
    t.integer  "course_id"
    t.datetime "closed_at"
    t.text     "observations"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.integer  "company_positions_cenco_id"
    t.integer  "hiring_request_record_id"
    t.integer  "company_vacancy_request_reason_id"
    t.integer  "created_by_id"
    t.string   "required_by_email"
    t.string   "required_by_name"
  end

  add_index "vacancies", ["company_positions_cenco_id"], name: "index_vacancies_on_company_positions_cenco_id", using: :btree
  add_index "vacancies", ["company_vacancy_request_reason_id"], name: "index_vacancies_on_company_vacancy_request_reason_id", using: :btree
  add_index "vacancies", ["course_id"], name: "index_vacancies_on_course_id", using: :btree
  add_index "vacancies", ["hiring_request_record_id"], name: "index_vacancies_on_hiring_request_record_id", using: :btree

  create_table "versions", force: :cascade do |t|
    t.string   "item_type",  null: false
    t.integer  "item_id",    null: false
    t.string   "event",      null: false
    t.string   "whodunnit"
    t.text     "object"
    t.datetime "created_at"
  end

  add_index "versions", ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id", using: :btree

  create_table "video_interview_answers", force: :cascade do |t|
    t.integer  "time_limit"
    t.string   "question"
    t.integer  "enrollment_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "user_id"
  end

  create_table "video_interview_questions", force: :cascade do |t|
    t.string   "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "worker_bases", force: :cascade do |t|
    t.string   "type"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "worker_bases", ["user_id"], name: "index_worker_bases_on_user_id", using: :btree

  add_foreign_key "applicant_bases", "company_recruitment_sources"
  add_foreign_key "applicant_reports", "users"
  add_foreign_key "applicant_update_info_requests", "applicant_bases", column: "applicant_id"
  add_foreign_key "assigning_weighting_reports", "stage_bases", column: "assigning_weighting_stage_id"
  add_foreign_key "calendar_event_guests", "calendar_events", column: "event_id"
  add_foreign_key "calendar_event_managers", "calendar_events", column: "event_id"
  add_foreign_key "calendar_event_resourceables", "calendar_events", column: "event_id"
  add_foreign_key "calendar_events", "users", column: "created_by_id"
  add_foreign_key "comments", "users"
  add_foreign_key "company_cencos", "company_managements"
  add_foreign_key "company_employee_positions_cencos", "company_employees"
  add_foreign_key "company_employee_positions_cencos", "company_positions_cencos"
  add_foreign_key "company_employees", "company_employee_positions_cencos", column: "current_employee_position_cenco_id"
  add_foreign_key "company_employees", "company_employees", column: "boss_id"
  add_foreign_key "company_employees", "users"
  add_foreign_key "company_managements", "company_business_units"
  add_foreign_key "company_managements", "company_records"
  add_foreign_key "company_position_trabajando_job_types", "company_positions"
  add_foreign_key "company_position_trabajando_job_types", "trabajando_job_types"
  add_foreign_key "company_positions_cencos", "company_cencos"
  add_foreign_key "company_positions_cencos", "company_positions"
  add_foreign_key "company_positions_cencos", "company_positions"
  add_foreign_key "company_positions_references", "company_positions"
  add_foreign_key "company_positions_references", "company_references"
  add_foreign_key "company_references", "applicant_bases", column: "applicant_id"
  add_foreign_key "company_references", "company_employees"
  add_foreign_key "company_timetable_trabajando_workdays", "company_timetables"
  add_foreign_key "company_timetable_trabajando_workdays", "trabajando_work_days"
  add_foreign_key "course_careers", "courses"
  add_foreign_key "course_careers", "education_careers"
  add_foreign_key "course_postulations", "applicant_bases", column: "applicant_id"
  add_foreign_key "course_postulations", "courses"
  add_foreign_key "courses", "applicant_profile_types"
  add_foreign_key "courses", "applicant_software_levels"
  add_foreign_key "courses", "company_areas", column: "area_id"
  add_foreign_key "courses", "company_contract_types", column: "contract_type_id"
  add_foreign_key "courses", "company_engagement_origins", column: "engagement_origin_id"
  add_foreign_key "courses", "company_positions"
  add_foreign_key "courses", "company_timetables", column: "timetable_id"
  add_foreign_key "courses", "company_vacancy_request_reasons", column: "request_reason_id"
  add_foreign_key "courses", "education_study_levels", column: "study_level_id"
  add_foreign_key "courses", "territory_cities"
  add_foreign_key "courses", "users"
  add_foreign_key "education_institutions", "territory_countries", column: "country_id"
  add_foreign_key "engagement_agreements", "company_contract_types"
  add_foreign_key "engagement_agreements", "vacancies"
  add_foreign_key "enrollment_course_invitations", "enrollment_bases", column: "enrollment_id"
  add_foreign_key "enrollment_discardings", "enrollment_discard_reasons", column: "discard_reason_id"
  add_foreign_key "evaluar_results", "applicant_bases", column: "applicant_id"
  add_foreign_key "evaluar_results", "evaluar_courses"
  add_foreign_key "final_interview_reports", "stage_bases", column: "final_interview_stage_id"
  add_foreign_key "group_interview_reports", "stage_bases", column: "group_interview_stage_id"
  add_foreign_key "hiring_request_records", "applicant_bases", column: "applicant_id"
  add_foreign_key "hiring_request_records", "company_contract_types"
  add_foreign_key "hiring_request_records", "company_estates"
  add_foreign_key "hiring_request_records", "company_positions_cencos"
  add_foreign_key "hiring_request_records", "company_timetables"
  add_foreign_key "hiring_request_records", "company_vacancy_request_reasons"
  add_foreign_key "hiring_request_records", "users", column: "created_by_id"
  add_foreign_key "interview_reports", "stage_bases", column: "interview_stage_id"
  add_foreign_key "laborum_postulations", "applicant_bases", column: "applicant_id"
  add_foreign_key "laborum_postulations", "laborum_publications"
  add_foreign_key "laborum_publications", "courses"
  add_foreign_key "manager_interview_reports", "stage_bases", column: "manager_interview_stage_id"
  add_foreign_key "minisite_answers", "minisite_postulations"
  add_foreign_key "minisite_answers", "minisite_questions"
  add_foreign_key "minisite_category_publications", "minisite_categories"
  add_foreign_key "minisite_category_publications", "minisite_publications"
  add_foreign_key "minisite_postulations", "applicant_bases", column: "applicant_id"
  add_foreign_key "minisite_postulations", "minisite_publications"
  add_foreign_key "minisite_publications", "company_contract_types"
  add_foreign_key "minisite_publications", "courses"
  add_foreign_key "minisite_questions", "minisite_publications"
  add_foreign_key "offer_letters", "company_contract_types"
  add_foreign_key "offer_letters", "company_positions"
  add_foreign_key "offer_letters", "offer_reject_reasons"
  add_foreign_key "offer_letters", "vacancies"
  add_foreign_key "onboarding_document_groups", "document_groups"
  add_foreign_key "onboarding_document_groups", "stage_bases", column: "onboarding_stage_id"
  add_foreign_key "people_degrees", "education_careers", column: "career_id"
  add_foreign_key "people_degrees", "education_institutions", column: "institution_id"
  add_foreign_key "people_degrees", "people_professional_informations", column: "professional_information_id"
  add_foreign_key "people_identification_document_types", "territory_countries", column: "country_id"
  add_foreign_key "people_laboral_experiences", "people_professional_informations", column: "professional_information_id"
  add_foreign_key "people_language_skills", "education_languages"
  add_foreign_key "people_language_skills", "people_professional_informations", column: "professional_information_id"
  add_foreign_key "people_personal_informations", "company_marital_statuses"
  add_foreign_key "people_personal_informations", "company_recruitment_sources", column: "recruitment_source_id"
  add_foreign_key "people_personal_informations", "people_identification_document_types", column: "identification_document_type_id"
  add_foreign_key "people_personal_informations", "territory_cities"
  add_foreign_key "people_personal_informations", "territory_countries", column: "nationality_id"
  add_foreign_key "people_personal_informations", "users"
  add_foreign_key "people_personal_referrals", "people_personal_informations", column: "personal_information_id"
  add_foreign_key "people_professional_informations", "users"
  add_foreign_key "people_software_skills", "education_softwares"
  add_foreign_key "people_software_skills", "people_professional_informations", column: "professional_information_id"
  add_foreign_key "psycholaboral_evaluation_reports", "stage_bases", column: "psycholaboral_evaluation_stage_id"
  add_foreign_key "skill_interview_reports", "stage_bases", column: "skill_interview_stage_id"
  add_foreign_key "stage_not_applyings", "stage_bases", column: "stage_id"
  add_foreign_key "stage_not_applyings", "stage_not_apply_reasons", column: "not_apply_reason_id"
  add_foreign_key "tag_used_tags", "tag_base_tags", column: "tag_id"
  add_foreign_key "technical_test_reports", "stage_bases", column: "technical_test_stage_id"
  add_foreign_key "territory_cities", "territory_states"
  add_foreign_key "territory_states", "territory_countries"
  add_foreign_key "test_reports", "stage_bases", column: "test_stage_id"
  add_foreign_key "trabajando_applicant_profiles", "applicant_profile_types", column: "hupe_profile_type_id"
  add_foreign_key "trabajando_areas", "company_areas", column: "hupe_area_id"
  add_foreign_key "trabajando_careers", "education_careers", column: "hupe_career_id"
  add_foreign_key "trabajando_cities", "territory_cities", column: "hupe_city_id"
  add_foreign_key "trabajando_countries", "territory_countries", column: "country_id"
  add_foreign_key "trabajando_education_states", "education_study_levels", column: "hupe_study_level_id"
  add_foreign_key "trabajando_job_types", "company_positions", column: "hupe_position_id"
  add_foreign_key "trabajando_marital_statuses", "company_marital_statuses"
  add_foreign_key "trabajando_postulations", "applicant_bases", column: "applicant_id"
  add_foreign_key "trabajando_postulations", "trabajando_publications"
  add_foreign_key "trabajando_publications", "courses"
  add_foreign_key "trabajando_publications", "trabajando_applicant_profiles", column: "applicant_profile_id"
  add_foreign_key "trabajando_publications", "trabajando_areas", column: "area_id"
  add_foreign_key "trabajando_publications", "trabajando_cities", column: "city_id"
  add_foreign_key "trabajando_publications", "trabajando_company_activities", column: "company_activity_id"
  add_foreign_key "trabajando_publications", "trabajando_education_levels", column: "education_level_id"
  add_foreign_key "trabajando_publications", "trabajando_education_states", column: "education_state_id"
  add_foreign_key "trabajando_publications", "trabajando_job_types", column: "job_type_id"
  add_foreign_key "trabajando_publications", "trabajando_software_levels", column: "software_level_id"
  add_foreign_key "trabajando_publications", "trabajando_states", column: "region_id"
  add_foreign_key "trabajando_publications", "trabajando_work_days", column: "work_day_id"
  add_foreign_key "trabajando_software_levels", "applicant_software_levels", column: "hupe_software_level_id"
  add_foreign_key "trabajando_states", "territory_states", column: "hupe_state_id"
  add_foreign_key "trabajando_universities", "education_institutions", column: "hupe_university_id"
  add_foreign_key "user_groups", "groups"
  add_foreign_key "user_groups", "users"
  add_foreign_key "vacancies", "company_positions_cencos"
  add_foreign_key "vacancies", "company_positions_cencos"
  add_foreign_key "vacancies", "company_vacancy_request_reasons"
  add_foreign_key "vacancies", "courses"
  add_foreign_key "vacancies", "hiring_request_records"
  add_foreign_key "vacancies", "users", column: "created_by_id"
end
