require 'rails_helper'

RSpec.describe "hiring_request/records/edit", type: :view do
  before(:each) do
    @hiring_request_record = assign(:hiring_request_record, FactoryGirl.create(:hiring_request_record))
  end

  it "renders the edit hiring_request_record form" do
    render

    assert_select "form[action=?][method=?]", hiring_request_record_path(@hiring_request_record), "post" do

      assert_select "input[name=?]", "hiring_request_record[entry_date]"
      assert_select "input[name=?]", "hiring_request_record[revenue_expected]"
      assert_select("textarea[name=?]", "hiring_request_record[observations]")
      assert_select "input[name=?][value=?]", "hiring_request_record[workplace]", "#{@hiring_request_record.workplace}"

      assert_select "select[name=?]", "hiring_request_record[company_contract_type_id]" do
        Company::ContractType.all.each do |contract_type|
          assert_select "option[value=?]", "#{contract_type.id}"
          expect(assert_select("option[value=?]", "#{contract_type.id}").text).to eq(@hiring_request_record.company_contract_type.name)
        end
      end

      assert_select "select[name=?]", "hiring_request_record[company_estate_id]" do
        Company::Estate.all.each do |estate|
          assert_select "option[value=?]", "#{estate.id}"
          expect(assert_select("option[value=?]", "#{estate.id}").text).to eq(@hiring_request_record.company_estate.name)
        end
      end

      assert_select "select[name=?]", "hiring_request_record[company_timetable_id]" do
        Company::Timetable.all.each do |timetable|
          assert_select "option[value=?]", "#{timetable.id}"
          expect(assert_select("option[value=?]", "#{timetable.id}").text).to eq(@hiring_request_record.company_timetable.name)
        end
      end

      assert_select("select[name=?]", "hiring_request_record[company_vacancy_request_reason_id]") do
        Company::VacancyRequestReason.all.each do |company_vacancy_request_reason|
          assert_select "option[value=?]", "#{company_vacancy_request_reason.id}"
          expect(assert_select("option[value=?]", "#{company_vacancy_request_reason.id}").text).to eq(@hiring_request_record.company_vacancy_request_reason.name)
        end
      end
    end
  end
end
