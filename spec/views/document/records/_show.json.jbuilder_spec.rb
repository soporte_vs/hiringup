require "rails_helper"

RSpec.describe "document/records/_show" do
  context 'render json' do
    before :each do
      @applicant = FactoryGirl.create(:applicant_bases)
      @document = FactoryGirl.create(:document_record, applicant_id: @applicant.id)
    end

    it 'should render without errors' do
      render partial: 'document/records/show',  locals: { document_record: @document }
      json_response = JSON.parse(response.body)
      expect(json_response).to match(
        { 
          "applicant_id" => @document.applicant_id,
          "created_at" => I18n.l(@document.created_at, format: :custom),
          "document" => @document.document.file.filename,
          "id" => @document.id,
          "document_descriptor_id" => @document.document_descriptor_id,
          "url" => download_document_applicant_basis_path(@applicant, @document.document_descriptor.name),
          "errors" => {}
        }
      )
    end

    it 'should render with errors' do
      @document.errors.add(:update, I18n.t('errors.messages.extension_white_list_error'))

      render partial: 'document/records/show',  locals: { document_record: @document }
      json_response = JSON.parse(response.body)
      expect(json_response).to match(
        {
          "applicant_id" => @document.applicant_id,
          "created_at" => I18n.l(@document.created_at, format: :custom),
          "document" => @document.document.file.filename,
          "id" => @document.id,
          "document_descriptor_id" => @document.document_descriptor_id,
          "url" => download_document_applicant_basis_path(@applicant, @document.document_descriptor.name),
          "errors" => {"update" => [I18n.t('errors.messages.extension_white_list_error')]}
        }
      )
    end

    it 'should render without url' do
      @document = FactoryGirl.build(:document_record, applicant_id: @applicant.id, document: '')
      render partial: 'document/records/show',  locals: { document_record: @document }
      json_response = JSON.parse(response.body)
      expect(json_response).to match(
        {
          "applicant_id" => @document.applicant_id,
          "created_at" => nil,
          "document" => nil,
          "id" => @document.id,
          "document_descriptor_id" => @document.document_descriptor_id,
          "url" => nil,
          "errors" => {}
        }
      )
    end

    it 'should render with url' do
      @document = FactoryGirl.create(:document_record, applicant_id: @applicant.id)
      render partial: 'document/records/show',  locals: { document_record: @document }
      json_response = JSON.parse(response.body)
      expect(json_response).to match(
        {
          "applicant_id" => @document.applicant_id,
          "created_at" => I18n.l(@document.created_at, format: :custom),
          "document" => @document.document.file.filename,
          "id" => @document.id,
          "document_descriptor_id" => @document.document_descriptor_id,
          "url" => download_document_applicant_basis_path(@applicant, @document.document_descriptor.name),
          "errors" => {}
        }
      )
    end
  end
end