require 'rails_helper'

RSpec.describe "document/descriptors/new", type: :view do
  before(:each) do
    assign(:document_descriptor, FactoryGirl.build(:document_descriptor))
  end

  it "renders new document_descriptor form" do
    render

    assert_select "form[action=?][method=?]", document_descriptors_path, "post" do

      assert_select "input#document_descriptor_name[name=?]", "document_descriptor[name]"

      #assert_select "input#document_descriptor_required[name=?]", "document_descriptor[required]"

      assert_select "input#document_descriptor_details[name=?]", "document_descriptor[details]"

      #assert_select "input#document_descriptor_allowed_extensions[name=?]", "document_descriptor[allowed_extensions]"
    end
  end
end
