require 'rails_helper'

RSpec.describe "document/descriptors/edit", type: :view do
  before(:each) do
    @document_descriptor = FactoryGirl.create(:document_descriptor)
  end

  it "renders the edit document_descriptor form" do
    render

    assert_select "form[action=?][method=?]", document_descriptor_path(@document_descriptor), "post" do

      #assert_select "input#document_descriptor_required[name=?]", "document_descriptor[required]"

      assert_select "input#document_descriptor_details[name=?]", "document_descriptor[details]"

      #assert_select "input#document_descriptor_allowed_extensions[name=?]", "document_descriptor[allowed_extensions]"
    end
  end
end
