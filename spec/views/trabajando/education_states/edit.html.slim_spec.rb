require 'rails_helper'

RSpec.describe "trabajando/education_states/edit", type: :view do
  before(:each) do
    @trabajando_education_state = assign(:trabajando_education_state, FactoryGirl.create(:trabajando_education_state))
  end

  it "renders the edit trabajando_education_state form" do
    render

    assert_select "form[action=?][method=?]", trabajando_education_state_path(@trabajando_education_state), "post" do
      assert_select "input#trabajando_education_state_name[name=?]", "trabajando_education_state[name]"
    end
  end
end
