require 'rails_helper'

RSpec.describe "trabajando/countries/edit", type: :view do
  before(:each) do
    @trabajando_country = assign(:trabajando_country, FactoryGirl.create(:trabajando_country))
  end

  it "renders the edit trabajando_country form" do
    render

    assert_select "form[action=?][method=?]", trabajando_country_path(@trabajando_country), "post" do

      assert_select "input#trabajando_country_name[name=?]", "trabajando_country[name]"

      assert_select "select#trabajando_country_country_id[name=?]", "trabajando_country[country_id]"
    end
  end
end
