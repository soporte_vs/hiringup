require 'rails_helper'

RSpec.describe "trabajando/marital_statuses/edit", type: :view do
  before(:each) do
    @trabajando_marital_status = assign(:trabajando_marital_status, FactoryGirl.create(:trabajando_marital_status))
  end

  it "renders the edit trabajando_marital_status form" do
    render

    assert_select "form[action=?][method=?]", trabajando_marital_status_path(@trabajando_marital_status), "post" do
      assert_select "input#trabajando_marital_status_name[name=?]", "trabajando_marital_status[name]"
      assert_select "select#trabajando_marital_status_company_marital_status_id[name=?]","trabajando_marital_status[company_marital_status_id]"
    end
  end
end
