require 'rails_helper'

RSpec.describe "trabajando/publications/new", type: :view do
  before(:each) do
    @course = FactoryGirl.create :course
    assign(:trabajando_publication, @course.trabajando_publications.new)
  end

  it "renders new trabajando_publication form" do
    render

    assert_select "form[action=?][method=?]", trabajando_publication_path(id: @course.id, publication_id: ''), "post" do
      assert_select "input[type=?][name=?]", "text", "trabajando_publication[title]"
      assert_select "textarea[name=?]", "trabajando_publication[description]"

      assert_select "input[type=?][name=?]", "text", "trabajando_publication[q1]"
      assert_select "input[type=?][name=?]", "text", "trabajando_publication[q2]"
      assert_select "input[type=?][name=?]", "text", "trabajando_publication[q3]"
      assert_select "input[type=?][name=?]", "text", "trabajando_publication[q4]"
      assert_select "input[type=?][name=?]", "text", "trabajando_publication[q5]"
    end
  end
end
