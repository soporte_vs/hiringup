require 'rails_helper'

RSpec.describe "trabajando/publications/edit", type: :view do
  before(:each) do
    @trabajando_publication = assign(:trabajando_publication, FactoryGirl.create(:trabajando_publication))
  end

  it "renders the edit trabajando_publication form" do
    render

    assert_select "form[action=?][method=?]", trabajando_publication_path(id: @trabajando_publication.course.id, publication_id: @trabajando_publication.id), "post" do
      assert_select "input[type=?][name=?][value=?]", "text", "trabajando_publication[title]", @trabajando_publication.title
      assert_select "textarea[name=?]", "trabajando_publication[description]", @trabajando_publication.description

      assert_select "input[type=?][name=?][value=?]", "text", "trabajando_publication[q1]", @trabajando_publication.q1
      assert_select "input[type=?][name=?][value=?]", "text", "trabajando_publication[q2]", @trabajando_publication.q2
      assert_select "input[type=?][name=?][value=?]", "text", "trabajando_publication[q3]", @trabajando_publication.q3
      assert_select "input[type=?][name=?][value=?]", "text", "trabajando_publication[q4]", @trabajando_publication.q4
      assert_select "input[type=?][name=?][value=?]", "text", "trabajando_publication[q5]", @trabajando_publication.q5
    end
  end
end
