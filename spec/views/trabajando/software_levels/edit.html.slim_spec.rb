require 'rails_helper'

RSpec.describe "trabajando/software_levels/edit", type: :view do
  before(:each) do
    @trabajando_software_level = assign(:trabajando_software_level, FactoryGirl.create(:trabajando_software_level))
  end

  it "renders the edit trabajando_software_level form" do
    render

    assert_select "form[action=?][method=?]", trabajando_software_level_path(@trabajando_software_level), "post" do
      assert_select "input#trabajando_software_level_name[name=?]", "trabajando_software_level[name]"
    end
  end
end
