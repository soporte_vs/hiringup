require 'rails_helper'

RSpec.describe "trabajando/software_levels/new", type: :view do
  before(:each) do
    assign(:trabajando_software_level, Trabajando::SoftwareLevel.new())
  end

  it "renders new trabajando_software_level form" do
    render

    assert_select "form[action=?][method=?]", trabajando_software_levels_path, "post" do
      assert_select "input#trabajando_software_level_name[name=?]", "trabajando_software_level[name]"
    end
  end
end
