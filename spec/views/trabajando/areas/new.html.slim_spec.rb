require 'rails_helper'

RSpec.describe "trabajando/areas/new", type: :view do
  before(:each) do
    assign(:trabajando_area, FactoryGirl.build(:trabajando_area))
  end

  it "renders new trabajando_area form" do
    render

    assert_select "form[action=?][method=?]", trabajando_areas_path, "post" do
    end
  end
end
