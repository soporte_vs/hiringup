require 'rails_helper'

RSpec.describe "trabajando/areas/edit", type: :view do
  before(:each) do
    @trabajando_area = assign(:trabajando_area, FactoryGirl.create(:trabajando_area))
  end

  it "renders the edit trabajando_area form" do
    render

    assert_select "form[action=?][method=?]", trabajando_area_path(@trabajando_area), "post" do
      assert_select "input#trabajando_area_name[name=?]", "trabajando_area[name]"
    end
  end
end
