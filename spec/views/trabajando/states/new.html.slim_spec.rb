require 'rails_helper'

RSpec.describe "trabajando/states/new", type: :view do
  before(:each) do
    assign(:trabajando_state, Trabajando::State.new(
      :name => "MyString",
      :trabajando_id => 1,
      :hupe_state => nil
    ))
  end

  it "renders new trabajando_state form" do
    render

    assert_select "form[action=?][method=?]", trabajando_states_path, "post" do

      assert_select "input#trabajando_state_name[name=?]", "trabajando_state[name]"

      assert_select "select#trabajando_state_hupe_state_id[name=?]", "trabajando_state[hupe_state_id]"
    end
  end
end
