require 'rails_helper'

RSpec.describe "trabajando/states/edit", type: :view do
  before(:each) do
    @trabajando_state = assign(:trabajando_state, FactoryGirl.create(:trabajando_state))
  end

  it "renders the edit trabajando_state form" do
    render

    assert_select "form[action=?][method=?]", trabajando_state_path(@trabajando_state), "post" do

      assert_select "input#trabajando_state_name[name=?]", "trabajando_state[name]"

      assert_select "select#trabajando_state_hupe_state_id[name=?]", "trabajando_state[hupe_state_id]"
    end
  end
end
