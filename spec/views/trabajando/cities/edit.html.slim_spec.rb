require 'rails_helper'

RSpec.describe "trabajando/cities/edit", type: :view do
  before(:each) do
    @trabajando_city = assign(:trabajando_city, FactoryGirl.create(:trabajando_city))
    Territory::Country.where(name: "Chile").first_or_create
  end

  it "renders the edit trabajando_city form" do
    render

    assert_select "form[action=?][method=?]", trabajando_city_path(@trabajando_city), "post" do

      assert_select "input#trabajando_city_name[name=?]", "trabajando_city[name]"

      assert_select "select#trabajando_city_hupe_city_id[name=?]", "trabajando_city[hupe_city_id]"
    end
  end
end
