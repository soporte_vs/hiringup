require 'rails_helper'

RSpec.describe "trabajando/job_types/new", type: :view do
  before(:each) do
    assign(:trabajando_job_type, FactoryGirl.build(:trabajando_job_type))
  end

  it "renders new trabajando_job_type form" do
    render

    assert_select "form[action=?][method=?]", trabajando_job_types_path, "post" do
      assert_select "input#trabajando_job_type_name[name=?]", "trabajando_job_type[name]"
      assert_select "select#trabajando_job_type_company_position_ids[name=?]","trabajando_job_type[company_position_ids][]"
    end
  end
end
