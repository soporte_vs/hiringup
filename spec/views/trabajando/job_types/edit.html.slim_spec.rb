require 'rails_helper'

RSpec.describe "trabajando/job_types/edit", type: :view do
  before(:each) do
    @trabajando_job_type = assign(:trabajando_job_type, FactoryGirl.create(:trabajando_job_type))
  end

  it "renders the edit trabajando_job_type form" do
    render

    assert_select "form[action=?][method=?]", trabajando_job_type_path(@trabajando_job_type), "post" do
      assert_select "input#trabajando_job_type_name[name=?]", "trabajando_job_type[name]"
      assert_select "select#trabajando_job_type_company_position_ids[name=?]","trabajando_job_type[company_position_ids][]"
    end
  end
end
