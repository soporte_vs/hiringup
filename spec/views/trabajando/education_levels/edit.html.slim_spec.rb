require 'rails_helper'

RSpec.describe "trabajando/education_levels/edit", type: :view do
  before(:each) do
    @trabajando_education_level = FactoryGirl.create(:trabajando_education_level)
  end

  it "renders the edit trabajando_education_level form" do
    render

    assert_select "form[action=?][method=?]", trabajando_education_level_path(@trabajando_education_level), "post" do
      assert_select "input#trabajando_education_level_name[name=?]", "trabajando_education_level[name]"
    end
  end
end
