require "rails_helper"

RSpec.describe "worker/bases/search" do
  login_executive_with_role :admin
  
  context "check links to worker" do
    it "should has 1 link to 1 worker" do
      worker = FactoryGirl.create(:worker_basis)
      assign(:workers, Worker::Base.page)
      render
      assert_select("a[href='"+worker_basis_path(worker)+"']", 1)
    end
  end

end