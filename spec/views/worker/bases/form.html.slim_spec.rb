require "rails_helper"

RSpec.describe "worker/bases/_form" do
  login_executive_with_role :admin

  context "check links to worker" do
    it "should has 1 link to 1 worker" do
      worker = FactoryGirl.create(:worker_basis)
      assign(:worker, worker)
      render
      assert_select "form[action=?][method=?]", worker_basis_path(worker), "post" do
        assert_select "input[name=?]", "worker[user_attributes][personal_information_attributes][first_name]"
        assert_select "input[name=?]", "worker[user_attributes][personal_information_attributes][last_name]"
        assert_select "input[name=?]", "worker[user_attributes][personal_information_attributes][landline]"
        assert_select "input[name=?]", "worker[user_attributes][personal_information_attributes][cellphone]"

        People::PersonalInformation::SEX_OPTIONS.each do |option|
          assert_select "input[type=?][name=?][value=?]", "radio", "worker[user_attributes][personal_information_attributes][sex]", option
        end
      end
    end
  end

end
