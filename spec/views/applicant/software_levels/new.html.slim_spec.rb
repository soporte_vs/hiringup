require 'rails_helper'

RSpec.describe "applicant/software_levels/new", type: :view do
  before(:each) do
    assign(:applicant_software_level, Applicant::SoftwareLevel.new(
      :name => "MyString"
    ))
  end

  it "renders new applicant_software_level form" do
    render

    assert_select "form[action=?][method=?]", applicant_software_levels_path, "post" do

      assert_select "input#applicant_software_level_name[name=?]", "applicant_software_level[name]"
    end
  end
end
