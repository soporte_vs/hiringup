require 'rails_helper'

RSpec.describe "applicant/software_levels/edit", type: :view do
  before(:each) do
    @applicant_software_level = assign(:applicant_software_level, Applicant::SoftwareLevel.create!(
      :name => "MyString"
    ))
  end

  it "renders the edit applicant_software_level form" do
    render

    assert_select "form[action=?][method=?]", applicant_software_level_path(@applicant_software_level), "post" do

      assert_select "input#applicant_software_level_name[name=?]", "applicant_software_level[name]"
    end
  end
end
