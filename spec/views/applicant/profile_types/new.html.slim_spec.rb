require 'rails_helper'

RSpec.describe "applicant/profile_types/new", type: :view do
  before(:each) do
    assign(:applicant_profile_type, Applicant::ProfileType.new(
      :name => "MyString"
    ))
  end

  it "renders new applicant_profile_type form" do
    render

    assert_select "form[action=?][method=?]", applicant_profile_types_path, "post" do

      assert_select "input#applicant_profile_type_name[name=?]", "applicant_profile_type[name]"
    end
  end
end
