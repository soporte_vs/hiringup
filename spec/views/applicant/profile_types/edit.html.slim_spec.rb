require 'rails_helper'

RSpec.describe "applicant/profile_types/edit", type: :view do
  before(:each) do
    @applicant_profile_type = assign(:applicant_profile_type, Applicant::ProfileType.create!(
      :name => "MyString"
    ))
  end

  it "renders the edit applicant_profile_type form" do
    render

    assert_select "form[action=?][method=?]", applicant_profile_type_path(@applicant_profile_type), "post" do

      assert_select "input#applicant_profile_type_name[name=?]", "applicant_profile_type[name]"
    end
  end
end
