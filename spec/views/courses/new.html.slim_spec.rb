require "rails_helper"

RSpec.describe "courses/new" do
  context "Get only courses that current user has role to show" do
    before(:each) do
      @course = FactoryGirl.build(:course_with_tags)
      assign(:course, @course)
    end
    
    it "should has one link to a course dashboard" do
      render
      
      assert_select "form[action=?][method=?]", courses_path, "post" do
        assert_select "input[name=?]", "course[title]"
        assert_select "textarea[name=?]", "course[description]"
        assert_select "select[name=?]", "course[type_enrollment]"
        assert_select "select[name=?][multiple=?]", "course[tags][]","multiple" do
          @course.tags.each do |tag|
            assert_select "option[value=?]", "#{tag.id}"
            expect(assert_select("option[value=?]", "#{tag.id}").text).to eq(tag.name)
          end 
        end
      end
    
    end
  end
end