require 'rails_helper'

RSpec.describe "education/institutions/new", type: :view do
  before(:each) do
    assign(:education_institution, Education::Institution.new(
      :type => "",
      :name => "MyString"
    ))
  end

  it "renders new education_institution form" do
    render

    assert_select "form[action=?][method=?]", education_institutions_path, "post" do
      assert_select "input#education_institution_name[name=?]", "education_institution[name]"
    end
  end
end
