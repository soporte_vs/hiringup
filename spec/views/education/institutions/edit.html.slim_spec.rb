require 'rails_helper'

RSpec.describe "education/institutions/edit", type: :view do
  before(:each) do
    @education_institution = assign(:education_institution, Education::Institution.create!(
      :type => "",
      :name => "MyString"
    ))
  end

  it "renders the edit education_institution form" do
    render

    assert_select "form[action=?][method=?]", education_institution_path(@education_institution), "post" do
      assert_select "input#education_institution_name[name=?]", "education_institution[name]"
    end
  end
end
