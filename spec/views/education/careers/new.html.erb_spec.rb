require 'rails_helper'

RSpec.describe "education/careers/new", type: :view do
  before(:each) do
    assign(:education_career, Education::Career.new(
      :name => "MyString"
    ))
  end

  it "renders new education_career form" do
    render

    assert_select "form[action=?][method=?]", education_careers_path, "post" do

      assert_select "input#education_career_name[name=?]", "education_career[name]"
    end
  end
end
