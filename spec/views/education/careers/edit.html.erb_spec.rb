require 'rails_helper'

RSpec.describe "education/careers/edit", type: :view do
  before(:each) do
    @education_career = assign(:education_career, Education::Career.create!(
      :name => "MyString"
    ))
  end

  it "renders the edit education_career form" do
    render

    assert_select "form[action=?][method=?]", education_career_path(@education_career), "post" do

      assert_select "input#education_career_name[name=?]", "education_career[name]"
    end
  end
end
