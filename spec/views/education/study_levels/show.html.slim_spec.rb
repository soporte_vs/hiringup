require 'rails_helper'

RSpec.describe "education/study_levels/show", type: :view do
  before(:each) do
    @education_study_level = assign(:education_study_level, Education::StudyLevel.create!(
      :name => "Name"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
  end
end
