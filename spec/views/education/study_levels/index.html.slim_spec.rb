require 'rails_helper'

RSpec.describe "education/study_levels/index", type: :view do
  before(:each) do
    FactoryGirl.create(:education_study_level)
    FactoryGirl.create(:education_study_level)
    assign(:education_study_levels, Education::StudyLevel.page(params[:page]))
  end

  it "renders a list of education/study_levels" do
    render
    assert_select "tbody>tr", :count => 2
  end
end
