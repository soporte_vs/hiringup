require 'rails_helper'

RSpec.describe "education/study_levels/new", type: :view do
  before(:each) do
    assign(:education_study_level, Education::StudyLevel.new(
      :name => "MyString"
    ))
  end

  it "renders new education_study_level form" do
    render

    assert_select "form[action=?][method=?]", education_study_levels_path, "post" do

      assert_select "input#education_study_level_name[name=?]", "education_study_level[name]"
    end
  end
end
