require 'rails_helper'

RSpec.describe "education/study_levels/edit", type: :view do
  before(:each) do
    @education_study_level = assign(:education_study_level, Education::StudyLevel.create!(
      :name => "MyString"
    ))
  end

  it "renders the edit education_study_level form" do
    render

    assert_select "form[action=?][method=?]", education_study_level_path(@education_study_level), "post" do

      assert_select "input#education_study_level_name[name=?]", "education_study_level[name]"
    end
  end
end
