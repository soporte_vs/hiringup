require "rails_helper"

RSpec.describe "applicant/bases/edit" do

  context "EDIT form to applicant" do
    login_executive_with_role :admin

    it "should render form to applicant_catched with name applicant" do
      applicant_catched = FactoryGirl.create(:applicant_catched)
      assign(:applicant, applicant_catched)
      assign(:document_records, [])
      render
      assert_select "form[action=?][method=?]", applicant_basis_path(applicant_catched), "post" do
        assert_select "input[name=?]", "applicant[user_attributes][personal_information_attributes][first_name]"
        assert_select "input[name=?]", "applicant[user_attributes][personal_information_attributes][last_name]"
        assert_select "input[name=?]", "applicant[user_attributes][personal_information_attributes][landline]"
        assert_select "input[name=?]", "applicant[user_attributes][personal_information_attributes][cellphone]"
        assert_select "input[name=?]", "applicant[user_attributes][personal_information_attributes][birth_date]"

        People::PersonalInformation::SEX_OPTIONS.each do |option|
          assert_select "input[type=?][name=?][value=?]", "radio", "applicant[user_attributes][personal_information_attributes][sex]", option
        end
      end
    end
  end

end
