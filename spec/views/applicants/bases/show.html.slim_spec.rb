require "rails_helper"

RSpec.describe "applicant/bases/show" do

  context "Show view of applicant base" do
    login_executive_with_role :admin

    it "should render correctly" do
      courses = [FactoryGirl.create(:course)]
      applicant = FactoryGirl.create(:applicant_bases)

      education_careers_available = [FactoryGirl.create(:education_career)]

      assign(:applicant, applicant)
      assign(:courses, courses)

      assign(:education_careers_available, education_careers_available)

      render

      expect{have_link(href: enroll_applicant_basis_path(applicant))}.not_to raise_error

    end
  end
end
