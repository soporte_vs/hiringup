require 'rails_helper'

RSpec.describe "company/white_ruts/edit", type: :view do
  before(:each) do
    @company_white_rut = assign(:company_white_rut, FactoryGirl.create(:company_white_rut))
  end

  it "renders the edit company_white_rut form" do
    render

    assert_select "form[action=?][method=?]", company_white_rut_path(@company_white_rut), "post" do

      assert_select "input#company_white_rut_rut[name=?]", "company_white_rut[rut]"
    end
  end
end
