require 'rails_helper'

RSpec.describe "company/white_ruts/new", type: :view do
  before(:each) do
    assign(:company_white_rut, FactoryGirl.build(:company_white_rut))
  end

  it "renders new company_white_rut form" do
    render

    assert_select "form[action=?][method=?]", company_white_ruts_path, "post" do

      assert_select "input#company_white_rut_rut[name=?]", "company_white_rut[rut]"
    end
  end
end
