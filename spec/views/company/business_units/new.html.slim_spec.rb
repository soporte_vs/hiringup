require 'rails_helper'

RSpec.describe "company/business_units/new", type: :view do
  before(:each) do
    assign(:company_business_unit, FactoryGirl.build(:company_business_unit))
  end

  it "renders new company_business_unit form" do
    render

    assert_select "form[action=?][method=?]", company_business_units_path, "post" do

      assert_select "input#company_business_unit_name[name=?]", "company_business_unit[name]"

      assert_select "textarea#company_business_unit_description[name=?]", "company_business_unit[description]"
    end
  end
end
