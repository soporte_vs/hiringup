require 'rails_helper'

RSpec.describe "company/business_units/edit", type: :view do
  before(:each) do
    @company_business_unit = assign(:company_business_unit, FactoryGirl.create(:company_business_unit))
  end

  it "renders the edit company_business_unit form" do
    render

    assert_select "form[action=?][method=?]", company_business_unit_path(@company_business_unit), "post" do

      assert_select "input#company_business_unit_name[name=?]", "company_business_unit[name]"

      assert_select "textarea#company_business_unit_description[name=?]", "company_business_unit[description]"
    end
  end
end
