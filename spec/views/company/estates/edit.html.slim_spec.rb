require 'rails_helper'

RSpec.describe "company/estates/edit", type: :view do
  before(:each) do
    @company_estate = assign(:company_estate, Company::Estate.create!(
      :name => "MyString"
    ))
  end

  it "renders the edit company_estate form" do
    render

    assert_select "form[action=?][method=?]", company_estate_path(@company_estate), "post" do

      assert_select "input#company_estate_name[name=?]", "company_estate[name]"
    end
  end
end
