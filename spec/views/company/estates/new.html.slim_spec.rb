require 'rails_helper'

RSpec.describe "company/estates/new", type: :view do
  before(:each) do
    assign(:company_estate, Company::Estate.new(
      :name => "MyString"
    ))
  end

  it "renders new company_estate form" do
    render

    assert_select "form[action=?][method=?]", company_estates_path, "post" do

      assert_select "input#company_estate_name[name=?]", "company_estate[name]"
    end
  end
end
