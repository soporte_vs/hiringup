require 'rails_helper'

RSpec.describe "company/records/new", type: :view do
  before(:each) do
    assign(:company_record, FactoryGirl.build(:company_record))
  end

  it "renders new company_record form" do
    render

    assert_select "form[action=?][method=?]", company_records_path, "post" do

      assert_select "input#company_record_name[name=?]", "company_record[name]"
      assert_select "input#company_record_email_domain[name=?]", "company_record[email_domain]"
    end
  end
end
