require 'rails_helper'

RSpec.describe "company/records/edit", type: :view do
  before(:each) do
    @company_record = assign(:company_record, FactoryGirl.create(:company_record))
  end

  it "renders the edit company_record form" do
    render

    assert_select "form[action=?][method=?]", company_record_path(@company_record), "post" do

      assert_select "input#company_record_name[name=?]", "company_record[name]"
      assert_select "input#company_record_email_domain[name=?]", "company_record[email_domain]"
    end
  end
end
