require 'rails_helper'

RSpec.describe "company/recruitment_sources/edit", type: :view do
  before(:each) do
    @company_recruitment_source = assign(:company_recruitment_source, FactoryGirl.create(:company_recruitment_source))
  end

  it "renders the edit company_recruitment_source form" do
    render

    assert_select "form[action=?][method=?]", company_recruitment_source_path(@company_recruitment_source), "post" do

      assert_select "input#company_recruitment_source_name[name=?]", "company_recruitment_source[name]"

    end
  end
end
