require 'rails_helper'

RSpec.describe "company/areas/edit", type: :view do
  before(:each) do
    @company_area = assign(:company_area, FactoryGirl.create(:company_area))
  end

  it "renders the edit company_area form" do
    render

    assert_select "form[action=?][method=?]", company_area_path(@company_area), "post" do

      assert_select "input#company_area_name[name=?]", "company_area[name]"
    end
  end
end
