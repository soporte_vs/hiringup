require 'rails_helper'

RSpec.describe "company/areas/new", type: :view do
  before(:each) do
    assign(:company_area, FactoryGirl.build(:company_area))
  end

  it "renders new company_area form" do
    render

    assert_select "form[action=?][method=?]", company_areas_path, "post" do

      assert_select "input#company_area_name[name=?]", "company_area[name]"
    end
  end
end
