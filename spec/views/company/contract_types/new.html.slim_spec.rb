require 'rails_helper'

RSpec.describe "company/contract_types/new", type: :view do
  before(:each) do
    assign(:company_contract_type, FactoryGirl.build(:company_contract_type))
  end

  it "renders new company_contract_type form" do
    render

    assert_select "form[action=?][method=?]", company_contract_types_path, "post" do

      assert_select "input#company_contract_type_name[name=?]", "company_contract_type[name]"

      assert_select "textarea#company_contract_type_description[name=?]", "company_contract_type[description]"
    end
  end
end
