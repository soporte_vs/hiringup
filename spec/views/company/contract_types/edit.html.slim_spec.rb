require 'rails_helper'

RSpec.describe "company/contract_types/edit", type: :view do
  before(:each) do
    @company_contract_type = assign(:company_contract_type, FactoryGirl.create(:company_contract_type))
  end

  it "renders the edit company_contract_type form" do
    render

    assert_select "form[action=?][method=?]", company_contract_type_path(@company_contract_type), "post" do

      assert_select "input#company_contract_type_name[name=?]", "company_contract_type[name]"

      assert_select "textarea#company_contract_type_description[name=?]", "company_contract_type[description]"
    end
  end
end
