require 'rails_helper'

RSpec.describe "company/marital_statuses/new", type: :view do
  before(:each) do
    assign(:company_marital_status, FactoryGirl.build(:company_marital_status))
  end

  it "renders new company_marital_status form" do
    render

    assert_select "form[action=?][method=?]", company_marital_statuses_path, "post" do

      assert_select "input#company_marital_status_name[name=?]", "company_marital_status[name]"
    end
  end
end
