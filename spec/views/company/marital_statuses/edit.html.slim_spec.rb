require 'rails_helper'

RSpec.describe "company/marital_statuses/edit", type: :view do
  before(:each) do
    @company_marital_status = assign(:company_marital_status, FactoryGirl.create(:company_marital_status))
  end

  it "renders the edit company_marital_status form" do
    render

    assert_select "form[action=?][method=?]", company_marital_status_path(@company_marital_status), "post" do

      assert_select "input#company_marital_status_name[name=?]", "company_marital_status[name]"
    end
  end
end
