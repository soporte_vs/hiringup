require 'rails_helper'

RSpec.describe "company/vacancy_request_reasons/edit", type: :view do
  before(:each) do
    @company_vacancy_request_reason = assign(:company_vacancy_request_reason, FactoryGirl.create(:company_vacancy_request_reason))
  end

  it "renders the edit company_vacancy_request_reason form" do
    render

    assert_select "form[action=?][method=?]", company_vacancy_request_reason_path(@company_vacancy_request_reason), "post" do

      assert_select "input#company_vacancy_request_reason_name[name=?]", "company_vacancy_request_reason[name]"
    end
  end
end
