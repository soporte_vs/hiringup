require 'rails_helper'

RSpec.describe "company/vacancy_request_reasons/new", type: :view do
  before(:each) do
    assign(:company_vacancy_request_reason, FactoryGirl.build(:company_vacancy_request_reason))
  end

  it "renders new company_vacancy_request_reason form" do
    render

    assert_select "form[action=?][method=?]", company_vacancy_request_reasons_path, "post" do

      assert_select "input#company_vacancy_request_reason_name[name=?]", "company_vacancy_request_reason[name]"
    end
  end
end
