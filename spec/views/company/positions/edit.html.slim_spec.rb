require 'rails_helper'

RSpec.describe "company/positions/edit", type: :view do
  before(:each) do
    @company_position = assign(:company_position, FactoryGirl.create(:company_position))
  end

  it "renders the edit company_position form" do
    render

    assert_select "form[action=?][method=?]", company_position_path(@company_position), "post" do

      assert_select "input#company_position_name[name=?]", "company_position[name]"

      assert_select "textarea#company_position_description[name=?]", "company_position[description]"
    end
  end
end
