require 'rails_helper'

RSpec.describe "company/positions/new", type: :view do
  before(:each) do
    assign(:company_position, FactoryGirl.build(:company_position))
  end

  it "renders new company_position form" do
    render

    assert_select "form[action=?][method=?]", company_positions_path, "post" do

      assert_select "input#company_position_name[name=?]", "company_position[name]"

      assert_select "textarea#company_position_description[name=?]", "company_position[description]"
    end
  end
end
