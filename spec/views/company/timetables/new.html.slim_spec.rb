require 'rails_helper'

RSpec.describe "company/timetables/new", type: :view do
  before(:each) do
    assign(:company_timetable, Company::Timetable.new(
      :name => "MyString",
      :description => "MyText"
    ))
  end

  it "renders new timetable form" do
    render

    assert_select "form[action=?][method=?]", company_timetables_path, "post" do

      assert_select "input#company_timetable_name[name=?]", "company_timetable[name]"

      assert_select "textarea#company_timetable_description[name=?]", "company_timetable[description]"
    end
  end
end
