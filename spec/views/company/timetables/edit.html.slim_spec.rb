require 'rails_helper'

RSpec.describe "company/timetables/edit", type: :view do
  before(:each) do
    @company_timetable = assign(:company_timetable, Company::Timetable.create!(
      :name => "MyString",
      :description => "MyText"
    ))
  end

  it "renders the edit timetable form" do
    render

    assert_select "form[action=?][method=?]", company_timetable_path(@company_timetable), "post" do

      assert_select "input#company_timetable_name[name=?]", "company_timetable[name]"

      assert_select "textarea#company_timetable_description[name=?]", "company_timetable[description]"
    end
  end
end
