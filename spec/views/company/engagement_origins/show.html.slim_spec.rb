require 'rails_helper'

RSpec.describe "company/engagement_origins/show", type: :view do
  before(:each) do
    @company_engagement_origin = assign(:company_engagement_origin, Company::EngagementOrigin.create!(
      :name => "Name",
      :description => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/MyText/)
  end
end
