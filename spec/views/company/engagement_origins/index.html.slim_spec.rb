require 'rails_helper'

RSpec.describe "company/engagement_origins/index", type: :view do
  before(:each) do
    FactoryGirl.create(:company_engagement_origin)
    FactoryGirl.create(:company_engagement_origin)
    assign(:company_engagement_origins, Company::EngagementOrigin.page(params[:page]))
  end

  it "renders a list of company/engagement_origins" do
    render
    assert_select "tbody>tr", :count => 2
  end
end
