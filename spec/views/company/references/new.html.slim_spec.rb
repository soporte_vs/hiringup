require 'rails_helper'

RSpec.xdescribe "company/references/new", type: :view do
  before(:each) do
    assign(:company_reference, FactoryGirl.build(:company_reference))
  end

  it "renders new company_reference form" do
    render

    assert_select "form[action=?][method=?]", company_references_path, "post" do
      assert_select "input#company_reference_email_name[name=?]", "company_reference[email_name]"
      assert_select "select#company_reference_email_domain[name=?]", "company_reference[email_domain]"
      assert_select "input#company_reference_first_name[name=?]", "company_reference[first_name]"
      assert_select "input#company_reference_last_name[name=?]", "company_reference[last_name]"
      assert_select "input#company_reference_position[name=?]", "company_reference[position]"
      assert_select "input#company_reference_applicant_attributes_user_attributes_email[name=?]", "company_reference[applicant_attributes][user_attributes][email]"
      assert_select "input#company_reference_applicant_attributes_user_attributes_personal_information_attributes_first_name[name=?]", "company_reference[applicant_attributes][user_attributes][personal_information_attributes][first_name]"
      assert_select "input#company_reference_applicant_attributes_user_attributes_personal_information_attributes_last_name[name=?]", "company_reference[applicant_attributes][user_attributes][personal_information_attributes][last_name]"
      assert_select "select#company_reference_position_ids[name=?]", "company_reference[position_ids][]"
      assert_select "textarea#company_reference_observations[name=?]", "company_reference[observations]"
    end
  end
end
