require "rails_helper"

RSpec.describe "vacancies/assign_course" do
  context 'render' do
    before :each do
      course = FactoryGirl.create(:course)
      company_cenco = FactoryGirl.create(:company_cenco)
      course.company_position.company_cencos << company_cenco
      company_positions_cenco = course.company_position.company_positions_cencos.first
      @vacancies = FactoryGirl.create_list(:vacancy, 3, course: course, company_positions_cenco: company_positions_cenco)
    end

    it 'should render without errors' do
      render
      json_response = JSON.parse(response.body)
      expect(json_response).to match(
        @vacancies.map do |vacancy|
          {
            "id" => vacancy.id,
            "created_at" => I18n.l(vacancy.created_at, format: :short),
            "updated_at" => I18n.l(vacancy.updated_at, format: :short),
            "course" => {
              "id" => vacancy.course.try(:id),
              "title" => vacancy.course.try(:title),
              "url" => vacancy.course.try(:persisted?) ? course_url(vacancy.course) : nil
            },
            "errors" => {}
          }
        end
      )
    end

    it 'should render with error update 403' do
      error_message_forbiden = I18n.t(
        'errors.messages.forbiden.f',
        object_name: Vacancy.model_name.human
      )
      error_message_course = I18n.t('activerecord.errors.models.vacancy.attributes.course_id.inconsistent_agreement_course')
      @vacancies.each do |vacancy|
        vacancy.errors.add(:update, error_message_forbiden)
        vacancy.errors.add(:course_id, error_message_course)
      end
      render
      json_response = JSON.parse(response.body)

      expect(json_response).to match(
        @vacancies.map do |vacancy|
          {
            "id" => vacancy.id,
            "created_at" => I18n.l(vacancy.created_at, format: :short),
            "updated_at" => I18n.l(vacancy.updated_at, format: :short),
            "course" => {
              "id" => vacancy.course.try(:id),
              "title" => vacancy.course.try(:title),
              "url" => vacancy.course.try(:persisted?) ? course_url(vacancy.course) : nil
            },
            "errors" => {
              "update" => [error_message_forbiden],
              "course_id" => [error_message_course]
            }
          }
        end
      )
    end
  end
end