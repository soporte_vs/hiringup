require 'rails_helper'

RSpec.describe "minisite/banners/edit", type: :view do
  before(:each) do
    @minisite_banner = assign(:minisite_banner, FactoryGirl.create(:minisite_banner))
  end

  it "renders the edit minisite_banner form" do
    render

    assert_select "form[action=?][method=?]", minisite_banner_path(@minisite_banner), "post" do

      assert_select "input#minisite_banner_title[name=?]", "minisite_banner[title]"

      assert_select "input#minisite_banner_image[name=?]", "minisite_banner[image]"

      assert_select "input#minisite_banner_active_image[name=?]", "minisite_banner[active_image]"
    end
  end
end
