require 'rails_helper'

RSpec.describe "minisite/banners/new", type: :view do
  before(:each) do
    assign(:minisite_banner, Minisite::Banner.new(
      :title => "MyString",
      :image => "MyString",
      :active_image => false
    ))
  end

  it "renders new minisite_banner form" do
    render

    assert_select "form[action=?][method=?]", minisite_banners_path, "post" do

      assert_select "input#minisite_banner_title[name=?]", "minisite_banner[title]"

      assert_select "input#minisite_banner_image[name=?]", "minisite_banner[image]"

      assert_select "input#minisite_banner_active_image[name=?]", "minisite_banner[active_image]"
    end
  end
end
