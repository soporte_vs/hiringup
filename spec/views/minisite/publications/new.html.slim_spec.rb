require 'rails_helper'

RSpec.describe "minisite/publications/new", type: :view do
  before(:each) do
    @course = FactoryGirl.create :course
    assign(:minisite_publication, @course.minisite_publications.new)
  end

  it "renders new minisite_publication form" do
    render

    assert_select "form[action=?][method=?]", minisite_publication_path(id: @course.id, publication_id: ''), "post" do
      assert_select "input[type=?][name=?]", "text", "minisite_publication[title]"
      assert_select "textarea[name=?]", "minisite_publication[description]"
    end
  end
end
