require 'rails_helper'

RSpec.describe "minisite/publications/edit", type: :view do
  before(:each) do
    @minisite_publication = assign(:minisite_publication, FactoryGirl.create(:minisite_publication))
  end

  it "renders the edit minisite_publication form" do
    render
    
    assert_select "form[action=?][method=?]", minisite_publication_path(id: @minisite_publication.course.id, publication_id: @minisite_publication.id), "post" do
      assert_select "input[type=?][name=?][value=?]", "text", "minisite_publication[title]", @minisite_publication.title
      assert_select "textarea[name=?]", "minisite_publication[description]", @minisite_publication.description
    end
  end
end
