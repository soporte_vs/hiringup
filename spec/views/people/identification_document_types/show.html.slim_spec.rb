require 'rails_helper'

RSpec.describe "people/identification_document_types/show", type: :view do
  before(:each) do
    @people_identification_document_type = assign(:people_identification_document_type, People::IdentificationDocumentType.create!(
      :name => "Type",
      :country => Territory::Country.last || FactoryGirl.create(:territory_country),
      :validation_regex => "MyText",
      :validate_uniqueness => false
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Type/)
    expect(rendered).to match(/#{@people_identification_document_type.country.name}/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/No/)
  end
end
