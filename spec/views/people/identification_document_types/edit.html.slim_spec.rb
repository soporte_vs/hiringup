require 'rails_helper'

RSpec.describe "people/identification_document_types/edit", type: :view do
  before(:each) do
    @people_identification_document_type = assign(:people_identification_document_type, People::IdentificationDocumentType.create!(
      :name => "Nombre",
      :country => Territory::Country.last || FactoryGirl.create(:territory_country),
      :validation_regex => "MyText",
      :validate_uniqueness => false
    ))
  end

  it "renders the edit people_identification_document_type form" do
    render

    assert_select "form[action=?][method=?]", people_identification_document_type_path(@people_identification_document_type), "post" do

      assert_select "input#people_identification_document_type_name[name=?]", "people_identification_document_type[name]"


      assert_select "select#people_identification_document_type_country_id[name=?]", "people_identification_document_type[country_id]"

      assert_select "input#people_identification_document_type_validation_regex[name=?]", "people_identification_document_type[validation_regex]"

      assert_select "input#people_identification_document_type_validate_uniqueness[name=?]", "people_identification_document_type[validate_uniqueness]"
    end
  end
end
