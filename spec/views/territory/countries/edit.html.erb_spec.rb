require 'rails_helper'

RSpec.describe "territory/countries/edit", type: :view do
  before(:each) do
    @territory_country = assign(:territory_country, Territory::Country.create!(
      :name => "MyString"
    ))
  end

  it "renders the edit territory_country form" do
    render

    assert_select "form[action=?][method=?]", territory_country_path(@territory_country), "post" do

      assert_select "input#territory_country_name[name=?]", "territory_country[name]"
    end
  end
end
