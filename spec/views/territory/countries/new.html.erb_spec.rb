require 'rails_helper'

RSpec.describe "territory/countries/new", type: :view do
  before(:each) do
    assign(:territory_country, Territory::Country.new(
      :name => "MyString"
    ))
  end

  it "renders new territory_country form" do
    render

    assert_select "form[action=?][method=?]", territory_countries_path, "post" do

      assert_select "input#territory_country_name[name=?]", "territory_country[name]"
    end
  end
end
