require 'rails_helper'

RSpec.describe "territory/cities/edit", type: :view do
  before(:each) do
    @territory_city = assign(:territory_city, FactoryGirl.create(:territory_city))
  end

  it "renders the edit territory_city form" do
    render

    assert_select "form[action=?][method=?]", territory_city_path(@territory_city), "post" do

      assert_select "input#territory_city_name[name=?]", "territory_city[name]"
      
    end
  end
end
