require 'rails_helper'

RSpec.describe "territory/cities/new", type: :view do
  before(:each) do
    assign(:territory_city, FactoryGirl.build(:territory_city))
  end

  it "renders new territory_city form" do
    render
    
    assert_select "form[action=?][method=?]", territory_cities_path, "post" do

      assert_select "input#territory_city_name[name=?]", "territory_city[name]"

    end
  end
end
