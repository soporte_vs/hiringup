require 'rails_helper'

RSpec.describe "territory/states/edit", type: :view do
  before(:each) do
    @territory_state = assign(:territory_state, FactoryGirl.create(:territory_state))
  end

  it "renders the edit territory_state form" do
    render

    assert_select "form[action=?][method=?]", territory_state_path(@territory_state), "post" do
      assert_select "input#territory_state_name[name=?]", "territory_state[name]"

      assert_select "select#territory_state_territory_country[name=?]", "territory_state[territory_country]"
    end
  end
end
