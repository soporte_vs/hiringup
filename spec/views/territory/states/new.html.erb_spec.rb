require 'rails_helper'

RSpec.describe "territory/states/new", type: :view do
  before(:each) do
    assign(:territory_state, Territory::State.new(
      :name => "MyString",
      :territory_country => nil
    ))
  end

  it "renders new territory_state form" do
    render

    assert_select "form[action=?][method=?]", territory_states_path, "post" do

      assert_select "input#territory_state_name[name=?]", "territory_state[name]"

      assert_select "select#territory_state_territory_country[name=?]", "territory_state[territory_country]"
    end
  end
end
