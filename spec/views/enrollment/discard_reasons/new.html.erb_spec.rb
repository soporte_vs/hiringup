require 'rails_helper'

RSpec.describe "enrollment/discard_reasons/new", :type => :view do
  before(:each) do
    assign(:enrollment_discard_reason, FactoryGirl.build(:enrollment_discard_reason))
  end

  it "renders new enrollment_discard_reason form" do
    render

    assert_select "form[action=?][method=?]", enrollment_discard_reasons_path, "post" do

      assert_select "input#enrollment_discard_reason_name[name=?]", "enrollment_discard_reason[name]"

      assert_select "textarea#enrollment_discard_reason_description[name=?]", "enrollment_discard_reason[description]"
    end
  end
end
