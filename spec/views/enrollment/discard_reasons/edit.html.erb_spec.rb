require 'rails_helper'

RSpec.describe "enrollment/discard_reasons/edit", :type => :view do
  before(:each) do
    @enrollment_discard_reason = assign(:enrollment_discard_reason, FactoryGirl.create(:enrollment_discard_reason))
  end

  it "renders the edit discard_reason form" do
    render

    assert_select "form[action=?][method=?]", enrollment_discard_reason_path(@enrollment_discard_reason), "post" do

      assert_select "input#enrollment_discard_reason_name[name=?]", "enrollment_discard_reason[name]"

      assert_select "textarea#enrollment_discard_reason_description[name=?]", "enrollment_discard_reason[description]"
    end
  end
end
