FactoryGirl.define do
  factory :education_institution, :class => 'Education::Institution' do
    type ""
    name { FFaker::Education.school }
    country_id { Territory::Country.try(:last).try(:id) || FactoryGirl.create(:territory_country) }
    # custom false

    factory :education_institution_invalid do
      name ""
    end

    factory :education_institution_custom do
      custom true
    end

    factory :education_university do
      type "Education::University"
    end
  end
end
