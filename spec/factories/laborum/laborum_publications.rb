FactoryGirl.define do
  factory :laborum_publication, :class => 'Laborum::Publication' do
    service_id nil
    title { FFaker::Lorem.sentence }
    description { FFaker::Lorem.sentence }
    desactivate_at { Date.today }
    state_id nil
    city_id nil
    address { FFaker::Address.street_address }
    area_id 1
    subarea_id 1
    pay_id 1
    language_id nil
    language_level_id nil
    type_job_id nil
    study_area_id nil
    study_type_id 1
    study_status_id 1
    status nil
    question1 'pregunta1'
    question2 'pregunta2'
    question3 'pregunta3'
    question4 'pregunta4'
    question5 'pregunta5'
    course { Course.last || FactoryGirl.create(:course) }
  end

end
