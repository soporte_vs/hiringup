FactoryGirl.define do
  factory :laborum_postulation, :class => 'Laborum::Postulation' do
    laborum_publication_id { FactoryGirl.create(:laborum_publication).id }
    applicant { FactoryGirl.create(:laborum_applicant)}
    answer1 "Respuesta pregunta 1"
    answer2 "Respuesta pregunta 2"
    answer3 "Respuesta pregunta 3"
    answer4 "Respuesta pregunta 4"
    answer5 "Respuesta pregunta 5"
    information nil
    user_portal_id 1
    cv nil
  end

end
