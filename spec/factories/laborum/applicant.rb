FactoryGirl.define do
  factory :laborum_applicant, class: 'Laborum::Applicant' do
    user_attributes { FactoryGirl.attributes_for(:user) }
  end
end
