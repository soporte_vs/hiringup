FactoryGirl.define do
  factory :offer_letter, :class => 'Offer::Letter' do
    company_position {FactoryGirl.create(:company_position)}
    company_contract_type {FactoryGirl.create(:company_contract_type)}
    vacancy_id { FactoryGirl.create(:vacancy).id }
    stage { FactoryGirl.create(:offer_stage) }
    direct_boss { FFaker::Name.name }
    net_remuneration { 100000 + Random.rand(1000000) }
    address { FFaker::Address.street_address }
    zone_assignation 1
    contract_start_date { DateTime.now + 2.days }

    factory :offer_letter_accepted, :class => Offer::Letter do
      accepted true
    end

    factory :offer_letter_rejected, :class => Offer::Letter do
      offer_reject_reason { FactoryGirl.create(:offer_reject_reason) }
      accepted false
    end
  end
end
