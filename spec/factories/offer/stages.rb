FactoryGirl.define do
  factory :offer_stage, :class => 'Offer::Stage' do
     enrollment { FactoryGirl.create(:enrollment_basis_with_applicant) }
  end
end