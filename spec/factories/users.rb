# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default("")
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string
#  last_sign_in_ip        :string
#  created_at             :datetime
#  updated_at             :datetime
#  invitation_token       :string
#  invitation_created_at  :datetime
#  invitation_sent_at     :datetime
#  invitation_accepted_at :datetime
#  invitation_limit       :integer
#  invited_by_id          :integer
#  invited_by_type        :string
#  invitations_count      :integer          default(0)
#

FactoryGirl.define do
  factory :user do
    email { FFaker::Internet.email }
    password "09090909"
    password_confirmation "09090909"
    personal_information_attributes { FactoryGirl.attributes_for(:people_personal_information) }
    is_active true

    factory :user_no_avatar do
      personal_information_attributes { FactoryGirl.attributes_for(:people_personal_information_no_avatar) }
    end

    factory :user_invitable do
      password nil
      password_confirmation nil
    end

    factory :user_invalid do
      email nil
    end
  end

  factory :user_without_personal_information, class: User do
    email { FFaker::Internet.email }
    password "09090909"
    password_confirmation "09090909"
  end

end
