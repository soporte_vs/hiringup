FactoryGirl.define do
  factory :tag_base_tag, :class => 'Tag::BaseTag' do
    name { FFaker::Lorem.sentence }
  end

  factory :tag_course_tag, :class => 'Tag::CourseTag' do
    name { FFaker::Lorem.sentence }
  end

  factory :tag_company_position_tag, :class => 'Tag::CompanyPositionTag' do
    name { FFaker::Lorem.sentence }
  end
end
