FactoryGirl.define do
  factory :tag_used_course_tag, :class => 'Tag::UsedTag' do
    tag { FactoryGirl.create(:tag_course_tag) }
    taggable { Course.try(:last) || FactoryGirl.create(:course) }
  end

  factory :tag_used_company_position_tag, :class => 'Tag::UsedTag' do
    tag { FactoryGirl.create(:tag_company_position_tag) }
    taggable { Company::Position.try(:last) || FactoryGirl.create(:company_position) }
  end
end
