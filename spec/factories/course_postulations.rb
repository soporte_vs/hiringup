FactoryGirl.define do
  factory :course_postulation do
    postulationable {
      FactoryGirl.create(:minisite_postulation)
    }
    course { postulationable.course }
  end

  factory :course_postulation_invalid, class: 'CoursePostulation' do
    postulationable {
      FactoryGirl.create(:minisite_postulation)
    }
    course { FactoryGirl.create(:course) }
  end

end
