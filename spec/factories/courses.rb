# == Schema Information
#
# Table name: courses
#
#  id                  :integer          not null, primary key
#  type_enrollment     :string
#  title               :string
#  description         :text
#  created_at          :datetime
#  updated_at          :datetime
#  work_flow           :string
#  territory_city_id   :integer
#  company_position_id :integer
#  document_group_id   :integer
#  aasm_state          :string
#  user_id             :integer
#

FactoryGirl.define do
  factory :course do
    type_enrollment "Enrollment::Base"
    title { FFaker::Lorem.sentence }
    description { FFaker::Lorem.paragraph }
    user { User.try(:last) || FactoryGirl.create(:user) }
    territory_city { Territory::City.last || FactoryGirl.create(:territory_city) }
    timetable_id { Company::Timetable.last.try(:id) || FactoryGirl.create(:company_timetable).id }
    contract_type { Company::ContractType.last || FactoryGirl.create(:company_contract_type) }
    request_reason { Company::VacancyRequestReason.last || FactoryGirl.create(:company_vacancy_request_reason) }
    engagement_origin { Company::EngagementOrigin.last || FactoryGirl.create(:company_engagement_origin) }
    start { DateTime.now }
    experience_years { Random.rand(50) }
    is_supervisor { [true, false].sample }
    study_level_id { Education::StudyLevel.last.try(:id) || FactoryGirl.create(:education_study_level).id }
    tags []
    document_group_id { Document::Group.try(:last).try(:id) || FactoryGirl.create(:document_group).id }
    company_position_id { Company::Position.try(:last).try(:id) || FactoryGirl.create(:company_position).id }
    applicant_software_level_id { Applicant::SoftwareLevel.last.try(:id) || FactoryGirl.create(:applicant_software_level).id }
    applicant_profile_type_id { Applicant::ProfileType.last.try(:id) || FactoryGirl.create(:applicant_profile_type).id }
    study_type { People::Degree::TYPES.sample[0] }
    area_id { Company::Area.last.try(:id) || FactoryGirl.create(:company_area).id }
    minimun_requirements { FFaker::Lorem.paragraph }

    factory :course_type_enrollment_invalid, class: Course  do
      type_enrollment "Enrollment::JJJJJJ"
    end

    factory :course_with_questions, class: Course do
      video_interview_questions { VideoInterview::Question.last.present? ? [VideoInterview::Question.last] : [FactoryGirl.create(:video_interview_question)] }
    end

    factory :course_with_tags, class: Course do
      tags { Tag::CourseTag.try(:last, 2).any? ? Tag::CourseTag.try(:last, 2) : FactoryGirl.create_list(:tag_course_tag, 2)}
    end

    factory :course_with_new_company_position do
      company_position_id { FactoryGirl.create(:company_position).id }
    end

    factory :course_with_invalid_params, class: Course do
      type_enrollment ""
      title ""
      description ""
      work_flow []
      user nil
      company_position_id ''
    end

    trait :with_vacancies do
      after(:create) do |instance|
        cenco = create(:company_positions_cenco, company_position: instance.company_position)
        create_list(:vacancy, 2, course: instance, company_positions_cenco: cenco)
      end
    end
  end
end
