FactoryGirl.define do
  factory :company_area, :class => 'Company::Area' do
    name { FFaker::Lorem.word }
  end

  factory :company_area_invalid, :class => 'Company::Area' do
    name ''
  end

end
