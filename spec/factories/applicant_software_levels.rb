FactoryGirl.define do
  factory :applicant_software_level, :class => 'Applicant::SoftwareLevel' do
    name { FFaker::Lorem.sentence }
  end

  factory :applicant_software_level_invalid, :class => 'Applicant::SoftwareLevel' do
    name ''
  end
end
