# == Schema Information
#
# Table name: groups
#
#  id          :integer          not null, primary key
#  name        :string
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

FactoryGirl.define do
  factory :group do
    name { FFaker::HipsterIpsum.sentence }
    description { FFaker::HipsterIpsum.paragraph }
    users { User.try(:last, 2).any? ? User.try(:last, 2) : FactoryGirl.create_list(:user, 2) }

    factory :group_without_users do
      users []
    end

    factory :group_invalid do
      name ''
      description  ''
      users []
    end
  end
end
