FactoryGirl.define do
  factory :document_record, :class => 'Document::Record' do
    applicant { Applicant::Base.try(:last) || FactoryGirl.create(:applicant_bases) }
    document_descriptor { Document::Descriptor.try(:last) || FactoryGirl.create(:document_descriptor) }
    document ActionDispatch::Http::UploadedFile.new(:tempfile => File.new("#{Rails.root}/spec/tmp/avatar.jpg"), filename: "avatar.jpg")
  end
end
