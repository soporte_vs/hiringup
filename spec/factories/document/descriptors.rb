FactoryGirl.define do
  factory :document_descriptor, :class => 'Document::Descriptor' do
    name { FFaker::Lorem.word }
    details { FFaker::Lorem.word }
    document_groups { Document::Group.try(:last, 1).any? ? Document::Group.try(:last, 1) : FactoryGirl::create_list(:document_group, 1) }
    allowed_extensions "docx doc jpg jpeg pdf"
    internal true

    factory :document_descriptor_only_onboarding, :class => "Document::Descriptor" do
      internal false
      only_onboarding true
    end
  end

  factory :document_descriptor_invalid, class: "Document::Descriptor" do
    name nil
    details nil
    document_groups nil
    allowed_extensions nil
    internal nil
  end
end
