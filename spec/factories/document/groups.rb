FactoryGirl.define do
  factory :document_group, :class => 'Document::Group' do
    name { FFaker::Lorem.word }
  end
end
