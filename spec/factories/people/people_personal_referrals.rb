FactoryGirl.define do
  factory :people_personal_referral, :class => 'People::PersonalReferral' do
    personal_information {  People::PersonalInformation.last || FactoryGirl.create(:user).personal_information }
    full_name { FFaker::Lorem.sentence }
    email { FFaker::Internet.email }
    phone { FFaker::Lorem.sentence }
  end

end
