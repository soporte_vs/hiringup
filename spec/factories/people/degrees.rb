FactoryGirl.define do
  factory :people_degree, :class => 'People::Degree' do
    career_id { Education::Career.try(:last).try(:id) || FactoryGirl.create(:education_career).id }
    professional_information_id { People::ProfessionalInformation.try(:last).try(:id) || FactoryGirl.create(:people_professional_information).id }
    condition People::Degree::CONDITIONS.first
    institution_id { Education::Institution.try(:last).try(:id) || FactoryGirl.create(:education_institution).id }
    name { FFaker::HipsterIpsum.sentence }
    culmination_date { Date.today }
  end
end
