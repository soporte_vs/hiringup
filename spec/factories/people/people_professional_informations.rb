FactoryGirl.define do
  factory :people_professional_information, :class => 'People::ProfessionalInformation' do
    user { User.try(:last) || FactoryGirl.create(:user) }
  end
end
