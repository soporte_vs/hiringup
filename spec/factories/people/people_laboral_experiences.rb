# == Schema Information
#
# Table name: people_laboral_experiences
#
#  id                          :integer          not null, primary key
#  position                    :string
#  company                     :string
#  since_date                  :date
#  until_date                  :date
#  professional_information_id :integer
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#  description                 :text
#

FactoryGirl.define do
  factory :people_laboral_experience, :class => 'People::LaboralExperience' do
    position { FFaker::Company.position }
    company { FFaker::Company.name }
    since_date { FFaker::Time.date }
    until_date { FFaker::Time.date }
    description { FFaker::HipsterIpsum.paragraph }
    professional_information_id { People::ProfessionalInformation.try(:last).try(:id) || FactoryGirl.create(:people_professional_information).id }
  end

  factory :people_laboral_experience_invalid, :class => 'People::LaboralExperience' do
    position ''
    company ''
    since_date ''
    until_date ''
    professional_information_id nil
  end

end
