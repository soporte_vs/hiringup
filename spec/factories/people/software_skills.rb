FactoryGirl.define do
  factory :people_software_skill, :class => 'People::SoftwareSkill' do
    education_software_id { Education::Software.try(:last).try(:id) || FactoryGirl.create(:education_software).id }
    professional_information_id { People::ProfessionalInformation.try(:last).try(:id) || FactoryGirl.create(:people_professional_information).id }
    level People::SoftwareSkill::LEVELS.first
  end
end
