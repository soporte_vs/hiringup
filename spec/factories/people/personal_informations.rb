# coding: utf-8
FactoryGirl.define do
  factory :people_personal_information, :class => 'People::PersonalInformation' do
    first_name { FFaker::Name.name[0..20] } # no más de 25 caracteres
    last_name { FFaker::Name.last_name[0..20] } # no más de 25 caracteres
    birth_date { (Date.today - 20.year).to_s }
    landline { FFaker::PhoneNumberAU.international_home_work_phone_number }
    cellphone { FFaker::PhoneNumberAU.international_phone_number }
    territory_city_id { Territory::City.try(:last).try(:id) || FactoryGirl.create(:territory_city).id }
    avatar ActionDispatch::Http::UploadedFile.new(:tempfile => File.new("#{Rails.root}/spec/tmp/avatar.jpg"), filename: "avatar.jpg")
    sex { ['Female','Male'].sample }
    address { FFaker::Address.street_address }
    nationality_id { Territory::Country.try(:last).try(:id) || FactoryGirl.create(:territory_country).id }
    company_marital_status_id { Company::MaritalStatus.try(:last).try(:id) || FactoryGirl.create(:company_marital_status).id }
    study_type { People::Degree::TYPES.sample[0] }
    recruitment_source_id { FactoryGirl.create(:company_recruitment_source).id }
    areas_of_interest { FFaker::HipsterIpsum.paragraph }
    nationality { Territory::Country.last || FactoryGirl.create(:territory_country) }
    identification_document_number { FFaker::IdentificationESCL.rut }
    identification_document_type_id {
      chile = Territory::Country.where(name: "Chile").first_or_create
      chile_rut = People::IdentificationDocumentType.where(
        country: chile,
        name: "RUT",
        validation_regex: "^([0-9]+-[0-9Kk])$"
      ).first_or_create
      chile_rut.id
    }
    availability_replacements { true }
    availability_work_other_residence { true }

    factory :people_personal_information_no_avatar do
      avatar nil
    end
  end
end
