FactoryGirl.define do
  factory :people_language_skill, :class => 'People::LanguageSkill' do
    education_language_id { Education::Language.try(:last).try(:id) || FactoryGirl.create(:education_language).id }
    professional_information_id { People::ProfessionalInformation.try(:last).try(:id) || FactoryGirl.create(:people_professional_information).id }
    level People::LanguageSkill::LEVELS.first
  end
end
