FactoryGirl.define do
  factory :generic_attachment do
    attachment { ActionDispatch::Http::UploadedFile.new(:tempfile => File.new("#{Rails.root}/spec/tmp/document.doc"), filename: "document.doc") }
    resourceable nil
  end
end
