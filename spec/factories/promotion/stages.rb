FactoryGirl.define do
  factory :promotion_stage, :class => 'Promotion::Stage' do
     enrollment { FactoryGirl.create(:enrollment_basis_with_applicant) }
  end
end