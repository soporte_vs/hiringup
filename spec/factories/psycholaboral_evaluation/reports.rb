FactoryGirl.define do
  factory :psycholaboral_evaluation_report, :class => 'PsycholaboralEvaluation::Report' do
    observations { FFaker::Lorem.paragraph }
    psycholaboral_evaluation_stage_id { FactoryGirl.create(:psycholaboral_evaluation_stage).id }
    notified_to { FFaker::Internet.email }
  end

  factory :psycholaboral_evaluation_report_invalid, :class => 'PsycholaboralEvaluation::Report' do
    observations ''
    psycholaboral_evaluation_stage_id { FactoryGirl.create(:psycholaboral_evaluation_stage).id }
    notified_to ''
  end
end