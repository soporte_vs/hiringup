FactoryGirl.define do
  factory :psycholaboral_evaluation_stage, :class => 'PsycholaboralEvaluation::Stage' do
     enrollment { FactoryGirl.create(:enrollment_basis_with_applicant) }
  end
end