# == Schema Information
#
# Table name: stage_bases
#
#  id            :integer          not null, primary key
#  enrollment_id :integer
#  type          :string
#  created_at    :datetime
#  updated_at    :datetime
#

FactoryGirl.define do
  factory :stage_document, :class => 'Stage::Document' do
    resource { FactoryGirl.create(:manager_interview_report) }
    document ActionDispatch::Http::UploadedFile.new(:tempfile => File.new("#{Rails.root}/spec/tmp/document.doc"), filename: "document.doc")
  end

end
