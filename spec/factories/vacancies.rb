# == Schema Information
#
# Table name: vacancies
#
#  id                                 :integer          not null, primary key
#  course_id                          :integer
#  closed_at                          :datetime
#  observations                       :text
#  created_at                         :datetime         not null
#  updated_at                         :datetime         not null
#  company_positions_cenco_id :integer
#

FactoryGirl.define do
  factory :vacancy, class: Vacancy do
    observations { FFaker::HipsterIpsum.paragraph }
    company_positions_cenco_id { Company::PositionsCenco.try(:last).try(:id) || FactoryGirl.create(:company_positions_cenco).id }
    course { Course.where(company_position: company_positions_cenco.company_position).try(:last) || FactoryGirl.create(:course, company_position: company_positions_cenco.company_position) }
    company_vacancy_request_reason_id { Company::VacancyRequestReason.try(:last).try(:id) || FactoryGirl.create(:company_vacancy_request_reason).id }

    factory :vacancy_closed, class: Vacancy do
      closed_at { Date.today }
    end

    factory :vacancy_without_course, class: Vacancy do
      course nil
    end

    factory :vacancy_invalid, class: Vacancy do
      observations ''
      course nil
      company_positions_cenco_id nil
      company_vacancy_request_reason_id nil
    end
  end
end
