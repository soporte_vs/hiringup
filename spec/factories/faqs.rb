FactoryGirl.define do
  factory :faq do
    question FFaker::Lorem.sentence
    answer FFaker::Lorem.paragraph

    factory :faq_invalid do
      question ''
      answer ''
    end
  end
end
