FactoryGirl.define do
  factory :group_interview_report, :class => 'GroupInterview::Report' do
    observations { FFaker::Lorem.paragraph }
    group_interview_stage_id { FactoryGirl.create(:group_interview_stage).id }
    notified_to { FFaker::Internet.email }
  end

  factory :group_interview_report_invalid, :class => 'GroupInterview::Report' do
    observations ''
    group_interview_stage_id { FactoryGirl.create(:group_interview_stage).id }
    notified_to ''
  end
end
