FactoryGirl.define do
  factory :group_interview_stage, :class => 'GroupInterview::Stage' do
     enrollment { FactoryGirl.create(:enrollment_basis_with_applicant) }
  end
end