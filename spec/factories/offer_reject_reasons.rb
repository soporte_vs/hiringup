FactoryGirl.define do
  factory :offer_reject_reason, :class => 'Offer::RejectReason' do
    sequence(:name) { |n| "Reject reason #{n}" }
    description { FFaker::Lorem.paragraph }
  end

  factory :offer_reject_reason_invalid, :class => 'Offer::RejectReason' do
    name nil
  end

end
