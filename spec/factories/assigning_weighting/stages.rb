FactoryGirl.define do
  factory :assigning_weighting_stage, :class => 'AssigningWeighting::Stage' do
     enrollment { FactoryGirl.create(:enrollment_basis_with_applicant) }
  end
end