FactoryGirl.define do
  factory :assigning_weighting_report, :class => 'AssigningWeighting::Report' do
    assigning_weighting_stage_id { FactoryGirl.create(:assigning_weighting_stage).id }
  end

  factory :assigning_weighting_report_invalid, :class => 'AssigningWeighting::Report' do
    # assigning_weighting_stage_id { FactoryGirl.create(:assigning_weighting_stage).id }
    assigning_weighting_stage_id ''
  end
end
