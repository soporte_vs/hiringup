FactoryGirl.define do
  factory :minisite_publication, :class => 'Minisite::Publication' do
    course_id { Course.try(:last).try(:id) || FactoryGirl.create(:course).id }
    description { FFaker::HipsterIpsum.paragraph }
    title { FFaker::HipsterIpsum.sentence[0...80] }
    desactivate_at { Date.today }
    company_contract_type_id { Company::ContractType.try(:last).try(:id) || FactoryGirl.create(:company_contract_type).id }

    trait :with_questions do
      after(:create) do |publication|
        create_list(:minisite_question, 5, minisite_publication_id: publication.id)
      end
    end
  end

  factory :minisite_publication_invalid, :class => 'Minisite::Publication' do
    course_id ''
    description ''
    title ''
    desactivate_at ''
    company_contract_type_id nil
  end
end
