FactoryGirl.define do
  factory :minisite_postulation, :class => 'Minisite::Postulation' do
    minisite_publication_id { Minisite::Publication.try(:last).try(:id) || FactoryGirl.create(:minisite_publication).id }
    applicant_id { FactoryGirl.create(:minisite_applicant).id }
    is_accepted true
    accepted_at Date.today
  end

  factory :minisite_postulation_invalid, :class => 'Minisite::Postulation' do
    minisite_publication_id ''
    applicant_id  ''
  end
end
