FactoryGirl.define do
  factory :minisite_category, :class => 'Minisite::Category' do
    name { FFaker::Lorem.sentence }
    description { FFaker::Lorem.sentence }
  end
  factory :minisite_category_invalid, :class => "Minisite::Category" do
    name ''
    description ''
  end
end
