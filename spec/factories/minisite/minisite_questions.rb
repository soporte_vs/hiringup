# == Schema Information
#
# Table name: minisite_questions
#
#  id                      :integer          not null, primary key
#  minisite_publication_id :integer
#  description             :text
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#

FactoryGirl.define do
  factory :minisite_question, class: 'Minisite::Question' do
    minisite_publication_id { Minisite::Publication.try(:last).try(:id) || FactoryGirl.create(:minisite_publication).id }
    description { FFaker::HipsterIpsum.sentence }
  end
end
