FactoryGirl.define do
  factory :minisite_category_publication, :class => 'Minisite::CategoryPublication' do
    minisite_category { FactoryGirl.create (:minisite_category) }
    minisite_publication { FactoryGirl.create (:minisite_publication) }
  end
end
