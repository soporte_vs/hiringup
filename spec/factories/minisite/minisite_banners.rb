FactoryGirl.define do
  factory :minisite_banner, :class => 'Minisite::Banner' do
    title { FFaker::Lorem.word }
    image ActionDispatch::Http::UploadedFile.new(:tempfile => File.new("#{Rails.root}/spec/tmp/banner.png"), filename: "banner.png")
    active_image [true, false].sample
  end

  factory :minisite_banner_invalid, :class => 'Minisite::Banner' do
    title ''
    image ''
    active_image ''
  end
end
