FactoryGirl.define do
  factory :minisite_applicant, class: 'Minisite::Applicant' do
    user_attributes { FactoryGirl.attributes_for(:user) }
  end
end
