# == Schema Information
#
# Table name: minisite_answers
#
#  id                      :integer          not null, primary key
#  description             :string
#  minisite_question_id    :integer
#  minisite_postulation_id :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#

FactoryGirl.define do
  factory :minisite_answer, :class => 'Minisite::Answer' do
    description { FFaker::HipsterIpsum.sentence }
  end

end
