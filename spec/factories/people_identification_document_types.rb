FactoryGirl.define do
  factory :people_identification_document_type, :class => 'People::IdentificationDocumentType' do
    name { FFaker::Lorem.word }
    country_id { Territory::Country.try(:last).try(:id) || FactoryGirl.create(:territory_country).id }
    validation_regex ""
    validate_uniqueness true

    factory :people_identification_document_type_invalid do
      name ""
      country nil
    end
  end

end
