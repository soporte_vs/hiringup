FactoryGirl.define do
  factory :test_report, :class => 'Test::Report' do
    observations { FFaker::Lorem.paragraph }
    test_stage_id { FactoryGirl.create(:test_stage).id }
  end

  factory :test_report_invalid, :class => 'Test::Report' do
    observations ''
    test_stage_id { FactoryGirl.create(:test_stage).id }
  end
end
