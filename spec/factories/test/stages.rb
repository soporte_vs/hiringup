FactoryGirl.define do
  factory :test_stage, :class => 'Test::Stage' do
     enrollment { FactoryGirl.create(:enrollment_basis_with_applicant) }
  end
end
