FactoryGirl.define do
  factory :skill_interview_report, :class => 'SkillInterview::Report' do
    observations { FFaker::Lorem.paragraph }
    skill_interview_stage_id { FactoryGirl.create(:skill_interview_stage).id }
  end

  factory :skill_interview_report_invalid, :class => 'SkillInterview::Report' do
    observations ''
    skill_interview_stage_id { FactoryGirl.create(:skill_interview_stage).id }
  end
end
