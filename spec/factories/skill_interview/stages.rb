FactoryGirl.define do
  factory :skill_interview_stage, :class => 'SkillInterview::Stage' do
     enrollment { FactoryGirl.create(:enrollment_basis_with_applicant) }
  end
end