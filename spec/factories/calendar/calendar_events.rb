FactoryGirl.define do
  factory :calendar_event, :class => 'Calendar::Event' do
    manager_attributes { FactoryGirl.attributes_for(:calendar_event_manager_without_event) }
    starts_at { Time.current + 1.week } # Time.current es equivalente a Time.zone.now
    ends_at nil
    address { FFaker::Address.street_address }
    lat -33.425412
    lng -70.566567
    observations { FFaker::HipsterIpsum.paragraph }
    created_by { User.try(:last) || FactoryGirl.create(:user) }

    factory :event_invalid do
      starts_at nil
    end
  end
end
