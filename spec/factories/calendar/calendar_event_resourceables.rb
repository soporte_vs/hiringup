FactoryGirl.define do
  factory :calendar_event_resourceable, :class => 'Calendar::EventResourceable' do
    resourceable { FactoryGirl.create(:stage_basis) }
    event { Calendar::Event.try(:last) || FactoryGirl.create(:calendar_event) }
    status { FFaker::Lorem.word }

    factory :calendar_event_resourceable_invalid, :class => 'Calendar::EventResourceable' do
      resourceable nil
      event nil
    end
  end

end
