FactoryGirl.define do
  factory :calendar_event_guest, :class => 'Calendar::EventGuest' do
    event { Calendar::Event.try(:last) || FactoryGirl.create(:calendar_event) }
    name { FFaker::Name.name }
    email { FFaker::Internet.email }
  end
end
