FactoryGirl.define do
  factory :calendar_event_manager, :class => 'Calendar::EventManager' do
    event { Calendar::Event.try(:last) || FactoryGirl.create(:calendar_event) }
    name { FFaker::Name.name }
    email { FFaker::Internet.email }

    factory :calendar_event_manager_without_event do
      event nil
    end
  end

end
