# == Schema Information
#
# Table name: worker_bases
#
#  id         :integer          not null, primary key
#  type       :string(255)
#  user_id    :integer
#  created_at :datetime
#  updated_at :datetime
#

FactoryGirl.define do
  factory :worker_basis, :class => 'Worker::Base' do
    user_attributes { FactoryGirl.attributes_for(:user) }
  end
  factory :worker_basis_invitable, :class => 'Worker::Base' do
    user_attributes { FactoryGirl.attributes_for(:user_invitable) }
  end
  factory :worker_invalid, :class => 'Worker::Base' do
    user_attributes { FactoryGirl.attributes_for(:user_invalid) }
  end

end
