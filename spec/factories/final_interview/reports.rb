FactoryGirl.define do
  factory :final_interview_report, :class => 'FinalInterview::Report' do
    observations { FFaker::Lorem.paragraph }
    final_interview_stage_id { FactoryGirl.create(:final_interview_stage).id }
    notified_to { FFaker::Internet.email }
  end

  factory :final_interview_report_invalid, :class => 'FinalInterview::Report' do
    observations ''
    final_interview_stage_id { FactoryGirl.create(:final_interview_stage).id }
    notified_to ''
  end
end
