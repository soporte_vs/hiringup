FactoryGirl.define do
  factory :trabajando_career, :class => 'Trabajando::Career' do
    trabajando_id 1
    name { FFaker::Lorem.sentence }
  end

  factory :trabajando_career_invalid, :class => 'Trabajando::Career' do
    trabajando_id nil
    name ''
  end
end
