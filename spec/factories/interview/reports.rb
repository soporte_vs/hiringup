FactoryGirl.define do
  factory :interview_report, :class => 'Interview::Report' do
    observations { FFaker::Lorem.paragraph }
    interview_stage_id { FactoryGirl.create(:interview_stage).id }
    document ActionDispatch::Http::UploadedFile.new(:tempfile => File.new("#{Rails.root}/spec/tmp/document.doc"), filename: "document.doc")
  end

  factory :interview_report_invalid, :class => 'Interview::Report' do
    observations ''
    document ''
    interview_stage_id { FactoryGirl.create(:interview_stage).id }
  end
end
