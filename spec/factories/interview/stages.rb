FactoryGirl.define do
  factory :interview_stage, :class => 'Interview::Stage' do
     enrollment { FactoryGirl.create(:enrollment_dynamic) }
  end
end
