FactoryGirl.define do
  factory :stage_not_applying, :class => 'Stage::NotApplying' do
    stage { FactoryGirl.create(:stage_basis) }
    not_apply_reason { FactoryGirl.create(:stage_not_apply_reason) }
    observations { FFaker::Lorem.paragraph }
  end

end
