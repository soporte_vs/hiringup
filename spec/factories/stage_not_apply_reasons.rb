FactoryGirl.define do
  factory :stage_not_apply_reason, :class => 'Stage::NotApplyReason' do
    name { FFaker::Lorem.sentence }
    description { FFaker::Lorem.paragraph }
  end
  factory :stage_not_apply_reason_invalid, :class => 'Stage::NotApplyReason' do
    name nil
    description nil
  end
end
