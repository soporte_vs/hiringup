FactoryGirl.define do
  factory :company_white_rut, :class => 'Company::WhiteRut' do
    rut {FFaker::IdentificationESCL.rut}
  end

  factory :company_white_rut_invalid, :class => 'Company::WhiteRut' do
    rut ''
  end
end
