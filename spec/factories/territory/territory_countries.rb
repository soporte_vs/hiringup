FactoryGirl.define do
  factory :territory_country, :class => 'Territory::Country' do
    name { FFaker::Address.city }
  end

end
