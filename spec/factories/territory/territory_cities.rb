FactoryGirl.define do
  factory :territory_city, :class => 'Territory::City' do
    name { FFaker::Address.city }
    territory_state { Territory::State.try(:last) || FactoryGirl.create(:territory_state) }
  end
end
