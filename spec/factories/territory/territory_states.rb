FactoryGirl.define do
  factory :territory_state, :class => 'Territory::State' do
    name { FFaker::Address.city }
    territory_country { Territory::Country.try(:last) || FactoryGirl.create(:territory_country) }
  end
end
