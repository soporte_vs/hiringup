FactoryGirl.define do
  factory :trabajando_country, :class => 'Trabajando::Country' do
    name { FFaker::Lorem.word }
    trabajando_id { Trabajando::Country.last.try(:trabajando_id).to_i + 1 }
    country { Territory::Country.last || FactoryGirl.create(:territory_country) }

    factory :trabajando_country_invalid, :class => 'Trabajando::Country' do
      name ''
      trabajando_id ''
      country nil
    end
  end

end
