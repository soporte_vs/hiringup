FactoryGirl.define do
  factory :trabajando_company_activity, :class => 'Trabajando::CompanyActivity' do
    name { FFaker::Lorem.sentence }
    trabajando_id { Trabajando::CompanyActivity.last ? Trabajando::CompanyActivity.last.trabajando_id + 1 : 1 }
  end

end
