FactoryGirl.define do
  factory :trabajando_city, :class => 'Trabajando::City' do
    name { FFaker::Lorem.word }
    trabajando_id { Trabajando::City.last.try(:trabajando_id).to_i + 1 }
    hupe_city_id { Territory::City.last.try(:id) || FactoryGirl.create(:territory_city).id }

    factory :trabajando_city_invalid, :class => 'Trabajando::City' do
      name ''
      trabajando_id ''
      hupe_city_id nil
    end
  end

end
