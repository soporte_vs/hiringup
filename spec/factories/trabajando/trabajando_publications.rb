FactoryGirl.define do
  factory :trabajando_publication, :class => 'Trabajando::Publication' do
    course_id { Course.last.try(:id) || FactoryGirl.create(:course).id }
    area_id { Trabajando::Area.first.try(:id) || FactoryGirl.create(:trabajando_area).id }
    company_activity_id { Trabajando::CompanyActivity.first.try(:id) || FactoryGirl.create(:trabajando_company_activity).id }
    work_day_id { Trabajando::WorkDay.first.try(:id) || FactoryGirl.create(:trabajando_work_day).id }
    job_type_id { Trabajando::JobType.first.try(:id) || FactoryGirl.create(:trabajando_job_type).id }
    applicant_profile_id { Trabajando::ApplicantProfile.first.try(:id) || FactoryGirl.create(:trabajando_applicant_profile).id }
    education_level_id { Trabajando::EducationLevel.first.try(:id) || FactoryGirl.create(:trabajando_education_level).id }
    education_state_id { Trabajando::EducationState.first.try(:id) || FactoryGirl.create(:trabajando_education_state).id }
    software_level_id { Trabajando::SoftwareLevel.first.try(:id) || FactoryGirl.create(:trabajando_software_level).id }
    region_id { Trabajando::State.first.try(:id) || FactoryGirl.create(:trabajando_state).id }
    city_id { Trabajando::City.first.try(:id) || FactoryGirl.create(:trabajando_city).id }
    title { FFaker::Lorem.sentence }
    description { FFaker::Lorem.paragraph }
    q1 { FFaker::Lorem.sentence }
    q2 { FFaker::Lorem.sentence }
    q3 { FFaker::Lorem.sentence }
    q4 { FFaker::Lorem.sentence }
    q5 { FFaker::Lorem.sentence }
    number_vacancies 2
    salary 700000
    contract_time { FFaker::Lorem.sentence }
    show_salary true
    salary_comment ''
    work_time { FFaker::Lorem.sentence(1) }
    workplace { FFaker::Venue.name }
    minimum_requirements { FFaker::Lorem.paragraph }
    work_experience { Random.rand(1..10)  }
    own_transport false
    is_confidential false
    desactivate_at { Date.today }

    factory :trabajando_publication_invalid do
      course_id nil
      area_id nil
      title ''
      description ''
      q1 ''
      q2 ''
      q3 ''
      q4 ''
      q5 ''
      number_vacancies ''
      salary ''
      contract_time ''
      show_salary false
      salary_comment ''
      work_time ''
      workplace ''
      minimum_requirements ''
      work_experience 0
      own_transport false
    end
  end
end
