FactoryGirl.define do
  factory :trabajando_software_level, :class => 'Trabajando::SoftwareLevel' do
    name { FFaker::Lorem.word }
    trabajando_id {
      Trabajando::SoftwareLevel.last.try(:trabajando_id).to_i + 1
    }
    hupe_software_level_id {
      Applicant::SoftwareLevel.last.try(:id) ||
      FactoryGirl.create(:applicant_software_level).id
    }
  end

end
