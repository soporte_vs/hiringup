FactoryGirl.define do
  factory :trabajando_job_type, :class => 'Trabajando::JobType' do
    name { FFaker::Lorem.word }
    trabajando_id {
      Trabajando::JobType.last.try(:trabajando_id).to_i + 1
    }
    hupe_position_id {
      Company::Position.last.try(:id) || FactoryGirl.create(:company_position).id
    }
  end

  factory :trabajando_job_type_invalid, :class => 'Trabajando::JobType' do
    name nil
    trabajando_id nil
    hupe_position_id nil
  end

end
