FactoryGirl.define do
  factory :trabajando_work_day, :class => 'Trabajando::WorkDay' do
    name { FFaker::Lorem.word }
    trabajando_id { Trabajando::WorkDay.last ? Trabajando::WorkDay.last.trabajando_id + 1 : 1 }

    factory :trabajando_work_day_with_company_timetables do
      company_timetable_ids { FactoryGirl.create_list(:company_timetable, 3).map(&:id) }
    end
  end

end
