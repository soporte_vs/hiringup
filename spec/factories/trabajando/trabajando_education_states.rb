FactoryGirl.define do
  factory :trabajando_education_state, :class => 'Trabajando::EducationState' do
    name { FFaker::Lorem.word }
    trabajando_id {
      Trabajando::EducationState.last.try(:trabajando_id).to_i + 1
    }
    hupe_study_level_id {
      Education::StudyLevel.last.try(:id) ||
      FactoryGirl.create(:education_study_level).id
    }

    factory :trabajando_education_state_invalid, :class => 'Trabajando::EducationState' do
      name ''
      trabajando_id ''
    end
  end

end
