FactoryGirl.define do
  factory :trabajando_marital_status, :class => 'Trabajando::MaritalStatus' do
    name { FFaker::Lorem.sentence }
    trabajando_id {
      Trabajando::MaritalStatus.last ? Trabajando::MaritalStatus.last.trabajando_id + 1 : 0
    }
    company_marital_status_id {
      Company::MaritalStatus.last.try(:id) || FactoryGirl.create(:company_marital_status).id
    }

    factory :trabajando_marital_status_invalid, :class => 'Trabajando::MaritalStatus' do
      name ''
      trabajando_id nil
      company_marital_status_id nil
    end
  end
end
