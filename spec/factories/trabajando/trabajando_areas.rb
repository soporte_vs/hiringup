FactoryGirl.define do
  factory :trabajando_area, :class => 'Trabajando::Area' do
    trabajando_id {
      Trabajando::Area.last.try(:trabajando_id).to_i + 1
    }
    name { FFaker::Lorem.sentence }
    hupe_area_id {
      Company::Area.last.try(:id) ||
      FactoryGirl.create(:company_area).id
    }
  end

  factory :trabajando_area_invalid, :class => 'Trabajando::Area' do
    trabajando_id nil
    name ''
  end

end
