FactoryGirl.define do
  factory :trabajando_state, :class => 'Trabajando::State' do
    name { FFaker::Lorem.word }
    trabajando_id { Trabajando::State.last.try(:trabajando_id).to_i + 1 }
    hupe_state_id { Territory::State.last.try(:id) || FactoryGirl.create(:territory_state).id }

    factory :trabajando_state_invalid, :class => 'Trabajando::State' do
      name ''
      trabajando_id ''
      hupe_state nil
    end
  end

end
