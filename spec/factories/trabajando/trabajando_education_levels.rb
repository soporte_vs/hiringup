FactoryGirl.define do
  factory :trabajando_education_level, :class => 'Trabajando::EducationLevel' do
    name { FFaker::Lorem.word }
    trabajando_id {
      Trabajando::EducationLevel.last.try(:trabajando_id).to_i + 1
    }
    hupe_study_type {
      People::Degree::TYPES.sample[0]
    }

    factory :trabajando_education_level_invalid, :class => 'Trabajando::EducationLevel' do
      name ''
      trabajando_id ''
    end
  end

end
