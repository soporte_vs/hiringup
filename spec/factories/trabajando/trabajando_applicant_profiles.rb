FactoryGirl.define do
  factory :trabajando_applicant_profile, class: 'Trabajando::ApplicantProfile' do
    name { FFaker::Lorem.word }
    trabajando_id {
      Trabajando::ApplicantProfile.last.try(:trabajando_id).to_i + 1
    }
    hupe_profile_type_id {
      Applicant::ProfileType.last.try(:id) ||
      FactoryGirl.create(:applicant_profile_type).try(:id)
    }
  end

end
