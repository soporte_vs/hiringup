FactoryGirl.define do

  factory :video_interview_stage, :class => "VideoInterview::Stage" do
    enrollment { FactoryGirl.create(:video_interview_answer_with_enrollment).enrollment }
    name { FFaker::Lorem.phrase }
  end


  factory :video_interview_stage_with_enroll, :class => "VideoInterview::Stage" do
    enrollment { FactoryGirl.create(:enrollment_basis_with_applicant) }
    name { FFaker::Lorem.phrase }
  end

  factory :video_interview_stage_without_enroll, :class => "VideoInterview::Stage" do
    name { FFaker::Lorem.phrase }
  end
end
