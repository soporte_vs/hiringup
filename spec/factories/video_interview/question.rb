FactoryGirl.define do
  factory :video_interview_question, :class => "VideoInterview::Question" do
    content { FFaker::Lorem.phrase }
  end
end
