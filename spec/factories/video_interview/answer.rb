FactoryGirl.define do
  factory :video_interview_answer_without_enrollment, :class => "VideoInterview::Answer" do
    question { FFaker::Lorem.phrase }
    time_limit 60
  end

  factory :video_interview_answer_with_enrollment, :class => "VideoInterview::Answer" do
    question { FFaker::Lorem.phrase }
    enrollment { Enrollment::Base.try(:last) || FactoryGirl.create(:enrollment_basis_with_applicant) }
    user { enrollment.applicant.user }
    time_limit 60
  end
end
