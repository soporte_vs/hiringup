FactoryGirl.define do
  factory :comment_applicant, class: Comment do
    content { FFaker::HipsterIpsum.sentence }
    user { User.try(:last) || FactoryGirl.create(:user) }
    object { Applicant::Base.try(:last) || FactoryGirl.create(:applicant_bases) }
  end
end
