FactoryGirl.define do
  factory :trigger_blacklist_discard_reason, :class => 'Trigger::BlacklistDiscardReason' do
    resourceable { FactoryGirl.create(:enrollment_discard_reason) }
  end

end
