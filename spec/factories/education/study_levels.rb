FactoryGirl.define do
  factory :education_study_level, :class => 'Education::StudyLevel' do
    name {FFaker::Lorem.sentence}
  end

  factory :education_study_level_invalid, :class => 'Education::StudyLevel' do
    name nil
  end
end
