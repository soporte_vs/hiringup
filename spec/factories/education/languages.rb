FactoryGirl.define do
  factory :education_language, :class => 'Education::Language' do
    name { FFaker::Lorem.sentence}
  end

  factory :education_language_invalid, :class => 'Education::Language' do
    name ""
  end
end
