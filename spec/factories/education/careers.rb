FactoryGirl.define do
  factory :education_career, :class => 'Education::Career' do
    name { FFaker::Job.title }
  end

  factory :invalid_education_career, :class => 'Education::Career' do
    name ""
  end
end
