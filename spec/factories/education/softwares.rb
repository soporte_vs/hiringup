FactoryGirl.define do
  factory :education_software, :class => 'Education::Software' do
    name { FFaker::Lorem.sentence}
  end

  factory :education_software_invalid, :class => 'Education::Software' do
    name ""
  end
end
