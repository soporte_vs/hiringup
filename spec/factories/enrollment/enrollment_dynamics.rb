FactoryGirl.define do
  factory :enrollment_dynamic, :class => 'Enrollment::Dynamic' do
    course { Course.where(type_enrollment: 'Enrollment::Dynamic').try(:last) || FactoryGirl.create(:course, type_enrollment: Enrollment::Dynamic) }
    applicant { FactoryGirl.create(:applicant_catched) } # el Applicant no debe estar en uso
  end

end
