FactoryGirl.define do
  factory :enrollment_internal_with_applicant, 
    class: 'Enrollment::Internal', parent: :enrollment_basis_with_applicant do
  end
end