# coding: utf-8
FactoryGirl.define do
  factory :enrollment_discarding, :class => 'Enrollment::Discarding' do
    enrollment { FactoryGirl.create(:enrollment_basis_with_applicant) } # siempre crea uno, o sea, un enrollment descartado, no descarta el último
    discard_reason { Enrollment::DiscardReason.try(:last) || FactoryGirl.create(:enrollment_discard_reason)  }
    observations { FFaker::Lorem.paragraph }
  end
end
