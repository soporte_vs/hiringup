FactoryGirl.define do
  factory :enrollment_discard_reason, :class => 'Enrollment::DiscardReason' do
    name { FFaker::Lorem.sentence }
    description { FFaker::Lorem.paragraph }
  end

  factory :enrollment_discard_reason_custom, :class => "Enrollment::DiscardReason" do
    name { FFaker::Lorem.sentence }
    description { FFaker::Lorem.paragraph }
    mailer_content "<li>Candidato = [[candidato]]</li><li>Cargo = [[cargo]]</li><li>Gerencia [[gerencia]]</li><li>Proceso = [[proceso]]</li>"
  end
end
