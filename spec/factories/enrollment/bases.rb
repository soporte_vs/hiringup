# == Schema Information
#
# Table name: enrollment_bases
#
#  id           :integer          not null, primary key
#  type         :string(255)
#  course_id    :integer
#  created_at   :datetime
#  updated_at   :datetime
#  applicant_id :integer
#  stage_id     :integer
#

FactoryGirl.define do
  factory :enrollment_basis, :class => 'Enrollment::Base' do
    course { Course.try(:last) || FactoryGirl.create(:course) }

    factory :enrollment_basis_with_applicant do
      applicant { FactoryGirl.create(:applicant_catched) }
    end
  end
end
