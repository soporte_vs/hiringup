FactoryGirl.define do
  factory :enrollment_course_invitation, :class => 'Enrollment::CourseInvitation' do
    enrollment { FactoryGirl.create(:enrollment_basis_with_applicant) }
  end

  factory :enrollment_course_invitation_invalid, :class => 'Enrollment::CourseInvitation' do
    enrollment nil
  end
end
