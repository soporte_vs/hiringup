FactoryGirl.define do
  factory :technical_test_stage, :class => 'TechnicalTest::Stage' do
     enrollment { FactoryGirl.create(:enrollment_basis_with_applicant) }
  end
end