FactoryGirl.define do
  factory :technical_test_report, :class => 'TechnicalTest::Report' do
    observations { FFaker::Lorem.paragraph }
    technical_test_stage_id { FactoryGirl.create(:technical_test_stage).id }
    notified_to { FFaker::Internet.email }
  end

  factory :technical_test_report_invalid, :class => 'TechnicalTest::Report' do
    technical_test_stage_id { FactoryGirl.create(:technical_test_stage).id }
    notified_to ''
  end
end
