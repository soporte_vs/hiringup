FactoryGirl.define do
  factory :trabajando_postulation, :class => 'Trabajando::Postulation' do
    trabajando_publication_id { FactoryGirl.create(:trabajando_publication).id }
    applicant_id { FactoryGirl.create(:trabajando_applicant).id }
    postulation_date { Time.now }

    factory :trabajando_postulation_invalid do
      trabajando_publication_id nil
      applicant_id nil
      postulation_date nil
    end

    trait :with_answers do
      answers{
        trabajando_publication.questions.map do |q|
          {question: q, answer: FFaker::HipsterIpsum.sentence}
        end
      }
    end
  end

end
