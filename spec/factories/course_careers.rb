FactoryGirl.define do
  factory :course_career do
    course { FactoryGirl.create(:course)}
    education_career { FactoryGirl.create(:education_career) }
  end
end
