FactoryGirl.define do
  factory :onboarding_stage, :class => 'Onboarding::Stage' do
    enrollment { FactoryGirl.create(:enrollment_basis_with_applicant) }
  end
end
