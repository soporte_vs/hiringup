FactoryGirl.define do
  factory :evaluar_result, :class => 'Evaluar::Result' do
    data {{"data":true}}
    process_id { Random.rand(1000000..9999999).to_s}
    identification { FactoryGirl.create(:user).personal_information.identification_document_number }
    applicant_id { FactoryGirl.create(:applicant_bases).id }
    evaluar_course_id { Evaluar::Course.last.try(:id) || FactoryGirl.create(:evaluar_course).id }
  end

end
