FactoryGirl.define do
  factory :evaluar_course, :class => 'Evaluar::Course' do
    course_id { Course.try(:last).try(:id) || FactoryGirl.create(:course).id }
    #Parametro para asignar una agencia al proceso y poder agruparla.
    #Es la agencia que pertenece al proceso y es usado internamente por el api, cada cliente tiene agencias y departamentos dentro de su organización.
    #Se usa un identificador estándar para evitar conflictos con el api.
    agency_id '00000000-0000-0000-0000-000000000000'
    #Hace el proceso solo visible a la persona que lo envia
    confidential true
    #Emails para ser notificados adicionalmente al que envía el proceso
    cc { FFaker::Internet.safe_email }
    #Recibir alertas cuando termine todo el proceso
    notification_at_the_end true
    #Recibir notificaciones cuando termina el proceso de cada evaluado
    notification_per_evaluated true
    #Define la forma en que los evaluados podran rendir las pruebas (via email o a través de una URL)
    environment_type 'EMAIL'
    #Nombre del proceso
    name { FFaker::Lorem.sentence.gsub(/[^a-záéíóúñA-ZÁÉÍÓÚÑ0-9 ]/, "") }
    #Producto de evaluar asociado al proceso. En este caso es Coeficiente de Adecuación al Puesto
    product_type 'CAP'
    #Fecha a partir de la cual pueden acceder los evaluados
    start_date { (Date.today).to_s }
    #Fecha máxima para rendir las evaluaciones
    end_date { (Date.today + 10.day).to_s }
    #Estado del proceso se requiere DRAFT para crearlo
    status 'DRAFT'
    #Id del perfil asociado a cada proceso
    profile_id '3967'
  end

end
