FactoryGirl.define do
  factory :manager_interview_report, :class => 'ManagerInterview::Report' do
    observations { FFaker::Lorem.paragraph }
    manager_interview_stage_id { FactoryGirl.create(:manager_interview_stage).id }
  end

  factory :manager_interview_report_invalid, :class => 'ManagerInterview::Report' do
    observations ''
    manager_interview_stage_id { FactoryGirl.create(:manager_interview_stage).id }
  end
end
