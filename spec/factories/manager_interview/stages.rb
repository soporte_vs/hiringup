FactoryGirl.define do
  factory :manager_interview_stage, :class => 'ManagerInterview::Stage' do
     enrollment { FactoryGirl.create(:enrollment_basis_with_applicant) }
  end
end