FactoryGirl.define do
  factory :applicant_update_info_request, :class => 'Applicant::UpdateInfoRequest' do
    applicant { Applicant::Base.last || FactoryGirl.create(:applicant_bases) }

    factory :applicant_update_info_request_invalid, :class => 'Applicant::UpdateInfoRequest' do
      applicant nil
    end
  end
end
