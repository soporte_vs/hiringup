FactoryGirl.define do
  factory :applicant_catched, :class => 'Applicant::Catched' do
    user_attributes { FactoryGirl.attributes_for(:user) }
  end

  factory :applicant_catched_invitable, class: 'Applicant::Catched' do
    user_attributes { FactoryGirl.attributes_for(:user_invitable) }
  end
end