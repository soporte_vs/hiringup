FactoryGirl.define do
  factory :applicant_external, :class => 'Applicant::External' do
    user_attributes { FactoryGirl.attributes_for(:user_without_personal_information) }
  end
end
