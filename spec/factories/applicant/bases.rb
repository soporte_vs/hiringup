# == Schema Information
#
# Table name: applicant_bases
#
#  id         :integer          not null, primary key
#  type       :string(255)
#  user_id    :integer
#  created_at :datetime
#  updated_at :datetime
#

FactoryGirl.define do
  factory :applicant_bases, :class => 'Applicant::Base' do
    user_attributes { FactoryGirl.attributes_for(:user) }
  end
end
