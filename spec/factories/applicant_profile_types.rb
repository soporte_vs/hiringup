FactoryGirl.define do
  factory :applicant_profile_type, :class => 'Applicant::ProfileType' do
    name { FFaker::Lorem.sentence }
  end

  factory :applicant_profile_type_invalid, :class => 'Applicant::ProfileType' do
    name ''
  end
end
