FactoryGirl.define do
  factory :engagement_stage, :class => 'Engagement::Stage' do
     enrollment { FactoryGirl.create(:enrollment_basis_with_applicant) }
  end
end
