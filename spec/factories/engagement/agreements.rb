# == Schema Information
#
# Table name: engagement_agreements
#
#  id                  :integer          not null, primary key
#  revenue             :string(255)
#  observations        :text
#  engagement_stage_id :integer
#  created_at          :datetime
#  updated_at          :datetime
#

FactoryGirl.define do
  factory :engagement_agreement, :class => 'Engagement::Agreement' do
    revenue "12345678"
    observations { FFaker::Lorem.paragraph }
    engagement_stage { FactoryGirl.create(:engagement_stage) }
    company_contract_type_id { Company::ContractType.try(:last).try(:id) || FactoryGirl.create(:company_contract_type).id }
    vacancy_id {
      FactoryGirl.create(:vacancy_closed,
        company_positions_cenco: FactoryGirl.create(
          :company_positions_cenco,
          company_position: engagement_stage.enrollment.course.company_position),
        course: engagement_stage.enrollment.course
      ).id
    }
  end
end
