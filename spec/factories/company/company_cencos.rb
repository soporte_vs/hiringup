FactoryGirl.define do
  factory :company_cenco, :class => 'Company::Cenco' do
    company_management_id { Company::Management.try(:last).try(:id) || FactoryGirl.create(:company_management).id }
    name { FFaker::Lorem.sentence }
    address { FFaker::Lorem.sentence }
    cod { rand(100) }

    factory :company_cenco_invalid do
      company_management_id ''
      name ''
      address ''
    end
  end
end
