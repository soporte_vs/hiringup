FactoryGirl.define do
  factory :company_recruitment_source, :class => 'Company::RecruitmentSource' do
    name { FFaker::Lorem.sentence }
    description "MyText"
  end
  factory :company_recruitment_source_invalid, :class => 'Company::RecruitmentSource' do
    name ""
    description ""
  end
end
