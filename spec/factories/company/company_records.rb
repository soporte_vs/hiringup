FactoryGirl.define do
  factory :company_record, :class => 'Company::Record' do
    name { FFaker::Company.name }
    email_domain { FFaker::Internet.domain_name}
  end

  factory :company_record_invalid, :class => 'Company::Record' do
    name ''
    email_domain nil
  end
end
