FactoryGirl.define do
  factory :company_position, class: 'Company::Position' do
    name { FFaker::Lorem.word }
    description { FFaker::HipsterIpsum.paragraph }
    cod { (100 + Random.rand(10000)).to_s }
  end

  factory :company_position_invalid, class: 'Company::Position' do
    name ''
    description ''
    cod ''
  end
end
