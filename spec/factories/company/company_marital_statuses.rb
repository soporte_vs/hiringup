FactoryGirl.define do
  factory :company_marital_status, :class => 'Company::MaritalStatus' do
    name { FFaker::Lorem.sentence }
  end

  factory :company_marital_status_invalid, :class => 'Company::MaritalStatus' do
    name ''
  end
end
