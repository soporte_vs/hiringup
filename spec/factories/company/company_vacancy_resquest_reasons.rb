FactoryGirl.define do
  factory :company_vacancy_request_reason, :class => 'Company::VacancyRequestReason' do
    name { FFaker::Lorem.sentence }
  end

  factory :company_vacancy_request_reason_invalid, :class => 'Company::VacancyRequestReason' do
    name ''
  end
end
