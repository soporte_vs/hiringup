FactoryGirl.define do
  factory :company_timetable_trabajando_workday, class: 'Company::TimetableTrabajandoWorkday' do
    company_timetable { FactoryGirl.create(:company_timetable) }
    trabajando_work_day { FactoryGirl.create(:trabajando_work_day) }
  end

end
