FactoryGirl.define do
  factory :company_reference, :class => 'Company::Reference' do
    email_name { FFaker::Internet.user_name }
    email_domain { FFaker::Internet.domain_name }
    first_name { FFaker::Name.name }
    last_name { FFaker::Name.last_name }
    position { FFaker::Lorem.sentence }
    company_employee { FactoryGirl.create(:company_employee) }
    applicant_attributes { FactoryGirl.attributes_for(:applicant_bases) }
  end

  factory :company_reference_invalid, :class => 'Company::Reference' do
    email_name nil
    email_domain nil
    first_name nil
    last_name nil
    position nil
    company_employee nil
    applicant nil
  end

end
