FactoryGirl.define do
  factory :company_position_trabajando_job_type, class: 'Company::PositionTrabajandoJobType' do
    company_position { FactoryGirl.create(:company_position) }
    trabajando_job_type { FactoryGirl.create(:trabajando_job_type) }
  end

end
