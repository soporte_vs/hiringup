FactoryGirl.define do
  factory :company_engagement_origin, :class => 'Company::EngagementOrigin' do
    name {FFaker::Lorem.sentence}
    description { FFaker::HipsterIpsum.paragraph }
  end

  factory :company_engagement_origin_invalid, :class => 'Company::EngagementOrigin' do
    name nil
    description nil
  end
end
