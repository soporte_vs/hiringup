FactoryGirl.define do
  factory :company_estate, class: Company::Estate do
    name { FFaker::HipsterIpsum.sentence }
  end

  factory :company_estate_invalid, class: Company::Estate do
    name ''
  end

end
