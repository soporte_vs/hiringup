FactoryGirl.define do
  factory :company_timetable, :class => "Company::Timetable" do
    name { FFaker::Lorem.sentence }
    description { FFaker::Lorem.sentence }
  end

  factory :company_timetable_invalid, :class => "Company::Timetable" do
    name ''
    description ''
  end
end
