FactoryGirl.define do
  factory :company_management, :class => 'Company::Management' do
    company_record_id { Company::Record.try(:last).try(:id) || FactoryGirl.create(:company_record).id }
    company_business_unit_id { FactoryGirl.create(:company_business_unit).id }
  end

end
