FactoryGirl.define do
  factory :company_contract_type, :class => 'Company::ContractType' do
    name { FFaker::Lorem.sentence }
    description { FFaker::Lorem.paragraph }
  end

  factory :company_contract_type_invalid, :class => 'Company::ContractType' do
    name ''
    description  ''
  end
end
