FactoryGirl.define do
  factory :company_positions_cenco, class: 'Company::PositionsCenco' do
    company_position { FactoryGirl.create :company_position }
    company_cenco { FactoryGirl.create :company_cenco }
  end

end
