FactoryGirl.define do
  factory :company_business_unit, class: 'Company::BusinessUnit' do
    name { FFaker::Company.name }
    description { FFaker::HipsterIpsum.paragraph }
  end
  
  factory :company_business_unit_invalid, class: 'Company::BusinessUnit' do
    name ''
    description ''
  end
end
