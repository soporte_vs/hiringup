FactoryGirl.define do
  factory :company_employee_positions_cenco, :class => 'Company::EmployeePositionsCenco' do
    company_employee { Company::Employee.try(:last) || FactoryGirl.create(:company_employee) }
    company_positions_cenco { Company::PositionsCenco.try(:last) || FactoryGirl.create(:company_positions_cenco) }
    boss false
  end
end
