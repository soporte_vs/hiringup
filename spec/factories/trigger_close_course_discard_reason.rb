FactoryGirl.define do
  factory :trigger_close_course_discard_reason, :class => 'Trigger::CloseCourseDiscardReason' do
    resourceable {FactoryGirl.create(:enrollment_discard_reason)}
  end

end
