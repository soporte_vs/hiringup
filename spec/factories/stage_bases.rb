# == Schema Information
#
# Table name: stage_bases
#
#  id            :integer          not null, primary key
#  enrollment_id :integer
#  type          :string
#  created_at    :datetime
#  updated_at    :datetime
#

FactoryGirl.define do
  factory :stage_basis, :class => 'Stage::Base' do
    enrollment { FactoryGirl.create(:enrollment_basis_with_applicant) }
    name { FFaker::Lorem.word }
    is_approved nil
  end

end
