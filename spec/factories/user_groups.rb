# == Schema Information
#
# Table name: user_groups
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  group_id   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :user_group do
    user { FactoryGirl.create(:user) }
    group { FactoryGirl.create(:group_without_users) }
  end

end
