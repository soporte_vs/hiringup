FactoryGirl.define do
  factory :hiring_request_record, :class => 'HiringRequest::Record' do
    company_positions_cenco_id { Company::PositionsCenco.try(:last).try(:id) || FactoryGirl.create(:company_positions_cenco).id }
    request_date { Time.current }
    applicant_attributes { FactoryGirl.attributes_for(:applicant_bases) }
    revenue_expected 12345678
    workplace { FFaker::Company.name }
    observations { FFaker::Lorem.paragraph }
    company_contract_type_id { Company::ContractType.try(:last).try(:id) || FactoryGirl.create(:company_contract_type).id }
    company_estate_id { Company::Estate.try(:last).try(:last) || FactoryGirl.create(:company_estate).id }
    company_timetable_id { Company::Timetable.try(:last).try(:id) || FactoryGirl.create(:company_timetable).id }
    entry_date { Time.current + (3600*24) }
    company_vacancy_request_reason_id { Company::VacancyRequestReason.try(:last).try(:id) || FactoryGirl.create(:company_vacancy_request_reason).id }
    number_vacancies { rand(1..99) }
    required_by_email { FFaker::Internet.email }
    required_by_name { FFaker::Name.name }
    created_by { User.try(:last) || FactoryGirl.create(:user) }

    factory :hiring_request_record_all_new do
      company_positions_cenco_id { FactoryGirl.create(:company_positions_cenco).id }
      company_contract_type_id { FactoryGirl.create(:company_contract_type).id }
      company_estate_id { FactoryGirl.create(:company_estate).id }
      company_timetable_id { FactoryGirl.create(:company_timetable).id }
      company_vacancy_request_reason_id { FactoryGirl.create(:company_vacancy_request_reason).id }
    end
  end

  factory :hiring_request_record_invalid, :class => 'HiringRequest::Record' do
    company_positions_cenco_id nil
    request_date ""
    revenue_expected ""
    workplace ""
    observations ""
    company_contract_type_id nil
    company_estate_id nil
    company_timetable_id nil
    entry_date ""
    company_vacancy_request_reason_id nil
    number_vacancies 0
  end

end
