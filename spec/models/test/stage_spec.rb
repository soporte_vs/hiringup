require 'rails_helper'

RSpec.describe Test::Stage, :type => :model do
  context "RelationShips" do
    it 'should has_one test_report' do
      should have_one(:test_report).dependent(:destroy)
    end
  end

  context "RelationShips" do
    it 'should has_one leadership_interview_report' do
      should have_one(:test_report).dependent(:destroy)
    end
  end

  context 'Create' do
    it 'a valid test' do
      test_stage = FactoryGirl.create(:test_stage)
      expect(test_stage).to be_valid
      expect(test_stage).to be_a(Test::Stage)
    end
  end

  describe 'Not apply' do
    let(:not_apply_reason) { FactoryGirl.create(:stage_not_apply_reason) }

    context 'when action can be done' do
      it 'should create a Stage::NotApplying' do
        stage = FactoryGirl.create(:test_stage)
        expect{
          stage.not_apply(not_apply_reason, FFaker::Lorem.sentence)
        }.to change(Stage::NotApplying, :count).by(1)
      end
    end

    context 'when action can not be done' do
      it 'should not create a Stage::NotApplying' do
        test_report = FactoryGirl.create(:test_report)
        stage = test_report.test_stage
        expect{
          stage.not_apply(not_apply_reason, FFaker::Lorem.sentence)
        }.to change(Stage::NotApplying, :count).by(0)

        expect(stage.errors[:not_applying]).to_not be_empty
      end
    end
  end
end
