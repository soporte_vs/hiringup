require 'rails_helper'

RSpec.describe Test::Report, type: :model do
  context 'RelationShips' do
    it 'should belong a test_stage' do
      should belong_to :test_stage
    end
    it {
      should have_many(:documents).dependent(:destroy)
    }
  end

  context 'Validations' do
    it 'observations should be present' do
      should validate_presence_of :observations
    end

    it 'test_stage should be present' do
      should validate_presence_of :test_stage
    end
  end

  context 'Create' do
    it 'should create a valid' do
      expect(
        FactoryGirl.create(:test_report)
      ).to be_valid
    end

    it 'should build an invalid' do
      expect(
        FactoryGirl.build(:test_report_invalid)
      ).to_not be_valid
    end
  end
end
