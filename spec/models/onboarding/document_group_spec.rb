require 'rails_helper'

RSpec.describe Onboarding::DocumentGroup, type: :model do
  context 'RelationShips' do
    it 'should belong onboarding_stage' do
      should belong_to :onboarding_stage
    end

    it 'should belong document_group' do
      should belong_to :document_group
    end
  end

  context 'Validations' do
    it 'onboarding_stage should be present' do
      should validate_presence_of :onboarding_stage
    end

    it 'document_group should be present' do
      should validate_presence_of :document_group
    end
  end
end
