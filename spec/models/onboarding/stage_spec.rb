# == Schema Information
#
# Table name: stage_bases
#
#  id            :integer          not null, primary key
#  enrollment_id :integer
#  type          :string
#  created_at    :datetime
#  updated_at    :datetime
#

require 'rails_helper'

RSpec.describe Onboarding::Stage, :type => :model do
  context "Validate Oboarding Stage inheritance to Stage::Base" do
    it "should be ok" do
      expect(Onboarding::Stage.superclass).to eq(Stage::Base)
    end
  end

  context "Delagate" do
    it { should delegate_method(:document_group).to(:onboarding_document_group) }
  end

  context "Create a valid Onboarding::Stage" do
    context 'enrollment has an offer letter' do
      context 'is accepted' do
        before :each do
          @enrollment = FactoryGirl.create(:enrollment_basis_with_applicant)
          offer_stage = FactoryGirl.create(:offer_stage, enrollment: @enrollment)
          @letter = FactoryGirl.create(:offer_letter, stage: offer_stage, accepted: true)
          Delayed::Job.destroy_all
        end

        it "should be valid and send documents request email" do
          expect{
            expect(FactoryGirl.create(:onboarding_stage, enrollment: @enrollment)).to be_valid
          }.to change(Delayed::Job, :count).by(1).and\
           change(Onboarding::DocumentGroup, :count).by(1)

          email = YAML.load(Delayed::Job.last.handler)
          expect(email.object).to eq Onboarding::StageMailer
          expect(email.method_name).to eq :send_upload_documentation_request
          expect(email.args.include?(Onboarding::Stage.last)).to be true
        end
      end

      context 'is not accepted' do
        it "should be valid and not send an email" do
          enrollment = FactoryGirl.create(:enrollment_basis_with_applicant)
          offer_stage = FactoryGirl.create(:offer_stage, enrollment: enrollment)
          letter = FactoryGirl.create(:offer_letter, stage: offer_stage)

          Delayed::Job.destroy_all
          expect{
            expect(FactoryGirl.create(:onboarding_stage, enrollment: enrollment)).to be_valid
          }.to change(Delayed::Job, :count).by(0)
        end
      end
    end

    context 'enrollment does not have an offer letter' do
      it "should be valid and not send an email" do
        enrollment = FactoryGirl.create(:enrollment_basis_with_applicant)
        Delayed::Job.destroy_all
        expect{
            expect(FactoryGirl.create(:onboarding_stage, enrollment: enrollment)).to be_valid
          }.to change(Delayed::Job, :count).by(0)
      end
    end

    context "and create a Onboarding::DocumentGroup" do
      let(:enrollment) {FactoryGirl.create(:enrollment_dynamic)}

      it "should be valid and create onboarding_document_group with group of course when args is empty" do
        onboarding_stage = FactoryGirl.create(:onboarding_stage, enrollment: enrollment, name: FFaker::Name.name)
        expect(onboarding_stage.onboarding_document_group.document_group).to eq enrollment.course.document_group
      end

      it "should be valid and create onboarding_document_group with group by args" do
        document_group = FactoryGirl.create(:document_group)
        onboarding_stage = FactoryGirl.create(:onboarding_stage, enrollment: enrollment, args:{document_group: document_group.id}, name: FFaker::Name.name)

        expect(onboarding_stage.onboarding_document_group.document_group).to eq document_group
      end
    end
  end
end
