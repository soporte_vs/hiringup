# == Schema Information
#
# Table name: applicant_bases
#
#  id         :integer          not null, primary key
#  type       :string
#  user_id    :integer
#  created_at :datetime
#  updated_at :datetime
#

require 'rails_helper'

RSpec.describe Laborum::Applicant, type: :model do
  describe 'Elastic Search' do
    context 'index name' do
      it 'should be test-applicants', elasticsearch: true do |example|
        applicant = FactoryGirl.build(:laborum_applicant)
        expect(applicant.class.index_name).to eq "#{PREFIX_INDEX}-#{example.metadata[:block].object_id}"
      end
    end

    context 'mmapping name' do
      it 'should be test-applicant' do
        applicant = FactoryGirl.build :laborum_applicant
        expect(applicant.class.document_type).to eq 'applicants'
      end
    end
  end
end
