# == Schema Information
#
# Table name: laborum_publications
#
#  id                :integer          not null, primary key
#  service_id        :integer
#  title             :string
#  description       :text
#  state_id          :integer
#  city_id           :integer
#  area_id           :integer
#  subarea_id        :integer
#  pay_id            :integer
#  language_id       :integer
#  language_level_id :integer
#  type_job_id       :integer
#  study_area_id     :integer
#  study_type_id     :integer
#  study_status_id   :integer
#  salary            :integer
#  experience        :integer
#  question1         :string
#  question2         :string
#  question3         :string
#  question4         :string
#  question5         :string
#  course_id         :integer
#  status            :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  address           :string
#

require 'rails_helper'

RSpec.describe Laborum::Publication, type: :model do
  context 'Validations' do
    it { should validate_presence_of :title }
    it { should validate_presence_of :description }
    it { should validate_presence_of :course }
    it { should validate_presence_of(:pay_id) }
    it { should validate_presence_of(:area_id) }
    it { should validate_presence_of(:study_status_id) }
    it { should validate_presence_of(:study_type_id) }
    it { should validate_presence_of(:desactivate_at) }
  end

  context 'RelationShips' do
    it { should belong_to :course }
    it { should have_many :postulations }
    it { should have_many :applicants }
  end

  context 'Publications' do
    it 'after create add task to delayed job' do
      laborum_publication = FactoryGirl.create(:laborum_publication)
      expect(Delayed::Job.count).to eq(2)
      expect(laborum_publication.status).to eq('publishing')
    end

    it 'Unpublish publication' do
      laborum_publication = FactoryGirl.create(:laborum_publication)
      Delayed::Job.destroy_all
      laborum_publication.unpublish
      expect(Delayed::Job.count).to eq(1)
    end

    it 'create job desactivate publication' do
      Delayed::Job.destroy_all
      laborum_publication = FactoryGirl.create(:laborum_publication)
      expect(Delayed::Job.count).to eq(2)
    end
  end

  context 'Methods' do
    context 'has_account?' do
      it 'has_account? should return true' do
        expect(Laborum::Publication.has_account?).to eq(true)
      end

      it 'has_account? should return false' do
        Bumeran.client_id = nil
        expect(Laborum::Publication.has_account?).to eq(false)
      end
    end
  end

  describe '#get_laborum_publication_path' do
    let(:service_id) { 12345677 }
    let(:laborum_publication) { FactoryGirl.create(:laborum_publication, service_id: service_id) }

    it {
      expect(laborum_publication.get_laborum_publication_path).to eq "#{Laborum.get_publication_url}/#{service_id}.html"
    }
  end

  describe "#is_confidential?" do
    let(:laborum_publication){FactoryGirl.create(:laborum_publication)}

    context "when denomination_id is >= 0" do
      it "should response false" do
        laborum_publication.update(denomination_id: [*0..10].sample)
        expect(laborum_publication.is_confidential?).to be false
      end
    end

    context "when denomination_id is -1" do
      it "should response true" do
      laborum_publication.update(denomination_id: -1)
        expect(laborum_publication.is_confidential?).to be true
      end
    end
  end
  
end
