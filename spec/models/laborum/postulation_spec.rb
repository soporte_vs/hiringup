# == Schema Information
#
# Table name: laborum_postulations
#
#  id                     :integer          not null, primary key
#  laborum_publication_id :integer
#  applicant_id           :integer
#  answer1                :string
#  answer2                :string
#  answer3                :string
#  answer4                :string
#  answer5                :string
#  information            :text
#  cv                     :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#

require 'rails_helper'

RSpec.describe Laborum::Postulation, type: :model do
  before :each do
    allow_any_instance_of(Laborum::Postulation).to receive(:set_applicant_and_info).and_return(nil)
  end

  context 'Validations' do
    it { should validate_presence_of(:laborum_publication) }
    it { should validate_presence_of(:applicant) }

    it 'applicant should be unique scope to postulation' do
      postulation = FactoryGirl.create :laborum_postulation
      new_postulation = FactoryGirl.build(
        :laborum_postulation,
        applicant: postulation.applicant,
        laborum_publication: postulation.laborum_publication
      )
      expect(new_postulation).to_not be_valid
    end
  end

  context 'RelationShips' do
    it { should have_one(:course_postulation).dependent(:destroy) }
    it { should belong_to :laborum_publication }
    it { should belong_to :applicant }
  end

  describe '#create' do
    context 'a valid postulation and create a CoursePostulation' do
      it {
        expect{
          postulation = FactoryGirl.create(:laborum_postulation, is_accepted: nil, accepted_at: nil)
        }.to change(CoursePostulation, :count).by(1)
      }
    end
  end

  context 'Accept' do
    context 'when postulation has been not accepted yer' do
      it 'should return true and errors to eq 0' do
        postulation = FactoryGirl.create(:laborum_postulation, is_accepted: nil, accepted_at: nil)
        expect(postulation.accept).to be true
        expect(postulation.errors.count).to eq 0
      end
    end

    context 'when postulation has be discard' do
      it 'should return ok' do
        postulation = FactoryGirl.create(:laborum_postulation, is_accepted: nil, accepted_at: nil)
        expect(postulation.reject).to be true
        postulation.reload
        expect(postulation.is_accepted).to eq false
      end
    end

    context 'when postulation was acepted already' do
      it 'should return false and assign errors' do
        postulation = FactoryGirl.create(:laborum_postulation, is_accepted: true)
        expect(postulation.accept).to be false
        expect(postulation.course_postulation.errors.count).to eq 1
      end
    end
  end

  describe 'Delegates' do
    let(:laborum_postulation) { FactoryGirl.create(:laborum_postulation) }

    context '#course' do
      it { expect(laborum_postulation.course).to be_a Course }
    end

    context '#publication' do
      it { expect(laborum_postulation.publication).to be_a Laborum::Publication }
    end
  end

  describe "#get_identification_document_type" do
    let(:laborum_postulation) { FactoryGirl.create(:laborum_postulation) }
    let(:country_name) { "Chile" }
    let(:country_name_not_in_hupe) { "Chilito" }
    let(:identification_document_type_name) { "Pasaporte" }
    let(:identification_document_type_name_not_in_hupe) { "Documenti" }
    let(:country) { Territory::Country.where(name: country_name).first_or_create }
    let!(:identification_document_type) {
      FactoryGirl.create(:people_identification_document_type,
                         name: identification_document_type_name,
                         country: country
                        )
    }

    let(:expect_to_be_a_identification_document_type) {
      expect(
        laborum_postulation.get_identification_document_type(
          country_name,
          identification_document_type_name
        )
      ).to eq identification_document_type
    }
    context "when country name is not registered in hupe" do
      it "should return nil" do
        expect(
          laborum_postulation.get_identification_document_type(
            country_name_not_in_hupe,
            identification_document_type_name
          )
        ).to be nil
      end
    end

    context "when country name is registered in hupe" do
      context "and when codename is registered in hupe" do
        context "and codename is rut" do
          let(:identification_document_type_name) { "RUT" }
          it "should return a People::IdentificationDocumentType" do
          end
        end

        context "and codename is pasaporte" do
          it "should return a People::IdentificationDocumentType" do
          end
        end

        after :each do
          expect_to_be_a_identification_document_type
        end
      end

      context "and when codename is not registered in hupe" do
        it "should return nil" do
          expect(
            laborum_postulation.get_identification_document_type(
              country_name,
              identification_document_type_name_not_in_hupe
            )
          ).to be nil
        end
      end
    end
  end
end
