require 'rails_helper'

RSpec.describe Applicant::ProfileType, type: :model do
  context 'Validations' do
    it 'name should be present' do
      should validate_presence_of :name
    end

    it 'name should be uniq' do
      should validate_uniqueness_of :name
    end

  end

  context 'Build' do
    it 'should build an valid' do
      expect(FactoryGirl.build(:applicant_profile_type)).to be_valid
    end

    it 'should build an invalid' do
      expect(FactoryGirl.build(:applicant_profile_type_invalid)).to_not be_valid
    end
  end
end
