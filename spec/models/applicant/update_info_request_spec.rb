require 'rails_helper'

RSpec.describe Applicant::UpdateInfoRequest, type: :model do
  context 'Relations' do
    it { should belong_to :applicant}
  end

  context 'Validations' do
    it { should validate_presence_of :applicant }
  end

  context 'Create' do
    it 'should create a valid update_info_request' do
      expect(FactoryGirl.create :applicant_update_info_request).to be_valid
    end

    it 'should create an invalid update_info_request' do
      expect(FactoryGirl.build :applicant_update_info_request_invalid).to_not be_valid
    end
  end
end
