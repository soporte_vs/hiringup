# coding: utf-8
# == Schema Information
#
# Table name: applicant_bases
#
#  id         :integer          not null, primary key
#  type       :string
#  user_id    :integer
#  created_at :datetime
#  updated_at :datetime
#

require 'rails_helper'

RSpec.describe Applicant::Base, :type => :model do
  describe "Validations" do
    it { should serialize(:integration_data) }
  end

  context "Validate belongs_to" do
    it "should belongs_to a user" do
      should belong_to(:user).inverse_of(:applicant)
    end
    it "should belongs_to company_recruitment_source" do
      should belong_to(:company_recruitment_source)
    end
  end

  context "Validate relationship has_many" do
    it "should have_many comments" do
      should have_many(:comments).dependent(:destroy)
    end

    it "should have_many enrollments" do
      should have_many(:enrollments).dependent(:destroy)
    end

    it "should have_many document_records" do
      should have_many(:document_records).dependent(:destroy)
    end

    it "should have_many courses" do
      should have_many(:courses)
    end

    it 'should have many update_info_request' do
      should have_many(:update_info_requests).dependent(:destroy)
    end

    it { should have_many(:event_resourceables)
                 .dependent(:destroy) }
    it { should have_many(:events)
                 .through(:event_resourceables) }

    it { should serialize(:integration_data) }
    it { should have_many(:company_references)
                 .dependent(:destroy)}
  end


  describe 'Elastic Search' do
    context 'index name', elasticsearch: true do
      it 'should be test-applicants' do |example|
        applicant = FactoryGirl.create(:applicant_bases)
        expect(applicant.class.index_name).to eq "#{PREFIX_INDEX}-#{example.metadata[:block].object_id}"
        expect(applicant.class.document_type).to eq "applicants"
      end
    end

    context 'mmapping name' do
      it 'should be test-applicant' do
        applicant = FactoryGirl.build :applicant_bases
        expect(applicant.class.document_type).to eq 'applicants'
      end
    end
  end

  context 'Delegates' do
    it { should delegate_method(:get_rut).to(:user) }
    it { should delegate_method(:first_name).to(:user) }
    it { should delegate_method(:first_name_html).to(:user) }
    it { should delegate_method(:last_name).to(:user) }
    it { should delegate_method(:last_name_html).to(:user) }
    it { should delegate_method(:cellphone).to(:user) }
    it { should delegate_method(:cellphone_html).to(:user) }
    it { should delegate_method(:landline).to(:user) }
    it { should delegate_method(:landline_html).to(:user) }
    it { should delegate_method(:birth_date).to(:user) }
    it { should delegate_method(:birth_date_html).to(:user) }
    it { should delegate_method(:sex).to(:user) }
    it { should delegate_method(:sex_html).to(:user) }
    it { should delegate_method(:email).to(:user) }
    it { should delegate_method(:avatar).to(:user) }
    it { should delegate_method(:address).to(:user) }
    it { should delegate_method(:address_html).to(:user) }
  end

  context 'Get agreements' do
    it {
      FactoryGirl.create_list(:engagement_agreement, 3)
      agreement = FactoryGirl.create(:engagement_agreement)
      applicant = agreement.engagement_stage.enrollment.applicant
      expect(applicant.agreements).to match_array [agreement]
    }
  end

  describe 'profile_progress' do
    it 'should calculate total percentage rounded' do
      personal_information = People::PersonalInformation.new(
        first_name: FFaker::Name.first_name
      )
      user_attributes = FactoryGirl.attributes_for(
        :user_without_personal_information,
        personal_information: personal_information
      )
      user = User.new(user_attributes)

      exclude_attributes = Applicant::Base::PROGRESS_EXCLUDE_ATTRIBUTES

      attributes = People::PersonalInformation.column_names - exclude_attributes
      attributes_count = attributes.size + 1 # se agrega el degree

      applicant = Applicant::Base.new(user: user)

      # Cuando hay un solo atributo llenado
      expect(applicant.profile_progress).to eq (1 * 100 / attributes_count.to_f).round
      
      # Cuando hay dos atributos llenados
      personal_information.last_name = FFaker::Name.last_name
      expect(applicant.profile_progress).to eq (2 * 100 / attributes_count.to_f).round
      
      # Cuando hay tres atributos llenados
      personal_information.cellphone = FFaker::PhoneNumber.phone_number

      expect(applicant.profile_progress).to eq (3 * 100 / attributes_count.to_f).round
      
      # Cuando hay Todos los atributos llenados excepto el degree
      personal_information.update(FactoryGirl.attributes_for(:people_personal_information))

      expect(applicant.profile_progress).to eq (attributes.size * 100 / attributes_count.to_f).round
    end
  end

  describe '#postulation_data_completed?' do
    context 'when user does not has a personal_information' do
      let(:user) {  FactoryGirl.create(:user, personal_information: nil) }
      let(:applicant) { FactoryGirl.create(:applicant_bases, user: user) }

      it 'should return false' do
        expect(applicant.postulation_data_completed?).to be false
      end
    end

    context 'when user has a personal_information' do
      let(:user) {  FactoryGirl.create(:user) }
      let(:personal_information) { FactoryGirl.create(:personal_information, user: user) }
      let(:applicant) { FactoryGirl.create(:applicant_bases, user: user) }

      it 'should return false' do
        expect(applicant.postulation_data_completed?).to be true
      end
    end
  end

  context "TOUCH course_postulations" do
    it "should change updated at on applicant" do
      minisite_applicant = FactoryGirl.create(:minisite_applicant)
      minisite_applicant.update(blacklisted: true)

      expect(minisite_applicant.updated_at).not_to eq minisite_applicant.created_at
    end
  end

  describe 'postulation_data_completed?' do
    context 'personal_information is complete' do
      let(:applicant) { FactoryGirl.create(:applicant_bases) }

      it 'should return true' do
        expect(applicant.postulation_data_completed?).to be true
      end
    end

    context 'personal_information is not complete' do
      applicant = FactoryGirl.create(:applicant_bases)
      applicant.user.personal_information.update(last_name: nil)

      it 'should return false' do
        expect(applicant.postulation_data_completed?).to be false
      end
    end
  end

  describe '#self.generate_pdf' do
    applicant = FactoryGirl.create(:applicant_bases)
    let(:generate_pdf){ Applicant::Base.generate_pdf(applicant) }
    it 'should return a pdf file' do
      file_extension = File.extname(generate_pdf)
      expect(file_extension).to eq('.pdf')
    end
  end

  describe "is_enrolled?" do
    let(:applicant){FactoryGirl.create(:applicant_bases)}
    context "when applicant is enrolled" do
      it "should return true" do
        FactoryGirl.create(:enrollment_basis, applicant: applicant)
        applicant.reload
        expect(applicant.is_enrolled?).to be true
      end
    end
    context "when applicant is not enrolled" do
      it "should return false" do
        expect(applicant.is_enrolled?).to be false
      end
    end
  end

  describe '#has_references?' do
    let(:applicant) { FactoryGirl.create(:applicant_bases) }
    let(:has_references) { applicant.has_references? }
    let(:expected_result) { false }

    context 'reference exists' do
      let(:company_reference) { FactoryGirl.create(:company_reference, applicant: applicant) }

      context 'reference is confirmed' do
        before :each do
          company_reference.update(confirmed: true)
        end
        let(:expected_result) { true }

        it 'returns true' do
        end
      end

      context 'reference is not confirmed' do
        it 'returns false' do
        end
      end
    end

    context 'does not exist' do
      it 'returns false' do
      end
    end

    after :each do
      expect(has_references).to be expected_result
    end
  end

end
