# == Schema Information
#
# Table name: applicant_bases
#
#  id         :integer          not null, primary key
#  type       :string
#  user_id    :integer
#  created_at :datetime
#  updated_at :datetime
#

require 'rails_helper'

RSpec.describe Applicant::Catched, :type => :model do
  context "Applicant Catched relationship" do
    it "should belong a user" do
      should belong_to(:user)
    end
    it "should validate presence to user" do
      should validate_presence_of(:user)
    end
  end

  describe 'Elastic Search' do
    context 'index name' do
      it 'should be test', elasticsearch: true do |example|
        applicant = FactoryGirl.build(:applicant_catched)
        expect(applicant.class.index_name).to eq "#{PREFIX_INDEX}-#{example.metadata[:block].object_id}"
      end
    end

    context 'mmapping name' do
      it 'should be test-applicant' do
        applicant = FactoryGirl.build :applicant_catched
        expect(applicant.class.document_type).to eq 'applicants'
      end
    end
  end

  context "Create an Applicant Catched" do
    it "should be valid" do
      applicant_catched = FactoryGirl.create(:applicant_catched)
      expect(applicant_catched).to be_valid
    end
  end

  context "Create an appplicant and send invitation" do
    it "should create and appplicant, user and send email invitation" do
      expect(Delayed::Job.count).to eq(0)
      applicant = FactoryGirl.create(:applicant_catched_invitable)
      expect(applicant).to be_valid
      expect(Delayed::Job.count).to eq(1)
      expect(Delayed::Job.last.handler.include? applicant.user.email).to be true
    end
  end

  context "Create an Applicant Catched when email is associated to worker" do
    it "should create a Applicant Catched and associated it worker user" do
      worker = FactoryGirl.create(:worker_basis)
      password_user = worker.user.encrypted_password
      applicant_catched = FactoryGirl.create(:applicant_catched_invitable, user_attributes: { email: worker.user.email })
      worker.reload
      expect(worker.user.applicant.present?).to be true
      expect(worker.user.encrypted_password).to eq(password_user)
    end
  end
end
