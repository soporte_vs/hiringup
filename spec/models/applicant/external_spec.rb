# == Schema Information
#
# Table name: applicant_bases
#
#  id         :integer          not null, primary key
#  type       :string
#  user_id    :integer
#  created_at :datetime
#  updated_at :datetime
#

require 'rails_helper'

RSpec.describe Applicant::External, :type => :model do
  context "Applicant External relationship" do
    it "should belong a user" do
      should belong_to(:user)
    end
    it "should validate presence to user" do
      should validate_presence_of(:user)
    end
  end
  context "Create an Applicant External" do
    it "should be valid" do
      applicant_external = FactoryGirl.create(:applicant_external)
      expect(applicant_external).to be_valid
    end
  end

  describe 'Elastic Search' do
    context 'index name' do
      it 'should be test-applicants', elasticsearch: true do |example|
        applicant = FactoryGirl.build(:applicant_external)
        expect(applicant.class.index_name).to eq "#{PREFIX_INDEX}-#{example.metadata[:block].object_id}"
      end
    end

    context 'mmapping name' do
      it 'should be test-applicant' do
        applicant = FactoryGirl.build :applicant_external
        expect(applicant.class.document_type).to eq 'applicants'
      end
    end
  end
end
