require 'rails_helper'

RSpec.describe Offer::Stage, :type => :model do
  describe "Relationships" do
    it { should have_one(:offer_letter)
                 .class_name('Offer::Letter') }
  end

  context 'Create' do
    it 'a valid offer' do
      offer_stage = FactoryGirl.create(:offer_stage)
      expect(offer_stage).to be_valid
      expect(offer_stage).to be_a(Offer::Stage)
    end
  end

  describe 'Not apply' do
    let(:not_apply_reason) { FactoryGirl.create(:stage_not_apply_reason) }

    context 'when action can be done' do
      it 'should create a Stage::NotApplying' do
        stage = FactoryGirl.create(:offer_stage)
        expect{
          stage.not_apply(not_apply_reason, FFaker::Lorem.sentence)
        }.to change(Stage::NotApplying, :count).by(1)
      end
    end

    context 'when action can not be done' do
      it 'should not create a Stage::NotApplying' do
        stage = FactoryGirl.create(:offer_stage)
        offer_letter = FactoryGirl.create(:offer_letter, stage: stage)

        expect{
          stage.not_apply(not_apply_reason, FFaker::Lorem.sentence)
        }.to change(Stage::NotApplying, :count).by(0)

        expect(stage.errors[:not_applying]).to_not be_empty
      end
    end
  end

  describe '#is_approved' do
    context "when offer letter present" do
      let(:offer_letter) {FactoryGirl.create(:offer_letter)}

      it "should return nil " do
        expect(offer_letter.stage.is_approved?).to be nil
      end

      context "when offer letter is approved" do
        before :each do
          offer_letter.update(accepted: true)
        end

        it "should return true " do
          expect(offer_letter.stage.is_approved?).to be true
        end
      end

      context "when offer letter not is approved" do
        before :each do
          offer_letter.update(accepted: false)
        end

        it "should return false " do
          expect(offer_letter.stage.is_approved?).to be false
        end
      end
    end

    context "when offer stage not present" do
      let(:offer_stage) {FactoryGirl.create(:offer_stage)}

      it "should response nil" do
        expect(offer_stage.is_approved?).to be nil
      end
    end
  end

end
