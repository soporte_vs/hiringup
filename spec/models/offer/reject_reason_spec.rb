require 'rails_helper'

RSpec.describe Offer::RejectReason, type: :model do
  context 'Relationships' do
    it 'should have many offer letters' do
      should have_many(:offer_letters)
    end
  end

  context 'Validations' do
    it 'name should be present' do
      should validate_presence_of :name
    end

    it 'name should be unique' do
      should validate_uniqueness_of :name
    end
  end
end
