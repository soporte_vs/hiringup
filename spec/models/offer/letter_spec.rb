# coding: utf-8
require 'rails_helper'

RSpec.describe Offer::Letter, type: :model do
  describe "Relationships" do
    it { should belong_to(:stage)
                 .class_name('Offer::Stage') }

    it { should belong_to(:vacancy)
                  .inverse_of(:offer_letters) }
  end

  describe "Validations" do
    it { should validate_presence_of :stage }
    it { should validate_presence_of :vacancy }

    context "when creating a offer letter" do
      let(:ol){ FactoryGirl.build(:offer_letter)}
      before :each do
        ol.vacancy = FactoryGirl.create(:vacancy)
        ol.company_contract_type = FactoryGirl.create(:company_contract_type)
        ol.company_position = FactoryGirl.create(:company_position)
        ol.contract_start_date = FFaker::Time.date
        ol.direct_boss = FFaker::Name.name
      end

      it "should create a valid offer letter" do
        expect(ol).to be_valid
      end

      it 'should save a valid offer letter' do
        expect{
          ol.save
        }.to change(Offer::Letter, :count).by(1)
      end

      it 'should not be defined accepted' do
        expect(ol.accepted).to eq nil
      end

      it 'should have a token' do
        ol.save
        expect(ol.token).not_to be nil
      end
    end

    context "when checking if accepted" do
      it 'should return false if accepted is false' do
        ol = FactoryGirl.create(:offer_letter)
        ol.accepted = false
        ol.save
        expect(ol.accepted).to eq false
      end

      it 'should return true if accepted is true' do
        ol = FactoryGirl.create(:offer_letter)
        ol.accepted = true
        ol.save
        expect(ol.accepted).to eq true
      end

      it 'should return nil if accepted is not updated' do
        ol = FactoryGirl.create(:offer_letter)
        expect(ol.accepted).to eq nil
      end
    end
  end

  describe "Relationships" do
    it { should belong_to :offer_reject_reason }
  end

  context 'Accept an offer' do
    it 'should set accepted and send notification to psychologist and' do
      ol = FactoryGirl.create(:offer_letter)
      course = ol.stage.enrollment.course

      # Se elimina Onboarding del flujo para que no envíe el mail de onboarding
      # también, ya que al acepta automaticamente se pasa a la siguiente etapa
      # la cual es Onboarding::Stage
      course.update(work_flow: course.work_flow.reject{|wf| wf[:class_name] == 'Onboarding::Stage'})
      expect{
        result = ol.mark_accepted
        expect(result).to eq true
        expect(ol.accepted).to be true
      }.to change(Delayed::Job, :count).by(1)

      email = YAML.load(Delayed::Job.last.handler)
      expect(email.args.include? Offer::Letter.last).to be true
      expect(email.object).to eq Offer::StageMailer
      expect(email.method_name).to eq :send_notification_to_psychologist
    end

    it 'should return false if accepted is false, with no change of stage and no email sent' do
        offer_stage = FactoryGirl.create(:offer_stage)
        ol = FactoryGirl.create(:offer_letter_rejected, stage: offer_stage)
        ol_stage = ol.stage

        expect{
          result = ol.mark_accepted
          expect(result).to eq false
          expect(ol.accepted).to be false
        }.to change(Delayed::Job, :count).by(0)

        same_ol = Offer::Letter.find(ol.id)
        expect(same_ol.stage).to eq ol_stage
    end

    it 'should return true if already accepted, with no change of stage and no email sent' do
      offer_stage = FactoryGirl.create(:offer_stage)
      ol = FactoryGirl.create(:offer_letter, accepted: true, stage: offer_stage)
      ol_stage = ol.stage

      expect{
        result = ol.mark_accepted
        expect(result).to eq true
        expect(ol.accepted).to be true
      }.to change(Delayed::Job, :count).by(0)

      same_ol = Offer::Letter.find(ol.id)
      expect(same_ol.stage).to eq ol_stage
    end
  end

  context 'Reject an offer' do
    it 'should set accepted=false and send notification to psychologist' do
      offer_reject_reason = FactoryGirl.create(:offer_reject_reason)
      ol = FactoryGirl.create(:offer_letter)
      expect{
        result = ol.mark_rejected(offer_reject_reason.id)
        expect(result).to eq true
        expect(ol.accepted).to be false
      }.to change(Delayed::Job, :count).by(1)

      email = YAML.load(Delayed::Job.last.handler)
      expect(email.args.include? Offer::Letter.last).to be true
      expect(email.object).to eq Offer::StageMailer
      expect(email.method_name).to eq :send_notification_to_psychologist
    end

    it 'should return false if accepted is false, with no change of stage and no email sent' do
      offer_reject_reason = FactoryGirl.create(:offer_reject_reason)
      offer_stage = FactoryGirl.create(:offer_stage)
      ol = FactoryGirl.create(:offer_letter_rejected, stage: offer_stage)
      ol_stage = ol.stage

      expect{
        result = ol.mark_rejected(offer_reject_reason.id)
        expect(result).to eq false
        expect(ol.accepted).to be false
      }.to change(Delayed::Job, :count).by(0)

      same_ol = Offer::Letter.find(ol.id)
      expect(same_ol.stage).to eq ol_stage
    end

    it 'should return false if already accepted, with no change of stage and no email sent' do
      offer_reject_reason = FactoryGirl.create(:offer_reject_reason)
      offer_stage = FactoryGirl.create(:offer_stage)
      ol = FactoryGirl.create(:offer_letter_accepted, stage: offer_stage)
      ol_stage = ol.stage
      expect(ol.accepted).to eq true

      expect{
        result = ol.mark_rejected(offer_reject_reason.id)
        expect(result).to eq false
        expect(ol.accepted).to be true
      }.to change(Delayed::Job, :count).by(0)

      same_ol = Offer::Letter.find(ol.id)
      expect(same_ol.stage).to eq ol_stage
    end
  end
end
