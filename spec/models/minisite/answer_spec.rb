# == Schema Information
#
# Table name: minisite_answers
#
#  id                      :integer          not null, primary key
#  description             :string
#  minisite_question_id    :integer
#  minisite_postulation_id :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#

require 'rails_helper'

RSpec.describe Minisite::Answer, type: :model do
  context 'RelationShips' do
    it 'should belong_to a minisite_question' do
      should belong_to(:minisite_question)#.inverse_of(:answers)
    end

    it 'should belong_to a minisite_postulation' do
      should belong_to(:minisite_postulation).inverse_of(:answers)
    end
  end

  context 'Validations' do
    it 'description should be present' do
      should validate_presence_of :description
    end

    it 'minisite_question should be present' do
      should validate_presence_of :minisite_question
    end

    it 'minisite_postulation should be present' do
      should validate_presence_of :minisite_postulation
    end

    it 'question_id should be uniq scope postulation_id' do
      question = FactoryGirl.create :minisite_question
      publication = question.minisite_publication
      postulation = FactoryGirl.build :minisite_postulation, minisite_publication: publication
      postulation.answers << FactoryGirl.build(:minisite_answer, minisite_postulation: postulation, minisite_question: question)
      postulation.save
      expect(
        FactoryGirl.build(:minisite_answer, minisite_postulation_id: postulation.id, minisite_question_id: question.id)
      ).to_not be_valid
    end
  end
end
