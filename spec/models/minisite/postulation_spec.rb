# == Schema Information
#
# Table name: minisite_postulations
#
#  id                      :integer          not null, primary key
#  minisite_publication_id :integer
#  applicant_id            :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  is_accepted             :boolean
#  accepted_at             :date
#

require 'rails_helper'

RSpec.describe Minisite::Postulation, type: :model do
  context 'Validations' do
    it 'minisite_publication should be present' do
      should validate_presence_of :minisite_publication
    end

    it 'applicant should be present' do
      should validate_presence_of :applicant
    end

    it 'applicant should be unique socope to postulation' do
      postulation = FactoryGirl.create :minisite_postulation
      new_postulation = FactoryGirl.build(:minisite_postulation,
        applicant: postulation.applicant,
        minisite_publication: postulation.minisite_publication
      )
      expect(new_postulation).to_not be_valid
    end
  end

  context 'RelationShips' do
    it { should have_one(:course_postulation).dependent(:destroy) }

    it 'should belong to a minisite_publication' do
      should belong_to :minisite_publication
    end

    it 'should belong to a applicant' do
      should belong_to :applicant
    end

    it 'should has_many answer' do
      should have_many(:answers).inverse_of(:minisite_postulation)
    end

    it 'should has_many answer' do
      should have_many(:questions).through(:minisite_publication)
    end

    it 'should has answers as nested_attributes' do
      should accept_nested_attributes_for(:answers).allow_destroy(false)
    end
  end

  describe 'Delegates' do
    let(:minisite_postulation) { FactoryGirl.create(:minisite_postulation) }

    context '#course' do
      it { expect(minisite_postulation.course).to be_a Course }
    end

    context '#publication' do
      it { expect(minisite_postulation.publication).to be_a Minisite::Publication }
    end
  end

  describe '#answers_with_hupe_format' do
    let(:minisite_publication) { FactoryGirl.create(:minisite_publication) }

    before :each do
      FactoryGirl.build_list(:minisite_question, rand(3..5), minisite_publication: minisite_publication)
      minisite_publication.save
      minisite_publication.reload
    end

    it 'should return an array of hash with the follow formar: [{question: <XXX>, answer: <XXXX>}]' do
      postulation = FactoryGirl.build(:minisite_postulation)
      minisite_publication.questions.each do |question|
        postulation.answers.new(minisite_question: question, description: FFaker::Lorem.sentence)
      end
      expect(postulation.save).to be true
      postulation.answers_with_hupe_format.each do |answer|
        expect(answers.has_key?(:question)).to be true
        expect(answers.has_key?(:answer)).to be true
        expect(answers.length).to eq 2
      end
    end
  end

  context 'Create' do
    context 'When a course is a Internal' do
      let(:course) { FactoryGirl.create(:course, type_enrollment: Enrollment::Internal) }
      let(:minisite_publication) { FactoryGirl.create(:minisite_publication, course: course) }
      let(:minisite_postulation) { FactoryGirl.build(:minisite_postulation, minisite_publication: minisite_publication) }

      context 'and applicant rut is in Company::WhiteRuts' do
        before :each do
          white_rut = FactoryGirl.create(:company_white_rut, rut: minisite_postulation.applicant.get_rut)
        end

        it { expect(minisite_postulation.valid?).to be true }
      end

      context 'and applicant rut is not in Company::WhiteRuts' do
        it {
          expect(minisite_postulation.valid?).to be false
          expect(minisite_postulation.errors[:applicant].any?).to be true
        }
      end
    end


    it 'a valid postulation and create a CoursePostulation' do
      postulation = FactoryGirl.build(:minisite_postulation)
      publication = postulation.minisite_publication
      questions = FactoryGirl.create_list(:minisite_question, 3, minisite_publication: publication)
      questions.each do |question|
        postulation.answers << FactoryGirl.build(
          :minisite_answer,
          minisite_postulation: postulation,
          minisite_question: question
        )
      end
      expect{
        expect(postulation.save).to be true
      }.to change(CoursePostulation, :count).by(1)
    end

    it 'an invalid postulation' do
      expect(FactoryGirl.build(:minisite_postulation_invalid)).to_not be_valid
    end

    context 'when postulation does not has all answers for publications questions' do
      it 'should be invalid' do
        postulation = FactoryGirl.build(:minisite_postulation)
        publication = postulation.minisite_publication
        questions = FactoryGirl.create_list(:minisite_question, 3, minisite_publication: publication)
        expect(postulation).to_not be_valid
      end
    end

    context 'Send template internal postulation on create' do
      it 'should send internal postulation template' do
        applicant = FactoryGirl.create(:applicant_bases)
        white_rut = FactoryGirl.create(:company_white_rut, rut: applicant.get_rut)
        course = FactoryGirl.create(:course, type_enrollment: Enrollment::Internal)
        minisite_publication = FactoryGirl.create(:minisite_publication, course_id: course.id)
        Delayed::Job.destroy_all

        expect{
          Minisite::Postulation.create(minisite_publication_id: minisite_publication.id, applicant: applicant)
        }.to change(Delayed::Job, :count).by(1) # it has two after_create

        enrollment = Enrollment::Internal.last
        intenal_postulation_email = YAML.load(Delayed::Job.last.handler)
        expect(intenal_postulation_email.object).to eq Courses::InternalMailer
        expect(intenal_postulation_email.method_name).to eq :send_internal_postulation_template
        expect(intenal_postulation_email.args.include?(applicant)).to be true
        expect(intenal_postulation_email.args.include?(course)).to be true
      end
    end
  end

  describe 'Accept' do
    context 'when postulation has been not accepted yet' do
      it 'should return true and errors to eq 0' do
        postulation = FactoryGirl.create(:minisite_postulation, is_accepted: nil, accepted_at: nil)
        expect(postulation.accept).to be true
        expect(postulation.course_postulation.errors.count).to eq 0
      end
    end

    context 'when postulation was acepted already' do
      it 'should retur false and assign errors' do
        postulation = FactoryGirl.create(:minisite_postulation)
        expect(postulation.accept).to be false
        expect(postulation.course_postulation.errors.count).to eq 1
      end
    end
  end

  describe 'Reject' do
    context 'when postulation has been not accepted/rejected yet' do
      it 'should return true and errors to eq 0' do
        postulation = FactoryGirl.create(:minisite_postulation, is_accepted: nil, accepted_at: nil)
        expect(postulation.reject).to be true
        postulation.reload
        expect(postulation.is_accepted).to be false
        expect(postulation.course_postulation.errors.count).to eq 0
      end
    end

    context 'when postulation was rejected already' do
      it 'should return false and assign errors' do
        postulation = FactoryGirl.create(:minisite_postulation, is_accepted: false)
        expect(postulation.reject).to be false
        expect(postulation.is_accepted).to be false
        expect(postulation.course_postulation.errors.count).to eq 1
      end
    end

    context 'when postulation was accepted already' do
      it 'should return false and assign errors' do
        postulation = FactoryGirl.create(:minisite_postulation)
        expect(postulation.reject).to be false
        expect(postulation.is_accepted).to be true
        expect(postulation.course_postulation.errors.count).to eq 1
      end
    end
  end

end
