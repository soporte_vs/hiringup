# == Schema Information
#
# Table name: minisite_publications
#
#  id          :integer          not null, primary key
#  course_id   :integer
#  description :text
#  title       :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

require 'rails_helper'

RSpec.describe Minisite::Publication, type: :model do
  context 'Validations' do
    it 'title should be present' do
      should validate_presence_of :title
    end

    it 'description should be present' do
      should validate_presence_of :description
    end

    it 'desactivate_at should be present' do
      should validate_presence_of :desactivate_at
    end

    it 'course should be present' do
      should validate_presence_of :course
    end
  end

  context 'RelationShips' do
    it 'should belongs_to course' do
      should belong_to :course
    end

    it 'should has_many postulations' do
      should have_many :postulations
    end

    it 'should has_many minisite category publications' do
      should have_many(:minisite_category_publications).dependent(:destroy).inverse_of(:minisite_publication)
    end

    it 'should has_many minisite category publications' do
      should have_many(:minisite_categories).through(:minisite_category_publications)
    end

    it { should belong_to :company_contract_type }

    it 'should has_many applicants' do
      should have_many :applicants
    end

    it 'should has_many questions' do
      should have_many(:questions).inverse_of(:minisite_publication).dependent(:destroy)
    end

    it 'should has questions as nested_attributes' do
      should accept_nested_attributes_for(:questions).allow_destroy(true)
    end
  end

  context 'Create' do
    it 'a valid minisite_publication' do
      expect(FactoryGirl.create :minisite_publication).to be_valid
    end

    it 'an invalid minisite_publication' do
      expect(FactoryGirl.build :minisite_publication_invalid).to_not be_valid
    end

    it 'a publication with questions attributes' do
      publication = FactoryGirl.create :minisite_publication
      questions_attributes = [
        { description: FFaker::HipsterIpsum.paragraph },
        { description: FFaker::HipsterIpsum.paragraph },
        { description: '' }
      ]
      expect{
        publication.update(questions_attributes: questions_attributes)
      }.to change(publication.questions, :count).by(2)
      expect(publication.questions.first.description).to eq questions_attributes.first[:description]
      expect(publication.questions.last.description).to eq questions_attributes.second[:description]
    end

    it "should strip title" do
      title = "   example   "
      publication = FactoryGirl.create :minisite_publication, title: title
      expect(publication.title).to eq "example"

      title1 = "example   "
      publication1 = FactoryGirl.create :minisite_publication, title: title
      expect(publication1.title).to eq "example"
    end

    it 'job desactivate publication' do
      minisite_publication = FactoryGirl.create(:minisite_publication)
      expect(Delayed::Job.count).to eq(1)
    end
  end

  context 'methods' do
    it 'seo_url should return title of publication contact with dash and the id' do
      publication = FactoryGirl.create(:minisite_publication, title: 'hello world')
      expect(publication.seo_url).to eq("#{Rails.application.config.action_mailer.default_url_options[:host]}/#{publication.id}-hello-world")
    end
    it 'seo_path should return title of publication contact with dash and the id' do
      publication = FactoryGirl.create(:minisite_publication, title: 'hello world')
      expect(publication.seo_path).to eq("#{publication.id}-hello-world")
    end
  end

  context 'States Publications' do
    let(:minisite_publication) { FactoryGirl.create(:minisite_publication) }

    it 'should create a minisite publication opened' do
      expect(minisite_publication.opened?).to be true
    end

    it 'should close minisite_publication' do
      minisite_publication.close
      expect(minisite_publication.opened?).to be false
      expect(minisite_publication.closed?).to be true
    end

    context 'when course is closed' do
      before :each do
        course = minisite_publication.course
        course.close!
        minisite_publication.reload
      end

      it 'should not open minisite_publication' do
        expect(minisite_publication.closed?).to be true
        expect{
          minisite_publication.open
        }.to raise_error(AASM::InvalidTransition)
      end
    end
  end

  describe '#desactivation_scheduled' do
    let(:minisite_publication){FactoryGirl.create(:minisite_publication)}

    context 'when publication is open' do
      it 'should publication close' do
        expect(minisite_publication.opened?).to be true
        minisite_publication.desactivation_scheduled
        expect(minisite_publication.closed?).to be true
      end
    end

    context 'when publication is close' do
      before :each do
        minisite_publication.close!
      end

      it 'should not change publication' do
        expect(minisite_publication.closed?).to be true
        minisite_publication.desactivation_scheduled
        expect(minisite_publication.closed?).to be true
      end
    end
  end

  describe '#pdf_name' do
    it "should replace non alphanumeric characters with a dash" do
      minisite_publication = FactoryGirl.create(
        :minisite_publication,
        title: "Título ñoño pa'probar."
      )
      expect(minisite_publication.pdf_name).to eq "#{minisite_publication.created_at.strftime('%Y_%m_%d')}-Título-ñoño-pa-probar-"
    end
  end
 end
