# == Schema Information
#
# Table name: minisite_questions
#
#  id                      :integer          not null, primary key
#  minisite_publication_id :integer
#  description             :text
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#

require 'rails_helper'

RSpec.describe Minisite::Question, type: :model do
  context 'Relationships' do
    it 'should belong_to a publication' do
      should belong_to(:minisite_publication).inverse_of(:questions)
    end

    it 'should have_many answer' do
      should have_many(:answers).inverse_of(:minisite_question)
    end
  end

  context 'Validations' do
    it 'minisite_publication should be present' do
      should validate_presence_of(:minisite_publication)
    end

    it 'description should be present' do
      should validate_presence_of :description
    end

    it 'description should be uniq scope minisite_publication' do
      question = FactoryGirl.create :minisite_question
      expect(FactoryGirl.build(
        :minisite_question,
        minisite_publication: question.minisite_publication,
        description: question.description
      )).to_not be_valid
    end
  end
end
