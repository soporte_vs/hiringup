require 'rails_helper'

RSpec.describe Minisite::Category, type: :model do

  context 'RelationShips' do
    it { should have_many(:minisite_category_publications).dependent(:destroy) }
    it { should have_many(:minisite_publications) }
  end

  context 'Validation fields' do
    it 'name should be present' do
      should validate_presence_of :name
    end

    it 'description should be present' do
      should validate_presence_of :description
    end
  end

  context 'Create' do
    it 'should create a valid minisite_category' do
      expect(FactoryGirl.create :minisite_category).to be_valid
    end

    it 'should create an invalid minisite_category' do
      expect(FactoryGirl.build :minisite_category_invalid).to_not be_valid
    end
  end


end
