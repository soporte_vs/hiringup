require 'rails_helper'

RSpec.describe Minisite::CategoryPublication, type: :model do
  describe 'Relationships' do
    it { should belong_to(:minisite_category).inverse_of(:minisite_category_publications) }
    it { should belong_to(:minisite_publication).inverse_of(:minisite_category_publications) }
  end

  describe 'Validations' do
    it { should validate_presence_of(:minisite_category) }
    it { should validate_presence_of(:minisite_publication) }
  end

end
