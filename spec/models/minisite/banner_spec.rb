require 'rails_helper'

RSpec.describe Minisite::Banner, type: :model do
  context 'Validations overs fields' do
    it 'title should be present' do
      should validate_presence_of :title
    end

    it 'image should be present on create' do
      should validate_presence_of(:image).on(:create)
    end

    it "If active_image is selected the others must be inactive except the current" do
      FactoryGirl.create_list(:minisite_banner, 5)

      # Cambiar a true todos los active_image
      all_banners = Minisite::Banner.all
      all_banners.update_all(active_image: true)
      expect(all_banners.where(active_image: true).count).to be 5

      # validar que solo actualice el current_banner y lod demas queden en falso
      current_banner = Minisite::Banner.last
      all_banners = all_banners.where.not(id: current_banner.id).where(active_image: true).update_all(active_image: false)
      expect(all_banners).to be 4
      expect(current_banner.active_image).to be true
      expect(current_banner).to be_valid
    end
  end

  context 'Create' do
    it 'should create a valid minisite banner' do
      expect(FactoryGirl.create :minisite_banner).to be_valid
    end

    it 'should create an invalid minisite banner' do
      expect(FactoryGirl.build :minisite_banner_invalid).to_not be_valid
    end
  end


end
