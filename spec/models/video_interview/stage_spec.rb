require 'rails_helper'

RSpec.describe VideoInterview::Stage, :type => :model do
  before :each do
    allow_any_instance_of(VideoInterview::Answer).to receive(:get_video_state).and_return(nil)
  end

  context 'Create' do
    it 'a valid interview' do
      video_interview_stage = FactoryGirl.create(:video_interview_stage)
      expect(video_interview_stage).to be_valid
      expect(video_interview_stage).to be_a(VideoInterview::Stage)
    end
  end

  describe '#is_approved' do
    let(:course) { FactoryGirl.create(:course_with_questions) }
    let(:enrollment) { FactoryGirl.create(:enrollment_basis_with_applicant, course: course)}

    context 'uploaded all answers' do
      before :each do
        allow_any_instance_of(VideoInterview::Answer).to receive(:video_state).and_return(:uploaded)
      end

      it 'returns true' do
        video_interview_stage = FactoryGirl.create(:video_interview_stage, enrollment: enrollment)
        expect(video_interview_stage.is_approved?).to be true
      end
    end

    context 'uploaded some or no answers' do
      before :each do
        allow_any_instance_of(VideoInterview::Answer).to receive(:video_state).and_return(:not_uploaded)
      end

      it 'returns nil' do
        video_interview_stage = FactoryGirl.create(:video_interview_stage, enrollment: enrollment)
        expect(video_interview_stage.is_approved?).to be nil
      end
    end
  end

end
