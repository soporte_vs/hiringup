require 'rails_helper'

RSpec.describe Education::StudyLevel, type: :model do
  context 'Validations overs fields' do
    it 'name should be present' do
      should validate_presence_of :name
    end

    it 'name should be uniq' do
      should validate_uniqueness_of :name
    end
  end

  context 'RelationShips' do
  end

  context 'Create' do
    it 'should create a valid study level' do
      expect(FactoryGirl.create :education_study_level).to be_valid
    end

    it 'should create an invalid study level' do
      expect(FactoryGirl.build :education_study_level_invalid).to_not be_valid
    end
  end
end
