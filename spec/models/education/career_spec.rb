# == Schema Information
#
# Table name: education_careers
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Education::Career, type: :model do
  context "Validation on Education Careers" do
    it "should has presence name" do
      should validate_presence_of(:name)
    end
  end
end
