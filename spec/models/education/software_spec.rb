  require 'rails_helper'

  RSpec.describe Education::Software, type: :model do
    context "Validation on Model" do
      it "should validate name is present" do
        should validate_presence_of(:name)
      end

      it "should validate name is uniqueness" do
        should validate_uniqueness_of(:name)
      end
    end
  end
