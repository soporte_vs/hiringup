require 'rails_helper'

RSpec.describe Education::Institution, type: :model do
  describe "Validation on Model" do
    it { should validate_presence_of :name }

    it { should validate_uniqueness_of(:name)
                 .scoped_to(:type, :country_id) }

    it "should validate uniqueness of name scoped to type and country" do
      name = "ins1"
      country = Territory::Country.last || FactoryGirl.create(:territory_country)
      country2 = FactoryGirl.create(:territory_country)
      type = "x"

      i1 = FactoryGirl.create(:education_institution, name: name, type: type, country: country)
      i2 = FactoryGirl.build(:education_institution, name: name, type: type, country: country)
      expect(i2).not_to be_valid

      i3 = FactoryGirl.build(:education_institution, name: name, type: type, country: country2)
      expect(i3).to be_valid

      i4 = FactoryGirl.build(:education_institution, name: name, type: "z", country: country)
      expect(i4).to be_valid

      i5 = FactoryGirl.create(:education_institution, name: name, type: nil, country: nil)
      expect(i5).to be_valid

      i6 = FactoryGirl.build(:education_institution, name: name, type: nil, country: nil)
      expect(i6).not_to be_valid
    end
  end

  describe "Relationships" do
    it { should belong_to(:country)
                 .class_name("Territory::Country") }
  end

  describe "Delegations" do
    it { should delegate_method(:name).to(:country).with_prefix(true) }
  end

  describe "Create" do
    it "should be created as not custom if not specified" do
      i1 = FactoryGirl.create(:education_institution)
      expect(i1.custom).to be false
    end

    it "should be created as not custom if specified as falsy" do
      i1 = FactoryGirl.create(:education_institution, custom: nil)
      expect(i1.custom).to be false

      i2 = FactoryGirl.create(:education_institution, custom: 0)
      expect(i2.custom).to be false

      i3 = FactoryGirl.create(:education_institution, custom: "")
      expect(i3.custom).to be false
    end

    it "should be created as custom if specified as true and only true (not truthy)" do
      i1 = FactoryGirl.create(:education_institution_custom)
      expect(i1.custom).to be true

      i2 = FactoryGirl.create(:education_institution, custom: 1)
      expect(i2.custom).to be false
    end

    it "should NOT notify to anyone if it is NOT custom" do
      expect{
        FactoryGirl.create(:education_institution)
      }.to change(Delayed::Job, :count).by(0)
    end

    it "should notify to admin if it is custom" do
      expect{
        FactoryGirl.create(:education_institution_custom)
      }.to change(Delayed::Job, :count).by(1)

      i1 = Education::Institution.last
      expect(i1.custom).to be true

      mail = YAML.load(Delayed::Job.last.handler)
      expect(mail.object).to eq Education::InstitutionMailer
      expect(mail.method_name).to eq :notify_to_maintainer
      expect(mail.args.include?(i1)).to be true
    end
  end
end
