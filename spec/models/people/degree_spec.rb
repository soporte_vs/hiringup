# == Schema Information
#
# Table name: people_degrees
#
#  id                          :integer          not null, primary key
#  career_id                   :integer
#  professional_information_id :integer
#  condition                   :string
#  institution_id               :integer
#  culmination_date            :date
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#  name                        :string
#

require 'rails_helper'

RSpec.describe People::Degree, type: :model do
  context "Validation and RelationShip on Model " do
    it "should belong_to professional_information" do
      should belong_to :professional_information
    end
    it "should belong_to institution" do
      should belong_to :institution
    end
    it "should belong_to career" do
      should belong_to :career
    end

    context 'validate culmination_date' do
      it "when condition is EN CURSO" do
        degree = FactoryGirl.build(:people_degree, condition: 'EN CURSO', culmination_date: nil)
        expect(degree).to be_valid
      end

      it "when condition is EGRESADO" do
        degree = FactoryGirl.build(:people_degree, condition: 'EGRESADO', culmination_date: nil)
        expect(degree).to_not be_valid
      end
    end

    context 'validate condition' do
      it "should validate presence condition" do
        should validate_presence_of(:condition)
      end
      it 'should validate is a condition known' do
        should validate_inclusion_of(:condition).in_array(People::Degree::CONDITIONS)
      end
    end

    it "should validate presence institution" do
      should validate_presence_of(:institution)
    end

    it "should validate presence name " do
      should validate_presence_of(:name)
    end

    it "should validate presence career" do
      should validate_presence_of(:career)
    end

    it "should be uniqueness for professional_information, university, career" do
      people_degree = FactoryGirl.create(:people_degree)
      new_degree = FactoryGirl.build(
        :people_degree,
        institution: people_degree.institution,
        career: people_degree.career,
        professional_information: people_degree.professional_information
      )
      expect(new_degree).to_not be_valid
    end
  end

  describe '#methods' do
    describe 'translated_degree_type' do
      context 'when degree type translation is defined in TYPES' do
        let(:found_degree){People::Degree::TYPES.first}
        it 'returns appropiate translation' do
          expect(People::Degree.translated_degree_type(found_degree[0])).to eq found_degree[1]
        end
      end

      context 'when degree is not defined in types' do
        let(:incorrect_degree){FFaker::HipsterIpsum.sentence}
        it 'returns an empty string' do
          expect(People::Degree.translated_degree_type(incorrect_degree)).to eq ''
        end
      end
    end
  end
end
