# == Schema Information
#
# Table name: people_laboral_experiences
#
#  id                          :integer          not null, primary key
#  position                    :string
#  company                     :string
#  since_date                  :date
#  until_date                  :date
#  professional_information_id :integer
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#  description                 :text
#

require 'rails_helper'

RSpec.describe People::LaboralExperience, type: :model do
  context 'RelationShips' do
    it 'should belongs_to professional_information' do
      should belong_to :professional_information
    end
  end

  context 'Validations' do
    it 'position should be present' do
      should validate_presence_of :position
    end

    it 'company should be present' do
      should validate_presence_of :company
    end

    it 'since_date should be present' do
      should validate_presence_of :since_date
    end

    it 'position should be uniq scoped for company and professional_information' do
      laboral_experience = FactoryGirl.create :people_laboral_experience
      expect{
        FactoryGirl.create :people_laboral_experience, company: laboral_experience.company, position: laboral_experience.position, professional_information: laboral_experience.professional_information
      }.to raise_error(ActiveRecord::RecordInvalid)
    end
  end

  context 'Create' do
    it 'create a valid laboral_experience' do
      expect(FactoryGirl.create(:people_laboral_experience)).to be_valid
    end

    it 'create an invalid laboral_experience' do
      expect(FactoryGirl.build(:people_laboral_experience_invalid)).to_not be_valid
    end
  end
end
