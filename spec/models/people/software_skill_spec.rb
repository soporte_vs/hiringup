require 'rails_helper'

RSpec.describe People::SoftwareSkill, type: :model do
  context "RelationShip on Model " do
    it "should belong_to education_software" do
      should belong_to :education_software
    end

    it "should belong_to professional_information" do
      should belong_to :professional_information
    end

    context 'Validations' do
      it { should validate_presence_of(:level) }
      it { should validate_presence_of(:education_software_id) }
      it { should validate_presence_of(:professional_information_id) }
      it { should validate_inclusion_of(:level).in_array(People::SoftwareSkill::LEVELS) }

      it 'validates uniqueness of education_software' do
        expect(FactoryGirl.create(:people_software_skill)).to validate_uniqueness_of(:education_software_id).scoped_to(:professional_information_id)
      end
    end
  end
end
