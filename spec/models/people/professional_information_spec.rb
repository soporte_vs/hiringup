# == Schema Information
#
# Table name: people_professional_informations
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe People::ProfessionalInformation, type: :model do
  context "Validation on Model" do
    it "should belong to User" do
      should belong_to(:user).inverse_of(:professional_information)
    end

    it "should has_many to People::Degree" do
      should have_many(:degrees).dependent(:destroy)
    end

    it 'should has_many laboral_experiences' do
      should have_many(:laboral_experiences).dependent(:destroy)
    end

    it "should has_many to People::LanguageSkill" do
      should have_many(:language_skills).dependent(:destroy)
    end

    it "should accept nested attributes for degrees" do
      should accept_nested_attributes_for(:degrees)
    end

    it "should have_many to People::SoftwareSkill" do
       should have_many(:software_skills).dependent(:destroy)
    end

    it "should accept nested attributes for software_skills" do
      should accept_nested_attributes_for(:software_skills)
    end

    it "should accept nested attributes for laboral_experiences" do
      should accept_nested_attributes_for(:laboral_experiences)
    end

    it "should accept nested attributes for language_skills" do
      should accept_nested_attributes_for(:language_skills)
    end

    it "should validate that user is present" do
      should validate_presence_of(:user)
    end

    it "should ignore new degree if all attributes isn't present" do
      professional_information = FactoryGirl.create(:people_professional_information)
      new_degree_attributes = professional_information.degrees.new.attributes
      professional_information.degrees.destroy_all
      professional_information.update(degrees_attributes: new_degree_attributes)
      expect(professional_information.degrees.empty?).to be true
    end

    it "should not ignore new degree if there is someone attribute present" do
      professional_information = FactoryGirl.create(:people_professional_information)
      new_degree = professional_information.degrees.new(career: FactoryGirl.create(:education_career))
      new_degree_attributes = new_degree.attributes
      professional_information.degrees.destroy_all
      professional_information.update(degrees_attributes: new_degree_attributes)
      expect(professional_information.degrees.first.attributes).to eq new_degree_attributes
      expect(professional_information.degrees.first.persisted?).to_not be true
    end

    it 'should reject laboral_experiences empty and not save invalids' do
      professional_information = FactoryGirl.create :people_professional_information
      laboral_experience = FactoryGirl.create :people_laboral_experience, professional_information: professional_information
      professional_information.reload

      sentence = FFaker::HipsterIpsum.sentence
      company = FFaker::Company.name
      laboral_experiences_attributes = [
        { id: laboral_experience.id, _destroy: 1},
        { position: '', company: '', since_date: '', until_date: ''},
        { position: sentence, company: '', since_date: '', until_date: ''},
        { position: '', company: company, since_date: '', until_date: ''},
        { position: '', company: '', since_date: FFaker::Time.date, until_date: ''},
        { position: '', company: '', since_date: '', until_date: FFaker::Time.date},
        { position: sentence, company: company, since_date: FFaker::Time.date, until_date: FFaker::Time.date}
      ]

      professional_information.update(laboral_experiences_attributes: laboral_experiences_attributes)
      expect(professional_information.laboral_experiences.count).to eq 1
      expect(professional_information.laboral_experiences.length).to eq 6
    end
  end
end
