# == Schema Information
#
# Table name: people_personal_informations
#
#  id                :integer          not null, primary key
#  first_name        :string
#  last_name         :string
#  birth_date        :date
#  landline          :string
#  cellphone         :string
#  sex               :string
#  user_id           :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  territory_city_id :integer
#  avatar            :string
#

require 'rails_helper'

RSpec.describe People::PersonalInformation, type: :model do
  context "Validation and RelationShips on model" do
    it { should belong_to(:user).inverse_of(:personal_information) }
    it { should belong_to(:city) }
    it { should belong_to(:nationality) }
    it { should belong_to(:company_marital_status) }

    it { should have_db_column(:identification_document_number).of_type(:string) }
    it { should belong_to(:identification_document_type)
                  .class_name('People::IdentificationDocumentType')
                  .with_foreign_key(:identification_document_type_id) }

    it { should have_many(:personal_referrals).dependent(:destroy).inverse_of(:personal_information) }

    it { should validate_presence_of :user }

    it { should validate_presence_of :first_name }
    it { should validate_length_of(:first_name).is_at_least(3) }
    it { should validate_length_of(:first_name).is_at_most(25) }

    it { should validate_presence_of :last_name }
    it { should validate_length_of(:last_name).is_at_least(3) }
    it { should validate_length_of(:last_name).is_at_most(25) }

    it 'should validate presence of user' do
      should validate_presence_of :user
    end

    it 'should validate presence of first_name presence and min - max length' do
      should validate_presence_of :first_name
      should validate_length_of(:first_name).is_at_least(3)
      should validate_length_of(:first_name).is_at_most(25)
    end

    it { should validate_length_of(:landline).is_at_most(20) }
    it { should validate_length_of(:landline).is_at_least(5) }
    it { should allow_value('+56 5 555 55 55').for(:landline) }
    it { should_not allow_value('(+56) 5 555 55 55').for(:landline) }
    it { should_not allow_value('+kk s ttt gg hh').for(:landline) }

    it { should validate_length_of(:cellphone).is_at_most(20) }
    it { should validate_length_of(:cellphone).is_at_least(5) }
    it { should allow_value('+56 5 555 55 55').for(:cellphone) }
    it { should_not allow_value('(+56) 5 555 55 55').for(:cellphone) }
    it { should_not allow_value('+kk s ttt gg hh').for(:cellphone) }


    it { should validate_inclusion_of(:sex).in_array(People::PersonalInformation::SEX_OPTIONS) }
    it { should allow_value(nil).for(:sex) }

    it { should accept_nested_attributes_for(:personal_referrals) }

    context "validate birth_date over 18 years" do
      it "returns false and set the error" do
        personal_information = FactoryGirl.build(:people_personal_information, birth_date: Date.today)
        expect(personal_information.save).to be false
        expect(personal_information.errors[:birth_date]).to eq [I18n.t("activerecord.errors.models.people/personal_information.attributes.birth_date.not_adult")]
      end

      it "returns true when birth_date is exactly 18 years ago" do
        personal_information = FactoryGirl.build(:people_personal_information, birth_date: (Date.today - 18.years), user: FactoryGirl.build(:user))
        expect(personal_information.save).to be true
      end

      it "returns true when birth_date is more than 18 years ago" do
        personal_information = FactoryGirl.build(:people_personal_information, birth_date: (Date.today - 19.years), user: FactoryGirl.build(:user))
        expect(personal_information.save).to be true
      end
    end
  end

  context 'Methods for empty field' do
    it 'should return default value for any attribute' do
      personal_information = FactoryGirl.build(:people_personal_information,
        first_name: '',
        last_name: '',
        birth_date: '',
        landline: '',
        cellphone: '',
        sex: '',
        created_at: '',
        updated_at: '',
        territory_city_id: '',
        user_id: ''
      )
      default = I18n.t('activerecord.models.people.field_html_default')
      [
        :first_name,
        :last_name,
        :birth_date,
        :landline,
        :cellphone,
        :sex
      ].each do |attribute|
        value = personal_information.send("#{attribute}_html")
        expect(value).to eq I18n.t('activerecord.models.people.field_html_default')
      end
    end
  end

  context 'Create' do
    it 'a valid personal_information' do
      expect(FactoryGirl.create(:user).personal_information).to be_valid
    end
  end

  context 'Check AvatarUploader' do
    it 'avatar should be AvatarUploader' do
      user = FactoryGirl.create(:user)
      expect(user.personal_information.avatar.class).to eq AvatarUploader
    end
  end

  describe "#has_rut?" do
    let(:pi) { FactoryGirl.create(:user).personal_information }
    let(:identification_document_number) { "234234-0" }
    let(:identification_document_type_name) { "RUT" }
    let(:identification_document_type_country) { Territory::Country.where(name: "Chile").first_or_create }
    let(:identification_document_validation_regex) { nil }
    let(:identification_document_validate_uniqueness) { false }
    let(:identification_document_type) {
      attrs = FactoryGirl.attributes_for(
        :people_identification_document_type,
        country: identification_document_type_country,
        name: identification_document_type_name,
        validation_regex: identification_document_validation_regex,
        validate_uniqueness: identification_document_validate_uniqueness
      )
      People::IdentificationDocumentType.where(attrs).first_or_create
    }

    before :each do
      pi.identification_document_number = identification_document_number
      pi.identification_document_type = identification_document_type
      pi.save
    end

    context "identification document number is present" do
      context "identification document type is rut" do
        context "identification document type country is Chile" do
          it "should return true" do
            expect(pi.has_rut?).to be true
          end
        end

        context "identification document type country is not Chile" do
          let(:identification_document_type_country) { Territory::Country.where(name: "Argentina").first_or_create }
          it "should return true" do
            expect(pi.has_rut?).to be false
          end
        end
      end

      context "identification document type is not rut" do
        let(:identification_document_type_name) { "otro tipo" }
        it "should return false" do
          expect(pi.has_rut?).to be false
        end
      end
    end

    context "identification document number is not present" do
      let(:identification_document_number) { nil }
      it "should return false" do
        expect(pi.has_rut?).to be false
      end
    end
  end

  describe "identification_document_number validation" do
    let(:pi) { FactoryGirl.create(:user).personal_information }
    let(:identification_document_number) { "123456" }
    let(:identification_document_type_name) { "tipo" }
    let(:identification_document_validation_regex) { nil }
    let(:identification_document_validate_uniqueness) { false }
    let(:identification_document_type) {
      attrs = FactoryGirl.attributes_for(
        :people_identification_document_type,
        name: identification_document_type_name,
        validation_regex: identification_document_validation_regex,
        validate_uniqueness: identification_document_validate_uniqueness
      )
      People::IdentificationDocumentType.where(attrs).first_or_create
    }

    before :each do
      pi.identification_document_number = identification_document_number
    end

    context "when identification_document_number has spaces" do
      context "when it is only spaces" do
        let(:identification_document_number) { "          " }
        it "should set an empty string" do
          expect(pi.identification_document_number).to eq ""
        end
      end

      context "when it has a value with spaces" do
        context "when it has leading spaces" do
          let(:identification_document_number) { "          762354" }
          it "should remove all spaces" do
          end
        end

        context "when it has trailing spaces" do
          let(:identification_document_number) { "762354   " }
          it "should remove all spaces" do
          end
        end

        context "when it has leading and trailing spaces" do
          let(:identification_document_number) { "          762354        " }
          it "should remove all spaces" do
          end
        end

        after :each do
          expect(pi.identification_document_number).to eq "762354"
        end
      end
    end

    context "when identification_document_number is not present" do
      let(:identification_document_number) { "" }
      context "and identification_document_type is not present" do
        let(:identification_document_type_name) { nil }
        before :each do
          pi.identification_document_type = nil
        end

        it "should not be valid (have errors)" do
          expect(pi.identification_document_type).to be nil
          expect(pi).not_to be_valid
          expect(pi.errors[:identification_document_number]).not_to be_empty
        end
      end

      context "and identification_document_type is present" do
        before :each do
          pi.identification_document_type = identification_document_type
        end
        it "should not be valid" do
          expect(pi.identification_document_type).to be_present
          expect(pi).not_to be_valid
          expect(pi.errors[:identification_document_type]).to be_empty
          # should have blank error
          expect(pi.errors[:identification_document_number]).not_to be_empty
        end
      end
    end

    context "when identification_document_number is present" do
      it "should check identification_document_type is present" do
        expect(pi.identification_document_type).to be_present
        expect(pi).not_to be_valid
        expect(pi.errors[:identification_document_number]).not_to be_empty
      end

      context "and when identification_document_type is present" do
        before :each do
          pi.identification_document_type = identification_document_type
          expect(pi.identification_document_type).to be_present
        end

        context "and validation_regex is present" do
          context "and when identification_document_type is RUT" do
            let(:identification_document_type_name) { "RUT" }
            let(:identification_document_validation_regex) { "^([0-9]+-[0-9Kk])$" }
            let(:expected_result) {
              expect(pi).not_to be_valid
            }

            context "and number format is not valid" do
              it "should not be valid" do
              end
            end

            context "and number format is valid" do
              let(:identification_document_number) { "10000486-0" }
              let(:expected_result) {
                expect(pi).to be_valid
              }

              it "should be valid" do
              end
            end
          end

          context "and when identification_document_type is Pasaporte" do
            let(:identification_document_type_name) { "Pasaporte" }
            let(:identification_document_validation_regex) { "^[0-9a-zA-Z]+$" }
            let(:expected_result) {
              expect(pi).not_to be_valid
            }

            context "and number format is not valid" do
              let(:identification_document_number) { "12345678-9" }
              it "should not be valid" do
              end
            end

            context "and number format is valid" do
              let(:expected_result) {
                expect(pi).to be_valid
              }

              context "and number is only numbers" do
                let(:identification_document_number) { "123456789" }

                it "should be valid" do
                end
              end

              context "and number is only letters" do
                let(:identification_document_number) { "asdf" }

                it "should be valid" do
                end
              end

              context "and number is numbers and letters" do
                let(:identification_document_number) { "asdf123" }

                it "should be valid" do
                end
              end

            end
          end
        end

        context "validation_regex is not present" do
          let(:identification_document_number) { FFaker::Lorem.word }
          let(:expected_result) {
            expect(pi.identification_document_type.validation_regex).to be nil
            expect(pi).to be_valid
          }

          it "should be valid" do
          end
        end

        after :each do
          expected_result
        end
      end
    end

    context "when already exists another identification_document_number with the same identification_document_type" do
      let(:pi2) { FactoryGirl.create(:user).personal_information }
      before :each do
        pi.identification_document_type = identification_document_type
        expect(pi.save).to be true

        pi2.identification_document_number = identification_document_number
        pi2.identification_document_type = identification_document_type
      end

      context "when validate_uniqueness is true" do
        let(:identification_document_validate_uniqueness) { true }
        it "should not save People::PersonalInformation record and add errors" do
          expect(pi2.save).to be false
          expect(pi2.errors[:identification_document_number]).not_to be_empty
        end
      end

      context "when validate_uniqueness is false" do
        it "should save People::PersonalInformation record" do
          expect(pi2.save).to be true
        end
      end
    end
  end
end
