# coding: utf-8
require 'rails_helper'

RSpec.describe People::IdentificationDocumentType, type: :model do
  describe "Relationships" do
    it { should have_many(:people_personal_informations)
                  .class_name('People::PersonalInformation')
                  .with_foreign_key(:identification_document_type_id)
                  .dependent(:nullify)
    }
    it { should belong_to(:country)
                  .class_name('Territory::Country')
                  .with_foreign_key(:country_id) }
  end

  describe "Validations" do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:codename) }
    it { should validate_presence_of(:country) }

    it { should validate_uniqueness_of(:name)
               .scoped_to(:country_id) }
    it { should validate_uniqueness_of(:codename)
               .scoped_to(:country_id) }

    context "validate codename format" do
      let(:name) {"mi tipo"}
      let(:identification_document_type) {
        FactoryGirl.create(:people_identification_document_type, name: name)
      }

      it "should parametrize name" do
      end

      context "when name have many spaces" do
        let(:name) { "  mi   tipo    " }
        it "should squish spaces and parametrize the string" do
        end
      end

      context "when name have uppercased letters" do
        let(:name) { "Mi TiPo" }
        it "should downcase letters and parametrize the string" do
        end
      end

      context "when name have letters with tildes" do
        let(:name) { "Mi TíPò" }
        it "should set letters without tildes and parametrize the string" do
        end
      end

      after :each do
        expect(identification_document_type.codename).to eq "mi-tipo"
      end
    end
  end

  # This method retrieves the countries having an identification document type
  describe "People::IdentificationDocumentType#countries" do
    let!(:countries) { FactoryGirl.create_list(:territory_country, 10) }

    it "should return 3 countries" do
      expect(Territory::Country.count).to be 10

      identification_document_type1 = FactoryGirl.create(:people_identification_document_type, country: countries.first)
      identification_document_type2 = FactoryGirl.create(:people_identification_document_type, country: countries.second)
      identification_document_type3 = FactoryGirl.create(:people_identification_document_type, country: countries.third)

      expect(People::IdentificationDocumentType.countries.count).to eq 3
      expect(People::IdentificationDocumentType.countries.first).to eq countries.first
      expect(People::IdentificationDocumentType.countries.second).to eq countries.second
      expect(People::IdentificationDocumentType.countries.third).to eq countries.third

    end
  end
end
