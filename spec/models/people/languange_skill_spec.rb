require 'rails_helper'

RSpec.describe People::LanguageSkill, type: :model do
  context "Validation and RelationShip on Model " do
    it "should belong_to education_language" do
      should belong_to :education_language
    end

    it "should belong_to professional_information" do
      should belong_to :professional_information
    end

    it "should validate that education_language is present" do
      should validate_presence_of(:education_language)
    end

    it "should validate that professional_information is present" do
      should validate_presence_of(:professional_information)
    end

    context 'validate level' do
      it "should validate presence level" do
        should validate_presence_of(:level)
      end

      it 'should validate is a level known' do
        should validate_inclusion_of(:level).in_array(People::LanguageSkill::LEVELS)
      end
    end
  end
end
