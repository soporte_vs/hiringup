require 'rails_helper'

RSpec.describe People::PersonalReferral, type: :model do
  context "RelationShip on Model " do
    it "should belong_to personal_information" do
      should belong_to(:personal_information).inverse_of(:personal_referrals)
    end

    it "should validate that personal_information is present" do
      should validate_presence_of(:personal_information)
    end
  end

  context 'Validations' do
      it { should validate_presence_of(:full_name) }
      it { should validate_presence_of(:phone) }
      it { should validate_presence_of(:email) }
      it { should validate_uniqueness_of(:email).scoped_to(:personal_information_id) }

      it 'email should be an email' do
        personal_referral = FactoryGirl.create(:people_personal_referral)
        expect(personal_referral.email =~ /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i).to_not eq(nil)
      end
  end
end
