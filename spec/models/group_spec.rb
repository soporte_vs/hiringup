# == Schema Information
#
# Table name: groups
#
#  id          :integer          not null, primary key
#  name        :string
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

require 'rails_helper'

RSpec.describe Group, type: :model do
  context 'Relationships' do
    it 'should has_many user_groups' do
      should have_many(:user_groups).dependent(:destroy)
    end

    it 'should has_many users' do
      should have_many :users
    end
  end

  context 'Validations' do
    it 'name should be present' do
      should validate_presence_of :name
    end

    it 'description should be present' do
      should validate_presence_of :description
    end

    it 'name should be uniq' do
      should validate_uniqueness_of :name
    end
  end

  context 'Create' do
    it 'should create a valid' do
      expect(FactoryGirl.create :group).to be_valid
    end

    it 'should create an invalid' do
      expect(FactoryGirl.build :group_invalid).to_not be_valid
    end
  end
end
