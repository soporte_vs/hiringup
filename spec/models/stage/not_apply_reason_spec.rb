require 'rails_helper'

RSpec.describe Stage::NotApplyReason, type: :model do
  context "create" do
    it 'should create a not apply reason' do
      not_apply_reason = FactoryGirl.create(:stage_not_apply_reason)
      expect(not_apply_reason.id).not_to be nil
      expect(not_apply_reason).to be_persisted
    end

    it 'should not create an invalid not apply reason' do
      invalid_nar = FactoryGirl.build(:stage_not_apply_reason_invalid)
      expect(invalid_nar).not_to be_valid
      expect(invalid_nar).not_to be_persisted
      expect(invalid_nar.save).to be false
    end
  end

  context "retrieve" do
    it 'should retrieve last reason' do
      FactoryGirl.create(:stage_not_apply_reason)
      not_apply_reason = Stage::NotApplyReason.last
      expect(not_apply_reason.id).not_to be nil
      expect(not_apply_reason.name).not_to be nil
      expect(not_apply_reason).to be_a_kind_of(Stage::NotApplyReason)
    end
  end

  context "update" do
    it 'should update with valid attributes' do
      not_apply_reason = FactoryGirl.create(:stage_not_apply_reason)
      not_apply_reason_new_attributes = FactoryGirl.attributes_for(:stage_not_apply_reason)
      not_apply_reason.update(not_apply_reason_new_attributes)
      expect(not_apply_reason.name).to eq not_apply_reason_new_attributes[:name]
      expect(not_apply_reason.description).to eq not_apply_reason_new_attributes[:description]
    end

    it 'should not update with invalid attributes' do
      not_apply_reason = FactoryGirl.create(:stage_not_apply_reason)
      not_apply_reason_new_attributes = FactoryGirl.attributes_for(:stage_not_apply_reason_invalid)
      not_apply_reason.update(not_apply_reason_new_attributes)
      expect(not_apply_reason.errors).not_to be_empty
      expect(not_apply_reason.save).to be false
    end

  end

  context "destroy" do
    it 'should delete a not apply reason' do
      not_apply_reason = FactoryGirl.create(:stage_not_apply_reason)
      nar_id = not_apply_reason.id
      not_apply_reason.destroy
      expect{ Stage::NotApplyReason.find(nar_id) }.to raise_exception(ActiveRecord::RecordNotFound)
    end
  end
end
