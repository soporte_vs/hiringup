require 'rails_helper'

RSpec.describe Stage::NotApplying, type: :model do
  context "Validation model" do
    it "should has a validation of stage and not_apply_reason" do
      should validate_presence_of(:stage)
      should validate_presence_of(:not_apply_reason)
    end

    it "should has validation uniqueness of stage (only one NotApplying for each stage)" do
      not_applying = FactoryGirl.create(:stage_not_applying)
      expect(not_applying).to be_valid

      not_applying2 = FactoryGirl.build(:stage_not_applying,
                                        stage: not_applying.stage,
                                        not_apply_reason: not_applying.not_apply_reason
                                       )

      # with same attributes is not valid!
      expect(not_applying2).not_to be_valid
      expect(not_applying2.errors).not_to be_empty

      # same reason, another stage: is valid!
      not_applying2.stage = FactoryGirl.create(:stage_basis)
      expect(not_applying2).to be_valid

      #same stage, another reason, NOT valid!
      not_applying2.stage = not_applying.stage
      not_applying2.not_apply_reason = FactoryGirl.create(:stage_not_apply_reason)
      expect(not_applying2).not_to be_valid
    end
  end

  context "Relationships" do
    it "should be related to not apply reason" do
      should belong_to :not_apply_reason
    end

    it "should be related to stage" do
      should belong_to :stage
    end
  end

  context "create a valid not applying" do
    it "should accept any inherited Stage::Base" do
      not_applying = FactoryGirl.create(:stage_not_applying)

      expect(not_applying).to be_persisted
      expect(not_applying.stage).to be_a_kind_of(Stage::Base)
      expect(not_applying.not_apply_reason).to be_a_kind_of(Stage::NotApplyReason)

      stage1 = FactoryGirl.create(:engagement_stage)
      not_apply_reason1 = FactoryGirl.create(:stage_not_apply_reason)

      not_applying1 = FactoryGirl.create(:stage_not_applying,
                                        stage: stage1,
                                        not_apply_reason: not_apply_reason1)

      expect(not_applying1).to be_persisted
      expect(not_applying1.stage).to be_a_kind_of(Stage::Base)
      expect(not_applying1.stage).to be_a_kind_of(stage1.class)
      expect(not_applying1.not_apply_reason).to be_a_kind_of(Stage::NotApplyReason)
    end
  end
end
