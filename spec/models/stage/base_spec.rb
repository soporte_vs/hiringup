# == Schema Information
#
# Table name: stage_bases
#
#  id            :integer          not null, primary key
#  enrollment_id :integer
#  type          :string
#  created_at    :datetime
#  updated_at    :datetime
#

require 'rails_helper'

RSpec.describe Stage::Base, :type => :model do
  describe "Relationships" do
    it { should belong_to :enrollment }
    it { should have_many(:event_resourceables).dependent(:destroy) }
    it { should have_many(:events).through(:event_resourceables) }
  end

  context "Validations Stage" do
    it "should has name and enrollment" do
      should validate_presence_of(:enrollment)
    end

    it "should validate presence of name" do
      # Stage::Base tiene un callback on: :create que siempre cambia el
      # nombre, por lo que siempre falla testear la validación al hacer un 
      # should validate_presence_of que usa internamente un create.
      stage_with_no_name = FactoryGirl.build(:stage_basis, name: '')
      expect(stage_with_no_name).not_to be_valid 
    end
  end

  context "Create a Stage base" do
    it "should be valid" do
      expect(FactoryGirl.create(:stage_basis)).to be_valid
    end
  end

  context 'Enrollment stage Disposable?' do
    it 'should return true' do
      stage = FactoryGirl.create :stage_basis
      expect(stage.disposable?).to eq true
    end
  end

  context 'Validate attr_accessor ARGS' do
    it "should return true" do
      stage = FactoryGirl.build(:stage_basis, args: {message: FFaker::Lorem.sentence})
      expect(stage.valid?).to be true
    end
  end

  context 'Stage Repetead' do
    it 'sets different step numbers to repeated stages' do
      course = FactoryGirl.create(:course)
      work_flow = [
        {type: :module, class_name: 'Onboarding::Stage', name: 'Stage'},
        {type: :module, class_name: 'Stage::Base', name: 'Stage'},
        {type: :module, class_name: 'Stage::Base', name: 'Stage'}
      ]
      course.update(work_flow: work_flow)
      enrollment = FactoryGirl.create(:enrollment_basis_with_applicant, course: course)
      enrollment.next_stage
      expect(enrollment.stages.last.step).to eq 0
      enrollment.next_stage
      expect(enrollment.stages.last.step).to eq 1
      enrollment.next_stage
      expect(enrollment.stages.last.step).to eq 2
    end
  end

  describe "agendable?" do
    context "when stage has an event" do
      it "returns false" do
        enrollment = FactoryGirl.create(:enrollment_basis_with_applicant)
        enrollment.next_stage
        enrollment.reload
        stage = enrollment.stage
        event = FactoryGirl.create(:calendar_event, stages: [stage])
        expect(event.stages.count).to eq 1
        stage.reload
        expect(stage.agendable?).to be false
      end
    end

    context "when stage has no event" do
      it "returns true" do
        enrollment = FactoryGirl.create(:enrollment_basis_with_applicant)
        enrollment.next_stage
        enrollment.reload
        stage = enrollment.stage
        expect(stage.agendable?).to be true
      end
    end

    it 'should has one not_applyings' do
      should have_one(:not_applying).dependent(:destroy)
    end
  end

  context "Not apply" do
    it "should return a NotApplying object" do
      # enrollment = FactoryGirl.create(:enrollment_basis_with_applicant)
      # enrollment.next_stage
      # stage = enrollment.stage
      not_apply_reason = FactoryGirl.create(:stage_not_apply_reason)
      observations = FFaker::Lorem.paragraph
      stage = FactoryGirl.create(:stage_basis)
      not_applying = stage.not_apply(not_apply_reason, observations)
      expect(not_applying).to be_a_kind_of(Stage::NotApplying)
      expect(not_applying).to be_persisted
      expect(not_applying.observations).to eq observations
    end

    it 'should create a NotApplying without observations' do
      not_apply_reason = FactoryGirl.create(:stage_not_apply_reason)
      stage = FactoryGirl.create(:stage_basis)
      not_applying = stage.not_apply(not_apply_reason)
      expect(not_applying).to be_a_kind_of(Stage::NotApplying)
      expect(not_applying).to be_persisted
      expect(not_applying.observations).to eq nil
    end

    it 'should not duplicate an existing NotApplying' do
      not_apply_reason = FactoryGirl.create(:stage_not_apply_reason)
      stage = FactoryGirl.create(:stage_basis)
      not_applying = stage.not_apply(not_apply_reason)
      expect(not_applying).to be_a_kind_of(Stage::NotApplying)
      expect(not_applying).to be_persisted
      expect(not_applying.observations).to eq nil

      not_applying_again = stage.not_apply(not_apply_reason)
      expect(not_applying_again).to be_a_kind_of(Stage::NotApplying)
      expect(not_applying_again).not_to be_persisted
      expect(not_applying_again.errors).not_to be_empty
    end

    it 'should move enrollment to next stage' do
      not_apply_reason = FactoryGirl.create(:stage_not_apply_reason)
      stage = FactoryGirl.create(:stage_basis)
      stage.enrollment.next_stage
      expect(stage.enrollment.current_stage_index).to eq 0
      not_applying = stage.not_apply(not_apply_reason)
      expect(stage.enrollment.current_stage_index).to eq 1
      expect(stage.enrollment.stage.class.to_s).to eq stage.enrollment.class::WORK_FLOW[stage.enrollment.current_stage_index][:class_name]
    end
  end

  describe "#to_s" do
    it "should return name" do
      stage_name = FFaker::Lorem.word
      stage = FactoryGirl.create(:stage_basis, name: stage_name)
      expect("#{stage}").to eq stage_name
    end
  end

  describe "#is_approved?" do
    let(:stage_basis) {FactoryGirl.create(:stage_basis)}
    let(:stage_basis_true) {FactoryGirl.create(:stage_basis, is_approved: true)}
    let(:stage_basis_false) {FactoryGirl.create(:stage_basis, is_approved: false)}


    context "when stage is_approved is true" do
      it "should return true" do
        expect(stage_basis_true.is_approved?).to eq true
      end
    end

    context "when stage is_approved is false" do
      it "should return false" do
        expect(stage_basis_false.is_approved?).to eq false
      end
    end

    context "when stage doesn't approved" do
      it "should return nil." do
        expect(stage_basis.is_approved?).to eq nil
      end
    end
  end
end
