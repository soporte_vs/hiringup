require 'rails_helper'

RSpec.describe Stage::Document, :type => :model do
  describe 'RelationShips' do
    it {
      should belong_to(:resource)
    }
  end

  describe 'Validations' do
    it {
      should validate_presence_of(:document)
    }
    it {
      should validate_presence_of(:resource)
    }
  end

  describe 'Create a valid' do
    it {
      stage_document = FactoryGirl.create(:stage_document)
      expect(stage_document.valid?).to be true
    }
  end
end
