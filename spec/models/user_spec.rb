# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default("")
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string
#  last_sign_in_ip        :string
#  created_at             :datetime
#  updated_at             :datetime
#  invitation_token       :string
#  invitation_created_at  :datetime
#  invitation_sent_at     :datetime
#  invitation_accepted_at :datetime
#  invitation_limit       :integer
#  invited_by_id          :integer
#  invited_by_type        :string
#  invitations_count      :integer          default(0)
#

require 'rails_helper'

RSpec.describe User, :type => :model do
  it { should have_many(:applicant_reports).dependent(:nullify) }

  context "Relationship user" do
    it "should has one applicant" do
      should have_one(:applicant).inverse_of(:user).dependent(:destroy)
    end
    it "should has one worker" do
      should have_one(:worker).dependent(:destroy)
    end

    it "should has personal_information" do
      should accept_nested_attributes_for(:personal_information)
      should have_one(:personal_information).dependent(:destroy).inverse_of(:user)
    end

    it "should has professional_information" do
      should accept_nested_attributes_for(:professional_information)
      should have_one(:professional_information).dependent(:destroy).inverse_of(:user)
    end

    it "should has many activities" do
      should have_many(:activities)
    end

    it "should has many user_groups" do
      should have_many(:user_groups).dependent(:destroy)
    end

    it { should have_many(:comments).dependent(:destroy) }
  end

  context "Add a role unkown on object course" do
    it "should not create a new Roler" do
      user = FactoryGirl.create(:user)
      expect{
        user.add_role :aknsdkjnd, FactoryGirl.create(:course)
      }.to change(user.roles, :count).by(0).and change(Role, :count).by(0)
    end
  end

  context "Add a role unkown on class Course" do
    it "should not create a new Roler" do
      user = FactoryGirl.create(:user)
      expect{
        user.add_role :aknsdkjnd, Course
      }.to change(user.roles, :count).by(0).and change(Role, :count).by(0)
    end
  end

  context "Add a global role unkown" do
    it "should not create a new Roler" do
      user = FactoryGirl.create(:user)
      expect{
        user.add_role :aknsdkjnd
      }.to change(user.roles, :count).by(0).and change(Role, :count).by(0)
    end
  end

  context "Check individual role on user" do
    it "user has role admin all but does not has role admin Course" do
      user = FactoryGirl.create(:user)
      user.add_role :admin
      expect(user.check_role? :admin, Course).to be false
      expect(user.has_role? :admin, Course).to be true
      expect(user.check_role? :admin).to be true
    end
  end

  context 'Get Avatar' do
    before :each do
      @user = FactoryGirl.create(:user_no_avatar)
    end

    it 'should respose avatar female' do
      @user.personal_information.update(sex: 'Female')
      expect(@user.avatar).to eq "avatar_femenino.png"
    end

    it 'should respose avatar male' do
      @user.personal_information.update(sex: 'Male')
      expect(@user.avatar).to eq "avatar_masculino.png"
    end

    it 'should respose avatar default' do
      @user.personal_information.update(sex: '')
      expect(@user.avatar).to eq "default_avatar.png"
    end
  end

  context 'Delegates' do
    it { should delegate_method(:get_rut).to(:personal_information) }
    it { should delegate_method(:identification_document_number).to(:personal_information) }
    it { should delegate_method(:identification_document_type).to(:personal_information) }
    it { should delegate_method(:first_name).to(:personal_information) }
    it { should delegate_method(:last_name).to(:personal_information) }
    it { should delegate_method(:birth_date).to(:personal_information) }
    it { should delegate_method(:landline).to(:personal_information) }
    it { should delegate_method(:cellphone).to(:personal_information) }
    it { should delegate_method(:sex).to(:personal_information) }

    it { should delegate_method(:identification_document_number_html).to(:personal_information) }
    it { should delegate_method(:first_name_html).to(:personal_information) }
    it { should delegate_method(:last_name_html).to(:personal_information) }
    it { should delegate_method(:birth_date_html).to(:personal_information) }
    it { should delegate_method(:landline_html).to(:personal_information) }
    it { should delegate_method(:cellphone_html).to(:personal_information) }
    it { should delegate_method(:sex_html).to(:personal_information) }

    it { should delegate_method(:address).to(:personal_information) }
    it { should delegate_method(:address_html).to(:personal_information) }
  end

  describe "#sex_word" do
    let(:user) { FactoryGirl.create(:user) }
    it "should return 'Male'" do
      user.personal_information.update(sex: "Male")
      expect(user.sex_word).to eq I18n.t("activerecord.models.people/personal_information.genders.male")
    end

    it "should return 'Female'" do
      user.personal_information.update(sex: "Female")
      expect(user.sex_word).to eq I18n.t("activerecord.models.people/personal_information.genders.female")
    end

    it "should return 'Undefined'" do
      user.personal_information.update(sex: "m")
      expect(user.sex_word).to eq I18n.t("activerecord.models.people/personal_information.genders.undefined")

      user.personal_information.update(sex: "f")
      expect(user.sex_word).to eq I18n.t("activerecord.models.people/personal_information.genders.undefined")

      user.personal_information.update(sex: "")
      expect(user.sex_word).to eq I18n.t("activerecord.models.people/personal_information.genders.undefined")

      user.personal_information.update(sex: "Other")
      expect(user.sex_word).to eq I18n.t("activerecord.models.people/personal_information.genders.undefined")
    end

    context "when user has no personal_information" do
      it "should return 'Undefined'" do
        user.personal_information = nil
        user.save
        expect(user.sex_word).to eq I18n.t("activerecord.models.people/personal_information.genders.undefined")
      end
    end
  end

end
