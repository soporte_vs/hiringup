require 'rails_helper'

RSpec.describe SkillInterview::Stage, :type => :model do
  context "RelationShips" do
    it 'should has_one skill_interview_report' do
      should have_one(:skill_interview_report).dependent(:destroy)
    end
  end

  context 'Create' do
    it 'a valid skill_interview' do
      skill_interview_stage = FactoryGirl.create(:skill_interview_stage)
      expect(skill_interview_stage).to be_valid
      expect(skill_interview_stage).to be_a(SkillInterview::Stage)
    end
  end

  # describe 'Send Mail on init stage' do
  #   it {
  #     enrollment = FactoryGirl.create(:enrollment_basis_with_applicant)
  #     Delayed::Job.destroy_all
  #     expect {
  #       FactoryGirl.create(:skill_interview_stage, enrollment: enrollment)
  #     }.to change(Delayed::Job, :count).by(1)
  #     stage = SkillInterview::Stage.last
  #     skill_interview_mail = YAML.load(Delayed::Job.last.handler)
  #     expect(skill_interview_mail.object).to eq SkillInterview::StagesMailer
  #     expect(skill_interview_mail.method_name).to eq :send_email
  #     expect(skill_interview_mail.args.include? stage).to be true
  #   }
  # end
end
