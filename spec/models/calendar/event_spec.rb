# coding: utf-8
require 'rails_helper'

RSpec.describe Calendar::Event, type: :model do
  describe "Validations" do
    it { should validate_presence_of :starts_at }
    it { should validate_presence_of :address }
    it { should validate_presence_of :manager }

    context 'when ends_at is defined' do
      it 'does set event as valid if starts_at is before ends_at' do
        starts_at = Time.now
        event = FactoryGirl.build(:calendar_event, starts_at: starts_at, ends_at: starts_at + 1.minute)
        expect(event).to be_valid
      end

      it 'does set event as not valid if ends_at is before starts_at' do
        starts_at = Time.now
        event = FactoryGirl.build(:calendar_event, starts_at: starts_at, ends_at: starts_at - 1.minute)
        expect(event).not_to be_valid
        expect(event.errors[:starts_at]).not_to be_empty
      end
    end

    context 'when ends_at is not defined' do
      it 'does set event as valid and save it' do
        starts_at = Time.now
        event = FactoryGirl.build(:calendar_event, starts_at: starts_at)
        expect(event).to be_valid
        expect(event.save).to be true
      end
    end

  end

  describe "Relationships" do
    it { should have_many(:event_resourceables) }

    it { should have_many(:applicants)
                 .through(:event_resourceables)
                 .source(:resourceable)
                 .class_name('Applicant::Base')
    }

    it { should have_many(:stages)
                 .through(:event_resourceables)
                 .source(:resourceable)
                 .dependent(:destroy) }

    it { should have_many(:guests) }
    it { should have_one(:manager) }
  end

  describe "Create" do
    it "creates a valid event" do
      event = FactoryGirl.create(:calendar_event, observations: "Hola")
      expect(event.persisted?).to be true
      expect(event.id).not_to be nil
      expect(event.observations).to eq("Hola")
    end

    context "when it receives stages" do
      let(:course) { FactoryGirl.create(:course) }
      let(:enrollments) { FactoryGirl.create_list(:enrollment_basis_with_applicant, 3, course: course) }
      let(:stages) { enrollments.map(&:next_stage) }
      let(:guests) {
        [
          {name: FFaker::Name.name, email: FFaker::Internet.email},
          {name: FFaker::Name.name, email: FFaker::Internet.email},
          {name: FFaker::Name.name, email: FFaker::Internet.email}
        ]
      }

      it "creates an event and associates stages" do

        event = FactoryGirl.build(:calendar_event)
        event.assign_attributes(
          stages: stages,
          guests_attributes: guests
        )
        Delayed::Job.destroy_all

        expect(Delayed::Job.count).to eq 0

        expect(event).to be_valid
        expect{
          expect(event.save).to be true
        }.to change(Delayed::Job, :count).by(7)
        event.reload
        expect(event.stages.to_a.count).to eq 3
        expect(event.guests.to_a.count).to eq 3
        event.stages.each do |stage|
          expect(stage).to be_a Stage::Base
        end

        jobs = Delayed::Job.all.map{ |job| YAML.load(job.handler) }
        method_names = jobs.map(&:method_name)
        expect(method_names.count(:send_create_notification_to_manager)).to eq 1
        expect(method_names.count(:send_create_notification_to_guest)).to eq 3
        expect(method_names.count(:send_create_notification_to_stage)).to eq 3
      end
    end

    context "when it receives applicants" do
      let(:applicants) { FactoryGirl.create_list(:applicant_bases, 3) }
      let(:guests) {
        [
          {name: FFaker::Name.name, email: FFaker::Internet.email},
          {name: FFaker::Name.name, email: FFaker::Internet.email},
          {name: FFaker::Name.name, email: FFaker::Internet.email}
        ]
      }

      it "creates an event and associates applicants" do

        event = FactoryGirl.build(:calendar_event)
        event.assign_attributes(
          applicants: applicants,
          guests_attributes: guests
        )
        Delayed::Job.destroy_all

        expect(Delayed::Job.count).to eq 0

        expect(event).to be_valid
        expect{
          expect(event.save).to be true
        }.to change(Delayed::Job, :count).by(7)
        event.reload
        expect(event.applicants.to_a.count).to eq 3
        expect(event.guests.to_a.count).to eq 3
        event.applicants.each do |applicant|
          expect(applicant).to be_a Applicant::Base
        end

        jobs = Delayed::Job.all.map{ |job| YAML.load(job.handler) }
        method_names = jobs.map(&:method_name)
        expect(method_names.count(:send_create_notification_to_manager)).to eq 1
        expect(method_names.count(:send_create_notification_to_guest)).to eq 3
        expect(method_names.count(:send_create_notification_to_applicant)).to eq 3
      end
    end
  end

  describe "Update" do
    context "when updating an event with stages" do
      let(:course) { FactoryGirl.create(:course) }
      let(:enrollments) { FactoryGirl.create_list(:enrollment_basis_with_applicant, 3, course: course) }
      let(:stages) { enrollments.map(&:next_stage) }
      let(:guests_attributes) {
        [
          {name: FFaker::Name.name, email: FFaker::Internet.email},
          {name: FFaker::Name.name, email: FFaker::Internet.email},
          {name: FFaker::Name.name, email: FFaker::Internet.email}
        ]
      }

      before :each do
        Delayed::Job.destroy_all
        expect(Delayed::Job.count).to eq 0

        # create event
        event = FactoryGirl.build(:calendar_event)
        event.assign_attributes(
          stages: stages,
          guests_attributes: guests_attributes
        )
        event.save
        event.reload
        @event = event
      end

      it "updates a only event attributes" do
        new_attributes = FactoryGirl.attributes_for(:calendar_event)
        new_attributes[:manager_attributes] = {
          id: @event.manager.id,
          name: @event.manager.name,
          email: @event.manager.email,
        }
        guests = @event.guests
        new_attributes[:guests_attributes] = @event.guests.map{ |guest|
          {
            id: guest.id,
            name: guest.name,
            email: guest.email
          }
        }
        Delayed::Job.destroy_all
        @event = Calendar::Event.find(@event.id)
        @event.update(new_attributes)
        @event = Calendar::Event.find(@event.id)
        expect(@event.starts_at.to_s).to eq new_attributes[:starts_at].to_s
        expect(@event.ends_at.to_s).to eq new_attributes[:ends_at].to_s
        expect(@event.address).to eq new_attributes[:address]
        expect(@event.lat).to eq new_attributes[:lat]
        expect(@event.lng).to eq new_attributes[:lng]
        expect(@event.stages.count).to eq 3
        expect(@event.stages).to match_array(stages)
        expect(@event.guests.count).to eq 3
        expect(@event.guests).to match_array(guests)

        jobs = Delayed::Job.all.map{ |job| YAML.load(job.handler) }
        method_names = jobs.map(&:method_name)
        expect(method_names.count(:send_updates_notification_to_guest)).to eq 3
        expect(method_names.count(:send_updates_notification_to_stage)).to eq 3
        expect(method_names.count(:send_updates_notification_to_manager)).to eq 1
      end

      context "when updating only stages" do
        it "updates (removes and adds) same quantity of stages" do
          new_stages = enrollments.map(&:next_stage)

          expect(@event.stages).to match_array(stages)
          expect(@event.guests.count).to eq 3
          Delayed::Job.destroy_all
          @event = Calendar::Event.find(@event.id)
          expect{
            @event.update(stages: new_stages)
          }.to change(Delayed::Job, :count).by(3+3+3+1)
          @event = Calendar::Event.find(@event.id)
          expect(@event.stages.count).to eq 3
          expect(@event.stages).to match_array(new_stages)

          jobs = Delayed::Job.all.map{ |job| YAML.load(job.handler) }
          method_names = jobs.map(&:method_name)
          expect(method_names.count(:send_removed_notification_to_stage)).to eq 3
          expect(method_names.count(:send_create_notification_to_stage)).to eq 3
          expect(method_names.count(:send_updates_notification_to_guest_with_updated_stages)).to eq 3
          expect(method_names.count(:send_updates_notification_to_manager_with_updated_stages)).to eq 1
        end

        it "removes stages" do
          new_stages = stages.last(2)
          expect(@event.stages).to match_array(stages)
          Delayed::Job.destroy_all
          @event = Calendar::Event.find(@event.id)
          expect{
            @event.update(stages: new_stages)
          }.to change(Delayed::Job, :count).by(1+3+1)
          @event = Calendar::Event.find(@event.id)
          expect(@event.stages.count).to eq 2
          expect(@event.stages).to match_array(new_stages)

          method_names = Delayed::Job.all.map{ |job| YAML.load(job.handler) }.map(&:method_name)
          expect(method_names.count(:send_removed_notification_to_stage)).to eq 1
          expect(method_names.count(:send_updates_notification_to_guest_with_updated_stages)).to eq 3
          expect(method_names.count(:send_updates_notification_to_manager_with_updated_stages)).to eq 1
        end

        it "adds stages" do
          new_stages = stages + FactoryGirl.create_list(:stage_basis, 2)
          expect(@event.stages).to match_array(stages)
          Delayed::Job.destroy_all
          expect{
            @event.update(stages: new_stages)
          }.to change(Delayed::Job, :count).by(2+3+1)
          @event.reload
          expect(@event.stages.count).to eq 5
          expect(@event.stages).to match_array(new_stages)

          method_names = Delayed::Job.all.map{ |job| YAML.load(job.handler) }.map(&:method_name)
          expect(method_names.count(:send_create_notification_to_stage)).to eq 2
          expect(method_names.count(:send_updates_notification_to_guest_with_updated_stages)).to eq 3
          expect(method_names.count(:send_updates_notification_to_manager_with_updated_stages)).to eq 1
        end
      end

      context "when updates only guests" do
        it "changes all guests" do
          event_attributes = {}
          event_attributes[:guests_attributes] = [
            {name: FFaker::Name.name, email: FFaker::Internet.email},
            {name: FFaker::Name.name, email: FFaker::Internet.email},
            {name: FFaker::Name.name, email: FFaker::Internet.email}
          ]
          @event.guests.each do |guest|
            event_attributes[:guests_attributes] << {
              id: guest.id,
              name: guest.name,
              email: guest.email,
              _destroy: true
            }
          end

          Delayed::Job.destroy_all

          @event  = Calendar::Event.last
          expect{
            @event.update(event_attributes)
          }.to change(Delayed::Job, :count).by(3+3+1)
          @event  = Calendar::Event.last
          expect(@event.guests.count).to eq 3
          # expect(@event.guests).to match_array(new_guests)

          method_names = Delayed::Job.all.map{ |job| YAML.load(job.handler) }.map(&:method_name)
          expect(method_names.count(:send_removed_notification_to_guest)).to eq 3
          expect(method_names.count(:send_create_notification_to_guest)).to eq 3
          expect(method_names.count(:send_updates_notification_to_manager)).to eq 1
        end

        it "removes guests" do
          event_attributes = {}
          event_attributes[:guests_attributes] = [
            {
              id: @event.guests.first.id,
              name: @event.guests.first.name,
              email: @event.guests.first.email,
              _destroy: true
            }
          ]

          Delayed::Job.destroy_all

          @event = Calendar::Event.find(@event.id)
          expect{
            @event.update(event_attributes)
          }.to change(Delayed::Job, :count).by(1+2+1)
          @event = Calendar::Event.last
          expect(@event.guests.count).to eq 2

          method_names = Delayed::Job.all.map{ |job| YAML.load(job.handler) }.map(&:method_name)
          expect(method_names.count(:send_removed_notification_to_guest)).to eq 1
          expect(method_names.count(:send_updates_notification_to_guest)).to eq 2
          expect(method_names.count(:send_updates_notification_to_manager)).to eq 1
        end

        it "adds guests" do
          Delayed::Job.destroy_all
          @event = Calendar::Event.last
          event_attributes = {}
          event_attributes[:guests_attributes] = [
            {name: FFaker::Name.name, email: FFaker::Internet.email},
            {name: FFaker::Name.name, email: FFaker::Internet.email}
          ]
          expect{
            @event.update(event_attributes)
          }.to change(Delayed::Job, :count).by(2+3+1)
          @event.reload
          expect(@event.guests.count).to eq 5

          method_names = Delayed::Job.all.map{ |job| YAML.load(job.handler) }.map(&:method_name)
          expect(method_names.count(:send_create_notification_to_guest)).to eq 2
          expect(method_names.count(:send_updates_notification_to_guest)).to eq 3
          expect(method_names.count(:send_updates_notification_to_manager)).to eq 1
        end
      end

      context "when updates only manager" do
        it "changes manager" do
          manager_attributes = {id: "", name: "otro nombre", email: "otro@mail.com"}
          Delayed::Job.destroy_all
          @event = Calendar::Event.find(@event.id)
          expect(@event.stages.count).to eq 3
          expect(@event.guests.count).to eq 3
          expect{
            @event.update(
              manager_attributes: manager_attributes
            )
            @event.reload
            expect(@event.stages.count).to eq 3
            expect(@event.guests.count).to eq 3
          }.to change(Delayed::Job, :count).by(1+1+3+3)
          expect(@event.manager.name).to eq manager_attributes[:name]
          expect(@event.manager.email).to eq manager_attributes[:email]

          method_names = Delayed::Job.all.map{ |job| YAML.load(job.handler) }.map(&:method_name)
          expect(method_names.count(:send_removed_notification_to_manager)).to eq 1
          expect(method_names.count(:send_create_notification_to_manager)).to eq 1
          expect(method_names.count(:send_updates_notification_to_guest)).to eq 3
          expect(method_names.count(:send_updates_notification_to_stage)).to eq 3
        end

        it "changes manager if only manager email changed" do
          manager_attributes = {id: @event.manager.id, email: "otro_mas@mail.com"}
          Delayed::Job.destroy_all
          @event = Calendar::Event.find(@event.id)
          expect(@event.stages.count).to eq 3
          expect(@event.guests.count).to eq 3
          expect{
            @event.update(
              manager_attributes: manager_attributes
            )
            @event.reload
            expect(@event.stages.count).to eq 3
            expect(@event.guests.count).to eq 3
          }.to change(Delayed::Job, :count).by(1+1+3+3)
          expect(@event.manager.name).to eq manager_attributes[:name]
          expect(@event.manager.email).to eq manager_attributes[:email]

          method_names = Delayed::Job.all.map{ |job| YAML.load(job.handler) }.map(&:method_name)
          expect(method_names.count(:send_removed_notification_to_manager)).to eq 1
          expect(method_names.count(:send_create_notification_to_manager)).to eq 1
          expect(method_names.count(:send_updates_notification_to_guest)).to eq 3
          expect(method_names.count(:send_updates_notification_to_stage)).to eq 3
        end


        it "does not changes manager if only manager name changed" do
          old_manager = @event.manager
          manager_attributes = {
            id: old_manager.id,
            name: "Otro Nombre",
            email: old_manager.email
          }
          Delayed::Job.destroy_all
          @event = Calendar::Event.find(@event.id)
          expect(@event.stages.count).to eq 3
          expect(@event.guests.count).to eq 3
          expect{
            @event.update(
              manager_attributes: manager_attributes
            )
            @event.reload
            expect(@event.stages.count).to eq 3
            expect(@event.guests.count).to eq 3
          }.to change(Delayed::Job, :count).by(3+3)
          expect(@event.manager.id).to eq old_manager.id
          expect(@event.manager.name).to eq manager_attributes[:name]
          expect(@event.manager.email).to eq old_manager.email

          method_names = Delayed::Job.all.map{ |job| YAML.load(job.handler) }.map(&:method_name)
          expect(method_names.count(:send_updates_notification_to_guest)).to eq 3
          expect(method_names.count(:send_updates_notification_to_stage)).to eq 3
        end

      end

      context "when changes everything" do
        it "updates event and stages and guests and managers" do
          new_stages = @event.stages.last(2) + FactoryGirl.create_list(:stage_basis, 3)
          event_attributes = FactoryGirl.attributes_for(:calendar_event)
          event_attributes[:stages] = new_stages

          event_attributes[:guests_attributes] = [
            {name: FFaker::Name.name, email: FFaker::Internet.email},
            {name: FFaker::Name.name, email: FFaker::Internet.email},
            {name: FFaker::Name.name, email: FFaker::Internet.email},
            {id: @event.guests.first.id, _destroy: true},
            {id: @event.guests.second.id, email: "otro_correo@example.com"}
          ]

          @event = Calendar::Event.find(@event.id)
          Delayed::Job.destroy_all

          expect {
            @event.update(event_attributes)
          }.to change(Delayed::Job, :count).by(1+1+3+(3+1)+1+(1+1)+1+2)

          method_names = Delayed::Job.all.map{ |job| YAML.load(job.handler) }.map(&:method_name)
          expect(method_names.count(:send_removed_notification_to_stage)).to eq 1
          expect(method_names.count(:send_create_notification_to_manager)).to eq 1
          expect(method_names.count(:send_create_notification_to_stage)).to eq 3
          expect(method_names.count(:send_create_notification_to_guest)).to eq 3+1
          expect(method_names.count(:send_removed_notification_to_manager)).to eq 1
          expect(method_names.count(:send_removed_notification_to_guest)).to eq 1+1
          expect(method_names.count(:send_updates_notification_to_guest_with_updated_stages)).to eq 1
          expect(method_names.count(:send_updates_notification_to_stage)).to eq 2
        end
      end

      context "when changes everything but manager" do
        it "updates event and stages and guests and manager" do
          new_stages = @event.stages.last(2) + FactoryGirl.create_list(:stage_basis, 3)
          event_attributes = {}
          event_attributes[:starts_at] = Time.now + 1.week
          event_attributes[:address] = "Otra dirección 123"
          event_attributes[:stages] = new_stages

          event_attributes[:guests_attributes] = [
            {name: FFaker::Name.name, email: FFaker::Internet.email},
            {name: FFaker::Name.name, email: FFaker::Internet.email},
            {name: FFaker::Name.name, email: FFaker::Internet.email},
            {id: @event.guests.first.id, _destroy: true}
          ]

          # no cambia el manager
          event_attributes[:manager_attributes] = {
            id: @event.manager.id,
            name: @event.manager.name,
            email: @event.manager.email
          }

          old_manager = @event.manager

          @event = Calendar::Event.find(@event.id)
          Delayed::Job.destroy_all
          expect {
            @event.update(event_attributes)
          }.to change(Delayed::Job, :count).by(1+1+3+3+1+2+2)

          # no cambia el manager
          @event.reload
          expect(old_manager.id).to eq @event.manager.id
          expect(old_manager.name).to eq @event.manager.name
          expect(old_manager.email).to eq @event.manager.email

          method_names = Delayed::Job.all.map{ |job| YAML.load(job.handler) }.map(&:method_name)
          expect(method_names.count(:send_removed_notification_to_stage)).to eq 1
          expect(method_names.count(:send_removed_notification_to_guest)).to eq 1
          expect(method_names.count(:send_create_notification_to_stage)).to eq 3
          expect(method_names.count(:send_create_notification_to_guest)).to eq 3
          expect(method_names.count(:send_updates_notification_to_manager_with_updated_stages)).to eq 1
          expect(method_names.count(:send_updates_notification_to_guest_with_updated_stages)).to eq 2
          expect(method_names.count(:send_updates_notification_to_stage)).to eq 2

        end
      end

      context "when changes stages and guests" do
        it "updates stages and guests and manager" do
          new_stages = @event.stages.last(2) + FactoryGirl.create_list(:stage_basis, 3)
          # van 4 correos
          event_attributes = {}
          event_attributes[:stages] = new_stages

          event_attributes[:guests_attributes] = [
            {name: FFaker::Name.name, email: FFaker::Internet.email},
            {name: FFaker::Name.name, email: FFaker::Internet.email},
            {name: FFaker::Name.name, email: FFaker::Internet.email},
            {id: @event.guests.first.id, _destroy: true}
          ]
          # van 6 + 3 guests nuevos + 1 eliminado + 2 que quedaron

          # no cambia el manager
          event_attributes[:manager_attributes] = {
            id: @event.manager.id,
            name: @event.manager.name,
            email: @event.manager.email
          }

          old_manager = @event.manager

          Delayed::Job.destroy_all
          @event = Calendar::Event.find(@event.id)

          expect {
            @event.update(event_attributes)
          }.to change(Delayed::Job, :count).by(1+3+1+2+3+1)

          # no cambia el manager
          @event.reload
          expect(old_manager.id).to eq @event.manager.id
          expect(old_manager.name).to eq @event.manager.name
          expect(old_manager.email).to eq @event.manager.email

          method_names = Delayed::Job.all.map{ |job| YAML.load(job.handler) }.map(&:method_name)
          expect(method_names.count(:send_removed_notification_to_stage)).to eq 1
          expect(method_names.count(:send_create_notification_to_stage)).to eq 3
          expect(method_names.count(:send_updates_notification_to_manager_with_updated_stages)).to eq 1
          expect(method_names.count(:send_updates_notification_to_guest_with_updated_stages)).to eq 2
          expect(method_names.count(:send_create_notification_to_guest)).to eq 3
          expect(method_names.count(:send_removed_notification_to_guest)).to eq 1

        end
      end

      context "when changes guests email" do
        it "updates one guest email" do
          event_attributes = {}
          event_attributes[:guests_attributes] = [
            {id: @event.guests.first.id, email: "nuevo_email@email.com"}
          ]

          Delayed::Job.destroy_all
          @event = Calendar::Event.find(@event.id)

          expect {
            @event.update(event_attributes)
          }.to change(Delayed::Job, :count).by(1+2+1+1)

          method_names = Delayed::Job.all.map{ |job| YAML.load(job.handler) }.map(&:method_name)
          expect(method_names.count(:send_updates_notification_to_manager)).to eq 1
          expect(method_names.count(:send_updates_notification_to_guest)).to eq 2
          expect(method_names.count(:send_create_notification_to_guest)).to eq 1
          expect(method_names.count(:send_removed_notification_to_guest)).to eq 1

        end
      end
    end

    context "when updating an event with applicants" do
      let(:applicants) { FactoryGirl.create_list(:applicant_bases, 3) }
      let(:guests_attributes) {
        [
          {name: FFaker::Name.name, email: FFaker::Internet.email},
          {name: FFaker::Name.name, email: FFaker::Internet.email},
          {name: FFaker::Name.name, email: FFaker::Internet.email}
        ]
      }

      before :each do
        Delayed::Job.destroy_all
        expect(Delayed::Job.count).to eq 0

        # create event
        event = FactoryGirl.build(:calendar_event)
        event.assign_attributes(
          applicants: applicants,
          guests_attributes: guests_attributes
        )
        event.save
        event.reload
        @event = event
      end

      it "updates a only event attributes" do
        new_attributes = FactoryGirl.attributes_for(:calendar_event)
        new_attributes[:manager_attributes] = {
          id: @event.manager.id,
          name: @event.manager.name,
          email: @event.manager.email
        }
        guests = @event.guests
        new_attributes[:guests_attributes] = @event.guests.map{ |guest| {id: guest.id} }
        Delayed::Job.destroy_all
        @event = Calendar::Event.find(@event.id)
        @event.update(new_attributes)
        @event = Calendar::Event.find(@event.id)
        expect(@event.starts_at.to_s).to eq new_attributes[:starts_at].to_s
        expect(@event.ends_at.to_s).to eq new_attributes[:ends_at].to_s
        expect(@event.address).to eq new_attributes[:address]
        expect(@event.lat).to eq new_attributes[:lat]
        expect(@event.lng).to eq new_attributes[:lng]
        expect(@event.applicants.count).to eq 3
        expect(@event.applicants).to match_array(applicants)
        expect(@event.guests.count).to eq 3
        expect(@event.guests).to match_array(guests)

        jobs = Delayed::Job.all.map{ |job| YAML.load(job.handler) }
        method_names = jobs.map(&:method_name)
        expect(method_names.count(:send_updates_notification_to_guest)).to eq 3
        expect(method_names.count(:send_updates_notification_to_applicant)).to eq 3
        expect(method_names.count(:send_updates_notification_to_manager)).to eq 1
      end

      context "when updating only applicants" do
        it "updates (removes and adds) same quantity of applicants" do
          new_applicants = FactoryGirl.create_list(:applicant_bases, 3)

          expect(@event.applicants).to match_array(applicants)
          expect(@event.guests.count).to eq 3
          Delayed::Job.destroy_all
          @event = Calendar::Event.find(@event.id)
          expect{
            @event.update(applicants: new_applicants)
          }.to change(Delayed::Job, :count).by(3+3+3+1)
          @event = Calendar::Event.find(@event.id)
          expect(@event.applicants.count).to eq 3
          expect(@event.applicants).to match_array(new_applicants)

          jobs = Delayed::Job.all.map{ |job| YAML.load(job.handler) }
          method_names = jobs.map(&:method_name)
          expect(method_names.count(:send_removed_notification_to_applicant)).to eq 3
          expect(method_names.count(:send_create_notification_to_applicant)).to eq 3
          expect(method_names.count(:send_updates_notification_to_guest_with_updated_applicants)).to eq 3
          expect(method_names.count(:send_updates_notification_to_manager_with_updated_applicants)).to eq 1
        end

        it "removes applicants" do
          new_applicants = applicants.last(2)
          expect(@event.applicants).to match_array(applicants)
          Delayed::Job.destroy_all
          @event = Calendar::Event.find(@event.id)
          expect{
            @event.update(applicants: new_applicants)
          }.to change(Delayed::Job, :count).by(1+3+1)
          @event = Calendar::Event.find(@event.id)
          expect(@event.applicants.count).to eq 2
          expect(@event.applicants).to match_array(new_applicants)

          method_names = Delayed::Job.all.map{ |job| YAML.load(job.handler) }.map(&:method_name)
          expect(method_names.count(:send_removed_notification_to_applicant)).to eq 1
          expect(method_names.count(:send_updates_notification_to_guest_with_updated_applicants)).to eq 3
          expect(method_names.count(:send_updates_notification_to_manager_with_updated_applicants)).to eq 1
        end

        it "adds applicants" do
          new_applicants = applicants + FactoryGirl.create_list(:applicant_bases, 2)
          expect(@event.applicants).to match_array(applicants)
          Delayed::Job.destroy_all
          expect{
            @event.update(applicants: new_applicants)
          }.to change(Delayed::Job, :count).by(2+3+1)
          @event.reload
          expect(@event.applicants.count).to eq 5
          expect(@event.applicants).to match_array(new_applicants)

          method_names = Delayed::Job.all.map{ |job| YAML.load(job.handler) }.map(&:method_name)
          expect(method_names.count(:send_create_notification_to_applicant)).to eq 2
          expect(method_names.count(:send_updates_notification_to_guest_with_updated_applicants)).to eq 3
          expect(method_names.count(:send_updates_notification_to_manager_with_updated_applicants)).to eq 1
        end
      end

      context "when updates only guests" do
        it "changes all guests" do
          event_attributes = {}
          event_attributes[:guests_attributes] = [
            {name: FFaker::Name.name, email: FFaker::Internet.email},
            {name: FFaker::Name.name, email: FFaker::Internet.email},
            {name: FFaker::Name.name, email: FFaker::Internet.email}
          ]
          @event.guests.each do |guest|
            event_attributes[:guests_attributes] << {
              id: guest.id,
              _destroy: true
            }
          end

          Delayed::Job.destroy_all

          @event  = Calendar::Event.last
          expect{
            @event.update(event_attributes)
          }.to change(Delayed::Job, :count).by(3+3+1)
          @event  = Calendar::Event.last
          expect(@event.guests.count).to eq 3
          # expect(@event.guests).to match_array(new_guests)

          method_names = Delayed::Job.all.map{ |job| YAML.load(job.handler) }.map(&:method_name)
          expect(method_names.count(:send_removed_notification_to_guest)).to eq 3
          expect(method_names.count(:send_create_notification_to_guest)).to eq 3
          expect(method_names.count(:send_updates_notification_to_manager)).to eq 1
        end

        it "removes guests" do
          event_attributes = {}
          event_attributes[:guests_attributes] = [
            {
              id: @event.guests.first.id,
              _destroy: true
            }
          ]

          Delayed::Job.destroy_all

          @event = Calendar::Event.find(@event.id)
          expect{
            @event.update(event_attributes)
          }.to change(Delayed::Job, :count).by(1+2+1)
          @event = Calendar::Event.last
          expect(@event.guests.count).to eq 2

          method_names = Delayed::Job.all.map{ |job| YAML.load(job.handler) }.map(&:method_name)
          expect(method_names.count(:send_removed_notification_to_guest)).to eq 1
          expect(method_names.count(:send_updates_notification_to_guest)).to eq 2
          expect(method_names.count(:send_updates_notification_to_manager)).to eq 1
        end

        it "adds guests" do
          Delayed::Job.destroy_all
          @event = Calendar::Event.last
          event_attributes = {}
          event_attributes[:guests_attributes] = [
            {name: FFaker::Name.name, email: FFaker::Internet.email},
            {name: FFaker::Name.name, email: FFaker::Internet.email}
          ]
          expect{
            @event.update(event_attributes)
          }.to change(Delayed::Job, :count).by(2+3+1)
          @event.reload
          expect(@event.guests.count).to eq 5

          method_names = Delayed::Job.all.map{ |job| YAML.load(job.handler) }.map(&:method_name)
          expect(method_names.count(:send_create_notification_to_guest)).to eq 2
          expect(method_names.count(:send_updates_notification_to_guest)).to eq 3
          expect(method_names.count(:send_updates_notification_to_manager)).to eq 1
        end
      end

      context "when updates only manager" do
        it "changes manager" do
          manager_attributes = {name: "otro nombre", email: "otro@mail.com"}
          Delayed::Job.destroy_all
          @event = Calendar::Event.find(@event.id)
          expect(@event.applicants.count).to eq 3
          expect(@event.guests.count).to eq 3
          expect{
            @event.update(
              manager_attributes: manager_attributes
            )
            @event.reload
            expect(@event.applicants.count).to eq 3
            expect(@event.guests.count).to eq 3
          }.to change(Delayed::Job, :count).by(1+1+3+3)
          expect(@event.manager.name).to eq manager_attributes[:name]
          expect(@event.manager.email).to eq manager_attributes[:email]

          method_names = Delayed::Job.all.map{ |job| YAML.load(job.handler) }.map(&:method_name)
          expect(method_names.count(:send_removed_notification_to_manager)).to eq 1
          expect(method_names.count(:send_create_notification_to_manager)).to eq 1
          expect(method_names.count(:send_updates_notification_to_guest)).to eq 3
          expect(method_names.count(:send_updates_notification_to_applicant)).to eq 3
        end
      end

      context "when changes everything" do
        it "updates event and applicants and guests and managers" do
          new_applicants = @event.applicants.last(2) + FactoryGirl.create_list(:applicant_bases, 3)
          event_attributes = FactoryGirl.attributes_for(:calendar_event)
          event_attributes[:applicants] = new_applicants

          event_attributes[:guests_attributes] = [
            {name: FFaker::Name.name, email: FFaker::Internet.email},
            {name: FFaker::Name.name, email: FFaker::Internet.email},
            {name: FFaker::Name.name, email: FFaker::Internet.email},
            {id: @event.guests.first.id, _destroy: true}
          ]

          @event = Calendar::Event.find(@event.id)
          Delayed::Job.destroy_all

          expect {
            @event.update(event_attributes)
          }.to change(Delayed::Job, :count).by(1+1+3+3+1+1+2+2)

          method_names = Delayed::Job.all.map{ |job| YAML.load(job.handler) }.map(&:method_name)
          expect(method_names.count(:send_removed_notification_to_applicant)).to eq 1
          expect(method_names.count(:send_create_notification_to_manager)).to eq 1
          expect(method_names.count(:send_create_notification_to_applicant)).to eq 3
          expect(method_names.count(:send_create_notification_to_guest)).to eq 3
          expect(method_names.count(:send_removed_notification_to_manager)).to eq 1
          expect(method_names.count(:send_removed_notification_to_guest)).to eq 1
          expect(method_names.count(:send_updates_notification_to_guest_with_updated_applicants)).to eq 2
          expect(method_names.count(:send_updates_notification_to_applicant)).to eq 2

        end
      end

      context "when changes everything but manager" do
        it "updates event and applicants and guests and manager" do
          new_applicants = @event.applicants.last(2) + FactoryGirl.create_list(:applicant_bases, 3)
          event_attributes = {}
          event_attributes[:starts_at] = Time.now + 1.week
          event_attributes[:address] = "Otra dirección 123"
          event_attributes[:applicants] = new_applicants

          event_attributes[:guests_attributes] = [
            {name: FFaker::Name.name, email: FFaker::Internet.email},
            {name: FFaker::Name.name, email: FFaker::Internet.email},
            {name: FFaker::Name.name, email: FFaker::Internet.email},
            {id: @event.guests.first.id, _destroy: true}
          ]

          old_manager = @event.manager

          # no cambia el manager
          event_attributes[:manager_attributes] = {
            id: @event.manager.id,
            name: @event.manager.name,
            email: @event.manager.email
          }

          @event = Calendar::Event.find(@event.id)
          Delayed::Job.destroy_all

          expect {
            @event.update(event_attributes)
          }.to change(Delayed::Job, :count).by(1+1+3+3+1+2+2)

          # no cambia el manager
          @event.reload
          expect(old_manager.id).to eq @event.manager.id
          expect(old_manager.name).to eq @event.manager.name
          expect(old_manager.email).to eq @event.manager.email

          method_names = Delayed::Job.all.map{ |job| YAML.load(job.handler) }.map(&:method_name)
          expect(method_names.count(:send_removed_notification_to_applicant)).to eq 1
          expect(method_names.count(:send_removed_notification_to_guest)).to eq 1
          expect(method_names.count(:send_create_notification_to_applicant)).to eq 3
          expect(method_names.count(:send_create_notification_to_guest)).to eq 3
          expect(method_names.count(:send_updates_notification_to_manager_with_updated_applicants)).to eq 1
          expect(method_names.count(:send_updates_notification_to_guest_with_updated_applicants)).to eq 2
          expect(method_names.count(:send_updates_notification_to_applicant)).to eq 2

        end
      end

      context "when changes applicants and guests" do
        it "updates applicants and guests and manager" do
          new_applicants = @event.applicants.last(2) + FactoryGirl.create_list(:applicant_bases, 3)
          # van 4 correos
          event_attributes = {}
          event_attributes[:applicants] = new_applicants

          event_attributes[:guests_attributes] = [
            {name: FFaker::Name.name, email: FFaker::Internet.email},
            {name: FFaker::Name.name, email: FFaker::Internet.email},
            {name: FFaker::Name.name, email: FFaker::Internet.email},
            {id: @event.guests.first.id, _destroy: true}
          ]
          # van 6 + 3 guests nuevos + 1 eliminado + 2 que quedaron

          # no cambia el manager
          event_attributes[:manager_attributes] = {
            id: @event.manager.id,
            name: @event.manager.name,
            email: @event.manager.email
          }

          Delayed::Job.destroy_all
          @event = Calendar::Event.find(@event.id)

          expect {
            @event.update(event_attributes)
          }.to change(Delayed::Job, :count).by(1+3+1+2+3+1)

          method_names = Delayed::Job.all.map{ |job| YAML.load(job.handler) }.map(&:method_name)
          expect(method_names.count(:send_removed_notification_to_applicant)).to eq 1
          expect(method_names.count(:send_create_notification_to_applicant)).to eq 3
          expect(method_names.count(:send_updates_notification_to_manager_with_updated_applicants)).to eq 1
          expect(method_names.count(:send_updates_notification_to_guest_with_updated_applicants)).to eq 2
          expect(method_names.count(:send_create_notification_to_guest)).to eq 3
          expect(method_names.count(:send_removed_notification_to_guest)).to eq 1

        end
      end

      context "when changes guests email" do
        it "removes and adds the guest with the new email" do
          event_attributes = {}
          event_attributes[:guests_attributes] = [
            {id: @event.guests.first.id, email: "nuevo_email@email.com"}
          ]

          Delayed::Job.destroy_all
          @event = Calendar::Event.find(@event.id)

          expect {
            @event.update(event_attributes)
          }.to change(Delayed::Job, :count).by(1+2+1+1)

          method_names = Delayed::Job.all.map{ |job| YAML.load(job.handler) }.map(&:method_name)
          expect(method_names.count(:send_updates_notification_to_manager)).to eq 1
          expect(method_names.count(:send_updates_notification_to_guest)).to eq 2
          expect(method_names.count(:send_create_notification_to_guest)).to eq 1
          expect(method_names.count(:send_removed_notification_to_guest)).to eq 1

        end
      end
    end
  end

  describe "#as_json" do
    context "Resourceable is a Stage" do
      let(:course) { FactoryGirl.create(:course) }
      let(:enrollments) { FactoryGirl.create_list(:enrollment_basis_with_applicant, 3, course: course) }
      let(:stages) { enrollments.map(&:next_stage) }
      let(:guests) {
        [
          {name: FFaker::Name.name, email: FFaker::Internet.email},
          {name: FFaker::Name.name, email: FFaker::Internet.email},
          {name: FFaker::Name.name, email: FFaker::Internet.email}
        ]
      }
      let(:event){ FactoryGirl.create(:calendar_event)}

      it "should return a json with all parameters but empty stages" do
        event.assign_attributes(
          stages: stages,
          guests_attributes: guests
        )
        event.save
        event.reload

        expected_attributes = {
          id: event.id,
          manager_id: event.manager.id,
          manager_name: event.manager.name,
          manager_email: event.manager.email,
          starts_at: event.starts_at,
          ends_at: event.ends_at,
          address: { name: event.address, lat: event.lat, lng: event.lng },
          guests: event.guests.map{ |guest| { id: guest.id, name: guest.name, email: guest.email } },
          enrollment_ids: event.stages.map(&:enrollment_id),
          applicant_ids: []
        }

        expect(event.as_json).to eq expected_attributes.to_json
      end

    end

    context "Resourceable is an Applicant" do
      let(:course) { FactoryGirl.create(:course) }
      let(:enrollments) { FactoryGirl.create_list(:enrollment_basis_with_applicant, 3, course: course) }
      let(:applicants) { enrollments.map(&:applicant) }
      let(:guests) {
        [
          {name: FFaker::Name.name, email: FFaker::Internet.email},
          {name: FFaker::Name.name, email: FFaker::Internet.email},
          {name: FFaker::Name.name, email: FFaker::Internet.email}
        ]
      }
      let(:event){ FactoryGirl.create(:calendar_event)}

      it "should return a json with all parameters but empty stages" do
        event.assign_attributes(
          applicants: applicants,
          guests_attributes: guests
        )
        event.save
        event.reload

        expected_attributes = {
          id: event.id,
          manager_id: event.manager.id,
          manager_name: event.manager.name,
          manager_email: event.manager.email,
          starts_at: event.starts_at,
          ends_at: event.ends_at,
          address: { name: event.address, lat: event.lat, lng: event.lng },
          guests: event.guests.map{ |guest| { id: guest.id, name: guest.name, email: guest.email } },
          enrollment_ids: [],
          applicant_ids: event.applicant_ids
        }

        expect(event.as_json).to eq expected_attributes.to_json
      end
    end
  end
end
