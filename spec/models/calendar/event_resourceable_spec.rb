require 'rails_helper'

RSpec.describe Calendar::EventResourceable, type: :model do
  describe "Validations" do
    it { should validate_presence_of :event }
  end

  describe "Relationships" do
    it { should belong_to(:resourceable) }
    it { should belong_to(:event) }
  end

  describe "Create" do
    it 'does create a valid resourceable event' do
      event_resourceable = FactoryGirl.build(:calendar_event_resourceable)
      expect(event_resourceable).to be_valid
      expect(event_resourceable.save).to be true
    end
  end

  describe "Destroy" do
    context "when time now is after event.starts_at" do
      it "should not send email to removed enrollments" do
        today = Time.now
        yesterday = today - 1.day
        course = FactoryGirl.create(:course)
        enrollments = FactoryGirl.create_list(:enrollment_basis_with_applicant, 2, course: course)
        stages = enrollments.map(&:next_stage)
        event = FactoryGirl.create(:calendar_event, starts_at: yesterday, stages: stages)
        expect(event.stages).to eq stages
        expect{
          event.stages.destroy_all
        }.to change(Delayed::Job, :count).by(0)
      end
    end

    context "when time now is before event.starts_at" do
      it "should not send email to removed enrollments" do
        today = Time.now
        tomorrow = today + 1.day
        course = FactoryGirl.create(:course)
        enrollments = FactoryGirl.create_list(:enrollment_basis_with_applicant, 2, course: course)
        stages = enrollments.map(&:next_stage)
        event = FactoryGirl.create(:calendar_event, starts_at: tomorrow, stages: stages)
        expect(event.stages).to eq stages
        expect{
          event.stages.destroy_all
        }.to change(Delayed::Job, :count).by(2)
      end
    end
  end
end
