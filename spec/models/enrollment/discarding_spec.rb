# == Schema Information
#
# Table name: enrollment_discardings
#
#  id                :integer          not null, primary key
#  enrollment_id     :integer
#  discard_reason_id :integer
#  observations      :text
#  created_at        :datetime
#  updated_at        :datetime
#

require 'rails_helper'

RSpec.describe Enrollment::Discarding, :type => :model do
  context "Validation model" do
    it "should has a validation of enrollment and  discard_reason" do
      should validate_presence_of(:enrollment)
      should validate_presence_of(:discard_reason)
    end
  end

  it "should has validation uniqueness enrollment" do
    should validate_uniqueness_of(:enrollment_id)
  end

  describe '#create' do
    let!(:enrollment_discarding) { FactoryGirl.create(:enrollment_discarding) }
    let(:enrollment) { enrollment_discarding.enrollment }

    context 'when enrollment has already associate a discarding' do
      it 'should not create a new Enrollment:Discarding' do
        expect{
          FactoryGirl.create(:enrollment_discarding, enrollment: enrollment)
        }.to change(Enrollment::Discarding, :count).by(0).and \
            raise_error ActiveRecord::RecordInvalid, 'La validación falló: Enrollment ya está en uso'
      end
    end
  end

  describe "Relationships" do
    it { should belong_to(:discard_reason)
                  .class_name('Enrollment::DiscardReason')
                  .with_foreign_key(:discard_reason_id) }
  end

  describe "Set blacklisted" do
    context 'when discard reason has a blacklist discard reason trigger' do
      it 'should set enrollment applicant as blacklisted' do
        discard_reason = FactoryGirl.create(:enrollment_discard_reason)
        blacklist_discard_reason_trigger = FactoryGirl.create(:trigger_blacklist_discard_reason, resourceable: discard_reason)
        discarding = FactoryGirl.create(:enrollment_discarding, discard_reason: discard_reason)
        applicant = discarding.enrollment.applicant
        expect(applicant.blacklisted).to eq true
      end
    end

    context 'when discard reason has not a blacklist discard reason trigger' do
      it 'should set enrollment applicant as blacklisted' do
        discard_reason = FactoryGirl.create(:enrollment_discard_reason)
        discarding = FactoryGirl.create(:enrollment_discarding, discard_reason: discard_reason)
        applicant = discarding.enrollment.applicant
        expect(applicant.blacklisted).not_to eq true
      end
    end
  end

  describe 'Touch enrollment when discarding is created' do
    it 'should change updated_at on enrollment' do
      enrollment = FactoryGirl.create(:enrollment_basis_with_applicant)
      discarding = FactoryGirl.create(:enrollment_discarding, enrollment: enrollment)
      expect(enrollment.updated_at).not_to eq enrollment.created_at
    end
  end
end
