# coding: utf-8
# == Schema Information
#
# Table name: enrollment_bases
#
#  id           :integer          not null, primary key
#  type         :string
#  course_id    :integer
#  created_at   :datetime
#  updated_at   :datetime
#  applicant_id :integer
#  stage_id     :integer
#

require 'rails_helper'

RSpec.describe Enrollment::Base, :type => :model do
  context "Validations Enrollment::Base" do
    it "should validate course is present" do
      should validate_presence_of(:course)
    end

    it "should validate applicant is present" do
      should validate_presence_of(:applicant)
    end

    it "should validate that applicant is uniqueness for course" do
      should validate_uniqueness_of(:applicant_id).scoped_to(:course_id)
    end

    it "should validate belongs to course and applicant" do
      should belong_to(:course)
      should belong_to(:applicant)
      should belong_to(:stage)
    end

    it "should validate has_many stages" do
      should have_many(:stages).dependent(:destroy)
    end

    it "should has_one relation with discarding" do
      should have_one(:discarding).dependent(:destroy)
    end

    it { should have_one(:course_invitation).dependent(:destroy) }
  end

  context "Create Enrollment::Base valid" do
    it "it should be valid" do
      enrollment = FactoryGirl.create(:enrollment_basis_with_applicant)
      expect(enrollment).to be_valid
    end
  end

  context 'Enrollment is disposable' do
    it 'when enrollment has not inited work flow' do
      enrollment = FactoryGirl.create(:enrollment_basis_with_applicant)
      expect(enrollment.disposable?).to be true
    end

    it 'when enrollment inited work flow and current stage allow discard' do
      enrollment = FactoryGirl.create(:enrollment_basis_with_applicant)
      enrollment.next_stage
      allow_any_instance_of(enrollment.stage.class).to receive(:disposable?).and_return(true)
      enrollment.next_stage
      allow_any_instance_of(enrollment.stage.class).to receive(:disposable?).and_return(true)
      expect(enrollment.disposable?).to be true
    end

    it 'when enrollment inited work flow and current stage not allow discard' do
      enrollment = FactoryGirl.create(:enrollment_basis_with_applicant)
      enrollment.next_stage
      allow_any_instance_of(enrollment.stage.class).to receive(:disposable?).and_return(true)
      enrollment.next_stage
      allow_any_instance_of(enrollment.stage.class).to receive(:disposable?).and_return(false)
      expect(enrollment.disposable?).to be false
    end

    it 'when enrollment was discarted' do
      discard_reason = FactoryGirl.create(:enrollment_discard_reason)
      enrollment = FactoryGirl.create(:enrollment_basis_with_applicant)
      enrollment.discard(FFaker::Lorem.sentence, discard_reason)
      expect(enrollment.disposable?).to be false
      enrollment.reload
      expect(enrollment.errors[:discarding].any?).to be true
    end
  end

  describe 'Discard enrollment' do
    let(:discard_reason) { FactoryGirl.create(:enrollment_discard_reason) }

    context 'when it has a stage that can\'t let dispose enrollment' do
      let(:agreement) { FactoryGirl.create(:engagement_agreement) }
      let(:enrollment) { agreement.engagement_stage.enrollment }

      it 'should not create a new discarding, add errors on enrollment discarding and return false ' do
        expect{
          enrollment.discard(FFaker::Lorem.sentence, discard_reason)
        }.to change(Enrollment::Discarding, :count).by(0)
        expect(enrollment.errors[:discarding].any?).to be true
      end
    end
  end

  describe '#postulation' do
    let(:postulation) { enrollment.postulation }
    let(:enrollment) { FactoryGirl.create(:enrollment_basis, applicant: expected_postulation.applicant) }
    let(:expected_postulation) { nil }

    context 'has a postulation' do
      context 'it is a minisite_postulation' do
        let(:expected_postulation) { FactoryGirl.create(:minisite_postulation) }
        it 'returns a minisite_postulation' do end
      end
      context 'it is a laborum_postulation' do
        before :each do
          allow_any_instance_of(Laborum::Postulation).to receive(:set_applicant_and_info).and_return(nil)
        end
        let(:expected_postulation) { FactoryGirl.create(:laborum_postulation) }
        it 'returns a laborum_postulation' do end
      end
      context 'it is a trabajando_postulation' do
        let(:expected_postulation) { FactoryGirl.create(:trabajando_postulation) }
        it 'returns a trabajando_postulation' do end
      end
    end

    context 'has no postulation' do
      let(:enrollment) { FactoryGirl.create(:enrollment_basis_with_applicant) }
      it 'returns nil' do end
    end

    after :each do
      expect(postulation).to eq(expected_postulation)
    end
  end

  describe 'Postulation origin applicant' do

    context 'postulated from minisite' do
      it 'returns a minisite postulation' do
        minisite_postulation = FactoryGirl.create(:minisite_postulation)
        course = minisite_postulation.minisite_publication.course

        course.enrollments.each do |enrollment|
          expect(enrollment.postulation_origin).to be_a Minisite::Postulation
        end
      end
    end

    context 'postulated from laborum' do
      before :each do
        allow_any_instance_of(Laborum::Postulation).to receive(:set_applicant_and_info).and_return(nil)
      end

      it 'returns a laborum postulation' do
        laborum_postulation = FactoryGirl.create(:laborum_postulation)
        course = laborum_postulation.laborum_publication.course

        course.enrollments.each do |enrollment|
          expect(enrollment.postulation_origin).to be_a Laborum::Postulation
        end
      end
    end

    context 'doesnt have postulations' do
      it 'returns a course' do
        enrollment = FactoryGirl.create(:enrollment_basis_with_applicant)
        expect(enrollment.postulation_origin).to be_a Course
      end
    end
  end

  describe 'Revert discard' do
    let(:enrollment) { FactoryGirl.create(:enrollment_basis_with_applicant) }
    let(:discard_reason) { FactoryGirl.create(:enrollment_discard_reason) }

    context 'discarding is present' do
      before :each do
        FactoryGirl.create(:trigger_blacklist_discard_reason, resourceable: discard_reason)
        enrollment_discarding = FactoryGirl.create(:enrollment_discarding, enrollment: enrollment, discard_reason: discard_reason)
      end

      it 'should destroy the discarding and send email' do
        expect{
          enrollment.revert_discard
        }.to change(Enrollment::Discarding, :count).by(-1).and \
             change(Delayed::Job, :count).by(1)

        email = YAML.load(Delayed::Job.last.handler)
        expect(email.object).to eq Enrollment::DiscardingMailer
        expect(email.method_name).to eq :revert_discard
        expect(email.args.include? enrollment).to be true
      end

      context 'when applicant has not been discarted in another course with the same discard_reason related with a Trigger::BlacklistDiscardReason' do

        it 'should destroy the discarding and send email and take the applicant blacklist' do
          expect(enrollment.applicant.blacklisted).to eq true
          expect{
            enrollment.revert_discard
          }.to change(Enrollment::Discarding, :count).by(-1).and \
               change(Delayed::Job, :count).by(1)

          email = YAML.load(Delayed::Job.last.handler)
          expect(email.object).to eq Enrollment::DiscardingMailer
          expect(email.method_name).to eq :revert_discard
          expect(email.args.include? enrollment).to be true
          expect(enrollment.applicant.blacklisted).to eq false
        end
      end

      context 'when applicant has been discarted in another course with the another discard_reason related with a Trigger::BlacklistReason' do
        before :each do
          other_discard_reason = FactoryGirl.create(:enrollment_discard_reason)

          FactoryGirl.create(:trigger_blacklist_discard_reason, resourceable: other_discard_reason)
          #another course is created to ensure that the applicant associated with the enrollment is in another course associated with the trigger discard_reason Additional Factroy of enrollmen not create the course automatically
          other_course = FactoryGirl.create(:course)
          other_enrollment = FactoryGirl.create(:enrollment_basis_with_applicant, course: other_course, applicant: enrollment.applicant)
          FactoryGirl.create(:enrollment_discarding, enrollment: other_enrollment, discard_reason: other_discard_reason)
        end

        it 'should destroy the discarding and send email and let the applicant blacklist' do
          expect(enrollment.applicant.blacklisted).to eq true
          expect{
            enrollment.revert_discard
          }.to change(Enrollment::Discarding, :count).by(-1).and \
               change(Delayed::Job, :count).by(1)

          email = YAML.load(Delayed::Job.last.handler)
          expect(email.object).to eq Enrollment::DiscardingMailer
          expect(email.method_name).to eq :revert_discard
          expect(email.args.include? enrollment).to be true
          expect(enrollment.applicant.blacklisted).to eq true
        end
      end

      context 'when the applicant has been ruled in another course with the same discard_reason related to a trigger :: BlacklistReason' do
        before :each do
          #another course is created to ensure that the applicant associated with the enrollment is in another course associated with the trigger discard_reason, additional, FactoryGirl.create(:enrollment_basis_with_applicant) not create the course automatically
          other_course = FactoryGirl.create(:course)
          other_enrollment = FactoryGirl.create(:enrollment_basis_with_applicant, course: other_course, applicant: enrollment.applicant)
          FactoryGirl.create(:enrollment_discarding, enrollment: other_enrollment, discard_reason: discard_reason)
        end

        it 'should destroy the discarding and send email and let the applicant blacklist' do
          expect(enrollment.applicant.blacklisted).to eq true
          expect{
            enrollment.revert_discard
          }.to change(Enrollment::Discarding, :count).by(-1).and \
               change(Delayed::Job, :count).by(1)

          email = YAML.load(Delayed::Job.last.handler)
          expect(email.object).to eq Enrollment::DiscardingMailer
          expect(email.method_name).to eq :revert_discard
          expect(email.args.include? enrollment).to be true
          expect(enrollment.applicant.blacklisted).to eq true
        end
      end

    end

    context 'discarding is not present' do
      it 'should return false' do
        enrollment.reload
        expect{
          enrollment.revert_discard
        }.to change(Enrollment::Discarding, :count).by(0).and \
             change(Delayed::Job, :count).by(0)
      end
    end
  end

  describe 'Next stage' do
    let(:work_flow) {
      [
        {type: :module, class_name: 'Offer::Stage', name: 'Oferta'},
        {type: :module, class_name: 'Onboarding::Stage', name: 'Documentación'},
        {type: :module, class_name: 'Engagement::Stage', name: 'Ingreso'}
      ]
    }
    let(:course){ FactoryGirl.create(:course, work_flow: work_flow) }
    let(:enrollment){ FactoryGirl.create(:enrollment_basis_with_applicant, course: course) }

    context 'when it is the first stage' do
      it 'should create a new stage for enrollment ant set it as current and its index as 0' do
        expect{
          enrollment.next_stage
        }.to change(Offer::Stage, :count).by(1)
        expect(enrollment.stage).not_to be nil
        expect(enrollment.current_stage_index).to eq 0
      end
    end

    context 'when it is a middle stage' do
      before :each do
        enrollment.next_stage
      end

      it 'should set it as current and set a middle index' do
        expect{
          enrollment.next_stage
        }.to change(Onboarding::Stage, :count).by(1)
        expect(enrollment.stage).not_to be nil
        expect(enrollment.current_stage_index).to eq 1
      end
    end


    context 'when there is not a next stage' do
      before :each do
        3.times do
          enrollment.next_stage
        end
      end

      it 'should set it as current and set a middle index' do
        expect{
          enrollment.next_stage
        }.to change(Stage::Base, :count).by(0)
        expect(enrollment.stage).to be nil
        expect(enrollment.current_stage_index).to eq 2
        expect(enrollment.errors[:stage].any?).to be false
      end
    end

    context 'when enrollment already finish work_flow' do
      before :each do
        4.times do
          enrollment.next_stage
        end
      end

      it 'should not do anything' do
        expect{
          expect(enrollment.next_stage).to be nil
        }.to change(Stage::Base, :count).by(0)
        expect(enrollment.stage).to eq nil
        expect(enrollment.current_stage_index).to eq 2
        expect(enrollment.errors[:stage].any?).to be true
      end
    end


    context 'when course is closed' do
      let(:course){ FactoryGirl.create(:course) }
      let(:enrollment){ FactoryGirl.create(:enrollment_basis_with_applicant, course: course) }

      before :each do
        course.close!
      end

      it 'should not change enrollments stage, and no create a new stage for enrollment' do
        current_stage = enrollment.stage
        expect(course.closed?).to be true
        expect{enrollment.next_stage}.to change(Stage::Base, :count).by(0)
        expect(enrollment.stage).to eq current_stage
      end
    end

    context 'when enrollment is disposed' do
      before :each do
        FactoryGirl.create(:enrollment_discarding, enrollment: enrollment)
        enrollment.reload
      end

      it 'should return nil and not change current stage' do
        current_stage = enrollment.stage
        current_stage_index = enrollment.current_stage_index
        expect{
          expect(enrollment.next_stage).to be nil
        }.to change(Stage::Base, :count).by(0)
        expect(enrollment.stage).to eq current_stage
        expect(enrollment.current_stage_index).to eq current_stage_index
      end
    end
  end

  describe "#self.next_stage" do
    let(:user) { FactoryGirl.create(:user) }

    context "when enrollments are dynamic" do
      let(:stage_type) { Interview::Stage }
      let(:name_stage) { FFaker::Lorem.word }
      let(:enrollments) {
        Enrollment::Base.where(id:
          FactoryGirl.create_list(:enrollment_dynamic, 3).map(&:id))
      }
      let(:args) { nil }
      let(:public_activity_changes_expected) {
        change(PublicActivity::Activity, :count).by(0)
      }
      let(:changes_expected_by_enrollment){
        enrollments.each do |enrollment|
          expect(enrollment.stage.class).to eq stage_type
          expect(enrollment.stages.first.is_approved).to eq nil
        end
      }
      let(:delayed_job_changes_expected) {
        change(Delayed::Job, :count).by(0)
      }
      let(:expected_emails_sent) {  }

      context 'when current_user is present' do
        let(:args) { { current_user: user } }
        let(:public_activity_changes_expected) {
          change(PublicActivity::Activity, :count).by(enrollments.count)
        }

        it "should change stage and register activities" do
        end
      end

      context 'when current_user is nil' do
        let(:args) { {} }

        it "should change stage and not register public activities " do
        end
      end

      context "and args indicate that enrollments are approved in current_stage" do
        let(:enrollments_hash){
          enrollments_target = {}
          enrollments.each_with_index do |enrollment, index|
            enrollments_target[index] = { enrollment_id: enrollment.id, is_approved: true }
          end
          enrollments_target
        }
        let(:args){ { enrollments_status_stage: enrollments_hash } }
        let(:changes_expected_by_enrollment){
          enrollments.each do |enrollment|
            expect(enrollment.stage.class).to eq stage_type
            expect(enrollment.stages.first.is_approved).to eq  true
          end
        }

        before :each do
          enrollments.each do |enrollment|
            FactoryGirl.create(:interview_stage, enrollment: enrollment, name: FFaker::Lorem.word)
          end
        end

        it "should response enrollments approved" do
        end


        context "but current stage already was reviewed" do
          before :each do
            enrollments.each{|enrollment| enrollment.stage.update(is_approved: false)}
          end

          let(:changes_expected_by_enrollment){
            enrollments.each do |enrollment|
              expect(enrollment.stage.class).to eq stage_type
              expect(enrollment.stages.first.is_approved).to eq  false
            end
          }

          it "should current stage is_approved attribute keep the same value" do
          end
        end
      end

      before :each do
        # Se limpia contexto
        enrollments
        Delayed::Job.destroy_all
      end

      after :each do
        expect{
          enrollments.next_stage(stage_type, stage_type.model_name.human, args)
        }.to change(stage_type, :count).by(enrollments.count).and \
            public_activity_changes_expected.and \
            delayed_job_changes_expected

        changes_expected_by_enrollment
        expected_emails_sent
      end
    end

    context "when enrollments are bases" do
      let(:course) { FactoryGirl.create(:course) }
      let(:enrollments) {
        Enrollment::Base.where(id:
          FactoryGirl.create_list(:enrollment_basis_with_applicant, 3, course: course).map(&:id))
      }

      it "should change next_stage and register public activities" do
        args = { current_user: user }
        expect{
          enrollments.next_stage("", "", args)
        }.to change(Stage::Base, :count).by(enrollments.count)
             change(PublicActivity::Activity, :count).by(enrollments.count)
        enrollments.each do |enrollment|
          expect(enrollment.stage).not_to be nil
          expect(enrollment.current_stage_index).to eq 0
        end
      end
    end
  end

  describe "invited?" do
    it "should return true if course invitation is present" do
      enrollment = FactoryGirl.create(:enrollment_basis_with_applicant)
      invitation = FactoryGirl.create(:enrollment_course_invitation, enrollment: enrollment)
      enrollment.reload
      expect(enrollment.invited?).to be true
    end

    it "should return false if course invitation is not present" do
      enrollment = FactoryGirl.create(:enrollment_basis_with_applicant)
      expect(enrollment.invited?).to be false
    end
  end

  describe "in_screening?" do
    it "Should return true if stage is nil and enrollment is invitable" do
      enrollment = FactoryGirl.create(:enrollment_basis_with_applicant)
      expect(enrollment.stage).to be nil
      expect(enrollment.invited?).to be false
      expect(enrollment.invitable?).to be true
      expect(enrollment.in_screening?).to be true
    end

    it "Should return false if stage is not nil and enrollment is not invitable" do
      enrollment = FactoryGirl.create(:enrollment_basis_with_applicant)
      enrollment.invite
      enrollment.next_stage
      expect(enrollment.stage).not_to be nil
      expect(enrollment.invited?).to be true
      expect(enrollment.invitable?).to be false
      expect(enrollment.in_screening?).to be false
    end
  end

  describe "in_progress" do
    it "Should return true if already exists a stage" do
      enrollment = FactoryGirl.create(:enrollment_basis_with_applicant)
      expect(enrollment.stages.any?).to be false
      enrollment.next_stage
      expect(enrollment.stages.count).to eq(1)
      expect(enrollment.stage.present?).to be true
      expect(enrollment.invitable?).to be false
      expect(enrollment.in_progress?).to be true
    end

    it "Should return true if dont exists a stage" do
      enrollment = FactoryGirl.create(:enrollment_basis_with_applicant)
      expect(enrollment.stages.any?).to be false
      expect(enrollment.stages.count).to eq(0)
      enrollment.invite
      expect(enrollment.invited?).to be true
      expect(enrollment.stage.present?).to be false
      expect(enrollment.invitable?).to be true
      expect(enrollment.in_progress?).to be true
    end
  end

  describe "invite" do
    it "should create new Enrollment::CourseInvitation" do
      enrollment = FactoryGirl.create(:enrollment_basis_with_applicant)
      enrollment.invite
      expect(enrollment.invited?).to be true
      invitation = enrollment.course_invitation
      expect(invitation.accepted).to be nil
    end

    it "should not create a new Enrollment::CourseInvitation if already exists one" do
      invitation = FactoryGirl.create(:enrollment_course_invitation)
      enrollment = invitation.enrollment
      expect(enrollment.invite).to be false
      expect(enrollment.invited?).to be true
      expect(enrollment.errors).not_to be_empty
      expect(enrollment.errors[:course_invitation]).to include I18n.t("activerecord.errors.models.enrollment/base.attributes.course_invitation.already_invited")
    end

    it "should not create a new Enrollment::CourseInvitation if enrollment is disposed" do
      enrollment = FactoryGirl.create(:enrollment_basis_with_applicant)
      discard_reason = FactoryGirl.create(:enrollment_discard_reason)
      enrollment.discard(FFaker::Lorem.sentence, discard_reason)
      expect(enrollment.disposable?).to be false

      expect(enrollment.invite).to be false
      expect(enrollment.invited?).to be false
      expect(enrollment.errors).not_to be_empty
      expect(enrollment.errors[:course_invitation]).to include I18n.t("activerecord.errors.models.enrollment/base.attributes.course_invitation.disposed")
    end

    it "should not create a new Enrollment::CourseInvitation if enrollment is not invitable" do
      enrollment = FactoryGirl.create(:enrollment_basis_with_applicant)
      enrollment.next_stage
      expect(enrollment.invitable?).to be false

      expect(enrollment.invite).to be false
      expect(enrollment.invited?).to be false
      expect(enrollment.errors).not_to be_empty
      expect(enrollment.errors[:course_invitation]).to include I18n.t("activerecord.errors.models.enrollment/base.attributes.course_invitation.not_invitable")
    end
  end

  describe "invitable?" do
    it "should return true if exists a stage" do
      enrollment = FactoryGirl.create(:enrollment_basis_with_applicant)
      expect(enrollment.discarding.present?).to be false
      expect(enrollment.course.present?).to be true
      expect(enrollment.course.aasm_state).to eq ("opened")
      expect(enrollment.stage.present?).to be false
      expect(enrollment.invitable?).to be true
    end
  end

  describe "mark_invitation_accepted" do
    it "should mark invitation accepted=true" do
      enrollment = FactoryGirl.create(:enrollment_basis_with_applicant)
      enrollment.invite
      enrollment.mark_invitation_accepted
      expect(enrollment.invited?).to be true
      expect(enrollment.course_invitation.accepted).to be true
    end

    it "should return false with error if invitation doesn't exist" do
      enrollment = FactoryGirl.create(:enrollment_basis_with_applicant)
      expect(enrollment.mark_invitation_accepted).to be false
      expect(enrollment.invited?).to be false
      expect(enrollment.errors.any?).to be true
    end
  end

  describe "mark_invitation_rejected" do
    it "should mark invitation accepted=false" do
      enrollment = FactoryGirl.create(:enrollment_basis_with_applicant)
      enrollment.invite
      enrollment.mark_invitation_rejected
      expect(enrollment.invited?).to be true
      expect(enrollment.course_invitation.accepted).to be false
    end

    it "should return false with error if invitation doesn't exist" do
      enrollment = FactoryGirl.create(:enrollment_basis_with_applicant)
      expect(enrollment.mark_invitation_rejected).to be false
      expect(enrollment.invited?).to be false
      expect(enrollment.errors.any?).to be true
    end
  end

  describe "invitation_accepted? && invitation_rejected?" do
    let(:course_invitation) { FactoryGirl.create(:enrollment_course_invitation) }
    let(:enrollment) { course_invitation.enrollment }

    context 'when mark course invitation as accepted' do
      it "should return true if accepted" do
        enrollment.mark_invitation_accepted
        expect(enrollment.invitation_accepted?).to be true
        expect(enrollment.invitation_rejected?).to be false
      end

      it "should return false if not answered" do
        expect(enrollment.invitation_accepted?).to be false
      end

      it "should return false if not invited" do
        enrollment = FactoryGirl.create(:enrollment_basis_with_applicant)
        expect(enrollment.invitation_accepted?).to be false
      end

      it "should return false if rejected" do
        enrollment.mark_invitation_rejected
        expect(enrollment.invitation_accepted?).to be false
      end
    end

    context 'when mark course invitation as rejected' do
      it "should return true if rejeceted" do
        enrollment.mark_invitation_rejected
        expect(enrollment.invitation_accepted?).to be false
        expect(enrollment.invitation_rejected?).to be true
      end

      it "should return false if not answered" do
        expect(enrollment.invitation_rejected?).to be false
      end

      it "should return false if not invited" do
        enrollment = FactoryGirl.create(:enrollment_basis_with_applicant)
        expect(enrollment.invitation_rejected?).to be false
      end

      it "should return false if accepted" do
        enrollment.mark_invitation_accepted
        expect(enrollment.invitation_rejected?).to be false
      end
    end
  end

  describe 'stage_text method' do
    context 'when work flow not initialized' do
      it 'should return "Asignado"' do
        enrollment = FactoryGirl.create(:enrollment_basis_with_applicant)
        expect(enrollment.stage_text).to eq I18n.t('enrollment/base.assigned')
      end

      context 'and enrollment was discarted' do
        it "should return #{I18n.t('enrollment/base.assigned')} (#{I18n.t('enrollment/base.discarted')})" do
          enrollment = FactoryGirl.create(:enrollment_basis_with_applicant)
          enrollment.discard(FFaker::Lorem.word, FactoryGirl.create(:enrollment_discard_reason))
          expect(enrollment.stage_text).to eq "#{I18n.t('enrollment/base.assigned')} (#{I18n.t('enrollment/base.discarted')})"
        end
      end
    end

    context 'when work flow finished' do
      it 'should return "Finalizado"' do
        enrollment = FactoryGirl.create(:enrollment_basis_with_applicant)
        work_flow = [
          {type: :module, class_name: 'Test::Stage', name: 'Test'},
          {type: :module, class_name: 'Onboarding::Stage', name: 'Documentación'},
          {type: :module, class_name: 'Engagement::Stage', name: 'Ingreso'}
        ]
        enrollment.course.update(work_flow: work_flow)
        enrollment.next_stage
        enrollment.next_stage
        enrollment.next_stage
        enrollment.next_stage
        expect(enrollment.stage_text).to eq I18n.t('enrollment/base.finished')
      end
    end

    context 'when work flow finished' do
      it 'should return name of module' do
        enrollment = FactoryGirl.create(:enrollment_basis_with_applicant)
        work_flow = [
          {type: :module, class_name: 'Test::Stage', name: 'Test'},
          {type: :module, class_name: 'Onboarding::Stage', name: 'Documentación'},
          {type: :module, class_name: 'Engagement::Stage', name: 'Ingreso'}
        ]
        enrollment.course.update(work_flow: work_flow)
        enrollment.next_stage
        expect(enrollment.stage_text).to eq 'Test'
        enrollment.next_stage
        expect(enrollment.stage_text).to eq 'Documentación'
        enrollment.next_stage
        expect(enrollment.stage_text).to eq 'Ingreso'
      end
    end
  end

  describe 'unenroll' do
    let(:postulation) { FactoryGirl.create(:minisite_postulation) }
    let(:enrollment_postulation) { FactoryGirl.create(:enrollment_basis, applicant: postulation.applicant) }
    let(:enrollment) { FactoryGirl.create(:enrollment_basis_with_applicant) }

    context 'When enrollment does not have stages or postulations related' do
      it 'Should returns true' do
        expect(enrollment.unenroll).to be true
      end
    end

    context 'when enrollment has postulations related' do
      it 'Should returns false with error' do
        expect(enrollment_postulation.unenroll).to be false
        expect(enrollment_postulation.errors.count).to eq 1
        expect(enrollment_postulation.errors[:unenroll]).to eq(['no puede ser removido(a), verifique que no hay etapas o postulaciones asociadas.'])
      end
    end

    context 'when enrollment has stages related' do
      it 'Should returns false with error' do
        enrollment.next_stage
        expect(enrollment.unenroll).to be false
        expect(enrollment.errors.count).to eq 1
        expect(enrollment.errors[:unenroll]).to eq(['no puede ser removido(a), verifique que no hay etapas o postulaciones asociadas.'])
      end
    end
  end

end
