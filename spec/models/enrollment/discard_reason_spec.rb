# == Schema Information
#
# Table name: enrollment_discard_reasons
#
#  id          :integer          not null, primary key
#  name        :string
#  description :text
#  created_at  :datetime
#  updated_at  :datetime
#

require 'rails_helper'

RSpec.describe Enrollment::DiscardReason, :type => :model do
  context "Validations presences" do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:description) }
  end

  context "Validations uniqueness" do
    it { should validate_uniqueness_of(:name) }
    it { should validate_uniqueness_of(:description) }
  end

  describe 'Validate destroyable' do
    let!(:discard_reason) { FactoryGirl.create(:enrollment_discard_reason) }
    context 'when it is set as default when a course is closed' do
      it 'should not be deleted' do
        trigger = Trigger::CloseCourseDiscardReason.create(resourceable: discard_reason)

        expect{
          discard_reason.destroy
        }.to change(Enrollment::DiscardReason, :count).by(0)

      end
    end

    context 'when it is not set as default when a course is closed' do
      it 'should be deleted' do
        expect{
          discard_reason.destroy
        }.to change(Enrollment::DiscardReason, :count).by(-1)
      end
    end

    context "when discard reason is used in a discarding" do
      it 'should not be deleted' do
        discarding = FactoryGirl.create(:enrollment_discarding, discard_reason: discard_reason)
        expect{
          discard_reason.destroy
        }.to raise_error(ActiveRecord::InvalidForeignKey).and change(Enrollment::DiscardReason, :count).by(0)
        discarding.reload
        expect(discarding).to be_valid
      end
    end
  end

  describe "parse mailer content" do
    let(:discard_reason) {FactoryGirl.create(:enrollment_discard_reason_custom)}
    let(:enrollment) {FactoryGirl.create(:enrollment_basis_with_applicant)}

    before :each do
      company_position_cenco = FactoryGirl.create(:company_positions_cenco, company_position: enrollment.course.company_position)
      FactoryGirl.create(:vacancy, course: enrollment.course, company_positions_cenco: company_position_cenco)
    end

    context 'when mailer content' do
      it "with Company Cenco Name" do
        response = discard_reason.parse_mailer_content(enrollment)

        message = "<li>Candidato = #{enrollment.applicant.name_or_email.capitalize}</li>"
        message +="<li>Cargo = #{enrollment.course.company_position.try(:name).to_s.downcase}</li>"
        message +="<li>Gerencia #{enrollment.course.vacancies.first.company_positions_cenco.try(:company_cenco_name).to_s.downcase}</li>"
        message +="<li>Proceso = #{enrollment.course.title.downcase}</li>"

        expect(response).to eq(message)
      end
    end
  end
end



