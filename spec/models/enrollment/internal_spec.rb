# == Schema Information
#
# Table name: enrollment_bases
#
#  id           :integer          not null, primary key
#  type         :string
#  course_id    :integer
#  created_at   :datetime
#  updated_at   :datetime
#  applicant_id :integer
#  stage_id     :integer
#

require 'rails_helper'

RSpec.describe Enrollment::Internal, :type => :model do
  context 'Send template internal postulation on create' do
    # Integra pidió que no se enviara aqui la planilla de postulación interna
    it 'should not send internal postulation template' do
      applicant = FactoryGirl.create(:applicant_bases)
      course = FactoryGirl.create(:course)
      Delayed::Job.destroy_all

      expect{
        Enrollment::Internal.create(course: course, applicant: applicant)
      }.to change(Delayed::Job, :count).by(0)

      # enrollment = Enrollment::Internal.last
      # intenal_postulation_email = YAML.load(Delayed::Job.last.handler)
      # expect(intenal_postulation_email.object).to eq Courses::InternalMailer
      # expect(intenal_postulation_email.method_name).to eq :send_internal_postulation_template
      # expect(intenal_postulation_email.args.include?(applicant)).to be true
      # expect(intenal_postulation_email.args.include?(course)).to be true
    end
  end
end
