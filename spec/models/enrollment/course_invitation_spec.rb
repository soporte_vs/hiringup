require 'rails_helper'

RSpec.describe Enrollment::CourseInvitation, type: :model do
  describe "Validations" do
    it { should validate_presence_of :enrollment }
    # it { should validate_uniqueness_of :enrollment }
  end

  describe "Relationships" do
    it { should belong_to :enrollment }
  end

  describe "Create" do
    it "should create a valid invitation" do
      invitation = FactoryGirl.create(:enrollment_course_invitation)
      expect(invitation.errors).to be_empty
    end

    it "should exist only one invitation per enrollment" do
      enrollment = FactoryGirl.create(:enrollment_basis_with_applicant)
      invitation = FactoryGirl.create(:enrollment_course_invitation, enrollment: enrollment)
      expect(invitation.errors).to be_empty

      invitation = FactoryGirl.build(:enrollment_course_invitation, enrollment: enrollment)
      expect(invitation.valid?).not_to be true
      expect(invitation.errors[:enrollment_id]).not_to be nil
    end
  end
end
