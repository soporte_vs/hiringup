require 'rails_helper'

RSpec.describe Enrollment::Dynamic, type: :model do
  let(:enrollment_dynamic) { FactoryGirl.create(:enrollment_dynamic) }
  let(:work_flow) { Enrollment::Base::WORK_FLOW }
  let(:type_stage) { work_flow[Random.rand(0..(work_flow.length-1))][:class_name] }

  describe 'Constant WORK_FLOW' do
    it 'should be an empty array' do
      expect(Enrollment::Dynamic::WORK_FLOW).to eq []
    end
  end

  describe 'Next Stage Class' do
    it { expect(enrollment_dynamic.next_stage_class).to be nil }
  end

  describe 'Next stage' do
    before :each do
      @current_stage = enrollment_dynamic.next_stage(type_stage, FFaker::Lorem.word)
    end

    it 'should receive a params called type_stage and create an instance of it' do
      possible_type_stages = work_flow.map{ |w| w[:class_name] } - enrollment_dynamic.stages.map(&:to_s)
      type_stage = possible_type_stages[Random.rand(0..(possible_type_stages.length-1))]
      expect{
        enrollment_dynamic.next_stage(type_stage, FFaker::Lorem.word)
      }.to change(type_stage.constantize, :count).by(1).and \
        change(enrollment_dynamic.stages, :count).by(1)

      new_current_stage = enrollment_dynamic.stages.last
      expect(enrollment_dynamic.current_stage_index).to eq new_current_stage.step
      expect(enrollment_dynamic.stage).to eq new_current_stage
    end

    it "should response false when enrollment is discarding" do
      possible_type_stages = work_flow.map{ |w| w[:class_name] } - enrollment_dynamic.stages.map(&:to_s)
      type_stage = possible_type_stages[Random.rand(0..(possible_type_stages.length-1))]
      enrollment_discarding = FactoryGirl.create(:enrollment_discarding, enrollment: enrollment_dynamic)
      enrollment_dynamic.reload

      expect{
        enrollment_dynamic.next_stage(type_stage, FFaker::Lorem.word)
      }.to change(type_stage.constantize, :count).by(0).and \
           change(enrollment_dynamic.stages, :count).by(0)

      expect(enrollment_dynamic.errors[:stage]).to eq(["No es posible cambiar de etapa, el candidato ya fue descartado"])

    end

    context 'when course is closed' do
      let(:course){ FactoryGirl.create(:course) }
      let(:enrollment_dynamic){ FactoryGirl.create(:enrollment_dynamic, course: course) }

      before :each do
        course.close!
      end

      it 'should not change enrollments stage, and no create a new stage for enrollment' do
        possible_type_stages = work_flow.map{ |w| w[:class_name] } - enrollment_dynamic.stages.map(&:to_s)
        type_stage = possible_type_stages[Random.rand(0..(possible_type_stages.length-1))]

        current_stage = enrollment_dynamic.stage
        expect(course.closed?).to be true
        expect{enrollment_dynamic.next_stage(type_stage, FFaker::Lorem.word)}.to change(Stage::Base, :count).by(0)
        expect(enrollment_dynamic.stage).to eq current_stage
      end
    end

  end
end
