# == Schema Information
#
# Table name: tag_used_tags
#
#  id            :integer          not null, primary key
#  tag_id        :integer
#  taggable_id   :integer
#  taggable_type :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

require 'rails_helper'

RSpec.describe Tag::UsedTag, type: :model do
  context "Validation and RelationShips of Model" do
    it "should validate tag is present" do
      should validate_presence_of(:tag)
    end
    it "should validate taggable is present" do
      should validate_presence_of(:taggable_type)
    end
    it "should belongs_to tag and taggable" do
      should belong_to(:tag)
      should belong_to(:taggable)
    end
  end

  context "Validation when taggable is a Course" do
    it "should validate used_tag is unique for a specific course" do
      used_tag = FactoryGirl.create(:tag_used_course_tag)
      new_tag = FactoryGirl.build(:tag_used_course_tag, tag: used_tag.tag, taggable: used_tag.taggable)
      expect(new_tag).to_not be_valid
    end
  end
end
