# == Schema Information
#
# Table name: tag_base_tags
#
#  id         :integer          not null, primary key
#  name       :string
#  type       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Tag::CourseTag, type: :model do
  context "Validation and Relationships on Model" do
    it "should has_many courses" do
      should have_many(:courses)
    end
  end
end
