require 'rails_helper'

RSpec.describe Tag::CompanyPositionTag, type: :model do
  context "Validation and Relationships on Model" do
    it "should has_many company_position" do
      should have_many(:company_positions)
    end
  end
end
