# == Schema Information
#
# Table name: tag_base_tags
#
#  id         :integer          not null, primary key
#  name       :string
#  type       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Tag::BaseTag, type: :model do
  context "Validation and Relationships on Model" do
    it "should validate name is present" do
      should validate_presence_of(:name)
    end
    it "should validate name is uniqueness" do
      should validate_uniqueness_of(:name)
    end
    it "should validate that type is present" do
      should validate_presence_of(:type)
    end 
    it "should has_many used_tag" do
      should have_many(:used_tags)
    end
  end
end
