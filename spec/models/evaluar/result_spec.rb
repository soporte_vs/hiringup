require 'rails_helper'

RSpec.describe Evaluar::Result, type: :model do
  describe 'relationships' do
    it { should belong_to(:applicant).class_name('Applicant::Base') }
    it { should belong_to(:evaluar_course).class_name('Evaluar::Course') }
  end

  describe 'validations' do
    it { should validate_presence_of :applicant }
    it { should validate_presence_of :evaluar_course }
    it { should validate_presence_of :data }
    it { should validate_presence_of :process_id }
    it { should validate_presence_of :identification }
  end
end
