require 'rails_helper'

RSpec.describe Evaluar::Course, type: :model do

  describe 'Validations' do
    it { should validate_presence_of(:agency_id) }
    it { should validate_presence_of(:confidential) }
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:start_date) }
    it { should validate_presence_of(:end_date) }
    it { should validate_presence_of(:callback_url).on(:update) }
    it { should validate_uniqueness_of(:name) }

    context "when name is valid" do
      let(:name_valid){ FFaker::Lorem.sentence.gsub(/[^a-záéíóúñA-ZÁÉÍÓÚÑ0-9 ]/, "") }
      let(:name_invalid){ FFaker::Lorem.sentence }
      let(:evaluar_course) { FactoryGirl.create(:evaluar_course) }

      it "should be valid" do
        evaluar_course.name = name_valid
        expect(evaluar_course).to be_valid
      end

      it "should not be valid" do
        evaluar_course.name = name_invalid
        expect(evaluar_course).not_to be_valid
        expect(evaluar_course.errors[:name]).not_to be_empty
      end
    end
  end


  describe 'Relationships' do
    it { should belong_to(:course).class_name('Course') }
  end

  describe 'Update callback_url' do
    let(:evaluar_course) { FactoryGirl.create(:evaluar_course) }
    context 'after create' do
      it 'sets callback_url attribute' do
        expect(evaluar_course.callback_url).to eq("/app/evaluar/courses/#{evaluar_course.id}/results")
      end
    end
  end

  describe '#create course evaluar' do
    let(:evaluar_course){ FactoryGirl.create(:evaluar_course) }
    let(:message_already_created) { I18n.t("activerecord.errors.models.evaluar/course.attributes.evaluar_id.already_created") }
    let(:message_error) { I18n.t("activerecord.errors.models.evaluar/course.attributes.evaluar_id.no_evaluar_id") }

    context 'when the course is created to Evaluar' do
      before :each do
        WebMock.reset!
      end

      it 'should return a 200 status code and creates a valid Evaluar' do
        expect(evaluar_course.callback_url).to_not eq nil
        expect(evaluar_course.evaluar_id).to_not eq nil
      end
    end

    context 'When the Course already exists in Evaluar' do
      it 'should return errors.messages[:already_created]' do
        expect(evaluar_course.evaluar_id).to_not eq nil
        expect(evaluar_course.create_in_evaluar).to eq(false)
        expect(evaluar_course.errors.count).to eq(1)
        expect(evaluar_course.errors.messages[:evaluar_id]).not_to be_empty
        expect(evaluar_course.errors.messages[:evaluar_id][0].include?(message_already_created)).to eq(true)
      end
    end

    context 'when the course could not be created because it has invalid parameters' do
      before :each do
        WebMock.reset!
        @evaluar_course_invalid = FactoryGirl.create(:evaluar_course, status: '')
        #Borro el mensaje de error que se retorna al momento de crear el couse invalido
        @evaluar_course_invalid.errors.clear
      end

      it 'should return errors.messages[:no_evaluar_id]' do
        expect(evaluar_course.callback_url).to_not eq nil
        expect(@evaluar_course_invalid.create_in_evaluar).to eq(false)
        expect(@evaluar_course_invalid.errors.count).to eq(1)
        expect(@evaluar_course_invalid.errors.messages[:evaluar_id]).not_to be_empty
        expect(@evaluar_course_invalid.errors.messages[:evaluar_id][0].include?(message_error)).to eq(true)
      end
    end
  end

end
