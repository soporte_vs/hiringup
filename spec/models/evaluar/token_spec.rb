require 'rails_helper'

RSpec.describe Evaluar::Token, type: :model do
  let(:response_body) {
    File.read('spec/factories/evaluar/response_login_evaluar.json')
  }
  let(:response_json){ JSON.parse(response_body) }
  let(:valid_response_stub){
    stub_request(:post, EVALUAR_LOGIN_URL).to_return(
      body: response_json.to_json,
      status: 200
    )
  }
  let(:invalid_response_stub){
    response_json['access_token'] = ''
      stub_request(:post, EVALUAR_LOGIN_URL).to_return(
        body: response_json.to_json,
        status: 404
      )
  }


  context 'Validations' do
    before :each do
      invalid_response_stub
    end
    it { should validate_presence_of(:access_token) }
  end

  describe '#create' do
    let(:evaluar_token){ FactoryGirl.build(:evaluar_token) }
    let(:save_token){ evaluar_token.save }
    let(:expected_condition){nil}
    let(:evaluar_token_change_expected){nil}
    let(:evaluar_token_count_expected){
      change(Evaluar::Token, :count).by(1)
    }

    context 'connection was successful' do
      before :each do
        valid_response_stub
      end
      let(:expected_condition){true}
      let(:evaluar_token_change_expected){
        expect(evaluar_token.access_token).to eq(response_json['access_token'])
        expect(evaluar_token.expires_in).to eq(response_json['expires_in'])
        expect(evaluar_token.refresh_expires_in).to eq(response_json['refresh_expires_in'])
        expect(evaluar_token.refresh_token).to eq(response_json['refresh_token'])
        expect(evaluar_token.token_type).to eq(response_json['token_type'])
        expect(evaluar_token.id_token).to eq(response_json['id_token'])
        expect(evaluar_token.not_before_policy).to eq(response_json['not-before-policy'])
        expect(evaluar_token.session_state).to eq(response_json['session-state'])

      }

      it 'creates a valid Evaluar::Token' do
      end
    end

    context 'connection was unsuccessful' do
      before :each do
        invalid_response_stub
      end
      let(:expected_condition){false}
      let(:evaluar_token_change_expected){
        expect(evaluar_token.errors.count).to eq(1)
        expect(evaluar_token.errors.messages[:access_token][0].include?('no puede estar en blanco')).to eq(true)
      }
      let(:evaluar_token_count_expected){
        change(Evaluar::Token, :count).by(0)
      }

      it 'does not create a Evaluar::Token' do
      end
    end

    after :each do
      expect{
        expect(save_token).to be expected_condition
      }.to evaluar_token_count_expected

      evaluar_token_change_expected
    end
  end

  describe '#self.get_valid_token' do
    let(:get_valid_token) { Evaluar::Token.get_valid_token }
    let(:evaluar_token_change_expected){nil}
    let(:evaluar_token_count_expected){
      change(Evaluar::Token, :count).by(0)
    }

    before :each do
      valid_response_stub
    end

    context 'token already exists' do
      context 'token is valid' do
        let(:token){FactoryGirl.create(:evaluar_token)}
        let(:evaluar_token_change_expected){
          expect(token).to eq(get_valid_token)
        }

        it 'returns a already created valid token' do
          token.reload
        end
      end

      context 'token is invalid(expired)' do
        let(:token){FactoryGirl.create(:evaluar_token, created_at: 25.hour.ago)}
        let(:evaluar_token_change_expected){
          expect(token).to_not eq(get_valid_token)
        }
        let(:evaluar_token_count_expected){
          change(Evaluar::Token, :count).by(1)
        }

        it 'returns a new token' do
          token.reload
        end
      end
    end

    context 'token does not exist' do
      let(:evaluar_token_count_expected){
        change(Evaluar::Token, :count).by(1)
      }

      it 'creates a new token' do
      end
    end

    after :each do
      expect{
        get_valid_token
      }.to evaluar_token_count_expected

      evaluar_token_change_expected
    end
  end

  describe 'testing call all services' do
    let(:evaluar_token){ FactoryGirl.create(:evaluar_token) }

    before :each do
      WebMock.reset!
    end

    describe '#call_authentication_service' do
      it {
        response = evaluar_token.send(:call_authentication_service)
        expect(response.code).to eq 200
        expect(response.message).to eq 'OK'
        expect(response.body).to match_response_schema("evaluar/token/authentication")
        json_response = JSON.parse(response.body)
        expect(json_response['access_token']).to_not eq nil
      }
    end
  end

end
