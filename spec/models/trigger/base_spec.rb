require 'rails_helper'

RSpec.describe Trigger::Base, type: :model do

  describe 'validations' do
    it 'resourceable should be present' do
      should validate_presence_of :resourceable
    end

    it { should validate_presence_of :type }

  end
end
