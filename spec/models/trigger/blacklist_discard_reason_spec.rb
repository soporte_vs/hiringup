# coding: utf-8
require 'rails_helper'

RSpec.describe Trigger::BlacklistDiscardReason, type: :model do

  describe 'validations' do
    it 'resourceable should be present' do
      should validate_presence_of :resourceable
    end

    context 'create the trigger only if resourceable type is Enrollment::DiscardReason' do
      before :each do
        FactoryGirl.create(:trigger_blacklist_discard_reason)
      end

      it 'should create the trigger if Enrollment::DiscardReason is the given resourceable' do
        expected_resourceable_type = Enrollment::DiscardReason.new.class.to_s
        trigger = FactoryGirl.create(:trigger_close_course_discard_reason, resourceable: FactoryGirl.create(:enrollment_discard_reason))
        expect(trigger.resourceable_type).to eq expected_resourceable_type
        expect(trigger.errors[:resourceable].empty?).to eq true

        # y sigue siendo sólo uno
        expect(Trigger::CloseCourseDiscardReason.count).to eq 1
      end

      it 'should not create a trigger if given resourceable is not a Enrollment::DiscardReason' do
        expected_resourceable_type = Enrollment::DiscardReason.new.class.to_s
        trigger = Trigger::BlacklistDiscardReason.create(resourceable: FactoryGirl.create(:company_record))

        expect(trigger).not_to be_valid
        # expect(trigger).to have(1).error_on(:resourceable)
        expect(trigger.errors[:resourceable].empty?).to eq false

        # y sigue siendo sólo uno
        expect(Trigger::BlacklistDiscardReason.count).to eq 1
      end
    end
  end
end
