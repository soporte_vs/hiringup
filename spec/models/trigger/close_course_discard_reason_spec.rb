# coding: utf-8
require 'rails_helper'

RSpec.describe Trigger::CloseCourseDiscardReason, type: :model do

  describe 'validations' do
    it 'resourceable should be present' do
      should validate_presence_of :resourceable
    end

    it 'should have only one resource (discard reason)' do
      should validate_uniqueness_of(:resourceable_type)
    end


    it 'should be only one trigger close course discard reason' do
      FactoryGirl.create(:trigger_close_course_discard_reason)

      expect{
        FactoryGirl.create(:trigger_close_course_discard_reason)
      }.to change(Trigger::CloseCourseDiscardReason, :count).by(0)

    end

    context 'overwrite last record (discard reason) and use it if exists when creates one' do
      let(:discard_reason1) {FactoryGirl.create(:enrollment_discard_reason)}
      let(:discard_reason2) {FactoryGirl.create(:enrollment_discard_reason)}
      let(:trigger) {FactoryGirl.create(:trigger_close_course_discard_reason, resourceable: discard_reason1)}

      before :each do
        expect(trigger.resourceable_id).to eq discard_reason1.id
        expect(trigger.resourceable_type).to eq discard_reason1.class.to_s
        @expected_resourceable_type = discard_reason2.class.to_s
        @expected_resourceable_id = discard_reason2.id
      end

      it {
        expect{
          Trigger::CloseCourseDiscardReason.create(resourceable: discard_reason2)
        }.to change(Trigger::CloseCourseDiscardReason, :count).by(0)
      }

      it {
        expect{
          Trigger::CloseCourseDiscardReason.create(resourceable_id: discard_reason2.id)
        }.to change(Trigger::CloseCourseDiscardReason, :count).by(0)
      }

      it {
        @expected_resourceable_id = discard_reason1.id
        expect{
          Trigger::CloseCourseDiscardReason.create(resourceable_type: discard_reason2.class.to_s)
        }.to change(Trigger::CloseCourseDiscardReason, :count).by(0)
      }

      after :each do
        trigger.reload

        expect(trigger.resourceable_id).to eq @expected_resourceable_id
        expect(trigger.resourceable_type).to eq @expected_resourceable_type
        expect(Trigger::CloseCourseDiscardReason.count).to eq 1
      end
    end

    context 'create the trigger only if resourceable type is Enrollment::DiscardReason' do

      before :each do
        FactoryGirl.create(:trigger_close_course_discard_reason, resourceable: FactoryGirl.create(:enrollment_discard_reason))
      end

      it 'should create the trigger if Enrollment::DiscardReason is the given resourceable' do
        expected_resourceable_type = Enrollment::DiscardReason.new.class.to_s
        trigger = FactoryGirl.create(:trigger_close_course_discard_reason, resourceable: FactoryGirl.create(:enrollment_discard_reason))
        expect(trigger.resourceable_type).to eq expected_resourceable_type
        expect(trigger.errors[:resourceable].empty?).to eq true

        # y sigue siendo sólo uno
        expect(Trigger::CloseCourseDiscardReason.count).to eq 1
      end

      it 'should not create a trigger if given resourceable is not a Enrollment::DiscardReason' do
        expected_resourceable_type = Enrollment::DiscardReason.new.class.to_s
        trigger = Trigger::CloseCourseDiscardReason.create(resourceable: FactoryGirl.create(:company_record))

        expect(trigger).not_to be_valid
        # expect(trigger).to have(1).error_on(:resourceable)
        expect(trigger.errors[:resourceable].empty?).to eq false

        # y sigue siendo sólo uno
        expect(Trigger::CloseCourseDiscardReason.count).to eq 1
      end
    end
  end
end
