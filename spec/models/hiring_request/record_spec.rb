require 'rails_helper'

RSpec.describe HiringRequest::Record, type: :model do
  context 'RelationShips' do
    it 'should belongs_to a company_positions_cenco' do
      should belong_to(:company_positions_cenco)
    end

    it 'should has many vacancies' do
      should have_many(:vacancies).dependent(:nullify)
    end

    it 'should belongs_to a company_vacancy_request_reason' do
      should belong_to(:company_vacancy_request_reason)
    end

    it 'should belongs_to a applicant' do
      should belong_to(:applicant)
    end

    it 'should belongs_to a company_contract_type' do
      should belong_to(:company_contract_type)
    end

    it 'should belongs_to a company_estate' do
      should belong_to(:company_estate)
    end

    it 'should belongs_to a company_timetable' do
      should belong_to(:company_timetable)
    end

    it 'should accept nested attributes for applicant' do
      should accept_nested_attributes_for(:applicant)
    end

  end

  context 'Validations' do
    it 'should validate presence of company_positions_cenco' do
      should validate_presence_of(:company_positions_cenco)
    end

    it 'should validate presence of company_contract_type' do
      should validate_presence_of(:company_contract_type)
    end

    it { should validate_presence_of(:created_by) }

    it 'should validate presence of request_date' do
      should validate_presence_of(:request_date)
    end

    it 'should validate presence of entry_date' do
      should validate_presence_of(:entry_date)
    end

    it 'should validate presence of number_vacancies and beetween 0 and 101' do
      should validate_presence_of(:number_vacancies)
    end

    it 'should validate presence of company_vacancy_request_reason' do
      should validate_presence_of(:company_vacancy_request_reason)
    end
  end

  context 'Create' do
    it 'should create a valid' do
      hr = FactoryGirl.create(:hiring_request_record)
      expect(hr).to be_valid
      expect(hr.active?).to be true
    end

    it 'should build a invalid' do
      expect(
        FactoryGirl.build(:hiring_request_record_invalid)
      ).to_not be_valid
    end
  end

  context 'Switch State' do
    it 'should change state from active to rejected' do
      hr = FactoryGirl.create(:hiring_request_record)
      expect(hr.active?).to be true
      hr.reject!
      hr.reload
      expect(hr.active?).to be false
      expect(hr.rejected?).to be true
    end

    it 'should change state from active to accepted' do
      hr = FactoryGirl.create(:hiring_request_record)
      expect(hr.active?).to be true
      expect{
        hr.accept!
      }.to change(hr.vacancies, :count).by(hr.number_vacancies)
      hr.reload
      expect(hr.active?).to be false
      expect(hr.accepted?).to be true
    end

    it 'should change state from accepted to rejected' do
      hr = FactoryGirl.create(:hiring_request_record)
      hr.accept!
      expect(hr.accepted?).to be true
      hr.reload
      expect{
        hr.reject!
      }.to change(hr.vacancies, :count).by(-hr.vacancies.length)
      expect(hr.rejected?).to be true
    end

    it 'should not reject if vacancies has agreement' do
      stage = FactoryGirl.create(:engagement_stage)
      hr = FactoryGirl.create(:hiring_request_record)
      hr.accept!
      vacancy = hr.vacancies.first
      vacancy.update(course: stage.enrollment.course)
      FactoryGirl.create(:engagement_agreement,
                          engagement_stage: stage,
                          vacancy: vacancy)


      hr.reload
      expect(hr.accepted?).to be true
      expect{
        expect{
          hr.reject!
        }.to raise_error AASM::InvalidTransition
      }.to change(hr.vacancies, :count).by(0)

      expect(hr.rejected?).to be false
      expect(hr.accepted?).to be true
    end

    it 'should not change state from from rejected to accepted' do
      hr = FactoryGirl.create(:hiring_request_record)
      hr.reject!
      hr.reload
      expect{
        hr.accept!
      }.to raise_error AASM::InvalidTransition
      hr.reload
      expect(hr.active?).to be false
      expect(hr.rejected?).to be true
    end
  end
end
