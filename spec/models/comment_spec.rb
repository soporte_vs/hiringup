require 'rails_helper'

RSpec.describe Comment, type: :model do
  context 'RelationShips' do
    it 'should belong_to user' do
      should belong_to(:user)
    end

    it 'should belong_to object' do
      should belong_to(:object)
    end
  end

  context 'Validations' do
    it 'should validate presence of user' do
      should validate_presence_of :user
    end

    it 'should validate presence of content' do
      should validate_presence_of :content
    end

    it 'should validate presence of object_id' do
      should validate_presence_of :object_id
    end

    it 'should validate presence of object_type' do
      should validate_presence_of :object_type
    end
  end

  context 'Create' do
    it 'should create a valid' do
      comment_applicant = FactoryGirl.create(:comment_applicant)
      comment_course = FactoryGirl.create(:comment_applicant, object: FactoryGirl.create(:course))
      expect(comment_applicant.object.comments).to match_array [comment_applicant]
      expect(comment_applicant.user.personal_information).not_to be(nil)
    end
  end
end
