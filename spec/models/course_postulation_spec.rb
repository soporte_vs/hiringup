# coding: utf-8
require 'rails_helper'

RSpec.describe CoursePostulation, type: :model do
  describe 'Relationships' do
    it { should belong_to(:course) }
    it { should belong_to(:postulationable) }
    it { should belong_to(:applicant)
                  .class_name("Applicant::Base")
                  .with_foreign_key(:applicant_id) }
  end

  describe 'Validations' do
    it { should validate_presence_of(:course) }
    it { should validate_presence_of(:postulationable) }
    it { should validate_presence_of(:applicant) }

    context "$COMPANY[:validate_course_postulation_uniqueness] is declared" do
      let(:postulations_count_expect){
        change(Trabajando::Postulation, :count).by(1).and \
        change(CoursePostulation, :count).by(0)
      }
      let!(:define_value) {
        $COMPANY[:validate_course_postulation_uniqueness] = value
      }
      let(:course) { FactoryGirl.create(:course) }
      let(:minisite_publication){
        FactoryGirl.create(:minisite_publication, course_id: course.id)
      }
      let!(:minisite_postulation){
        FactoryGirl.create(
          :minisite_postulation,
          minisite_publication_id: minisite_publication.id,
          applicant: FactoryGirl.create(:applicant_bases)
        )
      }

      context "and it is declared as true" do
        let(:value) { true }

        it "should validate uniqueness of applicant_id scoped to course_id" do
        end
      end

      context "and it is declared as false" do
        let(:postulations_count_expect){
          change(Trabajando::Postulation, :count).by(1).and \
          change(CoursePostulation, :count).by(1)
        }
        let(:value) { false }

        it "should not validate uniqueness of applicant_id scoped to course_id" do
        end
      end

      after :each do
        trabajando_publication = FactoryGirl.create(:trabajando_publication, course: course)
        expect{
          trabajando_postulation = Trabajando::Postulation.create(
            trabajando_publication: trabajando_publication,
            applicant: minisite_postulation.applicant,
            postulation_date: Date.today
          )
        }.to postulations_count_expect
      end
    end
  end

  describe 'build a new' do
    it { expect(FactoryGirl.build(:course_postulation_invalid)).to be_invalid }
  end

  describe 'Create a new' do
    it {
      # este build genera una minisite_publication, que a su vez ya genera una CoursePostulation
      course_postulation = FactoryGirl.build(:course_postulation)
      expect{
        # Este save salva sólo course_postulation
        course_postulation.save
      }.to change(CoursePostulation, :count).by(1)
    }
  end

  describe "#validate_course_postulation_uniqueness" do
    context "$COMPANY[:validate_course_postulation_uniqueness] is declared" do
      let(:define_value) { $COMPANY[:validate_course_postulation_uniqueness] = value }

      context "and it is declared as true" do
        let(:value) { true }
        it "should return true" do
          define_value
          expect(CoursePostulation.validate_course_postulation_uniqueness).to be true
        end
      end

      context "and it is declared as false" do
        let(:value) { false }
        it "should return false" do
          define_value
          expect(CoursePostulation.validate_course_postulation_uniqueness).to be false
        end
      end
    end

    context "$COMPANY[:validate_course_postulation_uniqueness] is not declared" do
      it "should return false" do
        expect(CoursePostulation.validate_course_postulation_uniqueness).to be false
      end
    end
  end

  describe 'reset' do
    context 'when postulation has not been accepted/rejected yet' do
      it 'should return false and errors to eq 1' do
        postulation = FactoryGirl.create(:minisite_postulation, is_accepted: nil, accepted_at: nil)
        expect(postulation.course_postulation.reset).to be false
        expect(postulation.course_postulation.errors.count).to eq 1
        expect(postulation.course_postulation.errors[:reset]).to eq(['La postulación ya se encuentra pendiente.'])
      end
    end

    context 'when postulation current status is rejected' do
      context "postulation has no 'accepted_at' attribute"do
        it 'should return true and no errors' do
          @postulation = FactoryGirl.create(:trabajando_postulation, is_accepted: false)
        end
      end

      context "postulation has 'accepted_at' attribute" do
        it 'should return true and no errors' do
          @postulation = FactoryGirl.create(:minisite_postulation, is_accepted: false)
        end
      end

      after :each do
        expect(@postulation.course_postulation.reset).to be true
        @postulation.reload
        expect(@postulation.is_accepted).to be nil
        expect(@postulation.try(:accepted_at)).to be nil
        expect(@postulation.course_postulation.errors.count).to eq 0
      end
    end

    context 'when postulation current status is accepted' do
      context 'when postulation has enrollment related' do
        context 'when enrollment has stages' do
          before :each do
            @postulation = FactoryGirl.create(:minisite_postulation)
            enrollment = FactoryGirl.create(:enrollment_basis, applicant: @postulation.applicant)
            enrollment.next_stage
          end

          it 'should return false and assign errors' do
            expect(@postulation.course_postulation.reset).to be false
            expect(@postulation.is_accepted).to be true
            expect(@postulation.course_postulation.errors.count).to eq 1
            expect(@postulation.course_postulation.errors[:reset]).to eq(['La postulación posee etapas asociadas.'])
          end
        end

        context 'when enrollment does not have stages' do
          context "postulation has 'accepted_at' attribute" do
            it 'should return true, no errors and delete enrollment' do
              @postulation = FactoryGirl.create(:minisite_postulation)
            end
          end

          context "postulation has no 'accepted_at' attribute" do
            it 'should return true, no errors and delete enrollment' do
              @postulation = FactoryGirl.create(:trabajando_postulation, is_accepted: true)
            end
          end

          after :each do
            FactoryGirl.create(:enrollment_basis, applicant: @postulation.applicant)
            expect{
              expect(@postulation.course_postulation.reset).to be true
            }.to change(Enrollment::Base, :count).by -1
            @postulation.reload
            expect(@postulation.is_accepted).to be nil
            expect(@postulation.try(:accepted_at)).to be nil
            expect(@postulation.course_postulation.errors.count).to eq 0
          end
        end
      end

      context 'when enrollment is discarded' do
        let(:postulation){ FactoryGirl.create(:minisite_postulation) }
        context "postulation has 'accepted_at' attribute" do
          it 'should return false and assign errors' do
          end
        end

        context "postulation has no 'accepted_at' attribute" do
          let(:postulation) { FactoryGirl.create(:trabajando_postulation, is_accepted: true) }

          it 'should return false and assign errors' do
          end
        end

        after :each do
          enrollment = FactoryGirl.create(:enrollment_basis, applicant: postulation.applicant)
          FactoryGirl.create(:enrollment_discarding, enrollment: enrollment)
          expect{
            expect(postulation.course_postulation.reset).to be false
          }.to change(Enrollment::Base, :count).by 0
          expect(postulation.is_accepted).to be true
          expect(postulation.course_postulation.errors.count).to eq 1
          expect(postulation.course_postulation.errors[:reset]).to eq(['El candidato ha sido descartado del proceso.'])
        end
      end

      context 'when postulation does not have enrollment related' do
        context "postulation has 'accepted_at' attribute" do
          it 'should return true, no errors' do
            @postulation = FactoryGirl.create(:minisite_postulation)
          end
        end

        context "postulation has no 'accepted_at' attribute" do
          it 'should return true, no errors' do
            @postulation = FactoryGirl.create(:trabajando_postulation, is_accepted: true)
          end
        end

        after :each do
          expect{
              expect(@postulation.course_postulation.reset).to be true
          }.to change(Enrollment::Base, :count).by 0
          @postulation.reload
          expect(@postulation.is_accepted).to be nil
          expect(@postulation.try(:accepted_at)).to be nil
          expect(@postulation.course_postulation.errors.count).to eq 0
        end
      end
    end
  end

  describe '#postulation_date' do
    context 'when postulationable is a Minisite::Postulation' do
      let(:minisite_postulation){ FactoryGirl.create(:minisite_postulation) }
      let(:course_postulation){ minisite_postulation.course_postulation }

      it 'should return created_at of postulationable' do
        format = "%m/%d/%Y %I:%M"
        expect(
          course_postulation.postulation_date.strftime(format)
        ).to eq minisite_postulation.created_at.strftime(format)
      end
    end

    context 'when postulationable is a Trabajando::Postulation' do
      let(:trabajando_postulation){ FactoryGirl.create(:trabajando_postulation) }
      let(:course_postulation){ trabajando_postulation.course_postulation }

      it 'should return postulation_date of postulationable' do
        format = "%m/%d/%Y %I:%M"
        expect(
          course_postulation.postulation_date.strftime(format)
        ).to eq trabajando_postulation.postulation_date.strftime(format)
      end
    end

    # Cuando se refactorice bien la integración de Laborum debería
    # testearse esto
    # context 'when postulationable is a Laborum::Postulation' do
    #   let(:laborum_postulation){ FactoryGirl.create(:laborum_postulation) }
    #   let(:course_postulation){ laborum_postulation.course_postulation }

    #   it 'should return postulation_date of postulationable' do
    #     expect(course_postulation.postulation_date).to eq laborum_postulation.created_at
    #   end
    # end
  end
end
