# == Schema Information
#
# Table name: company_cencos
#
#  id                       :integer          not null, primary key
#  company_management_id :integer
#  name                     :string
#  address                  :string
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#

require 'rails_helper'

RSpec.describe Company::Cenco, type: :model do
  context 'Validations overs fields' do
    it 'name should be present' do
      should validate_presence_of :name
    end

    it 'name should be uniq' do
      should validate_uniqueness_of :name
    end

    it 'company_management should be present' do
      should validate_presence_of :company_management
    end
  end

  context 'RelationShips' do
    it 'should belong to company_management' do
      should belong_to(:company_management).inverse_of(:company_cencos)
    end

    it 'should has many company_positions_cencos' do
      should have_many(:company_positions_cencos).inverse_of(:company_cenco).dependent(:destroy)
    end

    it 'should has many company_positions' do
      should have_many(:company_positions).through(:company_positions_cencos)
    end
  end

  context 'Create' do
    it 'should create a valid cenco' do
      expect(FactoryGirl.create :company_cenco).to be_valid
    end

    it 'should create an invalid cenco' do
      expect(FactoryGirl.build :company_cenco_invalid).to_not be_valid
    end
  end
end
