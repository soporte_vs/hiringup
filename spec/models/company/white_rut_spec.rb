require 'rails_helper'

RSpec.describe Company::WhiteRut, type: :model do
  describe 'Validations' do
    it {
      should validate_presence_of(:rut)
    }

    it {
      should validate_uniqueness_of(:rut)
    }
  end

  describe 'Build a WhiteRUT' do
    it {
      expect(FactoryGirl.build(:company_white_rut).valid?).to be true
    }
    it {
      expect(FactoryGirl.build(:company_white_rut_invalid).valid?).to be false
    }
  end

  describe 'Load White list RUT from file' do
    it '' do
      notify_to = FFaker::Internet.email

      expect{
        Company::WhiteRut.load_white_list_from_file("#{Rails.root}/spec/tmp/rut-model.xlsx", notify_to)
      }.to change(Company::WhiteRut, :count).by(2).and \
           change(Delayed::Job, :count).by(1)

      report_mail = YAML.load(Delayed::Job.last.handler)
      expect(report_mail.object).to eq Company::WhiteRutMailer
      expect(report_mail.method_name).to eq :report_load_white_list
      expect(report_mail.args.include?(notify_to)).to be true
    end
  end
end
