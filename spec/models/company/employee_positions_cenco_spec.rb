require 'rails_helper'

RSpec.describe Company::EmployeePositionsCenco, type: :model do
  describe "relationship" do
    it{should belong_to(:company_employee).class_name(Company::Employee.to_s)}
    it{should belong_to(:company_positions_cenco).class_name(Company::PositionsCenco.to_s)}
  end

  describe 'validation' do
    it{should validate_presence_of(:company_employee)}
    it{should validate_presence_of(:company_positions_cenco)}
  end

end
