require 'rails_helper'

RSpec.describe Company::Employee, type: :model do
  describe "relationship" do
    it{should belong_to(:user)}
    it{should belong_to(:boss).class_name(Company::Employee.to_s)}
    it{should have_many(:subordinates).class_name(Company::Employee.to_s).dependent(:nullify)}
    it{should have_many(:company_employee_positions_cencos).class_name(Company::EmployeePositionsCenco.to_s).dependent(:destroy)}
    it{should have_many(:company_positions_cencos).through(:company_employee_positions_cencos)}
    it{should have_many(:company_references).dependent(:destroy)}
  end

  describe 'validation' do
    it{should validate_presence_of(:user)}
  end

  describe 'actions after save' do
    it 'should create an applicant' do
      expect{
        FactoryGirl.create(:company_employee)
      }.to change(Company::ApplicantEmployee, :count).by(1)
      employee = Company::Employee.last
      expect(employee.user.applicant).to eq Company::ApplicantEmployee.last
    end
  end

  describe 'Load employees from file' do
    let(:notify_to) { FFaker::Internet.email }


    # En el archivo que se lee, vienen los siguientes empleados
    # pedro@gmail.com     - activo   - jefe: jpaz@gmail.com
    # maria@gmail.com     - inactivo - jefe: jpaz@gmail.com
    # juan@gmail.com      - activo   - jefe: ppaez@gmail.com
    # guillermo@gmail.com - inactivo - jefe: ppaez@gmail.com
    # gonzalo@gmail.com   - activo   - jefe: cgonzales@gmail.com
    # claudio@gmail.com   - activo   - jefe: ppaez@gmail.com
    # rodrigo@gmail.com   - inactivo - jefe: cgonzales@gmail.com

    before :each do
      uploaded_file = ActionDispatch::Http::UploadedFile.new({
        :tempfile => File.new("#{Rails.root}/app/assets/documents/plantilla-modelo.xlsx")
      })

      file_name = "employees-#{I18n.l(Time.now, format: '%d_%m_%Y_%H_%M_%S')}"
      @tmp_file = "#{Rails.root}/tmp/#{file_name}.xlsx"
      File.open(@tmp_file, 'wb') do |f|
        f.write uploaded_file.read
      end
    end

    context 'when boss jpaz@gmail.com has user and employee created and he is not a boss' do
      let(:jpaz) { FactoryGirl.create(:user, email: 'jpaz@gmail.com') }
      before :each do
        FactoryGirl.create(:company_employee, user: jpaz, is_boss: false)
      end

      it 'should create a 6 company_employees and 6 users' do
        expect{
          Company::Employee.load_massive_from_file(@tmp_file, notify_to)
        }.to change(Company::Employee, :count).by(6).and \
             change(People::PersonalInformation, :count).by(4) # Debido a que se proporcionó información sólo de 4 empleados
             change(Delayed::Job, :count).by(1)

        jpaz.reload
        expect(jpaz.company_employee.is_boss).to be true
        expect(jpaz.company_employee.subordinates.map(&:user).map(&:email)).to match_array ['pedro@gmail.com']
      end
    end

    context 'when all employees are created already and are actives all' do
      let(:employees) { ['pedro', 'maria', 'juan', 'guillermo', 'gonzalo', 'claudio', 'rodrigo'] }
      let(:bosses) { ['jpaz', 'ppaez', 'cgonzales'] }

      before :each do
        (employees + bosses).each do |name|
          user = FactoryGirl.create(:user, email: "#{name}@gmail.com", is_active: true)
          company_employee = FactoryGirl.create(:company_employee, user: user)
          expect(company_employee.subordinates.count).to eq 0
          expect(company_employee.boss.nil?).to be true
        end
      end

      it 'should destroy all employees with active as false' do
        expect{
          Company::Employee.load_massive_from_file(@tmp_file, notify_to)
        }.to change(Company::Employee, :count).by(-3).and \
             change(Delayed::Job, :count).by(1)

      end
    end
  end
end
