# == Schema Information
#
# Table name: company_positions_cencos
#
#  id                       :integer          not null, primary key
#  company_position_id      :integer
#  company_cenco_id :integer
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#

require 'rails_helper'

RSpec.describe Company::PositionsCenco, type: :model do
  context 'Relations on model' do
    it 'should belong_to company_position' do
      should belong_to :company_position
    end

    it 'should belong_to company_cenco' do
      should belong_to :company_cenco
    end
  end

  context 'Validations' do
    it 'company_position should be present' do
      should validate_presence_of :company_position
    end

    it 'company_cenco should be present' do
      should validate_presence_of :company_cenco
    end

    it 'company_position should be uniq scoped to company_cenco' do
      FactoryGirl.create :company_positions_cenco
      position_business_unit = Company::PositionsCenco.last

      expect{
        FactoryGirl.create :company_positions_cenco, {
          company_position: position_business_unit.company_position,
          company_cenco: position_business_unit.company_cenco
        }
      }.to raise_error(ActiveRecord::RecordInvalid, 'La validación falló: Company position ya está en uso')
    end
  end

  context 'Create' do
    it 'should create a vlaid company_positions_cenco' do
      expect(FactoryGirl.create(:company_positions_cenco)).to be_valid
    end
  end

  describe 'Get CompanyPositions get_with_large_name' do
    it 'should be eq to return of method fullname' do
      company_positions_cenco = FactoryGirl.create(:company_positions_cenco)
      expect(company_positions_cenco.fullname).to eq Company::PositionsCenco.get_with_large_name.last.name
    end
  end
end
