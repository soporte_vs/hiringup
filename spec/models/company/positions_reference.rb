require 'rails_helper'

RSpec.describe Company::PositionsReference, type: :model do

  context 'RelationShips' do
    it { should belong_to(:company_reference).inverse_of(:company_positions_references) }
    it { should belong_to :company_position }
  end

  context 'Validations' do
    it { should validate_presence_of :company_reference }
    it { should validate_presence_of :company_position }
  end
end
