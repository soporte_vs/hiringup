require 'rails_helper'

RSpec.describe Company::Record, type: :model do
  describe 'Relationships' do
    it { should have_many(:managements).dependent(:destroy) }
    it { should have_many(:company_business_units) }
  end

  describe 'Validations' do
    it 'name should be present' do
      should validate_presence_of :name
    end

    it 'name should be uniq' do
      should validate_uniqueness_of :name
    end

    it 'email_domain should be present' do
      should validate_presence_of :email_domain
    end

    it 'email_domain should be uniq' do
      should validate_uniqueness_of :email_domain
    end
  end

  describe 'Create' do
    it 'should create a valid' do
      expect(FactoryGirl.build(:company_record)).to be_valid
    end

    it 'should create a invalid' do
      expect(FactoryGirl.build(:company_record_invalid)).to_not be_valid
    end
  end
end
