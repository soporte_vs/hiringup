require 'rails_helper'

RSpec.describe Company::PositionTrabajandoJobType, type: :model do
  context "validations" do
    before :each do
      @company_position_trabajando_job_type = FactoryGirl.create(:company_position_trabajando_job_type)
    end

    it 'should validate uniqueness of trabajando_job_type_id scoped to company_position id' do
      should validate_uniqueness_of(:trabajando_job_type_id).scoped_to(:company_position_id)
      expect(@company_position_trabajando_job_type.errors.empty?).to be true

      cc2 = FactoryGirl.build(:company_position_trabajando_job_type, company_position_id: @company_position_trabajando_job_type.company_position_id, trabajando_job_type_id: @company_position_trabajando_job_type.trabajando_job_type.id)
      expect(cc2.valid?).to be false
      expect(cc2.errors[:trabajando_job_type_id].empty?).to be false
      should validate_uniqueness_of(:trabajando_job_type_id).scoped_to(:company_position_id)
    end

    it { should belong_to :trabajando_job_type }
    it { should belong_to :company_position }
  end
end
