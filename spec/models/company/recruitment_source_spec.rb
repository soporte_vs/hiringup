require 'rails_helper'

RSpec.describe Company::RecruitmentSource, type: :model do
  context 'Validations and relationships' do
    it { should have_many(:applicants) }

    it 'name should be present' do
      should validate_presence_of :name
    end

    it 'name should be uniq' do
      should validate_uniqueness_of :name
    end
  end

  context 'Build' do
    it 'should build an valid' do
      expect(FactoryGirl.build(:company_recruitment_source)).to be_valid
    end

    it 'should build an invalid' do
      expect(FactoryGirl.build(:company_recruitment_source_invalid)).to_not be_valid
    end
  end
end
