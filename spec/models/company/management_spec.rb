require 'rails_helper'

RSpec.describe Company::Management, type: :model do
  describe 'Validations' do
    it { should validate_presence_of(:company_record) }
    it { should validate_presence_of(:company_business_unit) }

    it 'company_business_unit should be uniq for one company_record' do
      management = FactoryGirl.create(:company_management)
      expect{
        FactoryGirl.create(:company_management,
          company_record: management.company_record,
          company_business_unit: management.company_business_unit
        )
      }.to raise_error(ActiveRecord::RecordInvalid)
    end
  end

  describe 'Relationships' do
    it { should belong_to(:company_record).inverse_of(:managements) }
    it { should belong_to(:company_business_unit).inverse_of(:managements) }
    it { should have_many(:company_cencos).dependent(:destroy) }
  end
end
