require 'rails_helper'

RSpec.describe Company::TimetableTrabajandoWorkday, type: :model do

  describe "Validations" do
    it { should validate_presence_of :company_timetable }
    it { should validate_presence_of :trabajando_work_day }

    it 'should validate uniqueness of trabajando_work_day_id scoped to company_timetable id' do
      @company_timetable_trabajando_work_day = FactoryGirl.create(:company_timetable_trabajando_workday)
      should validate_uniqueness_of(:trabajando_work_day_id).scoped_to(:company_timetable_id)
      expect(@company_timetable_trabajando_work_day.errors.empty?).to be true

      cc2 = FactoryGirl.build(:company_timetable_trabajando_workday, company_timetable_id: @company_timetable_trabajando_work_day.company_timetable_id, trabajando_work_day_id: @company_timetable_trabajando_work_day.trabajando_work_day.id)
      expect(cc2.valid?).to be false
      expect(cc2.errors[:trabajando_work_day_id].empty?).to be false
      should validate_uniqueness_of(:trabajando_work_day_id).scoped_to(:company_timetable_id)
    end
  end

  describe "Create" do
    it "should create a valid object" do
      relation = FactoryGirl.create(:company_timetable_trabajando_workday)
      expect(relation).to be_valid
    end
  end
end
