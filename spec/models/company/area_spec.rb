require 'rails_helper'

RSpec.describe Company::Area, type: :model do
  context 'Validations over fields' do
    it 'name should be present' do
      should validate_presence_of :name
    end

    it 'name should be uniq' do
      should validate_uniqueness_of :name
    end
  end
end
