require 'rails_helper'

RSpec.xdescribe Company::Reference, type: :model do
  context 'RelationShips' do

    it 'should belong to applicant' do
      should belong_to(:applicant)
    end

    it 'should belong to company_employee' do
      should belong_to(:company_employee)
    end

    it 'should accept nested attributes for applicant' do
      should accept_nested_attributes_for(:applicant)
    end

    it "should have_many company_positions_references" do
      should have_many(:company_positions_references).dependent(:destroy)
    end

    it "should have_many positions" do
      should have_many(:positions).through(:company_positions_references)
    end
  end

  context 'Validations' do
    it 'should validate presence of applicant' do
      should validate_presence_of(:applicant)
    end

    it 'should validate presence of first_name' do
      should validate_presence_of(:first_name)
    end

    it 'should validate presence of last_name' do
      should validate_presence_of(:last_name)
    end

    it 'should validate presence of position' do
      should validate_presence_of(:position)
    end

    it 'should validate presence of email_domain' do
      should validate_presence_of(:email_domain)
    end

    it 'should validate presence of email_name' do
      should validate_presence_of(:email_name)
    end

    it 'email should be an email' do
      reference = FactoryGirl.create(:company_reference)
      expect(reference.email =~ /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i).to_not eq(nil)
    end
  end

  describe 'Confirm' do
    context 'is already confirmed' do
      it 'returns false' do
        company_reference = FactoryGirl.create(:company_reference, confirmed:true)
        expect(company_reference.confirm).to eq false
      end
    end

    context 'is not confirmed' do
      context 'user for employee email does not exist' do
        it "changes status to true and creates new user" do
          company_reference = FactoryGirl.create(:company_reference, company_employee: nil, confirmed:false)

          expect{
            company_reference.confirm
          }.to change(Company::Employee, :count).by(1).and \
                 change(User, :count).by(1).and \
                 change(Delayed::Job, :count).by(1)

            expect(company_reference.confirmed).to be true

            email = YAML.load(Delayed::Job.last.handler)
            expect(email.object).to eq Company::ReferenceMailer
            expect(email.method_name).to eq :send_notice_to_applicant
        end
      end

      context 'user for email employee exist' do
        let(:company_reference) { FactoryGirl.create(:company_reference, company_employee: nil, confirmed:false) }

        context 'company employee does not exist' do
          it 'changes the status to true and creates a company_employee' do
            FactoryGirl.create(:user, email: company_reference.email)

            expect{
              company_reference.confirm
            }.to change(Company::Employee, :count).by(1).and \
                 change(User, :count).by(0).and \
                 change(Delayed::Job, :count).by(1)

            expect(company_reference.confirmed).to be true

            email = YAML.load(Delayed::Job.last.handler)
            expect(email.object).to eq Company::ReferenceMailer
            expect(email.method_name).to eq :send_notice_to_applicant
          end
        end

        context 'company employee exist' do
          before :each do
            user = FactoryGirl.create(:user, email: company_reference.email)
            FactoryGirl.create(:company_employee, user: user)
          end

          it 'changes the status to true and sends an email' do
            expect{
              company_reference.confirm
            }.to change(Company::Employee, :count).by(0).and \
                 change(User, :count).by(0).and \
                 change(Delayed::Job, :count).by(1)

            expect(company_reference.confirmed).to be true

            email = YAML.load(Delayed::Job.last.handler)
            expect(email.object).to eq Company::ReferenceMailer
            expect(email.method_name).to eq :send_notice_to_applicant
          end
        end
      end
    end
  end

  context 'Create a valid Company::Reference' do
    it 'should send an email after create' do
      expect{
        FactoryGirl.create(:company_reference)
      }.to change(Delayed::Job, :count).by(1)

      email = YAML.load(Delayed::Job.last.handler)
      expect(email.object).to eq Company::ReferenceMailer
      expect(email.method_name).to eq :send_notice_to_employee
    end
  end
end
