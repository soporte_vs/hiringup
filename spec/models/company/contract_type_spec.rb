require 'rails_helper'

RSpec.describe Company::ContractType, type: :model do
  context 'Validations' do
    it 'name should be present' do
      should validate_presence_of :name
    end

    it 'name should be uniq' do
      should validate_uniqueness_of :name
    end

    it 'decription should be present' do
      should validate_presence_of :description
    end

    it 'decription should be uniq' do
      should validate_uniqueness_of :description
    end
  end
end
