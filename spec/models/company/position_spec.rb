# == Schema Information
#
# Table name: company_positions
#
#  id          :integer          not null, primary key
#  name        :string
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

require 'rails_helper'

RSpec.describe Company::Position, type: :model do
  context 'Validations over fields' do
    it 'name should be present' do
      should validate_presence_of :name
    end

    it 'name should be uniq' do
      should validate_uniqueness_of :name
    end

    it 'description should be present' do
      should validate_presence_of :description
    end
  end

  context 'RealationShips' do
    it 'should has many positions_cencos' do
      should have_many(:company_positions_cencos).inverse_of(:company_position).dependent(:destroy)
    end

    it 'should has many cencos' do
      should have_many(:company_cencos).through(:company_positions_cencos)
    end

    it 'should have many used_tags, tags' do
      should have_many(:used_tags).dependent(:destroy)
      should have_many(:tags)
    end

    it { should have_many :company_positions_references }
    it { should have_many(:company_references).through(:company_positions_references) }

  end

  context 'Create' do
    it 'should create a valid company_position' do
      expect(FactoryGirl.create :company_position).to be_valid
    end

    it 'should create an invalid company_position' do
      expect(FactoryGirl.build :company_position_invalid).to_not be_valid
    end
  end

end
