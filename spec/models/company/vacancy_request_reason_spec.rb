require 'rails_helper'

RSpec.describe Company::VacancyRequestReason, type: :model do
  context 'Validations' do
    it 'name should be present' do
      should validate_presence_of :name
    end

    it 'name should be uniq' do
      should validate_uniqueness_of :name
    end
  end

  context 'Build' do
    it 'should build an invalid' do
      expect(FactoryGirl.build(:company_vacancy_request_reason)).to be_valid
    end

    it 'should build an invalid' do
      expect(FactoryGirl.build(:company_vacancy_request_reason_invalid)).to_not be_valid
    end
  end
end
