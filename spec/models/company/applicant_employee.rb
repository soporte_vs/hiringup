require 'rails_helper'

RSpec.describe Company::ApplicantEmployee, type: :model do
  describe "index elasticsearch" do
    context 'index name' do
      it 'should be test', elasticsearch: true do |example|
        applicant = FactoryGirl.create(:company_employee).user.applicant
        expect(applicant.class.index_name).to eq "#{PREFIX_INDEX}-#{example.metadata[:block].object_id}"
      end
    end

    context 'mapping name' do
      it 'should be test-applicant' do
        applicant = FactoryGirl.create(:company_employee).user.applicant
        expect(applicant.class.document_type).to eq 'applicants'
      end
    end
  end
end
