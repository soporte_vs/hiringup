require 'rails_helper'

RSpec.describe Company::Estate, type: :model do
  context 'Validations' do
    it 'name should be present' do
      should validate_presence_of :name
    end

    it 'name should be uniq' do
      should validate_uniqueness_of :name
    end
  end

  context 'Create' do
    it 'should create a valid' do
      expect(FactoryGirl.create :company_estate).to be_valid
    end

    it 'should create an invalid' do
      expect(FactoryGirl.build :company_estate_invalid).to_not be_valid
    end
  end
end
