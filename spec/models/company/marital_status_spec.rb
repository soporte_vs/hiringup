require 'rails_helper'

RSpec.describe Company::MaritalStatus, type: :model do
  context 'Relationships' do
    it 'should has many people personal informations' do
      should have_many(:people_personal_informations).dependent(:nullify)
    end
  end
  context 'Validations' do
    it 'name should be present' do
      should validate_presence_of :name
    end

    it 'name should be uniq' do
      should validate_uniqueness_of :name
    end
  end
end
