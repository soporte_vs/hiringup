require 'rails_helper'

RSpec.describe Company::Timetable, type: :model do
  context 'Validations overs fields' do
    
    it 'name should be present' do
      should validate_presence_of :name
    end

    it 'name should be uniq' do
      should validate_uniqueness_of :name
    end
  end

  context 'Create' do
    it 'should create a valid company_timetable' do
      expect(FactoryGirl.create :company_timetable).to be_valid
    end

    it 'should create an invalid company_timetable' do
      expect(FactoryGirl.build :company_timetable_invalid).to_not be_valid
    end
  end
end
