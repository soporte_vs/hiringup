# == Schema Information
#
# Table name: worker_bases
#
#  id         :integer          not null, primary key
#  type       :string
#  user_id    :integer
#  created_at :datetime
#  updated_at :datetime
#

require 'rails_helper'

RSpec.describe Worker::Base, :type => :model do
  context "Validations Worker" do
    it "should belongs_to user" do
      should belong_to(:user)
    end

    it "should has a user" do
      should validate_presence_of(:user) 
    end
  end

  context "Create Worker" do
    it "should be valid" do
      expect(FactoryGirl.create(:worker_basis)).to be_valid
    end
  end

  context "Create a worker and invite her" do
    it "should create a new worker, user and send email to her" do
      expect(Delayed::Job.count).to eq(0)
      worker = FactoryGirl.create(:worker_basis_invitable)
      expect(Delayed::Job.count).to eq(1)
      expect(Delayed::Job.last.handler.include? worker.user.email).to be true
    end
  end

  context "Create a worker and associated to applicant email" do
    it "should create a new worker, user and not send email to her" do
      applicant = FactoryGirl.create(:applicant_bases)
      password_user = applicant.user.encrypted_password
      expect(Delayed::Job.count).to eq(0)
      worker = FactoryGirl.create(:worker_basis_invitable, user_attributes: {email: applicant.user.email})
      applicant.reload
      expect(applicant.user.worker.present?).to be true
      expect(Delayed::Job.count).to eq(0)
      expect(applicant.user.encrypted_password).to eq(password_user)
    end
  end
end
