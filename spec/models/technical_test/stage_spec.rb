require 'rails_helper'

RSpec.describe TechnicalTest::Stage, :type => :model do
  context "RelationShips" do
    it 'should has_one technical_test_report' do
      should have_one(:technical_test_report).dependent(:destroy)
    end
  end

  context 'Create' do
    it 'a valid technical_test' do
      technical_test_stage = FactoryGirl.create(:technical_test_stage)
      expect(technical_test_stage).to be_valid
      expect(technical_test_stage).to be_a(TechnicalTest::Stage)
    end
  end

  describe 'Not apply' do
    let(:not_apply_reason) { FactoryGirl.create(:stage_not_apply_reason) }

    context 'when action can be done' do
      it 'should create a Stage::NotApplying' do
        stage = FactoryGirl.create(:technical_test_stage)
        expect{
          stage.not_apply(not_apply_reason, FFaker::Lorem.sentence)
        }.to change(Stage::NotApplying, :count).by(1)
      end
    end

    context 'when action can not be done' do
      it 'should not create a Stage::NotApplying' do
        technical_test_report = FactoryGirl.create(:technical_test_report)
        stage = technical_test_report.technical_test_stage
        expect{
          stage.not_apply(not_apply_reason, FFaker::Lorem.sentence)
        }.to change(Stage::NotApplying, :count).by(0)

        expect(stage.errors[:not_applying]).to_not be_empty
      end
    end
  end
end
