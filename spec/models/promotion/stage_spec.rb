require 'rails_helper'

RSpec.describe Promotion::Stage, :type => :model do
  context 'Create' do
    it 'a valid promotion' do
      promotion_stage = FactoryGirl.create(:promotion_stage)
      expect(promotion_stage).to be_valid
      expect(promotion_stage).to be_a(Promotion::Stage)
    end
  end
end