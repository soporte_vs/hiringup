require 'rails_helper'

RSpec.describe GroupInterview::Report, type: :model do
  context 'RelationShips' do
    it 'should belong a group_interview_stage' do
      should belong_to :group_interview_stage
    end

    it {
      should have_many(:documents).dependent(:destroy)
    }
  end

  context 'Validations' do
    it 'observations should be present' do
      should validate_presence_of :observations
    end

    it 'group_interview_stage should be present' do
      should validate_presence_of :group_interview_stage
    end

    it { should allow_value(FFaker::Internet.email).for(:notified_to) }
    it { should_not allow_value(FFaker::Lorem.sentence).for(:notified_to) }
  end

  context 'Create' do
    it 'should create a valid' do
      expect(
        FactoryGirl.create(:group_interview_report)
      ).to be_valid
    end

    it 'should build an invalid' do
      expect(
        FactoryGirl.build(:group_interview_report_invalid)
      ).to_not be_valid
    end
  end
end
