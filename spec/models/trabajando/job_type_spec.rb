require 'rails_helper'

RSpec.describe Trabajando::JobType, type: :model do
  context 'RelationShips' do
    it {
      should have_many(:publications).class_name(Trabajando::Publication.to_s)
    }

    it { should have_many(:company_position_trabajando_job_types).dependent(:destroy) }

    it {
      should have_many(:company_positions)
              .through(:company_position_trabajando_job_types)
              .source(:company_position)
    }
  end

  context 'Validations' do
    it {
      should validate_presence_of(:name)
    }
    it {
      should validate_presence_of(:trabajando_id)
    }
    it {
      should validate_uniqueness_of(:name)
    }
    it {
      should validate_uniqueness_of(:trabajando_id)
    }
  end

  describe "Create" do
    it 'should have no company positions related' do
      job_type = FactoryGirl.create(:trabajando_job_type)
      expect(job_type.company_positions.count).to eq 0
    end

    it 'should have many company_positions related' do
      job_type = FactoryGirl.create(:trabajando_job_type)
      company_positions = FactoryGirl.create_list(:company_position, 5)
      job_type.company_positions << company_positions
      expect(job_type.company_positions.count).to eq 5
    end

    it 'should not permit to associate a company position to a trabajando job type twice' do
      job_type = FactoryGirl.create(:trabajando_job_type)
      company_position = FactoryGirl.create(:company_position)
      job_type.company_positions << company_position
      expect(job_type.company_positions.count).to eq 1

      expect{
        job_type.company_positions << company_position
      }.to raise_error(ActiveRecord::RecordInvalid)
    end
  end
end
