require 'rails_helper'

RSpec.describe Trabajando::Postulation, type: :model do
  describe 'RelationShips' do
    it { should belong_to(:trabajando_publication) }
    it { should belong_to(:applicant) }
    it { should have_one(:course_postulation).dependent(:destroy) }
  end

  describe 'Validation' do
    it { should validate_presence_of :trabajando_publication }
    it { should validate_presence_of :applicant }
    it { should validate_presence_of :postulation_date }
    it { should serialize :answers}
  end

  describe '#publication' do
    let(:trabajando_postulation) { FactoryGirl.create(:trabajando_postulation) }
    it {
      expect(trabajando_postulation.publication).to eq trabajando_postulation.trabajando_publication
    }
  end

  describe '#course' do
    let(:trabajando_postulation) { FactoryGirl.build(:trabajando_postulation) }
    it {
      expect(trabajando_postulation.course).to eq trabajando_postulation.trabajando_publication.course
    }
  end

  describe 'Accept' do
    context 'when postulation has been not accepted yet' do
      it 'should return true and errors to eq 0' do
        postulation = FactoryGirl.create(:trabajando_postulation, is_accepted: nil)
        expect(postulation.accept).to be true
        expect(postulation.course_postulation.errors.count).to eq 0
      end
    end

    context 'when postulation was acepted already' do
      it 'should retur false and assign errors' do
        postulation = FactoryGirl.create(:trabajando_postulation)
        postulation.update(is_accepted: true)
        expect(postulation.accept).to be false
        expect(postulation.course_postulation.errors.count).to eq 1
      end
    end
  end


  describe 'Reject' do
    context 'when postulation has been not accepted/rejected yet' do
      it 'should return true and errors to eq 0' do
        postulation = FactoryGirl.create(:trabajando_postulation, is_accepted: nil)
        expect(postulation.reject).to be true
        postulation.reload
        expect(postulation.is_accepted).to be false
        expect(postulation.course_postulation.errors.count).to eq 0
      end
    end

    context 'when postulation was rejected already' do
      it 'should return false and assign errors' do
        postulation = FactoryGirl.create(:trabajando_postulation)
        postulation.update(is_accepted: false)
        expect(postulation.reject).to be false
        expect(postulation.is_accepted).to be false
        expect(postulation.course_postulation.errors.count).to eq 1
      end
    end

    context 'when postulation was accepted already' do
      it 'should return false and assign errors' do
        postulation = FactoryGirl.create(:trabajando_postulation)
        postulation.update(is_accepted: true)
        expect(postulation.reject).to be false
        expect(postulation.is_accepted).to be true
        expect(postulation.course_postulation.errors.count).to eq 1
      end
    end
  end

  describe '#answers_with_hupe_format' do
    let(:trabajando_postulation) { FactoryGirl.build(:trabajando_postulation) }

    context 'when answers is not nil' do
      before :each do
        trabajando_postulation.answers = [{question: FFaker::Lorem.sentence, answer: FFaker::Lorem.sentence}]
      end

      it {
        expect(trabajando_postulation.answers_with_hupe_format).to eq trabajando_postulation.answers
      }
    end

    context 'when answers is nil' do
      it{
        expect(trabajando_postulation.answers_with_hupe_format).to eq []
      }
    end

  end

  describe 'Build a Trabajando::Postulation' do
    it {
      expect(FactoryGirl.build(:trabajando_postulation)).to be_valid
    }
    it {
      expect(FactoryGirl.build(:trabajando_postulation_invalid)).not_to be_valid
    }
  end

  let(:from_time){ Time.now - 3.days }

  describe '#self.get_remote_postulations_url' do
    let(:from_date){ I18n.l(from_time, format: '%Y%m%d') }
    let(:from_hour_date){ I18n.l(from_time, format: '%H:%M') }
    let(:trabajando_postulations_base_url) { TRABAJANDO_GET_POSTULATIONS }
    let(:get_remote_postulations_url){
      Trabajando::Postulation.get_remote_postulations_url(from_time)
    }

    it {
      expect(
        get_remote_postulations_url
      ).to eq "#{trabajando_postulations_base_url}&country=CL&from=#{from_date}&hour=#{from_hour_date}&pageNumber=1"
    }
  end

  describe '#self.get_remote_postulations' do
    let(:response_body_page_1) {
      File.read('spec/factories/trabajando/response_get_remote_postulations_page_1.json')
    }
    let(:response_body_page_2) {
      File.read('spec/factories/trabajando/response_get_remote_postulations_page_2.json')
    }
    let(:json_response_page_1){ JSON.parse(response_body_page_1) }
    let(:json_response_page_2){ JSON.parse(response_body_page_2) }

    let(:exclusive_postulations){
      json_response_page_1['data']['exclusivesPostulations'] + json_response_page_2['data']['exclusivesPostulations']
    }
    let(:shared_postulations){
      json_response_page_1['data']['sharedPostulations'] + json_response_page_2['data']['sharedPostulations']
    }
    let(:all_postulations) { exclusive_postulations + shared_postulations }

    before :each do
      pages = [1, 2]
      pages.each do |page_number|
        remote_postulations_url = Trabajando::Postulation.get_remote_postulations_url(from_time, page_number)
        stub_request(:get, remote_postulations_url).to_return(
          body: send("json_response_page_#{page_number}").to_json,
          status: 200
        )
        all_postulations.each do |postulation_data|
          trabajando_id = postulation_data['jobId']
          trabajando_applicant_id = postulation_data['applicantId']
          trabajando_publication = Trabajando::Publication.find_by(trabajando_id: trabajando_id)
          FactoryGirl.create(:trabajando_publication, trabajando_id: trabajando_id) unless trabajando_publication
          # Asumimos que el servicio funciona correctamente por lo cual creamos previamente los candidatos
          allow(Trabajando::Applicant).to receive(:get_trabajando_applicant)
            .with(trabajando_applicant_id)
            .and_return(FactoryGirl.create(:trabajando_applicant))
        end
      end
    end

    let(:get_remote_postulations){
      Trabajando::Postulation.get_remote_postulations(from_time)
    }

    it "should create all postulations imported from trabajando.com as Trabajando::Postulation" do
      expect{
        get_remote_postulations
      }.to change(Trabajando::Postulation, :count).by(all_postulations.count)
      all_postulations.each do |postulation_data|
        trabajando_postulation = Trabajando::Postulation
          .joins(:trabajando_publication).joins(:applicant)
          .find_by(
            trabajando_applicant_id: postulation_data['applicantId'],
            trabajando_publications: { trabajando_id: postulation_data['jobId'] }
          )
        expect(trabajando_postulation.applicant.present?).to be true
        expect(trabajando_postulation.answers.count).to eq (postulation_data['questions'].count / 2)
      end
    end

    context 'when there is not postulations' do
      let(:response_body_page_1) {
        response_data = JSON.parse(File.read('spec/factories/trabajando/response_get_remote_postulations_page_1.json'))
        response_data['data']['exclusivesPostulations'] = nil
        response_data['data']['sharedPostulations'] = nil
        response_data.to_json
      }
      let(:response_body_page_2) {
        response_data = JSON.parse(File.read('spec/factories/trabajando/response_get_remote_postulations_page_2.json'))
        response_data['data']['exclusivesPostulations'] = nil
        response_data['data']['sharedPostulations'] = nil
        response_data.to_json
      }
      let(:all_postulations){ [] }

      it 'should not load any Trabajando::Postulation' do
        expect{
          get_remote_postulations
        }.to change(Trabajando::Postulation, :count).by(0)
      end
    end
  end

  describe '#self.call_service_get_postulations' do

    let(:call_service_get_postulations){
      Trabajando::Postulation.send(:call_service_get_postulations,
        from_time, 1)
    }

    context 'when given a date that should return postulations' do
      let(:from_time){'08/05/2016 00:00'.to_time}
      let(:expected_schema){"trabajando/postulation/response_get_remote_postulations"}

      it 'responds with schema expected for postulations' do
      end
    end

    after :each do
      expect(call_service_get_postulations).to match_response_schema(expected_schema)
    end

  end
end
