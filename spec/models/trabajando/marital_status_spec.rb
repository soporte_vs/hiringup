require 'rails_helper'

RSpec.describe Trabajando::MaritalStatus, type: :model do
  context 'Validation' do
    it { should validate_presence_of :name }
    it { should validate_presence_of :trabajando_id }
    it { should validate_uniqueness_of :trabajando_id }
    it { should validate_uniqueness_of :name }
  end

  context 'Relationships' do
    it { should belong_to :company_marital_status }
  end
end
