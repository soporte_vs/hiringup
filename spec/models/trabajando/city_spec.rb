require 'rails_helper'

RSpec.describe Trabajando::City, type: :model do
  describe 'Validations' do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:trabajando_id) }
  end

  describe 'Relatopnships' do
    it { should belong_to(:hupe_city).class_name(Territory::City.to_s) }
  end
end
