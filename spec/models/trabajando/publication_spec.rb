require 'rails_helper'

RSpec.describe Trabajando::Publication, type: :model do
  let(:trabajando_publish_url){ TRABAJANDO_PUBLISH_JOB_URL }
  let(:trabajando_update_url){ TRABAJANDO_UPDATE_JOB_URL }

  let(:trabajando_id) { [*1..999999].sample }
  let(:response_body) {
    File.read('spec/factories/trabajando/response_publish_trabajando_publication.json')
  }
  let(:response_json){ JSON.parse(response_body) }
  before :each do
    response_json['data']['id'] = trabajando_id
    stub_request(:post, trabajando_publish_url).to_return(
      body: response_json.to_json,
      status: 200
    )
    stub_request(:put, trabajando_update_url).to_return(
      body: response_json.to_json,
      status: 200
    )
  end

  describe '#get_remote_url' do
    let(:trabajando_publication){
      FactoryGirl.create(:trabajando_publication, trabajando_id: 43545433)
    }
    let(:expected_url){
      "http://www.trabajando.cl/empleos/ofertas/#{trabajando_publication.trabajando_id}"
    }
    it {
      expect(trabajando_publication.get_remote_url).to eq expected_url
    }
  end

  describe 'RelationShips' do
    it { should belong_to :course }
    it { should belong_to(:area).class_name(Trabajando::Area.to_s) }
    it { should belong_to(:company_activity).class_name(Trabajando::CompanyActivity.to_s) }
    it { should belong_to(:job_type).class_name(Trabajando::JobType.to_s) }
    it { should belong_to(:work_day).class_name(Trabajando::WorkDay.to_s) }
    it { should belong_to(:applicant_profile).class_name(Trabajando::ApplicantProfile.to_s) }
    it { should belong_to(:education_level).class_name(Trabajando::EducationLevel.to_s) }
    it { should belong_to(:education_state).class_name(Trabajando::EducationState.to_s) }
    it { should belong_to(:software_level).class_name(Trabajando::SoftwareLevel.to_s) }
    it { should belong_to(:region).class_name(Trabajando::State.to_s) }
    it { should belong_to(:city).class_name(Trabajando::City.to_s) }
    it { should have_many(:postulations).class_name(Trabajando::Postulation.to_s).dependent(:destroy) }
  end

  describe 'Validations' do
    it { should validate_presence_of(:title) }
    it { should validate_presence_of(:description) }
    it { should validate_presence_of(:number_vacancies) }
    it { should validate_presence_of(:course) }
    it { should validate_presence_of(:area) }
    it { should validate_presence_of(:minimum_requirements) }
    it { should validate_presence_of(:job_type) }
    it { should validate_presence_of(:company_activity) }
    it { should validate_presence_of(:work_day) }
    it { should validate_presence_of(:applicant_profile) }
    it { should validate_presence_of(:education_level) }
    it { should validate_presence_of(:education_state) }
    it { should validate_presence_of(:software_level) }
    it { should validate_presence_of(:region) }
    it { should validate_presence_of(:city) }
    it { should validate_presence_of(:desactivate_at) }
  end

  context 'Create' do
    it 'job desactivate publication' do
      trabajando_publication = FactoryGirl.create(:trabajando_publication)
      expect(Delayed::Job.count).to eq(1)
    end
  end

  context 'States Publications' do
    let(:trabajando_publication) { FactoryGirl.create(:trabajando_publication) }
    it 'should create a trabajando publication opened' do
      expect(trabajando_publication.opened?).to be true
    end

    it 'should close trabajando_publication' do
      trabajando_publication.close!
      expect(trabajando_publication.opened?).to be false
      expect(trabajando_publication.closed?).to be true
    end

    context 'when course is closed' do
      before :each do
        course = trabajando_publication.course
        course.close!
        trabajando_publication.reload
      end

      it 'should not open trabajando_publication' do
        expect(trabajando_publication.closed?).to be false
        expect{
          trabajando_publication.open
        }.to raise_error(AASM::InvalidTransition)
      end
    end
  end

  describe '#deactivate' do
    let(:trabajando_publication){ FactoryGirl.build(:trabajando_publication) }
    let(:response_body) {
      File.read('spec/factories/trabajando/response_deactivate_publication.json')
    }
    let(:response_json){ JSON.parse(response_body) }

    before :each do
      stub_request(:post, TRABAJANDO_DEACTIVATE_JOB_URL).to_return(
        body: response_json.to_json,
        status: 200
      )
      trabajando_publication.save_and_publish
    end

    context 'when trabajando_publication is opened' do
      it 'should deactivate trabajando_publication and close it' do
        expect(trabajando_publication.deactivate).to be true
        expect(trabajando_publication.closed?).to be true
      end
    end

    context 'when trabajando_publication is closed' do
      before :each do
        trabajando_publication.close!
      end

      it 'should does not deactivate and return false' do
        expect(trabajando_publication.deactivate).to be false
        expect(trabajando_publication.closed?).to be true
      end
    end
  end

  describe '#activate' do
    let(:trabajando_publication){ FactoryGirl.build(:trabajando_publication) }
    let(:response_body) {
      File.read('spec/factories/trabajando/response_activate_publication.json')
    }
    let(:response_json){ JSON.parse(response_body) }

    before :each do
      stub_request(:post, TRABAJANDO_ACTIVATE_JOB_URL).to_return(
        body: response_json.to_json,
        status: 200
      )
      trabajando_publication.save_and_publish
    end

    context 'when trabajando_publication is opened' do
      it 'should not activate trabajando_publication' do
        expect(trabajando_publication.activate).to be false
        expect(trabajando_publication.opened?).to be true
      end
    end

    context 'when trabajando_publication is closed' do
      before :each do
        trabajando_publication.close!
      end

      it 'should does activate and return true' do
        expect(trabajando_publication.activate).to be true
        expect(trabajando_publication.opened?).to be true
      end
    end
  end

  describe '#save_and_publish' do
    let(:change_trabajando_counter){
      change(Trabajando::Publication, :count).by(1)
    }

    context 'when trabajando_publication is not persited yet' do
      let(:trabajando_publication) { FactoryGirl.build(:trabajando_publication) }


      it 'should create a new trabajando_publication and set trabajando_id to it' do
      end
    end

    context 'when trabajando_publication is persited' do
      let(:change_trabajando_counter){
        change(Trabajando::Publication, :count).by(0)
      }
      let(:trabajando_publication) { FactoryGirl.create(:trabajando_publication, trabajando_id: 0) }

      before :each do
        trabajando_publication
      end

      it 'should not create a new trabajando_publication and does not change trabajando_id' do
      end
    end

    after :each do
      expect{
        trabajando_publication.save_and_publish
      }.to change_trabajando_counter

      expect(trabajando_publication.trabajando_id.present?).to be true
      expect(trabajando_publication.trabajando_id).to eq trabajando_id
    end
  end

  describe '#publish' do
    let(:trabajando_publication) { FactoryGirl.build(:trabajando_publication) }
    let(:trabajando_publication_invalid) { FactoryGirl.build(:trabajando_publication_invalid) }

    context 'when trabajando_publication is invalid' do
      it {
        expect(trabajando_publication_invalid.publish).to eq nil
      }
    end

    context 'when trabajando_publication is valid' do
      it {
        response = trabajando_publication.publish
        expect(response.class).to eq HTTParty::Response
        expect(response.body).to eq response_json.to_json
      }
    end
  end

  describe 'testing call all services' do
    let(:trabajando_publication){ FactoryGirl.create(:trabajando_publication) }

    before :each do
      WebMock.reset!
      trabajando_publication.save_and_publish
    end

    describe '#call_publish_service' do
      it 'should send a trabajando_publication schema valid and get success response' do
        expect(
          trabajando_publication.json_trabajando_schema.to_json
        ).to match_response_schema("trabajando/publication/valid")
        response = trabajando_publication.publish
        response_json = JSON.parse(response.body)
        expect(response_json.to_json).to match_response_schema('trabajando/publication/valid_response_publish')
        expect(response_json['status']).to eq 'OK'
        expect(response_json['code']).to eq 0
        expect(response_json['message']).to eq nil
      end
    end

    describe '#call_deactivate_service' do
      it {
        response = trabajando_publication.send(:call_deactivate_service)
        expect(response.body).to match_response_schema("trabajando/publication/deactivate")
        json_response = JSON.parse(response.body)
        expect(json_response['status']).to eq 'OK'
        expect(json_response['code']).to eq 0
        expect(json_response['message']).to eq nil
        expect(json_response['data']['success']).to be true
      }
    end

    describe '#call_activate_service' do
      it {
        response = trabajando_publication.send(:call_activate_service)
        expect(response.body).to match_response_schema("trabajando/publication/activate")
        json_response = JSON.parse(response.body)
        expect(json_response['status']).to eq 'OK'
        expect(json_response['code']).to eq 0
        expect(json_response['message']).to eq nil
        expect(json_response['data']['success']).to be true
      }
    end
  end

  describe '#desactivation_scheduled' do
    let(:trabajando_publication){FactoryGirl.create(:trabajando_publication)}
    let(:response_body) {
      File.read('spec/factories/trabajando/response_deactivate_publication.json')
    }
    let(:response_json){ JSON.parse(response_body) }

    before :each do
      stub_request(:post, TRABAJANDO_DEACTIVATE_JOB_URL).to_return(
        body: response_json.to_json,
        status: 200
      )
      trabajando_publication.save_and_publish
    end
    
    context 'when publication is open' do
      it 'should publication close' do
        expect(trabajando_publication.opened?).to be true
        trabajando_publication.desactivation_scheduled
        expect(trabajando_publication.closed?).to be true
      end
    end

    context 'when publication is close' do
      before :each do
        trabajando_publication.close!
      end
      
      it 'should not change publication' do
        expect(trabajando_publication.closed?).to be true
        trabajando_publication.desactivation_scheduled
        expect(trabajando_publication.closed?).to be true
      end
    end
  end

end
