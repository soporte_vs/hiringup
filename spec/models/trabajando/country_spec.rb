require 'rails_helper'

RSpec.describe Trabajando::Country, type: :model do
  describe 'Validations' do
    it { should validate_presence_of(:name) }

    it { should validate_uniqueness_of(:name) }

    it { should validate_presence_of(:trabajando_id) }
  end

  describe 'Relatopnships' do
    it { should belong_to(:country).class_name(Territory::Country.to_s) }
  end
end
