# coding: utf-8
require 'rails_helper'

RSpec.describe Trabajando::Applicant, type: :model do
  let(:trabajando_applicant_example_id) { 'm3wxW4JcInPdQw9AYVqEYA==' }
  let(:trabajando_applicant_url) {
    Trabajando::Applicant.get_trabajando_applicant_url(trabajando_applicant_example_id)
  }
  let(:response_body) {
    File.read('spec/factories/trabajando/response_get_trabajando_applicant.json')
  }
  let(:response_data){ JSON.parse(response_body)['data']}
  let(:personal_information_data){ response_data['personalInfo']}
  let(:laboral_experiences_data){ response_data['workExperience']}
  let(:academics_background_data){ response_data['studies']}
  let(:degrees_data) { academics_background_data['higherEducation'] }

  describe '#get_user' do
    let(:primary_email) { FFaker::Internet.email }
    let(:secondary_email) { FFaker::Internet.email }
    let(:emails){ personal_information_data['emails'] }
    let(:user) { Trabajando::Applicant.send(:get_user, emails) }

    before :each do
      emails['primaryEmail'] = primary_email
      emails['secondaryEmail'] = secondary_email
    end

    context 'when rut is present in personal_information_data' do
      let(:rut){ personal_information_data['idNumberFormat'].gsub('.', '') }
      let(:get_user) { Trabajando::Applicant.send(:get_user, emails, rut) }

      before :each do
        chile = Territory::Country.where(name: "Chile").first_or_create
        identification_document_type = FactoryGirl.create(
          :people_identification_document_type,
          country: chile,
          name: "RUT",
          validation_regex: "^([0-9]+-[0-9Kk])$"
        )
        FactoryGirl.create(:user).personal_information.update(
          identification_document_number: rut,
          identification_document_type: identification_document_type
        )
      end

      it 'should return a persisted user' do
        expect(get_user).to be_persisted
        expect(get_user).to be_kind_of User
        expect(get_user.personal_information).to be_present
        expect(get_user.personal_information.identification_document_number).to be_present
        expect(get_user.personal_information.identification_document_type).to be_present
        expect(get_user.personal_information.identification_document_type.codename).to eq "rut"
      end
    end

    context 'when primary and secondary email exist' do
      before :each do
        FactoryGirl.create(:user, email: primary_email)
        FactoryGirl.create(:user, email: secondary_email)
      end

      it 'should return persisted user related with primaryEmail' do
      end

      context 'but with email in upcase' do
        before :each do
          emails['primaryEmail'] = primary_email.upcase
          emails['secondaryEmail'] = secondary_email.upcase
        end

        it 'should return persisted user related with primaryEmail' do
        end
      end

      after :each do
        expect(user.persisted?).to be true
        expect(user.email).to eq primary_email
      end
    end

    context 'when primary_email user email does not exist in bd' do
      it 'should return a new instance of User' do
        expect(user).to be_a_new User
      end

      context 'but secondary email exist' do
        before :each do
          FactoryGirl.create(:user, email: secondary_email)
        end

        it 'should return a persisted User' do
          expect(user.persisted?).to be true
        end
      end
    end
  end

  describe '#extract_personal_information' do
    let(:first_name) { FFaker::Name.first_name }
    let(:last_name) { FFaker::Name.last_name }
    let(:birth_date) { (Date.today - 18.years).to_s }
    let(:address) { FFaker::Address.street_address }
    let(:sex) { 'Female' }
    let(:rut) { '123.456-k' }
    let(:rut_without_points) { '123456-k' }
    let(:trabajando_marital_status) {
      FactoryGirl.create(:trabajando_marital_status,
        company_marital_status: FactoryGirl.create(:company_marital_status)
      )
    }

    before :each do
      personal_information_data['firstName'] = first_name
      personal_information_data['lastName'] = last_name
      personal_information_data['birthDate'] = birth_date
      personal_information_data['address'] = address
      personal_information_data['maritalStatus']['id'] = trabajando_marital_status.trabajando_id
      personal_information_data['gender']['description'] = 'Femenino'

      # se asigna el rut con puntos, después se prueba que quede sin puntos
      personal_information_data['idNumberFormat'] = rut
    end

    context 'when user is already persisted' do
      let(:user) { FactoryGirl.create(:user) }
      let(:initial_personal_information){
        FactoryGirl.create(:people_personal_information, user: user)
      }

      before :each do
        user.personal_information.update(sex: 'Male', company_marital_status: FactoryGirl.create(:company_marital_status))
      end

      context 'and it has personal_information related' do
        let(:personal_information){
          personal_information = Trabajando::Applicant.send(
            :extract_personal_information,
            user,
            personal_information_data
          )
        }

        it 'should not update personal information' do
          expect(personal_information.first_name).not_to eq first_name
          expect(personal_information.last_name).not_to eq last_name
          expect(personal_information.birth_date.to_s).not_to eq birth_date
          expect(personal_information.address).not_to eq address
          expect(personal_information.sex).not_to eq sex
          expect(personal_information.identification_document_number).not_to eq rut_without_points
          expect(personal_information.company_marital_status).not_to eq trabajando_marital_status.company_marital_status
        end

        context 'but personal_information attributes are empty' do
          before :each do
            initial_personal_information.first_name = ''
            initial_personal_information.last_name = ''
            initial_personal_information.birth_date = nil
            initial_personal_information.address = ''
            initial_personal_information.sex = ''
            initial_personal_information.identification_document_number = nil
            initial_personal_information.company_marital_status = nil
            initial_personal_information.save
          end

          it 'should update empty attributes' do
            expect(personal_information.first_name).to eq first_name
            expect(personal_information.last_name).to eq last_name
            expect(personal_information.birth_date.to_s).to eq birth_date
            expect(personal_information.address).to eq address
            expect(personal_information.sex).to eq sex
            expect(personal_information.identification_document_number).to eq rut_without_points
            expect(personal_information.company_marital_status). to eq trabajando_marital_status.company_marital_status
          end
        end

        after :each do
          expect(personal_information.persisted?).to be true
        end
      end
    end

    context 'when user is a new instance' do
      let(:user) {
        FactoryGirl.build(:user, personal_information_attributes: {})
      }

      it 'should set a new personal information with data' do
        personal_information = Trabajando::Applicant.send(
          :extract_personal_information,
          user,
          personal_information_data
        )
        expect(personal_information).to be_a_new People::PersonalInformation
        expect(personal_information.first_name).to eq first_name
        expect(personal_information.last_name).to eq last_name
        expect(personal_information.birth_date.to_s).to eq birth_date
        expect(personal_information.address).to eq address
      end
    end
  end

  describe '#extract_laboral_experiences' do
    let(:user) { FactoryGirl.create(:user) }
    let(:extract_laboral_experiences){
      Trabajando::Applicant.send(
        :extract_laboral_experiences,
        user,
        laboral_experiences_data
      )
    }

    before :each do
      laboral_experiences_data.each do |le|
        le['companyName'] = FFaker::Lorem.sentence
        le['jobPosition'] = FFaker::Lorem.sentence
        le['achievements'] = FFaker::Lorem.sentence
        le['fromDate'] = (Date.today - 1.years).to_s
        le['toDate'] = Date.today.to_s
      end
    end

    context 'when user has not a professional_information and laboral_experiences' do
      it 'should associate new laboral_experiences to user' do
        extract_laboral_experiences
        laboral_experiences = user.professional_information.laboral_experiences
        expect(laboral_experiences.length).to eq laboral_experiences_data.length
        laboral_experiences_data.each do |le|
          laboral_experience = laboral_experiences.select{ |experience|
            experience.position == le['jobPosition'] && experience.company == le['companyName']
          }.first
          expect(laboral_experience.since_date).to eq le['fromDate'].to_date
          expect(laboral_experience.until_date).to eq le['toDate'].to_date
          expect(laboral_experience.description).to eq le['achievements']
        end
      end
    end

    context 'when user already has the same laboral_experiences' do
      before :each do
        user.build_professional_information
        user.save
        laboral_experiences_data.each do |le|
          user.professional_information.laboral_experiences.create(
            position: le['jobPosition'],
            company: le['companyName'],
            description: le['achievements'],
            since_date: le['fromDate'].to_date,
            until_date: le['toDate'].to_date
          )
        end
        expect(user.professional_information.laboral_experiences.count).to eq laboral_experiences_data.count
      end

      it 'should not associate new laboral_experiences to user' do
        expect{
          extract_laboral_experiences
        }.to change(user.professional_information.laboral_experiences, :length).by(0)
      end
    end
  end

  describe '#get_institution' do
    let(:degree_data){ degrees_data.sample }
    let(:get_institution_expect){}
    let(:institution_data){
      degree_data['institution']
    }
    let(:country_data){ degree_data['country'] }
    let(:get_institution){
      Trabajando::Applicant.send(:get_institution, degree_data)
    }
    context 'when institution_data is nil' do
      let(:get_institution_expect){
        expect(get_institution).to be nil
      }

      before :each do
        degree_data['institution'] = nil
      end

      it 'should return a nil object' do
      end
    end

    context 'when institution_data is not nil' do
      let!(:trabajando_institution_country){
        FactoryGirl.create(:trabajando_country,
          trabajando_id: degree_data['country']['id']
        )
      }
      let(:education_institution_expected_count){
        change(Education::Institution, :count).by(0)
      }
      let(:get_institution_expect){
        expect{
          education_institution = get_institution
          expect(education_institution.name).to eq institution_data['name']
          expect(education_institution.country).to eq trabajando_institution_country.country
          expect(education_institution.custom).to be true
        }.to education_institution_expected_count

      }
      context 'and institution_data represent a not persisted Education::Institution' do
        let(:education_institution_expected_count){
          change(Education::Institution, :count).by(1)
        }
        it 'should create a new Education::Institution as custom' do
        end
      end

      context 'and institution_data represent a persisted Education::Institution' do
        before :each do
          FactoryGirl.create(:education_institution,
            name: institution_data['name'],
            country: trabajando_institution_country.country,
            custom: true
          )
        end
        let(:education_institution_expected_count){
          change(Education::Institution, :count).by(0)
        }
        it 'should not create a new Education::Institution as custom' do
        end
      end
    end

    after :each do
      get_institution_expect
    end
  end

  describe '#get_career' do
    let(:degree_data){ degrees_data.sample }
    let(:get_career){
      Trabajando::Applicant.send(:get_career, degree_data)
    }
    let(:career_name_expected){ FFaker::Lorem.sentence }

    context 'when Education::Career is not persisted yet' do
      let(:expected_education_career_count){
        change(Education::Career, :count).by(1)
      }

      context 'and career_data is nil but minor attribute not' do
        let(:career_name_expected){ degree_data['minor'] }

        before :each do
          degree_data['career'] = nil
        end

        it 'should create and return an Education::Career with value of minor attribute' do
        end
      end

      context 'and career_data is not nil' do
        before :each do
          degree_data['career']['nameAlias'] = career_name_expected
        end

        it 'should return and create Education::Career where name is value of nameAlias attribute' do
        end

        context 'but value of nameAlias attribute is empty or nil' do
          before :each do
            degree_data['career']['nameAlias'] = nil
            degree_data['career']['name'] = career_name_expected
          end

          it 'should return and create Education::Career where name is value of name attribute' do
          end
        end
      end
    end

    context 'when Education::Career is persisted already' do
      let(:expected_education_career_count){
        change(Education::Career, :count).by(0)
      }
      before :each do
        FactoryGirl.create(:education_career, name: career_name_expected)
        degree_data['career']['nameAlias'] = career_name_expected
      end

      it 'should return but no create Education::Career' do
      end
    end

    after :each do
      expect{
        education_career = get_career
        expect(education_career.name).to eq career_name_expected
      }.to expected_education_career_count
    end
  end

  describe '#extract_university_degree' do
    let(:user) { FactoryGirl.create(:user) }
    before :each do
      user.build_professional_information.save
    end
    let(:expected_people_degree) { }
    let(:degree_data){ degrees_data.sample }
    let(:extract_university_degree) {
      Trabajando::Applicant.send(:extract_university_degree,
        user, degree_data
      )
    }
    let(:people_degree){ extract_university_degree }

    context 'when degree_data represent a not persisted degree' do
      let(:is_a_new_expect){
        expect(people_degree).to be_a_new People::Degree
      }
      it 'should return a new instance of People::Degree' do
      end
    end

    context 'when degree_data represent a degree already persisted' do
      let(:is_a_new_expect){
        expect(people_degree).not_to be_a_new People::Degree
      }
      let(:education_career){
        Trabajando::Applicant.send(:get_career, degree_data)
      }
      let(:education_institution){
        Trabajando::Applicant.send(:get_institution, degree_data)
      }

      before :each do
        FactoryGirl.create(:people_degree,
          career: education_career,
          institution: education_institution
        )
      end

      it 'should not return a new instance of People::Degree' do
      end
    end

    after :each do
      is_a_new_expect
      expect(people_degree).to be_a People::Degree
      expect(people_degree.culmination_date).to eq "#{degree_data['graduationyear']}-12-31".to_date
      expect(people_degree.institution).to be_a Education::Institution
      expect(people_degree.career).to be_a Education::Career
      expect(people_degree.professional_information).to eq user.professional_information
      expect(people_degree.professional_information.user).to eq user
    end
  end

  describe '#extract_universities_degrees' do
    let(:user){ FactoryGirl.create(:user) }
    let(:extract_universities_degrees){
      Trabajando::Applicant.send(:extract_universities_degrees, user, degrees_data)
    }

    it "should return an array with all People::Degrees" do
      degrees = extract_universities_degrees
      expect(user.professional_information.degrees.length).to eq degrees.length
      degrees.each do |degree|
        expect(degree).to be_a_new  People::Degree
      end
    end
  end

  describe '#get_trabajando_applicant' do
    before :each do
      response_path = 'spec/factories/trabajando/response_get_trabajando_applicant.json'
      stub_request(:any, trabajando_applicant_url).to_return(
        body: File.new(response_path),
        status: 200
      )
    end

    context 'when user email exists' do
      let(:emails) { personal_information_data['emails'] }
      let(:primary_email) { emails['primaryEmail'] }
      let(:user) { FactoryGirl.create(:user, email: primary_email) }

      before :each do
        user
      end

      it 'should create all data' do
        trabajando_applicant = Trabajando::Applicant.get_trabajando_applicant(trabajando_applicant_example_id)
        expect{
          trabajando_applicant.save(validate: false)
        }.to change(User, :count).by(0).and \
             change(Trabajando::Applicant, :count).by(1).and \
             change(People::PersonalInformation, :count).by(0).and \
             change(People::ProfessionalInformation, :count).by(1).and \
             change(People::LaboralExperience, :count).by(laboral_experiences_data.count).and \
             change(People::Degree, :count).by(degrees_data.count).and \
             change(Delayed::Job, :count).by(0)
      end

      context 'and user has no personal information' do
        let(:user) { FactoryGirl.create(:user_without_personal_information, email: primary_email) }
        let!(:existent_applicant) { FactoryGirl.create(:applicant_bases, user: user) }

        it "should assign user to trabajando applicant with received data from Trabajando" do
          expect(Applicant::Base.find_by(user: user)).to be_a Applicant::Base
          expect(Applicant::Base.find_by(user: user)).to eq existent_applicant
          expect(user.personal_information).to be nil
          trabajando_applicant = Trabajando::Applicant.get_trabajando_applicant(trabajando_applicant_example_id)
          expect{
            trabajando_applicant.save(validate: false)
          }.to change(User, :count).by(0).and \
               change(Trabajando::Applicant, :count).by(0).and \
               change(People::PersonalInformation, :count).by(1).and \
               change(People::ProfessionalInformation, :count).by(1).and \
               change(People::LaboralExperience, :count).by(laboral_experiences_data.count).and \
               change(People::Degree, :count).by(degrees_data.count).and \
               change(Delayed::Job, :count).by(0)

          expect(trabajando_applicant.user).to eq user
          expect(trabajando_applicant).to eq existent_applicant
          expect(trabajando_applicant.user.personal_information).to be_present
          expect(trabajando_applicant.user.personal_information.first_name).to eq personal_information_data["firstName"]
          expect(trabajando_applicant.user.personal_information.last_name).to eq personal_information_data["lastName"]
        end
      end

      context 'and user has an applicant associated already' do
        let(:trabajando_applicant) {
          Trabajando::Applicant.get_trabajando_applicant(trabajando_applicant_example_id)
        }

        context 'and the applicant is a Trabajando::Applicant' do
          let(:expected_integration_data){
            expect(trabajando_applicant.integration_data).to be_a(Hash)
            expect(trabajando_applicant.integration_data).to include(:source, :source_id, :data, :imported_at)
          }

          before :each do
            FactoryGirl.create(:trabajando_applicant, user: user)
          end

          it 'should not create User and Trabajando::Applicant but must set integration data ' do
            expect(trabajando_applicant).to be_a Trabajando::Applicant
          end
        end

        context 'and the applicant is a not Trabajando::Applicant' do
          let(:expected_integration_data){
            expect(trabajando_applicant.integration_data).to be nil
          }

          before :each do
            FactoryGirl.create(:applicant_bases, user: user)
          end

          it 'should not create an User and Trabajando::Applicant and does set integration data' do
            expect(trabajando_applicant).to_not be_a Trabajando::Applicant
          end
        end

        after :each do
          expect{
            trabajando_applicant.save(validate: false)
          }.to change(User, :count).by(0).and \
                 change(Trabajando::Applicant, :count).by(0).and \
                 change(People::PersonalInformation, :count).by(0)
                 change(People::ProfessionalInformation, :count).by(0).and \
                 change(People::LaboralExperience, :count).by(laboral_experiences_data.count).and \
                 change(Delayed::Job, :count).by(0)

          expected_integration_data
        end
      end
    end

    context 'when user email does not exist' do
      it 'should create User and load all data ' do
        trabajando_applicant = Trabajando::Applicant.get_trabajando_applicant(trabajando_applicant_example_id)
        expect(trabajando_applicant).to be_a_new Trabajando::Applicant
        expect{
          trabajando_applicant.save(validate: false)
        }.to change(User, :count).by(1).and \
             change(Trabajando::Applicant, :count).by(1).and \
             change(People::PersonalInformation, :count).by(1).and \
             change(People::ProfessionalInformation, :count).by(1).and \
             change(People::LaboralExperience, :count).by(laboral_experiences_data.count).and \
             change(Delayed::Job, :count).by(1)
      end
    end
  end

  describe '#get_marital_status' do
    let(:marital_status_data) { personal_information_data['maritalStatus'] }
    let(:get_marital_status) {
      Trabajando::Applicant.send(:get_marital_status, marital_status_data)
    }

    context 'when marital status data is empty' do
      let(:marital_status_data) { {} }

      it 'should return nil' do
        expect(get_marital_status).to be nil
      end
    end

    context 'when marital status data is not empty' do
      context 'and trabajando marital status is created' do
        let(:trabajando_marital_status) {
          FactoryGirl.create(:trabajando_marital_status, company_marital_status: nil)
        }

        before :each do
          marital_status_data['id'] = trabajando_marital_status.trabajando_id
          marital_status_data['name'] = trabajando_marital_status.name
        end

        context 'but not related to a Company::MaritalStatus' do
          it 'should return nil' do
            expect(get_marital_status).to be nil
          end
        end

        context 'and related a one Compant::MaritalStatus' do
          let(:company_marital_status) { FactoryGirl.create(:company_marital_status) }

          before :each do
            trabajando_marital_status.update(company_marital_status: company_marital_status)
          end

          it 'should return company_marital_status' do
            expect(get_marital_status).to eq company_marital_status
          end
        end
      end

      context 'and trabajando marital status is not created' do
        it 'should create a new Trabajando::MaritalStatus' do
          expect{
            expect(get_marital_status).to be nil
          }.to change(Trabajando::MaritalStatus, :count).by(1)
        end
      end
    end
  end

  describe '#get_gender' do
    let(:gender_data) { personal_information_data['gender'] }
    let(:get_gender) {
      Trabajando::Applicant.send(:get_gender, gender_data)
    }

    context 'when gender data is empty' do
      let(:gender_data) { {} }

      it 'should return blank' do
        expect(get_gender).to eq ''
      end
    end

    context 'when gender data is not empty' do
      context 'and description is Masculino' do
        before :each do
          gender_data['description'] = 'Masculino'
        end

        it 'should return Male' do
          expect(get_gender).to eq 'Male'
        end
      end

      context 'and description is Femenino' do
        before :each do
          gender_data['description'] = 'Femenino'
        end

        it 'should return Female' do
          expect(get_gender).to eq 'Female'
        end
      end

      context 'and description is not Masculino or Femenino' do
        before :each do
          gender_data['description'] = 'OTRO'
        end

        it 'should return blank' do
          expect(get_gender).to eq ''
        end
      end
    end
  end

  describe '#get_city' do
    let(:city_data) { personal_information_data['city'] }
    let(:get_city) { Trabajando::Applicant.send(:get_city, city_data) }

    context 'when city data is empty' do
      let(:city_data) { {} }

      it 'should return nil' do
        expect(get_city).to be nil
      end
    end

    context 'when city data is not empty' do
      context 'and trabajando city is created' do
        let(:trabajando_city) { FactoryGirl.create(:trabajando_city) }

        context 'and it is related to a territory city' do
          before :each do
            city_data['id'] = trabajando_city.trabajando_id
          end

          it 'should return territory_city' do
            expect(get_city).to eq trabajando_city.hupe_city
          end
        end

        context 'but trabajando city is no related to a territory city' do
          let(:trabajando_city) {
            FactoryGirl.create(:trabajando_city, territory_city: nil)
          }

          it 'should return nil' do
            expect(get_city).to be nil
          end
        end
      end
    end
  end

  describe '#get_cellphone' do
    let(:phones_data) { personal_information_data['phoneNumbers'] }
    let(:get_cellphone) {
      Trabajando::Applicant.send(:get_cellphone, phones_data)
    }
    let(:cellphone) { FFaker::PhoneNumberAU.international_phone_number }

    context 'when phones_data is not empty' do
      before :each do
        phones_data.select{ |phone_data|
          phone_data['place'] == 'mobile'
        }.first['number'] = cellphone
      end

      it {
        expect(get_cellphone).to eq cellphone
      }

      context 'but cellphone is empty' do
        before :each do
          phones_data.select{ |phone_data|
            phone_data['place'] == 'mobile'
          }.first['number'] = ''
        end

        it {
          expect(get_cellphone).to eq ''
        }
      end
    end

    context 'when phones_data is empty' do
      let(:phones_data){ [] }

      it {
        expect(get_cellphone).to eq ''
      }
    end
  end

  describe '#get_landline' do
    let(:phones_data) { personal_information_data['phoneNumbers'] }
    let(:get_landline) {
      Trabajando::Applicant.send(:get_landline, phones_data)
    }
    let(:landline) { FFaker::PhoneNumberAU.international_home_work_phone_number }

    context 'when phones_data is not empty' do
      before :each do
        phones_data.select{ |phone_data|
          phone_data['place'] == 'home'
        }.first['number'] = landline
      end

      it {
        expect(get_landline).to eq landline
      }

      context 'but landline is empty' do
        before :each do
          phones_data.select{ |phone_data|
            phone_data['place'] == 'home'
          }.first['number'] = ''
        end

        it {
          expect(get_landline).to eq ''
        }
      end
    end

    context 'when phones_data is empty' do
      let(:phones_data){ [] }

      it {
        expect(get_landline).to eq ''
      }
    end
  end
end
