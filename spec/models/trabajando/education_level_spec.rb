require 'rails_helper'

RSpec.describe Trabajando::EducationLevel, type: :model do
  describe 'RelationsShips' do
    it { should have_many(:publications).class_name(Trabajando::Publication.to_s) }
  end

  describe 'Validation' do
    it { should validate_presence_of :name }
    it { should validate_presence_of :trabajando_id }
    it { should validate_uniqueness_of :trabajando_id }
    it { should validate_uniqueness_of :name }
  end
end
