require 'rails_helper'

RSpec.describe Trabajando::WorkDay, type: :model do
  describe 'RelationShips' do
    it { should have_many(:publications)
                 .class_name(Trabajando::Publication.to_s) }

    it { should have_many(:company_timetable_trabajando_workdays)
                 .class_name("Company::TimetableTrabajandoWorkday")
                 .with_foreign_key(:trabajando_work_day_id) }

    it { should have_many(:company_timetables)
                 .through(:company_timetable_trabajando_workdays)
                 .source(:company_timetable) }
  end

  describe 'Validations' do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:trabajando_id) }
    it { should validate_uniqueness_of(:name) }
    it { should validate_uniqueness_of(:trabajando_id) }
  end

  describe "Create" do
    it "should create trabajando work day without company timetables" do
      wd = FactoryGirl.create(:trabajando_work_day)
      expect(wd).to be_valid
      expect(wd).to be_persisted
      expect(wd.company_timetables).to be_empty
    end

    it "should create trabajando work day with company timetables" do
      wd = FactoryGirl.create(:trabajando_work_day)
      expect(wd).to be_valid
      expect(wd).to be_persisted
      wd.company_timetable_ids = FactoryGirl.create_list(:company_timetable, 3).map(&:id)
      expect(wd.company_timetables).not_to be_empty
      expect(wd.company_timetables.count).to eq 3
    end
  end
end
