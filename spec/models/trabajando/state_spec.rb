require 'rails_helper'

RSpec.describe Trabajando::State, type: :model do
  describe 'Validations' do
    it { should validate_presence_of(:name) }

    it { should validate_presence_of(:trabajando_id) }
  end

  describe 'Relatopnships' do
    it { should belong_to(:hupe_state).class_name(Territory::State.to_s) }
  end
end
