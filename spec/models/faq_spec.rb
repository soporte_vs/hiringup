require 'rails_helper'

RSpec.describe Faq, type: :model do

  context 'Validations' do
    it 'question should be present' do
      should validate_presence_of :question
    end

    it 'answer should be present' do
      should validate_presence_of :answer
    end
  end

  context 'Create' do
    it 'should create a valid' do
      expect(FactoryGirl.create :faq).to be_valid
    end

    it 'should create an invalid' do
      expect(FactoryGirl.build :faq_invalid).to_not be_valid
    end
  end
end
