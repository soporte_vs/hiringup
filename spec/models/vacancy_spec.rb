# == Schema Information
#
# Table name: vacancies
#
#  id                                 :integer          not null, primary key
#  course_id                          :integer
#  closed_at                          :datetime
#  observations                       :text
#  created_at                         :datetime         not null
#  updated_at                         :datetime         not null
#  company_positions_cenco_id :integer
#

require 'rails_helper'

RSpec.describe Vacancy, type: :model do
  context 'Relationship on model' do
    it 'should belong_to course' do
      should belong_to(:course).inverse_of(:vacancies)
    end

    it 'should belong_to company_positions_cenco' do
      should belong_to :company_positions_cenco
    end

    it 'should has_one agreement' do
      should have_one(:agreement)
    end

    it { should have_many(:offer_letters)
                  .class_name('Offer::Letter')
                  .inverse_of(:vacancy) }

    it 'should belong_to hiring_request_record' do
      should belong_to(:hiring_request_record)
    end

    it 'should belong_to company_vacancy_request_reason' do
      should belong_to(:company_vacancy_request_reason)
    end
  end

  context 'Validations' do
    it 'should have an attribute accesor as cenco_id' do
      vacancy = Vacancy.new
      expect{
        vacancy.cenco_id
      }.to_not raise_error
    end

    it 'company_positions_cenco should be present' do
      should validate_presence_of :company_positions_cenco
    end

    it 'validates company_vacancy_request_reason is eq to hiring_request_record.company_vacancy_request_reason' do
      hiring_request_record = FactoryGirl.create(:hiring_request_record, number_vacancies: 1)
      expect{
        hiring_request_record.accept!
      }.to change(hiring_request_record.vacancies, :count).by(1)
      vacancy = hiring_request_record.vacancies.first
      expect(hiring_request_record.company_vacancy_request_reason).to eq (vacancy.company_vacancy_request_reason)
      vacancy.update(company_vacancy_request_reason: FactoryGirl.create(:company_vacancy_request_reason))
      expect(vacancy.errors[:company_vacancy_request_reason_id].any?).to be true
    end

    it 'validates company_positions_cenco.company_position is the same as course.company_position on create' do
      vacancy = FactoryGirl.create(:vacancy)
      course = vacancy.course
      expect(vacancy.company_positions_cenco.company_position).to eq course.company_position
    end

    it 'validates company_positions_cenco.company_position is the same as course.company_position on update' do
      vacancy = FactoryGirl.create(:vacancy)
      course = FactoryGirl.create(:course_with_new_company_position)

      expect(course.company_position).to_not eq vacancy.company_positions_cenco.company_position
      expect(vacancy.update(course: course)).to be false
      expect(vacancy.errors[:course_id].length).to eq 1
      expect(
        vacancy.errors[:course_id].first
      ).to eq I18n.t('activerecord.errors.models.vacancy.attributes.course_id.inconsistent_company_position')
    end

    it 'validates company_positions_cenco is the same as hiring_request_record.company_positions_cenco on update' do
      hiring_request_record = FactoryGirl.create(:hiring_request_record, number_vacancies: 1)
      company_positions_cenco = FactoryGirl.create(:company_positions_cenco)
      hiring_request_record.accept!
      hiring_request_record.reload
      vacancy = hiring_request_record.vacancies.first
      expect(vacancy.update(company_positions_cenco: company_positions_cenco)).to be false
      expect(
        vacancy.errors[:company_positions_cenco].first
      ).to eq I18n.t('activerecord.errors.models.vacancy.attributes.company_positions_cenco.inconsistent_with_hiring_request_record')
    end
  end

  context 'check valid course' do
    it 'should be ok' do
      vacancy = FactoryGirl.create(:vacancy_without_course)
      company_position = vacancy.company_positions_cenco.company_position
      course = FactoryGirl.create(:course, company_position: company_position)
      expect(vacancy.update(course: course)).to be true
    end

    it 'should not be ok' do
      agreement = FactoryGirl.create(:engagement_agreement)
      course = FactoryGirl.create(:course)
      vacancy = agreement.vacancy
      expect(vacancy.update(course: course)).to be false
      expect(vacancy.errors[:agreement].any?).to be true
    end

    context 'when course is not opened' do
      context 'and vacancy already has a course' do
        it 'should not assign course to vacancy' do
          vacancy = FactoryGirl.create(:vacancy)
          expect(vacancy.course).not_to be nil
          course = FactoryGirl.create(:course)
          course.close!
          expect(vacancy.update(course: course)).to be false
          expect(vacancy.errors[:course_id].include?(I18n.t('activerecord.errors.models.vacancy.attributes.course_id.course_not_opened')))
        end
      end
      context 'and vacancy has no course' do
        it 'should not assign course to vacancy' do
          vacancy = FactoryGirl.create(:vacancy_without_course)
          expect(vacancy.course).to be nil
          course = FactoryGirl.create(:course)
          course.close!
          expect(vacancy.update(course: course)).to be false
          expect(vacancy.errors[:course_id].include?(I18n.t('activerecord.errors.models.vacancy.attributes.course_id.course_not_opened')))
        end
      end
    end
  end

  context 'Create' do
    it 'a valid Vacancy' do
      expect(FactoryGirl.create(:vacancy)).to be_valid
    end

    it 'an invalid Vacancy' do
      expect(FactoryGirl.build(:vacancy_invalid)).to_not be_valid
    end

    context 'when company_positions_cenco is nil but cenco_id is present' do
      it 'should create a Company::PositionsCenco' do
        course = FactoryGirl.create(:course)
        cenco = FactoryGirl.create(:company_cenco)
        expect{
          vacancy = Vacancy.new(course: course, cenco_id: cenco.id)
          expect(vacancy.company_positions_cenco.present?).to be true
        }.to change(Company::PositionsCenco, :count).by(1)
      end
    end

    context 'when company_positions_cenco is present and cenco_id is present' do
      it 'should create a Company::PositionsCenco' do
        cenco = FactoryGirl.create(:company_cenco)
        company_positions_cenco = FactoryGirl.create(:company_positions_cenco)
        expect{
          vacancy = Vacancy.new(
            company_positions_cenco: company_positions_cenco,
            cenco_id: cenco.id
          )
          expect(vacancy.company_positions_cenco.present?).to be true
          expect(vacancy.cenco_id).to eq company_positions_cenco.company_cenco_id
        }.to change(Company::PositionsCenco, :count).by(0)
      end
    end
  end
end
