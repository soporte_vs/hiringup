require 'rails_helper'

RSpec.describe AssigningWeighting::Stage, :type => :model do
  context "RelationShips" do
    it 'should has_one assigning_weighting_report' do
      should have_one(:assigning_weighting_report).dependent(:destroy)
    end
  end

  context 'Create' do
    it 'a valid assigning_weighting' do
      assigning_weighting_stage = FactoryGirl.create(:assigning_weighting_stage)
      expect(assigning_weighting_stage).to be_valid
      expect(assigning_weighting_stage).to be_a(AssigningWeighting::Stage)
    end
  end
end