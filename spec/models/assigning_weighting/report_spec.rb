require 'rails_helper'

RSpec.describe AssigningWeighting::Report, type: :model do
  context 'RelationShips' do
    it 'should belong a assigning_weighting_stage' do
      should belong_to :assigning_weighting_stage
    end
    it {
      should have_many(:documents).dependent(:destroy)
    }
  end

  context 'Validations' do

    it 'assigning_weighting_stage should be present' do
      should validate_presence_of :assigning_weighting_stage
    end
  end

  context 'Create' do
    it 'should create a valid' do
      expect(
        FactoryGirl.create(:assigning_weighting_report)
      ).to be_valid
    end

    it 'should build an invalid' do
      expect(
        FactoryGirl.build(:assigning_weighting_report_invalid)
      ).to_not be_valid
    end
  end
end
