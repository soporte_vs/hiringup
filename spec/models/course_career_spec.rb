# coding: utf-8
require 'rails_helper'

RSpec.describe CourseCareer, type: :model do
  context "validations" do
    before :each do
      @course_career = FactoryGirl.create(:course_career)
    end

    it 'should validate uniqueness of education_career_id scoped to course id' do
      should validate_uniqueness_of(:education_career_id).scoped_to(:course_id)
      expect(@course_career.errors.empty?).to be true

      cc2 = FactoryGirl.build(:course_career, course_id: @course_career.course_id, education_career_id: @course_career.education_career.id)
      expect(cc2.valid?).to be false
      expect(cc2.errors[:education_career_id].empty?).to be false
      should validate_uniqueness_of(:education_career_id).scoped_to(:course_id)
    end
  end
end
