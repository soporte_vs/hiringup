# == Schema Information
#
# Table name: territory_countries
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Territory::Country, type: :model do
  context "Validation on Country model" do
    it "should validate name is present" do
      should validate_presence_of(:name)
    end

    it "should validate name is uniqueness" do
      should validate_uniqueness_of(:name)
    end

    it "should validate has_many territory_states" do
      should have_many(:territory_states).dependent(:destroy)
    end
  end
end
