# == Schema Information
#
# Table name: territory_cities
#
#  id                 :integer          not null, primary key
#  name               :string
#  territory_state_id :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

require 'rails_helper'

RSpec.describe Territory::City, type: :model do
  context "Validation on Territory::City" do
    it "should presence name" do
      should validate_presence_of(:name)
    end

    it "should presence territory_state" do
      should validate_presence_of(:territory_state)
    end

    it "should uniqueness name" do
      should validate_uniqueness_of(:name)
    end
  end
end
