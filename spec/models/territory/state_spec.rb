# == Schema Information
#
# Table name: territory_states
#
#  id                   :integer          not null, primary key
#  name                 :string
#  territory_country_id :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#

require 'rails_helper'

RSpec.describe Territory::State, type: :model do
  context "Validation on Country model" do
    it "should validate name is present" do
      should validate_presence_of(:name)
    end

    it "should validate name is uniqueness" do
      should validate_uniqueness_of(:name)
    end

    it "should validate territory_country is present" do
      should validate_presence_of(:territory_country)
    end

    it "should has many cities" do
      should have_many(:territory_cities).dependent(:destroy)
    end
  end
end
