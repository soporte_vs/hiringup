require 'rails_helper'

RSpec.describe ManagerInterview::Stage, :type => :model do
  context "RelationShips" do
    it 'should has_one manager_interview_report' do
      should have_one(:manager_interview_report).dependent(:destroy)
    end
  end

  context 'Create' do
    it 'a valid manager_interview' do
      manager_interview_stage = FactoryGirl.create(:manager_interview_stage)
      expect(manager_interview_stage).to be_valid
      expect(manager_interview_stage).to be_a(ManagerInterview::Stage)
    end
  end

  describe 'Not apply' do
    let(:not_apply_reason) { FactoryGirl.create(:stage_not_apply_reason) }

    context 'when action can be done' do
      it 'should create a Stage::NotApplying' do
        stage = FactoryGirl.create(:manager_interview_stage)
        expect{
          stage.not_apply(not_apply_reason, FFaker::Lorem.sentence)
        }.to change(Stage::NotApplying, :count).by(1)
      end
    end

    context 'when action can not be done' do
      it 'should not create a Stage::NotApplying' do
        manager_interview_report = FactoryGirl.create(:manager_interview_report)
        stage = manager_interview_report.manager_interview_stage
        expect{
          stage.not_apply(not_apply_reason, FFaker::Lorem.sentence)
        }.to change(Stage::NotApplying, :count).by(0)

        expect(stage.errors[:not_applying]).to_not be_empty
      end
    end
  end
end
