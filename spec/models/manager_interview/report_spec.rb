require 'rails_helper'

RSpec.describe ManagerInterview::Report, type: :model do
  context 'RelationShips' do
    it 'should belong a manager_interview_stage' do
      should belong_to :manager_interview_stage
    end

    it {
      should have_many(:documents).dependent(:destroy)
    }
  end

  context 'Validations' do
    it 'observations should be present' do
      should validate_presence_of :observations
    end

    it 'manager_interview_stage should be present' do
      should validate_presence_of :manager_interview_stage
    end
  end

  context 'Create' do
    it 'should create a valid' do
      expect(
        FactoryGirl.create(:manager_interview_report)
      ).to be_valid
    end

    it 'should build an invalid' do
      expect(
        FactoryGirl.build(:manager_interview_report_invalid)
      ).to_not be_valid
    end
  end
end
