# == Schema Information
#
# Table name: stage_bases
#
#  id            :integer          not null, primary key
#  enrollment_id :integer
#  type          :string
#  created_at    :datetime
#  updated_at    :datetime
#

require 'rails_helper'

RSpec.describe Engagement::Stage, :type => :model do
  context 'RelationShips on model' do
    it 'should has one agreement' do
      should have_one(:agreement).dependent(:destroy)
    end
  end

  context "Validate that Engagement::Stage Inherited to Stage::Base" do
    it "should be ok" do
      expect(Engagement::Stage.superclass).to eq(Stage::Base)
    end
  end

  context 'Enrollment stage Disposable?' do
    it 'should return false and add errors to enrollment' do
      stage = FactoryGirl.create(:engagement_agreement).engagement_stage
      expect(stage.disposable?).to eq false
      expect(stage.enrollment.errors[:discarding].any?).to be true
    end
  end

  describe 'Not apply' do
    let(:not_apply_reason) { FactoryGirl.create(:stage_not_apply_reason) }

    context 'when action can be done' do
      it 'should create a Stage::NotApplying' do
        stage = FactoryGirl.create(:engagement_stage)
        expect{
          stage.not_apply(not_apply_reason, FFaker::Lorem.sentence)
        }.to change(Stage::NotApplying, :count).by(1)
      end
    end

    context 'when action can not be done' do
      it 'should not create a Stage::NotApplying' do
        agreement = FactoryGirl.create(:engagement_agreement)
        stage = agreement.engagement_stage

        expect{
          stage.not_apply(not_apply_reason, FFaker::Lorem.sentence)
        }.to change(Stage::NotApplying, :count).by(0)

        expect(stage.errors[:not_applying]).to_not be_empty
      end
    end
  end

  describe "#is_approved?" do
    context "when agreement present" do
      before :each do
        @agreement = FactoryGirl.create(:engagement_agreement)
      end

      it "should return true" do
        expect(@agreement.engagement_stage.is_approved?).to be true
      end
    end

    context "when engagement stage not present" do
      let(:engagement_stage) {FactoryGirl.create(:engagement_stage)}

      it "should response nil" do
        expect(engagement_stage.is_approved?).to be nil
      end
    end
  end
end
