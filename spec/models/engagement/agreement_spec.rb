# coding: utf-8
# == Schema Information
#
# Table name: engagement_agreements
#
#  id                  :integer          not null, primary key
#  revenue             :string
#  observations        :text
#  engagement_stage_id :integer
#  created_at          :datetime
#  updated_at          :datetime
#  vacancy_id          :integer
#

require 'rails_helper'

RSpec.describe Engagement::Agreement, :type => :model do
  context "Validations Engagement::Agreement" do
    it "should belongs_to engagement_stage" do
      should belong_to(:engagement_stage)
    end

    it 'should belong_to vacancy' do
      should belong_to(:vacancy)
    end

    it 'should belong_to company_contract_type' do
      should belong_to(:company_contract_type)
    end

    it {
      should have_many(:documents).dependent(:destroy)
    }

    it "should validate presences of: revenue, engagement_stage" do
      should validate_presence_of(:revenue)
      should validate_presence_of(:engagement_stage)
      should validate_presence_of(:company_contract_type)
    end

    it 'should validate presence of' do
      should validate_presence_of :vacancy
    end

    it 'should validate vacancy is uniquesness' do
      engagement_agreement = FactoryGirl.create :engagement_agreement
      error_vacancy_uniquesness = "Vacante ya está en uso"
      error_duplicate_agreement = "Duplicate agreement #{I18n.t('activerecord.errors.models.engagement.agreement.duplicate')}"
      error_expected= "La validación falló: #{error_vacancy_uniquesness}, #{error_duplicate_agreement}"
      expect {
        FactoryGirl.create :engagement_agreement, vacancy: engagement_agreement.vacancy , engagement_stage: engagement_agreement.engagement_stage
      }.to raise_error(ActiveRecord::RecordInvalid, error_expected)
    end

    it 'should validate that course vacancy is same that enrollment' do
      company_positions_cenco = FactoryGirl.create :company_positions_cenco
      course1 = FactoryGirl.create(:course, company_position: company_positions_cenco.company_position)
      vacancy = FactoryGirl.create :vacancy, course: course1, company_positions_cenco: company_positions_cenco

      course2 = FactoryGirl.create(:course)
      enrollment = FactoryGirl.create(:enrollment_basis_with_applicant, course: course2)
      engagement_stage = FactoryGirl.create(:engagement_stage, enrollment: enrollment)
      agreement = FactoryGirl.build :engagement_agreement, vacancy: vacancy, engagement_stage: engagement_stage
      expect(agreement).to_not be_valid
    end
  end

  context "Create more than one is not posible" do
    it "should return an error" do
      engagement_agreement = FactoryGirl.create(:engagement_agreement)
      engagement_agreement = FactoryGirl.build(:engagement_agreement, engagement_stage: engagement_agreement.engagement_stage)
      expect(engagement_agreement).to_not be_valid
    end
  end

  context 'Assign vacancy to Agreement' do
    it 'should update date close_at of vacancy used' do
      vacancy = FactoryGirl.create(:vacancy)
      enrollment = FactoryGirl.create(:enrollment_basis_with_applicant, course: vacancy.course)
      engagement_stage = FactoryGirl.create(:engagement_stage, enrollment: enrollment)
      engagement_agreement = FactoryGirl.create(:engagement_agreement, engagement_stage: engagement_stage)
      expect(engagement_agreement.vacancy.closed_at).to eq(engagement_agreement.created_at)

      another_vacancy = FactoryGirl.create(:vacancy, course: vacancy.course, company_positions_cenco: vacancy.company_positions_cenco)
      engagement_agreement.update(vacancy: another_vacancy)
      expect(engagement_agreement.vacancy.closed_at).to eq(engagement_agreement.created_at)
    end
  end

  context "Assign Company::Employee" do
    let(:applicant) {FactoryGirl.create(:applicant_bases)}
    let(:enrollment) {FactoryGirl.create(:enrollment_dynamic, applicant: applicant)}
    let(:engagement_stage) {FactoryGirl.create(:engagement_stage, enrollment: enrollment, name: "Contratación")}

    context 'when applicant is not a employee' do
      it 'should create a new employee' do
        engagement_agreement = nil
        expect{
          engagement_agreement = FactoryGirl.create(:engagement_agreement, engagement_stage: engagement_stage)
        }.to change(Company::Employee, :count).to eq(1)
        company_employee_positions_cenco = Company::EmployeePositionsCenco.last
        expect(company_employee_positions_cenco.company_positions_cenco).to eq(engagement_agreement.vacancy.company_positions_cenco)
        expect(company_employee_positions_cenco.company_employee.current_employee_position_cenco).to eq(company_employee_positions_cenco)
      end
    end

    context 'when applicant is an employee' do
      let(:employee) {FactoryGirl.create(:company_employee)}
      before :each do
       #applicant = FactoryGirl.create(:applicant_bases, user: employee.user)
       applicant = Applicant::Base.find_by(user_id: employee.user.id) # Employee siempre tendra un applicant asociados
       @enrollment = FactoryGirl.create(:enrollment_dynamic, applicant: applicant)
       @engagement_stage = FactoryGirl.create(:engagement_stage, enrollment: @enrollment, name: "Contratación")
      end

      it 'should not create a new employee' do
        engagement_agreement = nil
        expect{
          engagement_agreement = FactoryGirl.create(:engagement_agreement, engagement_stage: @engagement_stage)
        }.to change(Company::Employee, :count).by(0).and \
             change(Company::EmployeePositionsCenco, :count).by(1)

        company_employee_positions_cenco = Company::EmployeePositionsCenco.last
        expect(company_employee_positions_cenco.company_employee).to eq(employee)
        expect(company_employee_positions_cenco.company_positions_cenco).to eq(engagement_agreement.vacancy.company_positions_cenco)
        expect(company_employee_positions_cenco.company_employee.current_employee_position_cenco).to eq(company_employee_positions_cenco)
      end
    end
  end
end
