require 'rails_helper'

RSpec.describe Document::Group, type: :model do
  context 'Relationships' do
    it 'should have many document_descriptors' do
      should have_many(:document_descriptors)
    end
    it 'should have many document_descriptor_memberships' do
      should have_many(:document_descriptor_memberships)
    end
  end

  context 'Validations' do
    it 'name should be present' do
      should validate_presence_of :name
    end
  end

  context 'Create' do
    it 'should create a valid group' do
      expect(FactoryGirl.create :document_group).to be_valid
    end
  end
  
  context 'Remaining documents' do
    it 'it should have no document remaining' do
      record = FactoryGirl.create(:document_record)
      expect(record.document_descriptor.document_groups.first.remaining_document_records(Document::Record.where(:id => record.id))).to be 0
    end
  end
end
