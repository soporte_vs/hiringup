# coding: utf-8
require 'rails_helper'

RSpec.describe Document::Descriptor, type: :model do
  context 'Relationships' do
    it 'should have many document_groups' do
      should have_many(:document_groups)
    end
    it 'should have many document_descriptor_memberships' do
      should have_many(:document_descriptor_memberships)
    end
  end

  context 'Validations' do
    it 'name should be present' do
      should validate_presence_of :name
    end

    it 'name should be unique' do
      should validate_uniqueness_of :name
    end

    it 'name should be well formatted' do
      d = FactoryGirl.create(:document_descriptor, :name => 'ThIs iS a TESt')
      expect(d.name).to eq('this_is_a_test')

      d2 = FactoryGirl.create(:document_descriptor, :name => '     ThIs iS      a     TESt 2    ')
      expect(d2.name).to eq('this_is_a_test_2')

      d3 = FactoryGirl.create(:document_descriptor, :name => '          thís_is Á test     3 ')
      expect(d3.name).to eq('th_s_is___test_3')

      d4 = FactoryGirl.create(:document_descriptor, :name => ' &(*&)ANOTHER test ')
      expect(d4.name).to eq('_____another_test')

      d5 = FactoryGirl.create(:document_descriptor, :name => 'another _test5')
      expect(d5.name).to eq('another_test5')

      d6 = FactoryGirl.create(:document_descriptor, :name => 'another_____test6')
      expect(d6.name).to eq('another_test6')

      d7 = FactoryGirl.create(:document_descriptor, :name => 'another   test7')
      expect(d7.name).to eq('another_test7')
    end

    it 'name should remain with no changes' do
      d = FactoryGirl.create(:document_descriptor, :name => 'ThIs iS a TESt')
      expect(d.name).to eq('this_is_a_test')

      d.update(name: '     this is another test     ')
      expect(d.name).to eq('this_is_a_test')

      d.update(name: '     ano7her 0n3     ')
      expect(d.name).to eq('this_is_a_test')
    end

    it 'details should be present' do
      should validate_presence_of :details
    end
  end

  context 'Create' do
    it 'should create a valid descriptor' do
      expect(FactoryGirl.create :document_descriptor).to be_valid
    end
  end

  describe '#attachment=' do
    it 'validate upload file' do
      document_descriptor = FactoryGirl.create(:document_descriptor)
      attachment = FactoryGirl.create(:generic_attachment, resourceable: document_descriptor)
      expect(attachment.persisted?).to be true
      expect(attachment.resourceable.class.to_s).to eq(document_descriptor.class.to_s)
    end
  end

  describe "CV" do
    let(:cv) {FactoryGirl.create(:document_descriptor, name: "cv")}
    it "should not destroy document_descriptor" do
      cv.destroy
      cv.reload
      expect(cv.persisted?).to be true
    end

    it "should not update name of document_descriptor" do
      cv.update(name: FFaker::Lorem.word)
      cv.reload
      expect(cv.name).to eq "cv"
    end
  end
end
