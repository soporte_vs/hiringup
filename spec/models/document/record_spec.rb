require 'rails_helper'

RSpec.describe Document::Record, type: :model do
  context 'Relationships' do
    it { should belong_to(:document_descriptor) }
    it { should belong_to(:applicant) }
  end

  context 'Validations' do
    it { should validate_presence_of :document_descriptor }
    it { should validate_presence_of :document }
  end

  describe '#is_internal?' do
    it 'should return true if descriptor is internal' do
      document_descriptor = FactoryGirl.create(:document_descriptor, internal: true)
      document_record = FactoryGirl.create(:document_record, document_descriptor: document_descriptor)
      expect(document_record.is_internal?).to be true
    end

    it 'should return false if descriptor is not internal' do
      document_descriptor = FactoryGirl.create(:document_descriptor, internal: false)
      document_record = FactoryGirl.create(:document_record, document_descriptor: document_descriptor)
      expect(document_record.is_internal?).to be false
    end
  end

  context 'Notify upload internal postulation template' do
    it 'should send notify internal postulation' do
      document_descriptor = FactoryGirl.create(:document_descriptor, name: 'internal_postulation_template')
      enrollment = FactoryGirl.create(:enrollment_basis_with_applicant, type: Enrollment::Internal)

      expect{
        FactoryGirl.create(:document_record,
          document_descriptor: document_descriptor,
          applicant: enrollment.applicant
        )
      }.to change(Delayed::Job, :count).by(1)

      notify_upload_internal_postulation_email = YAML.load(Delayed::Job.last.handler)

      enrollment_args = notify_upload_internal_postulation_email.args.first
      expect(notify_upload_internal_postulation_email.object).to eq Courses::InternalMailer
      expect(notify_upload_internal_postulation_email.method_name).to eq :notify_internal_postulation
      expect(enrollment_args.type.constantize).to eq Enrollment::Internal
      expect(enrollment_args.id).to eq enrollment.id
    end
  end
end
