require 'rails_helper'

RSpec.describe Document::DescriptorMembership, type: :model do
  context 'Relationships' do
    it 'should belong to document_descriptor' do
      should belong_to(:document_descriptor)
    end
    it 'should belong to document_group' do
      should belong_to(:document_group)
    end
  end
end


