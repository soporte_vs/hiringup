require 'rails_helper'

RSpec.describe Interview::Report, type: :model do
  context 'RelationShips' do
    it 'should belong a interview_stage' do
      should belong_to :interview_stage
    end
  end

  context 'Validations' do
    it 'observations should be present' do
      should validate_presence_of :observations
    end

    it 'interview_stage should be present' do
      should validate_presence_of :interview_stage
    end
  end

  context 'Create' do
    it 'should create a valid' do
      expect(
        FactoryGirl.create(:interview_report)
      ).to be_valid
    end

    it 'should build an invalid' do
      expect(
        FactoryGirl.build(:interview_report_invalid)
      ).to_not be_valid
    end
  end
end
