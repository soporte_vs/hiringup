require 'rails_helper'

RSpec.describe Interview::Stage, :type => :model do
  context "RelationShips" do
    it 'should has_one interview_report' do
      should have_one(:interview_report).dependent(:destroy)
    end
  end

  context 'Create' do
    it 'a valid interview' do
      interview_stage = FactoryGirl.create(:interview_stage)
      expect(interview_stage).to be_valid
      expect(interview_stage).to be_a(Interview::Stage)
    end
  end

end
