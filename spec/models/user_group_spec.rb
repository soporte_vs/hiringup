# == Schema Information
#
# Table name: user_groups
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  group_id   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe UserGroup, type: :model do
  context 'Validations' do
    it 'group should be present' do
      should validate_presence_of :group
    end

    it 'user should be present' do
      should validate_presence_of :user
    end

    it 'user should be uniq scoped from group' do
      user_group = FactoryGirl.create :user_group
      expect{
        FactoryGirl.create :user_group, user: user_group.user, group: user_group.group
      }.to raise_error(ActiveRecord::RecordInvalid, 'La validación falló: User ya está en uso')
    end
  end
end
