# coding: utf-8

# coding: utf-8
# == Schema Information
#
# Table name: courses
#
#  id                  :integer          not null, primary key
#  type_enrollment     :string
#  title               :string
#  description         :text
#  created_at          :datetime
#  updated_at          :datetime
#  work_flow           :string
#  territory_city_id   :integer
#  company_position_id :integer
#  document_group_id   :integer
#  aasm_state          :string
#  user_id             :integer
#

require 'rails_helper'

RSpec.describe Course, :type => :model do

  context "Validations Course" do
    it "should validate type_enrollment, title and description are present" do
      should validate_presence_of(:user)
      should validate_presence_of(:type_enrollment)
      should validate_presence_of(:title)
      should validate_presence_of(:description)
      should validate_presence_of(:work_flow)
      should validate_presence_of(:start)
      should validate_presence_of(:territory_city)
      should validate_presence_of(:company_position)
      should validate_presence_of(:document_group)
      should validate_presence_of(:timetable)
      should validate_presence_of(:study_type)
      should validate_presence_of(:study_level)
      should validate_presence_of(:applicant_profile_type)
      should validate_presence_of(:area)
      should validate_presence_of(:applicant_software_level)
      should validate_presence_of(:minimun_requirements)
    end

    it { should belong_to(:applicant_profile_type) }
    it { should belong_to(:applicant_software_level) }
    it { should belong_to(:area) }

    it "should serialize work_flow" do
      should serialize(:work_flow)
    end

    it 'should belong_to territory_city' do
      should belong_to(:territory_city)
    end

    it 'should belong_to company_position' do
      should belong_to(:company_position)
    end

    it 'should have_many enrollments, applicants, used_tags, tags, vacancies, course_postulations' do
      should have_many(:enrollments).dependent(:destroy)
      should have_many(:applicants)
      should have_many(:used_tags).dependent(:destroy)
      should have_many(:tags)
      should have_many(:vacancies).dependent(:nullify).inverse_of(:course)
      should have_many(:minisite_publications).dependent(:destroy)
      should have_many(:trabajando_publications).dependent(:destroy)
      should have_many(:laborum_publications).dependent(:destroy)
      should have_many(:careers).through(:course_careers).source(:education_career)
      should have_many(:course_careers).dependent(:destroy)
      should have_many(:trabajando_publications).dependent(:destroy)
      should have_many(:course_postulations)
    end

    it "should has evaluar_course" do
      should have_one(:evaluar_course).dependent(:destroy)
      should accept_nested_attributes_for(:evaluar_course)
    end

    it 'should has vacancies as nested_attributes' do
      should accept_nested_attributes_for(:vacancies).allow_destroy(true)
    end
  end

  context "Create a Course" do
    it "should be valid" do
      course = FactoryGirl.create(:course)
      expect(course).to be_valid
      expect(course.opened?).to be true
    end

    it "should be invalid because type_enrollment is not a Enrollment descendant" do
      course = FactoryGirl.build(:course_type_enrollment_invalid)
      expect(course).to_not be_valid
    end
  end

  describe "multiple vacancies" do
    context "when vacancies attributes has qty param" do
      context "when a new course is created" do
        it "should multiply vacancies by qty times" do
          course_attributes = FactoryGirl.attributes_for(:course_with_tags)
          course = FactoryGirl.build(:course, course_attributes)
          company_positions_cenco = FactoryGirl.create(:company_positions_cenco, company_position_id: course_attributes[:company_position_id])
          company_vacancy_request_reason = FactoryGirl.create(:company_vacancy_request_reason)

          course.vacancies_attributes = [
            { observations: FFaker::Lorem.sentence,
              cenco_id: company_positions_cenco.company_cenco_id,
              company_vacancy_request_reason_id: company_vacancy_request_reason.id,
              qty: 4
            }
          ]

          expect(course.save).to be true
          expect(course.vacancies.count).to be 4
          course.vacancies.each do |vacancy|
            expect(vacancy.company_positions_cenco).to eq company_positions_cenco
            expect(vacancy.cenco_id).to eq company_positions_cenco.company_cenco_id
            expect(vacancy.company_vacancy_request_reason).to eq company_vacancy_request_reason
          end
        end
      end

      context "when a course is updated" do
        let(:course_attributes) { FactoryGirl.attributes_for(:course_with_tags) }
        let!(:built_course) { FactoryGirl.build(:course, course_attributes) }
        let!(:course) {
          built_course.vacancies_attributes = vacancies_attributes
          built_course.save
          built_course
        }
        let(:company_positions_cenco) {
          FactoryGirl.create(
            :company_positions_cenco,
            company_position_id: course_attributes[:company_position_id]
          )
        }
        let(:company_vacancy_request_reason) {
          FactoryGirl.create(:company_vacancy_request_reason)
        }

        let(:company_positions_cenco2) {
          FactoryGirl.create(
            :company_positions_cenco,
            company_position_id: course_attributes[:company_position_id]
          )
        }
        let(:company_vacancy_request_reason2) {
          FactoryGirl.create(:company_vacancy_request_reason)
        }

        let(:vacancies_qty) { 4 }
        let(:new_vacancies_qty) { vacancies_qty }
        let(:vacancies_qty2) { 3 }
        let(:new_vacancies_qty2) { vacancies_qty2 }

        let(:new_vacancies_attributes) {
          [
            {
              cenco_id: company_positions_cenco.company_cenco_id,
              company_vacancy_request_reason_id: company_vacancy_request_reason.id,
              qty: new_vacancies_qty,
              vacancy_ids: course.vacancy_ids
            }
          ]
        }

        let(:vacancies_attributes) {
          [
            {
              observations: FFaker::Lorem.sentence,
              cenco_id: company_positions_cenco.company_cenco_id,
              company_vacancy_request_reason_id: company_vacancy_request_reason.id,
              qty: vacancies_qty #,
              # vacancy_ids: course.vacancy_ids
            }
          ]
        }

        let!(:existing_vacancies) {
          course.vacancies
        }

        context "when vacancies have all the same attributes" do
          before :each do
            course.vacancies_attributes = [
              {
                observations: FFaker::Lorem.sentence,
                cenco_id: company_positions_cenco.company_cenco_id,
                company_vacancy_request_reason_id: company_vacancy_request_reason.id,
                qty: vacancies_qty,
                vacancy_ids: course.vacancy_ids
              }
            ]
            course.save

            expect(course.vacancies.count).to be vacancies_qty
          end

          context "when qty is the same as the quantity of vacancies with given attributes" do
            it "should not change vacancies" do
              course.vacancies_attributes = new_vacancies_attributes
              course.save
              expect(course.vacancies).to eq existing_vacancies
            end
          end

          context "when qty is not the same as the quantity of vacancies with given attributes" do
            let(:vacancies_expectations) {
              expect(course.vacancies.count).to eq new_vacancies_qty
              course.vacancies.each do |vacancy|
                expect(vacancy.company_positions_cenco).to eq company_positions_cenco
                expect(vacancy.cenco_id).to eq company_positions_cenco.company_cenco_id
                expect(vacancy.company_vacancy_request_reason).to eq company_vacancy_request_reason
              end
            }

            context "when qty is the more than the quantity of vacancies with given attributes" do
              let(:new_vacancies_qty) { 5 }

              it "should add qty vacancies" do
                course.vacancies_attributes = new_vacancies_attributes
                course.save
              end
            end

            context "when qty is the less than the quantity of vacancies with given attributes" do
              let(:new_vacancies_qty) { 3 }
              it "should delete qty vacancies" do
                course.vacancies_attributes = new_vacancies_attributes
                course.save
              end
            end

            context "when qty is 0" do
              let(:new_vacancies_qty) { 0 }
              let(:vacancies_expectations) {
                expect(course.vacancies.count).to eq new_vacancies_qty
              }
              it "should delete all vacancies" do
                course.vacancies_attributes = new_vacancies_attributes
                course.save
              end
            end

            after :each do
              vacancies_expectations
            end
          end
        end

        context "when there are vacancies with different attributes" do
          let(:vacancies_attributes1) {
            {
              observations: FFaker::Lorem.sentence,
              cenco_id: company_positions_cenco.company_cenco_id,
              company_vacancy_request_reason_id: company_vacancy_request_reason.id,
            }
          }

          let(:vacancies_attributes2) {
            {
              observations: FFaker::Lorem.sentence,
              cenco_id: company_positions_cenco2.company_cenco_id,
              company_vacancy_request_reason_id: company_vacancy_request_reason2.id,
            }
          }

          let(:expected_vacancies_qty) { vacancies_qty + vacancies_qty2 }
          let(:vacancies_expectations) {
            expect(course.vacancies.count).to eq expected_vacancies_qty
            expect(
              course.vacancies.where(
                company_positions_cenco: company_positions_cenco,
                company_vacancy_request_reason: company_vacancy_request_reason
              ).count
            ).to eq new_vacancies_qty

            expect(
              course.vacancies.where(
                company_positions_cenco: company_positions_cenco2,
                company_vacancy_request_reason: company_vacancy_request_reason2
              ).count
            ).to eq vacancies_qty2
          }

          let(:vacancies1) {
            vacancies = course.vacancies.where(
              company_vacancy_request_reason_id: vacancies_attributes1[:company_vacancy_request_reason_id],
              company_positions_cenco_id: company_positions_cenco.id
            )
            vacancies
          }

          let(:vacancies2) {
            vacancies = course.vacancies.where(
              company_vacancy_request_reason_id: vacancies_attributes2[:company_vacancy_request_reason_id],
              company_positions_cenco_id: company_positions_cenco2.id
            )
            vacancies
          }

          let(:vacancies_attributes) {
            [
              vacancies_attributes1.merge(
                {
                  qty: vacancies_qty #,
                  #vacancy_ids: vacancies1.map(&:id)
                }
              ),
              vacancies_attributes2.merge(
                {
                  qty: vacancies_qty2 #,
                  #vacancy_ids: vacancies2.map(&:id)
                }
              )
            ]
          }

          context "when changes only attributes for one type(group) of vacancies" do
            context "when changes only the vacancy request reason" do
              let(:new_company_vacancy_request_reason) { FactoryGirl.create(:company_vacancy_request_reason) }

              let(:new_vacancies_attributes) {
                [
                  vacancies_attributes1.merge(
                    {
                      qty: vacancies_qty,
                      company_vacancy_request_reason_id: new_company_vacancy_request_reason.id,
                      vacancy_ids: vacancies1.map(&:id)
                    }
                  ),
                  vacancies_attributes2.merge(
                    {
                      qty: vacancies_qty2,
                      vacancy_ids: vacancies2.map(&:id)
                    }
                  )
                ]
              }

              let(:new_vacancies1) {
                # estas son las vacantes del proceso, que tienen el nuevo parámetro vacancy request reason,
                # o sea, las vacantes del proceso que tienen el parámetro que cambió
                vacancies = course.vacancies.where(
                  company_vacancy_request_reason_id: new_company_vacancy_request_reason.id,
                  company_positions_cenco_id: company_positions_cenco.id
                )
                vacancies
              }

              it "should not change vacancies quantity, only vacancy_request_reason" do
                course.vacancies_attributes = new_vacancies_attributes
                course.save
                expect(course.vacancies.count).to eq expected_vacancies_qty
                expect(new_vacancies1.count).to eq vacancies_qty # tienen que ser la misma cantidad que antes
                expect(new_vacancies1 + vacancies2).to match_array course.vacancies
                new_vacancies1.each do |vacancy|
                  expect(vacancy.cenco_id).to eq company_positions_cenco.company_cenco_id
                  expect(vacancy.company_vacancy_request_reason_id).to eq new_company_vacancy_request_reason.id
                end

                vacancies2.each do |vacancy|
                  expect(vacancy.cenco_id).to eq company_positions_cenco2.company_cenco_id
                  expect(vacancy.company_vacancy_request_reason_id).to eq company_vacancy_request_reason2.id
                end
              end
            end

            context "when changes only the quantity of vacancies" do
              let(:new_vacancies_attributes) {
                [
                  vacancies_attributes1.merge(
                    {
                      qty: new_vacancies_qty,
                      vacancy_ids: vacancies1.map(&:id)
                    }
                  ),
                  vacancies_attributes2.merge(
                    {
                      qty: vacancies_qty2,
                      vacancy_ids: vacancies2.map(&:id)
                    }
                  )
                ]
              }

              context "when qty is the same as the quantity of vacancies with given attributes" do
                it "should not change vacancies" do
                  course.vacancies_attributes = new_vacancies_attributes
                  course.save
                  expect(course.vacancies).to eq existing_vacancies
                  expect(course.vacancies).to match_array (vacancies1 + vacancies2)
                end
              end

              context "when qty is not the same as the quantity of vacancies with given attributes" do
                let(:expected_vacancies_qty) { new_vacancies_qty + vacancies_qty2 }

                before :each do
                  course.vacancies_attributes = new_vacancies_attributes
                  course.save
                end

                context "when qty is the more than the quantity of vacancies with given attributes" do
                  let(:new_vacancies_qty) { 5 }

                  it "should add qty vacancies" do
                  end
                end

                context "when qty is the less than the quantity of vacancies with given attributes" do
                  let(:new_vacancies_qty) { 3 }
                  it "should delete qty vacancies" do
                  end
                end

                context "when qty is 0" do
                  let(:new_vacancies_qty) { 0 }
                  it "should delete all vacancies" do
                  end
                end

                after :each do
                  vacancies_expectations
                end
              end
            end

            context "when deleting a vacancy already used" do
              let(:new_vacancies_attributes) {
                [
                  vacancies_attributes1.merge(
                    {
                      qty: new_vacancies_qty,
                      vacancy_ids: vacancies1.map(&:id)
                    }
                  ),
                  vacancies_attributes2.merge(
                    {
                      qty: vacancies_qty2,
                      vacancy_ids: vacancies2.map(&:id)
                    }
                  )
                ]
              }

              let(:new_vacancies_qty) { 0 }

              let(:vacancies_expectations) {
                expect(course.vacancies.count).to eq expected_vacancies_qty
                expect(
                  course.vacancies.where(
                    company_positions_cenco: company_positions_cenco,
                    company_vacancy_request_reason: company_vacancy_request_reason
                  ).count
                ).to eq 1 # una que no se borra

                expect(
                  course.vacancies.where(
                    company_positions_cenco: company_positions_cenco2,
                    company_vacancy_request_reason: company_vacancy_request_reason2
                  ).count
                ).to eq vacancies_qty2
              }

              context "when a vacancy has a offer_letter" do
                let(:expected_vacancies_qty) { 1 + vacancies_qty2 } # 1 porque le asigno sólo a una una carta oferta
                before :each do
                  FactoryGirl.create :offer_letter, vacancy: vacancies1.last
                  expect(vacancies1.last.offer_letters).not_to be_empty
                  course.vacancies_attributes = new_vacancies_attributes
                  course.save

                  expect(vacancies1.count).to eq 1
                end

                it "should not delete the used vacancy" do
                end
              end

              context "when a vacancy has an agreement" do
                let(:expected_vacancies_qty) { 1 + vacancies_qty2 } # 1 porque le asigno sólo a un agreement

                before :each do
                  agreement = FactoryGirl.create :engagement_agreement, vacancy_id: vacancies1.last.id
                  expect(vacancies1.last.agreement).to eq agreement
                  course.vacancies_attributes = new_vacancies_attributes
                  course.save

                  expect(vacancies1.count).to eq 1
                end

                it "should not delete the used vacancy" do
                end
              end

              after :each do
                vacancies_expectations
              end
            end
          end
        end
      end
    end
  end # end multiple vacancies

  describe 'Elastic Search' do
    context 'index name' do
      it 'should be test-courses', elasticsearch: true do |example|
        course = FactoryGirl.build(:course)
        expect(course.class.index_name).to eq "#{PREFIX_INDEX}-#{example.metadata[:block].object_id}"
      end
    end

    context 'mmapping name' do
      it 'should be test-course' do
        course = FactoryGirl.build :course
        expect(course.class.document_type).to eq 'courses'
      end
    end
  end

  context "Enroll Applicant to Course" do
    it "should be valid" do
      applicant_catched = FactoryGirl.create(:applicant_catched, user_attributes: FactoryGirl.attributes_for(:user, email: "integraerror@valposystems.cl"))
      applicant_external = FactoryGirl.create(:applicant_external, user_attributes: FactoryGirl.attributes_for(:user, email: "integraerror@valposystems.cl"))
      course = FactoryGirl.create(:course)
      expect(course.enroll(applicant_catched)).to eq(true)
      expect(course.enroll(applicant_external)).to eq(true)
      expect(course.enrollments.count).to eq(2)
    end

    context 'when course is closed' do
      let(:course) { FactoryGirl.create(:course) }
      let(:applicants) { FactoryGirl.create_list(:applicant_bases, 2) }

      before :each do
        course.close!
      end

      it 'should not enroll applicants' do
        expect(course.enroll(applicants)).to be false
      end
    end

    context 'when applicant is already enrolled on course' do
      let(:enrollment) {FactoryGirl.create(:enrollment_basis_with_applicant)}
      let(:applicant) {enrollment.applicant}
      let(:course) {enrollment.course}

      it 'should not enroll applicant on course' do
        expect(course.enroll(applicant)).to be false
      end
    end

    context 'when applicant is postulated on course' do
     let(:postulation) { FactoryGirl.create(:minisite_postulation) }
     let(:course) { postulation.minisite_publication.course }
     let(:applicant) { postulation.applicant }

      it 'should not enroll applicant on course' do
        expect(course.enroll(applicant)).to be false
      end
    end
  end

  context "Enroll! Applicant to Course" do
    let(:postulation) { FactoryGirl.create(:minisite_postulation, is_accepted: nil) }
    let(:applicant) { postulation.applicant }
    let(:course) { FactoryGirl.create(:course) }

    it "should be valid" do
      expect(course.enroll!(applicant)).to eq(true)
      expect(course.enrollments.count).to eq(1)
    end

    context 'when course is closed' do
      before :each do
        course.close!
      end

      it 'should not enroll applicants' do
        expect(course.enroll!(applicant)).to be false
      end
    end
  end

  context "Call filter_by_role any role when user has role admin on Course" do
    it "should does not return duplicates courses" do
      user = FactoryGirl.create(:user)
      courses = []
      territory_city = FactoryGirl.create(:territory_city)
      rand(1..4).times.each do
        courses.push(FactoryGirl.create(:course, territory_city: territory_city))
      end
      user.add_role :admin, Course
      expect(Course.filter_by_role(:show, user)).to match_array(courses)
    end
  end

  context "Call filter_by_role any role when user has role admin and show on course object" do
    it "should does not return duplicates courses" do
      user = FactoryGirl.create(:user)
      courses = []
      territory_city = FactoryGirl.create(:territory_city)
      rand(1..4).times.each do
        courses.push(FactoryGirl.create(:course, territory_city: territory_city))
      end
      random_key = rand(courses.length)
      user.add_role :admin, courses[random_key]
      user.add_role :show, courses[random_key]
      expect(Course.filter_by_role(:show, user)).to match_array([courses[random_key]])
    end
  end

  context "Call users_by_role any role when user has role admin, admin/Course and admin/course_object" do
    it "should does not return users duplicates" do
      course = FactoryGirl.create(:course)
      user = FactoryGirl.create(:user)
      user.add_role :admin
      user.add_role :admin, Course
      user.add_role :admin, course
      expect(course.users_by_role(:kasdf)).to match_array([user])
    end
  end

  context 'Update Course' do
    it "should be invalid" do
      course = FactoryGirl.create(:course)
      expect {
        course.update_attribute(:type_enrollment, 'Enrollment::BTAD')
      }.to raise_error(ActiveRecord::ActiveRecordError, 'type_enrollment is marked as readonly')
    end
  end

  context 'Update vacancies into course' do
    let(:course){ FactoryGirl.create(:course) }
    let(:company_positions_cenco){ FactoryGirl.create(:company_positions_cenco, company_position: course.company_position) }
    let(:company_vacancy_request_reason){ FactoryGirl.create(:company_vacancy_request_reason) }

    context 'When vacancy already exists' do
      let(:vacancy){ FactoryGirl.create(:vacancy_without_course, company_positions_cenco: company_positions_cenco) }

      it 'Should create only one vacancy' do
        observations = FFaker::Lorem.sentence
        vacancies_attributes = [
          {
            id: vacancy.id,
            observations: observations,
            company_positions_cenco_id: company_positions_cenco.id,
            company_vacancy_request_reason_id: company_vacancy_request_reason.id
          },
          {
            observations: FFaker::Lorem.sentence,
            company_positions_cenco_id: company_positions_cenco.id,
            company_vacancy_request_reason_id: company_vacancy_request_reason.id
          }
        ]
        expect{
          expect(course.update(vacancies_attributes: vacancies_attributes)).to be true
        }.to change(course.vacancies, :count).by(2).and \
             change(Vacancy, :count).by(1)
      end
    end

    it 'should create vacancies only with observations not blank and company_positions_cenco_id' do
      vacancies_attributes = [
        { observations: FFaker::Lorem.sentence,
          company_positions_cenco_id: company_positions_cenco.id,
          company_vacancy_request_reason_id: company_vacancy_request_reason.id
        },
        { observations: FFaker::Lorem.sentence,
          company_positions_cenco_id: company_positions_cenco.id,
          company_vacancy_request_reason_id: company_vacancy_request_reason.id
        }
      ]
      course.update(vacancies_attributes: vacancies_attributes)
      expect(course.vacancies.count).to eq(2)
    end

    it 'should not reject vacancy nested if observations or company_positions_cenco_id is present' do
      vacancies_attributes = [
        { observations: '',
          company_positions_cenco_id: company_positions_cenco.id
        },
        { observations: FFaker::Lorem.sentence,
          company_positions_cenco_id: ''
        }
      ]
      course.update(vacancies_attributes: vacancies_attributes)
      expect(course.vacancies.length).to eq(2)
    end

    it 'should not destroy any vacancy occupied' do
      observations = FFaker::Lorem.sentence
      agreement = FactoryGirl.create(:engagement_agreement)
      course = agreement.engagement_stage.enrollment.course
      #Mapping vacancies_attributes to update course vacancies
      vacancies_attributes = course.vacancies.map { |v|
        {
          id: v.id,
          observations: v.observations,
          company_vacancy_request_reason_id: v.company_vacancy_request_reason_id,
          _destroy: 1
        }
      }
      #New vacancy into course
      vacancies_attributes.push({
        observations: observations,
        company_vacancy_request_reason_id: Company::VacancyRequestReason.last.id,
        company_positions_cenco_id: Company::PositionsCenco.last.id
      })
      # Vacancies Attributes expected
      vacancies_expected = course.vacancies.to_a
      vacancies_expected.push(Vacancy.new(
        course: course, observations: observations,
        company_vacancy_request_reason_id: Company::VacancyRequestReason.last.id,
        company_positions_cenco_id: Company::PositionsCenco.last.id
      ))
      # No update because one vancancy is not destroyable
      expect{
        course.update(vacancies_attributes: vacancies_attributes)
      }.to raise_error(ActiveRecord::RecordNotDestroyed)
      expect(course.vacancies.map{ |v| v.attributes }).to match_array vacancies_expected.map { |v| v.attributes }
    end

    context 'when vacancy is associated a hiring_request_record' do
      let(:hiring_request_record){
        FactoryGirl.create(
          :hiring_request_record,
          company_positions_cenco: company_positions_cenco,
          number_vacancies: Random.rand(1..3)
        )
      }

      before :each do
        hiring_request_record.accept!
        course.vacancies << hiring_request_record.vacancies
      end

      it {
        attributes = course.vacancies.map(&:attributes)
        attributes.each do |attrs|
          attrs['_destroy'] = "1"
        end
        expect{
          course.update(vacancies_attributes: attributes)
        }.to change(course.vacancies, :count).by(-hiring_request_record.vacancies.count).and \
             change(hiring_request_record.vacancies, :count).by(0)
      }

      it "should change vacancy course to a new course" do
        vacancies = course.vacancies

        vacancies_ids = vacancies.map(&:id)

        # Vacancies already have a course
        vacancies.each do |vacancy|
          expect(vacancy.course.id).to eq course.id
        end

        # Set vacancies to a new course *as attributes*
        new_course = FactoryGirl.build(:course, company_position_id: course.company_position_id)

        vacancies_attributes = vacancies.map do |vacancy|
          vacancy.attributes.slice(
            "id",
            "company_vacancy_request_reason_id",
            "course_id",
            "observations"
          )
        end

        new_course.vacancies_attributes = vacancies_attributes

        expect(new_course.save).to be true

        # Now vacancies should be associated to new_course
        vacancies_ids.each do |vacancy_id|
          vacancy = Vacancy.find(vacancy_id)
          expect(vacancy.course.id).to eq new_course.id
        end
      end
    end
  end

  describe 'Method step_present?' do
    let(:course){ FactoryGirl.create(:course) }

    before :each do
      work_flow = [
        {type: :module, name: 'Onboarding', class_name: Onboarding::Stage.to_s},
      ]
      course.update(work_flow: work_flow)
    end

    context 'when step is present' do
      it 'should return true' do
        expect(course.step_present?("Onboarding::Stage")).to be true
        expect(course.step_present?("Onboarding")).to be true
      end
    end

    context 'when step is not present' do
      it 'should return false' do
        expect(course.step_present?(Engagement::Stage.to_s)).to be false
      end
    end
  end

  describe '#total_pending_postulations' do
    let(:course) { FactoryGirl.create(:course) }
    let(:publications) { FactoryGirl.create_list(:minisite_publication,3, course: course) }
    let(:total_pending_postulations){course.total_pending_postulations}
    let(:expected_pending_postulations) {@postulations.count}

    it 'returns the count of pending postulations' do
      @postulations = []
      publications.each do |publication|
        @postulations += FactoryGirl.create_list(:minisite_postulation,rand(2),minisite_publication: publication, is_accepted: nil)
      end
    end

    after :each do
      expect(total_pending_postulations).to eq(expected_pending_postulations)
    end
  end

  context 'Close Course' do
    let(:course) { FactoryGirl.create(:course) }
    let(:company_positions_cenco) { FactoryGirl.create(:company_positions_cenco, company_position: course.company_position) }
    let(:number_vacancies) { Random.rand(2..4) }

    context 'when is enrollment_type: Enrollment::Base and has vacancies associated with Engagement::Stage' do
      let(:vacancies) {FactoryGirl.create_list(:vacancy, number_vacancies, course: course, company_positions_cenco: company_positions_cenco)}

      context 'with the same quantity of agreements and vacancies' do
        before :each do
          course.vacancies.each do |vacancy|
            enrollment = FactoryGirl.create(:enrollment_basis_with_applicant, course: course)
            engagement_stage = FactoryGirl.create(:engagement_stage, enrollment: enrollment)
            FactoryGirl.create(:engagement_agreement, vacancy: vacancy, engagement_stage: engagement_stage)
          end
        end

         it 'should change from opened to closed' do
          expect(course.close!).to be true
          expect(course.closed?).to be true
        end
      end

      context 'with different quantity of agreements and vacancies' do
        before :each  do
          FactoryGirl.create(:vacancy, course: course, company_positions_cenco: company_positions_cenco)
        end

        it 'not should change from opened to closed' do
          expect(course.closed?).to be false
        end
      end
    end

    context 'when is enrollment_type: Enrollment::Internal and has vacancies associated with Promotion::Stage' do
      let(:course_internal) { FactoryGirl.create(:course, type_enrollment: Enrollment::Internal) }
      let(:vacancies) {FactoryGirl.create_list(:vacancy, number_vacancies, course: course_internal, company_positions_cenco: company_positions_cenco)}

      context 'with the same quantity of Promotion::Stage and vacancies' do
        before :each do
          course_internal.vacancies.each do |vacancy|
            enrollment = FactoryGirl.create(:enrollment_basis_with_applicant, course: course_internal)
            FactoryGirl.create(:promotion_stage, enrollment: enrollment)
          end
        end

        it 'should change from opened to closed' do
          expect(course_internal.close!).to be true
          expect(course_internal.closed?).to be true
        end
      end

      context 'with different quantity Promotion::Stage and vacancies' do
        before :each  do
          FactoryGirl.create(:vacancy, course: course, company_positions_cenco: company_positions_cenco)
        end

        it 'not should change from opened to closed' do
          expect(course.closed?).to be false
        end
      end
    end

    context 'actions on enrollments' do
      before :each  do
        @enrollments = FactoryGirl.create_list(:enrollment_basis_with_applicant, 4, course: course)
        @trigger_close_course_discard_reason = FactoryGirl.create(:trigger_close_course_discard_reason)
        @discard_reason = FactoryGirl.create(:enrollment_discard_reason)
      end

      it 'should discard all enrollments' do
        expect{
          course.close!
        }.to change(Enrollment::Discarding, :count).by(course.enrollments.length)

        @enrollments.map(&:reload)
        @enrollments.each do |enrollment|
          expect(enrollment.discarding.discard_reason).to eq @trigger_close_course_discard_reason.resourceable
        end
      end

      context 'when enrollments discarded already' do
        let(:discard_reason){ FactoryGirl.create(:enrollment_discard_reason) }

        before :each do
          @discardings = []
          @enrollments.each do |enrollment|
            @discardings.push(FactoryGirl.create(:enrollment_discarding,
              enrollment: enrollment,
              discard_reason: discard_reason
            ))
          end
        end

        it 'should keep older discarding' do
        end

        context 'and there is a Triger::CloseCourseDiscardReason' do
          before :each do
            FactoryGirl.create(:trigger_close_course_discard_reason)
          end

          it 'should keep older discarding' do
          end
        end

        after :each do
          course.close!
          enrollment_discarding_ids = course.enrollments.map(&:discarding).map(&:id)
          expect(enrollment_discarding_ids).to match_array @discardings.map(&:id)
        end
      end

      context 'when some enrollments has an agreement' do
        it 'should discard some enrollments and others not' do
          enrollment_with_agreement = @enrollments[0..1]

          enrollment_with_agreement.each do |enrollment|
            engagement_stage = FactoryGirl.create(:engagement_stage, enrollment: enrollment)
            FactoryGirl.create(:engagement_agreement, engagement_stage: engagement_stage)
          end

          total_discardings = course.enrollments.length - enrollment_with_agreement.length
          expect{
            course.close!
          }.to change(Enrollment::Discarding, :count).by(total_discardings)

        end
      end
    end

    context 'actions on vacancies' do
      # No se puede puede eliminar vacantes al cerrar proceso porque se requiere
      # que sean completadas sí o sí
      let(:company_positions_cenco) { FactoryGirl.create(:company_positions_cenco, company_position: course.company_position) }
      let(:number_vacancies) { Random.rand(2..5) }

      before :each do
        FactoryGirl.create_list(:vacancy, number_vacancies, course: course, company_positions_cenco: company_positions_cenco)
      end

      context 'when all vacancies are not asociated to hiring_request_record or has been closed and course opened' do
        it 'should raise_error AASM::InvalidTransition and not delete vacancies' do
          expect{
            course.close!
          }.to change(course.vacancies, :count).by(0).and \
               raise_error AASM::InvalidTransition
          expect(course.closed?).to be false
        end
      end

      context 'when some vacancies are asociated to hiring_request_record' do
        let(:hiring_request_record) { FactoryGirl.create(:hiring_request_record, number_vacancies: rand(1..3)) }

        before :each do
          hiring_request_record.accept!
          hiring_request_record.vacancies.update_all(course_id: course.id)
        end

        it 'should not delete and disasociated vacancies and raise_error AASM::InvalidTransition' do
          expect{
            course.close!
          }.to change(course.vacancies, :count).by(0).and \
               raise_error AASM::InvalidTransition
          expect(course.closed?).to be false
        end

        context 'and others are closed' do
          let(:number_agreement) { rand(1..3) }

          before :each do
            vacancies = FactoryGirl.create_list(
              :vacancy, number_agreement,
              course: course,
              company_positions_cenco: company_positions_cenco
            )
            vacancies.each do |vacancy|
              enrollment = FactoryGirl.create(:enrollment_basis_with_applicant, course: course)
              engagement_stage = FactoryGirl.create(:engagement_stage, enrollment: enrollment)
              FactoryGirl.create(:engagement_agreement, vacancy: vacancy, engagement_stage: engagement_stage)
            end
          end

          it 'should not delete, not desasociated and not keep vacancies and raise_error AASM::InvalidTransition' do
            expect{
              course.close!
            }.to change(course.vacancies, :count).by(0).and \
               raise_error AASM::InvalidTransition
            expect(course.closed?).to be false
          end
        end
      end

      context 'when some vacancies are asociated to offer_letter' do
        let(:enrollment) {FactoryGirl.create(:enrollment_basis_with_applicant, course: course)}

        it 'should mark as closed those vacancies' do
          offer_stage = FactoryGirl.create(:offer_stage, enrollment: enrollment)

          FactoryGirl.create(:offer_letter, vacancy: course.vacancies.first, stage: offer_stage)

          expect{
            course.close!
          }.to change(course.vacancies, :count).by(0).and \
               raise_error AASM::InvalidTransition
          expect(course.closed?).to be false
        end
      end
    end


    context 'actions on publications' do
      let(:company_positions_cenco) { FactoryGirl.create(:company_positions_cenco, company_position: course.company_position) }

      context 'minisite publications' do
        before :each do
          FactoryGirl.create(:vacancy, course: course, company_positions_cenco: company_positions_cenco)
          minisite_publications = FactoryGirl.create_list(:minisite_publication, 3, course: course)

          minisite_publications.each do |publication|
            FactoryGirl.create(:minisite_postulation, minisite_publication: publication, is_accepted: nil)
            FactoryGirl.create(:minisite_postulation, minisite_publication: publication, is_accepted: true)
            FactoryGirl.create(:minisite_postulation, minisite_publication: publication, is_accepted: false)
          end

          course.vacancies.each do |vacancy|
            FactoryGirl.create(:engagement_agreement, vacancy: vacancy)
          end
        end

        it 'should skip closed publications' do
          minisite_publication_closed = FactoryGirl.create(:minisite_publication, course: course)
          FactoryGirl.create(:minisite_postulation, minisite_publication: minisite_publication_closed)
          minisite_publication_closed.close!
          expect{course.send('close!')}.not_to raise_error
        end

        it 'should close all all publications' do
          course.close!
          course.minisite_publications.each do |publication|
            expect(publication.closed?).to be true
            # Se verifica que todas las postulaciones al proceso
            # se mantuvieron como estaban (las 3 que habían, una nil, otra true y otra false)
            expect(publication.postulations.map(&:is_accepted).uniq).to match_array [nil, true, false]
          end
        end
      end

    end
  end

  describe 'hired_candidates' do
    let(:work_flow){[
      {type: :module, class_name: 'Onboarding::Stage', name: 'Onboarding'},
      {type: :module, class_name: 'Engagement::Stage', name: 'Etapa'}
    ]}
    let(:internal_work_flow){Enrollment::Internal::WORK_FLOW}
    let(:course){FactoryGirl.create(:course, work_flow: work_flow)}
    let(:enrollments){FactoryGirl.create_list(:enrollment_basis_with_applicant, 3, course: course)}
    
    context 'when there is not hired candidates' do
      context 'and course is not an internal work_flow' do
        it 'should return an empty array' do
        end
      end

      context 'and course is an internal work_flow' do
        let(:course){FactoryGirl.create(:course, type_enrollment: "Enrollment::Internal", work_flow: internal_work_flow)}
        let(:enrollments){FactoryGirl.create_list(:enrollment_internal_with_applicant, 3, course: course)}
        
        it 'should return an empty array' do
        end
      end
      
      after :each do
        expect(course.hired_candidates).to be_empty
        expect(course.hired_candidates.count).to eq 0
        expect(course.hired_candidates?).to eq false
      end

    end
    
    context 'when there is not hired candidates' do
      context 'and course is not an internal work_flow' do
        before :each do
          enrollments.each do |enrollment|
            enrollment.next_stage
            expect(enrollment.stage).to be_kind_of Onboarding::Stage
            enrollment.next_stage
            expect(enrollment.stage).to be_kind_of Engagement::Stage
            FactoryGirl.create(:engagement_agreement, engagement_stage: enrollment.stage)
            expect(enrollment.stage.is_hired?).to be true
          end
        end
        
        it 'should return all hired candidates' do
        end
      end

      context 'and course is an internal work_flow' do
        let(:course){FactoryGirl.create(:course, type_enrollment: "Enrollment::Internal", work_flow: internal_work_flow)}
        let(:enrollments){FactoryGirl.create_list(:enrollment_internal_with_applicant, 3, course: course)}
        
        before :each do
          enrollments.each do |enrollment|
            enrollment.next_stage
            expect(enrollment.stage).to be_kind_of SkillInterview::Stage
            enrollment.next_stage
            expect(enrollment.stage).to be_kind_of AssigningWeighting::Stage
            enrollment.next_stage
            enrollment.stage.update(is_approved: true)
            expect(enrollment.stage).to be_kind_of Promotion::Stage
            expect(enrollment.is_hired?).to be true
          end
        end

        it 'should return all hired candidates' do
        end
      end

      after :each do
        expect(course.hired_candidates).not_to be_empty
        expect(course.hired_candidates.count).to eq 3
        expect(course.hired_candidates?).to eq true
        course.hired_candidates.each do |candidate|
          expect(candidate).to be_kind_of Applicant::Base
        end
      end
    end
  end

  describe "#destroy" do
    it "should remove everything related" do
      user = FactoryGirl.create(:user)
      user.add_role :admin

      expect{
        course = FactoryGirl.create(:course)
        minisite_publications = FactoryGirl.create_list(:minisite_publication, 3, course_id: course.id) do |minisite_publication|
          FactoryGirl.create_list(:minisite_question, 3, minisite_publication_id: minisite_publication.id)
        end

        minisite_publications.each do |minisite_publication|
          minisite_postulations = FactoryGirl.build_list(:minisite_postulation, 3, minisite_publication_id: minisite_publication.id) do |minisite_postulation|
            questions = minisite_postulation.minisite_publication.questions
            questions.each do |question|
               minisite_postulation.answers << FactoryGirl.build(:minisite_answer,
                                                                 minisite_postulation_id: minisite_postulation.id,
                                                                 minisite_question_id: question.id)
            end
            minisite_postulation.save
          end
        end

        course.create_activity key: 'course.create', owner: user
      }.to change(Course, :count).by(1).and \
           change(Minisite::Publication, :count).by(3).and \
           change(Minisite::Postulation, :count).by(9).and \
           change(Minisite::Question, :count).by(9).and \
           change(Applicant::Base, :count).by(9).and \
           change(PublicActivity::Activity, :count).by(1).and \
           change(user.activities, :count).by(1)

      course = Course.last
      enrollments = FactoryGirl.create_list(:enrollment_basis_with_applicant, 3, course: course)
      enrollment_ids = enrollments.map(&:id)
      applicants = enrollments.map(&:applicant)
      expect{
        enrollments.first.next_stage # Interview::Stage
        key_activity = enrollments.first.stage.class.to_s.tableize.singularize.gsub('/','_').gsub('/','_').concat('.created')
        course.create_activity key: 'course.'.concat(key_activity), owner: user, recipient: enrollments.first


        enrollments.first.next_stage # Offer::Stage
        key_activity = enrollments.first.stage.class.to_s.tableize.singularize.gsub('/','_').gsub('/','_').concat('.created')
        course.create_activity key: 'course.'.concat(key_activity), owner: user, recipient: enrollments.first

        enrollments.second.next_stage # Intervew::Stage
        key_activity = enrollments.second.stage.class.to_s.tableize.singularize.gsub('/','_').gsub('/','_').concat('.created')
        course.create_activity key: 'course.'.concat(key_activity), owner: user, recipient: enrollments.second
      }.to change(Stage::Base, :count).by(3).and \
           change(PublicActivity::Activity, :count).by(3).and \
           change(user.activities, :count).by(3)

      expect(user.activities.count).to eq 4

      stages = Stage::Base.last(3)
      Delayed::Job.destroy_all

      expect{
        # event was yesterday
        yesterday = Time.now - 1.day
        event = FactoryGirl.create(:calendar_event, starts_at: yesterday, created_by: user, stages: stages)
      }.to change(Calendar::Event, :count).by(1).and \
           change(Calendar::EventResourceable, :count).by(3).and \
           change(Delayed::Job, :count).by(4) # 3 stages y el manager

      expect{
        course.destroy
      }.to change(Course, :count).by(-1).and \
           change(Minisite::Publication, :count).by(-3).and \
           change(Minisite::Postulation, :count).by(-9).and \
           change(Minisite::Question, :count).by(-9).and \
           change(Applicant::Base, :count).by(0).and \
           change(Enrollment::Base, :count).by(-3).and \
           change(Stage::Base, :count).by(-3).and \
           change(PublicActivity::Activity, :count).by(-4).and \
           change(user.activities, :count).by(-4).and \
           change(Calendar::Event, :count).by(0).and \
           change(Calendar::EventResourceable, :count).by(-3).and \
           change(Delayed::Job, :count).by(0)
    end
  end
end
