require 'rails_helper'

RSpec.describe PsycholaboralEvaluation::Stage, :type => :model do
  context "RelationShips" do
    it 'should has_one psycholaboral_evaluation_report' do
      should have_one(:psycholaboral_evaluation_report).dependent(:destroy)
    end
  end

  context 'Create' do
    it 'a valid psycholaboral_evaluation' do
      psycholaboral_evaluation_stage = FactoryGirl.create(:psycholaboral_evaluation_stage)
      expect(psycholaboral_evaluation_stage).to be_valid
      expect(psycholaboral_evaluation_stage).to be_a(PsycholaboralEvaluation::Stage)
    end
  end

  describe 'Not apply' do
    let(:not_apply_reason) { FactoryGirl.create(:stage_not_apply_reason) }

    context 'when action can be done' do
      it 'should create a Stage::NotApplying' do
        stage = FactoryGirl.create(:psycholaboral_evaluation_stage)
        expect{
          stage.not_apply(not_apply_reason, FFaker::Lorem.sentence)
        }.to change(Stage::NotApplying, :count).by(1)
      end
    end

    context 'when action can not be done' do
      it 'should not create a Stage::NotApplying' do
        psycholaboral_evaluation_report = FactoryGirl.create(:psycholaboral_evaluation_report)
        stage = psycholaboral_evaluation_report.psycholaboral_evaluation_stage
        expect{
          stage.not_apply(not_apply_reason, FFaker::Lorem.sentence)
        }.to change(Stage::NotApplying, :count).by(0)

        expect(stage.errors[:not_applying]).to_not be_empty
      end
    end
  end
end
