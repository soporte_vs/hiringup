require "rails_helper"

RSpec.describe Trabajando::PostulationMailer, type: :mailer do
  describe "send_get_postulations_notification" do
    let(:user) { FactoryGirl.create(:user) }
    let(:trabajando_publication) { FactoryGirl.create(:trabajando_publication) }
    let(:mail) { Trabajando::PostulationMailer.send_get_postulations_notification(user, trabajando_publication) }

    it "renders the headers" do
      # expect(mail.subject).to eq("Postulaciones actualizadas")
      expect(mail.subject).to eq I18n.t('trabajando.postulation_mailer.send_get_postulations_notification.subject')
      expect(mail.to).to eq([trabajando_publication.course.user.email])
    end
  end

end
