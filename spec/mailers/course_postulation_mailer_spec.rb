require "rails_helper"

RSpec.describe CoursePostulationMailer, type: :mailer do

  describe "accept" do
    let(:course_postulation) { FactoryGirl.create(:minisite_postulation).course_postulation }
    let(:mail) { CoursePostulationMailer.accept(course_postulation.postulationable) }

    it "renders the headers" do
      expect(mail.subject).to eq I18n.t("course_postulation_mailer.accept.subject")
      expect(mail.to).to eq([course_postulation.postulationable.applicant.email])
    end
  end

  describe "reject" do
    let(:course_postulation) { FactoryGirl.create(:minisite_postulation).course_postulation  }
    let(:mail) { CoursePostulationMailer.reject(course_postulation.postulationable) }

    it "renders the headers" do
      expect(mail.subject).to eq I18n.t("course_postulation_mailer.reject.subject")
      expect(mail.to).to eq([course_postulation.postulationable.applicant.email])
    end
  end
end
