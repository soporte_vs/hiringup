require "rails_helper"

RSpec.describe Onboarding::StageMailer, type: :mailer do
  describe "send_upload_documentation_request" do
    let(:onboarding_stage) { FactoryGirl.create(:onboarding_stage) }
    let(:mail) { Onboarding::StageMailer.send_upload_documentation_request(onboarding_stage) }

    it "renders the headers" do
      expect(mail.subject).to eq I18n.t("onboarding.stage_mailer.send_upload_documentation_request.subject")
      expect(mail.to).to eq([onboarding_stage.enrollment.applicant.email])
    end

    it "renders the body with token" do
      token = Digest::MD5.new
      token << onboarding_stage.enrollment.applicant.email
      token = "#{token}-#{onboarding_stage.enrollment.id}"
      expect(mail.body.parts.first.body).to match(token)
    end
  end

  describe "send_upload_documentation_request_attachements" do
    let(:document_descriptor) { FactoryGirl.create(:document_descriptor_only_onboarding) }
    let(:onboarding_stage) { FactoryGirl.create(:onboarding_stage) }

    before(:each) do
      FactoryGirl.create(:generic_attachment, resourceable: document_descriptor)
      onboarding_stage.enrollment.course.update(document_group: document_descriptor.document_groups.last)
      @mail = Onboarding::StageMailer.send_upload_documentation_request(onboarding_stage)
    end

    context "send mail with attachment" do
      it "send mail with attachment only onboarding" do
        expect(@mail.to).to eq([onboarding_stage.enrollment.applicant.email])
        # Se espera que sean tantos como archivos hayan asociados
        # Además del la ficha de personal de integra
        expect(@mail.attachments.count).to eq(2)
      end
    end

    context "not send mail with attachment" do
      after(:each) do
        mail = Onboarding::StageMailer.send_upload_documentation_request(onboarding_stage)
        # Se adjunta la ficha de personal de integra
        expect(mail.attachments.count).to eq(1)
      end

      it "document descriptor internal" do
        document_descriptor.update(internal: true, only_onboarding: false)
      end

      it "document descriptor internal and only_onboarding" do
        document_descriptor.update(internal: true, only_onboarding: true)
      end

      it "document descriptor not interal not only_onboarding" do
        document_descriptor.update(internal: false, only_onboarding: false)
      end

      it "document descriptor nil" do
        document_descriptor.update(internal: nil, only_onboarding: nil)
      end
    end
  end

  describe "documents_request" do
    let(:document_descriptors) { FactoryGirl.create_list(:document_descriptor_only_onboarding, 1) }
    let(:onboarding_stage) { FactoryGirl.create(:onboarding_stage) }
    let(:mail) { Onboarding::StageMailer.documents_request(onboarding_stage, document_descriptors, FFaker::Lorem.sentence) }

    it "renders the headers" do
      expect(mail.subject).to eq I18n.t("onboarding.stage_mailer.documents_request.subject")
      expect(mail.to).to eq([onboarding_stage.enrollment.applicant.email])
    end

    it "renders the body with token" do
      token = Digest::MD5.new
      token << onboarding_stage.enrollment.applicant.email
      token = "#{token}-#{onboarding_stage.enrollment.id}"
      expect(mail.body.encoded).to match("#{token}")
    end
  end
end
