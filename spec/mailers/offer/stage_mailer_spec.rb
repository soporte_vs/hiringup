require "rails_helper"

RSpec.describe Offer::StageMailer, type: :mailer do
  describe "send_notification_to_psychologist" do
    let(:offer_letter) { FactoryGirl.create(:offer_letter) }
    let(:mail) { Offer::StageMailer.send_notification_to_psychologist(offer_letter) }
    let(:applicant) { stage.enrollment.applicant }

    it "renders de headers" do
      expect(mail.subject).to eq I18n.t("offer.stage_mailer.send_notification_to_psychologist.subject")
      expect(mail.to).to include offer_letter.stage.enrollment.course.user.email
    end

    # Si funciona pero el match no lo logra hacer correctamente
    # it "should have 'ha aceptado' in body if accepted is true" do
    #   offer_letter.update(accepted: true)
    #   expect(mail.body.encoded).to match("ha aceptado")
    # end

    # Si funciona pero el match no lo logra hacer correctamente
    # it "should have 'ha rechazado' in body if accepted is not true" do
    #   offer_letter.update(accepted: false)
    # end
  end
end
