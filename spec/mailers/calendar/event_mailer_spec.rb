require "rails_helper"

RSpec.describe Calendar::EventMailer, type: :mailer do
  let(:course) { FactoryGirl.create(:course) }
  let(:enrollments) { FactoryGirl.create_list(:enrollment_basis_with_applicant, 1, course: course) }
  let(:applicants) { enrollments.map(&:applicant) }
  let(:stages) { enrollments.map(&:next_stage) }
  let(:event) { FactoryGirl.create(:calendar_event, stages: stages)}
  let(:guests) {
    [
      {name: FFaker::Name.name, email: FFaker::Internet.email},
      {name: FFaker::Name.name, email: FFaker::Internet.email}
    ]
  }

  # Create
  describe "send_create_notification_to_stage" do
    let(:stage) { stages.first }
    let(:mail) { Calendar::EventMailer.send_create_notification_to_stage(event, stage) }

    it "renders the headers" do
      expect(mail.subject).to eq I18n.t("calendar.event_mailer.send_create_notification_to_stage.subject")
      expect(mail.to).to eq([stage.enrollment.applicant.email])
    end

    # it "renders the body" do
    #   expect(mail.body.encoded).to match("Hi")
    # end
  end

  describe "send_create_notification_to_applicant" do
    let(:applicant) { applicants.first }
    let(:mail) { Calendar::EventMailer.send_create_notification_to_applicant(event, applicant) }

    it "renders the headers" do
      expect(mail.subject).to eq I18n.t("calendar.event_mailer.send_create_notification_to_applicant.subject")
      expect(mail.to).to eq([applicant.email])
    end

    # it "renders the body" do
    #   expect(mail.body.encoded).to match("Hi")
    # end
  end

  describe "send_create_notification_to_manager" do
    let(:mail) { Calendar::EventMailer.send_create_notification_to_manager(event) }

    it "renders the headers" do
      expect(mail.subject).to eq I18n.t("calendar.event_mailer.send_create_notification_to_manager.subject")
      expect(mail.to).to eq([event.manager.email])
    end

    # it "renders the body" do
    #   expect(mail.body.encoded).to match("Hi")
    # end
  end

  describe "send_create_notification_to_guest" do
    let(:guest) { guests.first }
    let(:mail) { Calendar::EventMailer.send_create_notification_to_guest(event, guest) }

    it "renders the headers" do
      expect(mail.subject).to eq I18n.t("calendar.event_mailer.send_create_notification_to_guest.subject")
      expect(mail.to).to eq([guest[:email]])
    end

    # it "renders the body" do
    #   expect(mail.body.encoded).to match("Hi")
    # end
  end

  # Remove
  describe "send_removed_notification_to_stage" do
    let(:stage) { stages.first }
    let(:mail) { Calendar::EventMailer.send_removed_notification_to_stage(event, stage) }

    it "renders the headers" do
      expect(mail.subject).to eq I18n.t("calendar.event_mailer.send_removed_notification_to_stage.subject")
      expect(mail.to).to eq([stage.enrollment.applicant.email])
    end

    # it "renders the body" do
    #   expect(mail.body.encoded).to match("Hi")
    # end
  end

  describe "send_removed_notification_to_applicant" do
    let(:applicant) { applicants.first }
    let(:mail) { Calendar::EventMailer.send_removed_notification_to_applicant(event, applicant) }

    it "renders the headers" do
      expect(mail.subject).to eq I18n.t("calendar.event_mailer.send_removed_notification_to_applicant.subject")
      expect(mail.to).to eq([applicant.email])
    end

    # it "renders the body" do
    #   expect(mail.body.encoded).to match("Hi")
    # end
  end

  describe "send_removed_notification_to_manager" do
    let(:old_manager_name) { FFaker::Name.first_name }
    let(:old_manager_email) { FFaker::Internet.email }
    let(:mail) { Calendar::EventMailer.send_removed_notification_to_manager(event, old_manager_name, old_manager_email) }

    it "renders the headers" do
      expect(mail.subject).to eq I18n.t("calendar.event_mailer.send_removed_notification_to_manager.subject")
      expect(mail.to).to eq([old_manager_email])
    end

    # it "renders the body" do
    #   expect(mail.body.encoded).to match("Hi")
    # end
  end

  describe "send_removed_notification_to_guest" do
    let(:guest) { guests.first }
    let(:mail) { Calendar::EventMailer.send_removed_notification_to_guest(event, guests.first[:name], guests.first[:email]) }

    it "renders the headers" do
      expect(mail.subject).to eq I18n.t("calendar.event_mailer.send_removed_notification_to_guest.subject")
      expect(mail.to).to eq([guest[:email]])
    end

    # it "renders the body" do
    #   expect(mail.body.encoded).to match("Hi")
    # end
  end


  # Updates
  describe "send_updates_notification_to_stage" do
    let(:stage) { stages.first }
    let(:mail) { Calendar::EventMailer.send_updates_notification_to_stage(event, stage) }

    it "renders the headers" do
      expect(mail.subject).to eq I18n.t("calendar.event_mailer.send_updates_notification_to_stage.subject")
      expect(mail.to).to eq([stage.enrollment.applicant.email])
    end

    # it "renders the body" do
    #   expect(mail.body.encoded).to match("Hi")
    # end
  end

  describe "send_updates_notification_to_applicant" do
    let(:applicant) { applicants.first }
    let(:mail) { Calendar::EventMailer.send_updates_notification_to_applicant(event, applicant) }

    it "renders the headers" do
      expect(mail.subject).to eq I18n.t("calendar.event_mailer.send_updates_notification_to_applicant.subject")
      expect(mail.to).to eq([applicant.email])
    end

    # it "renders the body" do
    #   expect(mail.body.encoded).to match("Hi")
    # end
  end

  describe "send_updates_notification_to_manager" do
    let(:mail) { Calendar::EventMailer.send_updates_notification_to_manager(event) }

    it "renders the headers" do
      expect(mail.subject).to eq I18n.t("calendar.event_mailer.send_updates_notification_to_manager.subject")
      expect(mail.to).to eq([event.manager.email])
    end

    # it "renders the body" do
    #   expect(mail.body.encoded).to match("Hi")
    # end
  end

  describe "send_updates_notification_to_guest" do
    let(:guest) { guests.first }
    let(:mail) { Calendar::EventMailer.send_updates_notification_to_guest(event, guest) }

    it "renders the headers" do
      expect(mail.subject).to eq I18n.t("calendar.event_mailer.send_updates_notification_to_guest.subject")
      expect(mail.to).to eq([guest[:email]])
    end

    # it "renders the body" do
    #   expect(mail.body.encoded).to match("Hi")
    # end
  end

  # Updates with resourceables
  describe "send_updates_notification_to_manager_with_updated_stages" do
    let(:mail) { Calendar::EventMailer.send_updates_notification_to_manager_with_updated_stages(event) }

    it "renders the headers" do
      expect(mail.subject).to eq I18n.t("calendar.event_mailer.send_updates_notification_to_manager_with_updated_stages.subject")
      expect(mail.to).to eq([event.manager.email])
    end

    # it "renders the body" do
    #   expect(mail.body.encoded).to match("Hi")
    # end
  end

  describe "send_updates_notification_to_manager_with_updated_applicants" do
    let(:mail) { Calendar::EventMailer.send_updates_notification_to_manager_with_updated_applicants(event) }

    it "renders the headers" do
      expect(mail.subject).to eq I18n.t("calendar.event_mailer.send_updates_notification_to_manager_with_updated_applicants.subject")
      expect(mail.to).to eq([event.manager.email])
    end

    # it "renders the body" do
    #   expect(mail.body.encoded).to match("Hi")
    # end
  end

  describe "send_updates_notification_to_guest_with_updated_stages" do
    let(:guest) { guests.first }
    let(:mail) { Calendar::EventMailer.send_updates_notification_to_guest_with_updated_stages(event, guest) }

    it "renders the headers" do
      expect(mail.subject).to eq I18n.t("calendar.event_mailer.send_updates_notification_to_guest_with_updated_stages.subject")
      expect(mail.to).to eq([guest[:email]])
    end

    # it "renders the body" do
    #   expect(mail.body.encoded).to match("Hi")
    # end
  end

  describe "send_updates_notification_to_guest_with_updated_applicants" do
    let(:guest) { guests.first }
    let(:mail) { Calendar::EventMailer.send_updates_notification_to_guest_with_updated_applicants(event, guest) }

    it "renders the headers" do
      expect(mail.subject).to eq I18n.t("calendar.event_mailer.send_updates_notification_to_guest_with_updated_applicants.subject")
      expect(mail.to).to eq([guest[:email]])
    end

    # it "renders the body" do
    #   expect(mail.body.encoded).to match("Hi")
    # end
  end

end
