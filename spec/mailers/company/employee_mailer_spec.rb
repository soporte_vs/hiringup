require "rails_helper"

RSpec.describe Company::EmployeeMailer, type: :mailer do

  describe "report_load_employees" do
    let(:invalido) { FFaker::Internet.email }
    let(:mail) { Company::EmployeeMailer.report_load_employees(invalido) }

    it "renders the headers" do
      expect(mail.subject).to eq I18n.t("company.employee_mailer.report_load_employees.subject")
      expect(mail.to).to eq([invalido])
    end
  end
end
