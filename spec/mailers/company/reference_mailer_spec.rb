require "rails_helper"

RSpec.xdescribe Company::ReferenceMailer, type: :mailer do
  let(:company_reference) { FactoryGirl.create(:company_reference) }

  describe "send_notice_to_employee" do
    let(:mail) { Company::ReferenceMailer.send_notice_to_employee(company_reference) }

    it "renders the headers" do
      expect(mail.subject).to eq I18n.t("company.reference_mailer.send_notice_to_employee.subject")
    end

    it "sends email to company_employee" do
      expect(mail.to).to eq([company_reference.email])
    end
  end

  describe "send_notice_to_applicant" do
    let(:mail) { Company::ReferenceMailer.send_notice_to_applicant(company_reference.applicant) }

    it "renders the headers" do
      expect(mail.subject).to eq I18n.t("company.reference_mailer.send_notice_to_applicant.subject")
    end

    it "sends email to the applicant" do
      expect(mail.to).to eq([company_reference.applicant.email])
    end
  end
end
