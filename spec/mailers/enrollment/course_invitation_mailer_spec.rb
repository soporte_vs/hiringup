require "rails_helper"

RSpec.describe Enrollment::CourseInvitationMailer, type: :mailer do
  describe "send_course_invitation" do
    let(:invitation) { FactoryGirl.create(:enrollment_course_invitation) }
    let(:enrollment) { invitation.enrollment }
    let(:mail) { Enrollment::CourseInvitationMailer.send_course_invitation(enrollment) }

    it "renders the headers" do
      expect(mail.subject).to eq I18n.t("enrollment.course_invitation_mailer.send_course_invitation.subject")
      expect(mail.to).to eq([enrollment.applicant.email])
    end
  end
end
