require "rails_helper"

RSpec.describe Enrollment::DiscardingMailer, type: :mailer do

  describe "discard" do
    let(:enrollment) { FactoryGirl.create(:enrollment_basis_with_applicant) }
    let(:mail) { Enrollment::DiscardingMailer.discard(enrollment) }

    it "renders the headers" do
      expect(mail.subject).to eq I18n.t("enrollment.discarding_mailer.discard.subject")
      expect(mail.to).to eq([enrollment.applicant.email])
    end
  end

  describe "revert_discard" do
    let(:enrollment) { FactoryGirl.create(:enrollment_basis_with_applicant) }
    let(:mail) { Enrollment::DiscardingMailer.revert_discard(enrollment) }

    it "renders the headers" do
      expect(mail.subject).to eq I18n.t("enrollment.discarding_mailer.revert_discard.subject")
      expect(mail.to).to eq([enrollment.applicant.email])
    end
  end
end
