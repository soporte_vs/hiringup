# Preview all emails at http://localhost:3000/rails/mailers/errors_mailer
class ErrorsMailerPreview < ActionMailer::Preview
  def errors_developers
    exception = nil
    begin
      underscore = "asdasdasd_stage_path"
      Rails.application.routes.url_helpers.send(underscore, id: 1, stage_id: 2)
    rescue Exception => e
      exception = e
    end
    object = FactoryGirl.create(:interview_stage, name: "Entrevista")
    ErrorsMailer.errors_developers("Error en", object, e)
  end
end
