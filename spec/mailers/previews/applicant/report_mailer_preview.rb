# Preview all emails at http://localhost:3000/rails/mailers/applicant/report
class Applicant::ReportPreview < ActionMailer::Preview
  # Preview this email at http://localhost:3000/rails/mailers/applicant/report/send_to_user
  def send_to_user
    applicants = Applicant::Catched.last(5)
    first_name = applicants.map(&:first_name).first
    report_params = {
      :applicant=>{
        :first_name=>first_name,
        :last_name=>"",
        :sex=>"",
        :country=>"",
        :state=>"",
        :city=>"",
        :degree_name=>"",
        :degree_condition=>""
      },
      :course=>{
        :country=>"",
        :state=>"",
        :city=>"",
        :engagement_origin_ids=> []
      }
    }
    report = Applicant::Report.new(report_params)
    user = User.last || FactoryGirl.create(:user)
    Applicant::ReportMailer.send_to_user(user, report)
  end

end
