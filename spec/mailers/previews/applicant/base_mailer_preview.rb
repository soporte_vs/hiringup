# Preview all emails at http://localhost:3000/rails/mailers/applicant/update_mail2
class Applicant::BaseMailerPreview < ActionMailer::Preview
  def send_update_request
    @applicant = Applicant::Base.last || FactoryGirl.create(:applicant_base)
    Applicant::BaseMailer.send_update_request(@applicant)
  end

  def massive_mailer
    @reply_to = FFaker::Internet.email
    @message = FFaker::Lorem.sentence
    @subject = FFaker::Lorem.sentence
    @applicant = Applicant::Base.last || FactoryGirl.create(:applicant_bases)
    Applicant::BaseMailer.massive_mailer(@applicant, @reply_to, @subject, @message)
  end

end
