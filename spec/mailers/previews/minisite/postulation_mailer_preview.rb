# Preview all emails at http://localhost:3000/rails/mailers/minisite/postulation_mailer
class Minisite::PostulationMailerPreview < ActionMailer::Preview
  def update_profile
    postulation = Minisite::Postulation.last || FactoryGirl.create(:minisite_postulation)
    Minisite::PostulationMailer.update_profile(postulation)
  end
end
