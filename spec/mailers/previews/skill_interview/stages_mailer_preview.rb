# Preview all emails at http://localhost:3000/rails/mailers/skill_interview/stages_mailer
class SkillInterview::StagesMailerPreview < ActionMailer::Preview
  def send_email
    stage = SkillInterview::Stage.last || FactoryGirl.create(:skill_interview_stage)
    SkillInterview::StagesMailer.send_email(stage)
  end
end
