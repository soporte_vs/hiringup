# Preview all emails at http://localhost:3000/rails/mailers/minisite/postulation_mailer
class CoursePostulationMailerPreview < ActionMailer::Preview
  def accept
    postulation = CoursePostulation.last || FactoryGirl.create(:course_postulation)
    CoursePostulationMailer.accept(postulation.postulationable)
  end

  def reject
    postulation = CoursePostulation.last || FactoryGirl.create(:course_postulation)
    CoursePostulationMailer.reject(postulation.postulationable)
  end
end
