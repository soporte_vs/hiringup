# Preview all emails at http://localhost:3000/rails/mailers/company/reference_mailer
class Company::ReferenceMailerPreview < ActionMailer::Preview
  def send_notice_to_applicant
    @company_reference = Company::Reference.last || FactoryGirl.create(:company_reference)
    Company::ReferenceMailer.send_notice_to_applicant(@company_reference.applicant)
  end

  def send_notice_to_employee
    @company_reference = Company::Reference.last || FactoryGirl.create(:company_reference)
    Company::ReferenceMailer.send_notice_to_employee(@company_reference)
  end

end
