# Preview all emails at http://localhost:3000/rails/mailers/company/employee_mailer
class Company::EmployeeMailerPreview < ActionMailer::Preview
  def report_load_employees
    Company::EmployeeMailer.report_load_employees(FFaker::Internet.email)
  end
end
