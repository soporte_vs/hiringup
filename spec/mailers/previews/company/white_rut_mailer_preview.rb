# Preview all emails at http://localhost:3000/rails/mailers/company/white_rut_mailer
class Company::WhiteRutMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/company/white_rut_mailer/report_load_white_list
  def report_load_white_list
    errors = [[FFaker::IdentificationESCL.rut, 'Rut invalido']]
    success = [[FFaker::IdentificationESCL.rut, 'OK']]
    Company::WhiteRutMailer.report_load_white_list(FFaker::Internet.email, errors, success)
  end

end
