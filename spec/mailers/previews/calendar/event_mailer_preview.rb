# Preview all emails at http://localhost:3000/rails/mailers/calendar/event_mailer
class Calendar::EventMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/calendar/event_mailer/send_create_notification_to_stage
  def send_create_notification_to_stage
    stages = Stage::Base.last(1)

    if stages.empty?
      stages = FactoryGirl.create_list(:stage_basis, 1)
    end

    event = Calendar::Event.last
    unless event.present?
      event = FactoryGirl.create(:calendar_event, stages: stages)
    end

    guests = [
      {name: FFaker::Name.name, email: FFaker::Internet.email},
      {name: FFaker::Name.name, email: FFaker::Internet.email}
    ]

    Calendar::EventMailer.send_create_notification_to_stage(event, stages.first)
  end

  def send_create_notification_to_applicant
    applicants = Applicant::Base.last(1)

    if applicants.empty?
      applicants = FactoryGirl.create_list(:applicant_basis, 1)
    end

    event = Calendar::Event.last
    unless event.present?
      event = FactoryGirl.create(:calendar_event, applicants: applicants)
    end

    guests = [
      {name: FFaker::Name.name, email: FFaker::Internet.email},
      {name: FFaker::Name.name, email: FFaker::Internet.email}
    ]

    if event.guests.empty?
      event.guests_attributes = guests
    end


    Calendar::EventMailer.send_create_notification_to_applicant(event, applicants.first)
  end

  # Preview this email at http://localhost:3000/rails/mailers/calendar/event_mailer/send_create_notification_to_manager
  def send_create_notification_to_manager
    stages = Stage::Base.last(1)

    if stages.empty?
      stages = FactoryGirl.create_list(:stage_basis, 1)
    end

    event = Calendar::Event.last
    unless event.present?
      event = FactoryGirl.create(:calendar_event, stages: stages)
    end

    guests = [
      {name: FFaker::Name.name, email: FFaker::Internet.email},
      {name: FFaker::Name.name, email: FFaker::Internet.email}
    ]

    if event.guests.empty?
      event.guests_attributes = guests
    end

    Calendar::EventMailer.send_create_notification_to_manager(event)
  end

  # Preview this email at http://localhost:3000/rails/mailers/calendar/event_mailer/send_create_notification_to_guest
  def send_create_notification_to_guest
    stages = Stage::Base.last(1)

    if stages.empty?
      stages = FactoryGirl.create_list(:stage_basis, 1)
    end

    event = Calendar::Event.last
    unless event.present?
      event = FactoryGirl.create(:calendar_event, stages: stages)
    end

    guests = [
      {name: FFaker::Name.name, email: FFaker::Internet.email},
      {name: FFaker::Name.name, email: FFaker::Internet.email}
    ]

    if event.guests.empty?
      event.update(guests_attributes: guests)
    end

    Calendar::EventMailer.send_create_notification_to_guest(event, guests.first)
  end

  def send_removed_notification_to_stage
    stages = Stage::Base.last(1)

    if stages.empty?
      stages = FactoryGirl.create_list(:stage_basis, 1)
    end

    event = Calendar::Event.last
    unless event.present?
      event = FactoryGirl.create(:calendar_event, stages: stages)
    end

    guests = [
      {name: FFaker::Name.name, email: FFaker::Internet.email},
      {name: FFaker::Name.name, email: FFaker::Internet.email}
    ]

    if event.guests.empty?
      event.guests_attributes = guests
    end
    Calendar::EventMailer.send_removed_notification_to_stage(event, stages.first)
  end

  def send_removed_notification_to_applicant
    applicants = Applicant::Base.last(1)

    if applicants.empty?
      applicants = FactoryGirl.create_list(:applicant_basis, 1)
    end

    event = Calendar::Event.last
    unless event.present?
      event = FactoryGirl.create(:calendar_event, applicants: applicants)
    end

    guests = [
      {name: FFaker::Name.name, email: FFaker::Internet.email},
      {name: FFaker::Name.name, email: FFaker::Internet.email}
    ]

    if event.guests.empty?
      # event.guests_attributes = guests
    end
    Calendar::EventMailer.send_removed_notification_to_applicant(event, applicants.first)
  end

  def send_removed_notification_to_guest
    stages = Stage::Base.last(1)

    if stages.empty?
      stages = FactoryGirl.create_list(:stage_basis, 1)
    end

    event = Calendar::Event.last
    unless event.present?
      event = FactoryGirl.create(:calendar_event, stages: stages)
    end

    guests = [
      {name: FFaker::Name.name, email: FFaker::Internet.email},
      {name: FFaker::Name.name, email: FFaker::Internet.email}
    ]

    if event.guests.empty?
      event.guests_attributes = guests
    end

    Calendar::EventMailer.send_removed_notification_to_guest(event, guests.first[:name], guests.first[:email])
  end

  def send_removed_notification_to_manager
    stages = Stage::Base.last(1)

    if stages.empty?
      stages = FactoryGirl.create_list(:stage_basis, 1)
    end

    event = Calendar::Event.last
    unless event.present?
      event = FactoryGirl.create(:calendar_event, stages: stages)
    end

    guests = [
      {name: FFaker::Name.name, email: FFaker::Internet.email},
      {name: FFaker::Name.name, email: FFaker::Internet.email}
    ]

    if event.guests.empty?
      event.guests_attributes = guests
    end

    Calendar::EventMailer.send_removed_notification_to_manager(event, event.manager.name, event.manager.email)
  end

  def send_updates_notification_to_manager
    stages = Stage::Base.last(1)

    if stages.empty?
      stages = FactoryGirl.create_list(:stage_basis, 1)
    end

    event = Calendar::Event.last
    unless event.present?
      event = FactoryGirl.create(:calendar_event, stages: stages)
    end

    guests = [
      {name: FFaker::Name.name, email: FFaker::Internet.email},
      {name: FFaker::Name.name, email: FFaker::Internet.email}
    ]

    if event.guests.empty?
      event.guests_attributes = guests
    end

    Calendar::EventMailer.send_updates_notification_to_manager(event)
  end

  def send_updates_notification_to_manager_with_updated_stages
    stages = Stage::Base.last(1)

    if stages.empty?
      stages = FactoryGirl.create_list(:stage_basis, 1)
    end

    event = Calendar::Event.last
    unless event.present?
      event = FactoryGirl.create(:calendar_event, stages: stages)
    end

    guests = [
      {name: FFaker::Name.name, email: FFaker::Internet.email},
      {name: FFaker::Name.name, email: FFaker::Internet.email}
    ]

    if event.guests.empty?
      event.guests_attributes = guests
    end

    Calendar::EventMailer.send_updates_notification_to_manager_with_updated_stages(event)
  end

  def send_updates_notification_to_stage
    stages = Stage::Base.last(1)

    if stages.empty?
      stages = FactoryGirl.create_list(:stage_basis, 1)
    end

    event = Calendar::Event.last
    unless event.present?
      event = FactoryGirl.create(:calendar_event, stages: stages)
    end

    guests = [
      {name: FFaker::Name.name, email: FFaker::Internet.email},
      {name: FFaker::Name.name, email: FFaker::Internet.email}
    ]

    if event.guests.empty?
      event.guests_attributes = guests
    end

    Calendar::EventMailer.send_updates_notification_to_stage(event, stages.first)
  end

  def send_updates_notification_to_applicant
    applicants = Applicant::Base.last(1)

    if applicants.empty?
      applicants = FactoryGirl.create_list(:applicant_basis, 1)
    end

    event = Calendar::Event.last
    unless event.present?
      event = FactoryGirl.create(:calendar_event, applicants: applicants)
    end

    guests = [
      {name: FFaker::Name.name, email: FFaker::Internet.email},
      {name: FFaker::Name.name, email: FFaker::Internet.email}
    ]

    if event.guests.empty?
      event.guests_attributes = guests
    end

    Calendar::EventMailer.send_updates_notification_to_applicant(event, applicants.first)
  end

  def send_updates_notification_to_guest
    stages = Stage::Base.last(1)

    if stages.empty?
      stages = FactoryGirl.create_list(:stage_basis, 1)
    end

    event = Calendar::Event.last
    unless event.present?
      event = FactoryGirl.create(:calendar_event, stages: stages)
    end

    guests = [
      {name: FFaker::Name.name, email: FFaker::Internet.email},
      {name: FFaker::Name.name, email: FFaker::Internet.email}
    ]

    if event.guests.empty?
      event.guests_attributes = guests
    end

    Calendar::EventMailer.send_updates_notification_to_guest(event, guests.first)
  end

  def send_updates_notification_to_guest_with_updated_stages
    stages = Stage::Base.last(1)

    if stages.empty?
      stages = FactoryGirl.create_list(:stage_basis, 1)
    end

    event = Calendar::Event.last
    unless event.present?
      event = FactoryGirl.create(:calendar_event, stages: stages)
    end

    guests = [
      {name: FFaker::Name.name, email: FFaker::Internet.email},
      {name: FFaker::Name.name, email: FFaker::Internet.email}
    ]

    if event.guests.empty?
      event.guests_attributes = guests
    end

    if event.stages.empty?
      event.stages = stages
    end

    Calendar::EventMailer.send_updates_notification_to_guest_with_updated_stages(event, guests.first)
  end

  def send_updates_notification_to_guest_with_updated_applicants
    applicants = Applicant::Base.last(1)

    if applicants.empty?
      applicants = FactoryGirl.create_list(:applicant_basis, 1)
    end

    event = nil #Calendar::Event.last
    unless event.present?
      event = FactoryGirl.create(:calendar_event, applicants: applicants)
    end

    guests = [
      {name: FFaker::Name.name, email: FFaker::Internet.email},
      {name: FFaker::Name.name, email: FFaker::Internet.email}
    ]

    if event.guests.empty?
      event.guests_attributes = guests
    end

    Calendar::EventMailer.send_updates_notification_to_guest_with_updated_applicants(event, guests.first)
  end

end
