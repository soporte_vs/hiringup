# Preview all emails at http://localhost:3000/rails/mailers/minisite/postulation_mailer
class Laborum::PostulationMailerPreview < ActionMailer::Preview
  def accept
    postulation = Laborum::Postulation.last
    postulation ||= FactoryGirl.build(:laborum_postulation)
    Laborum::PostulationMailer.accept(postulation)
  end
end
