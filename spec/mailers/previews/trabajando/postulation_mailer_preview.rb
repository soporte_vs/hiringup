# Preview all emails at http://localhost:3000/rails/mailers/trabajando/postulation_mailer
class Trabajando::PostulationMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/trabajando/postulation_mailer/send_get_postulations_notification
  def send_get_postulations_notification
    user = User.last || FactoryGirl.create(:user)
    trabajando_publication = Trabajando::Publication.last || FactoryGirl.create(:trabajando_publication)
    Trabajando::PostulationMailer.send_get_postulations_notification(user, trabajando_publication)
  end

end
