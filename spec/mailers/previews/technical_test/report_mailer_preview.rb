# Preview all emails at http://localhost:3000/rails/mailers/technical_test/report_mailer
class TechnicalTest::ReportMailerPreview < ActionMailer::Preview
  def send_report
    report = TechnicalTest::Report.last || FactoryGirl.create(:technical_test_report)
    TechnicalTest::ReportMailer.send_report(report)
  end
end
