# Preview all emails at http://localhost:3000/rails/mailers/education/institution_mailer
class Education::InstitutionMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/education/institution_mailer/notify_to_maintainer
  def notify_to_maintainer
    institution = Education::Institution.where(custom: true).last || FactoryGirl.create(:education_institution_custom)
    Education::InstitutionMailer.notify_to_maintainer(institution)
  end

end
