# Preview all emails at http://localhost:3000/rails/mailers/enrollment/base_mailer
class Enrollment::DiscardingMailerPreview < ActionMailer::Preview
  def discard
    @enrollment = Enrollment::Base.last || FactoryGirl.create(:enrollment_basis)
    Enrollment::DiscardingMailer.discard(@enrollment)
  end

  def revert_discard
    @enrollment = Enrollment::Base.last || FactoryGirl.create(:enrollment_basis)
    Enrollment::DiscardingMailer.revert_discard(@enrollment)
  end
end
