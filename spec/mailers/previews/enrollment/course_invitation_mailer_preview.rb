# Preview all emails at http://localhost:3000/rails/mailers/enrollment/course_invitation_mailer
class Enrollment::CourseInvitationMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/enrollment/course_invitation_mailer/send_course_invitation
  def send_course_invitation
    invitation = Enrollment::CourseInvitation.last || FactoryGirl.create(:enrollment_course_invitation)
    enrollment = invitation.enrollment
    Enrollment::CourseInvitationMailer.send_course_invitation(enrollment)
  end

end
