# Preview all emails at http://localhost:3000/rails/mailers/psycholaboral_evaluation/report_mailer
class PsycholaboralEvaluation::ReportMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/psycholaboral_evaluation/report_mailer/send_report
  def send_report
    report = PsycholaboralEvaluation::Report.last || FactoryGirl.create(:psycholaboral_evaluation_report)
    PsycholaboralEvaluation::ReportMailer.send_report(report)
  end

end
