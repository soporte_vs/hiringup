# Preview all emails at http://localhost:3000/rails/mailers/engagement/agreement_mailer
class Engagement::AgreementMailerPreview < ActionMailer::Preview
  def hire
    @agreement = Engagement::Agreement.last || FactoryGirl.create(:engagement_agreement)
    Engagement::AgreementMailer.hire(@agreement)
  end
end
