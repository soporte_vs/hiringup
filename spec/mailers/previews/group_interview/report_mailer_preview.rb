# Preview all emails at http://localhost:3000/rails/mailers/group_interview/report_mailer
class GroupInterview::ReportMailerPreview < ActionMailer::Preview
  def send_report
    report = GroupInterview::Report.last || FactoryGirl.create(:group_interview_report)
    GroupInterview::ReportMailer.send_report(report)
  end
end
