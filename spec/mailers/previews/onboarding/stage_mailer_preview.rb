# Preview all emails at http://localhost:3000/rails/mailers/onboarding/stage_mailer
class Onboarding::StageMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/onboarding/stage_mailer/send_upload_documentation_request
  def send_upload_documentation_request
    onboarding_stage = Onboarding::Stage.last || FactoryGirl.create(:onboarding_stage)
    Onboarding::StageMailer.send_upload_documentation_request(onboarding_stage)
  end

  def documents_request
    document_descriptors = Document::Descriptor.last(2) || FactoryGirl.create_list(:document_descriptor, 2, internal: false, only_onboarding: true)
    onboarding_stage = Onboarding::Stage.last || FactoryGirl.create(:onboarding_stage)
    Onboarding::StageMailer.documents_request(onboarding_stage, document_descriptors, FFaker::Lorem.sentence)
  end
end
