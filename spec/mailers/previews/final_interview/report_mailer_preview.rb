# Preview all emails at http://localhost:3000/rails/mailers/final_interview/report_mailer
class FinalInterview::ReportMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/final_interview/report_mailer/send_report
  def send_report
    report = FinalInterview::Report.last || FactoryGirl.create(:final_interview_report)
    FinalInterview::ReportMailer.send_report(report)
  end

end
