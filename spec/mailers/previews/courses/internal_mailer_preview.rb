  # Preview all emails at http://localhost:3000/rails/mailers/course/internal_mailer
class Courses::InternalMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/enrollment/internal_mailer/send_internal_postulation_template
  def send_internal_postulation_template
    course = Course.last || FactoryGirl.create(:course)
    minisite_publication = course.minisite_publications.first || FactoryGirl.create(:minisite_publication)
    postulation = minisite_publication.postulations.first || FactoryGirl.create(:minisite_postulation, minisite_publication: minisite_publication)
    applicant = postulation.applicant
    Courses::InternalMailer.send_internal_postulation_template(course, applicant)
  end

  def notify_internal_postulation
    enrollment = Enrollment::Internal.last || FactoryGirl.create(:enrollment_basis_with_applicant, type: Enrollment::Internal)
    Courses::InternalMailer.notify_internal_postulation(enrollment)
  end

end
