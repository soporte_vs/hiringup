# Preview all emails at http://localhost:3000/rails/mailers/info_dev_mailer
class InfoDevMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/info_dev_mailer/info
  def info
    InfoDevMailer.info(FFaker::Lorem.paragraph, FFaker::Internet.email)
  end

end
