# Preview all emails at http://localhost:3000/rails/mailers/offer_letter/stage_mailer
class Offer::StageMailerPreview < ActionMailer::Preview
  def send_offer_letter
    @offer_letter = Offer::Letter.last || FactoryGirl.create(:offer_letter)
    @enrollment = @offer_letter.enrollment
    Offer::StageMailer.send_offer_letter(@offer_letter)
  end

  def send_notification_to_psychologist
    @offer_letter = Offer::Letter.last || FactoryGirl.create(:offer_letter)
    @enrollment = @offer_letter.enrollment
    Offer::StageMailer.send_notification_to_psychologist(@offer_letter)
  end

  def send_notification_to_psychologist_accepted
    @offer_letter = Offer::Letter.last || FactoryGirl.create(:offer_letter)
    @offer_letter.accepted = true
    @offer_letter.offer_reject_reason = nil
    @offer_letter.save
    @enrollment = @offer_letter.enrollment
    Offer::StageMailer.send_notification_to_psychologist(@offer_letter)
  end

  def send_notification_to_psychologist_rejected
    @offer_reject_reason = FactoryGirl.create(:offer_reject_reason)
    @offer_letter = Offer::Letter.last || FactoryGirl.create(:offer_letter)
    @offer_letter.accepted = false
    @offer_letter.offer_reject_reason = @offer_reject_reason
    @offer_letter.save
    @enrollment = @offer_letter.enrollment
    Offer::StageMailer.send_notification_to_psychologist(@offer_letter)
  end

  def notify_course_manager
    @offer_letter = Offer::Letter.last || FactoryGirl.create(:offer_letter)
    Offer::StageMailer.notify_course_manager(@offer_letter)
  end
end
