# Preview all emails at http://localhost:3000/rails/mailers/video_interview/answer_mailer
class VideoInterview::AnswerMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/video_interview/stage_mailer/link_to_interview
  def link_to_interview
    video_interview_stage = VideoInterview::Stage.last || FactoryGirl.create(:video_interview_stage)
    user = video_interview_stage.enrollment.applicant.user
    VideoInterview::AnswerMailer.link_to_interview(user)
  end
end
