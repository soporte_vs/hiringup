class Promotion::StageMailerPreview < ActionMailer::Preview
  def send_acceptance_email
    @promotion_stage = Promotion::Stage.last || FactoryGirl.create(:promotion_stage)
    @enrollment = @promotion_stage.enrollment
    Promotion::StageMailer.send_acceptance_email(@promotion_stage)
  end

  def send_rejection_email
    @promotion_stage = Promotion::Stage.last || FactoryGirl.create(:promotion_stage)
    @enrollment = @promotion_stage.enrollment
    Promotion::StageMailer.send_rejection_email(@promotion_stage)
  end
end
