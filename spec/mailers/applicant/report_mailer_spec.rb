require "rails_helper"

RSpec.describe Applicant::ReportMailer, type: :mailer do
  describe "send_to_user" do
    let!(:enrollments) { FactoryGirl.create_list(:enrollment_basis_with_applicant, 3) }
    let(:user) { FactoryGirl.create(:user) }
    let(:report_params) {
      {
        :applicant=>{
         :first_name=>"",
         :last_name=>"",
         :sex=>"",
         :country=>"",
         :state=>"",
         :city=>"",
         :degree_name=>"",
         :degree_condition=>""
       },
       :course=>{
         :country=>"",
         :state=>"",
         :city=>"",
         :engagement_origin_ids=> Company::EngagementOrigin.pluck(:id)
       }
      }
    }
    let(:report) { Applicant::Report.create(user: user, params: report_params) }
    let(:mail) { Applicant::ReportMailer.send_to_user(user, report) }

    it "renders the headers" do
      expect(mail.subject).to eq("Informe de datos de candidatos")
      expect(mail.to).to eq([user.email])
    end
  end

end
