require "rails_helper"

RSpec.describe Applicant::BaseMailer, type: :mailer do

  describe "send_update_request" do
    let(:mail) { Applicant::BaseMailer.send_update_request(applicant) }
    let(:applicant) { FactoryGirl.create(:applicant_bases) }

    it "renders the headers" do
      expect(mail.subject).to eq I18n.t("applicant.base_mailer.send_update_request.subject")
      expect(mail.to).to eq([applicant.email])
    end
  end

  describe "massive_mailer" do
    let(:reply_to) { FFaker::Internet.email }
    let(:applicant) { FactoryGirl.create(:applicant_bases) }
    let(:message) { FFaker::Lorem.sentence }
    let(:subject) { FFaker::Lorem.sentence }
    let(:mail) { Applicant::BaseMailer.massive_mailer(applicant, reply_to, subject, message) }

    it "renders the headers" do
      expect(mail.subject).to match(subject)
      expect(mail.reply_to).to eq([reply_to])
      expect(mail.to).to eq([applicant.email])
    end

    it "renders the body" do
      expect(mail.body.encoded).to match(message)
    end
  end

end
