require "rails_helper"

RSpec.describe Education::InstitutionMailer, type: :mailer do
  describe "notify_to_maintainer" do
    let(:institution) { FactoryGirl.create(:education_institution_custom) }
    let(:mail) { Education::InstitutionMailer.notify_to_maintainer(institution) }

    it "renders the headers" do
      expect(mail.to).to eq([$COMPANY[:maintainer_email]])
    end

    it "renders the body" do
      expect(mail.body.encoded).to match(institution.name)
    end
  end
end
