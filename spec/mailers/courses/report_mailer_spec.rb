require "rails_helper"

RSpec.describe Courses::ReportMailer, type: :mailer do
  describe "send_to_user" do
    let(:user) { FactoryGirl.create(:user) }
    let(:report_params) {{ title: '', country: '',
        state: '', city: '', tags: [], stages: [],
        position_ids: [], engagement_origin_ids: []
      }
    }
    let(:report) { ProcessTracking::Report.new(report_params) }
    let(:mail) { Courses::ReportMailer.send_to_user(user, report) }

    before :each do
      FactoryGirl.create(:vacancy_closed)
      FactoryGirl.create(:vacancy)
    end

    it "renders the headers" do
      expect(mail.subject).to eq("Informe de seguimiento de procesos")
      expect(mail.to).to eq([user.email])
    end

    it "has attached the report" do
      expect(mail.attachments).not_to be_empty
      expect(mail.attachments.count).to eq 1
      expect(mail.attachments.first.filename).to eq "seguimiento_procesos.xlsx"
      expect(mail.attachments.first.content_type).to eq "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    end
  end
end

