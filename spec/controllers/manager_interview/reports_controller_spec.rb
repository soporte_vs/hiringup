require 'rails_helper'

RSpec.describe ManagerInterview::ReportsController, :type => :controller do
  render_views
  let(:work_flow) {
    work_flow = [
      {type: :module, class_name: 'ManagerInterview::Stage', name: 'ManagerInterview'},
      {type: :module, class_name: 'Onboarding::Stage', name: 'Onboarding'},
      {type: :module, class_name: 'Engagement::Stage', name: 'Contratación'}
    ]
  }

  describe 'POST create ManagerInterview Report' do
    before :each do
      @manager_interview_report_attribtues = FactoryGirl.attributes_for(:manager_interview_report)
      @manager_interview_report_attribtues[:documents] = [
        ActionDispatch::Http::UploadedFile.new(:tempfile => File.new("#{Rails.root}/spec/tmp/document.doc"), filename: "document.doc"),
        ActionDispatch::Http::UploadedFile.new(:tempfile => File.new("#{Rails.root}/spec/tmp/document.doc"), filename: "document.doc")
      ]
      @manager_interview_stage = ManagerInterview::Stage.find(@manager_interview_report_attribtues[:manager_interview_stage_id])
      @course = @manager_interview_stage.enrollment.course
      @course.update(work_flow: work_flow)
    end

    context 'when current_user is not loged in' do
      it 'should redirect_to new_user_session_path' do
        expect{
          post :create,
               id: @course.to_param,
               stage_id: @manager_interview_stage.to_param,
               manager_interview_report: @manager_interview_report_attribtues
        }.to change(ManagerInterview::Report, :count).by(0)
        should redirect_to new_user_session_path
      end
    end

    context 'when current_user is loged in' do
      login_executive_without_role
      context 'and he not has role valid' do
        it 'should response with page 403' do
          expect{
            post :create,
                 id: @course.to_param,
                 stage_id: @manager_interview_stage.to_param,
                 manager_interview_report: @manager_interview_report_attribtues
          }.to change(ManagerInterview::Report, :count).by(0)
          expect(response.status).to eq 403
        end
      end

      context 'and he has a valid role' do
        context 'with valid attributes' do
          it 'with admin role' do
            @user.add_role :admin
          end

          it 'with admin role on @course' do
            @user.add_role :admin, @course
          end

          it 'with admin role on ManagerInterview::Stage' do
            @user.add_role :admin, ManagerInterview::Stage
          end

          it "with #{ManagerInterview::Stage.to_s.tableize.to_sym} on @course" do
            @user.add_role ManagerInterview::Stage.to_s.tableize.to_sym, @course
          end

          it "with :admin on @manager_interview_stage" do
            @user.add_role :admin, @manager_interview_stage
          end

          after :each do
            expect{
              post :create,
                   id: @course.to_param,
                   stage_id: @manager_interview_stage.to_param,
                   manager_interview_report: @manager_interview_report_attribtues
            }.to change(ManagerInterview::Report, :count).by(1)
            should redirect_to course_manager_interview_stage_path(id: @course, stage_id: @manager_interview_stage.id)
            expect(flash[:success]).to eq I18n.t('manager_interview.report.created.success')
          end
        end

        context 'with invalid attributes' do
          before :each do
            @manager_interview_report_attribtues = FactoryGirl.attributes_for(:manager_interview_report_invalid)
            @manager_interview_stage = ManagerInterview::Stage.find(@manager_interview_report_attribtues[:manager_interview_stage_id])
            @course = @manager_interview_stage.enrollment.course
            @course.update(work_flow: work_flow)
          end

          it 'with admin role' do
            @user.add_role :admin
          end

          it 'with admin role on @course' do
            @user.add_role :admin, @course
          end

          it 'with admin role on ManagerInterview::Stage' do
            @user.add_role :admin, ManagerInterview::Stage
          end

          it "with #{ManagerInterview::Stage.to_s.tableize.to_sym} on @course" do
            @user.add_role ManagerInterview::Stage.to_s.tableize.to_sym, @course
          end

          it "with :admin on @manager_interview_stage" do
            @user.add_role :admin, @manager_interview_stage
          end

          after :each do
            expect{
              post :create,
                   id: @course.to_param,
                   stage_id: @manager_interview_stage.to_param,
                   manager_interview_report: @manager_interview_report_attribtues
            }.to change(ManagerInterview::Report, :count).by(0)
            should render_template('manager_interview/stages/show')
            should render_template('manager_interview/reports/_form')

          end
        end

      end
    end
  end

  it_should_behave_like 'a stage with reports', :manager_interview_report, :manager_interview_stage
end
