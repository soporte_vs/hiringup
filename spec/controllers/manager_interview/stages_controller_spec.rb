require 'rails_helper'

RSpec.describe ManagerInterview::StagesController, :type => :controller do
  render_views

  describe 'GET show stage' do
    before :each do
      @manager_interview_stage = FactoryGirl.create(:manager_interview_stage)
      @course = @manager_interview_stage.enrollment.course
      work_flow = [
        {type: :module, class_name: 'Scheduling::Stage', name: 'Agendamiento'},
        {type: :module, class_name: 'ManagerInterview::Stage', name: 'ManagerInterview'},
        {type: :module, class_name: 'Onboarding::Stage', name: 'Onboarding'},
        {type: :module, class_name: 'Engagement::Stage', name: 'Contratación'}
      ]
      @course.update(work_flow: work_flow)
    end

    context 'when current_user is not loged in' do
      it 'should redirect new_user_session_path' do
        get :show, id: @course.to_param, stage_id: @manager_interview_stage.to_param
        should redirect_to new_user_session_path
      end
    end

    context 'when current_user is loged in' do
      login_executive_without_role

      context 'and has role valid' do
        it 'should response ok with role :admin' do
          @user.add_role :admin
        end

        it 'should response ok with role admin on ManagerInterview::Stage' do
          @user.add_role :admin, ManagerInterview::Stage
        end

        it "should response ok with role :admin on @course" do
          @user.add_role :admin, @course
        end

        it "should response ok with role #{ManagerInterview::Stage.to_s.tableize.to_sym} on @course" do
          @user.add_role ManagerInterview::Stage.to_s.tableize.to_sym, @course
        end

        after :each do
          get :show, id: @course.to_param, stage_id: @manager_interview_stage.to_param
          expect(assigns(:stage)).to eq @manager_interview_stage
          should render_template('manager_interview/stages/show')
        end
      end

      context 'and does not has role valid' do
        it 'shoul response with page 403 when current_user has not role' do
        end

        after :each do
          get :show, id: @course.to_param, stage_id: @manager_interview_stage.to_param
          expect(response.status).to eq 403
        end
      end
    end
  end
end
