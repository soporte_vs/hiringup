# coding: utf-8
require 'rails_helper'

RSpec.describe Offer::StagesController, :type => :controller do
  render_views

  describe 'POST send offer letter' do
    login_executive_without_role
    before :each do
      enrollment = FactoryGirl.create(:enrollment_basis_with_applicant)
      work_flow = [
        {type: :module, class_name: 'Stage::Base', name: 'Etapa'},
        {type: :module, class_name: 'Offer::Stage', name: 'Offer Letter'},
        {type: :module, class_name: 'Stage::Base', name: 'Etapa'}
      ]

      company_positions_cenco = FactoryGirl.create(:company_positions_cenco, company_position: enrollment.course.company_position)
      @vacancies = FactoryGirl.create_list(:vacancy, 3, company_positions_cenco: company_positions_cenco, course: enrollment.course)
      contract_types = FactoryGirl.create_list(:company_contract_type, 3)

      enrollment.course.update(work_flow: work_flow)
      enrollment.next_stage
      enrollment.next_stage
      enrollment.course.save
      enrollment.reload
      @offer_letter_stage = enrollment.stage
      @offer_letter_stage.enrollment.reload
    end

    it 'should send an email and persist the offer letter data' do
      @user.add_role :admin
      request.env["HTTP_REFERER"] = course_offer_stage_url(id: @offer_letter_stage.enrollment.course.id, stage_id: @offer_letter_stage.id)

      course = @offer_letter_stage.enrollment.course
      offer_letter = FactoryGirl.attributes_for(:offer_letter)
      expect{
        post(:create_letter, {
          id: @offer_letter_stage.enrollment.course.id,
          stage_id: @offer_letter_stage.id,
          offer_letter: offer_letter
        })
      }.to change(Offer::Letter, :count).by(1).and change(Delayed::Job, :count).by(1).and \
           change(PublicActivity::Activity, :count).by(1)

      email = YAML.load(Delayed::Job.last.handler)
      expect(email.object).to be Offer::StageMailer
      expect(email.method_name).to eq :send_offer_letter
      expect(email.args.include?(Offer::Letter.last)).to be true
    end
  end

  describe 'GET show stage' do
    before :each do
      @offer_stage = FactoryGirl.create(:offer_stage)
      @course = @offer_stage.enrollment.course
      work_flow = [
        {type: :module, class_name: 'Offer::Stage', name: 'Offer'},
        {type: :module, class_name: 'Onboarding::Stage', name: 'Onboarding'},
        {type: :module, class_name: 'Engagement::Stage', name: 'Contratación'}
      ]
      @course.update(work_flow: work_flow)
    end

    context 'when current_user is not loged in' do
      it 'should redirect new_user_session_path' do
        get :show, id: @course.to_param, stage_id: @offer_stage.to_param
        should redirect_to new_user_session_path
      end
    end

    context 'when current_user is loged in' do
      login_executive_without_role

      context 'and has role valid' do
        it 'should response ok with role :admin' do
          @user.add_role :admin
        end

        it 'should response ok with role admin on Offer::Stage' do
          @user.add_role :admin, Offer::Stage
        end

        it "should response ok with role :admin on @course" do
          @user.add_role :admin, @course
        end

        it "should response ok with role #{Offer::Stage.to_s.tableize.to_sym} on @course" do
          @user.add_role Offer::Stage.to_s.tableize.to_sym, @course
        end

        after :each do
          get :show, id: @course.to_param, stage_id: @offer_stage.to_param
          expect(assigns(:stage)).to eq @offer_stage
          should render_template('offer/stages/show')
        end
      end

      context 'and does not has role valid' do
        it 'shoul response with page 403 when current_user has not role' do
        end

        after :each do
          get :show, id: @course.to_param, stage_id: @offer_stage.to_param
          expect(response.status).to eq 403
        end
      end
    end
  end

end
