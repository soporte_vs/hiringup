require 'rails_helper'

RSpec.xdescribe Company::ReferencesController, type: :controller do

  let(:valid_attributes) {
    FactoryGirl.attributes_for(:company_reference)
  }

  let(:invalid_attributes) {
    FactoryGirl.attributes_for(:company_reference_invalid)
  }

  describe "GET #new" do
    it "assigns a new company_reference as @company_reference" do
      get :new
      expect(assigns(:company_reference)).to be_a_new(Company::Reference)
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Company::Reference" do
        expect {
          post :create, {:company_reference => valid_attributes}
        }.to change(Company::Reference, :count).by(1)
       end

      it "redirects to the created company_reference" do
        request.env["HTTP_REFERER"] = new_company_reference_path
        post :create, {:company_reference => valid_attributes}
        expect(response).to redirect_to(new_company_reference_path)
      end
    end

    context "with invalid params" do
      it "assigns a newly created but unsaved company_reference as @company_reference" do
        post :create, {:company_reference => invalid_attributes}
        expect(assigns(:company_reference)).to be_a_new(Company::Reference)
      end

      it "re-renders the 'new' template" do
        post :create, {:company_reference => invalid_attributes}
        expect(response).to render_template("new")
      end
    end
  end

  describe "GET #confirm" do
    let(:company_reference) { FactoryGirl.create(:company_reference) }

    context 'token is correct'do
      context 'is already confirmed' do
        it 'redirects to root_path' do
          company_reference.update(confirmed: true)

          expect(
            get :confirm, id: company_reference.id, token: company_reference.generate_confirm_token
            ).to redirect_to(root_path)
        end
      end

      context 'is not confirmed' do
        it "changes confirmed to true" do
          get :confirm, id: company_reference.id, token: company_reference.generate_confirm_token
          company_reference.reload
          expect(company_reference.confirmed).to be true
        end
      end
    end

    context 'token is incorrect'do
      it 'responds with 404' do
        get :confirm, id: company_reference.id, token: FFaker::Lorem.sentence
        expect(response.status).to eq(404)
      end
    end
  end
end
