require 'rails_helper'

RSpec.describe Company::EmployeesController, type: :controller do

  describe 'Upload employees' do
    login_executive_with_role :admin

    it 'should create job with taks' do
      email = FFaker::Internet.email
      file_param = ActionDispatch::Http::UploadedFile.new(
        tempfile: File.new("#{Rails.root}/spec/tmp/employees.xlsx"),
        filename: "employees.xlsx"
      )
      expect{
        post :upload_load_massive, notify_to: email, file: file_param
      }.to change(Delayed::Job, :count).by(1)

      report_mail = YAML.load(Delayed::Job.last.handler)
      expect(report_mail.object).to eq Company::Employee
      expect(report_mail.method_name).to eq :load_massive_from_file
      expect(report_mail.args.include?(email)).to be true
    end
  end
end
