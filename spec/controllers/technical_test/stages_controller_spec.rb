require 'rails_helper'

RSpec.describe TechnicalTest::StagesController, :type => :controller do
  render_views

  describe 'GET show stage' do
    before :each do
      @technical_test_stage = FactoryGirl.create(:technical_test_stage)
      @course = @technical_test_stage.enrollment.course
      work_flow = [
        {type: :module, class_name: 'Scheduling::Stage', name: 'Agendamiento'},
        {type: :module, class_name: 'TechnicalTest::Stage', name: 'TechnicalTest'},
        {type: :module, class_name: 'Onboarding::Stage', name: 'Onboarding'},
        {type: :module, class_name: 'Engagement::Stage', name: 'Contratación'}
      ]
      @course.update(work_flow: work_flow)
    end

    context 'when current_user is not loged in' do
      it 'should redirect new_user_session_path' do
        get :show, id: @course.to_param, stage_id: @technical_test_stage.to_param
        should redirect_to new_user_session_path
      end
    end

    context 'when current_user is loged in' do
      login_executive_without_role

      context 'and has role valid' do
        it 'should response ok with role :admin' do
          @user.add_role :admin
        end

        it 'should response ok with role admin on TechnicalTest::Stage' do
          @user.add_role :admin, TechnicalTest::Stage
        end

        it "should response ok with role :admin on @course" do
          @user.add_role :admin, @course
        end

        it "should response ok with role #{TechnicalTest::Stage.to_s.tableize.to_sym} on @course" do
          @user.add_role TechnicalTest::Stage.to_s.tableize.to_sym, @course
        end

        after :each do
          get :show, id: @course.to_param, stage_id: @technical_test_stage.to_param
          expect(assigns(:stage)).to eq @technical_test_stage
          should render_template('technical_test/stages/show')
        end
      end

      context 'and does not has role valid' do
        it 'shoul response with page 403 when current_user has not role' do
        end

        after :each do
          get :show, id: @course.to_param, stage_id: @technical_test_stage.to_param
          expect(response.status).to eq 403
        end
      end
    end
  end

end
