require 'rails_helper'

RSpec.describe TechnicalTest::ReportsController, :type => :controller do
  render_views
  let(:work_flow) {
    work_flow = [
      {type: :module, class_name: 'TechnicalTest::Stage', name: 'TechnicalTest'},
      {type: :module, class_name: 'Onboarding::Stage', name: 'Onboarding'},
      {type: :module, class_name: 'Engagement::Stage', name: 'Contratación'}
    ]
  }

  describe 'POST create TechnicalTest Report' do
    before :each do
      @technical_test_report_attribtues = FactoryGirl.attributes_for(:technical_test_report)
      @technical_test_report_attribtues[:documents] = [
         ActionDispatch::Http::UploadedFile.new(:tempfile => File.new("#{Rails.root}/spec/tmp/document.doc"), filename: "document.doc"),
         ActionDispatch::Http::UploadedFile.new(:tempfile => File.new("#{Rails.root}/spec/tmp/document.doc"), filename: "document.doc")
       ]
      @technical_test_stage = TechnicalTest::Stage.find(@technical_test_report_attribtues[:technical_test_stage_id])
      @course = @technical_test_stage.enrollment.course
      @course.update(work_flow: work_flow)
      Delayed::Job.destroy_all
    end

    context 'when current_user is not loged in' do
      it 'should redirect_to new_user_session_path' do
        expect{
          post :create,
               id: @course.to_param,
               stage_id: @technical_test_stage.to_param,
               technical_test_report: @technical_test_report_attribtues
        }.to change(TechnicalTest::Report, :count).by(0)
        should redirect_to new_user_session_path
      end
    end

    context 'when current_user is loged in' do
      login_executive_without_role
      context 'and he not has role valid' do
        it 'should response with page 403' do
          expect{
            post :create,
                 id: @course.to_param,
                 stage_id: @technical_test_stage.to_param,
                 technical_test_report: @technical_test_report_attribtues
          }.to change(TechnicalTest::Report, :count).by(0)
          expect(response.status).to eq 403
        end
      end

      context 'and he has a valid role' do
        context 'with valid attributes' do
          it 'with admin role' do
            @user.add_role :admin
          end

          it 'with admin role on @course' do
            @user.add_role :admin, @course
          end

          it 'with admin role on TechnicalTest::Stage' do
            @user.add_role :admin, TechnicalTest::Stage
          end

          it "with #{TechnicalTest::Stage.to_s.tableize.to_sym} on @course" do
            @user.add_role TechnicalTest::Stage.to_s.tableize.to_sym, @course
          end

          it "with :admin on @technical_test_stage" do
            @user.add_role :admin, @technical_test_stage
          end

          after :each do
            expect{
              post :create,
                   id: @course.to_param,
                   stage_id: @technical_test_stage.to_param,
                   technical_test_report: @technical_test_report_attribtues
            }.to change(TechnicalTest::Report, :count).by(1).and \
                 change(Delayed::Job, :count).by(1)

            should redirect_to course_technical_test_stage_path(id: @course, stage_id: @technical_test_stage.id)
            expect(flash[:success]).to eq I18n.t('technical_test.report.created.success')

            report_email = YAML.load(Delayed::Job.last.handler)
            expect(report_email.args.include? TechnicalTest::Report.last).to be true
            expect(report_email.object).to eq TechnicalTest::ReportMailer
            expect(report_email.method_name).to eq :send_report
          end
        end

        context 'with invalid attributes' do
          before :each do
            @technical_test_report_attribtues = FactoryGirl.attributes_for(:technical_test_report_invalid)
            @technical_test_stage = TechnicalTest::Stage.find(@technical_test_report_attribtues[:technical_test_stage_id])
            @course = @technical_test_stage.enrollment.course
            @course.update(work_flow: work_flow)
          end

          it 'with admin role' do
            @user.add_role :admin
          end

          it 'with admin role on @course' do
            @user.add_role :admin, @course
          end

          it 'with admin role on TechnicalTest::Stage' do
            @user.add_role :admin, TechnicalTest::Stage
          end

          it "with #{TechnicalTest::Stage.to_s.tableize.to_sym} on @course" do
            @user.add_role TechnicalTest::Stage.to_s.tableize.to_sym, @course
          end

          it "with :admin on @technical_test_stage" do
            @user.add_role :admin, @technical_test_stage
          end

          after :each do
            expect{
              post :create,
                   id: @course.to_param,
                   stage_id: @technical_test_stage.to_param,
                   technical_test_report: @technical_test_report_attribtues
            }.to change(TechnicalTest::Report, :count).by(0).and \
                 change(Delayed::Job, :count).by(0)

            should render_template('technical_test/stages/show')
          end
        end

      end
    end
  end

  it_should_behave_like 'a stage with reports', :technical_test_report, :technical_test_stage
end
