# coding: utf-8
require 'rails_helper'

RSpec.describe Promotion::StagesController, :type => :controller do
  render_views

  describe 'POST send_acceptance_email' do
    login_executive_without_role
    before :each do
      enrollment = FactoryGirl.create(:enrollment_basis_with_applicant)
      work_flow = [
        {type: :module, class_name: 'Stage::Base', name: 'Etapa'},
        {type: :module, class_name: 'Promotion::Stage', name: 'Onboarding'},
        {type: :module, class_name: 'Stage::Base', name: 'Etapa'}
      ]
      enrollment.course.update(work_flow: work_flow)
      enrollment.next_stage
      enrollment.next_stage
      enrollment.course.save
      enrollment.reload
      @promotion_stage = enrollment.stage
      @promotion_stage.enrollment.reload
    end

    context "user has permission" do
      it 'should send acceptance email and take enrollment to next stage' do
        @user.add_role :admin, Promotion::Stage

        #next_stage_index = @promotion_stage.enrollment.current_stage_index + 1

        expect{
          post :send_acceptance_email, id: @promotion_stage.enrollment.course.id, stage_id: @promotion_stage.id
        }.to change(Delayed::Job, :count).by(1)

        promotion_email = YAML.load(Delayed::Job.last.handler)
        expect(promotion_email.args.include? Promotion::Stage.last).to be true
        expect(promotion_email.object).to eq Promotion::StageMailer
        expect(promotion_email.method_name).to eq :send_acceptance_email

        ## es posible que se requiera que al enviar el email se avance a la siguiente etapa automáticamente
        # @promotion_stage.enrollment.reload
        # expect(@promotion_stage.enrollment.current_stage_index).to eq next_stage_index

        expect(flash[:success]).to eq I18n.t('promotion/stage.actions.send_acceptance_email.success')
        expect(response).to redirect_to course_promotion_stage_path(id: @promotion_stage.enrollment.course.id, stage_id: @promotion_stage.id)
      end
    end

    context "user has no permission" do
      it 'should response 403' do
        expect{
          post :send_acceptance_email, id: @promotion_stage.enrollment.course.id, stage_id: @promotion_stage.id
        }.to change(Delayed::Job, :count).by(0)
        expect(response.status).to eq 403
      end
    end
  end

  describe 'GET show stage' do
    before :each do
      @promotion_stage = FactoryGirl.create(:promotion_stage)
      @course = @promotion_stage.enrollment.course
      work_flow = [
        {type: :module, class_name: 'Scheduling::Stage', name: 'Agendamiento'},
        {type: :module, class_name: 'Promotion::Stage', name: 'Promotion'},
        {type: :module, class_name: 'Onboarding::Stage', name: 'Onboarding'},
        {type: :module, class_name: 'Engagement::Stage', name: 'Contratación'}
      ]
      @course.update(work_flow: work_flow)
    end

    context 'when current_user is not loged in' do
      it 'should redirect new_user_session_path' do
        get :show, id: @course.to_param, stage_id: @promotion_stage.to_param
        should redirect_to new_user_session_path
      end
    end

    context 'when current_user is loged in' do
      login_executive_without_role

      context 'and has role valid' do
        it 'should response ok with role :admin' do
          @user.add_role :admin
        end

        it 'should response ok with role admin on Promotion::Stage' do
          @user.add_role :admin, Promotion::Stage
        end

        it "should response ok with role :admin on @course" do
          @user.add_role :admin, @course
        end

        it "should response ok with role #{Promotion::Stage.to_s.tableize.to_sym} on @course" do
          @user.add_role Promotion::Stage.to_s.tableize.to_sym, @course
        end

        after :each do
          get :show, id: @course.to_param, stage_id: @promotion_stage.to_param
          expect(assigns(:stage)).to eq @promotion_stage
          should render_template('promotion/stages/show')
        end
      end

      context 'and does not has role valid' do
        it 'shoul response with page 403 when current_user has not role' do
        end

        after :each do
          get :show, id: @course.to_param, stage_id: @promotion_stage.to_param
          expect(response.status).to eq 403
        end
      end
    end
  end
end
