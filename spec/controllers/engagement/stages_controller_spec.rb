require 'rails_helper'

RSpec.describe Engagement::StagesController, :type => :controller do
  describe "GET show Engagement stage" do
    login_executive_without_role
    render_views

    before :each do
      @engagement_stage = FactoryGirl.create(:engagement_stage)
    end

    context 'when current_user has role admin on Engagement::Stage class' do
      it "should response ok" do
        @user.add_role :admin, Engagement::Stage
        get :show, id: @engagement_stage.enrollment.course.id, stage_id: @engagement_stage.id
        expect(response.status).to eq(200)
      end
    end

    context 'when current_user has role admin on course' do
      it "should response ok" do
        @user.add_role :admin, @engagement_stage.enrollment.course
        get :show, id: @engagement_stage.enrollment.course.id, stage_id: @engagement_stage.id
        expect(response.status).to eq(200)
      end
    end

    context "when current_user has role #{Engagement::Stage.to_s.tableize} on course" do
      it "should response ok" do
        @user.add_role Engagement::Stage.to_s.tableize.to_sym, @engagement_stage.enrollment.course
        get :show, id: @engagement_stage.enrollment.course.id, stage_id: @engagement_stage.id
        expect(response.status).to eq(200)
      end
    end

    context "when current_user has role admin on engagement_stage" do
      it "should response ok" do
        @user.add_role :admin, @engagement_stage
        get :show, id: @engagement_stage.enrollment.course.id, stage_id: @engagement_stage.id
        expect(response.status).to eq(200)
      end
    end
  end
end
