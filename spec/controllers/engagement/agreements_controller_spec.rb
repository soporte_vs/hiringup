require 'rails_helper'

RSpec.describe Engagement::AgreementsController, :type => :controller do
  context "Create a Agreements when current_user is not signed in" do
    render_views
    it "should redirect_to new_user_session_path" do
      engagement_agreement = FactoryGirl.attributes_for(:engagement_agreement)
      stage = engagement_agreement[:engagement_stage]
      post :create, id: stage.enrollment.course.id, stage_id: stage.id, engagement_agreement: engagement_agreement
      should redirect_to new_user_session_path
    end
  end

  context "Create a Agreements when current_user does not has role" do
    login_executive_without_role
    render_views
    it "should response with page 403" do
      engagement_agreement = FactoryGirl.attributes_for(:engagement_agreement)
      stage = engagement_agreement[:engagement_stage]
      post :create, id: stage.enrollment.course.id, stage_id: stage.id, engagement_agreement: engagement_agreement
      expect(response.status).to eq(403)
    end
  end

  context "Create a Agreements on stage" do
    render_views
    login_executive_without_role

    before :each do
      @engagement_agreement = FactoryGirl.attributes_for(:engagement_agreement)
      @stage = @engagement_agreement[:engagement_stage]
      @engagement_agreement[:documents] = [
        ActionDispatch::Http::UploadedFile.new(:tempfile => File.new("#{Rails.root}/spec/tmp/document.doc"), filename: "document.doc"),
        ActionDispatch::Http::UploadedFile.new(:tempfile => File.new("#{Rails.root}/spec/tmp/document.doc"), filename: "document.doc")
      ]
    end

    context 'should response ok' do
      it "when current_user has role admin on stage" do
        @user.add_role :admin, @stage
      end

      it "when current_user has role #{Engagement::Stage.to_s.tableize.to_sym} on course" do
        @user.add_role Engagement::Stage.to_s.tableize.to_sym, @stage.enrollment.course
      end

      it "when current_user has role admin on #{Engagement::Stage.to_s}" do
        @user.add_role :admin, Engagement::Stage
      end

      after :each do
        expect{
          post :create, id: @stage.enrollment.course.id, stage_id: @stage.id, engagement_agreement: @engagement_agreement
        }.to change(Engagement::Agreement,:count).by(1).and \
             change(Delayed::Job, :count).by(1).and \
             change(PublicActivity::Activity, :count).by(1).and \
             change(Company::Employee, :count).by(1).and \
             change(Company::EmployeePositionsCenco, :count).by(1)

        hire_email = YAML.load(Delayed::Job.last.handler)
        expect(hire_email.args.include? Engagement::Agreement.last).to be true
        expect(hire_email.object).to eq Engagement::AgreementMailer
        expect(hire_email.method_name).to eq :hire

        public_activity = PublicActivity::Activity.last
        expect(public_activity.trackable).to eq @stage.enrollment.course
        expect(public_activity.recipient).to eq @stage.enrollment
        expect(
          public_activity.key
        ).to eq 'course.engagement_stage.hired'
      end
    end

    context 'when enrollment is already discarted' do
      before :each do
        @stage.enrollment.discard(FFaker::Lorem.sentence, FactoryGirl.create(:enrollment_discard_reason))
      end

      it 'should not create an agrement' do
        @user.add_role :admin
      end

      after :each do
        expect{
          post :create, id: @stage.enrollment.course.id, stage_id: @stage.id, engagement_agreement: @engagement_agreement
        }.to change(Engagement::Agreement,:count).by(0).and \
             change(Delayed::Job, :count).by(0).and \
             change(PublicActivity::Activity, :count).by(0)
      end
    end
  end

  context "Get New Agreement on stage when current_user is not signed in" do
    render_views
    it "should redirect_to new_user_session_path" do
      engagement_stage = FactoryGirl.create(:engagement_stage)
      get :new, id: engagement_stage.enrollment.course.id, stage_id: engagement_stage.id
      should redirect_to new_user_session_path
    end
  end

  context "Get New Agreement on stage when current_user does not has role" do
    login_executive_without_role
    render_views
    it "should response page 403" do
      engagement_stage = FactoryGirl.create(:engagement_stage)
      get :new, id: engagement_stage.enrollment.course.id, stage_id: engagement_stage.id
      expect(response.status).to eq(403)
    end
  end

  context "Get New Agreement on stage" do
    login_executive_without_role
    render_views
    it "should response page ok" do
      engagement_stage = FactoryGirl.create(:engagement_stage)
      @user.add_role :admin, engagement_stage
      get :new, id: engagement_stage.enrollment.course.id, stage_id: engagement_stage.id
      expect(response.status).to eq(200)
    end
  end

  context "Show agrement on stage when current_user is not signed in" do
    render_views
    it "should redirect_to new_user_session_path" do
      engagement_agreement = FactoryGirl.create(:engagement_agreement)
      get :show, id: engagement_agreement.engagement_stage.enrollment.course.id, stage_id: engagement_agreement.engagement_stage.id, agreement_id: engagement_agreement.id
      should redirect_to new_user_session_path
    end
  end

  context "Show agrement on stage when current_user does has not role" do
    login_executive_without_role
    render_views
    it "should response with page 403" do
      engagement_agreement = FactoryGirl.create(:engagement_agreement)
      get :show, id: engagement_agreement.engagement_stage.enrollment.course.id, stage_id: engagement_agreement.engagement_stage.id, agreement_id: engagement_agreement.id
      expect(response.status).to eq(403)
    end
  end

  context "Show agrement on stage" do
    login_executive_without_role
    render_views
    it "should response ok" do
      engagement_agreement = FactoryGirl.create(:engagement_agreement)
      @user.add_role :admin, engagement_agreement.engagement_stage
      get :show, id: engagement_agreement.engagement_stage.enrollment.course.id, stage_id: engagement_agreement.engagement_stage.id, agreement_id: engagement_agreement.id, format: :jasperpdf
      expect(response.status).to eq(200)
    end
  end
end
