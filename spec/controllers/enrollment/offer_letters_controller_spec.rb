require "rails_helper"

RSpec.describe Enrollment::OfferLettersController, :type => :controller do
  describe "GET accept" do
    it "should mark accepted and show message" do
      offer_letter = FactoryGirl.create(:offer_letter)
      expect(offer_letter.accepted).to be nil
      get :accept,
          id: offer_letter.stage.enrollment.id,
          offer_letter_id: offer_letter.id,
          token: offer_letter.token
      offer_letter.reload
      expect(offer_letter.accepted).to be true
      expect(response.status).to eq 200
      expect(response).to render_template("enrollment/offer_letters/accept")
    end

    it "should response 404 if token not present" do
      offer_letter = FactoryGirl.create(:offer_letter)
      expect(offer_letter.accepted).to be nil
      get :accept,
          id: offer_letter.stage.enrollment.id,
          offer_letter_id: offer_letter.id
      offer_letter.reload
      expect(offer_letter.accepted).to be nil
      expect(response.status).to eq 404
    end

    it "should response 200 if already accepted" do
      offer_letter = FactoryGirl.create(:offer_letter_accepted)
      expect(offer_letter.accepted).to be true
      get :accept,
          id: offer_letter.stage.enrollment.id,
          offer_letter_id: offer_letter.id,
          token: offer_letter.token
      offer_letter.reload
      expect(offer_letter.accepted).to be true
      expect(response.status).to eq 200
    end

    it "should shoy already rejected if already rejected" do
      offer_letter = FactoryGirl.create(:offer_letter_rejected)
      expect(offer_letter.accepted).to be false
      get :accept,
          id: offer_letter.stage.enrollment.id,
          offer_letter_id: offer_letter.id,
          token: offer_letter.token
      offer_letter.reload
      expect(offer_letter.accepted).to be false
      expect(response.status).to eq 200
      expect(response).to render_template("enrollment/offer_letters/already_rejected")
    end
  end

  describe "GET show_reject" do
    it "should show show_reject form" do
      offer_letter = FactoryGirl.create(:offer_letter)
      get :show_reject,
          id: offer_letter.stage.enrollment.id,
          offer_letter_id: offer_letter.id,
          token: offer_letter.token
      expect(response.status).to eq 200
      expect(response).to render_template("enrollment/offer_letters/show_reject")
    end

    it "should response 404 if token not present" do
      offer_letter = FactoryGirl.create(:offer_letter)
      expect(offer_letter.accepted).to be nil
      get :show_reject,
          id: offer_letter.stage.enrollment.id,
          offer_letter_id: offer_letter.id
      offer_letter.reload
      expect(offer_letter.accepted).to be nil
      expect(response.status).to eq 404
    end

    it "should show already rejected if accepted=true" do
      offer_letter = FactoryGirl.create(:offer_letter_accepted)
      expect(offer_letter.accepted).to be true
      get :show_reject,
          id: offer_letter.stage.enrollment.id,
          offer_letter_id: offer_letter.id,
          token: offer_letter.token
      offer_letter.reload
      expect(offer_letter.accepted).to be true
      expect(response.status).to eq 200
      expect(response).to render_template("enrollment/offer_letters/already_accepted")
    end

    it "should show already rejected if accepted=false" do
      offer_letter = FactoryGirl.create(:offer_letter_rejected)
      expect(offer_letter.accepted).to be false
      get :show_reject,
          id: offer_letter.stage.enrollment.id,
          offer_letter_id: offer_letter.id,
          token: offer_letter.token
      offer_letter.reload
      expect(offer_letter.accepted).to be false
      expect(response.status).to eq 200
      expect(response).to render_template("enrollment/offer_letters/already_rejected")
    end
  end

  describe "POST reject" do
    it "should mark rejected and show message" do
      offer_letter = FactoryGirl.create(:offer_letter)
      offer_reject_reason = FactoryGirl.create(:offer_reject_reason)
      expect(offer_letter.accepted).to be nil
      post :reject,
          id: offer_letter.stage.enrollment.id,
          offer_letter_id: offer_letter.id,
          token: offer_letter.token,
          offer_letter: {offer_reject_reason_id: offer_reject_reason.id}
      offer_letter.reload
      expect(offer_letter.accepted).to be false
      expect(response.status).to eq 200
      expect(response).to render_template("enrollment/offer_letters/reject")
    end

    it "should response 403 if already accepted" do
      offer_letter = FactoryGirl.create(:offer_letter_accepted)
      offer_reject_reason = FactoryGirl.create(:offer_reject_reason)
      expect(offer_letter.accepted).to be true
      post :reject,
          id: offer_letter.stage.enrollment.id,
          offer_letter_id: offer_letter.id,
          token: offer_letter.token,
          offer_letter: {offer_reject_reason_id: offer_reject_reason.id}
      offer_letter.reload
      expect(offer_letter.accepted).to be true
      expect(response.status).to eq 200
      expect(response).to render_template("enrollment/offer_letters/already_accepted")
    end


    it "should response 403 if already rejected" do
      offer_letter = FactoryGirl.create(:offer_letter_rejected)
      offer_reject_reason = FactoryGirl.create(:offer_reject_reason)
      expect(offer_letter.accepted).to be false
      post :reject,
          id: offer_letter.stage.enrollment.id,
          offer_letter_id: offer_letter.id,
          token: offer_letter.token,
          offer_letter: {offer_reject_reason_id: offer_reject_reason.id}
      offer_letter.reload
      expect(offer_letter.accepted).to be false
      expect(response.status).to eq 200
      expect(response).to render_template("enrollment/offer_letters/already_rejected")
    end
  end
end
