require 'rails_helper'

RSpec.describe Enrollment::BasesController, :type => :controller do
  render_views

  describe 'Dispose enrollments' do
    context 'when current user is logged' do
      login_executive_without_role
      let(:course) { FactoryGirl.create(:course) }
      let(:enrollments) { FactoryGirl.create_list(:enrollment_basis_with_applicant, 3, course: course) }
      let(:discard_reason) { FactoryGirl.create(:enrollment_discard_reason) }
      let(:observations) { FFaker::Lorem.sentence }

      context 'should discard enrollments' do
        before :each do
          @enrollment_disposed_count_expected = enrollments.count
          @enrollments_with_errors_expected = []
        end

        it 'when user has permission admin' do
          @user.add_role :admin
        end

        it 'when current user has permission admin on some enrollments' do
          @user.add_role :admin, enrollments.first
          @enrollment_disposed_count_expected = 1
          @enrollments_with_errors_expected.last(2)
        end

        it 'when current user has permission admin on course' do
          @user.add_role :admin, enrollments.first.course
        end

        after :each do
          expect{
            post :dispose, {
              ids: enrollments.map(&:id),
              discard_reason_id: discard_reason.id,
              observations: observations,
              send_discarding_email: true,
              format: :json
            }
          }.to change(Enrollment::Discarding, :count).by(@enrollment_disposed_count_expected).and \
               change(Delayed::Job, :count).by(@enrollment_disposed_count_expected).and \
               change(PublicActivity::Activity, :count).by(@enrollment_disposed_count_expected)


          discard_emails = Delayed::Job.all.map{ |job| YAML.load(Delayed::Job.last.handler) }
          expect(discard_emails.map(&:object).uniq).to eq [Enrollment::DiscardingMailer]
          expect(discard_emails.map(&:method_name).uniq).to eq [:discard]

          enrollments_disposed = Enrollment::Discarding.all.map(&:enrollment)
          enrollments_disposed.each do |enrollment|
            expect(
              PublicActivity::Activity.find_by(
                trackable: enrollment.course,
                recipient: enrollment,
                key: 'course.discarded'
              )
            ).not_to be nil
          end

          @enrollments_with_errors_expected = assigns(:enrollments)
              .where(id: @enrollments_with_errors_expected.map(&:id))
          @enrollments_with_errors_expected.each do |enrollment|
            expect(enrollment.errors.any?).to be true
          end
        end
      end

      context 'should discard enrollments but not send discarding email' do
        before :each do
          @enrollment_disposed_count_expected = enrollments.count
          @enrollments_with_errors_expected = []
        end

        it 'when user has permission admin' do
          @user.add_role :admin
        end

        after :each do
          expect{
            post :dispose, {
              ids: enrollments.map(&:id),
              discard_reason_id: discard_reason.id,
              observations: observations,
              send_discarding_email: false,
              format: :json
            }
          }.to change(Enrollment::Discarding, :count).by(@enrollment_disposed_count_expected).and \
               change(Delayed::Job, :count).by(0).and \
               change(PublicActivity::Activity, :count).by(@enrollment_disposed_count_expected)

          enrollments_disposed = Enrollment::Discarding.all.map(&:enrollment)
          enrollments_disposed.each do |enrollment|
            expect(
              PublicActivity::Activity.find_by(
                trackable: enrollment.course,
                recipient: enrollment,
                key: 'course.discarded'
              )
            ).not_to be nil
          end

          @enrollments_with_errors_expected = assigns(:enrollments)
              .where(id: @enrollments_with_errors_expected.map(&:id))
          @enrollments_with_errors_expected.each do |enrollment|
            expect(enrollment.errors.any?).to be true
          end
        end
      end
    end
  end

  describe 'revert_dispose' do
    context 'user is logged in' do
      login_executive_without_role
      let(:course) { FactoryGirl.create(:course) }
      let(:enrollments) { FactoryGirl.create_list(:enrollment_basis_with_applicant, 3, course: course) }
      let(:discard_reason) { FactoryGirl.create(:enrollment_discard_reason) }

      context 'user has admin role' do
        context 'discarding exists' do
          before :each do
            @user.add_role :admin
            enrollments.first.discard(nil,discard_reason)
          end

          it 'destroys the discarding' do
            expect{
              post :revert_dispose, { ids: enrollments.map(&:id), format: :json }
            }.to change(Enrollment::Discarding, :count).by(-1).and \
                 change(Delayed::Job, :count).by(1)
          end
        end

        context 'discarding does not exist' do
          before :each do
            @user.add_role :admin
            enrollments.map(&:touch)
          end

          it 'returns no_discarding_present', :retry => 1 do
            expect{
              post :revert_dispose, { ids: enrollments.map(&:id), format: :json }
            }.to change(Enrollment::Discarding, :count).by(0).and \
                 change(Delayed::Job, :count).by(0)
          end

          after :each do
            @enrollments = assigns(:enrollments)
            @enrollments.map(&:reload)

            @enrollments.each do |enrollment|
              expect(enrollment.errors.any?).to be true
              expect(enrollment.errors[:discarding]).to include I18n.t("activerecord.errors.models.enrollment/base.attributes.discarding.no_discarding_present")
            end
          end
        end
      end

      context 'user does not have admin role' do
        it 'returns not_enough_permission' do
          enrollments.map(&:touch)
          expect{
              post :revert_dispose, { ids: enrollments.map(&:id), format: :json }
            }.to change(Enrollment::Discarding, :count).by(0).and \
                 change(Delayed::Job, :count).by(0)
            expect(response.status).to eq 403
        end
      end
    end
  end

  describe 'Next stage' do
    context 'when current user is logged in' do
      login_executive_without_role

      context 'and has role valid' do
        context 'and course has not a dynamic workflow' do
          let(:course) { FactoryGirl.create(:course) }
          let(:enrollments) { FactoryGirl.create_list(:enrollment_basis_with_applicant, 3, course: course) }

          before :each do
            @enrollments_expected = enrollments
          end

          it 'should pass to next stage all enrollments (role admin)' do
            @user.add_role :admin
          end

          it 'should pass to next stage all enrollments (role admin on course)' do
            @user.add_role :admin, course
          end

          it 'should pass to next stage to first enrollments (role admin on first enrollment)' do
            @user.add_role :admin, enrollments.first
            @enrollments_expected = [enrollments.first]
          end

          after :each do
            expect{
              post :next_stage, ids: enrollments.map(&:id), format: :json
            }.to change(Stage::Base, :count).by(@enrollments_expected.count)

            enrollments = assigns(:enrollments)
            @enrollments_expected.map(&:reload)

            @enrollments_expected.each do |enrollment|
              expect(enrollment.stage).not_to be nil
            end

            (enrollments - @enrollments_expected).each do |enrollment|
              expect(enrollment.stage).to be nil
              expect(enrollment.errors[:stage].any?).to be true
            end
          end
        end

        context 'and course has a dynamic workflow' do
          let(:course) { FactoryGirl.create(:course, type_enrollment: Enrollment::Dynamic.to_s) }
          let(:enrollments) { FactoryGirl.create_list(:enrollment_dynamic, 3, course: course) }
          let(:type_stage) { [Offer::Stage, Onboarding::Stage, Engagement::Stage][Random.rand(0..2)] }
          let(:result_expected) { nil }

          it "should pass enrollments to specific stage" do
            @user.add_role :admin
            @count = enrollments.count
          end

          context "but new stage type is already for all enrollments" do
            let(:result_expected) {
              assigns(:enrollments).each do |enrollment|
                expect(enrollment.errors[:stage]).to eq [I18n.t("activerecord.errors.models.enrollment/base.attributes.stage.stage_already_created", stage: Offer::Stage.model_name.human)]
              end
            }

            before :each do
              @type_stage = Offer::Stage
              enrollments.each do |enrollment|
                FactoryGirl.create(:offer_stage, enrollment: enrollment, name: "Oferta")
              end
            end

            it "should response errors in all enrollments" do
              @user.add_role :admin
              @count = 0
            end
          end

          context "but current_user has permission over a enrollment" do
            let(:result_expected) {
              expect(assigns(:enrollments).map(&:errors).count(&:present?)).to eq 2
            }
            it "should response error next_stage_unuthorized for two enrollments" do
              @user.add_role :admin, enrollments.first
              @count = 1
            end
          end

          after :each do
            new_stage = @type_stage || type_stage
            expect{
              post :next_stage,
                      ids: enrollments.map(&:id),
                      type_stage: new_stage.to_s,
                      name_stage: FFaker::Lorem.word,
                      format: :json
            }.to change(new_stage, :count).by(@count)
            result_expected
          end
        end
      end
    end
  end

  describe 'Invite enrollments' do
    context 'when current user is logged in' do
      login_executive_without_role
      let(:course) { FactoryGirl.create(:course) }
      let(:enrollments) { FactoryGirl.create_list(:enrollment_basis_with_applicant, 3, course: course) }

      context 'should invite enrollments' do
        before :each do
          @enrollment_invited_count_expected = enrollments.count
          @enrollments_with_errors_expected = []
        end

        it 'when user has permission admin' do
          @user.add_role :admin
        end

        it 'when current user has permission admin on some enrollments' do
          @user.add_role :admin, enrollments.first
          @enrollment_invited_count_expected = 1
        end

        it 'when current user has permission admin on course' do
          @user.add_role :admin, enrollments.first.course
        end

        after :each do
          expect{
            post :invite, {
              ids: enrollments.map(&:id),
              format: :json
            }
          }.to change(Enrollment::CourseInvitation, :count).by(@enrollment_invited_count_expected).and \
               change(Delayed::Job, :count).by(@enrollment_invited_count_expected).and \
               change(PublicActivity::Activity, :count).by(@enrollment_invited_count_expected)


          invite_emails = Delayed::Job.all.map{ |job| YAML.load(Delayed::Job.last.handler) }
          expect(invite_emails.map(&:object).uniq).to eq [Enrollment::CourseInvitationMailer]
          expect(invite_emails.map(&:method_name).uniq).to eq [:send_course_invitation]

          enrollments_invited = Enrollment::CourseInvitation.all.map(&:enrollment)
          enrollments_invited.each do |enrollment|
            expect(
              PublicActivity::Activity.find_by(
                trackable: enrollment.course,
                recipient: enrollment,
                key: 'course.invited'
              )
            ).not_to be nil
          end

          _enrollments = assigns(:enrollments)
          _enrollments.map(&:reload)
          _enrollments_with_errors_expected = _enrollments - enrollments_invited

          _enrollments_with_errors_expected.each do |enrollment|
            expect(enrollment.errors.any?).to be true
          end
        end
      end

      context "should not invite enrollment" do
        before :each do
          @enrollment_invited_count_expected = 0
          @enrollments_with_errors_expected = enrollments
        end

        it 'when user has permission but enrollment is not invitable' do
          @user.add_role :admin
          enrollments.map(&:next_stage) # enrollments with stages are not invitable

          expect{
            post :invite, {
              ids: enrollments.map(&:id),
              format: :json
            }
          }.to change(Enrollment::CourseInvitation, :count).by(@enrollment_invited_count_expected).and \
               change(Delayed::Job, :count).by(@enrollment_invited_count_expected).and \
               change(PublicActivity::Activity, :count).by(@enrollment_invited_count_expected)

          enrollments_invited = Enrollment::CourseInvitation.all.map(&:enrollment)
          expect(enrollments_invited).to be_empty

          _enrollments = assigns(:enrollments)
          _enrollments.map(&:reload)
          _enrollments_with_errors_expected = _enrollments - enrollments_invited

          _enrollments_with_errors_expected.each do |enrollment|
            expect(enrollment.errors.any?).to be true
            expect(enrollment.errors[:course_invitation]).to include I18n.t("activerecord.errors.models.enrollment/base.attributes.course_invitation.not_invitable")
          end
        end
      end
    end
  end

  describe "GET mark_invitation_accepted" do
    context "applicant is logged in" do
      login_applicant
      it "should set enrollment invitation to accepted" do
        enrollment = FactoryGirl.create(:enrollment_basis_with_applicant, applicant: @applicant)
        enrollment.invite
        expect {
          get :mark_invitation_accepted, id: enrollment.id
        }.to change(Enrollment::CourseInvitation, :count).by(0)
        enrollment.reload
        expect(enrollment.course_invitation.accepted).to be true
        expect(flash[:success]).not_to be_empty
        expect(response).to render_template('enrollment/course_invitations/accepted')
      end

      it "should render 403 if applicant is not invited" do
        enrollment = FactoryGirl.create(:enrollment_basis_with_applicant, applicant: @applicant)
        expect {
          get :mark_invitation_accepted, id: enrollment.id
        }.to change(Enrollment::CourseInvitation, :count).by(0)
        enrollment.reload
        expect(enrollment.course_invitation).to be nil
        expect(response.status).to be 403
        # expect(flash[:error]).to include I18n.t("activerecord.errors.models.enrollment/base.attributes.course_invitation.not_invited")
        expect(flash[:error]).not_to be_empty
      end
    end

    context "logged user is not course's applicant" do
      login_applicant
      it "should render 403 if user is not course applicant" do
        enrollment = FactoryGirl.create(:enrollment_basis_with_applicant)
        expect {
          get :mark_invitation_accepted, id: enrollment.id
        }.to change(Enrollment::CourseInvitation, :count).by(0)
        expect(flash[:error]).to be nil
        expect(response.status).to be 403
      end
    end
  end

  describe "GET mark_invitation_rejected" do
    context "applicant is logged in" do
      login_applicant
      it "should set enrollment invitation to rejected" do
        enrollment = FactoryGirl.create(:enrollment_basis_with_applicant, applicant: @applicant)
        enrollment.invite
        expect {
          get :mark_invitation_rejected, id: enrollment.id
        }.to change(Enrollment::CourseInvitation, :count).by(0)
        enrollment.reload
        expect(enrollment.course_invitation.accepted).to be false
        expect(flash[:success]).not_to be_empty
        expect(response).to render_template('enrollment/course_invitations/rejected')
      end

      it "should render 403 if applicant is not invited" do
        enrollment = FactoryGirl.create(:enrollment_basis_with_applicant, applicant: @applicant)
        expect {
          get :mark_invitation_rejected, id: enrollment.id
        }.to change(Enrollment::CourseInvitation, :count).by(0)
        enrollment.reload
        expect(enrollment.course_invitation).to be nil
        # expect(flash[:error]).to include I18n.t("activerecord.errors.models.enrollment/base.attributes.course_invitation.not_invited")
        expect(flash[:error]).not_to be_empty
        expect(response.status).to be 403
      end
    end

    context "logged user is not course's applicant" do
      login_applicant
      it "should redirect to profile if user is not course applicant" do
        enrollment = FactoryGirl.create(:enrollment_basis_with_applicant)
        expect {
          get :mark_invitation_rejected, id: enrollment.id
        }.to change(Enrollment::CourseInvitation, :count).by(0)
        expect(flash[:error]).to be nil
        expect(response.status).to be 403
      end
    end
  end

  describe "EXPORT enrollments" do
    let(:course) {FactoryGirl.create(:course)}
    let(:enrollments) { FactoryGirl.create_list(:enrollment_basis_with_applicant, 3, course: course) }

    context "User is not Logged in" do
      it "should response status 302" do
        get :export, ids: enrollments.map(&:id)
        expect(response.status).to eq 302
      end
    end
    context "user is logged in" do
      login_executive_without_role
      it 'when user has permission admin' do
        @user.add_role :admin
      end
      it 'when current user has permission admin on course' do
        @user.add_role :admin, enrollments.first.course
      end
      after :each do
        enrollments_ids = enrollments.map(&:id)
        get :export, ids: enrollments_ids
        expect(response.status).to eq 200
        expect(response.content_type.to_s).to eq Mime::XLSX.to_s
      end
    end
  end

  describe "unenroll" do
    let(:course) { FactoryGirl.create(:course) }
    let(:enrollments) { FactoryGirl.create_list(:enrollment_basis_with_applicant, 3, course: course) }
    let(:postulation) { FactoryGirl.create(:minisite_postulation) }
    let(:enrollment_postulation) { FactoryGirl.create(:enrollment_basis, applicant: postulation.applicant) }

    context "when user is not signed in" do
      it "should render 401" do
        enrollments_ids = enrollments.map(&:id)
        post :unenroll, ids: enrollments_ids, format: :json
        expect(response.status).to be 401
      end
    end

    context "when current_user is logged in" do
      login_executive_without_role

      context "when current_user is admin" do
        it "should unenroll all applicants from enrollment" do
          @user.add_role :admin

          enrollments_ids = enrollments.map(&:id)
          expect{
            post :unenroll, ids: enrollments_ids, format: :json
            }.to change(Enrollment::Base, :count).by(-3)

          enrollments.each do |enrollment|
            expect(enrollment.unenroll).to be true
          end
        end
      end

      context 'When enrollment has stages related' do
        before :each do
          @user.add_role :admin
          enrollments_ids = enrollments.map(&:id)
          post :unenroll, ids: enrollments_ids, format: :json
          course.enrollments.each do |enrollment|
            enrollment.next_stage(Stage::Base.to_s, FFaker::Lorem.word)
          end
        end

        it 'Should not unenroll applicant' do
          enrollments_ids = enrollments.map(&:id)
          expect{
            post :unenroll, ids: enrollments_ids, format: :json
            }.to change(Enrollment::Base, :count).by(0)
        end
      end

      context 'When enrollment has postulations related' do
        before :each do
          @user.add_role :admin
        end

        it 'Should not unenroll applicant' do
          enrollments_postulation_id = enrollment_postulation.id
          expect{
            post :unenroll, ids: enrollments_postulation_id, format: :json
            }.to change(Enrollment::Base, :count).by(0)
        end
      end
    end

  end
end
