require 'rails_helper'

RSpec.describe Report::ProcessTrackingController, type: :controller do
  describe 'Create Report' do

    context 'when current_user is not signed in' do
      context 'and report should be sent in separate email' do
        before :each do
          post :generate, format: :html
        end

        it 'should redirect_to new_user_session_path' do
        end
      end

      context 'and report should be sent in xlsx directly' do
        before :each do
          post :generate, format: :xlsx
        end

        it 'should redirect_to new_user_session_path' do
        end
      end

      after :each do
        should redirect_to new_user_session_path
      end
    end

    context 'when current_user is signed in' do
      login_executive_without_role

      before :each do
        FactoryGirl.create(:vacancy_closed)
        FactoryGirl.create(:vacancy)
      end

      context 'and report should be sent directly' do
        let(:expected_delay_job_count){0}

        before :each do
          add_role
          post :generate, format: :xlsx
        end

        context 'and has role' do
          let(:add_role){@user.add_role :admin}
          let(:expected_status){200}
          let(:expected_content){
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
          }

          it 'should render report xlsx' do
          end
        end

        context 'and does not has role' do
          let!(:add_role){}
          let(:expected_status){403}
          let(:expected_content){'text/html'}

          it 'should response page 403' do
          end
        end
      end

      context 'and report should be sent in separate email' do


        before :each do
          add_role
          post :generate, format: :html
        end

        context 'and has role' do
          let(:add_role){@user.add_role :admin}
          let(:expected_status){302}
          let(:expected_content){'text/html'}
          let(:expected_delay_job_count){1}

          it 'should render the page html and send email' do
          end
        end

        context 'and does not has role' do
          let(:add_role){}
          let(:expected_status){403}
          let(:expected_content){'text/html'}
          let(:expected_delay_job_count){0}

          it 'should response page 403 with no emails' do
          end
        end
      end

      after :each do
        expect(response.status).to eq expected_status
        expect(response.content_type).to eq expected_content
        expect(Delayed::Job.count).to eq expected_delay_job_count
      end
    end
  end
end
