require 'rails_helper'

RSpec.describe Report::VacanciesDistributionsController, type: :controller do
  describe 'Create Report' do
    context 'when current_user is not signed in' do
      it 'should redirect_to new_user_session_path' do
        get :report, format: :pdf
        should redirect_to new_user_session_path
      end
    end

    context 'when current_user is signed in' do
      login_executive_without_role
      context 'and has role' do
        it 'should render report pdf' do
          FactoryGirl.create(:vacancy_closed)
          FactoryGirl.create(:vacancy)
          @user.add_role :admin
          get :report, format: :pdf, starts_at: '01/01/1900', ends_at: I18n.l(Date.today)
        end
      end

      context 'and does not has role' do
        it 'should response page 403' do
          get :report, format: :pdf
          expect(response.status).to eq 403
          expect(response.content_type).to eq 'text/html'
        end
      end
    end
  end
end
