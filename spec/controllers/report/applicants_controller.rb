require 'rails_helper'

RSpec.describe Report::ApplicantsController, type: :controller do
  describe 'Create Report' do
    context 'when current_user is not signed in' do
      it 'should redirect_to new_user_session_path' do
        post :generate
        should redirect_to new_user_session_path
      end
    end

    context 'when current_user is signed in' do
      login_executive_without_role
      context 'and has role' do
        it 'should render report xlsx' do
          FactoryGirl.create_list(:enrollment_basis_with_applicant, 3)

          report_params = {
            :applicant=>{
              :first_name=>"",
              :last_name=>"",
              :sex=>"",
              :country=>"",
              :state=>"",
              :city=>"",
              :degree_name=>"",
              :degree_condition=>""
            },
            :course=>{
              :country=>"",
              :state=>"",
              :city=>"",
              :engagement_origin_ids=> Company::EngagementOrigin.pluck(:id).map(&:to_s)
            }
          }

          @user.add_role :admin

          expect {
            post :generate, report_params
          }.to change(Delayed::Job, :count).by(1)

          expect(response.status).to redirect_to new_report_applicant_path

          report_mail = YAML.load(Delayed::Job.last.handler)
          expect(report_mail.object).to eq Applicant::ReportMailer
          expect(report_mail.method_name).to eq :send_to_user
          expect(report_mail.args.first).to eq @user
          expect(report_mail.args.second).to be_kind_of Applicant::Report
          expect(report_mail.args.second.instance_variable_get("@params")).to eq report_params.deep_stringify_keys
        end
      end

      context 'and does not has role' do
        it 'should response page 403' do
          post :generate
          expect(response.status).to eq 403
          expect(response.content_type).to eq 'text/html'
        end
      end
    end
  end
end
