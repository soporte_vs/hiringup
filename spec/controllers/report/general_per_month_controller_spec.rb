require 'rails_helper'

RSpec.describe Report::GeneralPerMonthController, type: :controller do
  describe 'Create Report' do
    context 'when current_user is not signed in' do
      it 'should redirect_to new_user_session_path' do

        get :report, format: :xlsx
        should redirect_to new_user_session_path
      end
    end

    context 'Download Spreadsheet Report' do
      login_executive_with_role :admin
      it 'Should you download spreadsheet document' do
        FactoryGirl.create(:course)
        file = get :report, format: :xlsx
        xlsx = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
      end
    end

    context 'and does not has role' do
      login_executive_without_role
      it 'should response page 403' do
        get :report, format: :xlsx
        expect(response.status).to eq 403
        expect(response.content_type).to eq 'text/html'
      end
    end
  end
end
