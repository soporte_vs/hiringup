require 'rails_helper'

RSpec.describe CoursesController, :type => :controller do
  render_views

  describe 'Get candidates' do
    let(:course){ FactoryGirl.create(:course) }
    context 'when current user is not logged in' do
      it 'should redirect_to new_user_session_path' do
        get :candidates, id: course.to_param
        should redirect_to new_user_session_path
      end
    end

    context 'when current user is logged in' do
      login_executive_without_role

      context 'and has a role valid' do
        before :each do
          FactoryGirl.create(:enrollment_basis_with_applicant, course: course)
          course.reload
        end

        it 'should response with all candidates' do
          @user.add_role :show, course

          get :candidates, id: course.to_param, format: :json
          expect(assigns(:course)).to eq course
          expect(assigns(:enrollments)).to eq course.enrollments
        end
      end
    end
  end

  describe 'PUT switch state' do
    let(:course) { FactoryGirl.create(:course) }
    let(:observations) { FFaker::Lorem.paragraph}

    context 'when current user is not signed in' do
      it 'should redirect_to new_user_session_path' do
        put :switch_state, id: course.to_param, switch: :close
        should redirect_to new_user_session_path
      end
    end

    context 'when current user is signed in' do
      login_executive_without_role

      context 'and has not role valid' do
        it 'should response with status 403' do
          put :switch_state, id: course.to_param, switch: :close
          expect(response.status).to eq 403
        end
      end

      context 'and has role valid' do
        context 'and transition state is valid' do
          it 'with role admin' do
            @user.add_role :admin
          end

          it "with role admin on Course" do
            @user.add_role :admin, Course
          end

          after :each do
            expect {
              put :switch_state, id: course.to_param, switch: :close, course: {observations_closing: observations}
            }.to change(PublicActivity::Activity, :count).by(1)
            course.reload
            expect(course.closed?).to be true
            expect(course.observations_closing).to eq(observations)
            should redirect_to course
          end
        end

        context 'and transition state is invalid' do
          before :each do
            course.close!
            course.reload
          end

          it 'with role admin' do
            @user.add_role :admin
          end

          it "with role admin on Course" do
            @user.add_role :admin, Course
          end

          after :each do
            put :switch_state, id: course.to_param, switch: :open
            course.reload
            expect(course.opened?).to be false
            expect(course.closed?).to be true
          end
        end
      end
    end
  end

  describe 'Clone course' do
    context 'when current_user is not signed in' do
      it 'should redirect_to new_user_session_path' do
        course = FactoryGirl.create :course
        expect{
          post :clone, id: course.to_param
        }.to change(Course, :count).by(0)
        should redirect_to new_user_session_path
      end
    end

    context 'when current_user is signed in' do
      login_executive_without_role

      context 'and current_user does not has role' do
        it 'should response with page 403' do
          course = FactoryGirl.create :course
          expect{
            post :clone, id: course.to_param
          }.to change(Course, :count).by(0)
          expect(response.status).to eq 403
        end
      end

      context 'and current_user has role' do
        before :each do
          @course = FactoryGirl.create(:enrollment_basis_with_applicant).course
        end

        context 'with aasm_state on opened' do
          it 'should create a new course and redirect to edit' do
            @user.add_role :create, Course
          end
        end

        context 'with aasm_state on closed' do
          it 'should create a new course and redirect to edit' do
            @user.add_role :create, Course
            @course.close!
          end
        end

        context 'when course creator is not current_user' do
          it 'should create the course with current_user as user id' do
            @user.add_role :create, Course
            worker = FactoryGirl.create(:worker_basis)
            @course.user_id = worker.user.id
            @course.save
          end 
        end

        after :each do
          expect{
            post :clone, id: @course.to_param
          }.to change(Course, :count).by(1)
          course_cloned = Course.last
          expect(course_cloned.opened?).to be true
          expect(course_cloned.user_id).to eq @user.id
          expect(course_cloned.title).to eq @course.title
          expect(course_cloned.description).to eq @course.description
          expect(course_cloned.type_enrollment).to eq @course.type_enrollment
          expect(course_cloned.work_flow).to eq @course.work_flow
          expect(course_cloned.territory_city_id).to eq @course.territory_city.id
          expect(course_cloned.company_position_id).to eq @course.company_position_id

          # la idea es que el usuario sea el current user y no el user del proceso clonado
          expect(course_cloned.user).to eq @user

          expect(@user.can?(:admin, course_cloned)).to be true
          should redirect_to edit_course_path(course_cloned)
        end
      end
    end
  end

  describe 'PUT update Role on course' do
    context 'when current_user has role admin' do
      login_executive_without_role

      context 'with valid roles and valid users' do
        it 'should assigns all role to all workers' do
          course = FactoryGirl.create(:course)
          @user.add_role :admin, course
          workers = FactoryGirl.create_list(:worker_basis, 4)
          users = workers.map{ |w| w.user.id }
          params = course.roles_instance.map { |r| {role_name: r[:name], users: users} }
          put :update_security, id: course.id, course: {security: params}
          course.reload
          expect(course.roles.map{ |r| {role_name: r.name.to_sym, users: r.user_ids} }).to match_array params.uniq
          should redirect_to security_course_path(course)
        end
      end

      context 'with role invalid' do
         it 'should not assigns any role to any worker' do
          course = FactoryGirl.create(:course)
          @user.add_role :admin, course
          workers = FactoryGirl.create_list(:worker_basis, 4)
          users = workers.map{ |w| w.user.id }
          params = course.roles_instance.map { |r| {role_name: :sddknsa, users: users} }
          expect {
            put :update_security, id: course.id, course: {security: params}
          }.to change(course.roles, :count).by(0)
        end
      end

      context 'with valid roles and valid users and user is an enrollment' do
        login_applicant
        it 'should assigns all role to all workers' do
          course = FactoryGirl.create(:course)
          @applicant.user.add_role :admin, course
          course.applicants << @applicant
          roles_expected = course.roles.map{|r| {name: r.name, users: r.user_ids}}
          workers = FactoryGirl.create_list(:worker_basis, 4)
          users = workers.map{ |w| w.user.id }
          params = course.roles_instance.map { |r| {role_name: r[:name], users: users} }
          put :update_security, id: course.id, course: {security: params}
          course.reload
          expect(course.roles.map{|r| {name: r.name, users: r.user_ids}}).to eq roles_expected
          expect(response.status).to eq(403)
        end
      end
    end

    context 'when current_user is not signed in' do
      it 'should redirect_to new_user_session_path' do
        course = FactoryGirl.create(:course)
        put :update_security, id: course.id
        should redirect_to new_user_session_path
      end
    end

    context 'when current_user hasn\'t role admin on course' do
      login_executive_without_role
      it 'should redirect_to new_user_session_path' do
        course = FactoryGirl.create(:course)
        put :update_security, id: course.id
        expect(response.status).to eq(403)
      end
    end
  end

  context "Create Course with current_user not signed in" do
    it "should redirect_to new_user_session_path" do
      expect{
        post :create, course: FactoryGirl.attributes_for(:course)
      }.to change(Course, :count).by(0)
      should redirect_to new_user_session_path
    end
  end

  context "Create Course without role create Course" do
    login_executive_without_role
    it "should response status to 403" do
      expect{
        post :create, course: FactoryGirl.attributes_for(:course)
      }.to change(Course, :count).by(0)
      expect(response.status).to eq(403)
    end
  end

  context "Create Course" do
    login_executive_with_role(:create, Course)
    context "with valid params" do
      it "should create a new valid course" do
        expect{
          post :create, course: FactoryGirl.attributes_for(:course)
        }.to change(Course, :count).by(1).and change(PublicActivity::Activity, :count).by(1).and change(@user.activities, :count).by(1)
        expect(Course.last.used_tags.count).to eq(0)
        should redirect_to course_path(Course.last)
      end

      it "should create a new course with tags" do
        course_attributes = FactoryGirl.attributes_for(:course_with_tags)
        expect{
          post :create, course: course_attributes
        }.to change(Course, :count).by(1)
        expect(Course.last.tags).to match_array(Tag::CourseTag.where(id: course_attributes[:tags]))
        expect(Course.last.company_position.id).to eq course_attributes[:company_position_id]
        expect(Course.last.roles.first.users.include? @user).to be true
        should redirect_to course_path(Course.last)
      end

      it 'should create a new course with vacancies' do
        course_attributes = FactoryGirl.attributes_for(:course_with_tags)
        company_positions_cenco = FactoryGirl.create(:company_positions_cenco, company_position_id: course_attributes[:company_position_id])
        company_vacancy_request_reason = FactoryGirl.create(:company_vacancy_request_reason)
        course_attributes[:vacancies_attributes] = [
          { observations: FFaker::Lorem.sentence,
            cenco_id: company_positions_cenco.company_cenco_id,
            company_vacancy_request_reason_id: company_vacancy_request_reason.id },
          { observations: FFaker::Lorem.sentence,
            cenco_id: company_positions_cenco.company_cenco_id,
            company_vacancy_request_reason_id: company_vacancy_request_reason.id }
        ]
        expect{
          post :create, course: course_attributes
        }.to change(Course, :count).by(1)

        expect(Course.last.vacancies.map(&:observations)).to match_array course_attributes[:vacancies_attributes].map{ |v| v[:observations] }
        expect(Course.last.vacancies.map(&:company_positions_cenco_id).uniq).to match_array [company_positions_cenco.id]
        expect(Course.last.vacancies.map(&:company_vacancy_request_reason_id)).to match_array course_attributes[:vacancies_attributes].map{ |v| v[:company_vacancy_request_reason_id] }
      end
    end

    context "with invalid params" do
      it "should not create a new Course and render form" do
        course_attributes = FactoryGirl.attributes_for(:course_with_invalid_params)
        expect{
          request = post :create, course: course_attributes
        }.to change(Course, :count).by(0)
        expect(request).to render_template("courses/new")
      end
    end
  end

  context "Get page edit course" do
    login_executive_without_role
    let(:course){FactoryGirl.create(:course, type_enrollment: 'Enrollment::Dynamic')}

    context 'when user is an executive' do
      
      before :each do
        @user.add_role(:update, course)
        get :edit, id: course.to_param
      end

      context 'when course has a legacy enrollment type' do
        let!(:course){FactoryGirl.create(:course)}
        
        it "should respond with course form" do
        end  
      end
      
      context 'when course has a normal selectable type' do
        it "should respond with course form" do
        end  
      end
      
      after :each do
        expect(assigns(:course)).to eq(course)
        expect(response.status).to eq(200)
      end       
    end 


    context 'when current_user has role and he\'s an course\'s applicant' do
      login_applicant

      before :each do
        @applicant.user.add_role :admin, course
        course.applicants << @applicant
        get :edit, id: course.to_param
      end

      it 'should response with page 403' do
        expect(response.status).to eq 403
      end
    end
  end

  describe "Update course" do
    login_executive_without_role
    let(:new_attributes){FactoryGirl.attributes_for(:course_with_tags, title: FFaker::Lorem.sentence)}
    let(:course){FactoryGirl.create(:course_with_tags, :with_vacancies)}

    context 'when course is open' do
      let(:existing_vacancies){course.vacancies}
     
      before :each do
        new_attributes[:vacancies_attributes] = [
          { id: existing_vacancies.first.id,
            observations: FFaker::Lorem.sentence,
            company_positions_cenco_id: existing_vacancies.first.company_positions_cenco.id
          },
          { id: existing_vacancies.last.id,
            observations: FFaker::Lorem.sentence,
            company_positions_cenco_id: existing_vacancies.last.company_positions_cenco.id
          }
        ]

        @user.add_role :update, course
        put :update, id: course.to_param, course: new_attributes.except(:company_position_id)
        course.reload
      end


      it "should respond with course form and change attributes" do
        expect(assigns(:course)).to eq(course)
        expect(course.title).to eq(new_attributes[:title])
        expect(course.document_group_id).to eq(new_attributes[:document_group_id])
        expect(course.description).to eq(new_attributes[:description])
        expect(course.tags).to match_array(new_attributes[:tags])
        expect(course.vacancies.map(&:observations)).to match_array new_attributes[:vacancies_attributes].map{ |v| v[:observations] }
        expect(course.vacancies.map(&:company_positions_cenco_id)).to match_array new_attributes[:vacancies_attributes].map{ |v| v[:company_positions_cenco_id] }
      end
    end

    context 'when course is closed' do
      let!(:course){FactoryGirl.create(:course_with_tags)}

      before :each do
        @user.add_role :update, course
        course.close!
        put :update, id: course.to_param, course: new_attributes.except(:company_position_id)
        course.reload
      end

      it 'should not update course attributes' do
        expect(course.title).to_not eq new_attributes[:title]
      end
    end

    context 'when current_user has role and he\'s an course\'s applicant' do
      login_applicant

      before :each do
        @applicant.user.add_role :admin, course
        course.applicants << @applicant

        put :update, id: course.to_param, course: new_attributes
        course.reload
      end

      it 'should response with page 403' do
        expect(course.attributes).to_not eq new_attributes
        expect(response.status).to eq 403
      end
    end

    context "when only vacancies are updated" do
      context "and when only changing one attributes of a group of vacancies" do
        it "should not change vacancies count, only vacancy request reason" do
          new_attributes = FactoryGirl.attributes_for(:course_with_tags, title: FFaker::Lorem.sentence, type_enrollment: 'Enrollment::Dynamic')
          course = FactoryGirl.create(:course, territory_city: new_attributes[:territory_city])
          company_positions_cenco = FactoryGirl.create(:company_positions_cenco, company_position: course.company_position)
          FactoryGirl.create_list(:vacancy, 2, course: course, company_positions_cenco: company_positions_cenco)

          course.reload

          new_company_vacancy_request_reason = FactoryGirl.create(:company_vacancy_request_reason)
          new_attributes[:vacancies_attributes] = [
            {
              qty: 2,
              vacancy_ids: course.vacancy_ids,
              company_vacancy_request_reason_id: new_company_vacancy_request_reason.id
            }
          ]
          @user.add_role :update, course
          put :update, id: course.to_param, course: new_attributes.except(:company_position_id)
          course.reload
          expect(course.vacancies.count).to eq 2
          course.vacancies.each do |vacancy|
            expect(vacancy.company_vacancy_request_reason).to eq new_company_vacancy_request_reason
          end
        end

        context "and when changed cenco_id" do
          context "and when changed ONLY cenco_id" do
            it "should not change vacancies count, only company_positions_cenco" do
              course = FactoryGirl.create(:course, type_enrollment: 'Enrollment::Internal')
              company_positions_cenco = FactoryGirl.create(:company_positions_cenco, company_position: course.company_position)
              company_vacancy_request_reason = FactoryGirl.create(:company_vacancy_request_reason)
              FactoryGirl.create_list(
                :vacancy,
                3,
                course: course,
                company_positions_cenco: company_positions_cenco,
                company_vacancy_request_reason: company_vacancy_request_reason
              )

              course.reload

              new_company_positions_cenco = FactoryGirl.create(:company_positions_cenco, company_position: course.company_position)
              expect(company_positions_cenco.company_cenco_id).not_to eq new_company_positions_cenco.company_cenco_id

              new_attributes = {}
              new_attributes[:territory_city] = course.territory_city
              new_attributes[:vacancies_attributes] = [
                {
                  qty: 3,
                  vacancy_ids: course.vacancy_ids,
                  cenco_id: new_company_positions_cenco.company_cenco_id
                }
              ]
              @user.add_role :update, course
              put :update, id: course.to_param, course: new_attributes
              course.reload
              expect(course.vacancies.count).to eq 3
              course.vacancies.each do |vacancy|
                expect(vacancy.company_vacancy_request_reason).to eq company_vacancy_request_reason
                expect(vacancy.company_positions_cenco).to eq new_company_positions_cenco
                expect(vacancy.cenco_id).to eq new_company_positions_cenco.company_cenco_id
              end
            end
          end # only cenco_id

          context "and when changed cenco_id AND observations" do
            it "should not change vacancies count, only company_positions_cenco and observations" do
              course = FactoryGirl.create(:course, type_enrollment: 'Enrollment::Internal')
              company_positions_cenco = FactoryGirl.create(:company_positions_cenco, company_position: course.company_position)
              company_vacancy_request_reason = FactoryGirl.create(:company_vacancy_request_reason)
              FactoryGirl.create_list(
                :vacancy,
                3,
                course: course,
                company_positions_cenco: company_positions_cenco,
                company_vacancy_request_reason: company_vacancy_request_reason
              )

              course.reload

              new_observations = FFaker::Lorem.sentence
              new_company_positions_cenco = FactoryGirl.create(:company_positions_cenco, company_position: course.company_position)
              expect(company_positions_cenco.company_cenco_id).not_to eq new_company_positions_cenco.company_cenco_id

              new_attributes = {}
              new_attributes[:territory_city] = course.territory_city
              new_attributes[:vacancies_attributes] = [
                {
                  qty: 3,
                  vacancy_ids: course.vacancy_ids.join(","),
                  cenco_id: new_company_positions_cenco.company_cenco_id,
                  observations: new_observations
                }
              ]
              @user.add_role :update, course
              put :update, id: course.to_param, course: new_attributes
              course.reload
              expect(course.vacancies.count).to eq 3
              course.vacancies.each do |vacancy|
                expect(vacancy.company_vacancy_request_reason).to eq company_vacancy_request_reason
                expect(vacancy.observations).to eq new_observations
                expect(vacancy.company_positions_cenco).to eq new_company_positions_cenco
                expect(vacancy.cenco_id).to eq new_company_positions_cenco.company_cenco_id
              end
            end
          end # only  cenco_id AND observations

        end
      end
    end
  end

  context "Try Get GUI New Course without user signed in" do
    it "should redirect_to to new_user_session_path" do
      get :new
      should redirect_to new_user_session_path
    end
  end

  context "Try GUI New Course with user singed in" do
    login_executive_without_role
    it "should response status 403" do
      get :new
      expect(response.status).to eq(403)
    end
  end

  context "Try GUI New Course with use singed and with role" do
    login_executive_with_role(:new,Course)
    it "should response with status 200" do
      get :new
      expect(response.status).to eq(200)
    end

    it "should response with status 200 with vacancies" do
      company_positions_cenco = FactoryGirl.create(:company_positions_cenco)
      vacancies = FactoryGirl.create_list(:vacancy_without_course, 3, company_positions_cenco: company_positions_cenco)
      get :new, vacancy_ids: vacancies.map(&:id)
      expect(response.status).to eq(200)
      expect(assigns(:course)).to be_a_new(Course)
      expect(assigns(:course).vacancies).to match_array(vacancies)
      expect(assigns(:course).company_position_id).to eq(company_positions_cenco.company_position_id)
    end
  end

  context "Get a course that does not exists" do
    login_executive_with_role(:show, Course)
    it "should be response with page 404 not found" do
      get :show, id: 39849320
      expect(response.status).to eq(404)
    end
  end

  context "Get a course" do
    login_executive_without_role
    it "should be response ok" do
      course = FactoryGirl.create(:course)
      @user.add_role :show, course
      get :show, id: course.id
      expect(response.status).to eq(200)
    end
  end

  context "User not signed in try get in to dashboard course " do
    it "should redirect_to new_user_session_path " do
      course = FactoryGirl.create(:course)
      get :show, id: course.id
      should redirect_to new_user_session_path
    end
  end

  context "User signed in but without role try get in to dashboard course" do
    login_executive_without_role
    it "should response with page 403" do
      course = FactoryGirl.create(:course)
      get :show, id: course.id
      expect(response.status).to eq(403)
    end
  end

  context "User signed and has role try get in to dashboard course" do
    login_executive_without_role
    it "should response with status 200" do
      course = FactoryGirl.create(:course)
      @user.add_role(:show, course)
      get :show, id: course.id
      expect(response.status).to eq(200)
    end

    context 'when current_user has role and he\'s an course\'s applicant' do
      login_applicant
      it 'should response with page 403' do
        course = FactoryGirl.create(:course)
        @applicant.user.add_role :admin, course
        course.applicants << @applicant
        get :show, id: course.id
        expect(response.status).to eq 403
      end
    end
  end

end
