require 'rails_helper'

RSpec.describe Laborum::PublicationsController, type: :controller do
  render_views

  # This should return the minimal set of attributes required to create a valid
  # Minisite::Publication. As you add validations to Minisite::Publication::publication, be sure to
  # adjust the attributes here as well.
  let(:valid_attributes) {
    FactoryGirl.attributes_for :laborum_publication
  }

  let(:invalid_attributes) {
    FactoryGirl.attributes_for :laborum_publication, title: nil
  }
  render_views

  describe "GET #index" do
    before :each do
      @publication = FactoryGirl.create(:laborum_publication)
      @course = @publication.course
    end

    context 'when current user has role admin_publications' do
      login_executive_without_role
      it "assigns all laborum_publications as @laborum_publications" do
        publication = FactoryGirl.create :laborum_publication
        @user.add_role :admin_publications, publication.course
        get :index, id: publication.course.id
        expect(assigns(:laborum_publications)).to eq(publication.course.laborum_publications)
      end
    end

    context 'when current user is not signed in' do
      it 'should redirect_to new_user_session_path' do
        get :index, id: @course.id
        should redirect_to new_user_session_path
      end
    end

    context 'when current user does not has role admin_publications' do
      login_executive_without_role
      it 'should response with page 403' do
        get :index, id: @course.id
        expect(response.status).to eq 403
      end
    end
  end

  describe "GET #show" do
    context 'when current user has role admin_publications' do
      login_executive_without_role

      it "assigns the requested laborum_publication as @laborum_publication" do
        # pending 'los weas de laborum estan cambiando la API'
        # publication = FactoryGirl.create :laborum_publication
        # @user.add_role :admin_publications, publication.course
        # get :show, id: publication.course.id, publication_id: publication.id
        # expect(assigns(:laborum_publication)).to eq(publication)
      end
    end

    context 'when current user is not signed in' do
      it 'should redirect_to new_user_session_path' do
        publication = FactoryGirl.create :laborum_publication
        get :show, id: publication.course.id, publication_id: publication.id
        should redirect_to new_user_session_path
      end
    end

    context 'when current user does not has role admin_publications' do
      login_executive_without_role
      it 'should response with page 403' do
        publication = FactoryGirl.create :laborum_publication
        get :show, id: publication.course.id, publication_id: publication.id
        expect(response.status).to eq 403
      end
    end
  end

  describe "GET #new" do
    context 'when current user has role admin_publications' do
      login_executive_without_role

      it "assigns a new laborum_publication as @laborum_publication" do
        course = FactoryGirl.create :course
        @user.add_role :admin_publications, course
        get :new, id: course.id
        expect(assigns(:laborum_publication)).to be_a_new(Laborum::Publication)
        expect(assigns(:laborum_publication).course.id).to eq course.id
      end
    end

    context 'when current user is not signed in' do
      it 'should redirect_to new_user_session_path' do
        get :new, id: 1
        should redirect_to new_user_session_path
      end
    end

    context 'when current user does not has role admin_publications' do
      login_executive_without_role
      it 'should response with page 403' do
        course = FactoryGirl.create :course
        get :new, id: course.id
        expect(response.status).to eq 403
      end
    end
  end

  describe "GET #edit" do
    context 'when current user has role admin' do
      login_executive_without_role

      it "assigns the requested laborum_publication as @laborum_publication" do
        publication = FactoryGirl.create :laborum_publication
        @user.add_role :admin_publications, publication.course
        get :edit, id: publication.course.id, publication_id: publication.id
        expect(assigns(:laborum_publication)).to eq(publication)
      end
    end

    context 'when current user is not signed in' do
      it 'should redirect_to new_user_session_path' do
        publication = FactoryGirl.create :laborum_publication
        get :edit, {id: publication.course.id, publication_id: publication.id}
        should redirect_to new_user_session_path
      end
    end

    context 'when current user does not has role admin' do
      login_executive_without_role
      it 'should response with page 403' do
        publication = FactoryGirl.create :laborum_publication
        get :edit, id: publication.course.id, publication_id: publication.id
        expect(response.status).to eq 403
      end
    end
  end

  describe "POST #create" do
    context 'when current user has role admin_publications' do
      login_executive_without_role

      context "with valid params and current user has role admin_publications" do
        it "assigns a newly created laborum_publication as @laborum_publication" do
          course = FactoryGirl.create :course
          @user.add_role :admin_publications, course
          post :create, id: course.id, laborum_publication: valid_attributes
          expect(assigns(:laborum_publication)).to be_a(Laborum::Publication)
          expect(assigns(:laborum_publication)).to be_persisted
        end

        it "redirects to the created laborum_publication" do
          course = FactoryGirl.create :course
          @user.add_role :admin_publications, course
          post :create, id: course.id, laborum_publication: valid_attributes
          publication = Laborum::Publication.last
          expect(publication.delayed_job_id).not_to be_nil
          expect(response).to redirect_to(laborum_publication_path(:publication_id => publication.id))
        end
      end

      context "with invalid params" do
        it "assigns a newly created but unsaved laborum_publication as @laborum_publication" do
          course = FactoryGirl.create :course
          @user.add_role :admin_publications, course
          post :create, id: course.id, laborum_publication: invalid_attributes
          expect(assigns(:laborum_publication)).to be_a_new(Laborum::Publication)
        end

        it "re-renders the 'new' template" do
          course = FactoryGirl.create :course
          @user.add_role :admin_publications, course
          post :create, id: course.id, laborum_publication: invalid_attributes
          expect(response).to render_template("new")
        end
      end
    end

     context 'when current user is not signed in' do
      it 'should redirect_to new_user_session_path' do
        course = FactoryGirl.create :course
        post :create, id: course.id, laborum_publication: valid_attributes
        should redirect_to new_user_session_path
      end
    end

    context 'when current user does not has role admin_publications' do
      login_executive_without_role
      it 'should response with page 403' do
        course = FactoryGirl.create :course
        post :create, id: course.id, laborum_publication: valid_attributes
        expect(response.status).to eq 403
      end
    end
  end

  describe "PUT #update" do
    let(:new_attributes) {
      FactoryGirl.attributes_for :laborum_publication
    }

    context 'when current user has role admin' do
      login_executive_with_role :admin_publications

      context "with valid params" do

        it "updates the requested laborum_publication" do
          publication = FactoryGirl.create(:laborum_publication, service_id: 12345678)
          @user.add_role :admin_publications, publication.course
          put :update, id: publication.course.id, publication_id: publication.id, laborum_publication: new_attributes
          publication.reload
          expect(publication.title).to eq new_attributes[:title]
          expect(publication.description).to eq new_attributes[:description]
          expect(publication.address).to eq new_attributes[:address]
        end

        it "assigns the requested laborum_publication as @laborum_publication" do
          publication = FactoryGirl.create(:laborum_publication)
          @user.add_role :admin_publications, publication.course
          put :update, id: publication.course, publication_id: publication.id, laborum_publication: valid_attributes
          expect(assigns(:laborum_publication)).to eq(publication)
        end

        it "redirects to the laborum_publication" do
          publication = FactoryGirl.create(:laborum_publication, service_id: 12345678)
          @user.add_role :admin_publications, publication.course
          put :update, id: publication.course.id, publication_id: publication.id, laborum_publication: valid_attributes
          should redirect_to laborum_publication_path(:publication_id => publication.id)
        end

        include_examples 'a publication controller update action', :laborum_publication
      end

      context "with invalid params" do
        it "assigns the laborum_publication as @laborum_publication" do
          publication = FactoryGirl.create(:laborum_publication)
          @user.add_role :admin_publications, publication.course
          put :update, id: publication.course, publication_id: publication.id, laborum_publication: invalid_attributes
          expect(assigns(:laborum_publication)).to eq(publication)
        end

        it "re-renders the 'edit' template" do
          publication = FactoryGirl.create(:laborum_publication)
          @user.add_role :admin_publications, publication.course
          put :update, id: publication.course.id, publication_id: publication.id, laborum_publication: invalid_attributes
          expect(response).to render_template("edit")
        end
      end
    end

    context 'when current user is not signed in' do
      it 'should redirect_to new_user_session_path' do
        publication = FactoryGirl.create(:laborum_publication)
        put :update, id: publication.course.id, publication_id: publication.id, laborum_publication: new_attributes
        should redirect_to new_user_session_path
      end
    end

    context 'when current user does not has role admin' do
      login_executive_without_role
      it 'should response with page 403' do
        publication = FactoryGirl.create(:laborum_publication)
        put :update, id: publication.course, publication_id: publication.id, laborum_publication: new_attributes
        expect(response.status).to eq 403
      end
    end
  end

  describe "Unpublish #destroy" do
    context 'when current user has role admin_publications' do
      login_executive_with_role :admin_publications

      it "destroys the requested laborum_publication" do
        publication = FactoryGirl.create :laborum_publication, service_id: 1234
        @user.add_role :admin_publications, publication.course
        delete :destroy, id: publication.course.id, publication_id: publication.id
        expect(Laborum::Publication.find(publication.id).status).to eq('unpublished')
      end

      it "redirects to the laborum_publications list" do
        publication = FactoryGirl.create :laborum_publication
        @user.add_role :admin_publications, publication.course
        delete :destroy, id: publication.course.id, publication_id: publication.id
        expect(response).to redirect_to(laborum_publications_path(publication.course))
      end
    end

    context 'when current user is not signed in' do
      it 'should redirect_to new_user_session_path' do
        publication = FactoryGirl.create :laborum_publication
        delete :destroy, id: publication.course.id, publication_id: publication.id
        should redirect_to new_user_session_path
      end
    end

    context 'when current user does not has role admin_publications' do
      login_executive_without_role
      it 'should response with page 403' do
        publication = FactoryGirl.create :laborum_publication
        delete :destroy, id: publication.course.id, publication_id: publication.id
        expect(response.status).to eq 403
      end
    end
  end

end
