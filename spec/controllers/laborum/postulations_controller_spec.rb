require 'rails_helper'

RSpec.describe Laborum::PostulationsController, type: :controller do
  render_views

  describe 'PUT accept postulation' do
    before(:each) do
      allow_any_instance_of(Laborum::Postulation).to receive(:set_applicant_and_info).and_return nil
    end

    context 'when current user is not signed in' do
      it 'should redirect_to new_user_session_path' do
        postulation = FactoryGirl.create(:laborum_postulation, is_accepted: nil, accepted_at: nil)
        put :accept, id: postulation.to_param
        should redirect_to new_user_session_path
      end
    end

    context 'when current user is signed in' do
      login_executive_without_role
      context 'and not has role admin_postulations' do
        it 'should response with page 403' do
          postulation = FactoryGirl.create(:laborum_postulation, is_accepted: nil, accepted_at: nil)
          put :accept, id: postulation.to_param
          expect(response.status).to eq 403
        end
      end

      context 'and has role admin_postulations' do
        it 'should set attribute is_accepted as true and send email' do
          postulation = FactoryGirl.create(:laborum_postulation, is_accepted: nil, accepted_at: nil)
          @user.add_role :admin_publications, postulation.laborum_publication.course

          Delayed::Job.destroy_all
          expect{
            put :accept, id: postulation.to_param
          }.to change(postulation.laborum_publication.course.enrollments, :count).by(1)
          postulation.reload
          expect(postulation.is_accepted).to be true
          expect(postulation.accepted_at).to_not be nil

          enrollment = postulation.laborum_publication.course.enrollments.first
          expect(enrollment.applicant.id).to eq postulation.applicant.id
        end

        it 'should set attribute is_accepted as false' do
          postulation = FactoryGirl.create(:laborum_postulation, is_accepted: nil, accepted_at: nil)
          @user.add_role :admin_publications, postulation.laborum_publication.course
          put :discard, id: postulation.to_param
          postulation.reload
          expect(postulation.is_accepted).to be false
        end
      end

      context 'and postulation was accepted already' do
        it 'should return postulation with error accept and redirect to publication' do
          postulation = FactoryGirl.create(:laborum_postulation, is_accepted: true)
          @user.add_role :admin_publications, postulation.laborum_publication.course
          put :accept, id: postulation.to_param
          expect(assigns(:postulation).course_postulation.errors.count).to_not eq 0
        end
      end
    end
  end
end
