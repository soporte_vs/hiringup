require 'rails_helper'

RSpec.describe Worker::BasesController, :type => :controller do
  render_views

  context 'Grand Roles' do
    before :each do
      @worker = FactoryGirl.create(:worker_basis)
      course = FactoryGirl.create(:course)
      @worker.user.add_role :admin, Course
      @worker.user.add_role :admin, course

      @worker_2 = FactoryGirl.create(:worker_basis)
      @worker_2.user.add_role :admin, Course
      @worker_2.user.add_role :admin, course

      @roles_expected = [
        {name: :admin, resource_type: Course.to_s, resource_id: course.id}.to_s,
        {name: :search, resource_type: Applicant::Base.to_s, resource_id: nil}.to_s
      ]
      @roles = [
        {name: :search, resource_type: Applicant::Base.to_s}.to_s,
        {name: :adm, resource_type: Worker::Base.to_s}.to_s,
        {name: :admin, resource_type: "kndas"}.to_s,
      ]
    end

    context 'when current_user is not signed in' do
      it 'should redirect_to new_user_session_path' do
        expect{
          post :add_role, id: @worker.id, roles: @roles
        }.to change(Role, :count).by(0)
        should redirect_to new_user_session_path
      end
    end

    context 'when current_user is signed in' do
      login_executive_without_role
      context 'and does not has role' do
        it 'should response with page 403' do
          expect{
            post :add_role, id: @worker.id, roles: @roles
          }.to change(Role, :count).by(0)
        end
      end

      context 'and does has role' do
        it 'admin should add role to worker' do
          @user.add_role :admin
        end

        after :each do
          post :add_role, id: @worker.id, roles: @roles

          @worker.reload
          @worker_2.reload
          worker_roles = @worker.user.roles.map do |role|
            {
              name: role.name.to_sym,
              resource_type: role.resource_type,
              resource_id: role.resource_id
            }.to_s
          end
          expect(@roles_expected).to match_array(worker_roles)
          expect(@worker_2.user.roles.count).to eq 2
        end
      end
    end
  end

  context "CREATE a Worker" do
    login_executive_with_role(:create, Worker::Base)
    it "should response with status 200 an create a worker and user" do
      attributes_worker = FactoryGirl.attributes_for(:worker_basis_invitable)
      expect(Delayed::Job.count).to eq(0)
      expect{
        post :create, worker: attributes_worker
      }.to change(Worker::Base, :count).by(1).and change(User, :count).by(1).and change(People::PersonalInformation, :count).by(1)
      user = User.last
      should redirect_to user.worker
      expect(Delayed::Job.count).to eq(1)
      expect(Delayed::Job.last.handler.include? user.email)
      expect{
        post :create, worker: attributes_worker
      }.to change(Worker::Base, :count).by(0).and change(User, :count).by(0)
    end

    it "should create an worker and associate to users's email" do
      applicant = FactoryGirl.create(:applicant_bases)
      password_user = applicant.user.encrypted_password
      expect(applicant.user.worker.present?).to be false
      attributes_worker = FactoryGirl.attributes_for(:worker_basis_invitable, user_attributes: { email: applicant.user.email})
      expect(Delayed::Job.count).to eq(0)
      expect{
        post :create, worker: attributes_worker
      }.to change(Worker::Base, :count).by(1).and change(User, :count).by(0)
      expect(Delayed::Job.count).to eq(0)
      applicant.reload
      expect(applicant.user.worker.present?).to be true
      expect(applicant.user.encrypted_password).to eq(password_user)
    end
  end

  context "EDIT a worker" do
    login_executive_without_role
    it "should response with form for edit worker" do
      worker = FactoryGirl.create(:worker_basis)
      @user.add_role :update, worker
      request = get :edit, id: worker.to_param
      expect(assigns(:worker)).to eq(worker)
      expect(request).to render_template("worker/bases/edit")
    end
  end

   context "UPDATE personal_information on worker" do
    login_executive_without_role

    it "should reditect_to show page and update worker" do
      worker = FactoryGirl.create(:worker_basis)
      new_attributes = FactoryGirl.attributes_for(:worker_basis)
      new_attributes[:user_attributes][:id] = worker.user.id
      new_attributes[:user_attributes][:personal_information_attributes][:id] = worker.user.personal_information.id
      @user.add_role :update, worker
      new_avatar = ActionDispatch::Http::UploadedFile.new(
        tempfile: File.new("#{Rails.root}/spec/tmp/avatar_2.jpg"),
        filename: "avatar_2.jpg"
      )

      new_attributes[:user_attributes][:personal_information_attributes][:avatar] = new_avatar
      put :update, { id: worker.to_param, worker: new_attributes }
      worker.reload

      expect(worker.user.email).to eq(new_attributes[:user_attributes][:email])
      expect(worker.user.personal_information.first_name).to eq(new_attributes[:user_attributes][:personal_information_attributes][:first_name])
      expect(worker.user.personal_information.last_name).to eq(new_attributes[:user_attributes][:personal_information_attributes][:last_name])
      expect(worker.user.personal_information.landline).to eq(new_attributes[:user_attributes][:personal_information_attributes][:landline])
      expect(worker.user.personal_information.cellphone).to eq(new_attributes[:user_attributes][:personal_information_attributes][:cellphone])
      expect(worker.user.personal_information.birth_date.to_s).to eq(new_attributes[:user_attributes][:personal_information_attributes][:birth_date])
      expect(worker.user.personal_information.territory_city_id).to eq(new_attributes[:user_attributes][:personal_information_attributes][:territory_city_id])
      expect(worker.user.personal_information.sex).to eq(new_attributes[:user_attributes][:personal_information_attributes][:sex])
      expect(worker.user.personal_information.company_marital_status_id).to eq(new_attributes[:user_attributes][:personal_information_attributes][:company_marital_status_id])
      expect(worker.user.personal_information.availability_replacements).to eq(new_attributes[:user_attributes][:personal_information_attributes][:availability_replacements])
      expect(worker.user.personal_information.availability_work_other_residence).to eq(new_attributes[:user_attributes][:personal_information_attributes][:availability_work_other_residence])  
      expect(worker.user.personal_information.avatar.file.filename).to eq(new_avatar.original_filename)
      should redirect_to worker_basis_path(worker)
    end

     it 'should update degrees' do
      worker = FactoryGirl.create(:worker_basis)
      @user.add_role :update, worker
      new_attributes = FactoryGirl.attributes_for(:worker_basis)
      professional_information = FactoryGirl.create(:people_professional_information, user: worker.user)
      degree = FactoryGirl.create(:people_degree, professional_information: professional_information)

      new_degrees_attributes = FactoryGirl.attributes_for_list(:people_degree, 2, professional_information_id: professional_information.id)
      new_attributes[:user_attributes] = {
        id: worker.user.id,
        professional_information_attributes: {
          id: professional_information.id,
          degrees_attributes: new_degrees_attributes.dup
        }
      }
      degree_attributes = degree.attributes
      degree_attributes[:_destroy] = 1
      new_attributes[:user_attributes][:professional_information_attributes][:degrees_attributes].push(degree_attributes)

      put :update, id: worker.id, worker: new_attributes
      worker.reload

      new_degrees_attributes.each_with_index do |degree_attr, index|
       degree = worker.user.professional_information.degrees.find_by(
          career_id: degree_attr[:career_id],
          institution_id: degree_attr[:institution_id],
          culmination_date: degree_attr[:culmination_date],
          condition: degree_attr[:condition],
        )
        expect(degree).to_not be nil
      end
    end

    it 'should update laboral_experiences' do
      worker = FactoryGirl.create(:worker_basis)
      @user.add_role :update, worker
      new_attributes = FactoryGirl.attributes_for(:worker_basis)
      professional_information = FactoryGirl.create(:people_professional_information, user: worker.user)

      laboral_experience = FactoryGirl.create :people_laboral_experience, professional_information: professional_information
      new_lb_experiences_attrs = FactoryGirl.attributes_for_list :people_laboral_experience, 3, professional_information_id: professional_information.id
      new_attributes[:user_attributes] = {
        id: worker.user.id,
        professional_information_attributes: {
          id: professional_information.id,
          laboral_experiences_attributes: new_lb_experiences_attrs.dup
        }
      }

      lb_experience_attributes = laboral_experience.attributes
      lb_experience_attributes[:_destroy] = 1
      new_attributes[:user_attributes][:professional_information_attributes][:laboral_experiences_attributes].push(lb_experience_attributes)
      put :update, id: worker.id, worker: new_attributes
      worker.reload

      new_lb_experiences_attrs.each_with_index do |lb_experience_attr, index|
        laboral_experience = worker.user.professional_information.laboral_experiences.find_by(
          position: lb_experience_attr[:position],
          company: lb_experience_attr[:company],
          since_date: lb_experience_attr[:since_date],
          until_date: lb_experience_attr[:until_date],
        )
        expect(laboral_experience).to_not be nil
      end
    end

    context "with invalid params" do
      it "" do
        worker = FactoryGirl.create(:worker_basis)
        new_attributes = FactoryGirl.attributes_for(:worker_invalid)
        @user.add_role :update, worker
        request = put :update, id: worker.to_param, worker: new_attributes
        worker.reload
        expect(assigns(:worker).errors.any?).to be true
        expect(request).to render_template("worker/bases/edit")
      end
    end
  end

  context "GET User Interface to create a worker" do
    login_executive_with_role(:new, Worker::Base)
    it "should response with form new worker" do
      get :new
      expect(response.status).to eq(200)
    end
  end

  context "GET User Interface to search workers" do
    login_executive_with_role(:admin, Worker::Base)
    it "should response with ok and return all workers" do
      workers = FactoryGirl.create_list(:worker_basis,5)
      get :search
      expect(response.status).to eq(200)
      expect(assigns(:workers).count).to eq(Worker::Base.all.count)
      expect(assigns(:workers).map(&:id)).to eq(Worker::Base.all.map(&:id))
    end

    it "should response with ok and return filtered query based on param" do
      workers = FactoryGirl.create_list(:worker_basis,5)
      worker = workers.last
      get :search, query: worker.email
      expect(response.status).to eq(200)
      expect(assigns(:workers).count).to eq(1)
      expect(assigns(:workers).first).to eq(worker)
    end
  end


  context "GET User Interface to see a Worker" do
    login_executive_with_role(:admin, Worker::Base)
    it "should response ok page" do
      worker = FactoryGirl.create(:worker_basis)
      get :show, id: worker.to_param
      expect(response.status).to eq(200)
    end
  end

  context "Request whe current_user is not signed in" do
    it "should redirect_to new_user_session_path when CREATE Worker" do
      expect{
        post :create, worker: FactoryGirl.attributes_for(:worker_basis)
      }.to change(Worker::Base, :count).by(0)
      should redirect_to new_user_session_path
    end

    it "should redirect_to new_user_session_path when form new worker" do
      get :new
      should redirect_to new_user_session_path
    end

    it "should redirect_to new_user_session_path when GET User Interface to search workers" do
      get :search
      should redirect_to new_user_session_path
    end

    it "should redirect_to new_user_session_path GET User Worker" do
      worker = FactoryGirl.create(:worker_basis)
      get :show, id: worker.to_param
      should redirect_to new_user_session_path
    end

    it "should response with page when GET a form for new worker" do
      worker = FactoryGirl.create(:worker_basis)
      get :edit, id: worker.to_param
      should redirect_to new_user_session_path
    end

    it "should response with page when UPDATE a worker" do
      worker = FactoryGirl.create(:worker_basis)
      put :update, id: worker.to_param
      should redirect_to new_user_session_path
    end
  end

  context "Request whe current_user is not signed in" do
    login_executive_without_role

    it "should response with page when GET a form for new worker" do
      get :new
      should respond_with 403
    end

    it "should response with page when UPDATE a worker" do
      worker = FactoryGirl.create(:worker_basis)
      put :update, id: worker.to_param
      should respond_with 403
    end

    it "should response with page when GET a form for new worker" do
      worker = FactoryGirl.create(:worker_basis)
      get :edit, id: worker.to_param
      should respond_with 403
    end

    it "should response with page 403 an not create worker and user when CREATE Worker with" do
      expect{
        post :create, worker: FactoryGirl.attributes_for(:worker_basis)
      }.to change(Worker::Base, :count).by(0)
      should respond_with 403
    end

    it "should response with page 403 GET User Interface to search workers" do
      get :search
      should respond_with 403
    end

    it "should response with status 403 GET User Interface to see a Worker" do
      worker = FactoryGirl.create(:worker_basis)
      get :show, id: worker.to_param
      should respond_with 403
    end
  end
end
