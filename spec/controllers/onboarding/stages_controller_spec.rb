# coding: utf-8
require 'rails_helper'

RSpec.describe Onboarding::StagesController, :type => :controller do
  render_views

  describe 'POST documents' do
    login_executive_without_role
    let(:descriptor) { FactoryGirl.create(:document_descriptor) }

    before :each do
      @onboarding_stage = FactoryGirl.create(:onboarding_stage)
      @onboarding_stage.enrollment.update(stage: @onboarding_stage)
      course = @onboarding_stage.enrollment.course
      course.work_flow.push(course.work_flow.first)
      course.document_group = descriptor.document_groups.first
      course.save
      request.env["HTTP_REFERER"] = course_onboarding_stage_url(course, @onboarding_stage)
    end

    context "when onboarding course has a document group" do
      it 'when user has admin role over Onboarding::Stage, should upload a document' do
        @user.add_role :admin, Onboarding::Stage
      end

      it 'when user has admin role over the course, should upload a document' do
        @user.add_role :admin, @onboarding_stage.enrollment.course
      end

      it 'when user has admin role over the stage, should upload a document' do
        @user.add_role :admin, @onboarding_stage
      end

      it 'when user has permission only over the course, should upload a document' do
        @user.add_role Onboarding::Stage.to_s.tableize.to_sym, @onboarding_stage.enrollment.course
      end

      after :each do
        expect {
          post :do_upload_document,
            id: @onboarding_stage.enrollment.course.id,
            stage_id: @onboarding_stage,
            applicant_documents: {
              descriptor.to_sym => Rack::Test::UploadedFile.new("#{Rails.root}/spec/tmp/avatar.jpg", 'image/jpeg')
            }
        }.to change(Document::Record, :count).by(1)

        should redirect_to course_onboarding_stage_path(id: @onboarding_stage.enrollment.course.id, stage_id: @onboarding_stage.id)


        expect {
          post :do_upload_document,
            id: @onboarding_stage.enrollment.course.id,
            stage_id: @onboarding_stage,
            applicant_documents: {descriptor.to_sym => Rack::Test::UploadedFile.new("#{Rails.root}/spec/tmp/avatar.jpg", 'image/jpeg')},
            format: :json
        }.to change(Document::Record, :count).by(1)

        document_records = assigns(:document_records)
        document_records.each do |record|
          expect(record.applicant).to eq @onboarding_stage.enrollment.applicant
          expect(record.enrollment).to eq @onboarding_stage.enrollment
        end
      end
    end

    context "when invalid params" do
      it "should redirect_to onboarding stage with flash errors" do
        @user.add_role(:admin)

        expect {
          post :do_upload_document,
            id: @onboarding_stage.enrollment.course.id,
            stage_id: @onboarding_stage
        }.to change(Document::Record, :count).by(0)

        expect(flash[:error]).to eq I18n.t("applicant.upload_documents.upload_error.invalid_args")
        should redirect_to course_onboarding_stage_path(id: @onboarding_stage.enrollment.course.id, stage_id: @onboarding_stage.id)

        expect {
          post :do_upload_document,
            id: @onboarding_stage.enrollment.course.id,
            stage_id: @onboarding_stage,
            format: :json
        }.to change(Document::Record, :count).by(0)

        document_records = assigns(:document_records)
        expect(document_records.empty?).to be true
      end
    end

    context "when onboarding course has a document group" do
      it 'when user has no role, should not upload a document' do

        expect {
          post :do_upload_document,
            id: @onboarding_stage.enrollment.course.id,
            stage_id: @onboarding_stage,
            applicant_documents: { descriptor.to_sym => Rack::Test::UploadedFile.new("#{Rails.root}/spec/tmp/avatar.jpg", 'image/jpeg')}
        }.to change(Document::Record, :count).by(0)

        expect(response.status).to eq 403

        expect {
          post :do_upload_document,
            id: @onboarding_stage.enrollment.course.id,
            stage_id: @onboarding_stage,
            applicant_documents: { descriptor.to_sym => Rack::Test::UploadedFile.new("#{Rails.root}/spec/tmp/avatar.jpg", 'image/jpeg')},
            format: :json
        }.to change(Document::Record, :count).by(0)

        expect(response.status).to eq 403
      end
    end
  end

  describe "GET upload_requested_documents" do
    let(:onboarding_stage) { FactoryGirl.create(:onboarding_stage) }

    it 'should show upload requested documents form' do
      token = Digest::MD5.new
      token << onboarding_stage.enrollment.applicant.email
      token = "#{token}-#{onboarding_stage.enrollment.id}"
      get :upload_requested_documents, id: onboarding_stage.enrollment.course.id, stage_id: onboarding_stage.to_param, token: token
      expect(response.status).to eq 200
    end
  end

  describe "POST do_upload_requested_documents" do
    let(:onboarding_stage) { FactoryGirl.create(:onboarding_stage) }

    it 'should upload requested documents and redirect to form with token' do
      token = Digest::MD5.new
      token << onboarding_stage.enrollment.applicant.email
      token = "#{token}-#{onboarding_stage.enrollment.id}"
      document_group = onboarding_stage.enrollment.course.document_group
      document_descriptor = FactoryGirl.create(:document_descriptor, document_groups: [document_group], allowed_extensions: 'jpg')
      expect{
        post :do_upload_requested_documents, id: onboarding_stage.enrollment.course.id, stage_id: onboarding_stage, token: token,
             applicant_documents: { document_descriptor.to_sym => Rack::Test::UploadedFile.new("#{Rails.root}/spec/tmp/avatar.jpg", 'image/jpeg')}
      }.to change(Document::Record, :count).by(1)
      expect(Document::Record.last.applicant_id).to eq onboarding_stage.enrollment.applicant.id
      expect(response).to redirect_to(upload_requested_documents_course_onboarding_stage_path(token: token))

      expect{
        post :do_upload_requested_documents, id: onboarding_stage.enrollment.course.id, stage_id: onboarding_stage, token: token, format: :json,
             applicant_documents: { document_descriptor.to_sym => Rack::Test::UploadedFile.new("#{Rails.root}/spec/tmp/avatar.jpg", 'image/jpeg')}
      }.to change(Document::Record, :count).by(1)


      expect(Document::Record.last.applicant_id).to eq onboarding_stage.enrollment.applicant.id
      expect(response.status).to eq 200
      expect(response).to render_template 'document/records/index'
    end

    it "should response 403 without token" do
      document_group = onboarding_stage.enrollment.course.document_group
      document_descriptor = FactoryGirl.create(:document_descriptor, document_groups: [document_group], allowed_extensions: 'jpg')

      #FORMAT HTML
      expect{
        post :do_upload_requested_documents, id: onboarding_stage.enrollment.course.id, stage_id: onboarding_stage,
             applicant_documents: { document_descriptor.to_sym => Rack::Test::UploadedFile.new("#{Rails.root}/spec/tmp/avatar.jpg", 'image/jpeg')}
      }.to change(Document::Record, :count).by(0)
      expect(response.status).to eq 403

      #FORMAT JSON
      expect{
        post :do_upload_requested_documents, id: onboarding_stage.enrollment.course.id, stage_id: onboarding_stage, format: :json,
             applicant_documents: { document_descriptor.to_sym => Rack::Test::UploadedFile.new("#{Rails.root}/spec/tmp/avatar.jpg", 'image/jpeg')}
      }.to change(Document::Record, :count).by(0)
      expect(response.status).to eq 403
    end


    it "should response 403 with wrong token" do
      document_group = onboarding_stage.enrollment.course.document_group
      document_descriptor = FactoryGirl.create(:document_descriptor, document_groups: [document_group], allowed_extensions: 'jpg')

      #FORMAT HTML
      expect{
        post :do_upload_requested_documents, id: onboarding_stage.enrollment.course.id, stage_id: onboarding_stage, token: "bad_token",
             applicant_documents: { document_descriptor.to_sym => Rack::Test::UploadedFile.new("#{Rails.root}/spec/tmp/avatar.jpg", 'image/jpeg')}
      }.to change(Document::Record, :count).by(0)

      expect(response.status).to eq 403

      #FORMAT JSON
      expect{
        post :do_upload_requested_documents, id: onboarding_stage.enrollment.course.id, stage_id: onboarding_stage, token: "bad_token",
             applicant_documents: { document_descriptor.to_sym => Rack::Test::UploadedFile.new("#{Rails.root}/spec/tmp/avatar.jpg", 'image/jpeg')},
             format: :json
      }.to change(Document::Record, :count).by(0)

      expect(response.status).to eq 403
    end
  end

  describe "POST documents_request" do

    let(:onboarding_stage) { FactoryGirl.create(:onboarding_stage) }
    let!(:enrollment){onboarding_stage.enrollment}
    let(:message) {FFaker::Lorem.sentence}

    before :each do
      FactoryGirl.create(:document_descriptor, internal: false, only_onboarding: false)
    end

    context "when user is logged in" do
      login_executive_without_role

      context "and user has a valid role" do
        
        context 'and document descriptors are found' do
          let(:delyed_job_count_expect){
            change(Delayed::Job, :count).by(1)
          }
          let(:email_expeceted){
            job = YAML.load(Delayed::Job.last.handler)
            expect(job.method_name).to eq :documents_request
            expect(job.args).to include onboarding_stage
            expect(job.args).to match_array [onboarding_stage, enrollment.document_descriptors.last(1), message]
            expect(job.object).to eq Onboarding::StageMailer
          }

          it "with admin_role should send an email" do
            @user.add_role :admin
          end
        end

        after :each do
          expect{
            post :documents_request, id: onboarding_stage.enrollment.course,\
              stage_id: onboarding_stage.id,\
              document_descriptors: enrollment.document_descriptors.map(&:name),
              message: message,
              format: :json
          }.to delyed_job_count_expect
          email_expeceted

        end
      end

      context "when document_descriptor is not found" do
        it "should response flash with error" do
          @user.add_role :admin
          document_descriptor_name = enrollment.document_descriptors.first.name
          enrollment.document_descriptors.first.destroy

          expect{
            post :documents_request, id: enrollment.course,\
              stage_id: onboarding_stage.id,\
              document_descriptors: [document_descriptor_name],
              message: message,
              format: :json
          }.to change(Delayed::Job, :count).by(0)

          expect(JSON.parse(response.body)["error"]).to eq I18n.t("onboarding/stage.messages.documents_request.unsuccess")

        end
      end
    end

    context "when user is not logged in" do
      it "should response error 401" do
        post :documents_request, id: onboarding_stage.enrollment.course,\
            stage_id: onboarding_stage.id,\
            document_descriptors: enrollment.document_descriptors.map(&:name),
            message: message,
            format: :json

        expect(response.status).to eq 401
      end
    end
  end

end
