require 'rails_helper'

RSpec.describe Interview::ReportsController, :type => :controller do
  render_views
  let(:work_flow) {
    work_flow = [
      {type: :module, class_name: 'Interview::Stage', name: 'Interview'},
      {type: :module, class_name: 'Onboarding::Stage', name: 'Onboarding'},
      {type: :module, class_name: 'Engagement::Stage', name: 'Contratación'}
    ]
  }

  describe 'POST create Interview Report' do
    before :each do
      @interview_report_attribtues = FactoryGirl.attributes_for(:interview_report)
      @interview_stage = Interview::Stage.find(@interview_report_attribtues[:interview_stage_id])
      @course = @interview_stage.enrollment.course
      @course.update(work_flow: work_flow)
    end

    context 'when current_user is not loged in' do
      it 'should redirect_to new_user_session_path' do
        expect{
          post :create,
               id: @course.to_param,
               stage_id: @interview_stage.to_param,
               interview_report: @interview_report_attribtues
        }.to change(Interview::Report, :count).by(0)
        should redirect_to new_user_session_path
      end
    end

    context 'when current_user is loged in' do
      login_executive_without_role
      context 'and he not has role valid' do
        it 'should response with page 403' do
          expect{
            post :create,
                 id: @course.to_param,
                 stage_id: @interview_stage.to_param,
                 interview_report: @interview_report_attribtues
          }.to change(Interview::Report, :count).by(0)
          expect(response.status).to eq 403
        end
      end

      context 'and he has a valid role' do
        context 'with valid attributes' do
          it 'with admin role' do
            @user.add_role :admin
          end

          it 'with admin role on @course' do
            @user.add_role :admin, @course
          end

          it 'with admin role on Interview::Stage' do
            @user.add_role :admin, Interview::Stage
          end

          it "with :admin on @interview_stage" do
            @user.add_role :admin, @interview_stage
          end

          after :each do
            expect{
              post :create,
                   id: @course.to_param,
                   stage_id: @interview_stage.to_param,
                   interview_report: @interview_report_attribtues
            }.to change(Interview::Report, :count).by(1)
            should redirect_to course_interview_stage_path(id: @course, stage_id: @interview_stage.id)
            expect(flash[:success]).to eq I18n.t('interview.report.created.success')
          end
        end

        context 'with invalid attributes' do
          before :each do
            @interview_report_attribtues = FactoryGirl.attributes_for(:interview_report_invalid)
            @interview_stage = Interview::Stage.find(@interview_report_attribtues[:interview_stage_id])
            @course = @interview_stage.enrollment.course
            @course.update(work_flow: work_flow)
          end

          it 'with admin role' do
            @user.add_role :admin
          end

          it 'with admin role on @course' do
            @user.add_role :admin, @course
          end

          it 'with admin role on Interview::Stage' do
            @user.add_role :admin, Interview::Stage
          end

          it "with :admin on @interview_stage" do
            @user.add_role :admin, @interview_stage
          end

          after :each do
            expect{ post :create, id: @course.to_param, stage_id: @interview_stage.to_param, interview_report: @interview_report_attribtues }.to change(Interview::Report, :count).by(0)
            should render_template('interview/stages/show')
            should render_template('interview/reports/_form')
          end
        end
      end
    end
  end
end
