require 'rails_helper'

RSpec.describe Interview::StagesController, :type => :controller do
  render_views
  describe 'GET show Interview Stage' do
    before :each do
      @interview_stage = FactoryGirl.create(:interview_stage)
      @course = @interview_stage.enrollment.course

    end

    context 'when current_user has not log in' do
      it 'should redirect_to new_user_session_path' do
        get :show, id: @course.id, stage_id: @interview_stage.id
        should redirect_to new_user_session_path
      end
    end

    context 'when current_user has loged in' do
      login_executive_without_role
      context 'and has role invalid' do
        it 'admin Interview::Stage' do
          @user.add_role :invalid, Interview::Stage
        end

        after :each do
          get :show, id: @course.to_param, stage_id: @interview_stage.to_param
          expect(response.status).to eq 403
        end
      end

      context 'and has role valid' do
        it 'admin on @interview_stage' do
          @user.add_role :admin, @interview_stage
        end

        it 'admin on @course' do
          @user.add_role :admin, @course
        end

        after :each do
          get :show, id: @course.to_param, stage_id: @interview_stage.to_param
          expect(response.status).to eq 200
        end
      end
    end
  end

end
