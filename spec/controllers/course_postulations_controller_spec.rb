require 'rails_helper'

RSpec.describe CoursePostulationsController, type: :controller do
  render_views

  let(:course) {FactoryGirl.create(:course, type_enrollment: "Enrollment::Dynamic")}
  let(:cant_postulations) { 5 }
  let(:minisite_publication) { FactoryGirl.create(:minisite_publication, course: course) }
  let(:minisite_postulations) {FactoryGirl.create_list(:minisite_postulation, cant_postulations, minisite_publication: minisite_publication, is_accepted: nil, accepted_at: nil)}
  let(:course_postulations) { minisite_postulations.map(&:course_postulation) }

  describe '#index' do
    before :each do
      course_postulations.map(&:id)
    end

    context 'when current_user is not logged in' do
      it 'should redirect_to new_user_session_path' do
        get :index, id: course.id, format: :json
        expect(assigns(:course_postulations)).to be nil
        expect(response.status).to eq 401
      end
    end

    context 'when current_user is logged in' do
      login_executive_without_role

      context 'and does not has a role valid' do
        it 'should response with http status 403' do
          get :index, id: course.id, format: :json
          expect(assigns(:course_postulations)).to be nil
          expect(response.status).to eq 403
        end
      end

      context 'and has a role valid' do
        it 'as admin, should response with all course_postulations' do
          @user.add_role :admin
        end

        it 'as admin_publications on course, should response with all course_postulations' do
          @user.add_role :admin_publications, course
        end

        it 'as admin on course, should response with all course_postulations' do
          @user.add_role :admin, course
        end

        after :each do
          get :index, id: course.id, format: :json
          expect(assigns(:course_postulations)).to match_array course_postulations
        end
      end
    end
  end

  describe "ACCEPT postulations" do
    context "when user is not signed in" do
      it "should render 401" do
        course_postulation_ids = course_postulations.map(&:id)
        post :accept, id: course.id, course_postulation_ids: course_postulation_ids, format: :json
        expect(response.status).to be 401
      end
    end

    context "when current_user is logged in" do
      login_executive_without_role

      context "with a valid role" do
        context 'and postulations are not accepted yet' do
          it "admin role" do
            @user.add_role :admin
          end

          it "admin_publications on @course role" do
            @user.add_role :admin_publications, course
          end

          after :each do
            course_postulation_ids =  course_postulations.map(&:id)
            expect{
              post :accept, id: course.id, course_postulation_ids: course_postulation_ids, format: :json
            }.to change(course.enrollments, :count).by(cant_postulations)

            course_postulations.each do |postulation|
              postulation.reload
              expect(postulation.is_accepted).to be true
            end
          end
        end

        context 'and postulations are accepta already' do
          before :each do
            course_postulations.map(&:accept)
          end

          it "should response error with accepted postulations" do
            @user.add_role :admin
            course_postulation_ids = course_postulations.map(&:id)

            expect{
              post :accept, id: course.id, course_postulation_ids: course_postulation_ids, format: :json
            }.not_to change(course.enrollments, :count)

            course_postulation_with_errors = assigns(:postulations)
            course_postulation_with_errors.each do |postulation|
              expect(postulation.errors.any?).to be true
              expect(postulation.errors.messages[:accept]).to  eq ["Postulación ya fue aceptada."]
            end
          end
        end

      end

      context "and has role invalid" do
        it "should not accept any postulations" do
          @user.add_role :invite
        end

        after :each do
          course_postulation_ids =  course_postulations.map(&:id)
          post :accept, id: course.id, course_postulation_ids: course_postulation_ids, format: :json
          expect(response.status).to eq 403
        end
      end
    end
  end

  describe "REJECT postulations" do

    context "when user is not signed in" do
      it "should render 401" do
        course_postulation_ids = course_postulations.map(&:id)
        post :reject, id: course.id, course_postulation_ids: course_postulation_ids, format: :json
        expect(response.status).to be 401
      end
    end

    context "when current_user is logged in" do
      login_executive_without_role

      context "with a role valid" do
        context 'and postulations are not rejected yet' do

          it "should reject all postulations with role admin" do
            @user.add_role :admin
          end

          it "should reject all postulations with role admin_publications on course" do
            @user.add_role :admin_publications, course
          end

          after :each do
            course_postulation_ids = course_postulations.map(&:id)
            expect{
              post :reject, id: course.id, course_postulation_ids: course_postulation_ids, format: :json
            }.to change(course.enrollments, :count).by(0).and \
                 change(Delayed::Job, :count).by(cant_postulations)

            course_postulations.each do |postulation|
              postulation.reload
              expect(postulation.is_accepted).to be false
            end
          end
        end

        context 'and postulations are rejected already' do
          before :each do
            course_postulations.map(&:reject)
          end

          it "should response error with rejected postulations" do
            @user.add_role :admin
            course_postulation_ids = course_postulations.map(&:id)
            expect{
              post :reject, id: course.id, course_postulation_ids: course_postulation_ids, format: :json
            }.not_to change(course.enrollments, :count)

            course_postulation_with_errors = assigns(:postulations)
            course_postulation_with_errors.each do |postulation|
              expect(postulation.errors.any?).to be true
              expect(postulation.errors.messages[:reject]).to  eq ["Postulación ha sido descartada con anterioridad."]
            end
          end
        end

      end

      context "and has role invalid" do
        after :each do
          post :reject, minisite_postulation_ids: minisite_postulations.map(&:id), id: course.id, format: :json
          expect(response.status).to eq 403
        end

        it "invite role" do
          @user.add_role :invite
        end
      end
    end
  end

  describe "#reset" do
    context "when user is not signed in" do
      it "should render 401" do
        course_postulation_ids = course_postulations.map(&:id)
        post :reset, id: course.id, course_postulation_ids: course_postulation_ids, format: :json
        expect(response.status).to be 401
      end
    end

    context "when current_user is logged in" do
      login_executive_without_role

      context "with a role valid" do
        context 'and postulations current status are rejected' do
          it "should reset all postulations with role admin" do
            @user.add_role :admin
          end

          it "should reset all postulations with role admin on course" do
            @user.add_role(:admin, course)
          end

          after :each do
            course_postulation_ids = course_postulations.map(&:id)
            expect{
              post :reset, id: course.id, course_postulation_ids: course_postulation_ids, format: :json
            }.to_not change(course.enrollments, :count)

            course_postulations.each do |postulation|
              postulation.reload
              expect(postulation.is_accepted).to be nil
              expect(postulation.accepted_at).to be nil
            end
          end
        end

        context 'and postulations current status are accepted' do
          context 'and postulations have enrollments releated' do
            context 'and enrollments do not have stages' do

              it "should reset all postulations with role admin" do
                @user.add_role :admin
              end

              it "should reset all postulations with role admin on course" do
                @user.add_role(:admin, course)
              end

              after :each do
                post :accept, id: course.id, course_postulation_ids: course_postulations.map(&:id), format: :json
                expect(course.enrollments.count).to eq(cant_postulations)
                course_postulation_ids = course_postulations.map(&:id)
                expect{
                  post :reset, id: course.id, course_postulation_ids: course_postulation_ids, format: :json
                }.to change(course.enrollments, :count).by(-cant_postulations)

                course_postulations.each do |postulation|
                  postulation.reload
                  expect(postulation.is_accepted).to be nil
                end
              end
            end

            context 'and enrollments have stages releated' do
              before :each do
                @user.add_role :admin
                @postulations = FactoryGirl.create_list(:minisite_postulation, 5, is_accepted: true)
                @postulations.each do |postulation|
                  enrollment = FactoryGirl.create(:enrollment_basis, applicant: postulation.applicant)
                  current_stage = FactoryGirl.build(:interview_stage, enrollment: enrollment)
                  current_stage.save(validate: false)
                end
              end

              it "should not reset and raise error" do
                course_postulation_ids = @postulations.map(&:id)
                expect{
                  post :reset, id: course.id, course_postulation_ids: course_postulation_ids, format: :json
                }.to change(course.enrollments, :count).by(0)

                course_postulation_with_errors = assigns(:postulations)
                course_postulation_with_errors.each do |postulation|
                  expect(postulation.is_accepted).to be true
                  expect(postulation.errors.any?).to be true
                  expect(postulation.errors.messages[:reset]).to eq(["La postulación posee etapas asociadas."])
                end
              end
            end
          end

          context 'and postulations do not have enrollments related' do
            it "should reset all postulations with role admin" do
              @user.add_role :admin
            end

            it "should reset all postulations with role admin on course" do
              @user.add_role(:admin, course)
            end

            after :each do
              course_postulations.map(&:accept)
              course_postulation_ids = course_postulations.map(&:id)
              expect{
                post :reset, id: course.id, course_postulation_ids: course_postulation_ids, format: :json
              }.to change(course.enrollments, :count).by(0)

              course_postulations.each do |postulation|
                postulation.reload
                expect(postulation.is_accepted).to be nil
              end
            end
          end
        end

        context 'and postulations current status are pending' do
          before :each do
            course_postulations.map(&:reset)
          end

          it "should response error" do
            @user.add_role :admin
            course_postulation_ids = course_postulations.map(&:id)
            expect{
              post :reset, id: course.id, course_postulation_ids: course_postulation_ids, format: :json
            }.not_to change(course.enrollments, :count)

            course_postulation_with_errors = assigns(:postulations)
            course_postulation_with_errors.each do |postulation|
              expect(postulation.errors.any?).to be true
              expect(postulation.errors.messages[:reset]).to eq(["La postulación ya se encuentra pendiente."])
            end
          end
        end

      end

      context "and has role invalid" do
        after :each do
          post :reset, minisite_postulation_ids: minisite_postulations.map(&:id), id: course.id, format: :json
          expect(response.status).to eq 403
        end

        it "invite role" do
          @user.add_role :invite
        end
      end
    end
  end

  describe '#export' do
    let(:course){
      FactoryGirl.create(:course)
    }
    let(:minisite_publication){
      FactoryGirl.create(:minisite_publication, course: course)
    }
    let(:trabajando_publication){
      FactoryGirl.create(:trabajando_publication, course: course)
    }
    let(:trabajando_postulations){
      FactoryGirl.create_list(
        :trabajando_postulation, 2,
        trabajando_publication: trabajando_publication
      )
    }
    let(:minisite_postulations){
      FactoryGirl.create_list(
        :minisite_postulation, 2,
        minisite_publication: minisite_publication
      )
    }
    let(:course_postulations){
      minisite_postulations.map(&:course_postulation) +
      trabajando_postulations.map(&:course_postulation)
    }

    context 'when current_user is not logged in' do
      let(:expected_response){
        expect(response.status).to eq 401
      }
      it 'should redirect_to new_user_session_path' do
      end
    end

    context 'when is logged in' do
      login_executive_without_role

      context 'and does nos has a valid role' do
        let(:expected_response){
          expect(response.status).to eq 403
        }
        it 'should response with sttus 403' do
        end
      end

      context 'and has a valid role' do
        let(:expected_response){
          expect(response.status).to eq 200
        }

        it 'should response with excele file (current_user has admin role on course)' do
          @user.add_role :admin, course
        end
      end
    end

    after :each do
      get :export, {
        id: course.id,
        course_postulation_ids: course_postulations.map(&:id),
        format: :xlsx
      }
      expected_response
    end
  end
end
