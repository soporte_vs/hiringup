require 'rails_helper'

RSpec.describe SkillInterview::ReportsController, :type => :controller do
  render_views
  let(:work_flow) {
    work_flow = [
      {type: :module, class_name: 'SkillInterview::Stage', name: 'SkillInterview'},
      {type: :module, class_name: 'Onboarding::Stage', name: 'Onboarding'},
      {type: :module, class_name: 'Engagement::Stage', name: 'Contratación'}
    ]
  }

  describe 'POST create SkillInterview Report' do
    before :each do
      @skill_interview_report_attribtues = FactoryGirl.attributes_for(:skill_interview_report)
      @skill_interview_report_attribtues[:documents] = [
        ActionDispatch::Http::UploadedFile.new(:tempfile => File.new("#{Rails.root}/spec/tmp/document.doc"), filename: "document.doc"),
        ActionDispatch::Http::UploadedFile.new(:tempfile => File.new("#{Rails.root}/spec/tmp/document.doc"), filename: "document.doc")
      ]
      @skill_interview_stage = SkillInterview::Stage.find(@skill_interview_report_attribtues[:skill_interview_stage_id])
      @course = @skill_interview_stage.enrollment.course
      @course.update(work_flow: work_flow)
    end

    context 'when current_user is not loged in' do
      it 'should redirect_to new_user_session_path' do
        expect{
          post :create,
               id: @course.to_param,
               stage_id: @skill_interview_stage.to_param,
               skill_interview_report: @skill_interview_report_attribtues
        }.to change(SkillInterview::Report, :count).by(0)
        should redirect_to new_user_session_path
      end
    end

    context 'when current_user is loged in' do
      login_executive_without_role
      context 'and he not has role valid' do
        it 'should response with page 403' do
          expect{
            post :create,
                 id: @course.to_param,
                 stage_id: @skill_interview_stage.to_param,
                 skill_interview_report: @skill_interview_report_attribtues
          }.to change(SkillInterview::Report, :count).by(0)
          expect(response.status).to eq 403
        end
      end

      context 'and he has a valid role' do
        context 'with valid attributes' do
          it 'with admin role' do
            @user.add_role :admin
          end

          it 'with admin role on @course' do
            @user.add_role :admin, @course
          end

          it 'with admin role on SkillInterview::Stage' do
            @user.add_role :admin, SkillInterview::Stage
          end

          it "with #{SkillInterview::Stage.to_s.tableize.to_sym} on @course" do
            @user.add_role SkillInterview::Stage.to_s.tableize.to_sym, @course
          end

          it "with :admin on @skill_interview_stage" do
            @user.add_role :admin, @skill_interview_stage
          end

          after :each do
            expect{
              post :create,
                   id: @course.to_param,
                   stage_id: @skill_interview_stage.to_param,
                   skill_interview_report: @skill_interview_report_attribtues
            }.to change(SkillInterview::Report, :count).by(1)
            should redirect_to course_skill_interview_stage_path(id: @course, stage_id: @skill_interview_stage.id)
            expect(flash[:success]).to eq I18n.t('skill_interview.report.created.success')
          end
        end

        context 'with invalid attributes' do
          before :each do
            @skill_interview_report_attribtues = FactoryGirl.attributes_for(:skill_interview_report_invalid)
            @skill_interview_stage = SkillInterview::Stage.find(@skill_interview_report_attribtues[:skill_interview_stage_id])
            @course = @skill_interview_stage.enrollment.course
            @course.update(work_flow: work_flow)
          end

          it 'with admin role' do
            @user.add_role :admin
          end

          it 'with admin role on @course' do
            @user.add_role :admin, @course
          end

          it 'with admin role on SkillInterview::Stage' do
            @user.add_role :admin, SkillInterview::Stage
          end

          it "with #{SkillInterview::Stage.to_s.tableize.to_sym} on @course" do
            @user.add_role SkillInterview::Stage.to_s.tableize.to_sym, @course
          end

          it "with :admin on @skill_interview_stage" do
            @user.add_role :admin, @skill_interview_stage
          end

          after :each do
            expect{
              post :create,
                   id: @course.to_param,
                   stage_id: @skill_interview_stage.to_param,
                   skill_interview_report: @skill_interview_report_attribtues
            }.to change(SkillInterview::Report, :count).by(0)
            should render_template('skill_interview/stages/show')
            should render_template('skill_interview/reports/_form')
          end
        end

      end
    end
  end

  it_should_behave_like 'a stage with reports', :skill_interview_report, :skill_interview_stage
end
