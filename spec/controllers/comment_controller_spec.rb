require 'rails_helper'

RSpec.describe CommentController, type: :controller do
  render_views

  describe "GET index list comment" do
    let(:publication) { FactoryGirl.create(:minisite_publication) }

    context "when user is not signed" do
      it 'should response with 200' do
        get :index, object_type: publication.class.name, object_id: publication.id, page: 1, format: :json
        expect(response.status).to eq(200)
      end
    end
  end

  describe "POST create comment" do
    let(:publication) { FactoryGirl.create(:minisite_publication) }

    context 'when user is not signed' do
      it 'should response with page 401' do
        post :create, object_type: publication.class.name, object_id: publication.id, content: FFaker::Lorem.paragraph, format: :json
        expect(response.status).to eq(401)
      end
    end

    context 'when user is signed' do
      login_executive_without_role
      it 'should response 200' do
        expect{
          post :create, object_type: publication.class.name, object_id: publication.id, content: FFaker::Lorem.paragraph, format: :json
        }.to change(@user.comments, :count).by(1)
        expect(response.status).to eq(200)
      end
      it 'sould response error 422' do
        expect{
          post :create, object_type: publication.class.name, object_id: publication.id, format: :json
        }.to change(@user.comments, :count).by(0)
        expect(response.status).to eq(422)
        expect(JSON.parse(response.body) == { "errors": {"content":["Debes ingresar un comentario"] }})
      end
    end
  end
end
