require 'rails_helper'

RSpec.describe Applicant::ExternalsController, :type => :controller do
  context "Create User Applicant External" do
    it "should be a new applicant_external" do
      expect {
        post :create, applicant_external: FactoryGirl.attributes_for(:applicant_external)
      }.to change(Applicant::External, :count).by(1).and change(User, :count).by(1)
    end
  end
  context "Try create a duplicate applicant_external" do
    it "should not duplicate applicant_external" do
      post :create, applicant_external: FactoryGirl.attributes_for(:applicant_external)
      expect {
        post :create, applicant_external: FactoryGirl.attributes_for(:applicant_external)
      }.to change(Applicant::External, :count).by(0).and change(User, :count).by(0)
    end
  end
  context "Try create a duplicate applicant_external" do
    login_executive_without_role
    it "should be invalid (User Signed In)" do
      expect {
        post :create, applicant_external: FactoryGirl.attributes_for(:applicant_external)
      }.to change(Applicant::External, :count).by(0).and change(User, :count).by(0)
    end
  end
  context "Get Sign Up Interface Applicant Extern" do
    render_views
    it "should render view sign_up" do
      get "new"
      expect(response.status).to eq(200)
    end
  end
end
