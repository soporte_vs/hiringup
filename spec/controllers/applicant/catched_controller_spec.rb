
require 'rails_helper'

RSpec.describe Applicant::CatchedsController, :type => :controller do
  render_views

  context "Create User Applicant Catched" do
    login_executive_with_role(:create,Applicant::Catched)
    it "should be valid" do
      expect(Delayed::Job.count).to eq(0)
      attributes = FactoryGirl.attributes_for(:applicant_catched_invitable)
      expect {
        post :create, :applicant_catched => attributes
      }.to change(Applicant::Catched, :count).by(1).and change(User, :count).by(1).and change(People::PersonalInformation, :count).by(1)
      user = User.last
      should redirect_to applicant_basis_path(id: user.applicant.id)
      expect(Delayed::Job.count).to eq(1)
      expect(Delayed::Job.last.handler.include? user.email).to be true
      expect {
        post :create, :applicant_catched => attributes
      }.to change(Applicant::Catched, :count).by(0).and change(User, :count).by(0)
    end
  end

  context "Create User Applicant Catched without login" do
    it "should be invalid because current_user not signed in" do
      expect {
        post :create, :applicant_catched => FactoryGirl.attributes_for(:applicant_catched_invitable)
      }.to change(Applicant::Catched, :count).by(0).and change(User, :count).by(0)
      should redirect_to new_user_session_path
    end
  end

  context "Create User Applicant Catched without role" do
    login_executive_without_role
    it "should be invalid because current_user has not role " do
      expect {
        post :create, :applicant_catched => FactoryGirl.attributes_for(:applicant_catched_invitable)
      }.to change(Applicant::Catched, :count).by(0).and change(User, :count).by(0)
      expect(response.status).to eq(403)
    end
  end

  context "Try Get Uset interface to create new Applicant Catched without login" do
    it "should response with status 403" do
      get :new
      should redirect_to new_user_session_path
    end
  end

  context "Try Get User interface to create new Applicant Catched without role" do
    login_executive_without_role
    it "should response with status 403" do
      get :new
      expect(response.status).to eq(403)
    end
  end

  context "Try Get User interface to create new Applicant Catched" do
    login_executive_with_role(:new, Applicant::Catched)
    it "should response with status 403" do
      get :new
      expect(response.status).to eq(200)
    end
  end
end