require 'rails_helper'

RSpec.describe Applicant::BasesController, :type => :controller do
  render_views

  context "Search applican into BD" do
    login_executive_with_role(:search, Applicant::Base)
    it "should response with page ok", elasticsearch: true do
      FactoryGirl.create_list(:applicant_bases, 20)
      applicants = FactoryGirl.create_list(:applicant_bases, 4)
      applicants.each do |a|
        a.update(
          user_attributes: {
            id: a.user.id,
            personal_information_attributes: {
              sex: 'masculino',
              territory_city_id: Territory::City.last.id
            }
          }
        )
      end
      get :search, genders: ['masculino'], cities: [Territory::City.last.name]
      expect(assigns(:applicants)).to_not be nil
      expect(assigns(:aggregations)).to_not be nil
      expect(response.status).to eq(200)
    end
  end

  context "SHOW Applicant" do
    login_executive_with_role(:search, Applicant::Base)
    it "should response with page show" do
      get :show, id: FactoryGirl.create(:applicant_bases).id
      expect(response.status).to eq(200)
    end
    it "should response 404" do
      get :show, id: 21312423
      expect(response.status).to eq(404)
    end
  end

  context "EDIT page on applicant" do
    login_executive_without_role
    it "should response with page edit" do
      applicant = FactoryGirl.create(:applicant_bases)
      @user.add_role :update, Applicant::Base
      get :edit, id: applicant.id
      expect(assigns(:applicant)).to eq(applicant)
      expect(response.status).to eq(200)
    end

    it "should response with page edit on applicant_external" do
      applicant = FactoryGirl.create(:applicant_external)
      @user.add_role :update, Applicant::Base
      get :edit, id: applicant.id
      expect(assigns(:applicant)).to eq(applicant)
      expect(assigns(:applicant).user.personal_information).to_not eq nil
      expect(assigns(:applicant).user.professional_information).to_not eq nil
      expect(response.status).to eq(200)
    end
  end

  context "UPDATE personal_information on applicant" do
    login_executive_without_role
    it "should reditect_to show page and update applicant" do
      applicant = FactoryGirl.create(:applicant_bases)
      new_attributes = FactoryGirl.attributes_for(:applicant_bases)
      new_attributes[:user_attributes][:id] = applicant.user.id
      new_attributes[:user_attributes][:personal_information_attributes][:id] = applicant.user.personal_information.id
      @user.add_role :update, Applicant::Base

      new_avatar = ActionDispatch::Http::UploadedFile.new(
        tempfile: File.new("#{Rails.root}/spec/tmp/avatar_2.jpg"),
        filename: "avatar_2.jpg"
      )
      new_attributes[:user_attributes][:personal_information_attributes][:avatar] = new_avatar

      put :update, { id: applicant.to_param, applicant: new_attributes }
      applicant.reload

      expect(applicant.user.email).to eq(new_attributes[:user_attributes][:email])
      expect(applicant.user.personal_information.first_name).to eq(new_attributes[:user_attributes][:personal_information_attributes][:first_name])
      expect(applicant.user.personal_information.last_name).to eq(new_attributes[:user_attributes][:personal_information_attributes][:last_name])
      expect(applicant.user.personal_information.landline).to eq(new_attributes[:user_attributes][:personal_information_attributes][:landline])
      expect(applicant.user.personal_information.cellphone).to eq(new_attributes[:user_attributes][:personal_information_attributes][:cellphone])
      expect(applicant.user.personal_information.birth_date.to_s).to eq(new_attributes[:user_attributes][:personal_information_attributes][:birth_date])
      expect(applicant.user.personal_information.territory_city_id).to eq(new_attributes[:user_attributes][:personal_information_attributes][:territory_city_id])
      expect(applicant.user.personal_information.sex).to eq(new_attributes[:user_attributes][:personal_information_attributes][:sex])
      expect(applicant.user.personal_information.address).to eq(new_attributes[:user_attributes][:personal_information_attributes][:address])
      expect(applicant.user.personal_information.company_marital_status_id).to eq(new_attributes[:user_attributes][:personal_information_attributes][:company_marital_status_id])
      expect(applicant.user.personal_information.areas_of_interest).to eq(new_attributes[:user_attributes][:personal_information_attributes][:areas_of_interest])
      expect(applicant.user.personal_information.recruitment_source_id).to eq(new_attributes[:user_attributes][:personal_information_attributes][:recruitment_source_id])
      expect(applicant.user.personal_information.availability_replacements).to eq(new_attributes[:user_attributes][:personal_information_attributes][:availability_replacements])
      expect(applicant.user.personal_information.availability_work_other_residence).to eq(new_attributes[:user_attributes][:personal_information_attributes][:availability_work_other_residence])  
      expect(applicant.user.personal_information.avatar.file.filename).to eq(new_avatar.original_filename)
      should redirect_to applicant_basis_path(applicant)
    end

    it 'should update degrees' do
      applicant = FactoryGirl.create(:applicant_bases)
      @user.add_role :update, Applicant::Base
      new_attributes = FactoryGirl.attributes_for(:applicant_bases)
      professional_information = FactoryGirl.create(:people_professional_information, user: applicant.user)
      degree = FactoryGirl.create(:people_degree, professional_information: professional_information)

      new_degrees_attributes = FactoryGirl.attributes_for_list(:people_degree, 2, professional_information_id: professional_information.id)
      new_attributes[:user_attributes] = {
        id: applicant.user.id,
        professional_information_attributes: {
          id: professional_information.id,
          degrees_attributes: new_degrees_attributes.dup
        }
      }
      degree_attributes = degree.attributes
      degree_attributes[:_destroy] = 1
      new_attributes[:user_attributes][:professional_information_attributes][:degrees_attributes].push(degree_attributes)

      put :update, id: applicant.id, applicant: new_attributes
      applicant.reload

      new_degrees_attributes.each_with_index do |degree_attr, index|
       degree = applicant.user.professional_information.degrees.find_by(
          career_id: degree_attr[:career_id],
          institution_id: degree_attr[:institution_id],
          culmination_date: degree_attr[:culmination_date],
          condition: degree_attr[:condition],
        )
        expect(degree).to_not be nil
      end
    end

    it 'should update laboral_experiences' do
      applicant = FactoryGirl.create(:applicant_bases)
      @user.add_role :update, applicant
      new_attributes = FactoryGirl.attributes_for(:applicant_bases)
      professional_information = FactoryGirl.create(:people_professional_information, user: applicant.user)

      laboral_experience = FactoryGirl.create :people_laboral_experience, professional_information: professional_information
      new_lb_experiences_attrs = FactoryGirl.create_list :people_laboral_experience, 3, professional_information: professional_information
      new_attributes[:user_attributes] = {
        id: applicant.user.id,
        professional_information_attributes: {
          id: professional_information.id,
          laboral_experiences_attributes: new_lb_experiences_attrs.dup
        }
      }
      lb_experience_attributes = laboral_experience.attributes
      lb_experience_attributes[:_destroy] = 1
      new_attributes[:user_attributes][:professional_information_attributes][:laboral_experiences_attributes].push(lb_experience_attributes)

      put :update, id: applicant.id, applicant: new_attributes
      applicant.reload

      new_lb_experiences_attrs.each_with_index do |lb_experience_attr, index|
        laboral_experience = applicant.user.professional_information.laboral_experiences.find_by(
          position: lb_experience_attr[:position],
          company: lb_experience_attr[:company],
          since_date: lb_experience_attr[:since_date],
          until_date: lb_experience_attr[:until_date],
        )
        expect(laboral_experience).to_not be nil
      end
    end
  end

  context "ENROLL Applicant into a course when current_user does not has role" do
    login_executive_without_role
    it "should response with page 403" do
      applicant = FactoryGirl.create(:applicant_bases)
      course = FactoryGirl.create(:course)
      expect{
        post :enroll, id: applicant.id, course_id: course.id
      }.to change(course.enrollments, :count).by(0)
      expect(response.status).to eq(403)
    end
  end

  context "ENROLL Applicant into a course when current_user does not has role" do
    login_executive_without_role
    it "should response ok" do
      applicant = FactoryGirl.create(:applicant_bases)
      course = FactoryGirl.create(:course)
      @user.add_role :enroll, course
      expect{
        post :enroll, id: applicant.id, course_id: course.id
      }.to change(course.enrollments, :count).by(1).and change(course.activities, :count).by(1).and change(@user.activities, :count).by(1)
      expect(response.status).to eq(200)
    end
  end

  #ENROLL WITH AJAX

  describe 'ENROLL Applicants into a course' do
    let(:applicant_count) { rand(1..5) }
    let(:applicants) { FactoryGirl.create_list(:applicant_bases, applicant_count) }
    let(:course) { FactoryGirl.create(:course) }

    context 'when user is not signed AJAX' do
      it 'should response with page 401' do
        expect{
          post :enroll, applicants_ids: applicants.map(&:id), course_id: course.id, format: :json
        }.to change(course.enrollments, :count).by(0)
        expect(response.status).to eq(401)
      end
    end

    context 'when current_user is signed' do
      login_executive_without_role

      it 'should response with page 403, current_user does not has role' do
        expect{
          post :enroll, applicants_ids: applicants.map(&:id), course_id: course.id, format: :json
        }.to change(course.enrollments, :count).by(0)
        expect(response.status).to eq(403)
      end

      it 'should response error 403, role NOT permit' do
        @user.add_role :invite
        expect{
          post :enroll, applicants_ids: applicants.map(&:id), course_id: course.id, format: :json
        }.to change(course.enrollments, :count).by(0)
        expect(response.status).to eq(403)
        expect(JSON.parse(response.body) == {'error' => 'Error 403'})
      end
    end

    context 'when current_user is signed with role' do
      login_executive_without_role
      after(:each) do
        expect{
          post :enroll, applicants_ids: applicants.map(&:id), course_id: course.id, format: :json
        }.to  change(course.enrollments, :count).by(applicant_count).and \
              change(course.activities, :count).by(applicant_count).and \
              change(@user.activities, :count).by(applicant_count)
        expect(response.status).to eq(200)
      end

      it 'should response OK, role admin' do
        @user.add_role :admin
      end

      it 'should response OK, role admin Course' do
        @user.add_role :admin, Course
      end

      it 'should response OK, role admin @course' do
        @user.add_role :admin, course
      end

      it 'should response OK, role enroll Course' do
        @user.add_role :enroll, Course
      end

      it 'should response OK, role enroll, course' do
        @user.add_role :enroll, course
      end
    end

    context 'when applicant is already postulated' do
      login_executive_with_role :admin
      let(:postulation) { FactoryGirl.create(:minisite_postulation) }
      let(:course) {postulation.minisite_publication.course}
      let(:applicant) { postulation.applicant }

      it 'should not enroll applicant' do
        expect{
          post :enroll, applicants_ids: applicant.id, course_id: course.id, format: :json
        }.to  change(course.enrollments, :count).by(0)
        expect(response.body.include?(I18n.t("applicant.base.enroll.errors.already_enrolled"))).to be true
      end
    end
  end

  context "Request when current_user is not signed in" do
    it "should redirect_to new_user_session_path when try edit applicant" do
      get :edit, id: 1
      should redirect_to new_user_session_path
    end

    it "should redirect_to new_user_session path when search applican into BD " do
      get :search
      should redirect_to new_user_session_path
    end

    it "should redirect_to new_user_session_path when try show Applicant " do
      get :show, id: FactoryGirl.create(:applicant_bases).id
      should redirect_to new_user_session_path
    end

    it "should redirect_to new_user_session_path when Enroll Applicant " do
      applicant = FactoryGirl.create(:applicant_bases)
      course = FactoryGirl.create(:course)
      expect{
        post :enroll, id: applicant.id, course_id: course.id
      }.to change(course.enrollments, :count).by(0)
      should redirect_to new_user_session_path
    end
  end

  context "Request when current_user is not signed in" do
    login_executive_without_role

    it "should response with page 403 when try edit applicant" do
      applicant = FactoryGirl.create(:applicant_bases)
      get :edit, id: applicant.to_param
      expect(response.status).to eq(403)
    end

    it "should response with page 403 when try update applicant" do
      applicant = FactoryGirl.create(:applicant_bases)
      put :update, id: applicant.to_param
      expect(response.status).to eq(403)
    end

    it "should response with page 403 when search applican into BD" do
      FactoryGirl.create(:applicant_bases)
      get :search
      expect(response.status).to eq(403)
    end

    it "should response with page 403 show Applicant" do
      get :show, id: FactoryGirl.create(:applicant_bases).id
      expect(response.status).to eq(403)
    end
  end

  describe 'POST documents' do
    login_executive_without_role
    let(:descriptor) { FactoryGirl.create(:document_descriptor) }

    before :each do
      @applicant = FactoryGirl.create(:applicant_catched)
      request.env["HTTP_REFERER"] = applicant_basis_url(@applicant)
    end

    context "when onboarding course has a document group" do
      it 'when user has admin role over Applicant::Base, should upload a document' do
        @user.add_role :update, Applicant::Base
      end

      it 'when user has admin role over the applicant, should upload a document' do
        @user.add_role :update, @applicant
      end

      after :each do
        expect {
          post :upload_documents, id: @applicant.id, applicant_documents: { descriptor.to_sym => Rack::Test::UploadedFile.new("#{Rails.root}/spec/tmp/avatar.jpg", 'image/jpeg')}
        }.to change(Document::Record, :count).by(1)

        format = :json
        descriptor =  FactoryGirl.create(:document_descriptor)
        expect {
          post :upload_documents, id: @applicant.id, applicant_documents: { descriptor.to_sym => Rack::Test::UploadedFile.new("#{Rails.root}/spec/tmp/avatar.jpg", 'image/jpeg')}, format: format
        }.to change(Document::Record, :count).by(1)
        if format == :json
          expect(response).to render_template('document/records/index')
        end
      end
    end

    context "when invalid params" do
      it "should redirect_to onboarding stage with flash errors" do
        @user.add_role :update, Applicant::Base
        expect {
          post :upload_documents, id: @applicant.id
        }.to change(Document::Record, :count).by(0)
        expect(flash[:error]).to eq I18n.t("applicant.upload_documents.upload_error.invalid_args")
        should redirect_to :back

        format = :json
        expect {
          post :upload_documents, id: @applicant.id, format: format
        }.to change(Document::Record, :count).by(0)
        if format == :json
          expect(response).to render_template('document/records/index')
        end
      end
    end

    context "when onboarding course has a document group" do
      it 'when user has no role, should not upload a document' do
        expect {
          post :upload_documents, id: @applicant.id, applicant_documents: { descriptor.to_sym => Rack::Test::UploadedFile.new("#{Rails.root}/spec/tmp/avatar.jpg", 'image/jpeg')}
        }.to change(Document::Record, :count).by(0)
        expect(response.content_type).to eq('text/html')


        expect {
          post :upload_documents, id: @applicant.id, applicant_documents: { descriptor.to_sym => Rack::Test::UploadedFile.new("#{Rails.root}/spec/tmp/avatar.jpg", 'image/jpeg')} , format: :json
        }.to change(Document::Record, :count).by(0)
        expect(response.status).to eq(403)
        expect(response.content_type).to eq('application/json')

      end
    end
  end

  describe 'Send update info email' do
    let(:applicant_count) { rand(1..5) }
    let(:applicants) { FactoryGirl.create_list(:applicant_bases, applicant_count) }

    context "user is not logged in" do
      it 'should response with page 401' do
        post :send_update_request, applicants_ids: applicants.map(&:id), format: :json
        expect(response.status).to eq(401)
      end
    end

    context "user is logged in" do
      login_executive_without_role

      it 'should response with page 403, user does not have role' do
        post :send_update_request, applicants_ids: applicants.map(&:id), format: :json
        expect(response.status).to eq(403)
      end

      it 'should response error 403, role NOT permit' do
        @user.add_role :invite
        post :send_update_request, applicants_ids: applicants.map(&:id), format: :json
        expect(response.status).to eq(403)
        expect(JSON.parse(response.body) == {'error' => 'Error 403'})
      end
    end

    context 'when user is signed with permit role' do
      login_executive_without_role
      after(:each) do
        expect{
          post :send_update_request, applicants_ids: applicants.map(&:id), format: :json
          }.to change(Applicant::UpdateInfoRequest, :count).by(applicant_count)
        expect(response.status).to eq(200)
      end

      it 'should response OK, role admin' do
        @user.add_role :admin
      end

      it 'should response OK, role send_update_request' do
        @user.add_role :send_update_request, Applicant::Base
      end
    end
  end

  describe 'POST massive_mailer' do
    before :each do
      @cant_applicant = rand(2..5)
      @applicants_ids = Array.new
      applicants = FactoryGirl.create_list(:applicant_bases, @cant_applicant)
      applicants.map {|ap| @applicants_ids << ap.id}
    end

    context 'when current_user is not signed in' do
       it 'should redirect_to new_user_session_path' do
        post :massive_mailer, applicants_ids: @applicants_ids, message: FFaker::Lorem.sentence, format: :json
        expect(response.status).to eq(401)
      end
    end

    context "user is logged in" do
      login_executive_without_role

      it 'should response with page 403, user does not have role' do
        post :massive_mailer, applicants_ids: @applicants_ids, message: FFaker::Lorem.sentence, format: :json
        expect(response.status).to eq(403)
      end

      it 'should response error 403, role NOT permit' do
        @user.add_role :invite
        post :massive_mailer, applicants_ids: @applicants_ids, message: FFaker::Lorem.sentence, format: :json
        expect(response.status).to eq(403)
        expect(JSON.parse(response.body) == {'error' => 'Error 403'})
      end
    end

    context 'when user is signed with permit role' do
      login_executive_without_role
      after(:each) do
        post :massive_mailer, applicants_ids: @applicants_ids, message: FFaker::Lorem.sentence, format: :json
        expect(response.status).to eq(200)
      end

      it 'should response OK, role admin' do
        @user.add_role :admin
      end

      it 'should response OK, role massive_mailer' do
        @user.add_role :massive_mailer, Applicant::Base
      end
    end
  end

  describe '#POST update_password' do
    before :each do
      @applicant = FactoryGirl.create(:applicant_bases)
      @password = '12345678'
    end

    context 'when current_user is not signed in' do
       it 'should redirect_to new_user_session_path' do
        post :update_password, id: @applicant.id, password: @password, password_confirmation: @password
        expect(response.status).to eq(302)
      end
    end

    context "user is logged in" do
      login_executive_without_role

      it 'should response with page 403, user does not have role' do
        post :update_password, id: @applicant.id, password: @password, password_confirmation: @password
        expect(response.status).to eq(403)
      end

      it 'should response error 403, role NOT permit' do
        @user.add_role :invite
        post :update_password, id: @applicant.id, password: @password, password_confirmation: @password
        expect(response.status).to eq(403)
      end

      context 'when user is signed with permit role' do
        context 'passwords match' do
          it 'should response OK, role admin' do
            @user.add_role :admin
          end

          after(:each) do
            post :update_password, id: @applicant.id, password: @password, password_confirmation: @password
            expect(response.status).to eq(302)
            expect(flash[:success]).not_to be_empty
            expect(flash[:success]).to eq('Contraseña actualizada correctamente')
          end
        end

        context 'passwords dont match' do
          it 'should response with error, role admin' do
            @user.add_role :admin
          end

          after(:each) do
            post :update_password, id: @applicant.id, password: @password, password_confirmation: 'abcdefgh'
            expect(response.status).to eq(302)
            expect(flash[:error]).not_to be_empty
            expect(flash[:error][:password_confirmation]).to eq(['Las contraseñas introducidas no coinciden'])
          end
        end
      end
    end
  end

  describe "generate_cv" do
    let(:applicant) { FactoryGirl.create(:applicant_bases) }
    context "when user is not Logged in" do
      it "should response status 302" do
        get :generate_cv, id: applicant.id
        expect(response.status).to eq 302
      end
    end
    context "when user is logged in" do
      login_executive_without_role
      context "with a valid role" do
        it 'should generate a pdf file if user is admin' do
          @user.add_role :admin
          get :generate_cv, id: applicant.id
          expect(response.content_type.to_s).to eq("application/pdf")
        end
        it 'should generate pdf file if user has the right privileges' do
          @user.add_role :admin, Applicant::Base
          get :generate_cv, id: applicant.id
          expect(response.content_type.to_s).to eq("application/pdf")
        end
      end
      context "with an invalid role" do
        it 'should response error 403' do
          get :generate_cv, id: applicant.id
          expect(response.status).to eq 403
        end
      end
    end
  end


  describe 'generate_massive_cvs' do
    let(:applicant_count) { rand(1..5) }
    let(:applicants) { FactoryGirl.create_list(:applicant_bases, applicant_count) }
    let(:expected_response){  expect(response.content_type.to_s).to eq('application/zip') }

    context "when user is not Logged in" do
      let(:expected_response) { expect(response.status).to eq 401 }
      it "should response status 401" do
      end
    end

    context "when user is logged in" do
      login_executive_without_role
      context "with a valid role" do
        it 'should generate a zip file if user is admin' do
          @user.add_role :admin
        end

        it 'should generate zip if user has admin role on Applicant::Base' do
          @user.add_role :admin, Applicant::Base
        end
      end

      context "with an invalid role" do
        let(:expected_response){  expect(response.status).to eq 403 }
        it 'should response error 403' do
        end
      end
    end

    after :each do
      get :generate_massive_cvs, ids: applicants.map(&:id), format: :json
      expected_response
    end
  end

end
