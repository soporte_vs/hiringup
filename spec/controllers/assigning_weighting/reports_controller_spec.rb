require 'rails_helper'

RSpec.describe AssigningWeighting::ReportsController, :type => :controller do
  render_views
  let(:work_flow) {
    work_flow = [
      {type: :module, class_name: 'AssigningWeighting::Stage', name: 'AssigningWeighting'},
      {type: :module, class_name: 'Onboarding::Stage', name: 'Onboarding'},
      {type: :module, class_name: 'Engagement::Stage', name: 'Contratación'}
    ]
  }

  describe 'POST create AssigningWeighting Report' do
    before :each do
      @assigning_weighting_report_attribtues = FactoryGirl.attributes_for(:assigning_weighting_report)
      @assigning_weighting_report_attribtues[:documents] = [
        ActionDispatch::Http::UploadedFile.new(:tempfile => File.new("#{Rails.root}/spec/tmp/document.doc"), filename: "document.doc"),
        ActionDispatch::Http::UploadedFile.new(:tempfile => File.new("#{Rails.root}/spec/tmp/document.doc"), filename: "document.doc")
      ]
      @assigning_weighting_stage = AssigningWeighting::Stage.find(@assigning_weighting_report_attribtues[:assigning_weighting_stage_id])
      @course = @assigning_weighting_stage.enrollment.course
      @course.update(work_flow: work_flow)
    end

    context 'when current_user is not loged in' do
      it 'should redirect_to new_user_session_path' do
        expect{
          post :create,
               id: @course.to_param,
               stage_id: @assigning_weighting_stage.to_param,
               assigning_weighting_report: @assigning_weighting_report_attribtues
        }.to change(AssigningWeighting::Report, :count).by(0)
        should redirect_to new_user_session_path
      end
    end

    context 'when current_user is loged in' do
      login_executive_without_role
      context 'and he not has role valid' do
        it 'should response with page 403' do
          expect{
            post :create,
                 id: @course.to_param,
                 stage_id: @assigning_weighting_stage.to_param,
                 assigning_weighting_report: @assigning_weighting_report_attribtues
          }.to change(AssigningWeighting::Report, :count).by(0)
          expect(response.status).to eq 403
        end
      end

      context 'and he has a valid role' do
        context 'with valid attributes' do
          it 'with admin role' do
            @user.add_role :admin
          end

          it 'with admin role on @course' do
            @user.add_role :admin, @course
          end

          it 'with admin role on AssigningWeighting::Stage' do
            @user.add_role :admin, AssigningWeighting::Stage
          end

          it "with #{AssigningWeighting::Stage.to_s.tableize.to_sym} on @course" do
            @user.add_role AssigningWeighting::Stage.to_s.tableize.to_sym, @course
          end

          it "with :admin on @assigning_weighting_stage" do
            @user.add_role :admin, @assigning_weighting_stage
          end

          after :each do
            expect{
              post :create,
                   id: @course.to_param,
                   stage_id: @assigning_weighting_stage.to_param,
                   assigning_weighting_report: @assigning_weighting_report_attribtues
            }.to change(AssigningWeighting::Report, :count).by(1)
            should redirect_to course_assigning_weighting_stage_path(id: @course, stage_id: @assigning_weighting_stage.id)
            expect(flash[:success]).to eq I18n.t('assigning_weighting.report.created.success')
          end
        end

        #  Como no tiene atributos no se aplica esta validación

        # context 'with invalid attributes' do
        #   before :each do
        #
        #     @assigning_weighting_report_attribtues = FactoryGirl.attributes_for(:assigning_weighting_report_invalid)
        #     @assigning_weighting_stage = AssigningWeighting::Stage.find(@assigning_weighting_report_attribtues[:assigning_weighting_stage_id])
        #
        #     @course = @assigning_weighting_stage.enrollment.course
        #     @course.update(work_flow: work_flow)
        #   end
        #
        #   it 'with admin role' do
        #     @user.add_role :admin
        #   end
        #
        #   it 'with admin role on @course' do
        #     @user.add_role :admin, @course
        #   end
        #
        #   it 'with admin role on AssigningWeighting::Stage' do
        #     @user.add_role :admin, AssigningWeighting::Stage
        #   end
        #
        #   it "with #{AssigningWeighting::Stage.to_s.tableize.to_sym} on @course" do
        #     @user.add_role AssigningWeighting::Stage.to_s.tableize.to_sym, @course
        #   end
        #
        #   it "with :admin on @assigning_weighting_stage" do
        #     @user.add_role :admin, @assigning_weighting_stage
        #   end
        #
        #   after :each do
        #     expect{
        #       post :create,
        #            id: @course.to_param,
        #            stage_id: @assigning_weighting_stage.to_param,
        #            assigning_weighting_report: @assigning_weighting_report_attribtues
        #     }.to change(AssigningWeighting::Report, :count).by(0)
        #     should render_template('assigning_weighting/stages/show')
        #     should render_template('assigning_weighting/reports/_form')
        #   end
        # end

      end
    end
  end

  it_should_behave_like 'a stage with reports', :assigning_weighting_report, :assigning_weighting_stage
end
