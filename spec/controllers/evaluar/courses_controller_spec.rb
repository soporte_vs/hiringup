require 'rails_helper'

RSpec.describe Evaluar::CoursesController, type: :controller do
  render_views

  describe '#create_in_evaluar' do
    let(:status_valid){ 'DRAFT' }
    let(:status_invalid){ '' }
    let(:evaluar_course) { FactoryGirl.create(:evaluar_course, status: status_invalid) }

    context 'connection was successful' do
      before :each do
        evaluar_course.update(status: status_valid)
        post :create_in_evaluar, evaluar_course_id: evaluar_course.id
        evaluar_course.reload
      end

      it 'shows success message, set evaluar_id' do
        expect(evaluar_course.callback_url).to_not be_nil
        expect(evaluar_course.evaluar_id).to_not be_nil
        expect(flash[:success]).to eq(I18n.t("evaluar_course.create_in_evaluar.success"))
      end
    end

    context 'when the course is not created in Evaluar' do
      before :each do
        evaluar_course = FactoryGirl.create(:evaluar_course, status: status_invalid)
        post :create_in_evaluar, evaluar_course_id: evaluar_course.id
      end

      it 'shows error message' do
         expect(evaluar_course.evaluar_id).to be nil
         expect(flash[:error]).to eq(evaluar_course.errors.messages)
      end
    end
  end


  describe 'results' do
    let!(:evaluar_course) { FactoryGirl.create(:evaluar_course) }
    let(:evaluar_results_request) {
      JSON.parse(File.read('spec/factories/evaluar/callback_url_request.json'))
    }
    let(:changes_expected){change(Evaluar::Result, :count).by(0)}
    let(:response_result){
      expect(JSON.parse(response.body)['messages']).to eq(I18n.t("evaluar_course.request.success"))
      expect(JSON.parse(response.body)['data']).to eq true
       }
    let(:applicant) { FactoryGirl.create(:applicant_bases) }

    before :each do
      evaluar_results_request['identification'] = applicant.user.personal_information.identification_document_number
    end

    context 'json is empty' do
      let(:evaluar_results_request){ {} }
      let(:response_result){
        expect(JSON.parse(response.body)['messages']).to eq(I18n.t("evaluar_course.request.error"))
        expect(JSON.parse(response.body)['data']).to eq false
      }
      it 'returns errors message' do
      end
    end

    context 'json is not empty' do
      context 'json values are valid' do
        let(:changes_expected){change(Evaluar::Result, :count).by(1)}
        it 'returns success message' do
        end
      end
      context 'json values are invalid' do
        let(:response_result){
          expect(JSON.parse(response.body)['messages']).to eq("applicant" => [I18n.t("activerecord.errors.models.evaluar/result.attributes.applicant.blank")], "identification"=>[I18n.t("activerecord.errors.models.evaluar/result.attributes.identification.blank")])
          expect(JSON.parse(response.body)['data']).to eq false
        }
        before :each do
          evaluar_results_request['identification'] = ''
        end
        it 'returns errors message' do
        end
      end
    end

    after :each do
      expect{
        post :results, evaluar_results_request.merge!(evaluar_course_id: evaluar_course.id, format: :json)
      }.to changes_expected
      response_result
    end

  end

end
