require 'rails_helper'

RSpec.describe VacanciesController, type: :controller do
  render_views

  describe "PUT #assign_course" do
    let(:course) { FactoryGirl.create(:course) }
    let(:company_positions_cenco) { FactoryGirl.create(:company_positions_cenco, company_position: course.company_position) }
    let(:vacancies) { FactoryGirl.create_list(:vacancy_without_course, 3, company_positions_cenco: company_positions_cenco) }

    context 'when current_user is not signed in' do
      it "should redirect_to new_user_session_path" do
        put :assign_course, ids: vacancies.to_param, course_id: course.to_param
        should redirect_to new_user_session_path
      end
    end

    context 'when current_user is signed in' do
      login_executive_without_role

      context 'and has not valid role' do
        it 'should response with status 403' do
          expect(vacancies.map(&:course).reduce{|r,e| r or e}).to be nil
          put :assign_course, ids: vacancies.map(&:id), course_id: course.to_param, format: :json
          expect(assigns(:vacancies).any?).to be true
          vacancies = assigns(:vacancies)
          expect(vacancies.map(&:course).reduce{|r,e| r or e}).to be nil
          expect(response).to render_template('vacancies/assign_course')
        end
      end

      context 'and has role valid' do
        let(:hiring_request_record) {
          FactoryGirl.create(:hiring_request_record, number_vacancies: 3, company_positions_cenco: company_positions_cenco)
        }

        before :each do
          hiring_request_record.accept!
        end

        it 'with role :admin should assign_course to vacancy' do
          @user.add_role :admin
          @courses_expected = [course]
        end

        it 'with role :admin on HiringRequest::Record for some vacancies' do
          @courses_expected = [course, nil]
          @user.add_role :admin, HiringRequest::Record
        end

        after :each do
          expect(vacancies.map(&:course).reduce{|r,e| r or e}).to be nil
          put :assign_course, ids: Vacancy.ids, course_id: course.id, format: :json
          expect(assigns(:vacancies).count).to eq 6
          vacancies = assigns(:vacancies)
          expect(vacancies.map(&:course).uniq).to match_array(@courses_expected)
          expect(response).to render_template('vacancies/assign_course')
        end
      end
    end
  end

end
