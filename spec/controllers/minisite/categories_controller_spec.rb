require 'rails_helper'

RSpec.describe Minisite::CategoriesController, type: :controller do
  render_views

  let(:valid_attributes) {
    FactoryGirl.attributes_for :minisite_category
  }

  let(:invalid_attributes) {
    FactoryGirl.attributes_for :minisite_category_invalid
  }

   describe "GET #index" do
    context 'when current user has role admin' do
      login_executive_with_role :admin
      it "assigns all minisite_categories as @minisite_categories" do
        category = FactoryGirl.create :minisite_category
        get :index, {}
        expect(assigns(:minisite_categories)).to eq([category])
      end
    end

    context 'when current user is not signed in' do
      it 'should redirect_to new_user_session_path' do
        get :index, {}
        should redirect_to new_user_session_path
      end
    end

    context 'when current user does not has role admin' do
      login_executive_without_role
      it 'should response with page 403' do
        get :index, {}
        expect(response.status).to eq 403
      end
    end
  end

  describe "GET #show" do
    context 'when current user has role admin' do
      login_executive_with_role :admin

      it "assigns the requested minisite_category as @minisite_category" do
        category = FactoryGirl.create :minisite_category
        get :show, {id: category.to_param}
        expect(assigns(:minisite_category)).to eq(category)
      end
    end

    context 'when current user is not signed in' do
      it 'should redirect_to new_user_session_path' do
        category = FactoryGirl.create :minisite_category
        get :show, {id: category.to_param}
        should redirect_to new_user_session_path
      end
    end

    context 'when current user does not has role admin' do
      login_executive_without_role
      it 'should response with page 403' do
        category = FactoryGirl.create :minisite_category
        get :show, {id: category.to_param}
        expect(response.status).to eq 403
      end
    end
  end

  describe "GET #new" do
    context 'when current user has role admin' do
      login_executive_with_role :admin

      it "assigns a new minisite_category as @minisite_category" do
        get :new, {}
        expect(assigns(:minisite_category)).to be_a_new(Minisite::Category)
      end
    end

    context 'when current user is not signed in' do
      it 'should redirect_to new_user_session_path' do
        get :new, {}
        should redirect_to new_user_session_path
      end
    end

    context 'when current user does not has role admin' do
      login_executive_without_role
      it 'should response with page 403' do
        get :new, {}
        expect(response.status).to eq 403
      end
    end
  end

  describe "GET #edit" do
    context 'when current user has role admin' do
      login_executive_with_role :admin

      it "assigns the requested minisite_category as @minisite_category" do
        category = FactoryGirl.create :minisite_category
        get :edit, {id: category.to_param}
        expect(assigns(:minisite_category)).to eq(category)
      end
    end

    context 'when current user is not signed in' do
      it 'should redirect_to new_user_session_path' do
        category = FactoryGirl.create :minisite_category
        get :edit, {id: category.to_param}
        should redirect_to new_user_session_path
      end
    end

    context 'when current user does not has role admin' do
      login_executive_without_role
      it 'should response with page 403' do
        get :new, {}
        expect(response.status).to eq 403
      end
    end
  end

  describe "POST #create" do
    context 'when current user has role admin' do
      login_executive_with_role :admin

      context "with valid params and current user has role admin" do
        it "creates a new Minisite::Category" do
          expect {
            post :create, {minisite_category: valid_attributes}
          }.to change(Minisite::Category, :count).by(1)
        end

        it "assigns a newly created minisite_category as @minisite_category" do
          post :create, {minisite_category: valid_attributes}
          expect(assigns(:minisite_category)).to be_a(Minisite::Category)
          expect(assigns(:minisite_category)).to be_persisted
        end

        it "redirects to the created minisite_category" do
          post :create, {minisite_category: valid_attributes}
          expect(response).to redirect_to(Minisite::Category.last)
        end
      end

      context "with invalid params" do
        it "assigns a newly created but unsaved minisite_category as @minisite_category" do
          post :create, {minisite_category: invalid_attributes}
          expect(assigns(:minisite_category)).to be_a_new(Minisite::Category)
        end

        it "re-renders the 'new' template" do
          post :create, {minisite_category: invalid_attributes}
          expect(response).to render_template("new")
        end
      end
    end

     context 'when current user is not signed in' do
      it 'should redirect_to new_user_session_path' do
        post :create, {minisite_category: valid_attributes}
        should redirect_to new_user_session_path
      end
    end

    context 'when current user does not has role admin' do
      login_executive_without_role
      it 'should response with page 403' do
        post :create, {minisite_category: valid_attributes}
        expect(response.status).to eq 403
      end
    end
  end

  describe "PUT #update" do
    let(:new_attributes) {
      FactoryGirl.attributes_for :minisite_category
    }

    context 'when current user has role admin' do
      login_executive_with_role :admin

      context "with valid params" do

        it "updates the requested minisite_category" do
          category = FactoryGirl.create(:minisite_category)
          put :update, {id: category.to_param, minisite_category: new_attributes}
          category.reload
          expect(category.name).to eq new_attributes[:name]
          expect(category.description).to eq new_attributes[:description]
        end

        it "assigns the requested minisite_category as @minisite_category" do
          category = FactoryGirl.create(:minisite_category)
          put :update, {id: category.to_param, minisite_category: valid_attributes}
          expect(assigns(:minisite_category)).to eq(category)
        end

        it "redirects to the minisite_category" do
          category = FactoryGirl.create(:minisite_category)
          put :update, {id: category.to_param, minisite_category: valid_attributes}
          expect(response).to redirect_to(category)
        end
      end

      context "with invalid params" do
        it "assigns the minisite_category as @minisite_category" do
          category = FactoryGirl.create(:minisite_category)
          put :update, {id: category.to_param, minisite_category: invalid_attributes}
          expect(assigns(:minisite_category)).to eq(category)
        end

        it "re-renders the 'edit' template" do
          category = FactoryGirl.create(:minisite_category)
          put :update, {id: category.to_param, minisite_category: invalid_attributes}
          expect(response).to render_template("edit")
        end
      end
    end

    context 'when current user is not signed in' do
      it 'should redirect_to new_user_session_path' do
        category = FactoryGirl.create(:minisite_category)
        put :update, {id: category.to_param, minisite_category: new_attributes}
        should redirect_to new_user_session_path
      end
    end

    context 'when current user does not has role admin' do
      login_executive_without_role
      it 'should response with page 403' do
        category = FactoryGirl.create(:minisite_category)
        put :update, {id: category.to_param, minisite_category: new_attributes}
        expect(response.status).to eq 403
      end
    end
  end

  describe "DELETE #destroy" do
    context 'when current user has role admin' do
      login_executive_with_role :admin

      it "destroys the requested minisite_category" do
        category = FactoryGirl.create :minisite_category
        expect {
          delete :destroy, {:id => category.to_param}
        }.to change(Minisite::Category, :count).by(-1)
      end

      it "redirects to the minisite_categorys list" do
        category = FactoryGirl.create :minisite_category
        delete :destroy, {:id => category.to_param}
        expect(response).to redirect_to(minisite_categories_url)
      end
    end

    context 'when current user is not signed in' do
      it 'should redirect_to new_user_session_path' do
        category = FactoryGirl.create :minisite_category
        delete :destroy, {:id => category.to_param}
        should redirect_to new_user_session_path
      end
    end

    context 'when current user does not has role admin' do
      login_executive_without_role
      it 'should response with page 403' do
        category = FactoryGirl.create :minisite_category
        delete :destroy, {:id => category.to_param}
        expect(response.status).to eq 403
      end
    end
  end


end
