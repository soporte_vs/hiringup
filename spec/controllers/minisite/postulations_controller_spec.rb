require 'rails_helper'

RSpec.describe Minisite::PostulationsController, type: :controller do
  describe 'POST postulate to job' do
    render_views
    context 'when user is not signed in' do
      it 'should redirect_to new_user_session_path' do
        publication = FactoryGirl.create :minisite_publication
        expect{
          post :create, id: publication.to_param
        }.to change(Applicant::Base, :count).by(0).and change(Minisite::Postulation, :count).by(0)
        should redirect_to new_user_session_path
      end
    end

    context 'when user signed in' do
      login_executive_without_role

      context 'and he does not postulated yet' do
        before :each do
          @publication = FactoryGirl.create :minisite_publication
          @questions = FactoryGirl.create_list(
            :minisite_question, 3, minisite_publication: @publication
          )
          @answers_attributes = []
          @questions.each do |question|
            @answers_attributes.push(FactoryGirl.attributes_for(
              :minisite_answer,
              minisite_question_id: question.to_param
            ))
          end
        end

        context 'and publication is opened' do
          context 'and applicant postulation data is completed' do
            before :each do
              # Debido a que constantemente cambian los campos requeridos para
              # postular para simular el contexto asumo que el postualtion_data_completed
              # return true en vez de construir un personal information para @user
              # y rellenar los
              allow_any_instance_of(Applicant::Base).to receive(:postulation_data_completed?).and_return(true)
            end

            it 'should create a new postulation and redirect to job view' do
              expect{
                post :create, id: @publication.to_param, minisite_postulation: { answers_attributes: @answers_attributes }
              }.to change(Minisite::Applicant, :count).by(1).and  change(@publication.postulations, :count).by(1)
            end

            context 'and applicant is internal' do
              let(:internal_course){
                FactoryGirl.create(:course, type_enrollment: Enrollment::Internal)
              }
              before :each do
                @publication.update(course: internal_course)
              end

              it 'should create a postulation and send_internal_postulation_template' do
                applicant = FactoryGirl.create(:applicant_bases, user: @user)
                FactoryGirl.create(:company_white_rut, rut: applicant.user.personal_information.get_rut)
                expect{
                  post :create, id: @publication.to_param, minisite_postulation: {
                    answers_attributes: @answers_attributes
                  }
                }.to change(@publication.postulations, :count).by(1).and \
                     change(Delayed::Job, :count).by(1)

                # Integra pidió que el correo con la plantilla de postulación interna se envie al momento de crear la postulacion
                postulation_internal_email = YAML.load(Delayed::Job.last.handler)
                expect(postulation_internal_email.object).to be Courses::InternalMailer
                expect(postulation_internal_email.method_name).to eq :send_internal_postulation_template
                expect(postulation_internal_email.args.include?(applicant)).to be true
                expect(postulation_internal_email.args.include?(@publication.course)).to be true
              end
            end

            context 'and he postulated already' do
              login_applicant
              it 'should redirect_to job_path with errors' do
                publication = FactoryGirl.create :minisite_publication
                publication.course.enroll(@applicant)
                expect{
                  post :create, id: publication.to_param, minisite_postulation: { answers_attributes: []}
                }.to change(Minisite::Applicant, :count).by(0).and change(publication.postulations, :count).by(0)
                publication = assigns(:minisite_publication)
                expect(assigns(:minisite_postulation).errors.count).to eq 1
              end
            end

            context 'when publication does not has questions' do
              it 'should create a postulation without answers' do
                publication = FactoryGirl.create(:minisite_publication)
                expect{
                  post :create, id: publication.to_param
                }.to change(Minisite::Applicant, :count).by(1).and  change(publication.postulations, :count).by(1)
              end
            end
          end

          context 'and applicant does not have all data completed' do
            it 'should not create a postulation' do
              applicant = FactoryGirl.create(:applicant_bases, user: FactoryGirl.create(:user_without_personal_information))
              expect{
                post :create, id: @publication.to_param, minisite_postulation: { applicant: applicant }
              }.to change(@publication.postulations, :count).by(0)
            end
          end
        end

        context 'and publication is not opened' do
          it 'shoult not crear a new postulation and should redirect_to back' do
            @publication.close!
            request.env["HTTP_REFERER"] = root_url
            expect{
              post :create, id: @publication.to_param, minisite_postulation: { answers_attributes: @answers_attributes }
            }.to change(Minisite::Applicant, :count).by(0).and  change(@publication.postulations, :count).by(0)
            should redirect_to :back
          end
        end
      end
    end
  end
end
