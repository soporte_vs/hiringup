require 'rails_helper'

RSpec.describe HiringRequest::RecordsController, type: :controller do

  let(:valid_attributes) {
    FactoryGirl.attributes_for(:hiring_request_record)
  }

  let(:invalid_attributes) {
    FactoryGirl.attributes_for(:hiring_request_record_invalid)
  }

  describe 'PUT switch state' do
    before :each do
      @hiring_request_record = FactoryGirl.create(:hiring_request_record)
    end

    context 'when current user is not signed in' do
      it 'should redirect_to new_user_session_path' do
        put :switch_state, id: @hiring_request_record.to_param, switch: :accept
        should redirect_to new_user_session_path
      end
    end

    context 'when current user is signed in' do
      login_executive_without_role

      context 'and has not role valid' do
        it 'should response with status 403' do
          put :switch_state, id: @hiring_request_record.to_param, switch: :accept
          expect(response.status).to eq 403
        end
      end

      context 'and has role valid' do
        context 'and transition state is valid' do
          it 'with role admin' do
            @user.add_role :admin
          end

          it "with role admin on HiringRequest::Record" do
            @user.add_role :admin, HiringRequest::Record
          end

          after :each do
            put :switch_state, id: @hiring_request_record.to_param, switch: :accept
            @hiring_request_record.reload
            expect(@hiring_request_record.accepted?).to be true
            should redirect_to @hiring_request_record
          end
        end

        context 'and transition state is invalid' do
          before :each do
            @hiring_request_record.reject!
            @hiring_request_record.reload
          end

          it 'with role admin' do
            @user.add_role :admin
          end

          it "with role admin on HiringRequest::Record" do
            @user.add_role :admin, HiringRequest::Record
          end

          after :each do
            put :switch_state, id: @hiring_request_record.to_param, switch: :accept
            @hiring_request_record.reload
            expect(@hiring_request_record.accepted?).to be false
          end
        end
      end
    end
  end

  describe "GET #index" do
    context 'when current user is not logged in' do
      it 'should redirect_to new_user_session_path' do
        get :index, {}
        should redirect_to new_user_session_path
      end
    end

    context 'when current user is logged in' do
      login_executive_without_role
      context 'and has does not has role valid' do
        it 'should response with page 403' do
          get :index, {}
          expect(response.status).to eq 403
        end
      end

      context 'and has does has role valid' do
        let(:record) {
          FactoryGirl.create(:hiring_request_record)
        }

        it "assigns all hiring_request_records as @hiring_request_records" do
          @user.add_role :admin, HiringRequest::Record
        end

        after :each do
          get :index, {}
          expect(assigns(:hiring_request_records)).to eq([record])
        end
      end
    end
  end

  describe "GET #show" do
    let(:record){
      FactoryGirl.create(:hiring_request_record)
    }

    context 'when current is not signed in' do
      it 'should redirect_to new_user_session_path' do
        get :show, {:id => record.to_param}
        should redirect_to new_user_session_path
      end
    end

    context 'when current is signed in' do
      login_executive_without_role

      context 'and does not has role valid' do
        it 'should response with page 403' do
        end

        after :each do
          get :show, {:id => record.to_param}
          expect(response.status).to eq 403
        end
      end

      context 'and does has role valid' do
        it "assigns the requested hiring_request_record as @hiring_request_record" do
          @user.add_role :admin, HiringRequest::Record
        end

        after :each do
          get :show, {:id => record.to_param}
          expect(assigns(:hiring_request_record)).to eq(record)
        end
      end
    end
  end

  describe "GET #new" do
    context 'when current is no logged in' do
      it 'should redirect_to new_user_session_path' do
        get :new, {}
        should redirect_to new_user_session_path
      end
    end

    context 'when current is logged in' do
      login_executive_without_role

      context 'and does not has role valid' do
        it 'should response with page 403' do
        end

        after :each do
          get :new, {}
          expect(response.status).to eq 403
        end
      end

      context 'and has role valid' do
        it "assigns a new hiring_request_record as @hiring_request_record" do
          @user.add_role :admin
        end

        after :each do
          get :new, {}
          expect(assigns(:hiring_request_record)).to be_a_new(HiringRequest::Record)
        end
      end
    end
  end

  describe "GET #edit" do
    before :each do
      @hiring_request_record = FactoryGirl.create(:hiring_request_record)
    end

    context 'when current user is not signed in' do
      it 'should redirect_to new_user_session_path' do
        get :edit, {id: @hiring_request_record.to_param}
        should redirect_to new_user_session_path
      end
    end

    context 'when current user is not signed in' do
      login_executive_without_role

      context 'and has a role valid' do
        context 'and hiring_request_record is state active' do
          it "assigns the requested hiring_request_record as @hiring_request_record" do
            @user.add_role :admin, HiringRequest::Record
          end

          after :each do
            get :edit, {:id => @hiring_request_record.to_param}
            expect(assigns(:hiring_request_record)).to eq(@hiring_request_record)
          end
        end

        context 'and hiring_request_record is not state active' do
          before :each do
            @hiring_request_record.accept!
            @hiring_request_record.reload
          end

          it "assigns the requested hiring_request_record as @hiring_request_record" do
            @user.add_role :admin, HiringRequest::Record
          end

          after :each do
            get :edit, {:id => @hiring_request_record.to_param}
            expect(assigns(:hiring_request_record)).to eq(@hiring_request_record)
            should redirect_to hiring_request_record_path(@hiring_request_record)

            @hiring_request_record.update(aasm_state: :rejected)
            @hiring_request_record.reload


            get :edit, {:id => @hiring_request_record.to_param}
            expect(assigns(:hiring_request_record)).to eq(@hiring_request_record)
            should redirect_to hiring_request_record_path(@hiring_request_record)
          end
        end
      end

      context 'and does not has a role valid' do
        it 'should response with page 403' do
        end

        after :each do
          get :edit, {id: @hiring_request_record.to_param}
          expect(response.status).to eq 403
        end
      end
    end
  end

  describe "POST #create" do
    context 'when current user is not signed in' do
      it 'should redirect_to new_user_session_path' do
        post :create, {:hiring_request_record => valid_attributes}
        should redirect_to new_user_session_path
      end
    end

    context 'when current user is no logged in' do
      login_executive_without_role
      context 'and has a role valid' do
        context "with valid params" do
          context "creates a new HiringRequest::Record" do
            it "user has role admin on HiringRequest::record" do
              @user.add_role :admin, HiringRequest::Record
            end

            it "user has role admin" do
              @user.add_role :admin
            end

            after :each do
              expect {
                post :create, {:hiring_request_record => valid_attributes}
              }.to change(HiringRequest::Record, :count).by(1)
            end
          end

          context "assigns a newly created hiring_request_record as @hiring_request_record" do
            it 'user has role admin on HiringRequest::Record' do
              @user.add_role :admin, HiringRequest::Record
            end

            it 'user has role admin' do
              @user.add_role :admin
            end

            after :each do
              post :create, {:hiring_request_record => valid_attributes}
              expect(assigns(:hiring_request_record)).to be_a(HiringRequest::Record)
              expect(assigns(:hiring_request_record)).to be_persisted
              expect(assigns(:hiring_request_record).created_by).to eq @user
            end
          end

          context "redirects to the created hiring_request_record" do
            it 'user has role admin on HiringRequest::Record' do
              @user.add_role :admin
            end

            it 'user has role admin Record' do
              @user.add_role :admin
            end

            after :each do
              post :create, {:hiring_request_record => valid_attributes}
              expect(response).to redirect_to(HiringRequest::Record.last)
            end
          end
        end

        context "with invalid params" do
          context "assigns a newly created but unsaved hiring_request_record as @hiring_request_record" do
            it 'user has role admin on HiringRequest::Record' do
              @user.add_role :admin, HiringRequest::Record
            end

            it 'user has role admin' do
              @user.add_role :admin
            end

            after :each do
              post :create, {:hiring_request_record => invalid_attributes}
              expect(assigns(:hiring_request_record)).to be_a_new(HiringRequest::Record)
            end
          end

          context "re-renders the 'new' template" do
            it 'user has role admin on HiringRequest::Record' do
              @user.add_role :admin, HiringRequest::Record
            end

            it 'user has role admin' do
              @user.add_role :admin
            end

            after :each do
              post :create, {:hiring_request_record => invalid_attributes}
              expect(response).to render_template("new")
            end
          end
        end

      end

      context 'and does not has a role valid' do
        it 'should response with page 403' do
          post :create, hiring_request_record: valid_attributes
          expect(response.status).to eq 403
        end
      end
    end

  end

  describe "PUT #update" do
    let(:new_attributes) {
      FactoryGirl.attributes_for(:hiring_request_record_all_new)
    }
    before :each do
      @hiring_request_record = FactoryGirl.create(:hiring_request_record)
    end

    context 'when current is not logged in' do
      it 'should redirect_to new_user_session_path' do
        put :update, {:id => @hiring_request_record.to_param, :hiring_request_record => new_attributes}
        should redirect_to new_user_session_path
      end
    end

    context 'when current is logged in' do
      login_executive_without_role

      context 'and has role valid' do
        context "with valid params" do
          context "updates the requested hiring_request_record" do
            context 'when hiring_request_record is active' do
              it 'user has role admin on HiringRequest::Record' do
                @user.add_role :admin, HiringRequest::Record
              end
              it 'user has role admin' do
                @user.add_role :admin
              end

              after :each do
                put :update, {:id => @hiring_request_record.to_param, :hiring_request_record => new_attributes}
                @hiring_request_record.reload
                expect(@hiring_request_record.company_positions_cenco_id).to eq new_attributes[:company_positions_cenco_id]
                expect(@hiring_request_record.applicant.user.email).to eq new_attributes[:applicant_attributes][:user_attributes][:email]
                expect(@hiring_request_record.revenue_expected).to eq new_attributes[:revenue_expected]
                expect(@hiring_request_record.workplace).to eq new_attributes[:workplace]
                expect(@hiring_request_record.company_vacancy_request_reason_id).to eq new_attributes[:company_vacancy_request_reason_id]
                expect(@hiring_request_record.company_contract_type_id).to eq new_attributes[:company_contract_type_id]
                expect(@hiring_request_record.company_estate_id).to eq new_attributes[:company_estate_id]
                expect(@hiring_request_record.company_timetable_id).to eq new_attributes[:company_timetable_id]
                expect(@hiring_request_record.observations).to eq new_attributes[:observations]
                expect(@hiring_request_record.number_vacancies).to eq new_attributes[:number_vacancies]
              end
            end

            context 'when hiring_request_record is not active' do
              before :each do
                @hiring_request_record.accept!
                @hiring_request_record.reload
              end

              it 'user has role admin on HiringRequest::Record' do
                @user.add_role :admin, HiringRequest::Record
              end

              it 'user has role admin' do
                @user.add_role :admin
              end

              after :each do
                put :update, {:id => @hiring_request_record.to_param, :hiring_request_record => new_attributes}
                @hiring_request_record.reload
                should redirect_to @hiring_request_record
                expect(@hiring_request_record.company_positions_cenco_id).to_not eq new_attributes[:company_positions_cenco_id]
                expect(@hiring_request_record.applicant.user.email).to_not eq new_attributes[:applicant_attributes][:user_attributes][:email]
                expect(@hiring_request_record.workplace).to_not eq new_attributes[:workplace]
                expect(@hiring_request_record.company_vacancy_request_reason_id).to_not eq new_attributes[:company_vacancy_request_reason_id]
                expect(@hiring_request_record.company_contract_type_id).to_not eq new_attributes[:company_contract_type_id]
                expect(@hiring_request_record.company_estate_id).to_not eq new_attributes[:company_estate_id]
                expect(@hiring_request_record.company_timetable_id).to_not eq new_attributes[:company_timetable_id]
                expect(@hiring_request_record.observations).to_not eq new_attributes[:observations]
              end
            end
          end

          context "assigns the requested hiring_request_record as @hiring_request_record" do

            it 'user has role admin on HiringRequest::Record' do
              @user.add_role :admin, HiringRequest::Record
            end
            it 'user has role admin' do
              @user.add_role :admin
            end

            after :each do
              put :update, {:id => @hiring_request_record.to_param, :hiring_request_record => valid_attributes}
              expect(assigns(:hiring_request_record)).to eq(@hiring_request_record)
            end
          end

          context "redirects to the hiring_request_record" do
            it 'user has role admin on HiringRequest::Record' do
              @user.add_role :admin, HiringRequest::Record
            end
            it 'user has role admin' do
              @user.add_role :admin
            end

            after :each do
              put :update, {:id => @hiring_request_record.to_param, :hiring_request_record => valid_attributes}
              expect(response).to redirect_to(@hiring_request_record)
            end
          end
        end

        context "with invalid params" do
          context "assigns the hiring_request_record as @hiring_request_record" do
            it 'user has role admin on HiringRequest' do
              @user.add_role :admin, HiringRequest::Record
            end

            it 'user has role admin' do
              @user.add_role :admin
            end

            after :each do
              put :update, {:id => @hiring_request_record.to_param, :hiring_request_record => invalid_attributes}
              expect(assigns(:hiring_request_record)).to eq(@hiring_request_record)
            end
          end

          context "re-renders the 'edit' template" do
            it 'user has role admin on HiringRequest' do
              @user.add_role :admin, HiringRequest::Record
            end

            it 'user has role admin' do
              @user.add_role :admin
            end

            after :each do
              put :update, {:id => @hiring_request_record.to_param, :hiring_request_record => invalid_attributes}
              expect(response).to render_template("edit")
            end
          end
        end
      end

      context 'and does not has role valid' do
        it 'should response with page 403' do
          put :update, {:id => @hiring_request_record.to_param, :hiring_request_record => new_attributes}
          expect(response.status).to eq 403
        end
      end
    end

  end

  # describe "DELETE #destroy" do
  #   context 'when current user is no logged in' do
  #     it 'should redirect_to new' do
  #       record = FactoryGirl.create(:hiring_request_record)
  #       delete :destroy, {:id => record.to_param}
  #       should redirect_to new_user_session_path
  #     end
  #   end

  #   context 'when current user is logged in' do
  #     login_executive_without_role

  #     context 'and has role valid' do
  #       context "destroys the requested hiring_request_record" do
  #         it 'user has role admin on HiringRequest::Record' do
  #           @user.add_role :admin, HiringRequest::Record
  #         end

  #         it 'user has role admin' do
  #           @user.add_role :admin
  #         end

  #         after :each do
  #           record = FactoryGirl.create(:hiring_request_record)
  #           expect {
  #             delete :destroy, {:id => record.to_param}
  #           }.to change(HiringRequest::Record, :count).by(-1)
  #         end
  #       end

  #       context "redirects to the hiring_request_records list" do
  #         it 'user has role admin on HiringRequest::Record' do
  #           @user.add_role :admin, HiringRequest::Record
  #         end

  #         it 'user has role admin' do
  #           @user.add_role :admin
  #         end

  #         after :each do
  #           record = FactoryGirl.create(:hiring_request_record)
  #           delete :destroy, {:id => record.to_param}
  #           expect(response).to redirect_to(hiring_request_records_url)
  #         end
  #       end
  #     end

  #     context 'and does not has role valid' do
  #       it 'should response with page 403' do
  #         record = FactoryGirl.create(:hiring_request_record)
  #         delete :destroy, {:id => record.to_param}
  #         expect(response.status).to eq 403
  #       end
  #     end
  #   end
  # end

end
