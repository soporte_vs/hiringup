require 'rails_helper'

RSpec.describe Document::RecordsController, :type => :controller do
  render_views

  describe "DELETE #destroy" do
    context "when user is not logged in" do
      document_record = FactoryGirl.create(:document_record)
      it "should response error 401 " do
        delete :destroy, {id: document_record.to_param, format: :json}
        expect(response.status).to eq 401
      end
    end

    context "when user is logged in" do
      login_executive_without_role
      let!(:document_record) { FactoryGirl.create(:document_record) }

      context 'when user has invalid role' do
        it 'should response with 403' do
          expect {
              delete :destroy, {id: document_record.to_param, format: :json}
            }.to change(Document::Record, :count).by(0)
          expect(response.status).to eq 403
          end
      end

      context 'when user has valid role' do
        it 'has admin role' do
          @user.add_role :admin
        end

        it 'has created role' do
          document_record.update(applicant: FactoryGirl.create(:applicant_bases, user: @user))
        end

        after :each do
          expect {
            delete :destroy, {id: document_record.to_param, format: :json}
          }.to change(Document::Record, :count).by(-1)
        end
      end
    end
  end
end
