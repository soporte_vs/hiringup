require 'rails_helper'

RSpec.describe Document::DescriptorsController, :type => :controller do
  render_views

  let(:valid_attributes) {
    FactoryGirl.attributes_for(:document_descriptor)
  }

  let(:new_attributes) {
    FactoryGirl.attributes_for(:document_descriptor)
  }

  let(:invalid_attributes) {
    FactoryGirl.attributes_for(:document_descriptor_invalid)
  }

  describe "GET #index" do
    context "when user is not logged in" do
      it "should response error 302 redirect sign_in" do
        get :index
        expect(response.status).to eq 302
        should redirect_to new_user_session_path
      end
    end

    context "when user is logged in" do
      login_executive_without_role
      it "should response status 200" do
        document_descriptor = FactoryGirl.create(:document_descriptor)
        @user.add_role :admin

        get :index

        document_descriptors = assigns(:document_descriptors)
        expect(document_descriptors).to eq [document_descriptor]
        expect(response.status).to eq 200
      end
    end
  end

  describe "GET #show" do
    let(:document_descriptor) {FactoryGirl.create(:document_descriptor)}

    context "when user is not logged in" do
      it "should response error 302 and redirect sign_in" do
        get :show, id: document_descriptor.to_param

        expect(response.status).to eq 302
        should redirect_to new_user_session_path
      end
    end

    context "when user is logged in" do
      login_executive_without_role
      it "should response status 200" do
        @user.add_role :admin
        get :show, id: document_descriptor.to_param

        expect(assigns(:document_descriptor)).to eq document_descriptor
        expect(response.status).to eq 200
      end
    end
  end

  describe "GET #new" do
    context "when user is not logged in" do
      it "should response error 302 and redirect sign_in" do
        get :new

        expect(response.status).to eq 302
        should redirect_to new_user_session_path
      end
    end

    context "when user is logged in" do
      login_executive_without_role
      it "should response status 200" do
        @user.add_role :admin

        get :new

        expect(assigns(:document_descriptor)).to be_a_new(Document::Descriptor)
        expect(response.status).to eq 200
      end
    end
  end

  describe "GET #edit" do
    let(:document_descriptor) {FactoryGirl.create(:document_descriptor)}

    context "when user is not logged in" do
      it "should response error 302 and redirect sign_in" do
        get :edit, {id: document_descriptor.to_param}
        expect(response.status).to eq 302
        should redirect_to new_user_session_path
      end
    end

    context "when user is logged in " do
      login_executive_without_role
      it "assigns the requested document_descriptor as @document_descriptor" do
        @user.add_role :admin
        document_descriptor = FactoryGirl.create(:document_descriptor)
        get :edit, {id: document_descriptor.to_param}
        expect(assigns(:document_descriptor)).to eq(document_descriptor)
      end
    end
  end

  describe "POST #create" do
    context "when user is not logged in" do
      it "should response error 302 and redirect sign_in" do
        post :create, {document_descriptor: valid_attributes}
        expect(response.status).to eq 302
        should redirect_to new_user_session_path
      end
    end

    context "when user is logged in" do
      login_executive_without_role
      context "with valid attributes" do
        it "create a new document descriptor" do
          @user.add_role :admin
          expect{
            post :create, {document_descriptor: valid_attributes}
          }.to change(Document::Descriptor, :count).by(1)

          expect(assigns(:document_descriptor)).to be_a(Document::Descriptor)
          expect(assigns(:document_descriptor)).to be_persisted

          should redirect_to document_descriptor_path(assigns(:document_descriptor))
        end
      end

      context "with invalid attributes" do
        it "should response error" do
          @user.add_role :admin
          expect{
            post :create, {document_descriptor: invalid_attributes}
          }.to change(Document::Descriptor, :count).by(0)

          expect(assigns(:document_descriptor)).to be_a_new(Document::Descriptor)
          expect(response).to render_template("new")
          expect(assigns(:document_descriptor).errors.any?).to be true
        end
      end
    end
  end

  describe "PUT #update" do
    let(:document_descriptor) {FactoryGirl.create(:document_descriptor)}
    context "when user is not logged in" do
      it "should response error 302 and redirect sign_in" do
        put :update, {:id => document_descriptor.to_param, :document_descriptor => new_attributes}
        expect(response.status).to eq 302
        should redirect_to new_user_session_path
      end
    end

    context "when user is logged in" do
      login_executive_without_role
      context "with valid params" do
        it "updates the requested document_descriptor" do
          @user.add_role :admin
          name_document_descriptor = FFaker::Lorem.word
          document_descriptor = FactoryGirl.create(:document_descriptor, name: name_document_descriptor)
          put :update, {:id => document_descriptor.to_param, :document_descriptor => new_attributes}
          document_descriptor.reload
          expect(document_descriptor.name).to eq name_document_descriptor
          expect(document_descriptor.details).to eq new_attributes[:details]
          expect(document_descriptor.internal).to eq new_attributes[:internal]
          expect(document_descriptor.details).to eq new_attributes[:details]
          expect(document_descriptor.allowed_extensions).to eq new_attributes[:allowed_extensions]
        end

        it "assigns the requested document_descriptor as @document_descriptor" do
          @user.add_role :admin
          document_descriptor = FactoryGirl.create(:document_descriptor)
          put :update, {:id => document_descriptor.to_param, :document_descriptor => valid_attributes}
          expect(assigns(:document_descriptor)).to eq(document_descriptor)
        end

        it "redirects to the document_descriptor" do
          @user.add_role :admin
          document_descriptor = FactoryGirl.create(:document_descriptor)
          put :update, {:id => document_descriptor.to_param, :document_descriptor => valid_attributes}
          expect(response).to redirect_to(document_descriptor)
        end
      end

      context "with invalid params" do
        it "assigns the document_descriptor as @document_descriptor" do
          @user.add_role :admin
          document_descriptor = FactoryGirl.create(:document_descriptor)
          put :update, {:id => document_descriptor.to_param, :document_descriptor => invalid_attributes}
          expect(assigns(:document_descriptor)).to eq(document_descriptor)
        end

        it "re-renders the 'edit' template" do
          @user.add_role :admin
          document_descriptor = FactoryGirl.create(:document_descriptor)
          put :update, {:id => document_descriptor.to_param, :document_descriptor => invalid_attributes}
          expect(response).to render_template("edit")
        end
      end
    end
  end

  describe "DELETE #destroy" do
    context "when user is not logged in" do
      document_descriptor = FactoryGirl.create(:document_descriptor)
      it "should response error 302 redirect sign_in" do
        delete :destroy, {:id => document_descriptor.to_param}
        expect(response.status).to eq 302
        should redirect_to new_user_session_path
      end
    end

    context "when user is logged in" do
      login_executive_without_role
      it "destroys the requested document_descriptor" do
        document_descriptor = FactoryGirl.create(:document_descriptor)
        @user.add_role :admin
        expect {
          delete :destroy, {:id => document_descriptor.to_param}
        }.to change(Document::Descriptor, :count).by(-1)
        expect(response).to redirect_to(document_descriptors_url)
      end
    end
  end
end
