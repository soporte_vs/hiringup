require 'rails_helper'

RSpec.describe ProfilesController, :type => :controller do
  render_views

  context "GET MyProfile" do
    context 'when current_user is not signed in' do
      it 'should redirect_to new_user_session_path' do
        get :edit
        should redirect_to new_user_session_path
      end
    end

    context 'when current_user is signed in' do
      login_applicant
      it 'should render profiles/edit' do
        @applicant.courses << FactoryGirl.create_list(:course, 3)
        get :edit
        should render_template('profiles/edit')
        should render_template('profiles/_form')
      end
    end
  end

  context "UPDATE MyProfile" do
    login_executive_without_role
    context "with valid params" do
      it "should be redirect_to profile and update information" do
        new_attributes = FactoryGirl.attributes_for(:user)
        new_avatar = ActionDispatch::Http::UploadedFile.new(
          tempfile: File.new("#{Rails.root}/spec/tmp/avatar_2.jpg"),
          filename: "avatar_2.jpg"
        )
        new_attributes[:personal_information_attributes][:avatar] = new_avatar

        put :update, user: new_attributes
        @user.reload

        expect(@user.email).to eq(new_attributes[:email])
        expect(@user.personal_information.first_name).to eq(new_attributes[:personal_information_attributes][:first_name])
        expect(@user.personal_information.last_name).to eq(new_attributes[:personal_information_attributes][:last_name])
        expect(@user.personal_information.birth_date.to_s).to eq(new_attributes[:personal_information_attributes][:birth_date])
        expect(@user.personal_information.sex).to eq(new_attributes[:personal_information_attributes][:sex])
        expect(@user.personal_information.territory_city_id).to eq(new_attributes[:personal_information_attributes][:territory_city_id])
        expect(@user.personal_information.cellphone).to eq(new_attributes[:personal_information_attributes][:cellphone])
        expect(@user.personal_information.landline).to eq(new_attributes[:personal_information_attributes][:landline])
        expect(@user.personal_information.address).to eq(new_attributes[:personal_information_attributes][:address])
        expect(@user.personal_information.company_marital_status_id).to eq(new_attributes[:personal_information_attributes][:company_marital_status_id])
        expect(@user.personal_information.recruitment_source_id).to   eq(new_attributes[:personal_information_attributes][:recruitment_source_id])
        expect(@user.personal_information.areas_of_interest).to   eq(new_attributes[:personal_information_attributes][:areas_of_interest])
        expect(@user.personal_information.availability_replacements).to   eq(new_attributes[:personal_information_attributes][:availability_replacements])
        expect(@user.personal_information.availability_work_other_residence).to   eq(new_attributes[:personal_information_attributes][:availability_work_other_residence])
        expect(@user.personal_information.avatar.file.filename).to eq(new_avatar.original_filename)
      end

      it 'should update degrees' do
        new_attributes = FactoryGirl.attributes_for(:user)
        professional_information = FactoryGirl.create(:people_professional_information, user: @user)
        degree = FactoryGirl.create(:people_degree, professional_information: professional_information)

        new_degrees_attributes = FactoryGirl.attributes_for_list(:people_degree, 2, professional_information_id: professional_information.id)
        new_attributes[:professional_information_attributes] = {
          id: professional_information.id,
          degrees_attributes: new_degrees_attributes.dup
        }

        degree_attributes = degree.attributes
        degree_attributes[:_destroy] = 1
        new_attributes[:professional_information_attributes][:degrees_attributes].push(degree_attributes)
        put :update, user: new_attributes
        @user.reload

        new_degrees_attributes.each_with_index do |degree_attr, index|
          degree = @user.professional_information.degrees.find_by(
            career_id: degree_attr[:career_id],
            institution_id: degree_attr[:institution_id],
            culmination_date: degree_attr[:culmination_date],
            condition: degree_attr[:condition],
          )
          expect(degree).to_not be nil
        end
      end

      it 'should update laboral_experiences' do
        new_attributes = FactoryGirl.attributes_for(:user)
        professional_information = FactoryGirl.create(:people_professional_information, user: @user)
        laboral_experience = FactoryGirl.create :people_laboral_experience, professional_information: professional_information

        new_lb_experiences_attrs = FactoryGirl.create_list :people_laboral_experience, 3, professional_information: professional_information

        new_attributes[:professional_information_attributes] = {
          id: professional_information.id,
          laboral_experiences_attributes: new_lb_experiences_attrs.dup
        }

        lb_experience_attributes = laboral_experience.attributes
        lb_experience_attributes[:_destroy] = 1
        new_attributes[:professional_information_attributes][:laboral_experiences_attributes].push(lb_experience_attributes)
        put :update, user: new_attributes
        @user.reload

        new_lb_experiences_attrs.each_with_index do |lb_experience_attr, index|
          laboral_experience = @user.professional_information.laboral_experiences.find_by(
            position: lb_experience_attr[:position],
            company: lb_experience_attr[:company],
            since_date: lb_experience_attr[:since_date],
            until_date: lb_experience_attr[:until_date],
          )
          expect(laboral_experience).to_not be nil
        end
      end
    end
  end

  describe 'POST documents' do
    login_executive_without_role
    let(:descriptor) { FactoryGirl.create(:document_descriptor) }

    before :each do
      request.env["HTTP_REFERER"] = edit_profiles_url
    end

    context "when valid params" do

      it "should upload documents" do
      end

      after :each do
        expect {
          post :upload_documents, applicant_documents: { descriptor.to_sym => Rack::Test::UploadedFile.new("#{Rails.root}/spec/tmp/avatar.jpg", 'image/jpeg')}
        }.to change(Document::Record, :count).by(1)

        format = :json
        descriptor =  FactoryGirl.create(:document_descriptor)
        expect {
          post :upload_documents, applicant_documents: { descriptor.to_sym => Rack::Test::UploadedFile.new("#{Rails.root}/spec/tmp/avatar.jpg", 'image/jpeg')}, format: format
        }.to change(Document::Record, :count).by(1)
        if format == :json
          expect(response).to render_template('document/records/index')
        end
      end
    end

    context "when invalid params" do
      it "should redirect_to onboarding stage with flash errors" do
        expect {
          post :upload_documents
        }.to change(Document::Record, :count).by(0)
        expect(flash[:error]).to eq I18n.t("applicant.upload_documents.upload_error.invalid_args")
        should redirect_to :back

        format = :json
        expect {
          post :upload_documents, format: format
        }.to change(Document::Record, :count).by(0)
        if format == :json
          expect(response).to render_template('document/records/index')
        end
      end
    end
  end
end
