# coding: utf-8
require 'rails_helper'

RSpec.describe Calendar::EventsController, type: :controller do

  describe "POST #create" do
    let(:course) { FactoryGirl.create(:course, user: FactoryGirl.create(:user)) }
    let(:enrollments) { FactoryGirl.create_list(:enrollment_basis_with_applicant, 3, course: course) }
    let(:guests) {
      [
        {name: FFaker::Name.name, email: FFaker::Internet.email},
        {name: FFaker::Name.name, email: FFaker::Internet.email},
        {name: FFaker::Name.name, email: FFaker::Internet.email}
      ]
    }

    let(:calendar_event_attributes) { FactoryGirl.attributes_for(:calendar_event, guests_attributes: guests) }

    context "when user is logged in" do
      login_executive_without_role

      context "and user has permissions" do
        before :each do
          enrollments.map(&:next_stage)
        end

        it "creates an event" do
          @user.add_role :admin

          expect{
            post :create,
                 format: :json,
                 enrollment_ids: enrollments.map(&:id),
                 calendar_event: calendar_event_attributes
          }.to change(Calendar::Event, :count).by(1).and \
               change(Calendar::EventResourceable, :count).by(3).and \
               change(Calendar::EventManager, :count).by(1).and \
               change(Calendar::EventGuest, :count).by(3).and \
               change(Delayed::Job, :count).by(7)
          expect(response.status).to be 200

          event = Calendar::Event.last
          expect(event.event_resourceables.count).to eq 3
          expect(event.stages.count).to eq 3

          _enrollments = assigns(:enrollments)
          _enrollments.each do |enrollment|
            expect(enrollment.errors).to be_empty
          end

          jobs = Delayed::Job.all.map{ |job| YAML.load(job.handler) }
          method_names = jobs.map(&:method_name)
          expect(method_names.count(:send_create_notification_to_stage)).to eq 3
          expect(method_names.count(:send_create_notification_to_manager)).to eq 1
          expect(method_names.count(:send_create_notification_to_guest)).to eq 3
        end

        context 'and one of the enrollments already has an event' do
          it 'creates events only for those without events' do
            @user.add_role :admin
            event_enrollment = FactoryGirl.create(:enrollment_basis_with_applicant)
            event_enrollment.next_stage
            event_enrollment.reload
            stage = event_enrollment.stage
            event = FactoryGirl.create(:calendar_event, stages: [stage])
            event_enrollment.reload
            expect(event.stages.count).to eq 1
            Delayed::Job.destroy_all

            sent_enrollments = enrollments + [event_enrollment]
            expect{
              post :create,
                   format: :json,
                   enrollment_ids: sent_enrollments.map(&:id),
                   calendar_event: calendar_event_attributes
            }.to change(Calendar::Event, :count).by(1).and \
                 change(Calendar::EventResourceable, :count).by(enrollments.count).and \
                 change(Calendar::EventManager, :count).by(1).and \
                 change(Calendar::EventGuest, :count).by(3).and \
                 change(Delayed::Job, :count).by(enrollments.count + 1 + 3)
            expect(response.status).to be 200

            event = Calendar::Event.last
            expect(event.event_resourceables.count).to eq enrollments.count
            expect(event.stages.count).to eq enrollments.count

            _enrollments = assigns(:enrollments)
            _enrollments.each do |enrollment|
              expect(enrollment.errors).to be_empty if enrollment.id != event_enrollment.id
            end

            jobs = Delayed::Job.all.map{ |job| YAML.load(job.handler) }

            method_names = jobs.map(&:method_name)
            expect(method_names.count(:send_create_notification_to_stage)).to eq 3
            expect(method_names.count(:send_create_notification_to_manager)).to eq 1
            expect(method_names.count(:send_create_notification_to_guest)).to eq 3
          end
        end

        context "and enrollment was disposed" do
          it "does not create an event" do
            @user.add_role :admin
            enrollment = enrollments.first
            enrollment.discard(FFaker::Lorem.word, FactoryGirl.create(:enrollment_discard_reason))
            enrollment.reload

            expect{
              post :create,
                   format: :json,
                   enrollment_ids: [enrollment.id],
                   calendar_event: calendar_event_attributes
            }.to change(Calendar::Event, :count).by(0).and \
                 change(Calendar::EventResourceable, :count).by(0).and \
                 change(Calendar::EventManager, :count).by(0).and \
                 change(Calendar::EventGuest, :count).by(0).and \
                 change(Delayed::Job, :count).by(0)
            expect(response.status).to be 200

            _enrollments = assigns(:enrollments)
            _enrollments.each do |enrollment|
              expect(enrollment.errors[:course]).to include I18n.t("activerecord.errors.models.enrollment/base.attributes.course.enrollment_disposed")
            end
          end
        end

        context "and enrollment rejected course invitation" do
          it "does not create an event" do
            @user.add_role :admin
            enrollment = FactoryGirl.create(:enrollment_basis_with_applicant)
            enrollment.invite
            expect(enrollment.invited?).to be true
            enrollment.mark_invitation_rejected
            expect(enrollment.invitation_accepted?).to be false

            expect{
              post :create,
                   format: :json,
                   enrollment_ids: [enrollment.id],
                   calendar_event: calendar_event_attributes
            }.to change(Calendar::Event, :count).by(0).and \
                 change(Calendar::EventResourceable, :count).by(0).and \
                 change(Calendar::EventManager, :count).by(0).and \
                 change(Calendar::EventGuest, :count).by(0).and \
                 change(Delayed::Job, :count).by(0)
            expect(response.status).to be 200

            _enrollments = assigns(:enrollments)
            _enrollments.each do |enrollment|
              expect(enrollment.errors[:course]).to include I18n.t("activerecord.errors.models.enrollment/base.attributes.course.course_invitation_rejected")
            end
          end
        end

        context "and enrollment stage is nil" do
          it "does not create an event" do
            @user.add_role :admin
            enrollment = FactoryGirl.create(:enrollment_basis_with_applicant)

            expect{
              post :create,
                   format: :json,
                   enrollment_ids: [enrollment.id],
                   calendar_event: calendar_event_attributes
            }.to change(Calendar::Event, :count).by(0).and \
                 change(Calendar::EventResourceable, :count).by(0).and \
                 change(Calendar::EventManager, :count).by(0).and \
                 change(Calendar::EventGuest, :count).by(0).and \
                 change(Delayed::Job, :count).by(0)
            expect(response.status).to be 200

            _enrollments = assigns(:enrollments)
            _enrollments.each do |enrollment|
              expect(enrollment.errors[:course]).to include I18n.t("activerecord.errors.models.enrollment/base.attributes.course.cant_schedule_without_stage")
            end
          end
        end

        context "and enrollment stage already have an event (event_resourceable)" do
          it "does not create an event" do
            @user.add_role :admin
            enrollment = FactoryGirl.create(:enrollment_basis_with_applicant)
            enrollment.next_stage
            enrollment.reload
            stage = enrollment.stage
            event = FactoryGirl.create(:calendar_event, stages: [stage])
            expect(event.stages.count).to eq 1

            expect{
              post :create,
                   format: :json,
                   enrollment_ids: [enrollment.id],
                   calendar_event: calendar_event_attributes
            }.to change(Calendar::Event, :count).by(0).and \
                 change(Calendar::EventResourceable, :count).by(0).and \
                 change(Calendar::EventManager, :count).by(0).and \
                 change(Calendar::EventGuest, :count).by(0).and \
                 change(Delayed::Job, :count).by(0)
            expect(response.status).to be 200

            _enrollments = assigns(:enrollments)
            _enrollments.each do |enrollment|
              expect(enrollment.errors[:course]).to include I18n.t("activerecord.errors.models.enrollment/base.attributes.course.stage_already_scheduled")
            end
          end
        end
      end

      context "and user has no permissions" do
        it "does not creates an event" do
          Delayed::Job.destroy_all
          expect{
            post :create,
                 format: :json,
                 enrollment_ids: enrollments.map(&:id),
                 calendar_event: calendar_event_attributes
          }.to change(Calendar::Event, :count).by(0).and \
               change(Calendar::EventResourceable, :count).by(0).and \
               change(Calendar::EventManager, :count).by(0).and \
               change(Calendar::EventGuest, :count).by(0)
          expect(response.status).to eq 200
          _enrollments = assigns(:enrollments)
          _enrollments.each do |enrollment|
            expect(enrollment.errors).not_to be_empty
            expect(enrollment.errors[:course]).to include I18n.t("activerecord.errors.models.enrollment/base.attributes.course.enough_permission")
          end
        end
      end
    end

    context "when user is not logged in" do
      it "response with status 401 (not authorized)" do
        expect{
          post :create,
               format: :json,
               enrollment_ids: enrollments.map(&:id),
                 calendar_event: calendar_event_attributes
        }.to change(Calendar::Event, :count).by(0).and \
             change(Calendar::EventResourceable, :count).by(0).and \
             change(Calendar::EventManager, :count).by(0).and \
             change(Calendar::EventGuest, :count).by(0)
        expect(response.status).to eq 401
      end
    end
  end

  describe "PUT #update" do
    let(:course) { FactoryGirl.create(:course, user: FactoryGirl.create(:user)) }
    let(:enrollments) { FactoryGirl.create_list(:enrollment_basis_with_applicant, 3, course: course) }
    let(:guests_attributes) {
      [
        {name: FFaker::Name.name, email: FFaker::Internet.email},
        {name: FFaker::Name.name, email: FFaker::Internet.email},
        {name: FFaker::Name.name, email: FFaker::Internet.email}
      ]
    }
    let(:calendar_event_attributes) { FactoryGirl.attributes_for(:calendar_event, guests_attributes: guests_attributes) }
    let(:event) {
      Calendar::Event.create(calendar_event_attributes)
    }

    before :each do
      @event = Calendar::Event.create(calendar_event_attributes)
    end

    context "when user is logged in" do
      login_executive_without_role

      context "and user has permissions" do
        before :each do
          @enrollments = enrollments
          @enrollments.map(&:next_stage)
        end

        context "when updates an event" do
          it "updates everything in an event" do
            @user.add_role :admin
            Delayed::Job.destroy_all

            expect{
              put :update, id: @event.id,
                  format: :json,
                  enrollment_ids: enrollments.map(&:id),
                  calendar_event: calendar_event_attributes
            }.to change(Calendar::Event, :count).by(0).and \
                 change(Calendar::EventResourceable, :count).by(3).and \
                 change(Calendar::EventGuest, :count).by(3).and \
                 change(Calendar::EventManager, :count).by(0).and \
                 change(Delayed::Job, :count).by(11)

            expect(response.status).to be 200

            event = Calendar::Event.last
            expect(event.event_resourceables.count).to eq 3
            expect(event.stages.count).to eq 3
            expect(event.guests.count).to eq 6
            expect(event.manager.id).not_to eq nil

            _enrollments = assigns(:enrollments)
            _enrollments.each do |enrollment|
              expect(enrollment.errors).to be_empty
            end

            jobs = Delayed::Job.all.map{ |job| YAML.load(job.handler) }
            method_names = jobs.map(&:method_name)
            expect(method_names.count(:send_create_notification_to_stage)).to eq 3
            expect(method_names.count(:send_create_notification_to_manager)).to eq 1
            expect(method_names.count(:send_removed_notification_to_manager)).to eq 1
            expect(method_names.count(:send_create_notification_to_guest)).to eq 3
            expect(method_names.count(:send_updates_notification_to_guest_with_updated_stages)).to eq 3
          end

          it "updates (add) only stages" do
            @user.add_role :admin

            other_enrollments = FactoryGirl.create_list(:enrollment_basis_with_applicant, 3, course: course)
            stages = other_enrollments.map(&:next_stage)
            @event.update(stages: stages, created_by: @user)

            Delayed::Job.destroy_all

            calendar_event_attributes = {}
            calendar_event_attributes[:manager_attributes] = {
              id: @event.manager.id,
              name: @event.manager.name,
              email: @event.manager.email

            }
            guests_attributes = @event.guests.map{ |guest| {id: guest.id} }
            calendar_event_attributes[:guests_attributes] = guests_attributes
            expect{
              put :update, id: @event.id,
                  format: :json,
                  enrollment_ids: other_enrollments.map(&:id) + enrollments.map(&:id),
                  calendar_event: calendar_event_attributes
            }.to change(Calendar::Event, :count).by(0).and \
                 change(Calendar::EventResourceable, :count).by(3).and \
                 change(Calendar::EventGuest, :count).by(0).and \
                 change(Calendar::EventManager, :count).by(0).and \
                 change(Delayed::Job, :count).by(7)

            expect(response.status).to be 200

            event = Calendar::Event.last
            expect(event.event_resourceables.count).to eq 6
            expect(event.stages.count).to eq 6
            expect(event.guests.count).to eq 3
            expect(event.manager.id).not_to eq nil

            _enrollments = assigns(:enrollments)
            _enrollments.each do |enrollment|
              expect(enrollment.errors).to be_empty
            end

            jobs = Delayed::Job.all.map{ |job| YAML.load(job.handler) }
            method_names = jobs.map(&:method_name)
            expect(method_names.count(:send_create_notification_to_stage)).to eq 3
            expect(method_names.count(:send_updates_notification_to_manager_with_updated_stages)).to eq 1
            expect(method_names.count(:send_updates_notification_to_guest_with_updated_stages)).to eq 3
          end

          it "updates (add) only guests" do
            @user.add_role :admin

            stages = enrollments.map(&:next_stage)
            @event.update(stages: stages, created_by: @user)

            Delayed::Job.destroy_all

            calendar_event_attributes = {}
            calendar_event_attributes[:manager_attributes] = {
              id: @event.manager.id,
              name: @event.manager.name,
              email: @event.manager.email
            }
            guests_attributes = @event.guests.map{ |guest| {id: guest.id} }
            new_guests_attributes = [
              {name: FFaker::Name.name, email: FFaker::Internet.email},
              {name: FFaker::Name.name, email: FFaker::Internet.email},
              {name: FFaker::Name.name, email: FFaker::Internet.email},
              {name: FFaker::Name.name, email: FFaker::Internet.email}
            ]
            calendar_event_attributes[:guests_attributes] = guests_attributes + new_guests_attributes

            expect{
              put :update, id: @event.id,
                  format: :json,
                  enrollment_ids: enrollments.map(&:id),
                  calendar_event: calendar_event_attributes
            }.to change(Calendar::Event, :count).by(0).and \
                 change(Calendar::EventResourceable, :count).by(0).and \
                 change(Calendar::EventGuest, :count).by(4).and \
                 change(Calendar::EventManager, :count).by(0).and \
                 change(Delayed::Job, :count).by(8)

            expect(response.status).to be 200

            event = Calendar::Event.last
            expect(event.event_resourceables.count).to eq 3
            expect(event.stages.count).to eq 3
            expect(event.guests.count).to eq 7
            expect(event.manager.id).not_to eq nil

            _enrollments = assigns(:enrollments)
            _enrollments.each do |enrollment|
              expect(enrollment.errors).to be_empty
            end

            jobs = Delayed::Job.all.map{ |job| YAML.load(job.handler) }
            method_names = jobs.map(&:method_name)
            expect(method_names.count(:send_updates_notification_to_manager)).to eq 1
            expect(method_names.count(:send_updates_notification_to_guest)).to eq 3
            expect(method_names.count(:send_create_notification_to_guest)).to eq 4
          end

          context "and the event is for its current stage" do
            it "changes nothing and no notifications" do
              @user.add_role :admin
              enrollment = FactoryGirl.create(:enrollment_basis_with_applicant)
              enrollment.next_stage
              enrollment.reload
              stage = enrollment.stage
              event = FactoryGirl.create(:calendar_event, stages: [stage], created_by: @user)
              expect(event.stages.count).to eq 1

              Delayed::Job.destroy_all

              calendar_event_attributes = {}
              calendar_event_attributes[:manager_attributes] = {
                id: event.manager.id,
                name: event.manager.name,
                email: event.manager.email
              }
              expect{
                put :update, id: event.id,
                    format: :json,
                    enrollment_ids: [enrollment.id],
                    calendar_event: calendar_event_attributes
              }.to change(Calendar::Event, :count).by(0).and \
                   change(Calendar::EventResourceable, :count).by(0).and \
                   change(Calendar::EventGuest, :count).by(0).and \
                   change(Calendar::EventManager, :count).by(0).and \
                   change(Delayed::Job, :count).by(0)
              expect(response.status).to be 200

              _enrollments = assigns(:enrollments)
              _enrollments.each do |enrollment|
                expect(enrollment.errors).to be_empty
              end
            end
          end
        end

        context "and enrollment was disposed" do
          it "does not update event" do
            @user.add_role :admin
            enrollment = enrollments.first
            enrollment.discard(FFaker::Lorem.word, FactoryGirl.create(:enrollment_discard_reason))
            enrollment.reload
            expect{
              put :update, id: @event.id,
                   format: :json,
                   enrollment_ids: [enrollment.id],
                   calendar_event: calendar_event_attributes
            }.to change(Calendar::Event, :count).by(0).and \
                 change(Calendar::EventResourceable, :count).by(0).and \
                 change(Calendar::EventGuest, :count).by(0).and \
                 change(Calendar::EventManager, :count).by(0).and \
                 change(Delayed::Job, :count).by(0)
            @event.reload

            expect(response.status).to be 200

            _enrollments = assigns(:enrollments)
            _enrollments.each do |enrollment|
              expect(enrollment.errors[:course]).to include I18n.t("activerecord.errors.models.enrollment/base.attributes.course.enrollment_disposed")
            end
          end
        end

        context "and enrollment rejected course invitation" do
          it "does not update event" do
            @user.add_role :admin
            enrollment = FactoryGirl.create(:enrollment_basis_with_applicant)
            enrollment.invite
            expect(enrollment.invited?).to be true
            enrollment.mark_invitation_rejected
            expect(enrollment.invitation_accepted?).to be false

            expect{
              put :update, id: @event.id,
                   format: :json,
                   enrollment_ids: [enrollment.id],
                   calendar_event: calendar_event_attributes
            }.to change(Calendar::Event, :count).by(0).and \
                 change(Calendar::EventResourceable, :count).by(0).and \
                 change(Calendar::EventGuest, :count).by(0).and \
                 change(Calendar::EventManager, :count).by(0).and \
                 change(Delayed::Job, :count).by(0)
            expect(response.status).to be 200

            _enrollments = assigns(:enrollments)
            _enrollments.each do |enrollment|
              expect(enrollment.errors[:course]).to include I18n.t("activerecord.errors.models.enrollment/base.attributes.course.course_invitation_rejected")
            end
          end
        end

        context "and enrollment stage is nil" do
          it "does not update event" do
            @user.add_role :admin
            enrollment = FactoryGirl.create(:enrollment_basis_with_applicant)

            expect{
              put :update, id: @event.id,
                   format: :json,
                   enrollment_ids: [enrollment.id],
                   calendar_event: calendar_event_attributes
            }.to change(Calendar::Event, :count).by(0).and \
                 change(Calendar::EventResourceable, :count).by(0).and \
                 change(Calendar::EventGuest, :count).by(0).and \
                 change(Calendar::EventManager, :count).by(0).and \
                 change(Delayed::Job, :count).by(0)
            expect(response.status).to be 200

            _enrollments = assigns(:enrollments)
            _enrollments.each do |enrollment|
              expect(enrollment.errors[:course]).to include I18n.t("activerecord.errors.models.enrollment/base.attributes.course.cant_schedule_without_stage")
            end
          end
        end

        context "and enrollment stage already have an event (event_resourceable)" do
          context "and the event is not for its current stage" do
            it "does update event (add stage to event)" do
              @user.add_role :admin
              enrollment = FactoryGirl.create(:enrollment_basis_with_applicant)
              enrollment.next_stage
              enrollment.reload
              stage = enrollment.stage
              event = FactoryGirl.create(:calendar_event, stages: [stage])
              expect(event.stages.count).to eq 1

              Delayed::Job.destroy_all

              expect{
                put :update, id: @event.id,
                    format: :json,
                    enrollment_ids: [enrollment.id],
                    calendar_event: calendar_event_attributes
              }.to change(Calendar::Event, :count).by(0).and \
                   change(Calendar::EventResourceable, :count).by(1).and \
                   change(Calendar::EventGuest, :count).by(3).and \
                   change(Calendar::EventManager, :count).by(0).and \
                   change(Delayed::Job, :count).by(9)
              expect(response.status).to be 200

              _enrollments = assigns(:enrollments)
              _enrollments.each do |enrollment|
                expect(enrollment.errors).to be_empty
              end

              jobs = Delayed::Job.all.map{ |job| YAML.load(job.handler) }
              method_names = jobs.map(&:method_name)
              expect(method_names.count(:send_create_notification_to_stage)).to eq 1
              expect(method_names.count(:send_create_notification_to_manager)).to eq 1
              expect(method_names.count(:send_removed_notification_to_manager)).to eq 1
              expect(method_names.count(:send_create_notification_to_guest)).to eq 3
              expect(method_names.count(:send_updates_notification_to_guest_with_updated_stages)).to eq 3
            end
          end
        end
      end

      context "and user has no permissions" do
        it "does not updates event" do
          Delayed::Job.destroy_all
          expect{
            put :update, id: @event.id,
                 format: :json,
                 enrollment_ids: enrollments.map(&:id),
                 calendar_event: calendar_event_attributes
          }.to change(Calendar::Event, :count).by(0).and \
               change(Calendar::EventResourceable, :count).by(0).and \
               change(Calendar::EventGuest, :count).by(0).and \
               change(Calendar::EventManager, :count).by(0)
          expect(response.status).to eq 200
          _enrollments = assigns(:enrollments)
          _enrollments.each do |enrollment|
            expect(enrollment.errors).not_to be_empty
            expect(enrollment.errors[:course]).to include I18n.t("activerecord.errors.models.enrollment/base.attributes.course.enough_permission")
          end
        end
      end
    end

    context "when user is not logged in" do
      it "response with status 401 (not authorized)" do
        Delayed::Job.destroy_all
        expect{
          put :update, id: @event.id,
               format: :json,
               enrollment_ids: enrollments.map(&:id),
               calendar_event: calendar_event_attributes
        }.to change(Calendar::Event, :count).by(0).and \
             change(Calendar::EventResourceable, :count).by(0).and \
             change(Calendar::EventGuest, :count).by(0).and \
             change(Calendar::EventManager, :count).by(0)
        expect(response.status).to eq 401
      end
    end
  end

  describe "POST #create_for_applicants" do
    let(:applicants) { FactoryGirl.create_list(:applicant_bases, 3) }
    let(:guests) {
      [
        {name: FFaker::Name.name, email: FFaker::Internet.email},
        {name: FFaker::Name.name, email: FFaker::Internet.email},
        {name: FFaker::Name.name, email: FFaker::Internet.email}
      ]
    }

    let(:calendar_event_attributes) { FactoryGirl.attributes_for(:calendar_event, guests_attributes: guests) }

    context "when user is logged in" do
      login_executive_without_role

      context "and user has permissions" do

        it "creates an event" do
          @user.add_role :admin

          expect{
            post :create_for_applicants,
                 format: :json,
                 applicant_ids: applicants.map(&:id),
                 calendar_event: calendar_event_attributes
          }.to change(Calendar::Event, :count).by(1).and \
               change(Calendar::EventResourceable, :count).by(3).and \
               change(Calendar::EventManager, :count).by(1).and \
               change(Calendar::EventGuest, :count).by(3).and \
               change(Delayed::Job, :count).by(7)
          expect(response.status).to be 200

          event = Calendar::Event.last
          expect(event.event_resourceables.count).to eq 3
          expect(event.applicants.count).to eq 3

          _applicants = assigns(:applicants)
          _applicants.each do |applicant|
            expect(applicant.errors).to be_empty
          end

          jobs = Delayed::Job.all.map{ |job| YAML.load(job.handler) }
          method_names = jobs.map(&:method_name)
          expect(method_names.count(:send_create_notification_to_applicant)).to eq 3
          expect(method_names.count(:send_create_notification_to_manager)).to eq 1
          expect(method_names.count(:send_create_notification_to_guest)).to eq 3
        end
      end

      context "and user has no permissions" do
        it "does not creates an event" do
          Delayed::Job.destroy_all
          expect{
            post :create_for_applicants,
                 format: :json,
                 applicant_ids: applicants.map(&:id),
                 calendar_event: calendar_event_attributes
          }.to change(Calendar::Event, :count).by(0).and \
               change(Calendar::EventResourceable, :count).by(0).and \
               change(Calendar::EventManager, :count).by(0).and \
               change(Calendar::EventGuest, :count).by(0)
          expect(response.status).to eq 200
          _applicants = assigns(:applicants)
          _applicants.each do |applicant|
            expect(applicant.errors).not_to be_empty
            expect(applicant.errors[:base]).to include I18n.t("activerecord.errors.models.applicant/base.attributes.base.enough_permission")
          end
        end
      end
    end

    context "when user is not logged in" do
      it "response with status 401 (not authorized)" do
        expect{
          post :create_for_applicants,
               format: :json,
               applicant_ids: applicants.map(&:id),
               calendar_event: calendar_event_attributes
        }.to change(Calendar::Event, :count).by(0).and \
             change(Calendar::EventResourceable, :count).by(0).and \
             change(Calendar::EventManager, :count).by(0).and \
             change(Calendar::EventGuest, :count).by(0)
        expect(response.status).to eq 401
      end
    end
  end


  describe "PUT #update_for_applicants" do
    let(:applicants) { FactoryGirl.create_list(:applicant_bases, 3) }
    let(:guests_attributes) {
      [
        {name: FFaker::Name.name, email: FFaker::Internet.email},
        {name: FFaker::Name.name, email: FFaker::Internet.email},
        {name: FFaker::Name.name, email: FFaker::Internet.email}
      ]
    }
    let(:calendar_event_attributes) { FactoryGirl.attributes_for(:calendar_event, guests_attributes: guests_attributes) }
    let(:event) {
      Calendar::Event.create(calendar_event_attributes)
    }

    before :each do
      @event = Calendar::Event.create(calendar_event_attributes)
    end

    context "when user is logged in" do
      login_executive_without_role

      context "and user has permissions" do

        context "when updates an event" do
          it "updates everything in an event" do
            @user.add_role :admin
            Delayed::Job.destroy_all

            expect{
              put :update_for_applicants, id: @event.id,
                  format: :json,
                  applicant_ids: applicants.map(&:id),
                  calendar_event: calendar_event_attributes
            }.to change(Calendar::Event, :count).by(0).and \
                 change(Calendar::EventResourceable, :count).by(3).and \
                 change(Calendar::EventGuest, :count).by(3).and \
                 change(Calendar::EventManager, :count).by(0).and \
                 change(Delayed::Job, :count).by(11)

            expect(response.status).to be 200

            event = Calendar::Event.last
            expect(event.event_resourceables.count).to eq 3
            expect(event.applicants.count).to eq 3
            expect(event.guests.count).to eq 6
            expect(event.manager.id).not_to eq nil

            _applicants = assigns(:applicants)
            _applicants.each do |applicant|
              expect(applicant.errors).to be_empty
            end

            jobs = Delayed::Job.all.map{ |job| YAML.load(job.handler) }
            method_names = jobs.map(&:method_name)
            expect(method_names.count(:send_create_notification_to_applicant)).to eq 3
            expect(method_names.count(:send_create_notification_to_manager)).to eq 1
            expect(method_names.count(:send_removed_notification_to_manager)).to eq 1
            expect(method_names.count(:send_create_notification_to_guest)).to eq 3
            expect(method_names.count(:send_updates_notification_to_guest_with_updated_applicants)).to eq 3
          end

          it "updates (add) only applicants" do
            @user.add_role :admin

            other_applicants = FactoryGirl.create_list(:applicant_bases, 3)
            @event.update(applicants: applicants, created_by: @user)

            Delayed::Job.destroy_all

            calendar_event_attributes = {}
            calendar_event_attributes[:manager_attributes] = {
              id: @event.manager.id,
              name: @event.manager.name,
              email: @event.manager.email
            }
            guests_attributes = @event.guests.map{ |guest| {id: guest.id} }
            calendar_event_attributes[:guests_attributes] = guests_attributes
            expect{
              put :update_for_applicants, id: @event.id,
                  format: :json,
                  applicant_ids: other_applicants.map(&:id) + applicants.map(&:id),
                  calendar_event: calendar_event_attributes
            }.to change(Calendar::Event, :count).by(0).and \
                 change(Calendar::EventResourceable, :count).by(3).and \
                 change(Calendar::EventGuest, :count).by(0).and \
                 change(Calendar::EventManager, :count).by(0).and \
                 change(Delayed::Job, :count).by(7)

            expect(response.status).to be 200

            event = Calendar::Event.last
            expect(event.event_resourceables.count).to eq 6
            expect(event.applicants.count).to eq 6
            expect(event.guests.count).to eq 3
            expect(event.manager.id).not_to eq nil

            _applicants = assigns(:applicants)
            _applicants.each do |applicant|
              expect(applicant.errors).to be_empty
            end

            jobs = Delayed::Job.all.map{ |job| YAML.load(job.handler) }
            method_names = jobs.map(&:method_name)
            expect(method_names.count(:send_create_notification_to_applicant)).to eq 3
            expect(method_names.count(:send_updates_notification_to_manager_with_updated_applicants)).to eq 1
            expect(method_names.count(:send_updates_notification_to_guest_with_updated_applicants)).to eq 3
          end

          it "updates (add) only guests" do
            @user.add_role :admin

            @event.update(applicants: applicants, created_by: @user)

            Delayed::Job.destroy_all

            calendar_event_attributes = {}
            calendar_event_attributes[:manager_attributes] = {
              id: @event.manager.id,
              name: @event.manager.name,
              email: @event.manager.email
            }
            guests_attributes = @event.guests.map{ |guest| {id: guest.id} }
            new_guests_attributes = [
              {name: FFaker::Name.name, email: FFaker::Internet.email},
              {name: FFaker::Name.name, email: FFaker::Internet.email},
              {name: FFaker::Name.name, email: FFaker::Internet.email},
              {name: FFaker::Name.name, email: FFaker::Internet.email}
            ]
            calendar_event_attributes[:guests_attributes] = guests_attributes + new_guests_attributes

            expect{
              put :update_for_applicants, id: @event.id,
                  format: :json,
                  applicant_ids: applicants.map(&:id),
                  calendar_event: calendar_event_attributes
            }.to change(Calendar::Event, :count).by(0).and \
                 change(Calendar::EventResourceable, :count).by(0).and \
                 change(Calendar::EventGuest, :count).by(4).and \
                 change(Calendar::EventManager, :count).by(0).and \
                 change(Delayed::Job, :count).by(8)

            expect(response.status).to be 200

            event = Calendar::Event.last
            expect(event.event_resourceables.count).to eq 3
            expect(event.applicants.count).to eq 3
            expect(event.guests.count).to eq 7
            expect(event.manager.id).not_to eq nil

            _applicants = assigns(:applicants)
            _applicants.each do |applicant|
              expect(applicant.errors).to be_empty
            end

            jobs = Delayed::Job.all.map{ |job| YAML.load(job.handler) }
            method_names = jobs.map(&:method_name)
            expect(method_names.count(:send_updates_notification_to_manager)).to eq 1
            expect(method_names.count(:send_updates_notification_to_guest)).to eq 3
            expect(method_names.count(:send_create_notification_to_guest)).to eq 4
          end

          context "and the event is for its current applicant" do
            it "changes nothing and no notifications" do
              @user.add_role :admin
              applicant = FactoryGirl.create(:applicant_bases)
              event = FactoryGirl.create(:calendar_event, applicants: [applicant], created_by: @user)
              expect(event.applicants.count).to eq 1

              Delayed::Job.destroy_all

              calendar_event_attributes = {}
              calendar_event_attributes[:manager_attributes] = {
                id: event.manager.id,
                name: event.manager.name,
                email: event.manager.email
              }
              expect{
                put :update_for_applicants, id: event.id,
                    format: :json,
                    applicant_ids: [applicant.id],
                    calendar_event: calendar_event_attributes
              }.to change(Calendar::Event, :count).by(0).and \
                   change(Calendar::EventResourceable, :count).by(0).and \
                   change(Calendar::EventGuest, :count).by(0).and \
                   change(Calendar::EventManager, :count).by(0).and \
                   change(Delayed::Job, :count).by(0)
              expect(response.status).to be 200

              _applicants = assigns(:applicants)
              _applicants.each do |applicant|
                expect(applicant.errors).to be_empty
              end
            end
          end
        end
      end

      context "and user has no permissions" do
        it "does not updates event" do
          Delayed::Job.destroy_all
          expect{
            put :update_for_applicants, id: @event.id,
                 format: :json,
                 applicant_ids: applicants.map(&:id),
                 calendar_event: calendar_event_attributes
          }.to change(Calendar::Event, :count).by(0).and \
               change(Calendar::EventResourceable, :count).by(0).and \
               change(Calendar::EventGuest, :count).by(0).and \
               change(Calendar::EventManager, :count).by(0)
          expect(response.status).to eq 200
          _applicants = assigns(:applicants)
          _applicants.each do |applicant|
            expect(applicant.errors).not_to be_empty
            expect(applicant.errors[:base]).to include I18n.t("activerecord.errors.models.applicant/base.attributes.base.enough_permission")
          end
        end
      end
    end

    context "when user is not logged in" do
      it "response with status 401 (not authorized)" do
        Delayed::Job.destroy_all
        expect{
          put :update_for_applicants, id: @event.id,
               format: :json,
               applicant_ids: applicants.map(&:id),
               calendar_event: calendar_event_attributes
        }.to change(Calendar::Event, :count).by(0).and \
             change(Calendar::EventResourceable, :count).by(0).and \
             change(Calendar::EventGuest, :count).by(0).and \
             change(Calendar::EventManager, :count).by(0)
        expect(response.status).to eq 401
      end
    end
  end

  describe "PUT #mark_attended" do
    context "when event resourceable is a stage" do
      let(:event) { FactoryGirl.create(:calendar_event) }
      let(:enrollment) { FactoryGirl.create(:enrollment_basis_with_applicant) }
      let(:stage) { enrollment.next_stage }

      context "when user is logged in" do
        login_executive_without_role

        context "and when user has permissions (is admin)" do
          context "and resourceable is in event" do
            context "set event resourceable as attended" do
              before :each do
                event.stages << stage
                event.save
                event.reload

                @event_resourceable = event.event_resourceables.first
              end

              it "when user is admin" do
                @user.add_role :admin
              end

              it "when user is admin for course" do
                @user.add_role :admin, @event_resourceable.resourceable.enrollment.course
              end

              it "when user is admin for stage" do
                @user.add_role :admin, stage
              end

              it "when user is admin for stage.class" do
                @user.add_role :admin, stage.class
              end

              after :each do
                put :mark_attended, format: :json,
                    id: event.id,
                    resourceable_id: stage.id,
                    resourceable_type: stage.class.to_s,
                    attended: true

                @event_resourceable.reload

                expect(@event_resourceable.attended).to be true
                expect(response.status).to eq 200
                expect(JSON::parse(response.body)).to match_array JSON::parse(@event_resourceable.to_json)
              end
            end

            context "set event resourceable as unattended" do
              before :each do
                event.stages << stage
                event.save
                event.reload

                @event_resourceable = event.event_resourceables.first
              end

              it "when user is admin" do
                @user.add_role :admin
              end

              it "when user is admin for course" do
                @user.add_role :admin, @event_resourceable.resourceable.enrollment.course
              end

              it "when user is admin for stage" do
                @user.add_role :admin, stage
              end

              it "when user is admin for stage.class" do
                @user.add_role :admin, stage.class
              end

              after :each do
                put :mark_attended, format: :json,
                    id: event.id,
                    resourceable_id: stage.id,
                    resourceable_type: stage.class.to_s,
                    attended: false

                @event_resourceable.reload

                expect(@event_resourceable.attended).to be false
                expect(response.status).to eq 200
                expect(JSON::parse(response.body)).to match_array JSON::parse(@event_resourceable.to_json)
              end
            end

            context "when attended param is missing return error (attended required)" do
              before :each do
                event.stages << stage
                event.save
                event.reload

                @event_resourceable = event.event_resourceables.first
              end

              it "when user is admin" do
                @user.add_role :admin
              end

              it "when user is admin for course" do
                @user.add_role :admin, @event_resourceable.resourceable.enrollment.course
              end

              it "when user is admin for stage" do
                @user.add_role :admin, stage
              end

              it "when user is admin for stage.class" do
                @user.add_role :admin, stage.class
              end

              after :each do
                put :mark_attended, format: :json,
                    id: event.id,
                    resourceable_id: stage.id,
                    resourceable_type: stage.class.to_s

                @event_resourceable.reload

                expect(@event_resourceable.attended).to be nil
                expect(response.status).to eq 200
                expected_response = {"errors" => assigns(:event_resourceable).errors.full_messages}
                expect(JSON::parse(response.body)).to match_array expected_response
              end
            end

            context "and resourceable is not in event" do
              it "should return 404" do
                @user.add_role :admin

                event2 = FactoryGirl.create(:calendar_event)
                event2.stages << stage
                event2.save
                event2.reload

                resourceable = event2.event_resourceables.first
                attended = resourceable.attended

                put :mark_attended, format: :json,
                    id: event.id,
                    resourceable_id: stage.id,
                    resourceable_type: stage.class.to_s

                resourceable.reload

                expect(resourceable.attended).to be attended
                expect(response.status).to eq 404
                expect(response.body).to eq ""
              end
            end
          end

          context "when user has no permissions (is not admin)" do
            it "should response 403" do
              event.stages << stage
              event.save
              event.reload

              resourceable = event.event_resourceables.first
              attended = resourceable.attended

              put :mark_attended, format: :json,
                  id: event.id,
                  resourceable_id: stage.id,
                  resourceable_type: stage.class.to_s

              expect(resourceable.attended).to be attended

              expect(response.status).to eq 403
              expect(response.body).to eq ""
            end
          end
        end

      end

      context "when user is not logged in" do
        context "format is json" do
          it "should response 401" do
            event.stages << stage
            event.save
            event.reload

            put :mark_attended, format: :json,
                id: event.id,
                resourceable_id: stage.id,
                resourceable_type: stage.class.to_s
            expect(response.status).to eq 401
            expected_response_body = {error:  I18n.t('devise.failure.unauthenticated') }
            expect(response.body).to eq expected_response_body.to_json
          end
        end
      end
    end

    context "when event resourceable is an applicant" do
      let(:event) { FactoryGirl.create(:calendar_event) }
      let(:enrollment) { FactoryGirl.create(:enrollment_basis_with_applicant) }
      let(:applicant) { enrollment.applicant }

      context "when user is logged in" do
        login_executive_without_role

        context "and when user has permissions (is admin)" do
          context "and resourceable is in event" do
            context "set event resourceable as attended" do
              before :each do
                event.applicants << applicant
                event.save
                event.reload

                @event_resourceable = event.event_resourceables.first
              end

              it "when user is admin" do
                @user.add_role :admin
              end

              it "when user is admin for Applicant::Base" do
                # no lo hace?
                @user.add_role :admin, Applicant::Base
              end

              after :each do
                put :mark_attended, format: :json,
                    id: event.id,
                    resourceable_id: applicant.id,
                    resourceable_type: applicant.class.to_s,
                    attended: true

                @event_resourceable.reload

                expect(@event_resourceable.attended).to be true
                expect(response.status).to eq 200
                expect(JSON::parse(response.body)).to match_array JSON::parse(@event_resourceable.to_json)
              end
            end

            context "set event resourceable as unattended" do
              before :each do
                event.applicants << applicant
                event.save
                event.reload

                @event_resourceable = event.event_resourceables.first
              end

              it "when user is admin" do
                @user.add_role :admin
              end

              it "when user is admin for Applicant::Base" do
                # no lo hace?
                @user.add_role :admin, Applicant::Base
              end

              after :each do
                put :mark_attended, format: :json,
                    id: event.id,
                    resourceable_id: applicant.id,
                    resourceable_type: applicant.class.to_s,
                    attended: false

                @event_resourceable.reload

                expect(@event_resourceable.attended).to be false
                expect(response.status).to eq 200
                expect(JSON::parse(response.body)).to match_array JSON::parse(@event_resourceable.to_json)
              end
            end

            context "when attended param is missing return error (attended required)" do
              before :each do
                event.applicants << applicant
                event.save
                event.reload

                @event_resourceable = event.event_resourceables.first
              end

              it "when user is admin" do
                @user.add_role :admin
              end

              it "when user is admin for Applicant::Base" do
                @user.add_role :admin, Applicant::Base
              end

              after :each do
                put :mark_attended, format: :json,
                    id: event.id,
                    resourceable_id: applicant.id,
                    resourceable_type: applicant.class.to_s

                @event_resourceable.reload

                expect(@event_resourceable.attended).to be nil
                expect(response.status).to eq 200
                expected_response = {"errors" => assigns(:event_resourceable).errors.full_messages}
                expect(JSON::parse(response.body)).to match_array expected_response
              end
            end
          end

          context "and resourceable is not in event" do
            it "should return 404" do
              @user.add_role :admin

              event2 = FactoryGirl.create(:calendar_event)
              event2.applicants << applicant
              event2.save
              event2.reload

              resourceable = event2.event_resourceables.first
              attended = resourceable.attended

              put :mark_attended, format: :json,
                  id: event.id,
                  resourceable_id: applicant.id,
                  resourceable_type: applicant.class.to_s

              resourceable.reload

              expect(resourceable.attended).to be attended
              expect(response.status).to eq 404
              expect(response.body).to eq ""
            end
          end
        end

        context "when user has no permissions (is not admin)" do
          it "should return 403" do
            event.applicants << applicant
            event.save
            event.reload

            resourceable = event.event_resourceables.first
            attended = resourceable.attended

            put :mark_attended, format: :json,
                id: event.id,
                resourceable_id: applicant.id,
                resourceable_type: applicant.class.to_s

            expect(resourceable.attended).to be attended

            expect(response.status).to eq 403
            expect(response.body).to eq ""
          end
        end
      end

      context "when user is not logged in" do
        context "format is json" do
          it "should response 401" do
            event.applicants << applicant
            event.save
            event.reload

            put :mark_attended, format: :json,
                id: event.id,
                resourceable_id: applicant.id,
                resourceable_type: applicant.class.to_s
            expect(response.status).to eq 401
            expected_response_body = {error:  I18n.t('devise.failure.unauthenticated') }
            expect(response.body).to eq expected_response_body.to_json
          end
        end
      end
    end
  end # end #mark_attended

  describe "PUT #mark_will_attend" do
    context "when event resourceable is a stage" do
      let(:event) { FactoryGirl.create(:calendar_event) }
      let(:enrollment) { FactoryGirl.create(:enrollment_basis_with_applicant) }
      let(:stage) { enrollment.next_stage }

      before :each do
        event.stages << stage
        event.save
        event.reload

        @event_resourceable = event.event_resourceables.first
      end

      context "when user is logged in" do
        login_executive_without_role

        context "and when user has permissions (is admin)" do
          context "and resourceable is in event" do
            context "and will_attend param is present" do
              let(:put_request_json) {
                put :mark_will_attend, format: :json,
                    id: event.id,
                    resourceable_id: stage.id,
                    resourceable_type: stage.class.to_s,
                    will_attend: will_attend

                @event_resourceable.reload
              }

              let(:put_request_json_expect) {
                expect(@event_resourceable.will_attend).to be will_attend
                expect(response.status).to eq 200
                expect(JSON::parse(response.body)).to match_array JSON::parse(@event_resourceable.to_json)
              }

              let(:will_attend) { [true, false].sample }

              context "set event resourceable will_attend as received (true or false)" do
                it "when user is admin" do
                  @user.add_role :admin
                end

                it "when user is admin for course" do
                  @user.add_role :admin, @event_resourceable.resourceable.enrollment.course
                end

                it "when user is admin for stage" do
                  @user.add_role :admin, stage
                end

                it "when user is admin for stage.class" do
                  @user.add_role :admin, stage.class
                end
              end
            end

            context "when will_attend param is missing return error (will_attend required)" do
              let(:put_request_json) {
                put :mark_will_attend, format: :json,
                    id: event.id,
                    resourceable_id: stage.id,
                    resourceable_type: stage.class.to_s
                # note will_attend is not present in request, I am testing exactly that.

                @event_resourceable.reload
              }

              let(:put_request_json_expect) {
                expect(@event_resourceable.will_attend).to be nil
                expect(response.status).to eq 200
                expected_response = {"errors" => assigns(:event_resourceable).errors.full_messages}
                expect(JSON::parse(response.body)).to match_array expected_response
              }

              it "when user is admin" do
                @user.add_role :admin
              end
            end

            after :each do
              put_request_json
              put_request_json_expect
            end
          end # end resourceable is in event

          context "and resourceable is not in event" do
            # acá se prueba que la etapa stage2 no esté en el evento event
            # es decir, stage2 está en el evento event2, no en el evento event
            let(:enrollment2) { FactoryGirl.create(:enrollment_basis_with_applicant) }
            let(:stage2) { enrollment.next_stage }

            it "should return 404" do
              @user.add_role :admin

              event2 = FactoryGirl.create(:calendar_event)
              event2.stages << stage2
              event2.save
              event2.reload

              resourceable = event2.event_resourceables.first
              will_attend = resourceable.will_attend

              put :mark_will_attend, format: :json,
                  id: event.id,
                  resourceable_id: stage2.id,
                  resourceable_type: stage2.class.to_s

              resourceable.reload

              expect(resourceable.will_attend).to be will_attend
              expect(response.status).to eq 404
              expect(response.body).to eq ""
            end
          end

          context "when user has no permissions (is not admin)" do
            it "should response 403" do
              resourceable = event.event_resourceables.first
              will_attend = resourceable.will_attend

              put :mark_will_attend, format: :json,
                  id: event.id,
                  resourceable_id: stage.id,
                  resourceable_type: stage.class.to_s

              expect(resourceable.will_attend).to be will_attend

              expect(response.status).to eq 403
              expect(response.body).to eq ""
            end
          end
        end

      end

      context "when user is not logged in" do
        context "format is json" do
          it "should response 401" do
            event.stages << stage
            event.save
            event.reload

            put :mark_will_attend, format: :json,
                id: event.id,
                resourceable_id: stage.id,
                resourceable_type: stage.class.to_s
            expect(response.status).to eq 401
            expected_response_body = {error:  I18n.t('devise.failure.unauthenticated') }
            expect(response.body).to eq expected_response_body.to_json
          end
        end
      end
    end

    context "when event resourceable is an applicant" do
      let(:event) { FactoryGirl.create(:calendar_event) }
      let(:enrollment) { FactoryGirl.create(:enrollment_basis_with_applicant) }
      let(:applicant) { enrollment.applicant }

      before :each do
        event.applicants << applicant
        event.save
        event.reload

        @event_resourceable = event.event_resourceables.first
      end

      context "when user is logged in" do
        login_executive_without_role

        context "and when user has permissions (is admin)" do
          context "and resourceable is in event" do
            let(:put_request_json) {
              put :mark_will_attend, format: :json,
                  id: event.id,
                  resourceable_id: applicant.id,
                  resourceable_type: applicant.class.to_s,
                  will_attend: will_attend

              @event_resourceable.reload
            }

            let(:put_request_json_expect) {
              expect(@event_resourceable.will_attend).to be will_attend
              expect(response.status).to eq 200
              expect(JSON::parse(response.body)).to match_array JSON::parse(@event_resourceable.to_json)
            }

            let(:will_attend) { [true, false].sample }

            context "set event resourceable will_attend as received (true or false)" do
              it "when user is admin" do
                @user.add_role :admin
              end

              it "when user is admin for Applicant::Base" do
                @user.add_role :admin, Applicant::Base
              end
            end

            context "when will_attend param is missing return error (will_attend required)" do
              let(:put_request_json) {
                put :mark_will_attend, format: :json,
                    id: event.id,
                    resourceable_id: applicant.id,
                    resourceable_type: applicant.class.to_s
                # note will_attend param is not present, I am testing exactly that

                @event_resourceable.reload
              }

              let(:put_request_json_expect) {
                expect(@event_resourceable.will_attend).to be nil
                expect(response.status).to eq 200
                expected_response = {"errors" => assigns(:event_resourceable).errors.full_messages}
                expect(JSON::parse(response.body)).to match_array expected_response
              }

              it "when user is admin" do
                @user.add_role :admin
              end
            end

            after :each do
              put_request_json
              put_request_json_expect
            end
          end

          context "and resourceable is not in event" do
            # acá se prueba que el postulante applicant2 no esté en el evento event
            # es decir, applicant2 está en el evento event2, no en el evento event
            let(:enrollment2) { FactoryGirl.create(:enrollment_basis_with_applicant) }
            let(:applicant2) { enrollment2.applicant }
            it "should return 404" do
              @user.add_role :admin

              event2 = FactoryGirl.create(:calendar_event)
              event2.applicants << applicant2
              event2.save
              event2.reload

              resourceable = event2.event_resourceables.first
              will_attend = resourceable.will_attend

              put :mark_will_attend, format: :json,
                  id: event.id,
                  resourceable_id: applicant2.id,
                  resourceable_type: applicant2.class.to_s

              resourceable.reload

              expect(resourceable.will_attend).to be will_attend
              expect(response.status).to eq 404
              expect(response.body).to eq ""
            end
          end
        end

        context "when user has no permissions (is not admin)" do
          it "should return 403" do
            event.applicants << applicant
            event.save
            event.reload

            resourceable = event.event_resourceables.first
            will_attend = resourceable.will_attend

            put :mark_will_attend, format: :json,
                id: event.id,
                resourceable_id: applicant.id,
                resourceable_type: applicant.class.to_s

            expect(resourceable.will_attend).to be will_attend

            expect(response.status).to eq 403
            expect(response.body).to eq ""
          end
        end
      end

      context "when user is not logged in" do
        context "format is json" do
          it "should response 401" do
            event.applicants << applicant
            event.save
            event.reload

            put :mark_will_attend, format: :json,
                id: event.id,
                resourceable_id: applicant.id,
                resourceable_type: applicant.class.to_s
            expect(response.status).to eq 401
            expected_response_body = {error:  I18n.t('devise.failure.unauthenticated') }
            expect(response.body).to eq expected_response_body.to_json
          end
        end
      end
    end
  end # end mark_will_attend

  describe "#confirm_will_attend" do
    context "when event resourceable is a stage" do
      let(:event) { FactoryGirl.create(:calendar_event) }
      let(:enrollment) { FactoryGirl.create(:enrollment_basis_with_applicant) }
      let(:stage) { enrollment.next_stage }

      context "when token is present" do
        # this context is when event_resourceable (enrollment) do click the link
        # in the email. Format is HTML
        context "and when token is valid" do
          let(:token) {
            token = Digest::MD5.new
            token << stage.enrollment.applicant.email
            token = "#{token}-#{stage.enrollment.id}"
            token
          }

          it "should mark as will_attend and redirect to a successful view" do
            event.stages << stage
            event.save
            event.reload

            event_resourceable = event.event_resourceables.first

            get :confirm_will_attend,
                id: event.id,
                resourceable_id: stage.id,
                resourceable_type: stage.class.to_s,
                will_attend: true,
                token: token

            event_resourceable.reload

            expect(event_resourceable.will_attend).to be true
            expect(response).to redirect_to marked_will_attend_calendar_event_path(id: event.to_param, resourceable_id: stage.id, resourceable_type: stage.class.to_s)
          end
        end

        context "and when token is invalid" do
          let(:token) { "invalid-token" }

          it "should response 403" do
            event.stages << stage
            event.save
            event.reload

            resourceable = event.event_resourceables.first
            will_attend = resourceable.will_attend

            get :confirm_will_attend,
                id: event.id,
                resourceable_id: stage.id,
                resourceable_type: stage.class.to_s,
                will_attend: !will_attend, # mando lo opuesto, para verificar después que no cambió
                token: token

            expect(resourceable.will_attend).to be will_attend

            expect(response.status).to eq 403
            expect(response.body).to eq ""
          end
        end
      end
    end

    context "when event resourceable is an applicant" do
      let(:event) { FactoryGirl.create(:calendar_event) }
      let(:enrollment) { FactoryGirl.create(:enrollment_basis_with_applicant) }
      let(:applicant) { enrollment.applicant }

      context "when token is present" do
        # this context is when event_resourceable (enrollment) do click the link
        # in the email. Format is HTML
        context "and when token is valid" do
          let(:token) {
            token = Digest::MD5.new
            token << applicant.email
            token = "#{token}-#{applicant.id}"
            token
          }

          it "should mark as will_attend and redirect to a successful view" do
            event.applicants << applicant
            event.save
            event.reload

            event_resourceable = event.event_resourceables.first

            get :confirm_will_attend,
                id: event.id,
                resourceable_id: applicant.id,
                resourceable_type: applicant.class.to_s,
                will_attend: true,
                token: token

            event_resourceable.reload

            expect(event_resourceable.will_attend).to be true
            expect(response).to redirect_to marked_will_attend_calendar_event_path(id: event.to_param, resourceable_id: applicant.id, resourceable_type: applicant.class.to_s)
          end
        end

        context "and when token is invalid" do
          let(:token) { "invalid-token" }

          it "should response 403" do
            event.applicants << applicant
            event.save
            event.reload

            resourceable = event.event_resourceables.first
            will_attend = resourceable.will_attend

            get :confirm_will_attend,
                id: event.id,
                resourceable_id: applicant.id,
                resourceable_type: applicant.class.to_s,
                will_attend: !will_attend, # mando lo opuesto, para verificar después que no cambió
                token: token

            expect(resourceable.will_attend).to be will_attend

            expect(response.status).to eq 403
            expect(response.body).to eq ""
          end
        end
      end
    end
  end
end
