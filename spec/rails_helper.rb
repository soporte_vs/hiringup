# coding: utf-8
## Rspec helper
ENV["RAILS_ENV"] ||= 'test'
require 'spec_helper'
require File.expand_path("../../config/environment", __FILE__)
require 'rspec/rails'
require 'devise'
require 'shoulda-matchers'
require 'ffaker'
require 'capybara/rspec'
require 'capybara/rails'
require 'rspec/retry'
require 'database_rewinder'
require "json_matchers/rspec"
require 'webmock/rspec'

WebMock.allow_net_connect!

#Coverage for test
if ENV['RAILS_ENV'] == 'test'
  require 'simplecov'
  SimpleCov.start 'rails'
  puts "required simplecov"
end


ActiveRecord::Migration.maintain_test_schema!

RSpec.configure do |config|

  config.tty = true

  config.verbose_retry = true

  config.default_retry_count = 3

  config.exceptions_to_retry = []

  config.fixture_path = "#{::Rails.root}/spec/fixtures"

  config.infer_spec_type_from_file_location!
  config.include ChosenSelect
  # implementar la autentificacion de devise
  config.include Devise::TestHelpers, type: :controller
  config.include Devise::TestHelpers, type: :view

  # Permite loguear directamente a un usuario cuando se esta en un caso de prueba
  # EndToEnd (Test case Request or features)
  config.include Warden::Test::Helpers
  Warden.test_mode!

  # Helpers de Logueo en casos de pruebas
  config.extend ControllerMacros
  config.extend RequestMacros


  # Capybara y selenium para test case EndToEnd
  config.include Capybara::DSL
  Capybara.register_driver :selenium do |app|
    Capybara::Selenium::Driver.new(app, :browser => :chrome)
  end

  Capybara.configure do |config|
    Capybara.server_port = 9887 + ENV['TEST_ENV_NUMBER'].to_i
  end

  #Headless env var for CI
  if ENV['HEADLESS'] == 'true'
    require 'headless'
    display = 100 + Random.rand(900)
    headless = Headless.new(display: display, destroy_at_exit: true)
    headless.start
  end

  # Esta configuración permite que los datos en cada ejemplo de prueba
  # sea visible tanto dentro de la definición del mismo como para selenium
  # cuando abre en el navegador (Ej: abre el navaegador y consulta a un Ws,
  # sin esto el ws no encuentra datos en la BD)
  config.use_transactional_fixtures = false
  #New database Cleaner
  config.before :suite do
    DatabaseRewinder.clean_all
    # or
    # DatabaseRewinder.clean_with :any_arg_that_would_be_actually_ignored_anyway
  end

  config.after :each do
    DatabaseRewinder.clean
  end

  # Stub Callback ElasticSearch
  config.before :each do |args|
    # En caso de que el test no requiera elasticsearch se hace un mock para que no se indexen
    # documentos y sea más eficiente
    unless args.metadata[:elasticsearch]
      allow_any_instance_of(Applicant::Base).to receive(:index_document).and_return({})
      allow_any_instance_of(Course).to receive(:index_document).and_return({})
      allow_any_instance_of(Enrollment::Base).to receive(:index_document).and_return({})
      allow_any_instance_of(HiringRequest::Record).to receive(:index_document).and_return({})
      allow_any_instance_of(CoursePostulation).to receive(:index_document).and_return({})

      allow_any_instance_of(Applicant::Base).to receive(:delete_document).and_return({})
      allow_any_instance_of(Course).to receive(:delete_document).and_return({})
      allow_any_instance_of(Enrollment::Base).to receive(:delete_document).and_return({})
      allow_any_instance_of(HiringRequest::Record).to receive(:delete_document).and_return({})
      allow_any_instance_of(CoursePostulation).to receive(:delete_document).and_return({})
    end
  end

  # Strategy manage Elasticsearch
  config.around :each, elasticsearch: true do |example|
    # Isolate Index Course
    index_name_course = Course.__elasticsearch__.index_name.clone
    Course.__elasticsearch__.index_name "#{index_name_course}-#{example.metadata[:block].object_id}"

    # Isolate Index Applicant Base
    index_name_applicant_base = Applicant::Base.__elasticsearch__.index_name.clone
    Applicant::Base.__elasticsearch__.index_name "#{index_name_applicant_base}-#{example.metadata[:block].object_id}"

    # Isolate Index Applicant Catched
    index_name_applicant_catched = Applicant::Catched.__elasticsearch__.index_name.clone
    Applicant::Catched.__elasticsearch__.index_name "#{index_name_applicant_catched}-#{example.metadata[:block].object_id}"

    # Isolate Index Applicant External
    index_name_applicant_external = Applicant::External.__elasticsearch__.index_name.clone
    Applicant::External.__elasticsearch__.index_name "#{index_name_applicant_external}-#{example.metadata[:block].object_id}"

    # Isolate Index Laborum Applicant
    index_name_laborum_applicant = Laborum::Applicant.__elasticsearch__.index_name.clone
    Laborum::Applicant.__elasticsearch__.index_name "#{index_name_laborum_applicant}-#{example.metadata[:block].object_id}"

    # Isolate Index Minisite Applicant
    index_name_minisite_applicant = Minisite::Applicant.__elasticsearch__.index_name.clone
    Minisite::Applicant.__elasticsearch__.index_name "#{index_name_minisite_applicant}-#{example.metadata[:block].object_id}"

    # Isolate Index Applicant Employee
    index_name_company_applicant_employee = Company::ApplicantEmployee.__elasticsearch__.index_name.clone
    Company::ApplicantEmployee.__elasticsearch__.index_name "#{index_name_company_applicant_employee}-#{example.metadata[:block].object_id}"

    # Isolate Index Enrollment::Base
    index_name_enrollment_base = Enrollment::Base.__elasticsearch__.index_name.clone
    Enrollment::Base.__elasticsearch__.index_name "#{index_name_enrollment_base}-#{example.metadata[:block].object_id}"

    # Isolate Index HriringRequest::Record
    index_name_hiring_request_record_base = HiringRequest::Record.__elasticsearch__.index_name.clone
    HiringRequest::Record.__elasticsearch__.index_name "#{index_name_hiring_request_record_base}-#{example.metadata[:block].object_id}"

    # Isolate Index CoursePostulation
    index_name_course_postulation = CoursePostulation.__elasticsearch__.index_name.clone
    CoursePostulation.__elasticsearch__.index_name "#{index_name_course_postulation}-#{example.metadata[:block].object_id}"

    # UP ELASTICSEARCH INDEX
    models = [Applicant::Base, Course, Enrollment::Base, HiringRequest::Record, CoursePostulation]
    models.each do |model|
      model.__elasticsearch__.create_index!
    end
    example.run

    # DOWN ELASTICSEARCH INDEX
    models.each do |model|
      target_index = model.__elasticsearch__.index_name
      if model.__elasticsearch__.index_exists?(index: target_index)
        model.__elasticsearch__.delete_index!
      end
    end

    # Return originals index_names
    Course.__elasticsearch__.index_name index_name_course
    Applicant::Base.__elasticsearch__.index_name index_name_applicant_base
    Applicant::Catched.__elasticsearch__.index_name index_name_applicant_catched
    Applicant::External.__elasticsearch__.index_name index_name_applicant_external
    Minisite::Applicant.__elasticsearch__.index_name index_name_minisite_applicant
    Company::ApplicantEmployee.__elasticsearch__.index_name index_name_company_applicant_employee
    Laborum::Applicant.__elasticsearch__.index_name index_name_laborum_applicant
    Enrollment::Base.__elasticsearch__.index_name index_name_enrollment_base
    HiringRequest::Record.__elasticsearch__.index_name index_name_hiring_request_record_base
    CoursePostulation.__elasticsearch__.index_name index_name_course_postulation
  end

  #Estrategy CarrierWave
  config.around :each do |example|
    example_id = "#{example.metadata[:block].object_id}"

    CarrierWave::Uploader::Base.descendants.each do |klass|
      next if klass.anonymous?

      klass.send(:attr_accessor, 'example_id')
      klass.instance_variable_set("@example_id", example_id)

      klass.class_eval do

        def cache_dir
          scope = self.class.instance_variable_get('@example_id')
          "#{Rails.root}/public/#{scope}/uploads"
        end

        def store_dir
          scope = self.class.instance_variable_get('@example_id')
          "#{Rails.root}/public/#{scope}/uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
        end
      end
    end
    example.run

    FileUtils.rm_rf(Dir["#{Rails.root}/public/#{example_id}"])
  end
end
