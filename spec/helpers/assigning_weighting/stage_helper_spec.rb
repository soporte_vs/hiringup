require 'rails_helper'

RSpec.describe AssigningWeighting::StageHelper, type: :helper do
  context 'method html_preview' do
    it 'should render link to AssigningWeighting' do
      assigning_weighting_stage = FactoryGirl.create(:assigning_weighting_stage)
      @result = AssigningWeighting::StageHelper.html_preview(view, assigning_weighting_stage)
      @link = view.link_to(
        view.t('assigning_weighting.stage.title'),
        view.polymorphic_path([assigning_weighting_stage.enrollment.course, assigning_weighting_stage]),
        class: 'btn-link'
      )
    end

    after :each do
      html = <<-HTML
      <div>
        #{@link}
      </div>
      HTML
      expect(@result.gsub(' ','')).to eq html.gsub(' ','')
    end
  end
end