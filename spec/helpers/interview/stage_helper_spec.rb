require 'rails_helper'

RSpec.describe Interview::StageHelper, type: :helper do
  context 'method html_preview' do
    it 'should render link to Interview' do
      interview_stage = FactoryGirl.create(:interview_stage)
      @result = Interview::StageHelper.html_preview(view, interview_stage)
      @link = view.link_to(
        view.t('interview.stage.title'),
        view.polymorphic_path([interview_stage.enrollment.course, interview_stage]),
        class: 'btn-link'
      )
    end

    after :each do
      html = <<-HTML
      <div>
        #{@link}
      </div>
      HTML
      expect(@result.gsub(' ','')).to eq html.gsub(' ','')
    end
  end
end