require 'rails_helper'

RSpec.describe Test::StageHelper, type: :helper do
  context 'method html_preview' do
    it 'should render link to Test' do
      test_stage = FactoryGirl.create(:test_stage)
      @result = Test::StageHelper.html_preview(view, test_stage)
      @link = view.link_to(
        view.t('test.stage.title'),
        view.polymorphic_path([test_stage.enrollment.course, test_stage]),
        class: 'btn-link'
      )
    end

    after :each do
      html = <<-HTML
      <div>
        #{@link}
      </div>
      HTML
      expect(@result.gsub(' ','')).to eq html.gsub(' ','')
    end
  end
end