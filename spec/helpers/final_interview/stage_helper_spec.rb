require 'rails_helper'

RSpec.describe FinalInterview::StageHelper, type: :helper do
  context 'method html_preview' do
    it 'should render link to FinalInterview' do
      final_interview_stage = FactoryGirl.create(:final_interview_stage)
      @result = FinalInterview::StageHelper.html_preview(view, final_interview_stage)
      @link = view.link_to(
        view.t('final_interview.stage.title'),
        view.polymorphic_path([final_interview_stage.enrollment.course, final_interview_stage]),
        class: 'scaffold_link __blue'
      )
    end

    after :each do
      html = <<-HTML
      <div>
        #{@link}
      </div>
      HTML
      expect(@result.gsub(' ','')).to eq html.gsub(' ','')
    end
  end
end