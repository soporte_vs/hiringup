require 'rails_helper'

# Specs in this file have access to a helper object that includes
# the Company::BusinessUnitsHelper. For example:
#
# describe Company::BusinessUnitsHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       expect(helper.concat_strings("this","that")).to eq("this that")
#     end
#   end
# end
RSpec.describe DeviseHelper, type: :helper do
  describe "#resource_class" do
    it "should return User class" do
      expect(resource_class).to be User
    end
  end
end
