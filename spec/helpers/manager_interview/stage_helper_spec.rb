require 'rails_helper'

RSpec.describe ManagerInterview::StageHelper, type: :helper do
  context 'method html_preview' do
    it 'should render link to ManagerInterview' do
      manager_interview_stage = FactoryGirl.create(:manager_interview_stage)
      @result = ManagerInterview::StageHelper.html_preview(view, manager_interview_stage)
      @link = view.link_to(
        view.t('manager_interview.stage.title'),
        view.polymorphic_path([manager_interview_stage.enrollment.course, manager_interview_stage]),
        class: 'btn-link'
      )
    end

    after :each do
      html = <<-HTML
      <div>
        #{@link}
      </div>
      HTML
      expect(@result.gsub(' ','')).to eq html.gsub(' ','')
    end
  end
end