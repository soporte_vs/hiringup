require 'rails_helper'

RSpec.describe IntegraSpreadsheet::ReportHelper, type: :helper do


  describe 'applicant_part' do
    let(:enrollment){FactoryGirl.create(:enrollment_basis_with_applicant)}

    context 'when applicant has an enrollment' do

      before :each do
        @applicant_row = helper.applicant_part(enrollment)
      end

      it 'returns an array with the proper size' do
        expect(@applicant_row.length).to eq helper.base_columns.length
      end
    end
  end

  describe 'answers_part' do
    let(:course){FactoryGirl.create(:course)}
    let(:enrollment){FactoryGirl.create(:enrollment_basis_with_applicant)}

    context 'when course has no publications' do
      it 'returns an empty array' do
        expect(helper.answers_part(enrollment.postulation, course.grouped_questions)).to be_empty
      end
    end

    context 'when course has a publication with no questions' do
      let(:minisite_publication){FactoryGirl.create(:minisite_publication,
        course_id: course.id)}
      let(:minisite_postulation){FactoryGirl.create(:minisite_postulation,
        minisite_publication_id: minisite_publication.id)}

      before :each do
        course.enroll!(minisite_postulation.applicant)
        @minisite_enrollment = minisite_postulation.applicant.enrollments.last
      end

      it 'returns an empty array' do
        expect(helper.answers_part(@minisite_enrollment.postulation, course.grouped_questions)).to be_empty
      end
    end

    context 'when course has both types of publications with questions' do
      let(:minisite_publication){FactoryGirl.create(:minisite_publication,
        :with_questions, course_id: course.id)}
      let(:minisite_answers){
        minisite_publication.questions.map do |question|
          FactoryGirl.attributes_for(:minisite_answer, minisite_question_id: question.to_param)
        end
      }
      let(:minisite_postulation){FactoryGirl.create(:minisite_postulation,
        answers_attributes: minisite_answers, minisite_publication_id: minisite_publication.id)}
      let(:trabajando_publication){FactoryGirl.create(:trabajando_publication)}
      let(:trabajando_postulation){FactoryGirl.create(:trabajando_postulation,
        :with_answers, trabajando_publication_id: trabajando_publication.id)}

      let(:total_questions){trabajando_publication.questions.size +
        minisite_publication.questions.size}

      let(:mst_answers_part){helper.answers_part(@minisite_enrollment.postulation, course.grouped_questions)}
      let(:trb_answers_part){helper.answers_part(@trabajando_enrollment.postulation, course.grouped_questions)}

      before :each do
        course.enroll!(minisite_postulation.applicant)
        course.enroll!(trabajando_postulation.applicant)
        @minisite_enrollment = minisite_postulation.applicant.enrollments.last
        @trabajando_enrollment = trabajando_postulation.applicant.enrollments.last
      end

      context 'and applicant applied through minisite' do
        it 'returns an array with a size eq to total amount of questions' do
          expect(mst_answers_part).not_to be_empty
          expect(mst_answers_part.size).to eq total_questions
        end

        it 'returns an array with blanks on unanswered questions' do
          expect(mst_answers_part.select(&:present?).size).to eq minisite_publication.questions.length
        end
      end

      context 'and applicant applied through trabajando publication' do
        it 'returns an array with a size eq to total amount of questions' do
          expect(trb_answers_part).not_to be_empty
          expect(trb_answers_part.size).to eq total_questions
        end

        it 'returns an array with blanks on unanswered questions' do
          expect(trb_answers_part.select(&:present?).size).to eq trabajando_publication.questions.length
        end
      end

      context 'and applicant was invited or has no associated publication' do
        it 'returns an empty array' do
          expect(helper.answers_part(enrollment.postulation, course.grouped_questions)).to be_empty
        end
      end
    end
  end


  describe 'question_headers' do
    let(:course){FactoryGirl.create(:course)}

    context 'when course has no publications' do
      it 'returns an empty array' do
        expect(helper.question_headers(course.grouped_questions)).to be_empty
      end
    end

    context 'when course has a publication with no questions' do
      let(:minisite_publication){FactoryGirl.create(:minisite_publication,
        course_id: course.id)}

      it 'returns an empty array' do
        expect(helper.question_headers(course.grouped_questions)).to be_empty
      end
    end

    context 'when course has both types of publications with questions' do
      let!(:minisite_publication){FactoryGirl.create(:minisite_publication,
        :with_questions, course_id: course.id)}
      let!(:trabajando_publication){FactoryGirl.create(:trabajando_publication)}

      let(:total_questions){trabajando_publication.questions.size +
        minisite_publication.questions.size}

      before :each do
        course.reload
        @header_result = helper.question_headers(course.grouped_questions)
        @minisite_part = @header_result.select{|cell| /Pregunta Minisitio/.match(cell)}
        @trabajando_part = @header_result.select{|cell| /Pregunta Trabajando/.match(cell)}
      end

      it 'returns an array with a size eq to total of questions' do
        expect(@header_result.size).to eq total_questions
      end

      it 'returns an array with minisite questions followed by trabajando questions' do
        expect(@minisite_part.size).to eq minisite_publication.questions.size
        expect(@trabajando_part .size).to eq trabajando_publication.questions.size
      end
    end
  end
end
