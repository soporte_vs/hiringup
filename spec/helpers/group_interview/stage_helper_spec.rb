require 'rails_helper'

RSpec.describe GroupInterview::StageHelper, type: :helper do
  context 'method html_preview' do
    it 'should render link to GroupInterview' do
      group_interview_stage = FactoryGirl.create(:group_interview_stage)
      @result = GroupInterview::StageHelper.html_preview(view, group_interview_stage)
      @link = view.link_to(
        view.t('group_interview.stage.title'),
        view.polymorphic_path([group_interview_stage.enrollment.course, group_interview_stage]),
        class: 'scaffold_link __blue'
      )
    end

    after :each do
      html = <<-HTML
      <div>
        #{@link}
      </div>
      HTML
      expect(@result.gsub(' ','')).to eq html.gsub(' ','')
    end
  end
end