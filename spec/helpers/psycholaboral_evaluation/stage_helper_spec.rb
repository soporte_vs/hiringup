require 'rails_helper'

RSpec.describe PsycholaboralEvaluation::StageHelper, type: :helper do
  context 'method html_preview' do
    it 'should render link to PsycholaboralEvaluation' do
      psycholaboral_evaluation_stage = FactoryGirl.create(:psycholaboral_evaluation_stage)
      @result = PsycholaboralEvaluation::StageHelper.html_preview(view, psycholaboral_evaluation_stage)
      @link = view.link_to(
        view.t('psycholaboral_evaluation.stage.title'),
        view.polymorphic_path([psycholaboral_evaluation_stage.enrollment.course, psycholaboral_evaluation_stage]),
        class: 'scaffold_link __blue'
      )
    end

    after :each do
      html = <<-HTML
      <div>
        #{@link}
      </div>
      HTML
      expect(@result.gsub(' ','')).to eq html.gsub(' ','')
    end
  end
end