require 'rails_helper'

RSpec.describe Promotion::StageHelper, type: :helper do
  context 'method html_preview' do
    it 'should render link to Promotion' do
      promotion_stage = FactoryGirl.create(:promotion_stage)
      @result = Promotion::StageHelper.html_preview(view, promotion_stage)
      @link = view.link_to(
        view.t('promotion.stage.title'),
        view.polymorphic_path([promotion_stage.enrollment.course, promotion_stage]),
        class: 'btn-link'
      )
    end

    after :each do
      html = <<-HTML
      <div>
        #{@link}
      </div>
      HTML
      expect(@result.gsub(' ','')).to eq html.gsub(' ','')
    end
  end
end