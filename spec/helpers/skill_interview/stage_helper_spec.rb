require 'rails_helper'

RSpec.describe SkillInterview::StageHelper, type: :helper do
  context 'method html_preview' do
    it 'should render link to SkillInterview' do
      skill_interview_stage = FactoryGirl.create(:skill_interview_stage)
      @result = SkillInterview::StageHelper.html_preview(view, skill_interview_stage)
      @link = view.link_to(
        view.t('skill_interview.stage.title'),
        view.polymorphic_path([skill_interview_stage.enrollment.course, skill_interview_stage]),
        class: 'btn-link'
      )
    end

    after :each do
      html = <<-HTML
      <div>
        #{@link}
      </div>
      HTML
      expect(@result.gsub(' ','')).to eq html.gsub(' ','')
    end
  end
end