require 'rails_helper'

RSpec.describe TechnicalTest::StageHelper, type: :helper do
  context 'method html_preview' do
    it 'should render link to TechnicalTest' do
      technical_test_stage = FactoryGirl.create(:technical_test_stage)
      @result = TechnicalTest::StageHelper.html_preview(view, technical_test_stage)
      @link = view.link_to(
        view.t('technical_test.stage.title'),
        view.polymorphic_path([technical_test_stage.enrollment.course, technical_test_stage]),
        class: 'scaffold_link __blue'
      )
    end

    after :each do
      html = <<-HTML
      <div>
        #{@link}
      </div>
      HTML
      expect(@result.gsub(' ','')).to eq html.gsub(' ','')
    end
  end
end