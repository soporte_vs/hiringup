require "rails_helper"

RSpec.describe Tag::BaseTagsController, type: :routing do
  describe "routing" do

    it "routes to #index on course_tag" do
      expect(:get => "/app/tag/course_tags").to route_to("tag/base_tags#index", type: Tag::CourseTag.to_s)
    end

    it "routes to #new" do
      expect(:get => "/app/tag/course_tags/new").to route_to("tag/base_tags#new", type: Tag::CourseTag.to_s)
    end

    it "routes to #show" do
      expect(:get => "/app/tag/course_tags/1").to route_to("tag/base_tags#show", :id => "1", type: Tag::CourseTag.to_s)
    end

    it "routes to #edit" do
      expect(:get => "/app/tag/course_tags/1/edit").to route_to("tag/base_tags#edit", :id => "1", type: Tag::CourseTag.to_s)
    end

    it "routes to #create" do
      expect(:post => "/app/tag/course_tags").to route_to("tag/base_tags#create", type: Tag::CourseTag.to_s)
    end

    it "routes to #update" do
      expect(:put => "/app/tag/course_tags/1").to route_to("tag/base_tags#update", :id => "1", type: Tag::CourseTag.to_s)
    end

    it "routes to #destroy" do
      expect(:delete => "/app/tag/course_tags/1").to route_to("tag/base_tags#destroy", :id => "1", type: Tag::CourseTag.to_s)
    end

  end
end
