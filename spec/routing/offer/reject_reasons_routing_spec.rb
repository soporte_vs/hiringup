require "rails_helper"

RSpec.describe Offer::RejectReasonsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/app/offer/reject_reasons").to route_to("offer/reject_reasons#index")
    end

    it "routes to #new" do
      expect(:get => "/app/offer/reject_reasons/new").to route_to("offer/reject_reasons#new")
    end

    it "routes to #show" do
      expect(:get => "/app/offer/reject_reasons/1").to route_to("offer/reject_reasons#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/app/offer/reject_reasons/1/edit").to route_to("offer/reject_reasons#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/app/offer/reject_reasons").to route_to("offer/reject_reasons#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/app/offer/reject_reasons/1").to route_to("offer/reject_reasons#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/app/offer/reject_reasons/1").to route_to("offer/reject_reasons#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/app/offer/reject_reasons/1").to route_to("offer/reject_reasons#destroy", :id => "1")
    end

  end
end
