require "rails_helper"

RSpec.describe Enrollment::DiscardReasonsController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/app/enrollment/discard_reasons").to route_to("enrollment/discard_reasons#index")
    end

    it "routes to #new" do
      expect(:get => "/app/enrollment/discard_reasons/new").to route_to("enrollment/discard_reasons#new")
    end

    it "routes to #show" do
      expect(:get => "/app/enrollment/discard_reasons/1").to route_to("enrollment/discard_reasons#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/app/enrollment/discard_reasons/1/edit").to route_to("enrollment/discard_reasons#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/app/enrollment/discard_reasons").to route_to("enrollment/discard_reasons#create")
    end

    it "routes to #update" do
      expect(:put => "/app/enrollment/discard_reasons/1").to route_to("enrollment/discard_reasons#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/app/enrollment/discard_reasons/1").to route_to("enrollment/discard_reasons#destroy", :id => "1")
    end

  end
end
