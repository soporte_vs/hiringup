require "rails_helper"

RSpec.describe Trabajando::JobTypesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/app/trabajando/job_types").to route_to("trabajando/job_types#index")
    end

    it "does not route to #new" do
      expect(:get => "/app/trabajando/job_types/new").to_not route_to("trabajando/job_types#new")
    end

    it "routes to #show" do
      expect(:get => "/app/trabajando/job_types/1").to route_to("trabajando/job_types#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/app/trabajando/job_types/1/edit").to route_to("trabajando/job_types#edit", :id => "1")
    end

    it "does not route to #create" do
      expect(:post => "/app/trabajando/job_types").to_not route_to("trabajando/job_types#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/app/trabajando/job_types/1").to route_to("trabajando/job_types#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/app/trabajando/job_types/1").to route_to("trabajando/job_types#update", :id => "1")
    end

    it "does not route to #destroy" do
      expect(:delete => "/app/trabajando/job_types/1").to_not route_to("trabajando/job_types#destroy", :id => "1")
    end

  end
end
