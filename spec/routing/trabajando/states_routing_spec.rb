require "rails_helper"

RSpec.describe Trabajando::StatesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "app/trabajando/states").to route_to("trabajando/states#index")
    end

    it "routes to #new" do
      expect(:get => "app/trabajando/states/new").not_to route_to("trabajando/states#new")
    end

    it "routes to #show" do
      expect(:get => "app/trabajando/states/1").to route_to("trabajando/states#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "app/trabajando/states/1/edit").to route_to("trabajando/states#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "app/trabajando/states").not_to route_to("trabajando/states#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "app/trabajando/states/1").to route_to("trabajando/states#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "app/trabajando/states/1").to route_to("trabajando/states#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "app/trabajando/states/1").not_to route_to("trabajando/states#destroy", :id => "1")
    end

  end
end
