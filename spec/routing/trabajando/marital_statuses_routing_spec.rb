require "rails_helper"

RSpec.describe Trabajando::MaritalStatusesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "app/trabajando/marital_statuses").to route_to("trabajando/marital_statuses#index")
    end

    it "routes to #new" do
      expect(:get => "app/trabajando/marital_statuses/new").to_not route_to("trabajando/marital_statuses#new")
    end

    it "routes to #show" do
      expect(:get => "app/trabajando/marital_statuses/1").to route_to("trabajando/marital_statuses#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "app/trabajando/marital_statuses/1/edit").to route_to("trabajando/marital_statuses#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "app/trabajando/marital_statuses").to_not route_to("trabajando/marital_statuses#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "app/trabajando/marital_statuses/1").to route_to("trabajando/marital_statuses#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "app/trabajando/marital_statuses/1").to route_to("trabajando/marital_statuses#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "app/trabajando/marital_statuses/1").to_not route_to("trabajando/marital_statuses#destroy", :id => "1")
    end

  end
end
