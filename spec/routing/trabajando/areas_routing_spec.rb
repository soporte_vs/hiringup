require "rails_helper"

RSpec.describe Trabajando::AreasController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "app/trabajando/areas").to route_to("trabajando/areas#index")
    end

    it "does not route to #new" do
      expect(:get => "app/trabajando/areas/new").to_not route_to("trabajando/areas#new")
    end

    it "routes to #show" do
      expect(:get => "app/trabajando/areas/1").to route_to("trabajando/areas#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "app/trabajando/areas/1/edit").to route_to("trabajando/areas#edit", :id => "1")
    end

    it "does not route to #create" do
      expect(:post => "app/trabajando/areas").to_not route_to("trabajando/areas#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "app/trabajando/areas/1").to route_to("trabajando/areas#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "app/trabajando/areas/1").to route_to("trabajando/areas#update", :id => "1")
    end

    it "does not route to #destroy" do
      expect(:delete => "app/trabajando/areas/1").to_not route_to("trabajando/areas#destroy", :id => "1")
    end

  end
end
