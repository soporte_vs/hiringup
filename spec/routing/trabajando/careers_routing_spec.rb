require "rails_helper"

RSpec.describe Trabajando::CareersController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/app/trabajando/careers").to route_to("trabajando/careers#index")
    end

    it "routes to #new" do
      expect(:get => "/app/trabajando/careers/new").to_not route_to("trabajando/careers#new")
    end

    it "routes to #show" do
      expect(:get => "/app/trabajando/careers/1").to route_to("trabajando/careers#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/app/trabajando/careers/1/edit").to route_to("trabajando/careers#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/app/trabajando/careers").to_not route_to("trabajando/careers#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/app/trabajando/careers/1").to route_to("trabajando/careers#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/app/trabajando/careers/1").to route_to("trabajando/careers#update", :id => "1")
    end
  end
end
