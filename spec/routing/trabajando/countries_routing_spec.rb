require "rails_helper"

RSpec.describe Trabajando::CountriesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "app/trabajando/countries").to route_to("trabajando/countries#index")
    end

    it "routes to #new" do
      expect(:get => "app/trabajando/countries/new").not_to route_to("trabajando/countries#new")
    end

    it "routes to #show" do
      expect(:get => "app/trabajando/countries/1").to route_to("trabajando/countries#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "app/trabajando/countries/1/edit").to route_to("trabajando/countries#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "app/trabajando/countries").not_to route_to("trabajando/countries#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "app/trabajando/countries/1").to route_to("trabajando/countries#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "app/trabajando/countries/1").to route_to("trabajando/countries#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "app/trabajando/countries/1").not_to route_to("trabajando/countries#destroy", :id => "1")
    end

  end
end
