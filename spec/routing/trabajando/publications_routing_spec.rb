require "rails_helper"

RSpec.describe Trabajando::PublicationsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(get: "/app/courses/1/trabajando/publications").to route_to("trabajando/publications#index", id: '1')
    end

    it "routes to #new" do
      expect(get: "/app/courses/1/trabajando/publications/new").to route_to("trabajando/publications#new", id: '1')
    end

    it "routes to #show" do
      expect(get: "/app/courses/1/trabajando/publications/1").to route_to("trabajando/publications#show", id: '1', publication_id: '1')
    end

    it "routes to #edit" do
      expect(get: "/app/courses/1/trabajando/publications/1/edit").to route_to("trabajando/publications#edit", id: '1', publication_id: '1')
    end

    it "routes to #create" do
      expect(post: "/app/courses/1/trabajando/publications").to route_to("trabajando/publications#create", id: '1')
    end

    it "routes to #update" do
      expect(put: "/app/courses/1/trabajando/publications/1").to route_to("trabajando/publications#update", id: '1', publication_id: '1')
    end

    it "routes to #destroy" do
      expect(delete: "/app/courses/1/trabajando/publications/1").to_not route_to("trabajando/publications#destroy", id: '1', publication_id: '1')
    end

  end
end
