require "rails_helper"

RSpec.describe Trabajando::SoftwareLevelsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "app/trabajando/software_levels").to route_to("trabajando/software_levels#index")
    end

    it "does not route to #new" do
      expect(:get => "app/trabajando/software_levels/new").to_not route_to("trabajando/software_levels#new")
    end

    it "routes to #show" do
      expect(:get => "app/trabajando/software_levels/1").to route_to("trabajando/software_levels#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "app/trabajando/software_levels/1/edit").to route_to("trabajando/software_levels#edit", :id => "1")
    end

    it "does not route to #create" do
      expect(:post => "app/trabajando/software_levels").to_not route_to("trabajando/software_levels#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "app/trabajando/software_levels/1").to route_to("trabajando/software_levels#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "app/trabajando/software_levels/1").to route_to("trabajando/software_levels#update", :id => "1")
    end

    it "does not route to #destroy" do
      expect(:delete => "app/trabajando/software_levels/1").to_not route_to("trabajando/software_levels#destroy", :id => "1")
    end

  end
end
