require "rails_helper"

RSpec.describe Trabajando::EducationStatesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "app/trabajando/education_states").to route_to("trabajando/education_states#index")
    end

    it "does not route to #new" do
      expect(:get => "app/trabajando/education_states/new").to_not route_to("trabajando/education_states#new")
    end

    it "routes to #show" do
      expect(:get => "app/trabajando/education_states/1").to route_to("trabajando/education_states#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "app/trabajando/education_states/1/edit").to route_to("trabajando/education_states#edit", :id => "1")
    end

    it "does not route to #create" do
      expect(:post => "app/trabajando/education_states").to_not route_to("trabajando/education_states#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "app/trabajando/education_states/1").to route_to("trabajando/education_states#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "app/trabajando/education_states/1").to route_to("trabajando/education_states#update", :id => "1")
    end

    it "does not route to #destroy" do
      expect(:delete => "app/trabajando/education_states/1").to_not route_to("trabajando/education_states#destroy", :id => "1")
    end

  end
end
