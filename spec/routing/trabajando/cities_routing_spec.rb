require "rails_helper"

RSpec.describe Trabajando::CitiesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "app/trabajando/cities").to route_to("trabajando/cities#index")
    end

    it "routes to #new" do
      expect(:get => "app/trabajando/cities/new").not_to route_to("trabajando/cities#new")
    end

    it "routes to #show" do
      expect(:get => "app/trabajando/cities/1").to route_to("trabajando/cities#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "app/trabajando/cities/1/edit").to route_to("trabajando/cities#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "app/trabajando/cities").not_to route_to("trabajando/cities#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "app/trabajando/cities/1").to route_to("trabajando/cities#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "app/trabajando/cities/1").to route_to("trabajando/cities#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "app/trabajando/cities/1").not_to route_to("trabajando/cities#destroy", :id => "1")
    end

  end
end
