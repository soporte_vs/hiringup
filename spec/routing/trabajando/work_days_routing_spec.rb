require "rails_helper"

RSpec.describe Trabajando::WorkDaysController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/app/trabajando/work_days").to route_to("trabajando/work_days#index")
    end

    it "routes to #new" do
      expect(:get => "/app/trabajando/work_days/new").not_to route_to("trabajando/work_days#new")
    end

    it "routes to #show" do
      expect(:get => "/app/trabajando/work_days/1").to route_to("trabajando/work_days#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/app/trabajando/work_days/1/edit").to route_to("trabajando/work_days#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/app/trabajando/work_days").not_to route_to("trabajando/work_days#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/app/trabajando/work_days/1").to route_to("trabajando/work_days#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/app/trabajando/work_days/1").to route_to("trabajando/work_days#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/app/trabajando/work_days/1").not_to route_to("trabajando/work_days#destroy", :id => "1")
    end

  end
end
