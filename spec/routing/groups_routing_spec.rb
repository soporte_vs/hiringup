require "rails_helper"

RSpec.describe GroupsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/app/groups").to route_to("groups#index")
    end

    it "routes to #new" do
      expect(:get => "/app/groups/new").to route_to("groups#new")
    end

    it "routes to #show" do
      expect(:get => "/app/groups/1").to route_to("groups#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/app/groups/1/edit").to route_to("groups#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/app/groups").to route_to("groups#create")
    end

    it "routes to #update" do
      expect(:put => "/app/groups/1").to route_to("groups#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/app/groups/1").to route_to("groups#destroy", :id => "1")
    end

  end
end
