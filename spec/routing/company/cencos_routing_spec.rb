require "rails_helper"

RSpec.describe Company::CencosController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/app/company/cencos").to route_to("company/cencos#index")
    end

    it "routes to #new" do
      expect(:get => "/app/company/cencos/new").to route_to("company/cencos#new")
    end

    it "routes to #show" do
      expect(:get => "/app/company/cencos/1").to route_to("company/cencos#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/app/company/cencos/1/edit").to route_to("company/cencos#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/app/company/cencos").to route_to("company/cencos#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/app/company/cencos/1").to route_to("company/cencos#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/app/company/cencos/1").to route_to("company/cencos#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/app/company/cencos/1").to route_to("company/cencos#destroy", :id => "1")
    end

  end
end
