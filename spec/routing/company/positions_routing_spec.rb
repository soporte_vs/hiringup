require "rails_helper"

RSpec.describe Company::PositionsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/app/company/positions").to route_to("company/positions#index")
    end

    it "routes to #new" do
      expect(:get => "/app/company/positions/new").to route_to("company/positions#new")
    end

    it "routes to #show" do
      expect(:get => "/app/company/positions/1").to route_to("company/positions#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/app/company/positions/1/edit").to route_to("company/positions#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/app/company/positions").to route_to("company/positions#create")
    end

    it "routes to #update" do
      expect(:put => "/app/company/positions/1").to route_to("company/positions#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/app/company/positions/1").to route_to("company/positions#destroy", :id => "1")
    end

  end
end
