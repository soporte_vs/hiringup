require "rails_helper"

RSpec.describe Company::VacancyRequestReasonsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/app/company/vacancy_request_reasons").to route_to("company/vacancy_request_reasons#index")
    end

    it "routes to #new" do
      expect(:get => "/app/company/vacancy_request_reasons/new").to route_to("company/vacancy_request_reasons#new")
    end

    it "routes to #show" do
      expect(:get => "/app/company/vacancy_request_reasons/1").to route_to("company/vacancy_request_reasons#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/app/company/vacancy_request_reasons/1/edit").to route_to("company/vacancy_request_reasons#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/app/company/vacancy_request_reasons").to route_to("company/vacancy_request_reasons#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/app/company/vacancy_request_reasons/1").to route_to("company/vacancy_request_reasons#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/app/company/vacancy_request_reasons/1").to route_to("company/vacancy_request_reasons#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/app/company/vacancy_request_reasons/1").to route_to("company/vacancy_request_reasons#destroy", :id => "1")
    end

  end
end
