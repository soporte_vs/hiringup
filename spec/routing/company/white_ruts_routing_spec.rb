require "rails_helper"

RSpec.describe Company::WhiteRutsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/app/company/white_ruts").to route_to("company/white_ruts#index")
    end

    it "routes to #new" do
      expect(:get => "/app/company/white_ruts/new").to route_to("company/white_ruts#new")
    end

    it "routes to #show" do
      expect(:get => "/app/company/white_ruts/1").to route_to("company/white_ruts#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/app/company/white_ruts/1/edit").to route_to("company/white_ruts#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/app/company/white_ruts").to route_to("company/white_ruts#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/app/company/white_ruts/1").to route_to("company/white_ruts#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/app/company/white_ruts/1").to route_to("company/white_ruts#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/app/company/white_ruts/1").to route_to("company/white_ruts#destroy", :id => "1")
    end

  end
end
