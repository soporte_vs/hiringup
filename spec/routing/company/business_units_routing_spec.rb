require "rails_helper"

RSpec.describe Company::BusinessUnitsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/app/company/business_units").to route_to("company/business_units#index")
    end

    it "routes to #new" do
      expect(:get => "/app/company/business_units/new").to route_to("company/business_units#new")
    end

    it "routes to #show" do
      expect(:get => "/app/company/business_units/1").to route_to("company/business_units#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/app/company/business_units/1/edit").to route_to("company/business_units#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/app/company/business_units").to route_to("company/business_units#create")
    end

    it "routes to #update" do
      expect(:put => "/app/company/business_units/1").to route_to("company/business_units#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/app/company/business_units/1").to route_to("company/business_units#destroy", :id => "1")
    end

  end
end
