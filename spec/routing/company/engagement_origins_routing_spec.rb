require "rails_helper"

RSpec.describe Company::EngagementOriginsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/app/company/engagement_origins").to route_to("company/engagement_origins#index")
    end

    it "routes to #new" do
      expect(:get => "/app/company/engagement_origins/new").to route_to("company/engagement_origins#new")
    end

    it "routes to #show" do
      expect(:get => "/app/company/engagement_origins/1").to route_to("company/engagement_origins#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/app/company/engagement_origins/1/edit").to route_to("company/engagement_origins#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/app/company/engagement_origins").to route_to("company/engagement_origins#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/app/company/engagement_origins/1").to route_to("company/engagement_origins#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/app/company/engagement_origins/1").to route_to("company/engagement_origins#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/app/company/engagement_origins/1").to route_to("company/engagement_origins#destroy", :id => "1")
    end

  end
end
