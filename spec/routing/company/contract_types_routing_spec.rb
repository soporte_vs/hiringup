require "rails_helper"

RSpec.describe Company::ContractTypesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/app/company/contract_types").to route_to("company/contract_types#index")
    end

    it "routes to #new" do
      expect(:get => "/app/company/contract_types/new").to route_to("company/contract_types#new")
    end

    it "routes to #show" do
      expect(:get => "/app/company/contract_types/1").to route_to("company/contract_types#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/app/company/contract_types/1/edit").to route_to("company/contract_types#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/app/company/contract_types").to route_to("company/contract_types#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/app/company/contract_types/1").to route_to("company/contract_types#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/app/company/contract_types/1").to route_to("company/contract_types#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/app/company/contract_types/1").to route_to("company/contract_types#destroy", :id => "1")
    end

  end
end
