require "rails_helper"

RSpec.describe Company::EstatesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/app/company/estates").to route_to("company/estates#index")
    end

    it "routes to #new" do
      expect(:get => "/app/company/estates/new").to route_to("company/estates#new")
    end

    it "routes to #show" do
      expect(:get => "/app/company/estates/1").to route_to("company/estates#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/app/company/estates/1/edit").to route_to("company/estates#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/app/company/estates").to route_to("company/estates#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/app/company/estates/1").to route_to("company/estates#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/app/company/estates/1").to route_to("company/estates#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/app/company/estates/1").to route_to("company/estates#destroy", :id => "1")
    end

  end
end
