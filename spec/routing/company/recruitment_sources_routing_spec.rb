require "rails_helper"

RSpec.describe Company::RecruitmentSourcesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/app/company/recruitment_sources").to route_to("company/recruitment_sources#index")
    end

    it "routes to #new" do
      expect(:get => "/app/company/recruitment_sources/new").to route_to("company/recruitment_sources#new")
    end

    it "routes to #show" do
      expect(:get => "/app/company/recruitment_sources/1").to route_to("company/recruitment_sources#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/app/company/recruitment_sources/1/edit").to route_to("company/recruitment_sources#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/app/company/recruitment_sources").to route_to("company/recruitment_sources#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/app/company/recruitment_sources/1").to route_to("company/recruitment_sources#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/app/company/recruitment_sources/1").to route_to("company/recruitment_sources#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/app/company/recruitment_sources/1").to route_to("company/recruitment_sources#destroy", :id => "1")
    end

  end
end
