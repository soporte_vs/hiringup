require "rails_helper"

RSpec.describe Company::RecordsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/app/company/records").to route_to("company/records#index")
    end

    it "routes to #new" do
      expect(:get => "/app/company/records/new").to route_to("company/records#new")
    end

    it "routes to #show" do
      expect(:get => "/app/company/records/1").to route_to("company/records#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/app/company/records/1/edit").to route_to("company/records#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/app/company/records").to route_to("company/records#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/app/company/records/1").to route_to("company/records#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/app/company/records/1").to route_to("company/records#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/app/company/records/1").to route_to("company/records#destroy", :id => "1")
    end

  end
end
