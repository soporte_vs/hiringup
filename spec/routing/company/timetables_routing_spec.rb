require "rails_helper"

RSpec.describe Company::TimetablesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/app/company/timetables").to route_to("company/timetables#index")
    end

    it "routes to #new" do
      expect(:get => "/app/company/timetables/new").to route_to("company/timetables#new")
    end

    it "routes to #show" do
      expect(:get => "/app/company/timetables/1").to route_to("company/timetables#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/app/company/timetables/1/edit").to route_to("company/timetables#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/app/company/timetables").to route_to("company/timetables#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/app/company/timetables/1").to route_to("company/timetables#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/app/company/timetables/1").to route_to("company/timetables#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/app/company/timetables/1").to route_to("company/timetables#destroy", :id => "1")
    end

  end
end
