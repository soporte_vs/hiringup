require "rails_helper"

RSpec.describe Company::AreasController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/app/company/areas").to route_to("company/areas#index")
    end

    it "routes to #new" do
      expect(:get => "/app/company/areas/new").to route_to("company/areas#new")
    end

    it "routes to #show" do
      expect(:get => "/app/company/areas/1").to route_to("company/areas#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/app/company/areas/1/edit").to route_to("company/areas#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/app/company/areas").to route_to("company/areas#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/app/company/areas/1").to route_to("company/areas#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/app/company/areas/1").to route_to("company/areas#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/app/company/areas/1").to route_to("company/areas#destroy", :id => "1")
    end

  end
end
