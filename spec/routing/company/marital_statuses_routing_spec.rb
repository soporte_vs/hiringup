require "rails_helper"

RSpec.describe Company::MaritalStatusesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/app/company/marital_statuses").to route_to("company/marital_statuses#index")
    end

    it "routes to #new" do
      expect(:get => "/app/company/marital_statuses/new").to route_to("company/marital_statuses#new")
    end

    it "routes to #show" do
      expect(:get => "/app/company/marital_statuses/1").to route_to("company/marital_statuses#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/app/company/marital_statuses/1/edit").to route_to("company/marital_statuses#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/app/company/marital_statuses").to route_to("company/marital_statuses#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/app/company/marital_statuses/1").to route_to("company/marital_statuses#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/app/company/marital_statuses/1").to route_to("company/marital_statuses#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/app/company/marital_statuses/1").to route_to("company/marital_statuses#destroy", :id => "1")
    end

  end
end
