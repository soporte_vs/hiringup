require "rails_helper"

RSpec.describe Applicant::SoftwareLevelsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/app/applicant/software_levels").to route_to("applicant/software_levels#index")
    end

    it "routes to #new" do
      expect(:get => "/app/applicant/software_levels/new").to route_to("applicant/software_levels#new")
    end

    it "routes to #show" do
      expect(:get => "/app/applicant/software_levels/1").to route_to("applicant/software_levels#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/app/applicant/software_levels/1/edit").to route_to("applicant/software_levels#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/app/applicant/software_levels").to route_to("applicant/software_levels#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/app/applicant/software_levels/1").to route_to("applicant/software_levels#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/app/applicant/software_levels/1").to route_to("applicant/software_levels#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/app/applicant/software_levels/1").to route_to("applicant/software_levels#destroy", :id => "1")
    end

  end
end
