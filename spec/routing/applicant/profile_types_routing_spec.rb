require "rails_helper"

RSpec.describe Applicant::ProfileTypesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/app/applicant/profile_types").to route_to("applicant/profile_types#index")
    end

    it "routes to #new" do
      expect(:get => "/app/applicant/profile_types/new").to route_to("applicant/profile_types#new")
    end

    it "routes to #show" do
      expect(:get => "/app/applicant/profile_types/1").to route_to("applicant/profile_types#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/app/applicant/profile_types/1/edit").to route_to("applicant/profile_types#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/app/applicant/profile_types").to route_to("applicant/profile_types#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/app/applicant/profile_types/1").to route_to("applicant/profile_types#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/app/applicant/profile_types/1").to route_to("applicant/profile_types#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/app/applicant/profile_types/1").to route_to("applicant/profile_types#destroy", :id => "1")
    end

  end
end
