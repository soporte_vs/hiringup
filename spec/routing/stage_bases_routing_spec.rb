require "rails_helper"

RSpec.describe Stage::BasesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/app/courses/1/stage/bases").to_not route_to("stage/bases#index", id: "1")
    end

    it "routes to #show" do
      expect(:get => "/app/courses/1/stage/bases/2").to_not route_to("stage/bases#show", id: "1", stage_id: "2")
    end

    it "routes to #finish_stage" do
      expect(:post => "/app/courses/1/stage/bases/2/finish_stage").to_not route_to("stage/bases#finish_stage", id: "1", stage_id: "2")
      expect(:put => "/app/courses/1/stage/bases/2/finish_stage").to_not route_to("stage/bases#finish_stage", id: "1", stage_id: "2")
      expect(:patch => "/app/courses/1/stage/bases/2/finish_stage").to_not route_to("stage/bases#finish_stage", id: "1", stage_id: "2")
    end

    it "routes to #massive_finish_stage" do
      expect(:post => "/app/courses/1/stage/bases/massive_finish_stage").to_not route_to("stage/bases#massive_finish_stage", id: "1")
      expect(:put => "/app/courses/1/stage/bases/massive_finish_stage").to_not route_to("stage/bases#massive_finish_stage", id: "1")
      expect(:patch => "/app/courses/1/stage/bases/massive_finish_stage").to_not route_to("stage/bases#massive_finish_stage", id: "1")
    end
  end
end
