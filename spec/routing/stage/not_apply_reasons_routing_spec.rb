require "rails_helper"

RSpec.describe Stage::NotApplyReasonsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/app/stage/not_apply_reasons").to route_to("stage/not_apply_reasons#index")
    end

    it "routes to #new" do
      expect(:get => "/app/stage/not_apply_reasons/new").to route_to("stage/not_apply_reasons#new")
    end

    it "routes to #show" do
      expect(:get => "/app/stage/not_apply_reasons/1").to route_to("stage/not_apply_reasons#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/app/stage/not_apply_reasons/1/edit").to route_to("stage/not_apply_reasons#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/app/stage/not_apply_reasons").to route_to("stage/not_apply_reasons#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/app/stage/not_apply_reasons/1").to route_to("stage/not_apply_reasons#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/app/stage/not_apply_reasons/1").to route_to("stage/not_apply_reasons#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/app/stage/not_apply_reasons/1").to route_to("stage/not_apply_reasons#destroy", :id => "1")
    end

  end
end
