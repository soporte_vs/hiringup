require "rails_helper"

RSpec.describe Minisite::BannersController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "minisite/banners").to route_to("minisite/banners#index")
    end

    it "routes to #new" do
      expect(:get => "minisite/banners/new").to route_to("minisite/banners#new")
    end

    it "routes to #show" do
      expect(:get => "minisite/banners/1").to route_to("minisite/banners#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "minisite/banners/1/edit").to route_to("minisite/banners#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "minisite/banners").to route_to("minisite/banners#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "minisite/banners/1").to route_to("minisite/banners#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "minisite/banners/1").to route_to("minisite/banners#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "minisite/banners/1").to route_to("minisite/banners#destroy", :id => "1")
    end

  end
end
