require "rails_helper"

RSpec.describe FaqsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/app/faqs").to route_to("faqs#index")
    end

    it "routes to #new" do
      expect(:get => "/app/faqs/new").to route_to("faqs#new")
    end

    it "routes to #show" do
      expect(:get => "/app/faqs/1").to route_to("faqs#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/app/faqs/1/edit").to route_to("faqs#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/app/faqs").to route_to("faqs#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/app/faqs/1").to route_to("faqs#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/app/faqs/1").to route_to("faqs#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/app/faqs/1").to route_to("faqs#destroy", :id => "1")
    end

  end
end
