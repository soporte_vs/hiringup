require "rails_helper"

RSpec.describe Education::StudyLevelsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/app/education/study_levels").to route_to("education/study_levels#index")
    end

    it "routes to #new" do
      expect(:get => "/app/education/study_levels/new").to route_to("education/study_levels#new")
    end

    it "routes to #show" do
      expect(:get => "/app/education/study_levels/1").to route_to("education/study_levels#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/app/education/study_levels/1/edit").to route_to("education/study_levels#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/app/education/study_levels").to route_to("education/study_levels#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/app/education/study_levels/1").to route_to("education/study_levels#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/app/education/study_levels/1").to route_to("education/study_levels#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/app/education/study_levels/1").to route_to("education/study_levels#destroy", :id => "1")
    end

  end
end
