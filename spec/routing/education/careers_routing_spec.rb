require "rails_helper"

RSpec.describe Education::CareersController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/app/education/careers").to route_to("education/careers#index")
    end

    it "routes to #new" do
      expect(:get => "/app/education/careers/new").to route_to("education/careers#new")
    end

    it "routes to #show" do
      expect(:get => "/app/education/careers/1").to route_to("education/careers#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/app/education/careers/1/edit").to route_to("education/careers#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/app/education/careers").to route_to("education/careers#create")
    end

    it "routes to #update" do
      expect(:put => "/app/education/careers/1").to route_to("education/careers#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/app/education/careers/1").to route_to("education/careers#destroy", :id => "1")
    end

  end
end
