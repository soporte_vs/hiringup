require "rails_helper"

RSpec.describe Education::InstitutionsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/app/education/institutions").to route_to("education/institutions#index")
    end

    it "routes to #new" do
      expect(:get => "/app/education/institutions/new").to route_to("education/institutions#new")
    end

    it "routes to #show" do
      expect(:get => "/app/education/institutions/1").to route_to("education/institutions#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/app/education/institutions/1/edit").to route_to("education/institutions#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/app/education/institutions").to route_to("education/institutions#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/app/education/institutions/1").to route_to("education/institutions#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/app/education/institutions/1").to route_to("education/institutions#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/app/education/institutions/1").to route_to("education/institutions#destroy", :id => "1")
    end

  end
end
