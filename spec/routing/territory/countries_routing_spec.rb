require "rails_helper"

RSpec.describe Territory::CountriesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/app/territory/countries").to route_to("territory/countries#index")
    end

    it "routes to #new" do
      expect(:get => "/app/territory/countries/new").to route_to("territory/countries#new")
    end

    it "routes to #show" do
      expect(:get => "/app/territory/countries/1").to route_to("territory/countries#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/app/territory/countries/1/edit").to route_to("territory/countries#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/app/territory/countries").to route_to("territory/countries#create")
    end

    it "routes to #update" do
      expect(:put => "/app/territory/countries/1").to route_to("territory/countries#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/app/territory/countries/1").to route_to("territory/countries#destroy", :id => "1")
    end

  end
end
