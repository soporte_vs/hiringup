require "rails_helper"

RSpec.describe Territory::StatesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/app/territory/states").to route_to("territory/states#index")
    end

    it "routes to #new" do
      expect(:get => "/app/territory/states/new").to route_to("territory/states#new")
    end

    it "routes to #show" do
      expect(:get => "/app/territory/states/1").to route_to("territory/states#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/app/territory/states/1/edit").to route_to("territory/states#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/app/territory/states").to route_to("territory/states#create")
    end

    it "routes to #update" do
      expect(:put => "/app/territory/states/1").to route_to("territory/states#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/app/territory/states/1").to route_to("territory/states#destroy", :id => "1")
    end

  end
end
