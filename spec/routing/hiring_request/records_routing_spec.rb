require "rails_helper"

RSpec.describe HiringRequest::RecordsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/app/hiring_request/records").to route_to("hiring_request/records#index")
    end

    it "routes to #new" do
      expect(:get => "/app/hiring_request/records/new").to route_to("hiring_request/records#new")
    end

    it "routes to #show" do
      expect(:get => "/app/hiring_request/records/1").to route_to("hiring_request/records#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/app/hiring_request/records/1/edit").to route_to("hiring_request/records#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/app/hiring_request/records").to route_to("hiring_request/records#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/app/hiring_request/records/1").to route_to("hiring_request/records#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/app/hiring_request/records/1").to route_to("hiring_request/records#update", :id => "1")
    end

    # it "routes to #destroy" do
    #   expect(:delete => "/app/hiring_request/records/1").to route_to("hiring_request/records#destroy", :id => "1")
    # end

  end
end
