require "rails_helper"

RSpec.describe People::IdentificationDocumentTypesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/app/people/identification_document_types").to route_to("people/identification_document_types#index")
    end

    it "routes to #new" do
      expect(:get => "/app/people/identification_document_types/new").to route_to("people/identification_document_types#new")
    end

    it "routes to #show" do
      expect(:get => "/app/people/identification_document_types/1").to route_to("people/identification_document_types#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/app/people/identification_document_types/1/edit").to route_to("people/identification_document_types#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/app/people/identification_document_types").to route_to("people/identification_document_types#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/app/people/identification_document_types/1").to route_to("people/identification_document_types#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/app/people/identification_document_types/1").to route_to("people/identification_document_types#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/app/people/identification_document_types/1").to route_to("people/identification_document_types#destroy", :id => "1")
    end

  end
end
