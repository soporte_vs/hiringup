require 'rails_helper'

RSpec.describe 'feature Create a Publication', type: :feature do
  context 'New publication on course' do
    login_user_with_role(:admin, nil)
    it 'should create a new publication and redirect to that', js: true do
      course = FactoryGirl.create :course
      visit new_minisite_publication_path(course)
      
      fill_in("minisite_publication[title]", with: FFaker::Lorem.sentence)
      within_frame(find('.wysihtml5-sandbox')) do 
        find('body').set(FFaker::Lorem.sentence)
      end
      
      _name_form = 'minisite_publication[questions_attributes]'
      
      3.times.each do |index|
        find("input[type=button][name=add_question]").click
        fill_in(_name_form + "[#{index}][description]", with: FFaker::HipsterIpsum.word)
      end
      
      expect{
        find("input[type=submit]").click
      }.to change(Minisite::Publication, :count).by(1)
      
      publication = Minisite::Publication.last

      expect(publication.questions.count).to eq 3
      expect(page.current_path).to eq minisite_publication_path(:publication_id => publication.id, :id => course.id)
    end
  end
end
