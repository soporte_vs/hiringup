require 'rails_helper'

RSpec.describe 'feature Edit a Publication', type: :feature do
  context 'Edit publication on course' do
    login_user_with_role(:admin, nil)
    it 'should update a new publication and redirect to that', js: true do
      publication = FactoryGirl.create :minisite_publication
      questions = FactoryGirl.create_list(:minisite_question, 3, minisite_publication: publication)

      visit edit_minisite_publication_path(publication.course, publication_id: publication.id)

      new_title = FFaker::Lorem.sentence
      new_description = FFaker::Lorem.paragraph

      fill_in("minisite_publication[title]", with: new_title)
      within_frame(find('.wysihtml5-sandbox')) do 
        find('body').set(new_description)
      end

      _name_form = 'minisite_publication[questions_attributes]'

      2.times.each do |index|
        find_field(_name_form + "[#{index}][_destroy]").click
        page.accept_confirm
      end


      new_question_description = FFaker::HipsterIpsum.word

      fill_in(_name_form + "[2][description]", with: new_question_description)

      expect{
        find("input[type=submit]").click
      }.to change(publication.questions, :count).by(-2)

      publication.reload

      expect(publication.title).to eq new_title
      expect(publication.description).to eq new_description
      expect(publication.questions.last.description).to eq new_question_description
      expect(page.current_path).to eq minisite_publication_path(:publication_id => publication.id, :id => publication.course_id)
    end
  end
end