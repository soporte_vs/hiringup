require 'rails_helper'

RSpec.describe "Feature UPDATE applicant", type: :feature do
  describe "GET and UPDATE /applicant/bases/edit and update" do

    login_user_with_role(:admin, nil)

    it "should update applicant and redirect to that", js: true do
      university_1 = FactoryGirl.create(:education_university)
      career_1 = FactoryGirl.create(:education_career)
      university_2 = FactoryGirl.create(:education_university)
      career_2 = FactoryGirl.create(:education_career)
      applicant = FactoryGirl.create(:applicant_bases)

      visit edit_applicant_basis_path(applicant)
      _name_form = "applicant[user_attributes][professional_information_attributes][degrees_attributes]"

      page.execute_script("$('.xdsoft_datetimepicker').css('visibility','hidden')")

      # click en el botón agregar degree
      find(:xpath, '//*[@id="edit_applicant"]/div[3]/div/div[2]/div/div[1]/div/div/div[1]/div[2]/button').click
      find_field(_name_form + "[0][type]").find(:xpath, 'option[2]').select_option
      page.execute_script "window.scrollBy(0,250)"

      fill_in(_name_form + "[0][name]", with: FFaker::HipsterIpsum.sentence)
      find_field(_name_form + "[0][career_id]").find(:xpath, 'option[2]').select_option
      find_field(_name_form + "[0][institution_id]").find(:xpath, 'option[2]').select_option
      find_field(_name_form + "[0][condition]").find(:xpath, 'option[2]').select_option
      fill_in(_name_form + "[0][culmination_date]", with: Date.today.to_s)

      page.execute_script "window.scrollBy(0,-250)"
      find(:xpath, '//*[@id="edit_applicant"]/div[3]/div/div[2]/div/div[1]/div/div/div[1]/div[2]/button').click

      find_field(_name_form + "[1][type]").find(:xpath, 'option[3]').select_option
      fill_in(_name_form + "[1][name]", with: FFaker::HipsterIpsum.sentence)
      find_field(_name_form + "[1][career_id]").find(:xpath, 'option[3]').select_option
      find_field(_name_form + "[1][institution_id]").find(:xpath, 'option[3]').select_option
      find_field(_name_form + "[1][condition]").find(:xpath, "option[#{People::Degree::CONDITIONS.count+1}]").select_option
      fill_in(_name_form + "[1][culmination_date]", with: Date.today.to_s)

      page.execute_script "window.scrollBy(0,-50000)"
      find(:xpath, "//*[@id=\"edit_applicant\"]/div[1]/div[2]/div/div/input").click

      applicant.reload
      expect(applicant.user.professional_information.degrees.count).to eq 2

      new_degree = applicant.user.professional_information.degrees.first
      expect(new_degree.type).to eq('People::PrimarySchoolDegree')
      expect(new_degree.institution_id).to eq(university_1.id)
      expect(new_degree.career_id).to eq(career_1.id)
      expect(new_degree.condition).to eq(People::Degree::CONDITIONS.first)

      new_degree = applicant.user.professional_information.degrees.last
      expect(new_degree.type).to eq('People::HighSchoolDegree')
      expect(new_degree.institution_id).to eq(university_2.id)
      expect(new_degree.career_id).to eq(career_2.id)
      expect(new_degree.condition).to eq(People::Degree::CONDITIONS.last)

      expect(page.current_path).to eq applicant_basis_path(applicant)
    end

    context 'when deleting degrees' do
      let(:degree){FactoryGirl.create(:people_degree)}
      let(:applicant){FactoryGirl.create(:applicant_bases, user: degree.professional_information.user)}
      let(:_name_form){"applicant[user_attributes][professional_information_attributes][degrees_attributes]"}
      let(:add_degree_xpath){'//*[@id="edit_applicant"]/div[3]/div/div[2]/div/div[1]/div/div/div[1]/div[2]/button'}

      it "should delete one applicant's degree and add a new one", js: true do

        visit edit_applicant_basis_path(applicant)
        find(:xpath, add_degree_xpath).click

        page.execute_script("$('.xdsoft_datetimepicker').css('visibility','hidden')")
        find_field(_name_form + "[1][type]").find(:xpath, 'option[2]').select_option
        fill_in(_name_form + "[1][name]", with: FFaker::HipsterIpsum.sentence)
        find_field(_name_form + "[1][career_id]").find(:xpath, 'option[2]').select_option
        find_field(_name_form + "[1][institution_id]").find(:xpath, 'option[2]').select_option
        find_field(_name_form + "[1][condition]").find(:xpath, 'option[2]').select_option
        fill_in(_name_form + "[1][culmination_date]", with: Date.today.to_s)

        find_field(_name_form + "[0][_destroy]").click

        # Al Agregar un degree, y borrar otro ya persistido no debe aumentar la cantidad
        # de degrees en la base de datos.
        expect{
          page.execute_script "window.scrollBy(0,-50000)"
          find(:xpath, "//*[@id=\"edit_applicant\"]/div[1]/div[2]/div/div/input").click
        }.to change(applicant.user.professional_information.degrees, :count).by(0)

        # Agregar un degree, destruirlo y destruir el ya existente
        visit edit_applicant_basis_path(applicant)
        find(:xpath, add_degree_xpath).click
        find_field(_name_form + "[1][type]").find(:xpath, 'option[1]').select_option
        fill_in(_name_form + "[1][name]", with: FFaker::HipsterIpsum.sentence)
        find_field(_name_form + "[1][career_id]").find(:xpath, 'option[1]').select_option
        find_field(_name_form + "[1][institution_id]").find(:xpath, 'option[1]').select_option
        find_field(_name_form + "[1][condition]").find(:xpath, 'option[1]').select_option
        fill_in(_name_form + "[1][culmination_date]", with: "")

        find_field(_name_form + "[1][_destroy]").click
        find_field(_name_form + "[0][_destroy]").click


        # Al Agregar un degree, borrarlo sin guardar y borrar el otro persistido debe
        # disminuir solo un degree en la base de datos.
        expect{
          page.execute_script "window.scrollBy(0,-50000)"
          find(:xpath, "//*[@id=\"edit_applicant\"]/div[1]/div[2]/div/div/input").click
        }.to change(applicant.user.professional_information.degrees, :count).by(-1)

        expect(page.current_path).to eq applicant_basis_path(applicant)
      end
    end


    context 'Laboral Experiences' do
      it 'should delete 3 and create 5 laboral_experiences', js: true do
        applicant = FactoryGirl.create(:applicant_bases)
        professional_information = FactoryGirl.create(:people_professional_information, user: applicant.user)
        laboral_experiences = FactoryGirl.create_list(:people_laboral_experience, 3, professional_information: professional_information)

        visit edit_applicant_basis_path(applicant)

        _name_form = "applicant[user_attributes][professional_information_attributes][laboral_experiences_attributes]"
        3.times.each do |index|
          find_field(_name_form + "[#{index}][_destroy]").click
        end


        (3..8).each do |index|
          find("button[type=button][name=add_laboral_experience]").click
          fill_in(_name_form + "[#{index}][position]", with: FFaker::HipsterIpsum.word)
          fill_in(_name_form + "[#{index}][company]", with: FFaker::Company.name)
          fill_in(_name_form + "[#{index}][since_date]", with: Date.today.to_s)
          fill_in(_name_form + "[#{index}][description]", with: FFaker::HipsterIpsum.paragraph)
          page.execute_script("$('.xdsoft_datetimepicker').css('visibility','hidden')")
          fill_in(_name_form + "[#{index}][until_date]", with: Date.today.to_s)
          page.execute_script("$('.xdsoft_datetimepicker').css('visibility','hidden')")
        end

        find_field(_name_form + "[6][_destroy]").click
        expect{
          page.execute_script "window.scrollBy(0,-50000)"
          find(:xpath, "//*[@id=\"edit_applicant\"]/div[1]/div[2]/div/div/input").click
        }.to change(applicant.user.professional_information.laboral_experiences, :count).by(2)
        expect(page.current_path).to eq applicant_basis_path(applicant)
      end
    end
  end

end
