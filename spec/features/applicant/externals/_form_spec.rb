require 'rails_helper'

RSpec.describe 'feature new session and/or register', type: :feature do
  describe 'GET /users/sign_up' do

    it 'should not register a user with invalid data', js: true do
      visit new_registration_path

      fill_in('applicant_external[user_attributes][email]', with: FFaker::BaconIpsum.word)
      fill_in('applicant_external[user_attributes][password]', with: FFaker::HipsterIpsum.word)

      find(:css, 'input[type="submit"]').click

      expect(Applicant::External.count).to eq(0)
    end

    it 'register a new user', js: true do
      visit new_registration_path

      expect(Applicant::External.count).to eq(0)

      fill_in('applicant_external[user_attributes][email]', with: FFaker::Internet.email)
      fill_in('applicant_external[user_attributes][password]', with: FFaker::Internet.password)

      find(:css, 'input[type="submit"]').click

      expect(page.current_path).to eq(edit_profiles_path)
      expect(Applicant::External.count).to eq(1)
    end
  end
end