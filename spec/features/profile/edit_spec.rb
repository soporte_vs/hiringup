require 'rails_helper'

RSpec.describe "Feature UPDATE current_user", type: :feature do
  describe "GET and UPDATE /profiles/edit and update" do
    login_user_with_role(:admin, nil)
    it "should update current_user and redirect to that", js: true do
      university_1 = FactoryGirl.create(:education_university)
      career_1 = FactoryGirl.create(:education_career)
      university_2 = FactoryGirl.create(:education_university)
      career_2 = FactoryGirl.create(:education_career)

      visit edit_profiles_path
      find("button[type=button][name=add_degree]").click
      select_from_chosen(Territory::Country.first.name, from: 'user[personal_information_attributes][nationality_id]')
      select_from_chosen(Company::MaritalStatus.first.name, from: 'user[personal_information_attributes][company_marital_status_id]')
      select_from_chosen(Company::RecruitmentSource.first.name, from: 'user[personal_information_attributes][recruitment_source_id]')

      areas_of_interest = FFaker::HipsterIpsum.paragraph
      fill_in("user[personal_information_attributes][areas_of_interest]", with: areas_of_interest)

      _name_form = "user[professional_information_attributes][degrees_attributes]"
      page.execute_script("$('.xdsoft_datetimepicker').css('visibility','hidden')")

      fill_in(_name_form + "[0][name]", with: FFaker::HipsterIpsum.sentence)
      find_field(_name_form + "[0][career_id]").find(:xpath, 'option[2]').select_option
      find_field(_name_form + "[0][institution_id]").find(:xpath, 'option[2]').select_option
      find_field(_name_form + "[0][condition]").find(:xpath, 'option[2]').select_option
      fill_in(_name_form + "[0][culmination_date]", with: Date.today.to_s)

      find("button[type=button][name=add_degree]").click

      fill_in(_name_form + "[1][name]", with: FFaker::HipsterIpsum.sentence)
      find_field(_name_form + "[1][career_id]").find(:xpath, 'option[3]').select_option
      find_field(_name_form + "[1][institution_id]").find(:xpath, 'option[3]').select_option
      find_field(_name_form + "[1][condition]").find(:xpath, "option[#{People::Degree::CONDITIONS.count + 1}]").select_option
      fill_in(_name_form + "[1][culmination_date]", with: Date.today.to_s)

      expect{
        page.execute_script "window.scrollBy(0,-50000)"
        find("form[action='#{profiles_path}']").find('input[type=submit][value="Guardar Información"]').click
      }.to change(@user.professional_information.degrees, :count).by(2)

      @user.reload
      expect(@user.personal_information.areas_of_interest).to eq areas_of_interest

      new_degree = @user.professional_information.degrees.first

      expect(new_degree.institution_id).to eq(university_1.id)
      expect(new_degree.career_id).to eq(career_1.id)
      expect(new_degree.institution.type).to eq(university_1.type)
      expect(new_degree.condition).to eq(People::Degree::CONDITIONS.first)

      new_degree = @user.professional_information.degrees.last
      expect(new_degree.institution_id).to eq(university_2.id)
      expect(new_degree.career_id).to eq(career_2.id)
      expect(new_degree.institution.type).to eq(university_2.type)
      expect(new_degree.condition).to eq(People::Degree::CONDITIONS.last)

      expect(page.current_path).to eq edit_profiles_path
    end

    it "should delete one user's degree ", js: true do
      professional_information = FactoryGirl.create(:people_professional_information, user: @user)
      FactoryGirl.create(:people_degree, professional_information: professional_information)

      visit edit_profiles_path

      _name_form = "user[professional_information_attributes][degrees_attributes]"
      page.execute_script("$('.xdsoft_datetimepicker').css('visibility','hidden')")
      page.execute_script "window.scrollBy(0, 250)"

      select_from_chosen(Territory::Country.first.name, from: 'user[personal_information_attributes][territory_country_id]')
      select_from_chosen(Company::MaritalStatus.first.name, from: 'user[personal_information_attributes][company_marital_status_id]')
      select_from_chosen(Company::RecruitmentSource.first.name, from: 'user[personal_information_attributes][recruitment_source_id]')

      find_field(_name_form + "[0][_destroy]").click

      find("button[type=button][name=add_degree]").click

      find_field(_name_form + "[1][type]").find(:xpath, 'option[2]').select_option
      fill_in(_name_form + "[1][name]", with: FFaker::HipsterIpsum.sentence)
      find_field(_name_form + "[1][career_id]").find(:xpath, 'option[2]').select_option
      find_field(_name_form + "[1][institution_id]").find(:xpath, 'option[2]').select_option
      find_field(_name_form + "[1][condition]").find(:xpath, 'option[2]').select_option
      fill_in(_name_form + "[1][culmination_date]", with: Date.today.to_s)

      # Al Agregar un degree, y borrar otro ya persistido no debe aumentar la cantidad
      # de degrees en la base de datos.
      expect{
        page.execute_script "window.scrollBy(0,-50000)"
        find("form[action='#{profiles_path}']").find('input[type=submit][value="Guardar Información"]').click
      }.to change(@user.professional_information.degrees, :count).by(0)

      page.execute_script "window.scrollBy(0, 500)"
      find("button[type=button][name=add_degree]").click

      find_field(_name_form + "[1][type]").find(:xpath, 'option[2]').select_option
      fill_in(_name_form + "[1][name]", with: FFaker::HipsterIpsum.sentence)
      find_field(_name_form + "[1][career_id]").find(:xpath, 'option[1]').select_option
      find_field(_name_form + "[1][institution_id]").find(:xpath, 'option[1]').select_option
      find_field(_name_form + "[1][condition]").find(:xpath, 'option[1]').select_option
      fill_in(_name_form + "[1][culmination_date]", with: "")

      find_field(_name_form + "[1][_destroy]").click
      find_field(_name_form + "[0][_destroy]").click

      # Al Agregar un degree, borrarlo sin guardar y borrar el otro persistido debe
      # disminuir solo un degree en la base de datos.
      expect{
        page.execute_script "window.scrollBy(0,-50000)"
        find("form[action='#{profiles_path}']").find('input[type=submit][value="Guardar Información"]').click
      }.to change(@user.professional_information.degrees, :count).by(-1)

      expect(page.current_path).to eq edit_profiles_path

    end

    context 'Laboral Experiences' do
      it 'should delete 3 and create 5 laboral_experiences', js: true do
        professional_information = FactoryGirl.create(:people_professional_information, user: @user)
        FactoryGirl.create_list :people_laboral_experience, 3, professional_information: professional_information
        visit edit_profiles_path

        select_from_chosen(Territory::Country.first.name, from: 'user[personal_information_attributes][nationality_id]')
        select_from_chosen(Company::MaritalStatus.first.name, from: 'user[personal_information_attributes][company_marital_status_id]')
        select_from_chosen(Company::RecruitmentSource.first.name, from: 'user[personal_information_attributes][recruitment_source_id]')

        _name_form = "user[professional_information_attributes][laboral_experiences_attributes]"
        3.times.each do |index|
          find_field(_name_form + "[#{index}][_destroy]").click
        end

        (3..8).each do |index|
          find("button[type=button][name=add_laboral_experience]").click
          fill_in(_name_form + "[#{index}][position]", with: FFaker::HipsterIpsum.word)
          fill_in(_name_form + "[#{index}][company]", with: FFaker::Company.name)
          fill_in(_name_form + "[#{index}][description]", with: FFaker::HipsterIpsum.paragraph)
          fill_in(_name_form + "[#{index}][since_date]", with: Date.today.to_s)
          page.execute_script("$('.xdsoft_datetimepicker').css('visibility','hidden')")
          fill_in(_name_form + "[#{index}][until_date]", with: Date.today.to_s)
          page.execute_script("$('.xdsoft_datetimepicker').css('visibility','hidden')")
        end

        find_field(_name_form + "[6][_destroy]").click

        expect{
          page.execute_script "window.scrollBy(0,-50000)"
          find("form[action='#{profiles_path}']").find('input[type=submit][value="Guardar Información"]').click
        }.to change(@user.professional_information.laboral_experiences, :count).by(2)
        expect(page.current_path).to eq edit_profiles_path
      end
    end
  end

end
