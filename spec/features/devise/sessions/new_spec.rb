require 'rails_helper'

RSpec.describe 'feature new session and/or register', type: :feature do
  describe 'GET /users/sign_in' do

    it 'should not login a non existing user', js: true do
      user = FactoryGirl.build(:user)
      visit new_user_session_path
      
      fill_in('user[email]', with: user.email)
      fill_in('user[password]', with: user.password)

      button = "input[type='submit']"
      
      find(:css, button).click

      # Espera volver donde mismo
      expect(page.current_path).to eq(new_user_session_path)
    end
    
    it 'should login existing user', js: true do
      user = FactoryGirl.create(:user)
      visit new_user_session_path

      fill_in('user[email]', with: user.email)
      fill_in('user[password]', with: user.password)

      button = "input[type='submit']"

      find(:css, button).click

      user.reload
      # Al crear un user, su sign_in_count es igual a 0, y aumenta por cada vez que se logea
      expect(user.sign_in_count).to eq(1)
      expect(page.current_path).to eq(edit_profiles_path)
    end
  end
end