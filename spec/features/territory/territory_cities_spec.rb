require 'rails_helper'

RSpec.describe "Feature new Territory::Cities", type: :feature do
  describe "GET /territory/cities/new and submit new city" do
    login_user_with_role(:admin, nil)
    it "should create a new City and redirect to that", js: true do
      state = FactoryGirl.create(:territory_state)
      visit new_territory_city_path

      fill_in("territory_city[name]", with: FFaker::Address.city)
      # NO CHOSEN
      # find_field("territory_city[territory_country]").find(:xpath, 'option[2]').select_option
      # territory_state_select = find_field("territory_city[territory_state]")
      # expect(territory_state_select).to have_xpath('option')
      # territory_state_select.find(:xpath, 'option[2]').select_option
      # CHOSEN
      select_from_chosen(state.territory_country.name, from: 'territory_city[territory_country]')
      select_from_chosen(state.name, from: 'territory_city[territory_state]')

      expect{
        find("input[type=submit]").click
      }.to change(Territory::City, :count).by(1)
      expect(page.current_path).to eq territory_city_path(Territory::City.last)
    end
  end
end