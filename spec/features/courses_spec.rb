# coding: utf-8
require 'rails_helper'

RSpec.describe "feature new Courses", type: :feature do
  describe "POST /courses/new", elasticsearch: true do
    login_user_with_role(:admin, nil)
    it "should create a new Course and redirect to that", js: true do
      FactoryGirl.create_list(:company_cenco, 4)
      FactoryGirl.create(:document_group)
      area = FactoryGirl.create(:company_area)
      timetable = FactoryGirl.create(:company_timetable)
      study_type = People::Degree::TYPES.first.last
      study_level = FactoryGirl.create(:education_study_level)
      software_level = FactoryGirl.create(:applicant_software_level)
      profile_type = FactoryGirl.create(:applicant_profile_type)
      company_vacancy_request_reason = FactoryGirl.create(:company_vacancy_request_reason)
      company_positions_cenco = FactoryGirl.create(:company_positions_cenco)
      tag = FactoryGirl.create(:tag_course_tag)
      city = FactoryGirl.create(:territory_city)
      visit new_course_path

      # desactiva la opcion de readonly para poder escribir en el input con capybara
      page.execute_script("$('.js-datepicker-start').removeAttr('readonly')")
      fill_in("course[start]", with: Date.today.strftime('%d/%m/%Y'))

      fill_in("course[title]", with: FFaker::Lorem.sentence)
      within_frame(find('.wysihtml5-sandbox')) do
        find('body').set(FFaker::Lorem.sentence)
      end
      find_field("course[type_enrollment]").find(:xpath, "option[2]").select_option

      page.execute_script "window.scrollBy(0,300)"

      select_from_chosen(timetable.name, from: 'course[timetable_id]')
      page.execute_script "window.scrollBy(0,100)"
      select_from_chosen(area.name, from: 'course[area_id]')
      page.execute_script "window.scrollBy(0,100)"
      select_from_chosen(profile_type.name, from: 'course[applicant_profile_type_id]')
      page.execute_script "window.scrollBy(0,100)"
      select_from_chosen(software_level.name, from: 'course[applicant_software_level_id]')
      page.execute_script "window.scrollBy(0,500)"
      select_from_chosen(study_type, from: 'course[study_type]')
      select_from_chosen(study_level.name, from: 'course[study_level_id]')

      fill_in("course[minimun_requirements]", with: FFaker::Lorem.paragraph)

      # NO CHOSEN
      # find_field("course[territory_country]").find(:xpath, "option[2]").select_option
      # CHOSEN
      page.execute_script "window.scrollBy(0,200)"

      # COUNTRY
      select_from_chosen(city.territory_state.territory_country.name, from: 'course[territory_country]')

      # # Find selector of territory state NO CHOSEN
      # territory_state_select = find_field("course[territory_state]")
      # # Waiting to states load via ajax into state_selector
      # expect(territory_state_select).to have_xpath('option')
      # # selecting firs state in state_selector (option 2)
      # territory_state_select.find(:xpath, 'option[2]').select_option
      # # CHOSEN
      # REGION
      page.execute_script "window.scrollBy(0, 100)"
      select_from_chosen(city.territory_state.name, from: 'course[territory_state]')

      # # Find selector of territory city NO CHOSEN
      # territory_city_select = find_field("course[territory_city]")
      # # Waiting to cities load via ajax into city_selector
      # expect(territory_city_select).to have_xpath('option')
      # # selecting firs city in city_selector (option 2)
      # territory_city_select.find(:xpath, 'option[2]').select_option
      # # Submit form and expect to save a new Course
      # # CHOSEN
      # CITY
      page.execute_script "window.scrollBy(0, 100)"
      select_from_chosen(city.name, from: 'course[territory_city]')

      # NO CHOSEN
      # positions_select = find_field("course[company_position_id]")
      # # selecting firs position in positions_selector (option 2)
      # positions_select.find(:xpath, 'option[2]').select_option
      # CHOSEN
      # POSITION
      select_from_chosen(company_positions_cenco.company_position.name, from: 'course[company_position_id]')

      # Find selector of document_group
      document_group_select = find_field("course[document_group_id]")
      document_group_select.find(:xpath, 'option[2]').select_option

      # Comento esta linea porque ahora muestra la vacante sin hacer click al botón de agregar vacante
      # find("input[type=button][name=add_vacancy]").click

      # ENTREVISTA POR VIDEO
      # TAGS
      select_from_chosen(tag.name, from: 'course[tags][]')

      fill_in("course[vacancies_attributes][0][observations]", with: FFaker::Lorem.sentence)

      select_from_chosen(company_vacancy_request_reason.name, from: 'course[vacancies_attributes][0][company_vacancy_request_reason_id]')

      select_from_chosen(company_positions_cenco.fullname, from: 'course[vacancies_attributes][0][cenco_id]')

      find("input[type=button][name=add_vacancy]").click

      fill_in("course[vacancies_attributes][1][observations]", with: FFaker::Lorem.sentence)

      select_from_chosen(company_vacancy_request_reason.name, from: 'course[vacancies_attributes][1][company_vacancy_request_reason_id]')
      ## No chosen

      # position_business_unit_select = find_field("course[vacancies_attributes][0][cenco_id]")

      # expect(position_business_unit_select).to have_xpath('option')
      # position_business_unit_select.find(:xpath, 'option[2]').select_option
      ## WITH CHOSEN
      select_from_chosen(company_positions_cenco.fullname, from: 'course[vacancies_attributes][1][cenco_id]')

      expect{
        page.execute_script "window.scrollBy(0,-50000)"
        find("input[type=submit]").click
      }.to change(Course, :count).by(1)
      course = Course.last
      expect(course.vacancies.first.company_positions_cenco).to eq company_positions_cenco
      expect(page.current_path).to eq course_path(course)
    end
  end
end
