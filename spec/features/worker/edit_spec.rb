require 'rails_helper'

RSpec.describe "Feature UPDATE worker", type: :feature do
  describe "GET and UPDATE /worker/bases/edit and update" do
    login_user_with_role(:admin, nil)

    it "should update worker and redirect to that", js: true do
      university_1 = FactoryGirl.create(:education_university)
      career_1 = FactoryGirl.create(:education_career)
      university_2 = FactoryGirl.create(:education_university)
      career_2 = FactoryGirl.create(:education_career)
      worker = FactoryGirl.create(:worker_basis)

      visit edit_worker_basis_path(worker)

      _name_form = "worker[user_attributes][professional_information_attributes][degrees_attributes]"
      page.execute_script("$('.xdsoft_datetimepicker').css('visibility','hidden')")

      find_field(_name_form + "[0][type]").find(:xpath, 'option[2]').select_option
      fill_in(_name_form + "[0][name]", with: FFaker::HipsterIpsum.sentence)
      find_field(_name_form + "[0][career_id]").find(:xpath, 'option[2]').select_option
      find_field(_name_form + "[0][institution_id]").find(:xpath, 'option[2]').select_option
      find_field(_name_form + "[0][condition]").find(:xpath, 'option[2]').select_option
      fill_in(_name_form + "[0][culmination_date]", with: Date.today.to_s)

      find("button[type=button][name=add_degree]").click
      find_field(_name_form + "[1][type]").find(:xpath, 'option[2]').select_option
      fill_in(_name_form + "[1][name]", with: FFaker::HipsterIpsum.sentence)
      find_field(_name_form + "[1][career_id]").find(:xpath, 'option[3]').select_option
      find_field(_name_form + "[1][institution_id]").find(:xpath, 'option[3]').select_option
      find_field(_name_form + "[1][condition]").find(:xpath, "option[#{People::Degree::CONDITIONS.count + 1}]").select_option
      fill_in(_name_form + "[1][culmination_date]", with: Date.today.to_s)

      expect{
        page.execute_script "window.scrollBy(0,-50000)"
        find("input[type=submit]").click
      }.to change(worker.user.professional_information.degrees, :count).by(2)

      new_degree = worker.user.professional_information.degrees.first
      expect(new_degree.institution_id).to eq(university_1.id)
      expect(new_degree.career_id).to eq(career_1.id)

      expect(new_degree.condition).to eq(People::Degree::CONDITIONS.first)
      expect(page.current_path).to eq worker_basis_path(worker)

      new_degree = worker.user.professional_information.degrees.last
      expect(new_degree.institution_id).to eq(university_2.id)
      expect(new_degree.career_id).to eq(career_2.id)

      expect(new_degree.condition).to eq(People::Degree::CONDITIONS.last)

      expect(page.current_path).to eq worker_basis_path(worker)
    end

    it "should delete one worker's degree ", js: true do
      degree = FactoryGirl.create(:people_degree)
      worker = FactoryGirl.create(:worker_basis, user: degree.professional_information.user)
      visit edit_worker_basis_path(worker)

      _name_form = "worker[user_attributes][professional_information_attributes][degrees_attributes]"

      page.execute_script("$('.xdsoft_datetimepicker').css('visibility','hidden')")


      find_field(_name_form + "[1][type]").find(:xpath, 'option[2]').select_option
      fill_in(_name_form + "[1][name]", with: FFaker::HipsterIpsum.sentence)
      find_field(_name_form + "[1][career_id]").find(:xpath, 'option[2]').select_option
      find_field(_name_form + "[1][institution_id]").find(:xpath, 'option[2]').select_option
      find_field(_name_form + "[1][condition]").find(:xpath, 'option[2]').select_option
      fill_in(_name_form + "[1][culmination_date]", with: Date.today.to_s)
      find_field(_name_form + "[0][_destroy]").click

      # Al Agregar un degree, y borrar otro ya persistido no debe aumentar la cantidad
      # de degrees en la base de datos.
      expect{
        page.execute_script "window.scrollBy(0,-50000)"
        find("input[type=submit]").click
      }.to change(worker.user.professional_information.degrees, :count).by(0)


      visit edit_worker_basis_path(worker)
      find_field(_name_form + "[1][type]").find(:xpath, 'option[2]').select_option
      fill_in(_name_form + "[1][name]", with: FFaker::HipsterIpsum.sentence)
      find_field(_name_form + "[1][career_id]").find(:xpath, 'option[1]').select_option
      find_field(_name_form + "[1][institution_id]").find(:xpath, 'option[1]').select_option
      find_field(_name_form + "[1][condition]").find(:xpath, 'option[1]').select_option
      fill_in(_name_form + "[1][culmination_date]", with: "")


      find_field(_name_form + "[1][_destroy]").click
      find_field(_name_form + "[0][_destroy]").click

      # Al Agregar un degree, borrarlo sin guardar y borrar el otro persistido debe
      # disminuir solo un degree en la base de datos.
      expect{
        page.execute_script "window.scrollBy(0,-50000)"
        find("input[type=submit]").click
      }.to change(worker.user.professional_information.degrees, :count).by(-1)

      expect(page.current_path).to eq worker_basis_path(worker)
    end

    context 'Laboral Experiences' do
      it 'should delete 3 and create 5 laboral_experiences', js: true do
        worker = FactoryGirl.create(:worker_basis)
        professional_information = FactoryGirl.create(:people_professional_information, user: worker.user)
        laboral_experiences = FactoryGirl.create_list(:people_laboral_experience, 3, professional_information: professional_information)
        visit edit_worker_basis_path(worker)

        _name_form = "worker[user_attributes][professional_information_attributes][laboral_experiences_attributes]"
        3.times.each do |index|
          find_field(_name_form + "[#{index}][_destroy]").click
        end

        (3..8).each do |index|
          find("button[type=button][name=add_laboral_experience]").click
          fill_in(_name_form + "[#{index}][position]", with: FFaker::HipsterIpsum.word)
          fill_in(_name_form + "[#{index}][company]", with: FFaker::Company.name)
          fill_in(_name_form + "[#{index}][description]", with: FFaker::HipsterIpsum.paragraph)
          fill_in(_name_form + "[#{index}][since_date]", with: Date.today.to_s)
          page.execute_script("$('.xdsoft_datetimepicker').css('visibility','hidden')")
          fill_in(_name_form + "[#{index}][until_date]", with: Date.today.to_s)
          page.execute_script("$('.xdsoft_datetimepicker').css('visibility','hidden')")
        end

        find_field(_name_form + "[6][_destroy]").click
        page.execute_script "window.scrollBy(0,-50000)"

        expect{
          find("input[type=submit]").click
        }.to change(worker.user.professional_information.laboral_experiences, :count).by(2)
        expect(page.current_path).to eq worker_basis_path(worker)
      end
    end
  end
end
