module RequestMacros
  def login_user_with_role(role_name, resource = nil)
    before(:each) do 
      @user = FactoryGirl.create(:user)
      @user.add_role role_name, resource
      login_as(@user)
    end
  end
end
