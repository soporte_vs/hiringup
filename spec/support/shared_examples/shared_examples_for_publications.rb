shared_examples 'a publication controller update action' do |publication_type|
  #Todo: Refactor to handle full behaviour of update action
  let(:publication) do
    if publication_type == :laborum_publication
      FactoryGirl.create(publication_type, service_id: 123456)
    else
      FactoryGirl.create(publication_type)
    end
  end
  let(:associated_job){Delayed::Job.find(publication.delayed_job_id)}

  context 'when desactivate_at date changes' do
    before :each do
      valid_attributes[:desactivate_at] = Date.today + 1.days
      @user.add_role :admin_publications, publication.course
    end

    it 'updates the associated delayed job run at' do
      expect {
        params = {
          id: publication.course.to_param,
          publication_id: publication.to_param,
          publication_type => valid_attributes
        }
        put :update, params
        associated_job.reload
      }.to change(associated_job, :run_at)
    end
  end

  context 'when desactivate_at is untouched' do

    before :each do
      @user.add_role :admin_publications, publication.course
    end

    it 'does not update the associated delayed job run at' do
      expect {
        put :update, {
          id: publication.course.to_param,
          publication_id: publication.to_param,
          publication_type => valid_attributes
        }
        associated_job.reload
      }.not_to change(associated_job, :run_at)
    end
  end
end
