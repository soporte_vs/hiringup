shared_examples "a stage with reports" do |report_type, stage_association|
  describe "GET download stage document" do
    login_executive_without_role
    let(:report){FactoryGirl.create(report_type)}
    let(:stage){report.send(stage_association)}
    let(:course){stage.enrollment.course}
    let!(:document){FactoryGirl.create(:stage_document, resource: report)}

    before :each do
      # Requires work flow set externally
      course.update(work_flow: work_flow) if work_flow.presence
      @user.add_role :admin
    end

    context 'when applicant is not discarded' do
      it 'should respond with success' do
      end
    end

    context 'when applicant is discarded' do
      it 'should respond with success' do
        stage.enrollment.discard(FFaker::Lorem.word, FactoryGirl.create(:enrollment_discard_reason))
      end
    end

    after :each do
      get :download, id: course.to_param,
        stage_id: stage.to_param,
        report_id: report.id,
        stage_document_id: document.id
      expect(response.status).to eq 200
    end
  end
end
