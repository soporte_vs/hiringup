module ControllerMacros
  def login_executive_with_role(role_name, resource = nil)
    before(:each) do
      @request.env["devise.mapping"] = Devise.mappings[:user]
			@user = FactoryGirl.create(:user,email: "integraerror@valposystems.cl")
			@user.add_role role_name, resource
			sign_in @user
    end
  end

  def login_executive_without_role
    before(:each) do
      @request.env["devise.mapping"] = Devise.mappings[:user]
			@user = FactoryGirl.create(:user,email: "integraerror@valposystems.cl")
			sign_in @user
    end
  end

  def login_applicant
    before(:each) do
      @request.env["devise.mapping"] = Devise.mappings[:user]
      @applicant = FactoryGirl.create(:applicant_bases)
      sign_in @applicant.user
    end
  end
end
