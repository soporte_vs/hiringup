$COMPANY = {
  name: "Fundación Integra",
  contact_email: "contact@hiringup.com",
  default_country: 'Chile',
  maintainer_email: "hpradenas@integra.cl",
  validate_course_postulation_uniqueness: true
}
