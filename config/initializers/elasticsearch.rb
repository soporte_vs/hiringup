require 'elasticsearch/model'
require 'elasticsearch/transport'


Elasticsearch::Model::Indexing::ClassMethods.module_eval do
  def update_mapping!(options={})
    target_index = options.delete(:index) || self.index_name

    self.create_index! force: true unless index_exists?(index: target_index)

    self.client.indices.put_mapping index: target_index,
                                    type: self.document_type,
                                    body: self.mappings.to_hash
  end
end


ELASTIC_SETTINGS_OPTIONS = {
  analysis: {
    char_filter: {
      remove_points: { type: "mapping", mappings: [".=>"] }
    },
    filter: {
      u_synonyms: {type: 'synonym', synonyms: ["u, universidad"]}
    },
    analyzer: {
      full_name: {tokenizer: 'keyword', filter: ['lowercase', 'asciifolding']},
      first_name: {tokenizer: 'classic', filter: ['lowercase', 'trim', 'asciifolding']},
      emails: { tokenizer: 'uax_url_email', filter: ['lowercase'] },
      ruts: { tokenizer: 'standard', char_filter: ['remove_points'], filter: ['lowercase'] },
      identification_document_numbers: { tokenizer: 'keyword', char_filter: ['remove_points'] },
      institutions: { tokenizer: 'standard', filter: ['lowercase', 'asciifolding', 'u_synonyms'] },
      rawlike: { tokenizer: 'keyword', filter: ['lowercase', 'asciifolding'] }
    }
  }
}

RAW = { raw: { type: "string", index: "not_analyzed" } }
RAW_LIKE = { raw_like: { type: 'string', analyzer: 'rawlike' } }


Elasticsearch::Model.client = Elasticsearch::Client.new url: Rails.application.config.elastic_server,
                                                        log: false
PREFIX_INDEX = "#{Rails.application.config.hostname}-#{Rails.env}"
