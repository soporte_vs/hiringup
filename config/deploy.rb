# coding: utf-8
set :stages, %w(production staging update)
set :default_stage, "production"
set :rvm_ruby_string, 'ruby-2.2.2@prod'

require "bundler/capistrano"
require 'capistrano/ext/multistage'
require "rvm/capistrano"
require "capistrano-unicorn"
require 'capistrano/measure'


set :application, "hupe"
default_run_options[:pty] = true
set :scm, "git"
set :use_sudo, false
set :shared_children, shared_children + %w{public/uploads private}

namespace :delayed_job do
    desc "Darle atomos delayed job"
    task :restart, :roles => :app do
        run "cd #{current_path}; RAILS_ENV=#{rails_env} bin/delayed_job restart"
    end

    desc "Bajar delayed_job"
    task :stop, :roles => :app do
        run "cd #{current_path}; RAILS_ENV=#{rails_env} bin/delayed_job stop"
    end

end

namespace :deploy do

  desc "Populando la base de datos con las seeds"
  task :seed do
    puts "\n\n=== Populando la base de datos con las seeds ===\n\n"
    run "cd #{current_path}; bundle exec rake db:seed RAILS_ENV=#{rails_env}"
  end

  desc "Copia archivo de configuracion para base de datos"
  task :dbconfig do
    run "cp /home/deploy/config/database.yml #{release_path}/config"
  end

  desc "Copia archivo de secrets.yml"
  task :secret do
    run "cp /home/deploy/config/secrets.yml #{release_path}/config"
  end

  desc "Copia especial de archivos de env"
  task :special_env do
    run "cp -f /home/deploy/config/#{rails_env}.rb #{release_path}/config/environments/#{rails_env}.rb"
  end

  desc "Populando la base de datos con seed seed_migration"
    task :seed_migration do
    puts "\n\n=== Populando la base de datos con el seed migration ===\n\n"
    run "cd #{release_path}; bundle exec rake seed:migrate RAILS_ENV=#{rails_env}"
  end

end

namespace :elastic do

  desc "Agrupa las tareas para incializar elasticsearch"
  task :setup do
    run "cd #{current_path}; bundle exec rake elasticsearch:recreate_all RAILS_ENV=#{rails_env}"
  end

  desc "Creacion de index para courses"
  task :courses_index do
    puts "\n\n=== Indexando Courses ElasticSearch ===\n\n"
    run "cd #{current_path}; bundle exec rake elasticsearch:courses_index RAILS_ENV=#{rails_env}"
  end

  desc "Creacion de index para applicants "
  task :applicants_index do
    puts "\n\n=== Indexando Applicants ElasticSearch ===\n\n"
    run "cd #{current_path}; bundle exec rake elasticsearch:applicants_index RAILS_ENV=#{rails_env}"
  end

  desc "Creacion de index para enrollments"
  task :enrollments_index do
    puts "\n\n=== Indexando Enrollments ElasticSearch ===\n\n"
    run "cd #{current_path}; bundle exec rake elasticsearch:enrollments_index RAILS_ENV=#{rails_env}"
  end

  desc "Creacion de index para hiring-request-records"
  task :hiring_request_records_index do
    puts "\n\n=== Indexando Hirning Request Records ElasticSearch ===\n\n"
    run "cd #{current_path}; bundle exec rake elasticsearch:hiring_request_records_index RAILS_ENV=#{rails_env}"
  end

  desc "Creacion de index para course postulations"
  task :course_postualtions_index do
    puts "\n\n=== Indexando Course Postulations ElasticSearch ===\n\n"
    run "cd #{current_path}; bundle exec rake elasticsearch:course_postualtions_index RAILS_ENV=#{rails_env}"
  end

  desc "Eliminación de todos los índices en elastic"
  task :destroy_all_index do
    puts "\n\n=== Eliminando todos los índices de ElasticSearch ===\n\n"
    run "cd #{current_path}; bundle exec rake elasticsearch:destroy_all_index RAILS_ENV=#{rails_env}"
  end

end

namespace :reports do

  desc "Agrupa los reportes en una tarea"
  task :default do
    validate_models
    validate_roles
  end

  desc "Valida los modelos"
  task :validate_models do
    puts "\n\n=== Validando modelos ===\n\n"
    run "cd #{current_path}; bundle exec rake models:validate RAILS_ENV=#{rails_env}"
  end

  desc "Creacion de index para applicants "
  task :validate_roles do
    puts "\n\n=== Validando roles ===\n\n"
    run "cd #{current_path}; bundle exec rake roles:validate RAILS_ENV=#{rails_env}"
  end

end

namespace :db do

  desc "Agrupa las tareas para limpiar los datos de la applicacion"
  task :purge_data do
    unicorn.stop
    delayed_job.stop
    puts "\n\n=== Borrando los indices de elasticsearch y borrando la base de datos ===\n\n"
    run "cd #{current_path}; bundle exec rake elasticsearch:delete_all_index RAILS_ENV=#{rails_env}"
    run "cd #{current_path}; bundle exec rake db:drop RAILS_ENV=#{rails_env}"
    run "cd #{current_path}; bundle exec rake db:create RAILS_ENV=#{rails_env}"

    puts "\n\n=== Ahora se necesita correr la tarea de capistrano elastic:setup ===\n\n"
    puts "\n\n=== y despues deployar ===\n\n"
  end

end

namespace :whenever do
  desc "Actuliza el crontab con las espeficicaciones de whenever (config/schedule.rb)"
  task :update do
    run "cd #{current_path}; bundle exec whenever --write-crontab"
  end
end

# before  'deploy:update_code', 'unicorn:stop'
after 'bundle:install', 'deploy:dbconfig', 'deploy:secret', 'deploy:special_env'
after 'deploy:update', 'deploy:migrate', 'deploy:seed_migration', 'deploy:restart','unicorn:restart','delayed_job:restart', 'whenever:update'
