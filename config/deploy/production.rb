#Refactored production deploy flags
server '45.56.125.227', :app, :web, :db, :primary => true

set :repository, "git@bitbucket.org:soporte_vs/hiringup.git"
set :default_environment, 'JAVA_HOME' => "/usr/lib/jvm/java-8-oracle/"
set :rails_env, "production"
set :user, "deploy"
set :port, 51337
set :deploy_to, "/home/deploy/hupe"
set :branch, "master"
