#Refactored production deploy flags
server '104.200.19.108', :app, :web, :db, :primary => true

set :repository, "git@github.com:4Talent-integra/hiringUpEnterprise.git"
set :default_environment, 'JAVA_HOME' => "/usr/lib/jvm/java-8-oracle/"
set :rails_env, "production"
set :user, "deploy"
set :port, 51337
set :deploy_to, "/home/deploy/hupe"
set :branch, "'#822-update-core-bang'"
