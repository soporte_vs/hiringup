require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module HiringUpEnterprise
  class Application < Rails::Application
    config.autoload_paths += %W(#{config.root}/lib)

    config.assets.precompile += %w( application_split2.css )

    config.encoding = "utf-8"
    config.time_zone = 'America/Santiago'
    #i18n
    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', '**/*.{rb,yml}').to_s]
    config.i18n.default_locale = :es
    config.i18n.locale = :es

    config.active_record.raise_in_transactional_callbacks = true

    #React
    config.react.jsx_transformer_class = React::JSX::JSXTransformer
    config.react.addons = true

    # Tags to remove
    config.action_view.sanitized_allowed_tags = ['strong', 'em', 'a']
    config.action_view.sanitized_allowed_attributes = ['href', 'title']

    # Layout Changes
    config.to_prepare do
      Devise::SessionsController.layout "devise"
      Devise::PasswordsController.layout "devise"
      Devise::RegistrationsController.layout "devise"
      Devise::InvitationsController.layout "devise"
      Devise::Mailer.layout "mailer"
    end

    config.middleware.delete(ActionDispatch::Cookies)
    config.middleware.delete(ActionDispatch::Session::CookieStore)
    config.middleware.insert_before(Rails::Rack::Logger, ActionDispatch::Session::CookieStore)
    config.middleware.insert_before(ActionDispatch::Session::CookieStore, ActionDispatch::Cookies)

    # Módulo reporte mensual
    WeekOfMonth.configuration.monday_active = true

    # Raven.configure do |config|
    #   config.dsn = 'https://14d971d69c474285aa77075446f66bd1:63071d9eb48f42a591869ad4a5812889@sentry.io/1249349'
    # end

  end
end
