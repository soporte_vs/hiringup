## Unicorn conf hupe
worker_processes 2

working_directory "/home/deploy/hupe/current"

listen "/home/deploy/config/unicorn/.unicorn.sock", :backlog => 64

timeout 420

pid "/home/deploy/hupe/shared/pids/unicorn.pid"

stderr_path "/home/deploy/hupe/shared/log/unicorn.stderr.log"
stdout_path "/home/deploy/hupe/shared/log/unicorn.stdout.log"


preload_app false
GC.respond_to?(:copy_on_write_friendly=) and
  GC.copy_on_write_friendly = true

check_client_connection false

before_fork do |server, worker|
  defined?(ActiveRecord::Base) and
    ActiveRecord::Base.connection.disconnect!
end

after_fork do |server, worker|
  defined?(ActiveRecord::Base) and
    ActiveRecord::Base.establish_connection
end
