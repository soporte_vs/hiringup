# coding: utf-8
Rails.application.routes.draw do
  namespace :minisite do
    resources :banners
  end

  # omniauth with doorkeeper
  use_doorkeeper do
    skip_controllers :applications
  end

  namespace :auth do
    get '/me' => 'credentials#me'
  end

  mount Bootsy::Engine => '/bootsy', as: 'bootsy'
  # Custom Errors
  %w( 404 403 500 ).each do |code|
    get code, :to => "errors#show", :code => code
  end

  # APP
  scope :app do

    get '/', to: 'hupe#index'
    get 'dashboards/general', to: 'dashboards#general'



    resources :scaffolds, only: [:index]

    resources :groups

    namespace :people do
      resources :identification_document_types
    end

    namespace :trabajando do
      resources :careers, only: [:show, :edit, :index, :update]
      resources :universities, only: [:show, :edit, :index, :update]
      resources :job_types, only: [:index,:edit, :update, :show]
      resources :software_levels, only: [:index, :show, :edit, :update]
      resources :education_levels, only: [:index, :show, :edit, :update]
      resources :education_states, only: [:index, :show, :edit, :update]
      resources :applicant_profiles, only: [:show, :edit, :index, :update]
      resources :areas, only: [:index, :show, :edit, :update]
      resources :work_days, only: [:show, :edit, :index, :update]
      resources :company_activities, only: [:show, :edit, :index, :update]
      resources :marital_statuses, only: [:show, :edit, :index, :update]
      resources :countries, only: [:show, :edit, :index, :update]
      resources :states, only: [:show, :edit, :index, :update]
      resources :cities, only: [:show, :edit, :index, :update]
    end

    resources :faqs

    # namespace :minisite do
    #   resources :banners
    # end

    resources :generic_attachments, path: :attachment, only: [:destroy] do
      member do
        get :download
      end
    end

    resources :comment, only: [:index, :create]

    namespace :company do
      resources :areas
      resources :marital_statuses
      resources :vacancy_request_reasons
      resources :records
      resources :contract_types
      resources :positions
      resources :business_units
      resources :cencos
      resources :estates
      resources :recruitment_sources
      resources :timetables
      resources :engagement_origins
      resources :recruitment_sources
      resources :white_ruts do
        collection do
          post :upload_white_list
        end
      end
      resources :employees, only: [:index] do
        collection do
          post :upload_load_massive
          get  :download_file_example
        end
      end
      # resources :references, except: [:destroy, :edit, :update] do
      #   member do
      #     get :confirm
      #   end
      # end
    end


    namespace :video_interview do
      resources :questions
      resources :answers, only: [:show, :index]
    end

    namespace :document do
      resources :descriptors
      resources :groups
      resources :records, only: [:destroy]
    end

    namespace :education do
      resources :study_levels
      resources :institutions do
        collection do
          get :custom
        end

        member do
          put :update_custom
          put :approve_custom
        end
      end

      resources :careers
    end

    namespace :tag do
      resources :course_tags, controller: 'base_tags', type: Tag::CourseTag.to_s
      resources :company_position_tags, controller: 'base_tags', type: Tag::CompanyPositionTag.to_s
    end

    namespace :territory do
      resources :cities
    end

    namespace :territory do
      resources :states
    end

    namespace :territory do
      resources :countries
    end

    namespace :stage do
      resources :not_apply_reasons
    end

    namespace :calendar do
      resources :events, only: [:create, :update] do
        member do
          put :update_for_applicants
          put :mark_attended
          put :mark_will_attend
          get :confirm_will_attend
          get :marked_will_attend
        end

        collection do
          post :create_for_applicants
        end
      end
    end

    namespace :applicant do
      resources :profile_types
      resources :software_levels
      resources :bases, only: [:show, :edit] do
        member do
          put :update
          post :enroll
          post :add_degree
          post :comment
          post :notification_update_profile
          get :generate_cv
          post :upload_documents
          get '/download_document/:name', action: :download_document, as: 'download_document'
          post :update_password
        end
        collection do
          get :index, action: :search
          get :search
          post :_search
          post :enroll
          post :send_update_request
          get :generate_massive_cvs
          post :massive_mailer
        end
      end
      resources :externals, only: [:create, :new]
      resources :catcheds, only: [:create, :new, :edit]
    end

    namespace :worker do
      resources :bases, only: [:create,:new, :show, :edit, :update] do
        member do
          post :add_role
        end
        collection do
          get :index, action: :search
          get :search
        end
      end
    end

    namespace :laborum do
      resources :metadata, only: [], defaults: { format: :json } do
        collection do
          get :areas
          get :subareas
          get :denominaciones
          get :frecuencias_pago
          get :denominaciones
          get :direcciones
          get :idiomas
          get :niveles_idiomas
          get :industrias
          get :tipos_trabajo
          get :areas_estudio
          get :estados_estudio
          get :tipos_estudio
          get :paises
          get :regiones
          get :comunas
        end
      end
    end

    namespace :enrollment do
      resources :bases, only: [] do
        member do
          post :discard

          get :mark_invitation_accepted
          get :mark_invitation_rejected

          resources :offer_letters, only: [], param: :offer_letter_id  do
            member do
              get :accept
              get :show_reject
              post :reject
              get :answered
            end
          end
        end

        collection do
          post :invite
          post :dispose
          post :revert_dispose
          post :next_stage
          get :export
          post :unenroll
        end
      end

      resources :discard_reasons do
        member do
          post :set_as_default_when_close_course
          post :toggle_blacklist
        end
      end
    end

    namespace :hiring_request do
      resources :records, except: [:destroy] do
        member do
          put :switch_state
        end
        collection do
          post :_search
        end
      end
    end

    resources :vacancies, only: [] do
      collection do
        put :assign_course
        put :assign_new_course
      end
    end

    namespace :offer do
      resources :letters, only: [] do
        member do
          post :accept
        end
      end
      resources :reject_reasons
    end

    resources :courses, only: [:create, :new, :show, :edit, :update] do
      member do
        resources :course_postulations, only: [:index] do
          collection do
            post :_search
            post :accept
            post :reject
            post :reset
            get :export
          end
        end

        post :_search, action: :_search_candidates

        # Modulo de VideoInterview
        namespace :video_interview, as: :course_video_interview do
          resources :stages, only: [:show], param: :stage_id do
          end
        end
        # Modulo de Promotion
        namespace :promotion, as: :course_promotion do
          resources :stages, only: [:show, :index], param: :stage_id do
            member do
              post :send_acceptance_email
            end
          end
        end

        # Módulo stage (genérico)
        namespace :stage, as: :course_stage do
          resources :bases, only: [], param: :stage_id do
          end
        end

        # Modulo de manager_interview
        namespace :manager_interview, as: :course_manager_interview do
          resources :stages, only: [:show, :index], param: :stage_id do
            member do
              resources :reports, only: [:create], param: :report_id do
                member do
                  get :download
                end
              end
            end
          end
        end

        # Modulo de skill_interview
        namespace :skill_interview, as: :course_skill_interview do
          resources :stages, only: [:show, :index], param: :stage_id do
            member do
              resources :reports, only: [:create], param: :report_id do
                member do
                  get :download
                end
              end
            end
          end
        end

        # Modulo de assigning_weighting
        namespace :assigning_weighting, as: :course_assigning_weighting do
          resources :stages, only: [:show, :index], param: :stage_id do
            member do
              resources :reports, only: [:create], param: :report_id do
                member do
                  get :download
                end
              end
            end
          end
        end

        # Modulo de internal_postulation
        namespace :internal_postulation, as: :course_internal_postulation do
          resources :stages, only: [:show, :index], param: :stage_id do
            member do
              resources :records, only: [:create], param: :record_id do
                member do
                  get :download
                end
              end
            end
          end
        end

        # Modulo de test
        namespace :test, as: :course_test do
          resources :stages, only: [:show, :index], param: :stage_id do
            member do
              resources :reports, only: [:create], param: :report_id do
                member do
                  get :download
                end
              end
            end
          end
        end

        # Modulo de offer
        namespace :offer, as: :course_offer do
          resources :stages, only: [:show], param: :stage_id do
            member do
              post :create_letter
              post :preview_letter
              post :resend_letter
            end
          end
        end

        get :candidates
        get :publications
        get :detail
        get :security
        get :statistics
        post :clone

        put :update_security
        put :switch_state

        # Modulo de final_interview
        namespace :final_interview, as: :course_final_interview do
          resources :stages, only: [:show, :index], param: :stage_id do
            member do
              resources :reports, only: [:create], param: :report_id do
                member do
                  get :download
                end
              end
            end
          end
        end

        # Modulo de psycholaboral_evaluation
        namespace :psycholaboral_evaluation, as: :course_psycholaboral_evaluation do
          resources :stages, only: [:show, :index], param: :stage_id do
            member do
              resources :reports, only: [:create], param: :report_id do
                member do
                  get :download
                end
              end
              put :finish_stage
            end

            collection do
              post :massive_finish_stage
            end
          end
        end

        # Modulo de group_interview
        namespace :group_interview, as: :course_group_interview do
          resources :stages, only: [:show, :index], param: :stage_id do
            member do
              resources :reports, only: [:create], param: :report_id do
                member do
                  get :download
                end
              end
            end
          end
        end

        # Modulo de technical_test
        namespace :technical_test, as: :course_technical_test do
          resources :stages, only: [:show, :index], param: :stage_id do
            member do
              resources :reports, only: [:create], param: :report_id do
                member do
                  get :download
                end
              end
            end
          end
        end

        # Modulo interview
        namespace :interview, as: :course_interview do
          resources :stages, only: [:show, :index], param: :stage_id do
            member do
              resources :reports, only: [:create], param: :report_id do
                member do
                  get :download
                end
              end
            end
          end
        end

        # Modulo de onboarding
        namespace :onboarding, as: :course_onboarding do
          resources :stages, only: [:show], param: :stage_id do
            member do
              post :do_upload_document
              post :documents_request
              get '/download_document/:name', action: :download_document, as: 'download_document'
              get :upload_requested_documents
              post :do_upload_requested_documents
            end
          end
        end

        # Modulo de Contratación
        namespace :engagement, as: :course_engagement do
          resources :stages, only: [:show], param: :stage_id do
            member do
              resources :agreements, only: [:create, :new, :show, :edit, :update], param: :agreement_id do
                member do
                  get :download
                end
                collection do
                  post :preview_agreement
                end
              end
            end
          end
        end

        namespace :minisite do
          resources :publications, param: :publication_id do
            member do
              put 'update_state/:switch', action: :update_state, as: :update_state
            end
          end
        end

        namespace :trabajando do
          resources :publications, except: [:destroy], param: :publication_id do
            member do
              put 'switch_state/:switch', action: :switch_state, as: :switch_state
            end
          end
        end

        namespace :laborum do
          resources :publications, param: :publication_id
        end
      end

      collection do
        get :index, action: :search
        get :search
        post :_search
      end
    end

    namespace :laborum do
      resources :postulations, only: [:show] do
        member do
          put :accept
          put :discard
        end
      end
    end

    namespace :minisite do
      resources :categories
    end


    namespace :report do
      resources :vacancies_distributions, only: [:new] do
        collection do
          get :report
        end
      end

      resource :transparency_information, only: [:new], controller: 'transparency_information' do
        get '/:month/:year/generate', action: :generate, as: 'generate'
        get '/:month/:year/download_publications', action: :download_publications, as: 'download_publications'
      end

      resources :vacancies_evolution, only: [:new] do
        collection do
          get :report
        end
      end

      resources :process_tracking, only: [:new] do
        collection do
          get :report
          post :generate
        end
      end

      resources :applicants, only: [:new] do
        member do
          get :download
        end
        collection do
          get :report
          post :generate
        end
      end

      resources :general_per_month, only: [:new] do
        collection do
          get :report
        end
      end
    end
  end

  namespace :evaluar do
    resources :courses, only: [], param: :evaluar_course_id do
      member do
        post :create_in_evaluar
        post :results
      end
    end
  end

  root 'minisite/site#index'

  #url shortener
  get '/s/:id', to: "shortener/shortened_urls#show"
  #minisite
  get '/:id', to: 'minisite/publications#job', as: :job
  post '/:id', to: 'minisite/postulations#create', as: :postulate_job
  get '/categoria/:id', to: 'minisite/site#category', as: :category_job
  get '/categoria/:id/search', to: 'minisite/site#search', as: :search_job
  get '/support/faqs', to: 'support#faqs'

  devise_for :users

  devise_scope :users do
    get "/users/sign_up", to: "applicant/externals#new", as: "new_registration"
  end

  resources :profiles, only: [] do
    collection do
      post :upload_documents
      get '/download_document/:name', action: :download_document, as: 'download_document'
      get :edit
      put :update
    end
  end
end
