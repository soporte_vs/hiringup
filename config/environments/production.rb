

Rails.application.configure do

  config.cache_classes = true
  config.eager_load = true

  config.consider_all_requests_local       = false
  config.action_controller.perform_caching = true

  config.serve_static_files = false

  config.assets.js_compressor = :uglifier

  config.assets.compile = false

  config.assets.digest = true

  config.log_level = :info

  config.active_support.deprecation = :notify

  config.log_formatter = ::Logger::Formatter.new

  config.active_record.dump_schema_after_migration = false

  # Set hostname
  config.hostname = 'integra'

  # Elastic Search Server IP
  #config.elastic_server = '192.168.197.229:9200'
  config.elastic_server = 'localhost:9200'

  # Mailer configuration default
  config.action_mailer.smtp_settings = {
    smtp_enable: true,
    address: "smtp.gmail.com",
    port: 587,
    domain: "hiringup.com",
    user_name: "integratest4@gmail.com",
    password: "Int3gra@19.",
    authentication: "plain",
    enable_starttls_auto: true
  }

  config.action_mailer.delivery_method = :smtp

  config.action_mailer.default_url_options = { :host => "trabajaconnosotros.integra.cl", :protocol => 'https'}
  config.action_mailer.asset_host = "https://trabajaconnosotros.integra.cl"

  # SSL
  config.force_ssl = false

  # Exception Notification
  Rails.application.config.middleware.use ExceptionNotification::Rack,
    :ignore_crawlers => %w{Googlebot bingbot bot spider wget curl Yandex baidu},
    :email => {
      :email_prefix => "[HUPE-trabajaconnosotros.integra.cl #{Rails.env} error] ",
      :sender_address => %{"Exception Notifier" <contact@hiringup.com>},
      :exception_recipients => %w{rysnoreply@fundacion.integra.cl}
    }
  config.react.variant = :production
  config.react.addons = true
end
