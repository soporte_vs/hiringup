# coding: utf-8
source 'https://rubygems.org'
ruby "2.2.2"

gem 'rails', '4.2'

# Use SCSS for stylesheets
gem 'sass-rails'

# gem "sentry-raven"

# Bootstrap
gem 'bootstrap-sass', '~> 3.3.5'

# Add vendor prefixes on assets compile.
gem "autoprefixer-rails"

# OAuth server
gem 'doorkeeper'

# Default database PG
gem 'pg'

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier'

# Use jquery as the JavaScript library
gem 'jquery-rails'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder'

# render html
gem 'slim-rails'

# dejar procesos en cola
gem 'delayed_job_active_record'

# ejecucion automatica de tareas RAKE
gem 'whenever', require: false

# es para que corra delayed Job
gem 'daemons'

# permite crear migraciones de datos
gem 'seed_migration'

# Sistema de usuarios
gem 'devise', :git => 'https://github.com/plataformatec/devise.git'
gem 'devise_invitable'

# Sistema de Roles
gem 'rolify'

# integracion bumera(laborum)
gem 'bumeran', git: 'https://github.com/ElamT/bumeran.git'

#React
gem 'react-rails', '~> 1.6.0'
gem "jasper-rails", github: 'ElamT/jasper-rails'


# PDF
gem 'wicked_pdf'

# ics calendar support
gem 'icalendar', '2.3.0'

# Users Activities
gem 'public_activity'

# Paginate
gem 'kaminari'

# file uploader
gem 'carrierwave'
gem "mini_magick"
gem 'validates_overlap'

# elastic-search
gem 'elasticsearch-model', '~> 0.1.9'
gem 'elasticsearch-rails', '~> 0.1.9'

# Exception notification
gem 'exception_notification'

# WYSIWYG
gem 'bootsy'

# Sanitize para sacar los tags html que no acepta Trabajando
gem 'sanitize'

# Browser Detection
gem 'browser'

# Link Shortener
gem 'shortener'

gem 'paper_trail'

# Chartkick
gem 'chartkick'
gem 'groupdate'
gem 'active_median'

# State Machine
gem 'aasm'
gem 'zip-zip'
gem 'rubyzip', '>= 1.0.0'
gem 'axlsx_rails'
gem 'roo'
# end state machine

#Linkedin
gem 'omniauth'
gem 'omniauth-linkedin'

# Reporte mensual
gem 'week_of_month'

group :development, :test do
  gem "json_matchers"
  # Use debugger
  gem 'byebug'
  # test
  gem 'rspec-rails'
  # permite crear datos para los test
  gem 'factory_girl_rails'
  # helper para los test
  gem 'shoulda-matchers', require: false

  gem 'ffaker'
  #test en parallelo
  gem "parallel_tests"
  #Headless
  gem 'headless'
  #Rspec retry
  gem 'rspec-retry'
  #Foreman
  gem 'foreman'
end


group :development do
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'

  # deploy de la aplicacion
  gem 'capistrano', '2.15.4'
  gem 'capistrano-ext', '1.2.1'
  gem 'rvm-capistrano', '1.3.1', :require => false
  gem 'capistrano-unicorn', '0.1.9' , :require => false
  gem 'capistrano-measure', :require => false


  # agregar en la cabecera del modelo los atributos de este
  gem 'annotate', github: 'ctran/annotate_models'

  # recarga el navegador automaticamente al editar un css o vista.
  gem 'guard-livereload'
  # Meta request for rails panel
  gem 'meta_request'

  # Paint ActiveRecord queries
  gem 'activerecord-colored_log_subscriber'

end

group :test do
   #Capybara Test EndToEnd
  gem 'capybara'
  gem 'selenium-webdriver'
  gem "chromedriver-helper"
  gem 'database_rewinder'
  gem 'simplecov'
  gem 'webmock'
end

# Use unicorn as the app server
group :production do
  gem 'unicorn'
end

#Develop new server
gem 'puma'

#gem 'f4_camera', :git => 'git@github.com:4Talent/F4Camera-rails.git'
#gem 'f4_camera', :git => 'https://github.com/4Talent/F4Camera-rails.git'

gem 'simple_token_authentication', '~> 1.0'

#Crea css compatible para IE8
gem 'css_splitter'

#Cocoon is form builder-agnostic
gem 'cocoon'

gem 'httparty'

gem 'figaro'

