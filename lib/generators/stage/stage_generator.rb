class StageGenerator < Rails::Generators::NamedBase
  source_root File.expand_path('../templates', __FILE__)
  def generate_stage
    tableized_name = name.tableize.singularize
    template "stages_controller.rb", "app/controllers/#{tableized_name}/stages_controller.rb"
    template "stage.rb", "app/models/#{tableized_name}/stage.rb"
    template "index.html.slim", "app/views/#{tableized_name}/stages/index.html.slim"
    template "show.html.slim", "app/views/#{tableized_name}/stages/show.html.slim"
    template "stage_helper.rb", "app/helpers/#{tableized_name}/stage_helper.rb"
    template "stages_controller_spec.rb", "spec/controllers/#{tableized_name}/stages_controller_spec.rb"
    template "stage_spec.rb", "spec/models/#{tableized_name}/stage_spec.rb"
    template "stage_helper_spec.rb", "spec/helpers/#{tableized_name}/stage_helper_spec.rb"
    template "stages.rb", "spec/factories/#{tableized_name}/stages.rb"
    template "_created.html.slim", "app/views/public_activity/course/#{tableized_name}_stage/_created.html.slim"
    template "es.yml", "config/locales/es/#{tableized_name}.es.yml"
    template "en.yml", "config/locales/en/#{tableized_name}.en.yml"

    stage_route = <<END
        # Modulo de #{name}
        namespace :#{tableized_name}, as: :course_#{tableized_name} do
          resources :stages, only: [:show, :index], param: :stage_id do
            collection do
              post :massive_finish_stage
            end

            member do
              put :finish_stage
            end
          end
        end
END

    gsub_file('config/routes.rb', /\s*(resources :courses, only: \[.+\] do$*\s+member do)/) do |m|
      m << "\n" << stage_route
    end
  end
end
