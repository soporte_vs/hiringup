class <%= name.classify %>::Stage <  Stage::Base
  ROLES_CLASS = [
    {name: :admin, description: I18n.t("<%= name.tableize.singularize %>.stage.roles.class.admin")}
  ]

  def roles_instance
    [
      {name: :admin, description: I18n.t("<%= name.tableize.singularize %>.stage.roles.instance.admin")},
    ]
  end
end