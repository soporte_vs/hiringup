# coding: utf-8
require 'rails_helper'

RSpec.describe <%= name.classify %>::StagesController, :type => :controller do
  render_views

    describe 'PUT finish stage' do
    login_executive_without_role
    before :each do
      enrollment = FactoryGirl.create(:enrollment_basis_with_applicant)
      work_flow = [
        {type: :module, class_name: 'Stage::Base', name: 'Etapa'},
        {type: :module, class_name: '<%= name.classify %>::Stage', name: 'Onboarding'},
        {type: :module, class_name: 'Stage::Base', name: 'Etapa'}
      ]
      enrollment.course.update(work_flow: work_flow)
      enrollment.init_work_flow
      enrollment.next_stage
      enrollment.course.save
      enrollment.reload
      @<%= name.tableize.singularize %>_stage = enrollment.stage
      @<%= name.tableize.singularize %>_stage.enrollment.reload
    end

    context 'when user has role' do
      context 'and this is the last stage' do
        before :each do
          @course = @<%= name.tableize.singularize %>_stage.enrollment.course
          work_flow = [
            {type: :module, class_name: '<%= name.classify %>::Stage', name: 'Onboarding'}
          ]
          @course.update(work_flow: work_flow)
          @stage_expected = nil
        end

        it "user has role admin" do
          @user.add_role :admin
        end

        it 'user has role admin on <%= name.classify %>::Stage' do
          @user.add_role :admin, <%= name.classify %>::Stage
        end

        it "user has role #{<%= name.classify %>::Stage.to_s.tableize.to_sym} on course" do
          @user.add_role <%= name.classify %>::Stage.to_s.tableize.to_sym, @course
        end

        it "user has role admin on course" do
          @user.add_role :admin, @course
        end

        it "user has role admin on @<%= name.tableize.singularize %>_stage" do
          @user.add_role :admin, @<%= name.tableize.singularize %>_stage
        end


        after :each do
          expect{
            put :finish_stage, id: @<%= name.tableize.singularize %>_stage.enrollment.course.id, stage_id: @<%= name.tableize.singularize %>_stage.id
          }.to change(Stage::Base, :count).by(0).and change(PublicActivity::Activity, :count).by(1)

          @<%= name.tableize.singularize %>_stage.enrollment.reload
          expect(@<%= name.tableize.singularize %>_stage.enrollment.stage).to eq nil

          public_activity = PublicActivity::Activity.last
          expect(public_activity.trackable).to eq @<%= name.tableize.singularize %>_stage.enrollment.course
          expect(public_activity.recipient).to eq @<%= name.tableize.singularize %>_stage.enrollment
          expect(
            public_activity.key
          ).to eq 'course.enrollment.work_flow_finished'
        end
      end

      context 'and this is not the last stage' do
        before :each do
          @course = @<%= name.tableize.singularize %>_stage.enrollment.course
          @stage_expected = @<%= name.tableize.singularize %>_stage.enrollment.next_stage_class[:class_name].constantize
        end

        it "user has role admin" do
          @user.add_role :admin
        end

        it 'user has role admin on <%= name.classify %>::Stage' do
          @user.add_role :admin, <%= name.classify %>::Stage
        end

        it "user has role #{<%= name.classify %>::Stage.to_s.tableize.to_sym} on course" do
          @user.add_role <%= name.classify %>::Stage.to_s.tableize.to_sym, @course
        end

        it "user has role admin on course" do
          @user.add_role :admin, @course
        end

        it "user has role admin on @<%= name.tableize.singularize %>_stage" do
          @user.add_role :admin, @<%= name.tableize.singularize %>_stage
        end


        after :each do
          expect{
            put :finish_stage, id: @<%= name.tableize.singularize %>_stage.enrollment.course.id, stage_id: @<%= name.tableize.singularize %>_stage.id
          }.to change(@stage_expected, :count).by(1).and change(PublicActivity::Activity, :count).by(1)
          @<%= name.tableize.singularize %>_stage.enrollment.reload
          expect(@<%= name.tableize.singularize %>_stage.enrollment.stage).to eq @stage_expected.last

          public_activity = PublicActivity::Activity.last
          expect(public_activity.trackable).to eq @<%= name.tableize.singularize %>_stage.enrollment.course
          expect(public_activity.recipient).to eq @<%= name.tableize.singularize %>_stage.enrollment

          expect(
            public_activity.key
          ).to eq 'course.'.concat(@stage_expected.to_s.tableize.singularize.gsub('/','_').concat('.created'))
        end
      end
    end

    context "when enrollment.stage is not same that engagement_stage" do
      it 'should create a new next Stage' do
        @user.add_role :admin
        <%= name.tableize.singularize %>_stage = FactoryGirl.create(:<%= name.tableize.singularize %>_stage)
        @<%= name.tableize.singularize %>_stage.enrollment.update(stage: <%= name.tableize.singularize %>_stage)
        course = @<%= name.tableize.singularize %>_stage.enrollment.course
        course.work_flow.push(course.work_flow.first)
        course.save
        expect{
          put :finish_stage, id: @<%= name.tableize.singularize %>_stage.enrollment.course.id, stage_id: @<%= name.tableize.singularize %>_stage.id
        }.to change(Stage::Base, :count).by(0)
      end
    end
  end

  describe 'Massive Finish Stage' do
    before :each do
      work_flow = [
        {type: :module, class_name: '<%= name.classify %>::Stage', name: 'Etapa'},
        {type: :module, class_name: 'Stage::Base', name: 'Etapa'}
      ]
      @course = FactoryGirl.create(:course)
      @course.update(work_flow: work_flow)
      enrollments = FactoryGirl.create_list(:enrollment_basis_with_applicant, 3, course: @course)
      @stages = []
      enrollments.each do |enrollment|
        enrollment.init_work_flow
        @stages.push(enrollment.stage)
      end
      extra_stage_no_next_stage = FactoryGirl.create(:stage_basis)
      extra_stage_no_next_stage.enrollment.update(stage: extra_stage_no_next_stage)
      @stages.push(extra_stage_no_next_stage)

      extra_stage = FactoryGirl.create(:stage_basis)
      extra_stage.enrollment.update(stage: extra_stage)
      extra_stage.enrollment.course.update(work_flow: work_flow)
      @stages.push(extra_stage)
      @stages_target = @stages
    end

    context 'when current_user is not signed in' do
      it 'should redirect_to new_user_session_path' do
        expect{
          post :massive_finish_stage, id: @course.to_param, stage_ids: @stages.map(&:id)
        }.to change(Stage::Base, :count).by(0)
        should redirect_to new_user_session_path
      end
    end

    context 'when current_user is signed in' do
      login_executive_without_role

      context 'with role valid' do
        it 'admin role should finish stage' do
          @user.add_role :admin
        end

        it 'admin role on @course' do
          @user.add_role :admin, @course
        end

        it "#{<%= name.classify %>::Stage.to_s.tableize.to_sym} role on @course" do
          @user.add_role <%= name.classify %>::Stage.to_s.tableize.to_sym, @course
        end

        it "admin role on allmost stages" do
          # De los ultimos 3 stages solo hay uno valido
          @stages_target = @stages.last(3)
          @stages_target.each do |stage|
            @user.add_role :admin, stage
          end
        end

        after :each do
          expect{
            post :massive_finish_stage, id: @course.to_param, stage_ids: @stages.map(&:id)
          }.to change(Stage::Base, :count).by(@stages_target.size - 2)
          should redirect_to candidates_course_path(id: @course.id)
        end
      end

    end
  end

  describe 'GET show stage' do
    before :each do
      @<%= name.tableize.singularize %>_stage = FactoryGirl.create(:<%= name.tableize.singularize %>_stage)
      @course = @<%= name.tableize.singularize %>_stage.enrollment.course
      work_flow = [
        {type: :module, class_name: '<%= name.classify %>::Stage', name: '<%= name.classify %>'},
        {type: :module, class_name: 'Onboarding::Stage', name: 'Onboarding'},
        {type: :module, class_name: 'Engagement::Stage', name: 'Contratación'}
      ]
      @course.update(work_flow: work_flow)
    end

    context 'when current_user is not loged in' do
      it 'should redirect new_user_session_path' do
        get :show, id: @course.to_param, stage_id: @<%= name.tableize.singularize %>_stage.to_param
        should redirect_to new_user_session_path
      end
    end

    context 'when current_user is loged in' do
      login_executive_without_role

      context 'and has role valid' do
        it 'should response ok with role :admin' do
          @user.add_role :admin
        end

        it 'should response ok with role admin on <%= name.classify %>::Stage' do
          @user.add_role :admin, <%= name.classify %>::Stage
        end

        it "should response ok with role :admin on @course" do
          @user.add_role :admin, @course
        end

        it "should response ok with role #{<%= name.classify %>::Stage.to_s.tableize.to_sym} on @course" do
          @user.add_role <%= name.classify %>::Stage.to_s.tableize.to_sym, @course
        end

        after :each do
          get :show, id: @course.to_param, stage_id: @<%= name.tableize.singularize %>_stage.to_param
          expect(assigns(:stage)).to eq @<%= name.tableize.singularize %>_stage
          should render_template('<%= name.tableize.singularize %>/stages/show')
        end
      end

      context 'and does not has role valid' do
        it 'shoul response with page 403 when current_user has not role' do
        end

        after :each do
          get :show, id: @course.to_param, stage_id: @<%= name.tableize.singularize %>_stage.to_param
          expect(response.status).to eq 403
        end
      end
    end
  end

  describe 'GET index stages' do
    before :each do
      work_flow = [
        {type: :module, class_name: '<%= name.classify %>::Stage', name: '<%= name.classify %>'},
        {type: :module, class_name: 'Onboarding::Stage', name: 'Onboarding'},
        {type: :module, class_name: 'Engagement::Stage', name: 'Contratación'}
      ]
      @course = FactoryGirl.create(:course)
      @course.update(work_flow: work_flow)
      enrollments = FactoryGirl.create_list(:enrollment_basis_with_applicant, 3, course: @course)
      @<%= name.tableize.singularize %>_stages = []
      enrollments.each do |enrollment|
        @<%= name.tableize.singularize %>_stages.push(
          FactoryGirl.create(:<%= name.tableize.singularize %>_stage, enrollment: enrollment)
        )
      end
    end

    context 'when current_user is not loged in' do
      it 'should redirect new_user_session_path' do
        get :index, id: @course.to_param
        should redirect_to new_user_session_path
      end
    end

    context 'when current_user is loged in' do
      login_executive_without_role

      context 'and has a role valid' do
        it 'should response ok when current_user has role admin' do
          @user.add_role :admin
        end

        it 'should response ok when current_user has role admin on @course' do
          @user.add_role :admin, @course
        end

        it 'should response ok when current_user has role admin on <%= name.classify %>::Stage' do
          @user.add_role :admin, <%= name.classify %>::Stage
        end

        it 'should response ok when current_user has role admin on @<%= name.tableize.singularize %>_stage' do
          @user.add_role :admin, @<%= name.tableize.singularize %>_stage
        end

        it "should response ok when current_user has role #{<%= name.classify %>::Stage.to_s.tableize.to_sym} on @coruse" do
          @user.add_role <%= name.classify %>::Stage.to_s.tableize.to_sym, @course
        end

        after :each do
          get :index, id: @course.id
          expect(assigns(:stages)).to eq @<%= name.tableize.singularize %>_stages
          should render_template('<%= name.tableize.singularize %>/stages/index')
        end
      end

      context 'and has a role invalid' do
        it 'should response with page 403 when current_user has not any role' do
        end

        after :each do
          get :index, id: @course.id
          expect(response.status).to eq 403
        end
      end
    end
  end
end
