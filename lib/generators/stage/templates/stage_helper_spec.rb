require 'rails_helper'

RSpec.describe <%= name.classify %>::StageHelper, type: :helper do
  context 'method html_preview' do
    it 'should render link to <%= name.classify %>' do
      <%= name.tableize.singularize %>_stage = FactoryGirl.create(:<%= name.tableize.singularize %>_stage)
      @result = <%= name.classify %>::StageHelper.html_preview(view, <%= name.tableize.singularize %>_stage)
      @link = view.link_to(
        view.t('<%= name.tableize.singularize %>.stage.title'),
        view.polymorphic_path([<%= name.tableize.singularize %>_stage.enrollment.course, <%= name.tableize.singularize %>_stage]),
        class: 'btn-link'
      )
    end

    after :each do
      html = <<-HTML
      <div>
        #{@link}
      </div>
      HTML
      expect(@result.gsub(' ','')).to eq html.gsub(' ','')
    end
  end
end