FactoryGirl.define do
  factory :<%= name.tableize.singularize %>_stage, :class => '<%= name.classify %>::Stage' do
     enrollment { FactoryGirl.create(:enrollment_basis_with_applicant) }
  end
end