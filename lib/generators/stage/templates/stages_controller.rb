class <%= name.classify %>::StagesController < Stage::BasesController
  private
    def catch_stages
      @stages = <%= name.classify %>::Stage.joins(:enrollment).where(enrollment_bases: {course_id: @course.id}, step: params[:step]).joins(enrollment: :course).page(params[:page])
    end

    def authorize_action
      unless has_permission?
        render 'errors/403', status: 403, layout: 'application'
      end
    end

    def has_permission?(stage=nil)
      current_user.can?(:admin, @stage || stage) ||
      current_user.can?(<%= name.classify %>::Stage.to_s.tableize.to_sym, @course) ||
      current_user.can?(:admin, <%= name.classify %>::Stage)
    end
end
