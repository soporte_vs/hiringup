require 'rails_helper'

RSpec.describe <%= name.classify %>::Stage, :type => :model do
  context 'Create' do
    it 'a valid <%= name.tableize.singularize %>' do
      <%= name.tableize.singularize %>_stage = FactoryGirl.create(:<%= name.tableize.singularize %>_stage)
      expect(<%= name.tableize.singularize %>_stage).to be_valid
      expect(<%= name.tableize.singularize %>_stage).to be_a(<%= name.classify %>::Stage)
    end
  end
end