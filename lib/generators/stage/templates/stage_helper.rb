module <%= name.classify %>::StageHelper
  class << self
    def html_preview(view, <%= name.tableize.singularize %>_stage)
      return "" if <%= name.tableize.singularize %>_stage.nil?

      message = view.t('<%= name.tableize.singularize %>.stage.title')

      link = view.link_to(
        message,
        view.polymorphic_path([<%= name.tableize.singularize %>_stage.enrollment.course,<%= name.tableize.singularize %>_stage]),
        class: 'btn-link'
      )
      html = <<-HTML
      <div>
        #{link}
      </div>
      HTML

      html.html_safe
    end
  end
end