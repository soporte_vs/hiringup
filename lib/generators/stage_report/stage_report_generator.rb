class StageReportGenerator < Rails::Generators::NamedBase
  include Rails::Generators::Migration
  source_root File.expand_path('../templates', __FILE__)

  def self.next_migration_number(dir)
    Time.now.utc.strftime("%Y%m%d%H%M%S")
  end

  def start
    tableized_name = name.tableize.singularize

    migration_template "migration.rb", "db/migrate/create_#{tableized_name}_reports.rb"
    template "report.rb", "app/models/#{tableized_name}/report.rb"
    template "namespace.rb", "app/models/#{tableized_name}.rb"
    template "factory.rb", "spec/factories/#{tableized_name}/reports.rb"
    template "report_spec.rb", "spec/models/#{tableized_name}/report_spec.rb"

    template "reports_controller.rb", "app/controllers/#{tableized_name}/reports_controller.rb"
    template "reports_controller_spec.rb", "spec/controllers/#{tableized_name}/reports_controller_spec.rb"

    template "show.html.slim", "app/views/#{tableized_name}/reports/_show.html.slim"
    template "form.html.slim", "app/views/#{tableized_name}/reports/_form.html.slim"

    template "report.es.yml", "config/locales/es/#{tableized_name}_report.es.yml"
    template "report.en.yml", "config/locales/en/#{tableized_name}_report.en.yml"

    stage_report_route = "              resources :reports, only: [:create]"

    gsub_file('config/routes.rb', /\s*(namespace :hola, as: :course_hola do\s*$\s+ resources :stages, only: \[.+\], param: :stage_id do$\s+ member do)/) do |m|
      m << "\n" << stage_report_route
    end

    relationship_report = <<END
  has_one :#{tableized_name}_report,
          class_name: #{name.classify}::Report,
          foreign_key: :#{tableized_name}_stage_id,
          dependent: :destroy
END

    gsub_file("app/models/#{tableized_name}/stage.rb", /class\s+#{name.classify}::Stage\s+<\s+Stage::Base$/) do |m|
      m << "\n" << relationship_report
    end

    test_relationship_report = <<END
  context "RelationShips" do
    it 'should has_one #{tableized_name}_report' do
      should have_one(:#{tableized_name}_report).dependent(:destroy)
    end
  end
END

  gsub_file("spec/models/#{tableized_name}/stage_spec.rb", /RSpec.describe\s+#{name.classify}::Stage,\s+:type\s+=>\s+:model\s+do$/) do |m|
      m << "\n" << test_relationship_report
    end
  end
end
