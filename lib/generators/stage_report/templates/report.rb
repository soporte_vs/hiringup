class <%= name.classify %>::Report < ActiveRecord::Base
  after_commit :touch_enrollment, on: [:create, :update]
  belongs_to :<%= name.tableize.singularize %>_stage,
             class_name: <%= name.classify %>::Stage,
             foreign_key: :<%= name.tableize.singularize %>_stage_id

  validates :observations, presence: true
  validates :<%= name.tableize.singularize %>_stage, presence: true

  def touch_enrollment
    self.<%= name.tableize.singularize %>_stage.touch_enrollment
  end
end