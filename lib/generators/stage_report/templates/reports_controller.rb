class <%= name.classify %>::ReportsController < <%= name.classify %>::StagesController
  def create
    @<%= name.tableize.singularize %>_report = <%= name.classify %>::Report.new(report_params)
    if @<%= name.tableize.singularize %>_report.save
      flash[:success] = I18n.t('<%= name.tableize.singularize %>.report.created.success')
    else
      flash[:error] = @<%= name.tableize.singularize %>_report.errors.messages
    end
    redirect_to course_<%= name.tableize.singularize %>_stage_path(id: @course, stage_id: @stage.id)
  end

  private

    def report_params
      params.require(:<%= name.tableize.singularize %>_report).permit(:observations).merge(<%= name.tableize.singularize %>_stage_id: @stage.id)
    end
end