class Create<%= name.classify %>Reports < ActiveRecord::Migration
  def change
    create_table :<%= name.tableize.singularize %>_reports do |t|
      t.text :observations
      t.references :<%= name.tableize.singularize %>_stage, index: true

      t.timestamps null: false
    end
    add_foreign_key :<%= name.tableize.singularize %>_reports, :stage_bases, column: :<%= name.tableize.singularize %>_stage_id
  end
end