require 'rails_helper'

RSpec.describe <%= name.classify %>::ReportsController, :type => :controller do
  render_views
  let(:work_flow) {
    work_flow = [
      {type: :module, class_name: '<%= name.classify %>::Stage', name: '<%= name.classify %>'},
      {type: :module, class_name: 'Onboarding::Stage', name: 'Onboarding'},
      {type: :module, class_name: 'Engagement::Stage', name: 'Contratación'}
    ]
  }

  describe 'POST create <%= name.classify %> Report' do
    before :each do
      @<%= name.tableize.singularize %>_report_attribtues = FactoryGirl.attributes_for(:<%= name.tableize.singularize %>_report)
      @<%= name.tableize.singularize %>_stage = <%= name.classify %>::Stage.find(@<%= name.tableize.singularize %>_report_attribtues[:<%= name.tableize.singularize %>_stage_id])
      @course = @<%= name.tableize.singularize %>_stage.enrollment.course
      @course.update(work_flow: work_flow)
    end

    context 'when current_user is not loged in' do
      it 'should redirect_to new_user_session_path' do
        expect{
          post :create,
               id: @course.to_param,
               stage_id: @<%= name.tableize.singularize %>_stage.to_param,
               <%= name.tableize.singularize %>_report: @<%= name.tableize.singularize %>_report_attribtues
        }.to change(<%= name.classify %>::Report, :count).by(0)
        should redirect_to new_user_session_path
      end
    end

    context 'when current_user is loged in' do
      login_executive_without_role
      context 'and he not has role valid' do
        it 'should response with page 403' do
          expect{
            post :create,
                 id: @course.to_param,
                 stage_id: @<%= name.tableize.singularize %>_stage.to_param,
                 <%= name.tableize.singularize %>_report: @<%= name.tableize.singularize %>_report_attribtues
          }.to change(<%= name.classify %>::Report, :count).by(0)
          expect(response.status).to eq 403
        end
      end

      context 'and he has a valid role' do
        context 'with valid attributes' do
          it 'with admin role' do
            @user.add_role :admin
          end

          it 'with admin role on @course' do
            @user.add_role :admin, @course
          end

          it 'with admin role on <%= name.classify %>::Stage' do
            @user.add_role :admin, <%= name.classify %>::Stage
          end

          it "with #{<%= name.classify %>::Stage.to_s.tableize.to_sym} on @course" do
            @user.add_role <%= name.classify %>::Stage.to_s.tableize.to_sym, @course
          end

          it "with :admin on @<%= name.tableize.singularize %>_stage" do
            @user.add_role :admin, @<%= name.tableize.singularize %>_stage
          end

          after :each do
            expect{
              post :create,
                   id: @course.to_param,
                   stage_id: @<%= name.tableize.singularize %>_stage.to_param,
                   <%= name.tableize.singularize %>_report: @<%= name.tableize.singularize %>_report_attribtues
            }.to change(<%= name.classify %>::Report, :count).by(1)
            should redirect_to course_<%= name.tableize.singularize %>_stage_path(id: @course, stage_id: @<%= name.tableize.singularize %>_stage.id)
            expect(flash[:success]).to eq I18n.t('<%= name.tableize.singularize %>.report.created.success')
          end
        end

        context 'with invalid attributes' do
          before :each do
            @<%= name.tableize.singularize %>_report_attribtues = FactoryGirl.attributes_for(:<%= name.tableize.singularize %>_report_invalid)
            @<%= name.tableize.singularize %>_stage = <%= name.classify %>::Stage.find(@<%= name.tableize.singularize %>_report_attribtues[:<%= name.tableize.singularize %>_stage_id])
            @course = @<%= name.tableize.singularize %>_stage.enrollment.course
            @course.update(work_flow: work_flow)
          end

          it 'with admin role' do
            @user.add_role :admin
          end

          it 'with admin role on @course' do
            @user.add_role :admin, @course
          end

          it 'with admin role on <%= name.classify %>::Stage' do
            @user.add_role :admin, <%= name.classify %>::Stage
          end

          it "with #{<%= name.classify %>::Stage.to_s.tableize.to_sym} on @course" do
            @user.add_role <%= name.classify %>::Stage.to_s.tableize.to_sym, @course
          end

          it "with :admin on @<%= name.tableize.singularize %>_stage" do
            @user.add_role :admin, @<%= name.tableize.singularize %>_stage
          end

          after :each do
            expect{
              post :create,
                   id: @course.to_param,
                   stage_id: @<%= name.tableize.singularize %>_stage.to_param,
                   <%= name.tableize.singularize %>_report: @<%= name.tableize.singularize %>_report_attribtues
            }.to change(<%= name.classify %>::Report, :count).by(0)
            should redirect_to course_<%= name.tableize.singularize %>_stage_path(id: @course, stage_id: @<%= name.tableize.singularize %>_stage.id)
            expect(flash[:error]).to match_array [[:observations, [I18n.t('activerecord.errors.models.<%= name.tableize.singularize %>/report.attributes.observations.blank')]]]
          end
        end

      end
    end
  end
end