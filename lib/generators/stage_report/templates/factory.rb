FactoryGirl.define do
  factory :<%= name.tableize.singularize %>_report, :class => '<%= name.classify %>::Report' do
    observations { FFaker::Lorem.paragraph }
    <%= name.tableize.singularize %>_stage_id { FactoryGirl.create(:<%= name.tableize.singularize %>_stage).id }
  end

  factory :<%= name.tableize.singularize %>_report_invalid, :class => '<%= name.classify %>::Report' do
    observations ''
    <%= name.tableize.singularize %>_stage_id { FactoryGirl.create(:<%= name.tableize.singularize %>_stage).id }
  end
end