module <%= name.classify %>
  def self.table_name_prefix
    '<%= name.tableize.singularize %>_'
  end
end
