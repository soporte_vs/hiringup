require 'rails_helper'

RSpec.describe <%= name.classify %>::Report, type: :model do
  context 'RelationShips' do
    it 'should belong a <%= name.tableize.singularize %>_stage' do
      should belong_to :<%= name.tableize.singularize %>_stage
    end
  end

  context 'Validations' do
    it 'observations should be present' do
      should validate_presence_of :observations
    end

    it '<%= name.tableize.singularize %>_stage should be present' do
      should validate_presence_of :<%= name.tableize.singularize %>_stage
    end
  end

  context 'Create' do
    it 'should create a valid' do
      expect(
        FactoryGirl.create(:<%= name.tableize.singularize %>_report)
      ).to be_valid
    end

    it 'should build an invalid' do
      expect(
        FactoryGirl.build(:<%= name.tableize.singularize %>_report_invalid)
      ).to_not be_valid
    end
  end
end