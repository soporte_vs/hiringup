# Requiere que instancia tenga a @applicant
module ApplicantDocumentActions

  def upload_documents
    do_upload_documents
    redirect_to :back
  end

  def download_document
    if params[:record_id]
      desc = Document::Descriptor.find_by_name(params[:name])
      # Es estricto, debe coincidir el id con el descriptor
      document = @applicant.document_records.find_by(id: params[:record_id], document_descriptor: desc).document rescue nil
    else
      # La instancia que incluyo este modulo puede o no tener un enrollment. Esto cubre los dos casos: profile (sin enrollment) y stage (con enrollment)
      enrollment_id=nil
      if @enrollment
        enrollment_id = @enrollment.id
      end
      document = @applicant.get_latest_document_record(params[:name], enrollment_id).document rescue nil
    end


    if document
      send_file document.path, filename: document.filename
    else
      render 'errors/404', status: 404, layout: 'application'
    end
  end

  private
    def do_upload_documents
      documents = upload_documents_params
      records = []
      if documents.empty?
        flash[:error] = t("applicant.upload_documents.upload_error.invalid_args")
      else
        enrollment_id = nil
        if @enrollment
          enrollment_id = @enrollment.id
        end
        documents.each_key do |name|
          record = @applicant.create_document_record(name.to_s, enrollment_id)
          record.document = documents[name]
          if record.save
            flash[:success] = t("applicant.upload_documents.upload_success")
            records << record
          else
            flash[:error] = record.errors
            record.delete
          end
        end
      end
      records
    end

    def do_upload_documents_ajax
      documents = upload_documents_params
      records = []

      enrollment_id = nil
      if @enrollment
        enrollment_id = @enrollment.id
      end
      documents.each_key do |name|
        record = @applicant.create_document_record(name.to_s, enrollment_id)
        record.document = documents[name]
        unless record.save
          record.delete
        end
        records << record
      end
      records
    end

    def upload_documents_params
      if params[:applicant_documents].present?
        params.require(:applicant_documents).permit(*get_permitted_documents)
      else
        {}
      end
    end

    def get_permitted_documents
      Document::Descriptor.all.map do |desc|
        desc.to_sym
      end
    end
end
