# lib/tasks/manage.rake
# Management tasks
namespace :manage do
  desc "Revert a postulant to a specific stage"
  # usage
  # rake manage:revert_to_stage COURSE_ID="" ENROLLMENT_ID="" STAGE_ID=""
  #
  task revert_to_stage: :environment do
    course = Course.find ENV['COURSE_ID']
    enrollment = course.enrollments.find(ENV['ENROLLMENT_ID'])
    stage = enrollment.stages.find(ENV['STAGE_ID'])
    puts "Volviendo proceso #{course.title} a la etapa #{stage.name}"
    enrollment.stages.each do |st|
        if(st.step > stage.step)
            puts "eliminando etapa #{st.name} - paso #{st.step}"
            st.destroy!
        end
    end
    enrollment.stage = stage
    enrollment.current_stage_index = stage.step
    enrollment.save!
  end

  desc "Return the postulant to the first stage"
  # usage
  # rake manage:revert_to_initial_stage COURSE_ID="" ENROLLMENT_ID=""
  #
  task revert_to_initial_stage: :environment do
    if (ENV['STAGE_ID'])
        puts "el parametro STAGE_ID es para la tarea revert_to_stage!"
        break
    end
    course = Course.find ENV['COURSE_ID']
    enrollment = course.enrollments.find(ENV['ENROLLMENT_ID'])
    puts "Volviendo proceso #{course.title} a la etapa inicial"
    enrollment.stages.each do |st|
        puts "eliminando etapa #{st.name} - paso #{st.step}"
        st.destroy!
    end
    enrollment.stage = nil
    enrollment.current_stage_index = nil
    enrollment.save!
  end
end
