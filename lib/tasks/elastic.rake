# coding: utf-8
namespace :elasticsearch do
  desc "Create elasticsearch index for applicants"
  task applicants_index: :environment do
    Applicant::Base.inrefresh
  end

  desc "Create elasticsearch index for courses"
  task courses_index: :environment do
    Course.inrefresh
  end

  desc "Create elasticsearch index for enrollments"
  task enrollments_index: :environment do
    Enrollment::Base.inrefresh
  end

  desc "Create elasticsearch index for hiring request records"
  task hiring_request_records_index: :environment do
    HiringRequest::Record.inrefresh
  end

  desc "Create elasticsearch index for course postulations"
  task course_postulations_index: :environment do
    CoursePostulation.inrefresh
  end

  desc "Delete all indexes from elasticsearch"
  task delete_all_index: :environment do
    puts "Eliminando todos los índices..."
    system("curl -XDELETE '#{Rails.application.config.elastic_server}/#{PREFIX_INDEX}'")
  end

  desc "Refresh all indexes"
  task refresh_all: :environment do
    [
      'courses_index',
      'applicants_index',
      'enrollments_index',
      'hiring_request_records_index',
      'course_postulations_index'
    ].each do |task|
      puts "Refrescando #{task}..."
      Rake::Task['elasticsearch:' + task].invoke
    end
  end

  desc "Deletes and refreshes all indexes"
  task recreate_all: :environment do
    Rake::Task['elasticsearch:delete_all_index'].invoke
    Rake::Task['elasticsearch:refresh_all'].invoke
  end
end
