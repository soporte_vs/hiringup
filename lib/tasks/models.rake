namespace :models do
  desc "Validate all models"
  task validate: :environment do
    ActiveRecord::Base.descendants.each do |model|
      message = "validating records of #{model}..."
      model_valid = model.all.map{|record| record.valid?}.reduce{|j,k| j and k}

      if model_valid.nil? || model_valid
        puts "\033[32m#{message}\033[0m" # No problems (Green message)
      else
        puts "\033[31m#{message}\033[0m" # Problems (Red message)
      end

      model.all.each do |object|
        if !object.valid?
          object_message = "validating [#{object.class}-#{object.id}] #{object.errors.full_messages}"
          puts "       Errors => \033[31m#{object_message}\033[0m" # Problems (Red message)
        end
      end
    end
  end

end
