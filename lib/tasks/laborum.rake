namespace :laborum do

  desc "Get postulations from laborum"
  task get_postulations: :environment do
    Laborum::Publication.where(status: :published).each do |laborum_publication|
      laborum_publication.get_postulations
    end
  end

  desc "Unpublish old Laborum Publications"
  task unpublish_old_publications: :environment do
    Laborum::Publication
    .where(status: :published)
    .where("created_at < ?", Date.today - 1.month)
    .each do |laborum_publication|
      laborum_publication.unpublish
    end
  end

end
