namespace :validate do
  desc "Validate users"
  task users: :environment do
    # has_many :users_roles
    # has_many :applicant_reports, class_name: 'Applicant::Report', dependent: :nullify
    # has_many :user_groups, dependent: :destroy
    # has_many :comments, dependent: :destroy
    
    # has_one :applicant, class_name: Applicant::Base, foreign_key: :user_id, dependent: :destroy, inverse_of: :user
    # has_one :worker, class_name: Worker::Base, foreign_key: :user_id, dependent: :destroy
    # has_one :personal_information, class_name: People::PersonalInformation, foreign_key: :user_id, dependent: :destroy, inverse_of: :user
    # has_one :professional_information, class_name: People::ProfessionalInformation, foreign_key: :user_id, dependent: :destroy, inverse_of: :user
    # has_one :company_employee, class_name: Company::Employee, foreign_key: :user_id, dependent: :destroy

    # has_many :activities, class_name: PublicActivity::Activity, foreign_key: :owner_id
    # has_many :video_interview_answers, class_name: VideoInterview::Answer, dependent: :destroy

    roles_without_user = UsersRole.where(user: nil)
    if roles_without_user.any?
        puts "roles_without_user: #{roles_without_user.count}"
        roles_without_user.destroy_all
    else
        puts "user_roles OK"
    end

    applicant_reports_without_user = Applicant::Report.where(user: nil)
    if applicant_reports_without_user.any?
        puts "applicant_reports_without_user: #{applicant_reports_without_user.count}"
        applicant_reports_without_user.destroy_all
    else
        puts "applicant_reports OK"
    end

    user_groups_without_user = UserGroup.where(user: nil)
    if user_groups_without_user.any?
        puts "user_groups_without_user: #{user_groups_without_user.count}"
        user_groups_without_user.destroy_all
    else
        puts "user_groups OK"
    end

    comments_without_user = Comment.where(user: nil)
    if comments_without_user.any?
        puts "comments_without_user: #{comments_without_user.count}"
        comments_without_user.destroy_all
    else
        puts "comments OK"
    end

    applicants_without_user = Applicant::Base.where(user: nil)
    if applicants_without_user.any?
        puts "applicants_without_user: #{applicants_without_user.count}"
        applicants_without_user.destroy_all
    else
        puts "applicants OK"
    end

    workers_without_user = Worker::Base.where(user: nil)
    if workers_without_user.any?
        puts "workers_without_user: #{workers_without_user.count}"
        workers_without_user.destroy_all
    else
        puts "workers OK"
    end

    
    personal_information_without_user = People::PersonalInformation.where(user: nil)
    if personal_information_without_user.any?
        puts "personal_information_without_user: #{personal_information_without_user.count}"
        personal_information_without_user.destroy_all
    else
        puts "personal_information OK"
    end

    
    professional_information_without_user = People::ProfessionalInformation.where(user: nil)
    if professional_information_without_user.any?
        puts "professional_information_without_user: #{professional_information_without_user.count}"
        professional_information_without_user.destroy_all
    else
        puts "professional_information OK"
    end

    company_employee_without_user = Company::Employee.where(user: nil)
    if company_employee_without_user.any?
        puts "company_employee_without_user: #{company_employee_without_user.count}"
        company_employee_without_user.destroy_all
    else
        puts "company_employee OK"
    end


    PublicActivity::Activity.where(trackable_type: nil).destroy_all
    PublicActivity::Activity.all.each do |pa|
        track_entity = pa.trackable_type.constantize.where(id: pa.trackable_id)
        owner_entity = pa.owner_type ? pa.owner_type.constantize.where(id: pa.owner_id) : []
        recipient_entity = pa.recipient_type ? pa.recipient_type.constantize.where(id: pa.recipient_id ) : []
        if track_entity.empty? || owner_entity.empty? || recipient_entity.empty?
            puts "deleting pa: #{pa.id}"
            pa.destroy
        end
    end

    #Enrollment::Base.where applicant: nil

  end

end
