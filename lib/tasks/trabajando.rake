namespace :trabajando do
  desc "Get all postulations from trabajando.com"
  task get_postulations: :environment do
#    trabajando_postulation = Trabajando::Postulation.order(postulation_date: :asc).last
#    postulation_date = trabajando_postulation.try(:postulation_date)
#    postulation_date ||= Trabajando::Publication.last.try(:created_at)
#    if postulation_date
#      message = "(#{$COMPANY[:name]}) Cargando Postulaciones de trabajando.com desde la fecha #{postulation_date} "
#      InfoDevMailer.delay.info(message, 'integraerror@valposystems.cl')
    Trabajando::Postulation.get_remote_postulations(Time.now.beginning_of_day)
#    end
  end
end
