# coding: utf-8
## Scaffolds <-> Recursos
#Discard Reasons
#Groups
#Cenco -> Bussines unit
#Bussines
#Posistions -> Bussines unit
#Universidades
#Carreras
#Tags para procesos
#Ciudades
#Pases
#Estados

## Application
#Admin User
#Courses
#Workers <-> Ejecutivo
#Applicants

#def red;            "\033[31m#{self}\033[0m" end
#def green;          "\033[32m#{self}\033[0m" end
#def brown;          "\033[33m#{self}\033[0m" end
#def blue;           "\033[34m#{self}\033[0m" end

namespace :development do
  desc "Setup for development enviroment"
  task :build => ["db:drop", "db:create", "db:migrate", "seed:migrate",
                  "elasticsearch:delete_all_index", "elasticsearch:applicants_index",
                  "elasticsearch:courses_index", "development:fake_data"]

  desc "Create fake data for development"
  task fake_data: :environment do

    #Init data
    admin_email = "integraerror@valposystems.cl"
    admin_pass = "hdDDZ*n8jX"

    #Scaffold fake data
    FactoryGirl.create_list(:enrollment_discard_reason, 5)
    puts "\033[32m Discard Reasons creadas \033[0m"

    FactoryGirl.create_list(:user_group,3)
    puts "\033[32m Groups creados \033[0m"

    FactoryGirl.create_list(:company_cenco,3)
    puts "\033[32m Cencos creados \033[0m"

    FactoryGirl.create_list(:company_positions_cenco,3)
    puts "\033[32m Posistions creadas \033[0m"

    FactoryGirl.create_list(:education_career,10)
    puts "\033[32m Careers creadas \033[0m"

    FactoryGirl.create_list(:education_university,10)
    puts "\033[32m Universidades creadas \033[0m"

    FactoryGirl.create(:tag_used_course_tag)
    puts "\033[32m Tag creado \033[0m"

    FactoryGirl.create_list(:territory_city,10)
    puts "\033[32m Ciudades creadas \033[0m"

    FactoryGirl.create_list(:territory_country,10)
    puts "\033[32m Paises creados \033[0m"

    FactoryGirl.create_list(:territory_state,10)
    puts "\033[32m Regiones creadas \033[0m"

    FactoryGirl.create_list(:company_contract_type,2)
    puts "\033[32m Tipos de contratos creados \033[0m"

    FactoryGirl.create_list(:company_timetable,5)
    puts "\033[32m Horarios creados \033[0m"

    FactoryGirl.create_list(:company_estate,5)
    puts "\033[32m Estamentos creados \033[0m"

    #Application Fake data
    #Default admin user
    # @user = FactoryGirl.build(:user, email: admin_email)
    # FactoryGirl.create(:worker_basis, user: @user)
    # @user.reload
    # @user.add_role :admin
    # @user.update(password: admin_pass, password_confirmation: admin_pass)

    puts "\033[32m Admin creado \033[0m"

    FactoryGirl.create_list(:course,5)
    puts "\033[32m Procesos creados \033[0m"

    FactoryGirl.create_list(:applicant_catched, 5)
    puts "\033[32m Candidatos creados \033[0m"

    FactoryGirl.create_list(:worker_basis,5)
    puts "\033[32m Usuarios Creados \033[0m"

    puts "Se cargo los datos de prueba, para ulitlizar la plataforma ingresar a http://localhost:3000/"
    puts "Con los siguientes datos Usuario: \033[32m #{admin_email} \033[0m y Contraseña: \033[32m #{admin_pass} \033[0m"

  end
end
