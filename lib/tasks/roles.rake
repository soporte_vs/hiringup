namespace :roles do
  desc "Validating all role(Permissions) are validate"
  task validate: :environment do
    Role.all.each do |role|
      message = "validating permission #{role.name} on #{role.resource || role.resource_type} is known"
      # puts role.check_if_known
      if role.check_if_known
        puts "\033[32m#{message}\033[0m"
      else
        puts "\033[31m#{message}\033[0m"
      end
    end
  end

end
