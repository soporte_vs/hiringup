$(document).ready(function(){
  $('.share:not(.s_twitter)').ShareLink({
    title: $('meta[property="og:title"]')[0].content,
    text: $('meta[property="og:description"]')[0].content,
    image: $('meta[property="og:image"]')[0].content,
    url: $('meta[property="og:url"]')[0].content
  });

  $('.share.s_twitter').ShareLink({
    title: $('meta[property="og:title"]')[0].content,
    text: $('meta[property="tw:description"]')[0].content,
    image: $('meta[property="og:image"]')[0].content,
    url: $('meta[property="og:url"]')[0].content
  });
});
