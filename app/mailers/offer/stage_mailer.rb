class Offer::StageMailer < ApplicationMailer
  def send_offer_letter(offer_letter)
    @offer_letter = offer_letter
    @enrollment = offer_letter.stage.enrollment

    attachments['carta_oferta.pdf'] = File.read(@offer_letter.generate_pdf)
    mail(
      to: [@enrollment.applicant.email],
      subject: default_i18n_subject(position: @enrollment.course.title)
    )
  end

  def notify_course_manager(offer_letter)
    @offer_letter = offer_letter
    @enrollment = offer_letter.stage.enrollment
    @user = offer_letter.stage.enrollment.course.user
    mail(
      to: [@user.email], subject: default_i18n_subject(position: @offer_letter.company_position.name)
    )


  end

  def send_notification_to_psychologist(offer_letter)
    @offer_letter = offer_letter
    @enrollment = offer_letter.stage.enrollment

    attachments['carta_oferta.pdf'] = File.read(@offer_letter.generate_pdf)
    mail(
      to: [@enrollment.course.user.email],
      #subject: t('offer/letter.email.subject', position: @offer_letter.company_position.name)
    )
  end

end
