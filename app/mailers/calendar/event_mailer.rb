class Calendar::EventMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.calendar.event_mailer.send_create_notification_to_stage.subject
  #
  def send_create_notification_to_stage(event, stage)
    @event = event
    @stage = stage
    token = Digest::MD5.new
    token << @stage.enrollment.applicant.email
    @token = "#{token}-#{@stage.enrollment.id}"
    mail to: stage.enrollment.applicant.email
    # Fundación Integra pidió quitar el ics
    # email_with_ics(@event, __callee__)
  end

  def send_create_notification_to_applicant(event, applicant)
    @event = event
    @applicant = applicant

    token = Digest::MD5.new
    token << @applicant.email
    @token = "#{token}-#{@applicant.id}"

    mail to: applicant.email
    # Fundación Integra pidió quitar el ics
    # email_with_ics(@event, __callee__)
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.calendar.event_mailer.send_create_notification_to_manager.subject
  #
  def send_create_notification_to_manager(event)
    @event = event
    mail to: event.manager.email
    # Fundación Integra pidió quitar el ics
    # email_with_ics(@event, __callee__)
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.calendar.event_mailer.send_create_notification_to_guest.subject
  #
  def send_create_notification_to_guest(event, guest)
    @event = event
    @guest = guest
    @other_guests = @event.guests.to_a.delete_if{ |h| h[:email] == @guest[:email] }
    mail to: guest[:email]
    # Fundación Integra pidió quitar el ics
    # email_with_ics(@event, __callee__)
  end

  def send_removed_notification_to_stage(event, stage)
    @event = event
    @stage = stage

    mail to: stage.enrollment.applicant.email
    # Fundación Integra pidió quitar el ics
    # canceled_event_email_with_ics(@event, __callee__)
  end

  def send_removed_notification_to_applicant(event, applicant)
    @event = event
    @applicant = applicant

    mail to: applicant.email
    # Fundación Integra pidió quitar el ics
    # canceled_event_email_with_ics(@event, __callee__)
  end

  def send_removed_notification_to_guest(event, guest_name, guest_email)
    @event = event
    @guest_name = guest_name
    @other_guests = @event.guests.to_a.delete_if{ |h| h[:email] == guest_email }

    mail to: guest_email
    # Fundación Integra pidió quitar el ics
    # canceled_event_email_with_ics(@event, __callee__)
  end

  def send_removed_notification_to_manager(event, manager_name, manager_email)
    @event = event
    @manager_name = manager_name

    mail to: manager_email
    # Fundación Integra pidió quitar el ics
    # canceled_event_email_with_ics(@event, __callee__)
  end

  def send_updates_notification_to_guest(event, guest)
    @event = event
    @guest = guest
    @other_guests = @event.guests.to_a.delete_if{ |h| h[:email] == @guest[:email] }

    mail to: guest[:email]
    # Fundación Integra pidió quitar el ics
    # email_with_ics(@event, __callee__)
  end

  def send_updates_notification_to_guest_with_updated_stages(event, guest)
    @event = event
    @guest = guest
    @other_guests = @event.guests.to_a.delete_if{ |h| h[:email] == @guest[:email] }

    mail to: guest[:email]
    # Fundación Integra pidió quitar el ics
    # email_with_ics(@event, __callee__)
  end

  def send_updates_notification_to_guest_with_updated_applicants(event, guest)
    @event = event
    @guest = guest
    @other_guests = @event.guests.to_a.delete_if{ |h| h[:email] == @guest[:email] }

    mail to: guest[:email]
    # Fundación Integra pidió quitar el ics
    # email_with_ics(@event, __callee__)
  end

  def send_updates_notification_to_manager_with_updated_stages(event)
    @event = event

    mail to: event.manager.email
    # Fundación Integra pidió quitar el ics
    # email_with_ics(@event, __callee__)
  end

  def send_updates_notification_to_manager_with_updated_applicants(event)
    @event = event

    mail to: event.manager.email
    # Fundación Integra pidió quitar el ics
    # email_with_ics(@event, __callee__)
  end

  def send_updates_notification_to_manager(event)
    @event = event

    mail to: event.manager.email
    # Fundación Integra pidió quitar el ics
    # email_with_ics(@event, __callee__)
  end

  def send_updates_notification_to_stage(event, stage)
    @event = event
    @stage = stage

    token = Digest::MD5.new
    token << @stage.enrollment.applicant.email
    @token = "#{token}-#{@stage.enrollment.id}"

    mail to: stage.enrollment.applicant.email
    # Fundación Integra pidió quitar el ics
    # email_with_ics(@event, __callee__)
  end

  def send_updates_notification_to_applicant(event, applicant)
    @event = event
    @applicant = applicant

    token = Digest::MD5.new
    token << @applicant.email
    @token = "#{token}-#{@applicant.id}"

    mail to: applicant.email
    # Fundación Integra pidió quitar el ics
    # email_with_ics(@event, __callee__)
  end

  private
    def email_with_ics(event, method)
      html_body = render_to_string("/calendar/event_mailer/#{method}")
      ical = Calendar::EventIcs.draw(event)
      mail.add_part(
        Mail::Part.new do
          content_type "text/html"
          body html_body
        end
      )

      ics = Mail::Part.new do
              content_type 'text/calendar'; name="invite.ics"
              body ical.to_ical
            end
      mail.add_part(ics)
    end

    def canceled_event_email_with_ics(event, method)
      html_body = render_to_string("/calendar/event_mailer/#{method}")
      ical = Calendar::EventIcs.draw(event)
      mail.add_part(
        Mail::Part.new do
          content_type "text/html"
          body html_body
        end
      )

      ics = Mail::Part.new do
              content_type 'text/calendar'; name="invite.ics"
              ical.events.last.status = "CANCELLED"
              ical.ip_method = "CANCEL"
              body ical.to_ical
            end
      mail.add_part(ics)
    end
end
