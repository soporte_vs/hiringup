class Education::InstitutionMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.education.institution_mailer.notify_to_maintainer.subject
  #
  def notify_to_maintainer(institution)
    @institution = institution
    mail to: $COMPANY[:maintainer_email]
  end
end
