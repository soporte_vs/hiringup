class Onboarding::StageMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.onboarding.stage_mailer.send_upload_documentation_request.subject
  #

  def send_upload_documentation_request(onboarding_stage)
    if onboarding_stage.enrollment.class == Enrollment::Simple
      attachments['ficha-personal-estudiante-practica.pdf'] = File.read('./app/assets/documents/ficha-personal-estudiante-practica.pdf')
    else
      attachments['ficha-personal-digital.pdf'] = File.read('./app/assets/documents/ficha-personal-digital.pdf')
    end
    @stage = onboarding_stage
    @enrollment = @stage.enrollment
    @onboarding_stage = onboarding_stage
    token = Digest::MD5.new
    token << onboarding_stage.enrollment.applicant.email
    @token = "#{token}-#{onboarding_stage.enrollment.id}"
    @document_descriptors = onboarding_stage.document_group
        .document_descriptors
        .where(internal: [nil, false], only_onboarding: true)

    @document_descriptors.each do |descriptor|
      if descriptor.attachment.present?
        attachments[descriptor.attachment.name] = File.read(descriptor.attachment.attachment.path)
      end
    end

    mail to: @enrollment.applicant.email
  end

  def documents_request(onboarding_stage, document_descriptors, message)
    @stage = onboarding_stage
    @document_descriptors = document_descriptors
    @enrollment = @stage.enrollment
    @message = message

    token = Digest::MD5.new
    token << onboarding_stage.enrollment.applicant.email
    @token = "#{token}-#{onboarding_stage.enrollment.id}"
    @document_descriptors.each do |descriptor|
      if descriptor.attachment.present?
        attachments[descriptor.attachment.name] = File.read(descriptor.attachment.attachment.path)
      end
    end

    mail to: @stage.enrollment
  end
end
