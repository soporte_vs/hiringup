class GroupInterview::ReportMailer < ApplicationMailer
  def send_report(report)
    @report = report
    @report.documents.each do |stage_document|
      attachments[stage_document.document.file.filename] = File.read(stage_document.document.path)
    end
    mail(to: @report.notified_to)
  end
end
