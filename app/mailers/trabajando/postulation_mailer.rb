class Trabajando::PostulationMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.trabajando.postulation_mailer.send_get_postulations_notification.subject
  #
  def send_get_postulations_notification(user, trabajando_publication)
    @user = user
    @trabajando_publication = trabajando_publication

    mail to: trabajando_publication.course.user.email
  end
end
