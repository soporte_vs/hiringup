class Company::ReferenceMailer < ApplicationMailer

  def send_notice_to_employee(company_reference)
    @company_reference = company_reference
    mail(to: company_reference.email)
  end

  def send_notice_to_applicant(applicant)
    @applicant = applicant
    mail(to: applicant.user.email)
  end

end
