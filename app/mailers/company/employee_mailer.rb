class Company::EmployeeMailer < ApplicationMailer
  def report_load_employees(notify_to)
    mail to: notify_to
  end
end
