class Company::WhiteRutMailer < ApplicationMailer
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.company.white_rut_mailer.report_load_white_list.subject
  #
  def report_load_white_list(notify_to, errors = [], success = [])
    @errors, @success = errors, success
    if @errors.any?
      p = Axlsx::Package.new
      wb = p.workbook
      wb.add_worksheet(name: "Ruts no registrados") do |sheet|
        @errors.each do |row|
          sheet.add_row(row)
        end
      end
      attachments['ruts_no_registrados.xlsx'] = {
        mime_type: 'application/xlsx',
        content: p.to_stream.read
      }
    end
    mail to: notify_to
  end
end
