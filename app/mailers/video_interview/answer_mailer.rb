class VideoInterview::AnswerMailer < ApplicationMailer
  def link_to_interview(user)
    user.authentication_token = Devise.friendly_token
    user.save
    @user = user
    mail(to: [user.email])
  end
end
