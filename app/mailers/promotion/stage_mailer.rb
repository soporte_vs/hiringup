class Promotion::StageMailer < ApplicationMailer
  def send_acceptance_email(stage)
    @stage = stage
    @enrollment = @stage.enrollment

    mail(
      to: [@enrollment.applicant.email],
      subject: "Aprobación de la Promoción y/o traslado"
    )
  end

  def send_rejection_email(stage)
    @stage = stage
    @enrollment = @stage.enrollment

    mail(
      to: [@enrollment.applicant.email],
      subject: t('promotion/stage.email.rejection_subject')
    )
  end
end
