class Courses::InternalMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.course.internal_mailer.send_internal_postulation_template.subject
  #
  def send_internal_postulation_template(course, applicant)
    @applicant = applicant
    @course = course
    email_to = @applicant.user.email
    attachments['Anexo 1.pdf'] = File.read("#{Rails.root}/app/assets/documents/planilla_postulacion_interna.pdf")
    mail to: email_to
  end

  def notify_internal_postulation(enrollment)
    @enrollment = enrollment
    @applicant = @enrollment.applicant
    email_to = @enrollment.course.user.email
    mail to: email_to
  end
end
