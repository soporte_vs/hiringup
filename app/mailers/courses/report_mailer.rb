class Courses::ReportMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.courses.report_mailer.send_to_user.subject
  #
  def send_to_user(user, report)
    @user = user
    @report = report
    @columns = ProcessTracking::Report.columns
    xlsx = render_to_string formats: [:xlsx], template: "report/process_tracking/generate"
    attachments["seguimiento_procesos.xlsx"] = {mime_type: Mime::XLSX, content: xlsx}

    mail to: user.email
  end
end
