class Enrollment::CourseInvitationMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.enrollment.course_invitation_mailer.send_course_invitation.subject
  #
  def send_course_invitation(enrollment)
    @enrollment = enrollment
    mail to: enrollment.applicant.email
  end
end
