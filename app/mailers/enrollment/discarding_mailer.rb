class Enrollment::DiscardingMailer < ApplicationMailer

  def discard(enrollment)
    @enrollment = enrollment
    @applicant = @enrollment.applicant
    @discard_reason = @enrollment.try(:discarding).try(:discard_reason)
    if @discard_reason.try(:subject).present?
      subject = @discard_reason.subject
    else
      subject = default_i18n_subject(course_title: @enrollment.course.title.downcase)
    end
    mail(
      to: [@applicant.email],
      subject:  subject
    )
  end

  def revert_discard(enrollment)
    @enrollment = enrollment
    @applicant = @enrollment.applicant

    mail to: @applicant.email
  end
end
