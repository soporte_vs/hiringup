class Minisite::PostulationMailer < ApplicationMailer
  def update_profile(postulation)
    @postulation = postulation
    mail(to: [@postulation.applicant.email])
  end
end
