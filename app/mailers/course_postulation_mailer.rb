class CoursePostulationMailer < ApplicationMailer
  def accept(postulation)
    @postulation = postulation
    mail(to: [@postulation.applicant.email])
  end

  def reject(postulation)
    @postulation = postulation
    mail(to: [@postulation.applicant.email])
  end
end
