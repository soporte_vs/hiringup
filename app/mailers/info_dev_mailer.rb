class InfoDevMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.info_dev_mailer.info.subject
  #
  def info(message, to)
    @info = message

    mail to: [to]
  end
end
