class Laborum::PostulationMailer < ApplicationMailer

  def accept(postulation)
    @postulation = postulation
    mail(to: [@postulation.applicant.email])
  end
end
