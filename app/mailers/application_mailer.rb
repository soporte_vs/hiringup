class ApplicationMailer < ActionMailer::Base
  layout 'mailer'
  default from: "#{$COMPANY[:name]}<#{$COMPANY[:contact_email]}>"
end
