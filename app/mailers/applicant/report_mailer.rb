class Applicant::ReportMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.applicant.report.send_to_user.subject
  #
  def send_to_user(user, applicant_report)
    @user = user
    @applicant_report = applicant_report
    mail to: user.email
  end
end
