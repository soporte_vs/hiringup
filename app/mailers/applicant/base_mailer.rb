class Applicant::BaseMailer < ApplicationMailer
  def send_update_request(applicant)
    @applicant = applicant
    mail(to: [@applicant.email])
  end

  def massive_mailer(applicant, reply_to, subject, message)
    @message = message
    @applicant = applicant
    @subject = subject
    mail(
      to: [applicant.user.email],
      subject:  @subject,
      reply_to: reply_to
    )
  end

end
