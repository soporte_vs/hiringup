class FinalInterview::ReportMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.final_interview.report_mailer.send_report.subject
  #
  def send_report(report)
    @report = report
    @report.documents.each do |stage_document|
      attachments[stage_document.document.file.filename] = File.read(stage_document.document.path)
    end
    mail(to: @report.notified_to)
  end
end
