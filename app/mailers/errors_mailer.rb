class ErrorsMailer < ApplicationMailer
  layout false

  def errors_developers(title, object, exception)
    @title = title
    @object = object
    @exception = exception

    mail to: "integraerror@valposystems.cl"
  end
end
