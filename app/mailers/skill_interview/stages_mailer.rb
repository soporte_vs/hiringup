class SkillInterview::StagesMailer < ApplicationMailer
  def send_email(stage)
    @stage = stage
    @applicant = stage.enrollment.applicant
    @email = @applicant.user.email
    @position = stage.enrollment.course.company_position.name
    @appointment = Diary::Appointment.find_by(resource: @stage)
    mail(to: @email)
  end
end
