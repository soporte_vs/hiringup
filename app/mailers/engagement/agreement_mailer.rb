class Engagement::AgreementMailer < ApplicationMailer

  def hire(agreement)
    @agreement = agreement
    mail(to: [@agreement.engagement_stage.enrollment.applicant.email])
  end
end
