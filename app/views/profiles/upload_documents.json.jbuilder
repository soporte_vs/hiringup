json.array!(@document_records) do |document_record|
  json.partial! 'document/records/show', document: document_record
end
