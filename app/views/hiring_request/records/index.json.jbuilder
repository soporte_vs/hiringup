json.array!(@hiring_request_records) do |hiring_request_record|
  json.extract! hiring_request_record, :id
  json.url hiring_request_record_url(hiring_request_record, format: :json)
end
