json.array!(@document_records) do |document_record|
  json.partial! 'document/records/show', document_record: document_record
end
