json.array!(@applicant_profile_types) do |applicant_profile_type|
  json.extract! applicant_profile_type, :id, :name
  json.url applicant_profile_type_url(applicant_profile_type, format: :json)
end
