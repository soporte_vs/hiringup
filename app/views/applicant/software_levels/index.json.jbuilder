json.array!(@applicant_software_levels) do |applicant_software_level|
  json.extract! applicant_software_level, :id, :name
  json.url applicant_software_level_url(applicant_software_level, format: :json)
end
