json.array!(@enrollment_discard_reasons) do |discard_reason|
  json.extract! discard_reason, :id, :name, :description
  json.url enrollment_discard_reason_url(discard_reason, format: :json)
end
