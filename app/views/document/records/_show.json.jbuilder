json.extract! document_record, :id, :applicant_id, :document_descriptor_id
json.document document_record.document.present? ? document_record.document.file.filename : nil
json.created_at document_record.created_at.present? ? l(document_record.created_at, format: :custom) : nil
json.url document_record.document.present? ? download_document_applicant_basis_path(document_record.applicant, document_record.document_descriptor.name) : nil
json.errors document_record.errors