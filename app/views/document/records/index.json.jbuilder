json.array!(@document_records) do |document_record|
  json.extract! document_record, :id, :document_descriptor_id
  json.applicant document_record.applicant.present? ? document_record.applicant.name_or_email : nil
  json.user_id document_record.applicant.present? ? document_record.applicant.user.id : nil
  json.document document_record.document.present? ? document_record.document.file.filename : nil
  json.created_at document_record.document.present? ? l(document_record.created_at, format: :custom) : nil
  json.url download_document_applicant_basis_path(id: document_record.applicant.id, name: document_record.document_descriptor.name, record_id: document_record.id)
  json.course document_record.enrollment.present? ? document_record.enrollment.course : nil
  json.course_path document_record.enrollment.present? ? course_path(document_record.enrollment.course) : nil
  json.profile_url document_record.applicant.present? ? applicant_basis_path(document_record.applicant_id) : nil
  json.errors document_record.errors
end
