json.array!(@document_descriptors) do |document_descriptor|
  json.extract! document_descriptor, :id, :name, :details, :allowed_extensions
  json.url document_descriptor_url(document_descriptor, format: :json)
end
