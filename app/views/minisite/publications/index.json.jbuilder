json.array!(@minisite_publications) do |minisite_publication|
  json.extract! minisite_publication, :id
  json.url minisite_publication_url(minisite_publication, format: :json)
end
