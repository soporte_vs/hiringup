json.array!(@minisite_categories) do |minisite_category|
  json.extract! minisite_category, :id, :name, :description
  json.url minisite_category_url(minisite_category, format: :json)
end
