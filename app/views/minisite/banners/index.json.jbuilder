json.array!(@minisite_banners) do |minisite_banner|
  json.extract! minisite_banner, :id, :title, :image, :active_image
  json.url minisite_banner_url(minisite_banner, format: :json)
end
