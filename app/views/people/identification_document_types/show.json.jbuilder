json.extract! @people_identification_document_type, :id, :name, :codename, :country_id, :validation_regex, :validate_uniqueness, :created_at, :updated_at
