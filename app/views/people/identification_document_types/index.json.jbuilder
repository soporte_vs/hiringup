json.array!(@people_identification_document_types) do |people_identification_document_type|
  json.extract! people_identification_document_type, :id, :name, :codename, :country_id, :validation_regex, :validate_uniqueness
  json.url people_identification_document_type_url(people_identification_document_type, format: :json)
end
