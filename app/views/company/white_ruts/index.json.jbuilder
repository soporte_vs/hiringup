json.array!(@company_white_ruts) do |company_white_rut|
  json.extract! company_white_rut, :id, :rut
  json.url company_white_rut_url(company_white_rut, format: :json)
end
