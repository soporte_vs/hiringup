json.cache! ['company_positions_cencos_v1', @company_position], expires_in: 5.hours do
  json.extract! @company_position, :id, :name, :description, :cod
  json.company_positions_cencos Company::Cenco.get_with_large_name
end
