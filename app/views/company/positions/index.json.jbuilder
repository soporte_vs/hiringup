json.array!(@company_positions) do |company_position|
  json.extract! company_position, :id, :name, :description, :cod
  json.url company_position_url(company_position, format: :json)
end
