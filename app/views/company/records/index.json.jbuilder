json.array!(@company_records) do |company_record|
  json.extract! company_record, :id, :name
  json.url company_record_url(company_record, format: :json)
end
