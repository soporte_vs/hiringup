json.array!(@company_vacancy_request_reasons) do |company_vacancy_request_reason|
  json.extract! company_vacancy_request_reason, :id, :name
  json.url company_vacancy_request_reason_url(company_vacancy_request_reason, format: :json)
end
