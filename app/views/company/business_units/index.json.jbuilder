json.array!(@company_business_units) do |company_business_unit|
  json.extract! company_business_unit, :id, :name, :description
  json.url company_business_unit_url(company_business_unit, format: :json)
end
