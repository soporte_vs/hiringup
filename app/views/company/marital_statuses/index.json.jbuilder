json.array!(@company_marital_statuses) do |company_marital_status|
  json.extract! company_marital_status, :id, :name
  json.url company_marital_status_url(company_marital_status, format: :json)
end
