json.array!(@company_engagement_origins) do |company_engagement_origin|
  json.extract! company_engagement_origin, :id, :name, :description
  json.url company_engagement_origin_url(company_engagement_origin, format: :json)
end
