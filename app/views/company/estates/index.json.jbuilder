json.array!(@company_estates) do |company_estate|
  json.extract! company_estate, :id, :name
  json.url company_estate_url(company_estate, format: :json)
end
