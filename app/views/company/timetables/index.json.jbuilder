json.array!(@company_timetables) do |company_timetable|
  json.extract! company_timetable, :id, :name, :description
  json.url company_timetable_url(company_timetable, format: :json)
end
