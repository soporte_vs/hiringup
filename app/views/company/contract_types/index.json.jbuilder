json.array!(@company_contract_types) do |company_contract_type|
  json.extract! company_contract_type, :id, :name, :description
  json.url company_contract_type_url(company_contract_type, format: :json)
end
