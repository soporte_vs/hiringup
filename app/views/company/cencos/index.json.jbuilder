json.array!(@company_cencos) do |company_cenco|
  json.extract! company_cenco, :id, :company_business_unit_id, :name, :address, :cod
  json.url company_cenco_url(company_cenco, format: :json)
end
