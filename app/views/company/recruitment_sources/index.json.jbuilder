json.array!(@company_recruitment_sources) do |company_recruitment_source|
  json.extract! company_recruitment_source, :id, :name, :description
  json.url company_recruitment_source_url(company_recruitment_source, format: :json)
end
