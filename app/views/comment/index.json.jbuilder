json.array! @comments do |comment|
  @comment = comment
  json.partial! 'comment/show'
end
