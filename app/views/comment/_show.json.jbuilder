json.extract! @comment, :id, :content, :object_type, :object_id, :created_at
json.user do
  json.extract! @comment.user, :id, :email
  if @comment.user.personal_information.present?
    json.personal_information do
      json.extract! @comment.user.personal_information, :first_name, :last_name
    end
    json.avatar image_path(@comment.user.avatar)
  end
  json.profile_path @comment.try(:user).try(:applicant).present? ? applicant_basis_path(id: @comment.user.applicant.id) : nil
end
