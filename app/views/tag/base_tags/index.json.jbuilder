json.array!(@tag_base_tags) do |tag_base_tag|
  json.extract! tag_base_tag, :id, :name, :typer
  json.url tag_base_tag_url(tag_base_tag, format: :json)
end
