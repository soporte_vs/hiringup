json.array!(@education_institutions) do |education_institution|
  json.extract! education_institution, :id, :type, :name, :custom, :country_id
  json.url education_institution_url(education_institution, format: :json)
end
