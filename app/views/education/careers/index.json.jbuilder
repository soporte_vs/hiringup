json.array!(@education_careers) do |education_career|
  json.extract! education_career, :id, :name
  json.url education_career_url(education_career, format: :json)
end
