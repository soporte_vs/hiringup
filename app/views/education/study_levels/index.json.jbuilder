json.array!(@education_study_levels) do |education_study_level|
  json.extract! education_study_level, :id, :content
  json.url education_study_level_url(education_study_level, format: :json)
end
