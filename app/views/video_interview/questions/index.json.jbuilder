json.array!(@video_interview_questions) do |video_interview_question|
  json.extract! video_interview_question, :id, :content
  json.url video_interview_question_url(video_interview_question, format: :json)
end
