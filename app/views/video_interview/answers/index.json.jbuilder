json.array!(@video_interview_answers) do |video_interview_answer|
  json.extract! video_interview_answer, :id, :time_limit, :question, :enrollment_id
  json.url video_interview_answer_url(video_interview_answer, format: :json)
end
