json.array!(@offer_reject_reasons) do |offer_reject_reason|
  json.extract! offer_reject_reason, :id, :name, :description
  json.url offer_reject_reason_url(offer_reject_reason, format: :json)
end
