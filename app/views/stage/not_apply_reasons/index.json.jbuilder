json.array!(@stage_not_apply_reasons) do |stage_not_apply_reason|
  json.extract! stage_not_apply_reason, :id, :name, :description
  json.url stage_not_apply_reason_url(stage_not_apply_reason, format: :json)
end
