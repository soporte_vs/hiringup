json.array!(@vacancies) do |vacancy|
  json.extract! vacancy, :id
  json.created_at l(vacancy.created_at, format: :short)
  json.updated_at l(vacancy.updated_at, format: :short)
  json.course do
    json.id vacancy.course.try(:id)
    json.title vacancy.course.try(:title)
    json.url vacancy.course.try(:persisted?) ? course_url(vacancy.course) : nil
  end
  json.errors vacancy.errors
end
