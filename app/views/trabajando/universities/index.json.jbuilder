json.array!(@trabajando_universities) do |trabajando_university|
  json.extract! trabajando_university, :id, :name, :trabajando_id, :hupe_university_id
  json.url trabajando_university_url(trabajando_university, format: :json)
end
