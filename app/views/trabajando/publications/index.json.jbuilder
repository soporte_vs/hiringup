json.array!(@trabajando_publications) do |trabajando_publication|
  json.extract! trabajando_publication, :id
  json.url trabajando_publication_url(trabajando_publication, format: :json)
end
