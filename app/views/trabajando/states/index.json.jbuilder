json.array!(@trabajando_states) do |trabajando_state|
  json.extract! trabajando_state, :id, :name, :trabajando_id, :hupe_state_id
  json.url trabajando_state_url(trabajando_state, format: :json)
end
