json.array!(@trabajando_company_activities) do |trabajando_company_activity|
  json.extract! trabajando_company_activity, :id, :name, :trabajando_id
  json.url trabajando_company_activity_url(trabajando_company_activity, format: :json)
end
