json.array!(@trabajando_countries) do |trabajando_country|
  json.extract! trabajando_country, :id, :name, :trabajando_id, :country_id
  json.url trabajando_country_url(trabajando_country, format: :json)
end
