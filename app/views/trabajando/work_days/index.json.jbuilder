json.array!(@trabajando_work_days) do |trabajando_work_day|
  json.extract! trabajando_work_day, :id, :hupe_timetable
  json.url trabajando_work_day_url(trabajando_work_day, format: :json)
end
