json.array!(@trabajando_job_types) do |trabajando_job_type|
  json.extract! trabajando_job_type, :id
  json.url trabajando_job_type_url(trabajando_job_type, format: :json)
end
