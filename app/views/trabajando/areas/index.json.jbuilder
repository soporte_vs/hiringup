json.array!(@trabajando_areas) do |trabajando_area|
  json.extract! trabajando_area, :id, :name, :trabajando_id
  json.url trabajando_area_url(trabajando_area, format: :json)
end
