json.array!(@trabajando_education_levels) do |trabajando_education_level|
  json.extract! trabajando_education_level, :id, :name, :trabajando_id
  json.url trabajando_education_level_url(trabajando_education_level, format: :json)
end
