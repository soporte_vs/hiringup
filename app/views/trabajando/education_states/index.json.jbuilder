json.array!(@trabajando_education_states) do |trabajando_education_state|
  json.extract! trabajando_education_state, :id, :name, :trabajando_id
  json.url trabajando_education_state_url(trabajando_education_state, format: :json)
end
