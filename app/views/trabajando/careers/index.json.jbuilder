json.array!(@trabajando_careers) do |trabajando_career|
  json.extract! trabajando_career, :id, :name, :trabajando_id, :hupe_career_id
  json.url trabajando_career_url(trabajando_career, format: :json)
end
