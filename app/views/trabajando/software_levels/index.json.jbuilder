json.array!(@trabajando_software_levels) do |trabajando_software_level|
  json.extract! trabajando_software_level, :id
  json.url trabajando_software_level_url(trabajando_software_level, format: :json)
end
