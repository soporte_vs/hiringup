json.array!(@trabajando_marital_statuses) do |trabajando_marital_status|
  json.extract! trabajando_marital_status, :id
  json.url trabajando_marital_status_url(trabajando_marital_status, format: :json)
end
