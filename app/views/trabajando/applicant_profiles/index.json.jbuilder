json.array!(@trabajando_applicant_profiles) do |trabajando_applicant_profile|
  json.extract! trabajando_applicant_profile, :id
  json.url trabajando_applicant_profile_url(trabajando_applicant_profile, format: :json)
end
