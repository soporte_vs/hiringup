json.array!(@territory_states) do |territory_state|
  json.extract! territory_state, :id, :name, :territory_country_id
  json.url territory_state_url(territory_state, format: :json)
end
