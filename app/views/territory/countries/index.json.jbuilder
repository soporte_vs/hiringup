json.array!(@territory_countries) do |territory_country|
  json.extract! territory_country, :id, :name
  json.url territory_country_url(territory_country, format: :json)
end
