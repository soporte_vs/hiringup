json.array!(@territory_cities) do |territory_city|
  json.extract! territory_city, :id, :name, :territory_state_id
  json.url territory_city_url(territory_city, format: :json)
end
