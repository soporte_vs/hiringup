# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default("")
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string
#  last_sign_in_ip        :string
#  created_at             :datetime
#  updated_at             :datetime
#  invitation_token       :string
#  invitation_created_at  :datetime
#  invitation_sent_at     :datetime
#  invitation_accepted_at :datetime
#  invitation_limit       :integer
#  invited_by_id          :integer
#  invited_by_type        :string
#  invitations_count      :integer          default(0)
#  authentication_token   :string
#

class User < ActiveRecord::Base
  has_paper_trail
  acts_as_token_authenticatable
  # rolify
  has_many :users_roles

  has_many :applicant_reports, class_name: 'Applicant::Report', dependent: :nullify

  #Group
  has_many :user_groups, dependent: :destroy
  has_many :groups, through: :user_groups

  has_many :comments, dependent: :destroy
  rolify before_add: :check_if_known
  # ROLE_NAME_SECURITY_CLASS_LEVEL_INVALID = "Role name is not in role_name valid for class given"
  # ROLE_NAME_SECURITY_INSTANCE_LEVEL_INVALID = "Role name is not in role_name valid for object given"
  # ROLE_NAME_SECURITY_GLOBAL_LEVEL_INVALID = "Role name is not in role_name valid for global roles"
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :invitable, :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :lockable
         #:omniauthable, :omniauth_providers => [:linkedin]

  has_one :applicant, class_name: Applicant::Base, foreign_key: :user_id, dependent: :destroy, inverse_of: :user
  has_one :worker, class_name: Worker::Base, foreign_key: :user_id, dependent: :destroy
  has_one :personal_information, class_name: People::PersonalInformation, foreign_key: :user_id, dependent: :destroy, inverse_of: :user
  has_one :professional_information, class_name: People::ProfessionalInformation, foreign_key: :user_id, dependent: :destroy, inverse_of: :user
  has_one :company_employee, class_name: Company::Employee, foreign_key: :user_id, dependent: :destroy

  accepts_nested_attributes_for :personal_information
  accepts_nested_attributes_for :professional_information
  accepts_nested_attributes_for :company_employee

  has_many :user_groups, dependent: :destroy
  has_many :groups, through: :user_groups

  has_many :activities, class_name: PublicActivity::Activity, foreign_key: :owner_id
  has_many :video_interview_answers, class_name: VideoInterview::Answer, dependent: :destroy

  delegate :get_rut, to: :personal_information, allow_nil: true
  delegate :identification_document_number, to: :personal_information, allow_nil: true
  delegate :identification_document_type, to: :personal_information, allow_nil: true
  delegate :first_name, to: :personal_information, allow_nil: true
  delegate :last_name, to: :personal_information, allow_nil: true
  delegate :birth_date, to: :personal_information, allow_nil: true
  delegate :landline, to: :personal_information, allow_nil: true
  delegate :cellphone, to: :personal_information, allow_nil: true
  delegate :sex, to: :personal_information, allow_nil: true
  delegate :address, to: :personal_information, allow_nil: true

  delegate :identification_document_number_html, to: :personal_information, allow_nil: true
  delegate :first_name_html, to: :personal_information, allow_nil: true
  delegate :last_name_html, to: :personal_information, allow_nil: true
  delegate :birth_date_html, to: :personal_information, allow_nil: true
  delegate :landline_html, to: :personal_information, allow_nil: true
  delegate :cellphone_html, to: :personal_information, allow_nil: true
  delegate :sex_html, to: :personal_information, allow_nil: true
  delegate :address_html, to: :personal_information, allow_nil: true
  delegate :nationality_name, to: :personal_information, allow_nil: true
  delegate :nationality_id, to: :personal_information, allow_nil: true
  delegate :has_document, to: :applicant, allow_nil: true

  def sex_word
    if personal_information.present?
      return personal_information.sex_word
    else
      return I18n.t("activerecord.models.people/personal_information.genders.undefined")
    end
  end

  def avatar
    # Si no tiene avatar propio, es decir , si no ha subio uno...
    if personal_information.try(:avatar).try(:url).nil?
      # en el caso que tenga asignado el sexo...
      case personal_information.try(:sex)
      when 'Female'
        "avatar_femenino.png"
      when 'Male'
        "avatar_masculino.png"
      else
        "default_avatar.png"
      end
    else
      personal_information.avatar.url
    end
  end

  def can?(role_name, resource=nil)
    self.has_any_role? :admin, { name: :admin, resource: resource}, { name: role_name, resource: resource }
  end

  def check_role?(role_name, resource=nil, resource_id=nil)
    Role.joins(:users).where(users_roles: { user_id: self.id }).where(roles: { name: role_name, resource_type: resource, resource_id: resource_id}).count != 0
  end

  def to_s
    self.first_name || self.email
  end

  def full_name
    "[#{email}] - #{first_name} #{last_name}"
  end

  #Never gonna take me down
  def city
    personal_information.try(:city).try(:name)
  end

  def state
    personal_information.try(:city).try(:territory_state).try(:name)
  end

  def country
    personal_information.try(:city).try(:territory_state).try(:territory_country).try(:name)
  end

  def is_employee?
    self.company_employee.present?
  end

  # Si existe nombre y apellido, los retorna separados por un espacio, si existe uno lo retorna solo y si no
  # retorna el correo del usuario (que siempre esta)
  def name_or_email
    if personal_information.present? and (personal_information.first_name.present? or personal_information.last_name.present?)

      pi = personal_information
      first_name = pi.first_name.present? ? pi.first_name+' ' : ''
      last_name = pi.last_name.present? ? pi.last_name : ''
      name = first_name + last_name
      name
    else
      email
    end
  end

  def rut
    if self.personal_information
      self.personal_information.identification_document_number
    end
  end

  def self.from_omniauth(auth)
    if User.find_by(email: auth.info.email, provider: nil).nil?
      where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
        user.provider = auth.provider
        user.uid = auth.uid
        user.email = auth.info.email
        user.password = Devise.friendly_token[0,20]

        personal = People::PersonalInformation.create_with(
          first_name: auth.info.first_name,
          last_name: auth.info.last_name,
          cellphone: auth.info.phone,
          address: auth.info.location,
          avatar: auth.info.image
        ).find_or_create_by(user: user)
      end
    end
  end

  private

    def check_if_known(role)
      if (role.resource_id and role.resource_type.try(:constantize)) != nil
        resource = role.resource_type.try(:constantize).find(role.resource_id)
        if !resource.roles_instance.map{ |r| r[:name] }.include?(role.name.to_sym)
          role.destroy
          return false
        end
      elsif role.resource_type.try(:constantize) != nil
        if !role.resource_type.try(:constantize)::ROLES_CLASS.map{ |r| r[:name] }.include?(role.name.to_sym)
          role.destroy
          return false
        end
      else
        if !Role::GLOBAL_ROLES.map{ |r| r[:name] }.include?(role.name.to_sym)
          Role.find(role.id).destroy
          return false
        end
      end
    end
end
