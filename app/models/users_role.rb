# == Schema Information
#
# Table name: users_roles
#
#  id      :integer          not null, primary key
#  user_id :integer
#  role_id :integer
#

class UsersRole < ActiveRecord::Base
  belongs_to :user
  belongs_to :role
end
