# coding: utf-8
# == Schema Information
#
# Table name: vacancies
#
#  id                                :integer          not null, primary key
#  course_id                         :integer
#  closed_at                         :datetime
#  observations                      :text
#  created_at                        :datetime         not null
#  updated_at                        :datetime         not null
#  company_positions_cenco_id        :integer
#  hiring_request_record_id          :integer
#  company_vacancy_request_reason_id :integer
#  created_by_id                     :integer
#  required_by_email                 :string
#  required_by_name                  :string
#

class Vacancy < ActiveRecord::Base
  has_paper_trail

  before_validation :associate_postions_cenco
  belongs_to :course, inverse_of: :vacancies
  has_one :agreement, class_name: Engagement::Agreement
  has_many :offer_letters, class_name: Offer::Letter, inverse_of: :vacancy
  belongs_to :company_positions_cenco, class_name: Company::PositionsCenco
  belongs_to :hiring_request_record, class_name: HiringRequest::Record
  belongs_to :company_vacancy_request_reason, class_name: Company::VacancyRequestReason

  validates :company_positions_cenco, presence: true
  validate :consistent?
  validate :course_opened?, on: [:save, :update]

  before_destroy :destroyable?
  after_commit :touch_hiring_request_record, on: :update
  delegate :company_business_unit, to: :company_positions_cenco, allow: nil

  def attributes
    super.merge(cenco_id: self.cenco_id)
  end

  def business_name
    company_business_unit.try(:name)
  end

  def to_s
    "#{company_positions_cenco.fullname} - #{observations}"
  end

  def destroyable?
    if self.offer_letters.any?
      errors.add(:offer_letters, :offer_letters_not_destroyable)
      return false
    elsif self.agreement.present?
      errors.add(:agreement, :agreement_not_destroyable)
      return false
    else
      return true
    end
  end

  def consistent?
    consistent_agreement? && consistent_vacancy_request_reason? && consitent_company_positions_cenco? && consitent_course_company_position?
  end

  def cenco_id=(value)
    @cenco_id = value
    associate_postions_cenco
  end

  def cenco_id
    self.company_positions_cenco.try(:company_cenco_id) || @cenco_id
  end

  def closed?
    return true if self.closed_at.present?
  end

  private

    def associate_postions_cenco
      if @cenco_id.present? && self.course.present? && self.course.company_position.present?
        company_positions_cenco = Company::PositionsCenco.where(
          company_position: self.course.company_position,
          company_cenco: Company::Cenco.find_by_id(@cenco_id)
        ).first_or_create
        self.company_positions_cenco = company_positions_cenco
        @cenco_id = self.company_positions_cenco.company_cenco_id
      end
    end

    def consistent_agreement?
      if self.company_positions_cenco_id_changed? && agreement.present?
        self.errors.add(:company_positions_cenco, :violation_update_agreement)
        return false
      elsif self.company_vacancy_request_reason_id_changed? && agreement.present?
        self.errors.add(:company_vacancy_request_reason_id, :violation_update_agreement)
        return false
      elsif agreement.nil? || (agreement.engagement_stage.enrollment.course == self.course)
        return true
      else
        self.errors.add(:agreement, :inconsistent_agreement)
        return false
      end
    end

    def consistent_vacancy_request_reason?
      if hiring_request_record.nil? || company_vacancy_request_reason == hiring_request_record.company_vacancy_request_reason
        return true
      else
        self.errors.add(:company_vacancy_request_reason_id, :inconsistent_company_vacancy_request_reason)
        return false
      end
    end

    def consitent_company_positions_cenco?
      if hiring_request_record.nil? || hiring_request_record.company_positions_cenco == self.company_positions_cenco
        return true
      else
        self.errors.add(:company_positions_cenco, :inconsistent_with_hiring_request_record)
        return false
      end
    end

    def consitent_course_company_position?
      if course.nil? || (course.try(:company_position) == self.company_positions_cenco.try(:company_position))
        return true
      else
        self.errors.add(:course_id, :inconsistent_company_position)
      end
    end

    def course_opened?
      if self.course.nil? || self.course.opened?
        return true
      else
        self.errors.add(:course_id, :course_not_opened)
        return false
      end
    end

    # Para que cuando le asigne un proceso a la vacante, actualice la HRR y por lo tanto reindexe en ES
    def touch_hiring_request_record
      self.hiring_request_record.touch if self.hiring_request_record.present?
    end
end
