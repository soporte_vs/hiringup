# == Schema Information
#
# Table name: hiring_request_records
#
#  id                                :integer          not null, primary key
#  company_positions_cenco_id        :integer
#  request_date                      :date
#  applicant_id                      :integer
#  revenue_expected                  :float
#  workplace                         :string
#  observations                      :text
#  company_contract_type_id          :integer
#  company_estate_id                 :integer
#  company_timetable_id              :integer
#  entry_date                        :date
#  created_at                        :datetime         not null
#  updated_at                        :datetime         not null
#  company_vacancy_request_reason_id :integer
#  aasm_state                        :string
#  number_vacancies                  :integer          default(1)
#  created_by_id                     :integer
#  required_by_name                  :string
#  required_by_email                 :string
#

class HiringRequest::Record < ActiveRecord::Base
  include AASM
  include HiringRequestRecordSearchable

  aasm do
    state :active, initial: true
    state :rejected
    state :accepted

    event :reject do
      transitions from: :active, to: :rejected
      transitions from: :accepted, to: :rejected, after: :delete_vacancies, guards: [:vacancies_destroyable?]
    end

    event :accept, after: :generate_vacancies do
      transitions from: :active, to: :accepted
    end
  end

  ROLES_CLASS = [
    {name: :admin, description: I18n.t("hiring_request/record.roles.class.admin")},
  ]

  belongs_to :company_positions_cenco, class_name: Company::PositionsCenco
  belongs_to :company_vacancy_request_reason, class_name: Company::VacancyRequestReason
  belongs_to :applicant, class_name: Applicant::Base
  belongs_to :company_contract_type, class_name: Company::ContractType
  belongs_to :company_estate, class_name: Company::Estate
  belongs_to :company_timetable, class_name: Company::Timetable
  belongs_to :created_by, class_name: User
  has_many :vacancies, class_name: Vacancy, dependent: :nullify,
           foreign_key: :hiring_request_record_id

  validates :company_positions_cenco, presence: true
  validates :company_contract_type, presence: true
  validates :request_date, presence: true
  validates :entry_date, presence: true
  validates :company_vacancy_request_reason, presence: true
  validates :revenue_expected, length: { maximum: 8 }
  validates :number_vacancies, presence: true, inclusion: 1..100
  validates :created_by, presence: true

  accepts_nested_attributes_for :applicant, reject_if: :applicant_empty?

  delegate :name, to: :company_vacancy_request_reason, allow_nil: true, prefix: true

  def applicant_empty?(attributes)
    !attributes[:user_attributes][:email].present?
  end

  def request_date
    super.strftime("%d/%m/%Y") if super
  end

  def entry_date
    super.strftime("%d/%m/%Y") if super
  end

  def revenue_expected
    super.to_i if super
  end

  private

    def generate_vacancies
      self.number_vacancies.times do
        vacancy = self.vacancies.new(
          observations: self.observations,
          company_positions_cenco: self.company_positions_cenco,
          company_vacancy_request_reason: self.company_vacancy_request_reason)
        unless vacancy.save
          self.errors.add(:vacancies, :cant_generated)
        end
      end
    end

    def delete_vacancies
      self.vacancies.destroy_all
    end

    def vacancies_destroyable?
      destroyable = self.vacancies.empty? || self.vacancies.map(&:destroyable?).reduce{ |r,e| r and e }
      unless destroyable
        self.errors.add(:aasm_state, :vacancies_cant_destroyed)
      end
      destroyable
    end
end
