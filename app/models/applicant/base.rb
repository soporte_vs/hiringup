# coding: utf-8
# == Schema Information
#
# Table name: applicant_bases
#
#  id          :integer          not null, primary key
#  type        :string
#  user_id     :integer
#  created_at  :datetime
#  updated_at  :datetime
#  blacklisted :boolean
#

class Applicant::Base < ActiveRecord::Base
  has_paper_trail
  include ApplicantSearchable

  belongs_to :user, inverse_of: :applicant
  belongs_to :company_recruitment_source,  class_name: Company::RecruitmentSource
  has_many :comments, class_name: Comment, as: :object, dependent: :destroy
  has_many :enrollments, class_name: Enrollment::Base, foreign_key: :applicant_id, dependent: :destroy
  has_many :courses, through: :enrollments
  has_many :document_records, class_name: 'Document::Record', foreign_key: :applicant_id, dependent: :destroy
  has_many :update_info_requests, class_name: Applicant::UpdateInfoRequest, foreign_key: :applicant_id, dependent: :destroy
  has_many :company_references, class_name: 'Company::Reference', foreign_key: :applicant_id, dependent: :destroy

  has_many :event_resourceables,
           class_name: Calendar::EventResourceable,
           as: :resourceable,
           dependent: :destroy

  has_many :events,
           class_name: Calendar::Event,
           through: :event_resourceables

  after_update :touch_enrollments
  after_update :touch_course_postulations

  validates :user, presence: true, uniqueness: true

  accepts_nested_attributes_for :user

  delegate :get_rut, to: :user, allow_nil: true
  delegate :identification_document_number, to: :user, allow_nil: true
  delegate :identification_document_type, to: :user, allow_nil: true
  delegate :full_name, to: :user
  delegate :name_or_email, to: :user
  delegate :first_name, to: :user
  delegate :first_name_html, to: :user
  delegate :last_name, to: :user
  delegate :last_name_html, to: :user
  delegate :birth_date, to: :user
  delegate :birth_date_html, to: :user
  delegate :cellphone, to: :user
  delegate :cellphone_html, to: :user
  delegate :landline, to: :user
  delegate :landline_html, to: :user
  delegate :sex, to: :user
  delegate :sex_word, to: :user
  delegate :sex_html, to: :user
  delegate :email, to: :user
  delegate :avatar, to: :user
  delegate :address, to: :user
  delegate :address_html, to: :user
  delegate :nationality_name, to: :user
  delegate :nationality_id, to: :user

  serialize :integration_data

  ROLES_CLASS = [
    {name: :admin, description: I18n.t("applicant.base.roles.class.admin")},
    {name: :search, description: I18n.t("applicant.base.roles.class.search")},
    {name: :update, description: I18n.t("applicant.base.roles.class.update")},
    {name: :schedule, description: I18n.t("applicant.base.roles.class.schedule")},
    {name: :send_update_request, description: I18n.t("applicant.base.roles.class.send_update_request")},
    {name: :massive_mailer, description: I18n.t("applicant.base.roles.class.massive_mailer")}
  ]

  PROGRESS_EXCLUDE_ATTRIBUTES = [
    "id",
    "created_at",
    "updated_at",
    "identification_document_number",
    "identification_document_type_id",
    "user_id",
    "avatar"
  ]

  def touch_enrollments
    self.enrollments.map(&:touch)
  end

  def touch_course_postulations
    CoursePostulation.where(applicant: self).map(&:touch)
  end

# Exportar a Excel (tsv)
  def self.to_csv(applicants = nil)
    CSV.generate(col_sep: "\t") do |tsv|
      column_names = People::PersonalInformation.column_names
      column_names -= ["id", "user_id", "created_at", "updated_at", "territory_city_id", "nationality_id", "avatar", "territory_state_id", "territory_country_id"]
      tsv << column_names.map { |column|
        People::PersonalInformation.human_attribute_name(column.to_sym)
      } + [Territory::Country, Territory::State, Territory::City].map{ |klass| klass.model_name.human }

      (applicants || self.all).each do |applicant|
        datos = applicant.user.personal_information.attributes.values_at(*column_names)
        datos.push(applicant.user.personal_information.city.try(:territory_state).try(:territory_country).try(:name))
        datos.push(applicant.user.personal_information.city.try(:territory_state).try(:name))
        datos.push(applicant.user.personal_information.city.try(:name))
        tsv << datos
      end
    end
  end

  def self.new(attributes = {}, options = {})
    if attributes[:user_attributes].present? && attributes[:user_attributes][:email].present? && new_user = User.find_by_email(attributes[:user_attributes][:email])
      if new_user.applicant.present?
        return new_user.applicant
      else
        attributes.delete(:user_attributes)
        applicant = super(attributes, options)
        applicant.user = new_user
        return applicant
      end
    end
    return super(attributes, options)
  end

  def roles_instance
    []
  end

  def notification_update_profile
    Applicant::BaseMailer.delay.notification_update_profile(self)
  end

  def agreements
    Engagement::Agreement.joins(engagement_stage: :enrollment).where(enrollment_bases: {applicant_id: self.id})
  end

  def has_document(name)
    desc = Document::Descriptor.find_by_name(name.to_s)
    record = self.document_records.find_by(:document_descriptor => desc)
    record != nil
  end

  def get_latest_document_record(desc_name, enrollment_id)
    desc = Document::Descriptor.find_by_name(desc_name.to_s)
    # Primero busca el documento asociado a enrollment_id
    if not desc.nil?
      record = self.document_records.where(:document_descriptor => desc, :enrollment_id => enrollment_id).last if not enrollment_id.nil?
      if record.nil?
        # Sino busca el mas reciente, independiente del enrollment
        record = self.document_records.where(:document_descriptor => desc).last
      end
      record
    end
  end

  def create_document_record(desc_name, enrollment_id)
    desc = Document::Descriptor.find_by_name(desc_name.to_s)
    self.document_records.create(:document_descriptor => desc, :enrollment_id => enrollment_id) if desc != nil
  end

  def is_enrolled?
    self.enrollments.find{|e| !e.course.nil? && e.course.opened? }.present?
  end

  def profile_progress
    
    # todos los atributos que ingresa el usuario, menos los que define rails
    attributes = People::PersonalInformation.column_names - PROGRESS_EXCLUDE_ATTRIBUTES
    # Si el atributo está presente, es decir, el usuario lo ingresó, se cuenta 1, de lo contrario se cuenta 0
    present_attributes = attributes.map {
      |attr| self.user.personal_information.present? && self.user.personal_information.send(attr).present? ? 1 : 0
    }.sum


    # La cantidad de atributos que puede llenar el usuario, más un degree.
    # El usuario para tener completo su perfil, necesita llenar todos los atributos posibles,
    # y al menos un degree.
    total_attributes = attributes.size + 1 # attributes size + degree

    # Teniendo la suma de todos los atributos presentes (que ha ingresado el usuario),
    # se suma uno, que corresponde al degree. Si le falta ingresar un degree, no tiene su perfil completo.
    # Si ingresa más de un degree el cálculo va a retornar 100. Si no, la cantidad de atributos
    # presentes sobre la cantidad total de atributos +1 (más uno para considerar el degree)
    present_attributes += 1 if not degrees.empty?

    ((present_attributes / total_attributes.to_f) * 100).round
  end

  def degrees
    degrees = self.try(:user).try(:professional_information).try(:degrees)
    if degrees
      return degrees
    end
    return []
  end

  def associated_courses
    sql = Course.includes(:enrollments)
                .joins(:enrollments)
                .where(enrollment_bases: {applicant_id: self.id})
                .union(
                  Course.includes(:enrollments)
                        .joins(minisite_publications: :postulations)
                        .where(minisite_postulations: {
                          applicant_id: self.id
                        })
                ).to_sql + " ORDER BY created_at DESC"
    (
      Course.find_by_sql(sql) +
      Course.includes(:enrollments)
            .joins(trabajando_publications: :postulations)
            .where(trabajando_postulations: {
              applicant_id: self.id
            }) +
      Course.where(id: nil)
    ).uniq
  end

  def postulation_data_completed?

    exclude_attributes = [
      "id",
      "created_at",
      "updated_at",
      "user_id",
      "avatar",
      "areas_of_interest",
      "recruitment_source_id",
      "availability_replacements",
      "availability_work_other_residence"
    ]

    attributes = People::PersonalInformation.column_names - exclude_attributes
    present_attributes = self.user.personal_information.present? ? attributes.map {
      |attr| self.user.personal_information.send(attr).present? ? 1 : 0
    }.sum : 0

    return present_attributes == attributes.size
  end

  def has_postulation?(course_id)
    return Course.joins(minisite_publications: :postulations)
                 .where(minisite_postulations: {
                    applicant_id: self.id
                 })
                 .where(minisite_publications:{
                    course_id: course_id
                 }).exists? ||
          Course.joins(laborum_publications: :postulations)
                 .where(laborum_postulations: {
                    applicant_id: self.id
                 })
                 .where(laborum_publications:{
                    course_id: course_id
                 }).exists?
  end

  def postulation_data_complete?
    attributes = ["first_name", "last_name"]

    present_attributes = self.user.personal_information.present? ? attributes.map {
      |attr| self.user.personal_information.send(attr).present? ? 1 : 0
    }.sum : 0

    return present_attributes == attributes.size
  end

  def self.generate_pdf(applicant)
    action_controller = ActionController::Base.new
    action_controller.instance_variable_set("@applicant", applicant)

    pdf = WickedPdf.new.pdf_from_string(
      action_controller.render_to_string('applicant/bases/cv.pdf.slim', layout: false)
    )

    pdf_path = "#{Rails.root}/tmp/cv-#{applicant.id}-#{applicant.full_name}.pdf"
    pdf = File.open(pdf_path, 'wb') do |file|
      file << pdf
    end
    return pdf
  end

  def has_references?
    self.company_references.where(confirmed: true).any?
  end
end
