# coding: utf-8
class Applicant::Report < ActiveRecord::Base
  belongs_to :user
  validates :user, presence: true
  # attr_accessor :start_date, :end_date

  # validates :start_date, presence: { message: I18n.t("activerecord.errors.models.applicant/report.attributes.start_date.blank") }
  # validates :end_date, presence: { message: I18n.t("activerecord.errors.models.applicant/report.attributes.end_date.blank") }
  # validate :end_date_after_start_date
  serialize :params
  after_commit :schedule_process_report, on: :create

  def schedule_process_report
    self.delay.process_report
  end

  def process_report
    @applicants = self.generate
    xlsx = ApplicationController.new.render_to_string(
      formats: [:xlsx],
      locals: {
        :@applicants => @applicants,
        :@COLUMNS => Applicant::Report.report_columns
      },
      template: "report/applicants/generate"
    )
    d_path =  Rails.root.join("private", "reports", "applicants")
    FileUtils.mkdir_p(d_path) unless File.directory?(d_path)
    report_path = Rails.root.join("private", "reports", "applicants", "#{self.id}_#{self.created_at.strftime('%Y%m%d')}.xlsx")
    file = File.open(report_path, 'w+')
    file.write(xlsx)
    file.close
    self.report_file = report_path
    if self.save
      Applicant::ReportMailer.send_to_user(self.user, self).deliver!
    end
  end

  def report_url
  end

  def self.report_columns
    return [
      "Nombre",
      "Apellidos",
      "Rut",
      "Fecha de Nacimiento",
      "Género",
      "Estado Civil",
      "Teléfono",
      "Celular",
      "Mail",
      "Nacionalidad",
      "País",
      "Región",
      "Comuna",
      "Dirección",
      "Fuente de Reclutamiento",
      "Área de Interés",
      "Cargo (Registro del más actualizado)",
      "Empresa (Ídem)",
      "desde (Ídem)",
      "hasta (Ídem)",
      "Nivel Máximo de Estudios",
      "Título de\n Enseñanza Primaria",
      "Área de estudio\n Enseñanza Primaria",
      "Institución\n Enseñanza Primaria",
      "Tipo de Estudios\n Enseñanza Primaria",
      "Estado\n Enseñanza Primaria",
      "Título de\n Enseñanza Media",
      "Área de estudio\n Enseñanza Media",
      "Institución\n Enseñanza Media",
      "Tipo de Estudios\n Enseñanza Media",
      "Estado\n Enseñanza Media",
      "Título\n Liceo Técnico",
      "Área de estudio\n Liceo Técnico",
      "Institución\n Liceo Técnico",
      "Tipo de Estudios\n Liceo Técnico",
      "Estado\n Liceo Técnico",
      "Título\n Técnico Profesional",
      "Área de estudio\n Técnico Profesional",
      "Institución\n Técnico Profesional",
      "Tipo de Estudios\n Técnico Profesional",
      "Estado\n Técnico Profesional",
      "Título\n Universitario",
      "Área de estudio\n Universitario",
      "Institución\n Universitario",
      "Tipo de Estudios\n Universitario",
      "Estado\n Universitario",
      "Título\n Postgrado",
      "Área de estudio\n Postgrado",
      "Institución\n Postgrado",
      "Tipo de Estudios\n Postgrado",
      "Estado\n Postgrado",
      "Título\n Magister",
      "Área de estudio\n Magister",
      "Institución\n Magister",
      "Tipo de Estudios\n Magister",
      "Estado\n Magister",
      "Título\n Doctorado",
      "Área de estudio\n Doctorado",
      "Institución\n Doctorado",
      "Tipo de Estudios\n Doctorado",
      "Estado\n Doctorado",
      "Título\n Otros estudios",
      "Área de estudio\n Otros estudios",
      "Institución\n Otros estudios",
      "Tipo de Estudios\n Otros estudios",
      "Estado\n Otros estudios"
    ]
  end

  def generate
    @applicants = Applicant::Base.all
    @applicants = @applicants.joins(:user => :personal_information).where('people_personal_informations.first_name ILIKE ?', "%#{self.params[:applicant][:first_name]}%") if self.params[:applicant][:first_name].present?
    @applicants = @applicants.joins(:user => :personal_information).where('people_personal_informations.last_name ILIKE ?', "%#{self.params[:applicant][:last_name]}%") if self.params[:applicant][:first_name].present?
    @applicants = @applicants.joins(:user => :personal_information).where('people_personal_informations.sex ILIKE ?', "#{self.params[:applicant][:sex][0]}%") if self.params[:applicant][:sex].present?

    if self.params[:applicant][:country].present? && self.params[:applicant][:state].present? && self.params[:applicant][:city].present?
      @applicants = @applicants.joins(:user => :personal_information).where(:people_personal_informations => {territory_city_id: self.params[:applicant][:city]})
    elsif self.params[:applicant][:country].present? && self.params[:applicant][:state].present?
      @applicants = @applicants.joins(user: {personal_information: {city: :territory_state}}).where(:territory_states => {id: self.params[:applicant][:state]})
    elsif self.params[:applicant][:country].present?
      @applicants = @applicants.joins(user: {personal_information: {city: :territory_state}}).where(:territory_states => {territory_country_id: self.params[:applicant][:country]})
    end

    @applicants = @applicants.joins(user: {professional_information: :degrees}).where('people_degrees.name ILIKE ?', "%#{self.params[:applicant][:degree_name]}%")  if self.params[:applicant][:degree_name].present?
    @applicants = @applicants.joins(user: {professional_information: {degrees: :career} }).where(people_degrees: {:career_id => self.params[:applicant][:degree_career_ids]}) if self.params[:applicant][:degree_career_ids].present?
    @applicants = @applicants.joins(user: {professional_information: {degrees: :career} }).where(people_degrees: {:university_id => self.params[:applicant][:degree_university_ids]}) if self.params[:applicant][:degree_university_ids].present?
    @applicants = @applicants.joins(user: {professional_information: :degrees}).where(people_degrees: {:condition => self.params[:applicant][:degree_condition]}) if self.params[:applicant][:degree_condition].present?
    @applicants = @applicants.joins(enrollments: {course: :tags}).where(tag_used_tags: {tag_id: self.params[:course][:tags]}) if self.params[:course][:tags].present?

    if self.params[:course][:country].present? && self.params[:course][:state].present? && self.params[:course][:city].present?
      @applicants = @applicants.joins(enrollments: :course).where(courses: {territory_city_id: self.params[:course][:city]})
    elsif self.params[:course][:country].present? && self.params[:course][:state].present?
      @applicants = @applicants
                     .joins(enrollments: {course: { territory_city: :territory_state }})
                     .where(territory_states: { id: self.params[:course][:state]})
    elsif self.params[:course][:country].present?
      @applicants = @applicants
                     .joins(enrollments: {course: { territory_city: :territory_state }})
                     .where(territory_states: { territory_country_id: self.params[:course][:country]})
    end

    @applicants = @applicants.joins(enrollments: :course).where(:courses => { :engagement_origin_id => self.params[:course][:engagement_origin_ids] }) if self.params[:course][:engagement_origin_ids].present?

    return @applicants.uniq
  end
end
