class Applicant::ProfileType < ActiveRecord::Base
  validates :name, presence: true, uniqueness: true
end
