class Applicant::UpdateInfoRequest < ActiveRecord::Base
  after_create :touch

  belongs_to :applicant, class_name: Applicant::Base
  validates :applicant, presence: :true

  def touch
    self.applicant.touch
    self.applicant.enrollments.map(&:touch)
    Minisite::Postulation.where(applicant: self.applicant).map(&:course_postulation).map(&:touch)
    Laborum::Postulation.where(applicant: self.applicant).map(&:course_postulation).map(&:touch)
  end
end
