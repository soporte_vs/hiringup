class Applicant::SoftwareLevel < ActiveRecord::Base
  validates :name, presence: true, uniqueness: true
end
