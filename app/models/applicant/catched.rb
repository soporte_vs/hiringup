# == Schema Information
#
# Table name: applicant_bases
#
#  id          :integer          not null, primary key
#  type        :string
#  user_id     :integer
#  created_at  :datetime
#  updated_at  :datetime
#  blacklisted :boolean
#

class Applicant::Catched < Applicant::Base
  __elasticsearch__.document_type 'applicants'
  __elasticsearch__.index_name "#{PREFIX_INDEX}"

  after_create :invite_user
  before_validation :assign_user_applicant, if: "self.user.present?"
  before_validation :generate_password, if: "self.user.present?"

  ROLES_CLASS = [
    {name: :admin, description: I18n.t("applicant.catched.roles.admin")},
    {name: :new, description: I18n.t("applicant.catched.roles.new")},
    {name: :create, description: I18n.t("applicant.catched.roles.create")}
  ]

  def roles_instance
    [{name: :update, description: I18n.t("applicant.catched.roles.instance.update")}]
  end

  private

    def invite_user
      if self.valid? and !self.user.worker.present?
        self.user.delay.invite!
      end
    end

    def generate_password
      if !self.user.id.present?
        generate_password = Devise.friendly_token.first(8)
        self.user.password = generate_password
        self.user.password_confirmation = generate_password
      end
    end

    def assign_user_applicant
      if !self.user.valid? and self.user.errors.messages[:email].present?
        user = User.find_by(email: self.user.email)
        if user.try(:worker).present? and !user.try(:applicant).present?
          self.user = user
        end
      end
    end

end
