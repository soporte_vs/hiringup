# == Schema Information
#
# Table name: applicant_bases
#
#  id          :integer          not null, primary key
#  type        :string
#  user_id     :integer
#  created_at  :datetime
#  updated_at  :datetime
#  blacklisted :boolean
#

class Applicant::Internal < Applicant::Base

end
