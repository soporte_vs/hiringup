# == Schema Information
#
# Table name: applicant_bases
#
#  id          :integer          not null, primary key
#  type        :string
#  user_id     :integer
#  created_at  :datetime
#  updated_at  :datetime
#  blacklisted :boolean
#

class Applicant::External < Applicant::Base
  __elasticsearch__.document_type 'applicants'
  __elasticsearch__.index_name "#{PREFIX_INDEX}"
end
