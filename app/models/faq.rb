class Faq < ActiveRecord::Base
  include Bootsy::Container
  validates :question, presence: true
  validates :answer, presence: true
  end
