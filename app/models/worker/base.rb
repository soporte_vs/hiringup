# == Schema Information
#
# Table name: worker_bases
#
#  id         :integer          not null, primary key
#  type       :string
#  user_id    :integer
#  created_at :datetime
#  updated_at :datetime
#

class Worker::Base < ActiveRecord::Base
  belongs_to :user

  after_create :invite_user
  before_validation :assign_user_applicant, if: "self.user.present?"
  before_validation :generate_password, if: "self.user.present?"

  validates :user, presence: true

  accepts_nested_attributes_for :user

  delegate :full_name, to: :user
  delegate :name_or_email, to: :user
  delegate :first_name, to: :user
  delegate :first_name_html, to: :user
  delegate :last_name, to: :user
  delegate :last_name_html, to: :user
  delegate :birth_date, to: :user
  delegate :birth_date_html, to: :user
  delegate :cellphone, to: :user
  delegate :cellphone_html, to: :user
  delegate :landline, to: :user
  delegate :landline_html, to: :user
  delegate :sex, to: :user
  delegate :sex_html, to: :user
  delegate :email, to: :user
  delegate :avatar, to: :user
  delegate :address, to: :user
  delegate :address_html, to: :user
  delegate :nationality_name, to: :user
  delegate :nationality_id, to: :user

  ROLES_CLASS = [
    {name: :admin, description: I18n.t("worker/base.roles.class.admin")},
    {name: :new, description: I18n.t("worker/base.roles.class.new")},
    {name: :create, description: I18n.t("worker/base.roles.class.create")}
  ]

  def roles_instance
    [
      {name: :update, description: I18n.t("worker/base.roles.instance.update")}
    ]
  end
  
  private
    def invite_user
      if self.valid? and !self.user.applicant.present?
        self.user.delay.invite!
      end
    end

    def generate_password
      if !self.user.id.present?
        generated_password = Devise.friendly_token.first(8)
        self.user.password = generated_password
        self.user.password_confirmation = generated_password
      end
    end

    def assign_user_applicant
      if !self.user.valid? and self.user.errors.messages[:email].present?
        user = User.find_by(email: self.user.email)
        if user.try(:applicant).present? and !user.try(:worker).present?
          self.user = user
        end
      end
    end
end
