class PsycholaboralEvaluation::Report < ActiveRecord::Base
  belongs_to :psycholaboral_evaluation_stage,
             class_name: PsycholaboralEvaluation::Stage,
             foreign_key: :psycholaboral_evaluation_stage_id

 has_many :documents,
          class_name: Stage::Document,
          as: :resource,
          dependent: :destroy

  validates :observations, presence: true
  validates :notified_to, email: true
  validates :psycholaboral_evaluation_stage, presence: true
end
