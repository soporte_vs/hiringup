class PsycholaboralEvaluation::Stage <  Stage::Base
  has_one :psycholaboral_evaluation_report,
          class_name: PsycholaboralEvaluation::Report,
          foreign_key: :psycholaboral_evaluation_stage_id,
          dependent: :destroy

  ROLES_CLASS = [
    {name: :admin, description: I18n.t("psycholaboral_evaluation.stage.roles.class.admin")}
  ]

  def roles_instance
    [
      {name: :admin, description: I18n.t("psycholaboral_evaluation.stage.roles.instance.admin")},
    ]
  end

  def can_not_apply?
    return false if self.psycholaboral_evaluation_report.present?
    super
  end
end
