# == Schema Information
#
# Table name: groups
#
#  id          :integer          not null, primary key
#  name        :string
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Group < ActiveRecord::Base
  has_paper_trail
  validates :name, presence: true, uniqueness: true
  validates :description, presence: true

  has_many :user_groups, dependent: :destroy
  has_many :users, through: :user_groups
end
