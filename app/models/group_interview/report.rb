class GroupInterview::Report < ActiveRecord::Base
  belongs_to :group_interview_stage,
             class_name: GroupInterview::Stage,
             foreign_key: :group_interview_stage_id

 has_many :documents,
          class_name: Stage::Document,
          as: :resource,
          dependent: :destroy

  validates :observations, presence: true
  validates :notified_to, email: true
  validates :group_interview_stage, presence: true
end
