class GroupInterview::Stage <  Stage::Base
  has_one :group_interview_report,
          class_name: GroupInterview::Report,
          foreign_key: :group_interview_stage_id,
          dependent: :destroy

  ROLES_CLASS = [
    {name: :admin, description: I18n.t("group_interview.stage.roles.class.admin")}
  ]

  def roles_instance
    [
      {name: :admin, description: I18n.t("group_interview.stage.roles.instance.admin")},
    ]
  end

  def can_not_apply?
    return false if self.group_interview_report.present?
    super
  end

end
