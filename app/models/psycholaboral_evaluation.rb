module PsycholaboralEvaluation
  def self.table_name_prefix
    'psycholaboral_evaluation_'
  end

  def self.to_s
    "Evaluación psicolaboral"
  end
end
