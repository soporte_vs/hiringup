# coding: utf-8
class ProcessTracking::Report
  include ActiveModel::Validations

  def initialize(params)
    @params = params
  end

  # Retorna encabezados de columnas usados tanto en reporte de seguimiento de
  # de procesos como en el reporte de candidatos para un proceso en específico
  def self.columns
    return [
      "Título",
      "Còdigo",
      "Estado",
      "Inicio del Proceso",
      "Tipo de Proceso",
      "Tipo de Contrato",
      "Cargo",
      "Etiquetas",
      "Región",
      "Comuna",
      "Motivo de la Solicitud",
      "Origen de la Contratación",
      "Psicólogo a cargo del proceso",
      "Centro de costo",
      "Número de vacantes",
      "Nombre",
      "Apellidos",
      "Rut",
      "Fecha de Nacimiento",
      "Género",
      "Estado Civil",
      "Teléfono",
      "Celular",
      "Mail",
      "Nacionalidad",
      "País",
      "Región",
      "Comuna",
      "Dirección",
      "Fuente de Reclutamiento",
      "Área de Interés",
      "Cargo (Registro del más actualizado)",
      "Empresa (Ídem)",
      "desde (Ídem)",
      "hasta (Ídem)",
      "Nivel Máximo de Estudios",
      "Título de\n Enseñanza Media",
      "Área de estudio\n Enseñanza Media",
      "Institución\n Enseñanza Media",
      "Tipo de Estudios\n Enseñanza Media",
      "Estado\n Enseñanza Media",
      "Título\n Liceo Técnico",
      "Área de estudio\n Liceo Técnico",
      "Institución\n Liceo Técnico",
      "Tipo de Estudios\n Liceo Técnico",
      "Estado\n Liceo Técnico",
      "Título\n Técnico Profesional",
      "Área de estudio\n Técnico Profesional",
      "Institución\n Técnico Profesional",
      "Tipo de Estudios\n Técnico Profesional",
      "Estado\n Técnico Profesional",
      "Título\n Universitario",
      "Área de estudio\n Universitario",
      "Institución\n Universitario",
      "Tipo de Estudios\n Universitario",
      "Estado\n Universitario",
      "Título\n Postgrado",
      "Área de estudio\n Postgrado",
      "Institución\n Postgrado",
      "Tipo de Estudios\n Postgrado",
      "Estado\n Postgrado",
      "Título\n Magister",
      "Área de estudio\n Magister",
      "Institución\n Magister",
      "Tipo de Estudios\n Magister",
      "Estado\n Magister",
      "Título\n Doctorado",
      "Área de estudio\n Doctorado",
      "Institución\n Doctorado",
      "Tipo de Estudios\n Doctorado",
      "Estado\n Doctorado",
      "Etapa Actual",
      "Centro Costo",
      "Nombre del Centro de costo"
    ]
  end

  def courses
    _courses = Course.order(:id)
    _courses = _courses.where(:company_position_id => @params[:position_ids]) if @params[:position_ids].present?
    _courses = _courses.where(:engagement_origin_id => @params[:engagement_origin_ids]) if @params[:engagement_origin_ids].present?

    if @params[:country].present? && @params[:state].present? && @params[:city].present?
      _courses = _courses.where(territory_city_id: @params[:city])
    elsif @params[:country].present? && @params[:state].present?
      _courses = _courses.joins(territory_city: :territory_state).where(territory_cities: { territory_state_id: @params[:state] } )
    elsif @params[:country].present?
      _courses = _courses.joins(territory_city: :territory_state).where(territory_states: { territory_country_id: @params[:country] })
    end

    _courses = _courses.where('title ILIKE ?', "%#{@params[:title]}%") if @params[:title].present?
    _courses = _courses.joins(:tags).where(tag_base_tags: {id: @params[:tags]}) if @params[:tags].present?
    _courses
  end

  def enrollments(_course)
    _enrollments = _course.enrollments
    _enrollments = _enrollments.joins(:stage).where(stage_bases: {type: @params[:stages]}) if @params[:stages].present?
    _enrollments
  end

  # Usado tanto en reporte de seguimiento de procesos como en el reporte de
  # candidatos para un proceso en específico. Retorna tipo de flujo si está
  # definido o al menos retornar un espacio en blanco
  def self.type_enrollment_name(course)
    filtered_type = course.safe_enrollment_type
    filtered_type[:name] rescue ''
  end

  # Usado tanto en reporte de seguimiento de procesos como en el reporte de
  # candidatos para un proceso en específico. Retorna array que sería la porción
  # de la fila del reporte que tiene info relativa al proceso y no cambia de acuerdo
  # al aplicante
  def self.course_part(course)
    if course.present?
      [
        course.try(:title),
        course.try(:id),
        course.try("test"),
        course.try(:start),
        type_enrollment_name(course),
        course.try(:contract_type).try(:name),
        course.try(:company_position).try(:name),
        course.tags.map(&:name).join(','),
        course.try(:territory_city).try(:territory_state).try(:name),
        course.try(:territory_city).try(:name),
        course.try(:request_reason).try(:name),
        course.try(:engagement_origin).try(:name),
        course.try(:user).try(:name_or_email),
        course.vacancies.try(:first).try(:company_positions_cenco).try(:company_cenco).try(:name),
        course.vacancies.count
      ].map do |c|
        if c == nil or c == ""
          '-'
        else
          c
        end
      end
    else
      [nil]*10
    end
  end

  # Usado tanto en reporte de seguimiento de procesos como en el reporte de
  # candidatos para un proceso en específico. Retorna array que sería la porción
  # de la fila del reporte que tiene info relativa al aplicante(enrollment).
  # Esta parte más course_part conforman toda una fila de los reportes de seguimiento
  # de proceso y el reporte de candidatos de un proceso en específico.
  def self.enrollment_part(enrollment)
    last_laboral_experience = enrollment.applicant.user.professional_information.laboral_experiences.order("since_date DESC").first rescue nil
    degrees = enrollment.applicant.user.professional_information.degrees rescue nil
    ed_media = degrees.where(type: People::HighSchoolDegree).first rescue nil
    ed_liceo = degrees.where(type: People::TechnicalDegree).first rescue nil
    ed_tecnica = degrees.where(type: People::TechnicalProfessionalDegree).first rescue nil
    ed_universitaria = degrees.where(type: People::ProfessionalDegree).first rescue nil
    ed_postgrado = degrees.where(type: People::PostgraduateDegree).first rescue nil
    ed_magister = degrees.where(type: People::MagisterDegree).first rescue nil
    ed_doctorado = degrees.where(type: People::PhdDegree).first rescue nil
    cenco_cod = ''
    cenco_name = ''

    # Se muestra centro de costo si el candidato aceptó la oferta (si tiene una oferta en realidad, por si se finalizó la etapa manualmente), y si tiene contrato
    # Es decir, se muestra el centro de costo de la vacante de la oferta, o bien, el centro de costo del contrato
    # OJO: esto hay que cambiarlo con la actualización de Offer::Stage en el core.
    if enrollment.type != "Enrollment::Internal" && enrollment.is_hired?
        cenco_cod = (Engagement::Stage.find_by(enrollment: enrollment).agreement.vacancy.try(:company_positions_cenco).try(:company_cenco).try(:cod) )
        cenco_name = (Engagement::Stage.find_by(enrollment: enrollment).agreement.vacancy.try(:company_positions_cenco).try(:company_cenco).try(:name) )
    elsif Offer::Stage.find_by(enrollment: enrollment).present? && Offer::Stage.find_by(enrollment: enrollment).offer_letter.present?
      offer_letter = Offer::Stage.find_by(enrollment: enrollment).offer_letter
      cenco_cod = offer_letter.vacancy.try(:company_positions_cenco).try(:company_cenco).try(:cod)
      cenco_name = offer_letter.vacancy.try(:company_positions_cenco).try(:company_cenco).try(:name)
    end

    enrollment_sex = enrollment.applicant.try(:user).try(:personal_information).try(:sex_word)

    [
      enrollment.applicant.try(:first_name),
      enrollment.applicant.try(:last_name),
      enrollment.applicant.try(:user).try(:personal_information).try(:get_rut),
      enrollment.applicant.try(:birth_date),
      enrollment_sex,
      enrollment.applicant.try(:user).try(:personal_information).try(:company_marital_status).try(:name),
      enrollment.applicant.try(:user).try(:personal_information).try(:landline),
      enrollment.applicant.try(:user).try(:personal_information).try(:cellphone),
      enrollment.applicant.try(:user).try(:email),
      enrollment.applicant.try(:user).try(:personal_information).try(:nationality).try(:name),
      enrollment.applicant.try(:user).try(:personal_information).try(:city).try(:territory_state).try(:territory_country).try(:name),
      enrollment.applicant.try(:user).try(:personal_information).try(:city).try(:territory_state).try(:name),
      enrollment.applicant.try(:user).try(:personal_information).try(:city).try(:name),
      enrollment.applicant.try(:user).try(:personal_information).try(:address),
      enrollment.applicant.try(:user).try(:personal_information).try(:company_recruitment_source).try(:name),
      enrollment.applicant.try(:user).try(:personal_information).try(:areas_of_interest),
      last_laboral_experience.try(:position),
      last_laboral_experience.try(:company),
      last_laboral_experience.try(:since_date),
      last_laboral_experience.try(:until_date),
      People::Degree.translated_degree_type(enrollment.applicant.try(:user).try(:personal_information).try(:study_type)),
      ed_media.try(:name),
      ed_media.try(:career).try(:name),
      ed_media.try(:institution_name),
      Education::Institution.translated_degree_type(ed_media.try(:institution_type)),
      ed_media.try(:condition),
      ed_liceo.try(:name),
      ed_liceo.try(:career).try(:name),
      ed_liceo.try(:institution_name),
      Education::Institution.translated_degree_type(ed_liceo.try(:institution_type)),
      ed_liceo.try(:condition),
      ed_tecnica.try(:name),
      ed_tecnica.try(:career).try(:name),
      ed_tecnica.try(:institution_name),
      Education::Institution.translated_degree_type(ed_tecnica.try(:institution_type)),
      ed_tecnica.try(:condition),
      ed_universitaria.try(:name),
      ed_universitaria.try(:career).try(:name),
      ed_universitaria.try(:institution_name),
      Education::Institution.translated_degree_type(ed_universitaria.try(:institution_type)),
      ed_universitaria.try(:condition),
      ed_postgrado.try(:name),
      ed_postgrado.try(:career).try(:name),
      ed_postgrado.try(:institution_name),
      Education::Institution.translated_degree_type(ed_postgrado.try(:institution_type)),
      ed_postgrado.try(:condition),
      ed_magister.try(:name),
      ed_magister.try(:career).try(:name),
      ed_magister.try(:institution_name),
      Education::Institution.translated_degree_type(ed_magister.try(:institution_type)),
      ed_magister.try(:condition),
      ed_doctorado.try(:name),
      ed_doctorado.try(:career).try(:name),
      ed_doctorado.try(:institution_name),
      Education::Institution.translated_degree_type(ed_doctorado.try(:institution_type)),
      ed_doctorado.try(:condition),
      enrollment.id.present? ? enrollment.stage_text : '-',
      cenco_cod,
      cenco_name
    ].map do |c|
        if c == nil or c == ""
          '-'
        else
          c
        end
      end
  end

end
