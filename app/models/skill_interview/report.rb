class SkillInterview::Report < ActiveRecord::Base
  after_commit :touch_enrollment, on: [:create, :update]
  belongs_to :skill_interview_stage,
             class_name: SkillInterview::Stage,
             foreign_key: :skill_interview_stage_id

  has_many :documents,
    class_name: Stage::Document,
    as: :resource,
    dependent: :destroy

  validates :observations, presence: true
  validates :skill_interview_stage, presence: true

  def touch_enrollment
    self.skill_interview_stage.touch_enrollment
  end
end
