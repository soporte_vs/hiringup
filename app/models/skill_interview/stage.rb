class SkillInterview::Stage <  Stage::Base
  has_one :skill_interview_report,
          class_name: SkillInterview::Report,
          foreign_key: :skill_interview_stage_id,
          dependent: :destroy

  ROLES_CLASS = [
    {name: :admin, description: I18n.t("skill_interview.stage.roles.class.admin")}
  ]

  def roles_instance
    [
      {name: :admin, description: I18n.t("skill_interview.stage.roles.instance.admin")},
    ]
  end
end
