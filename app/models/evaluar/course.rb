class Evaluar::Course < ActiveRecord::Base
  belongs_to :course, class_name: 'Course'

  VALID_NAME_REGEX = /^[a-záéíóúñA-ZÁÉÍÓÚÑ0-9 ]*$/

  validates :agency_id, presence: true
  validates :confidential, presence: true
  validates :name, presence: true, uniqueness: true
  validates :name, format: { with: VALID_NAME_REGEX, multiline: true }
  validates :start_date, presence: true
  validates :end_date, presence: true
  validates :callback_url, presence: true, on: :update

  after_create :set_callback_url
  after_create :create_in_evaluar

  def create_in_evaluar
    unless self.callback_url.present?
      self.errors.add(:callback_url, :blank)
      return false
    end

    if self.evaluar_id.present?
      self.errors.add(:evaluar_id, :already_created)
      return false
    end

    process =  {
      agencyId: self.agency_id,
      confidential: self.confidential,
      environmentSettings: {
        cc:[],
        notificationAtTheEnd: self.notification_at_the_end,
        notificationPerEvaluated: self.notification_per_evaluated
      },
      environmentType: self.environment_type,
      name: self.name,
      productType: self.product_type,
      startDate: self.start_date,
      endDate: self.end_date,
      status: self.status,
      processAdvancedDTO: {
          callbackUrl:  self.callback_url
        }
      }

    token = Evaluar::Token.get_valid_token
    auth = "Bearer #{token.access_token}"

    @options = {
      body: process.to_json,
      headers: {
        "Authorization" => auth,
        "Realm" => "evcore",
        "Content-Type" => "application/json"
      }
    }

    response = HTTParty.post(get_process_url, @options)
    json_response = JSON.parse(response.body)

    if response.code == 200
      self.update(evaluar_id: json_response['data'])
      return true
    else
      self.errors.add(:evaluar_id, :no_evaluar_id)
      return false
    end
  end

  private
    def get_process_url
      "#{EVALUAR_PROCESS_URL}"
    end

    def set_callback_url
      self.update(callback_url: "/app/evaluar/courses/#{self.id}/results")
    end

end
