class Evaluar::Result < ActiveRecord::Base
  belongs_to :applicant, class_name: Applicant::Base
  belongs_to :evaluar_course, class_name: Evaluar::Course

  validates :applicant, presence: true
  validates :evaluar_course, presence: true
  validates :data, presence: true
  validates :process_id, presence: true
  validates :identification, presence: true

end
