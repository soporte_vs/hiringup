class Evaluar::Token < ActiveRecord::Base
  before_validation  :get_token_from_api
  validates :access_token, presence: true

  def self.get_valid_token
    return Evaluar::Token.where(created_at: 23.hour.ago..Time.now).last || Evaluar::Token.create
  end

 private
    def get_login_url
      "#{EVALUAR_LOGIN_URL}"
    end

    def get_password
      "#{EVALUAR_PASSWORD}"
    end

    def get_username
      "#{EVALUAR_USER}"
    end

    def call_authentication_service
      auth = { body:
                { grant_type: 'password', username: get_username, password: get_password }
              }
      response = HTTParty.post(get_login_url, auth)
    end

    def get_token_from_api
      @response = call_authentication_service

      json_response = JSON.parse(@response.body)
      self.access_token = json_response['access_token']
      self.expires_in = json_response['expires_in']
      self.refresh_expires_in = json_response['refresh_expires_in']
      self.refresh_token = json_response['refresh_token']
      self.token_type = json_response['token_type']
      self.id_token = json_response['id_token']
      self.not_before_policy = json_response['not-before-policy']
      self.session_state = json_response['session-state']
    end
end
