# coding: utf-8
class CoursePostulation < ActiveRecord::Base
  include PostulationSearcheable

  belongs_to :course
  belongs_to :postulationable, polymorphic: true
  belongs_to :applicant,
             class_name: "Applicant::Base",
             foreign_key: :applicant_id

  before_validation :set_applicant
  validates :course, presence: true
  validates :postulationable, presence: true
  validate :postulation_has_same_course?

  validates :applicant, presence: true
  validates :applicant_id, uniqueness: { scope: [:course_id] }, if: 'CoursePostulation.validate_course_postulation_uniqueness'

  delegate :postulation_date, to: :postulationable
  delegate :publication, to: :postulationable
  delegate :is_accepted, to: :postulationable
  delegate :accepted_at, to: :postulationable
  delegate :answers_with_hupe_format, to: :postulationable

  after_commit :index_course

  def self.validate_course_postulation_uniqueness
    $COMPANY[:validate_course_postulation_uniqueness] || false
  end

  # Esto es medio extraño pero funciona
  def errors
    if postulationable.try(:errors).try(:any?)
      return postulationable.errors
    else
      super
    end
  end

  def reset
    enrollment = Enrollment::Base.find_by(applicant_id: self.applicant.id, course_id: self.course.id)
    if self.postulationable.is_accepted == nil
      errors.add(:reset, 'La postulación ya se encuentra pendiente.')
    elsif self.postulationable.is_accepted && enrollment.try(:work_flow_started?)
      errors.add(:reset, 'La postulación posee etapas asociadas.')
    elsif self.postulationable.is_accepted && enrollment.try(:discarding).present?
      errors.add(:reset, 'El candidato ha sido descartado del proceso.')
    else
      self.postulationable.is_accepted = nil
      self.postulationable.try(:accepted_at=, nil) # En caso de que postulationable no tenga el atributo accepted_at
      self.postulationable.save
      enrollment.try(:destroy)
      self.touch # Es necesario actualizar course para mostrar el numero correcto de postulaciones pendientes
      return true
    end
    false
  end

  def accept
    if self.postulationable.is_accepted == nil && self.accept!
      # Integra no envía email al aceptar carta oferta
      # CoursePostulationMailer.delay.accept(self.postulationable)
      return true
    elsif self.postulationable.is_accepted == false
      errors.add(:accept, 'Postulación ha sido descartada con anterioridad.')
    else
      errors.add(:accept, 'Postulación ya fue aceptada.')
    end
    false
  end

  def accept!
    self.postulationable.is_accepted = true
    self.postulationable.try(:accepted_at=, Date.today)
    self.postulationable.save
    self.touch # Es necesario actualizar course para mostrar el numero correcto de postulaciones pendientes
  end

  def reject
    if self.postulationable.is_accepted == nil && self.reject!
      CoursePostulationMailer.delay.reject(self.postulationable)
      return true
    elsif self.postulationable.is_accepted == false
      errors.add(:reject, 'Postulación ha sido descartada con anterioridad.')
    else
      errors.add(:reject, 'Postulación ya fue aceptada.')
    end
    false
  end

  def reject!
    self.postulationable.is_accepted = false
    self.postulationable.try(:accepted_at=, Date.today)
    self.postulationable.save
    self.touch # Es necesario actualizar course para mostrar el numero correcto de postulaciones pendientes
  end

  private

    def postulation_has_same_course?
      unless self.course == postulationable.try(:course)
        self.errors.add(:course, :is_not_same_course)
        return false
      end
    end

    def set_applicant
      self.applicant = self.postulationable.try(:applicant)
    end

    def index_course
      self.course.try(:index_document)
    end
end
