class AssigningWeighting::Report < ActiveRecord::Base
  after_commit :touch_enrollment, on: [:create, :update]
  belongs_to :assigning_weighting_stage,
             class_name: AssigningWeighting::Stage,
             foreign_key: :assigning_weighting_stage_id

  has_many :documents,
           class_name: Stage::Document,
           as: :resource,
           dependent: :destroy

  validates :assigning_weighting_stage, presence: true

  def touch_enrollment
    self.assigning_weighting_stage.touch_enrollment
  end
end
