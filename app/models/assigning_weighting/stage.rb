class AssigningWeighting::Stage <  Stage::Base
  has_one :assigning_weighting_report,
          class_name: AssigningWeighting::Report,
          foreign_key: :assigning_weighting_stage_id,
          dependent: :destroy

  ROLES_CLASS = [
    {name: :admin, description: I18n.t("assigning_weighting.stage.roles.class.admin")}
  ]

  def roles_instance
    [
      {name: :admin, description: I18n.t("assigning_weighting.stage.roles.instance.admin")},
    ]
  end
end