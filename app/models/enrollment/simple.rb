class Enrollment::Simple < Enrollment::Base
  __elasticsearch__.document_type 'enrollments'
  __elasticsearch__.index_name "#{PREFIX_INDEX}"

  WORK_FLOW = [
    {type: :module, class_name: 'ManagerInterview::Stage', name: 'Entrevista Inicial'},
    {type: :module, class_name: 'TechnicalTest::Stage', name: 'Prueba Técnica'},
    {type: :module, class_name: 'GroupInterview::Stage', name: 'Entrevista Grupal'},
    {type: :module, class_name: 'Test::Stage', name: 'Test'},
    {type: :module, class_name: 'PsycholaboralEvaluation::Stage', name: 'Evaluación Psicolaboral'},
    {type: :module, class_name: 'FinalInterview::Stage', name: 'Entrevista Final'},
    {type: :module, class_name: 'Offer::Stage', name: 'Seleccionado'},
    {type: :module, class_name: 'Onboarding::Stage', name: 'Documentación'},
    {type: :module, class_name: 'Engagement::Stage', name: 'Ingreso'}
  ]
end
