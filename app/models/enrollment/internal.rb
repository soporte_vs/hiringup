# coding: utf-8
class Enrollment::Internal < Enrollment::Base
  __elasticsearch__.document_type 'enrollments'
  __elasticsearch__.index_name "#{PREFIX_INDEX}"
  # after_create :send_template

  WORK_FLOW = [
    {type: :module, class_name: 'SkillInterview::Stage', name: 'Entrevista por Competencia y/o Entrevista para Informe Social'},
    {type: :module, class_name: 'AssigningWeighting::Stage', name: 'Asignación de ponderación'},
    {type: :module, class_name: 'Promotion::Stage', name: 'Aprobación de la Promoción y/o traslado'}
  ]

  def is_hired?
    promotion_stage = Promotion::Stage.find_by(enrollment: self)
    return promotion_stage.present? && promotion_stage.is_approved?
  end

  private
  def send_template
    applicant = self.applicant
    course = self.course
    Courses::InternalMailer.delay.send_internal_postulation_template(course, applicant)
  end
end
