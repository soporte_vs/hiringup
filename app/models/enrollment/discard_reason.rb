# == Schema Information
#
# Table name: enrollment_discard_reasons
#
#  id             :integer          not null, primary key
#  name           :string
#  description    :text
#  created_at     :datetime
#  updated_at     :datetime
#  mailer_content :text
#  subject        :text
#

class Enrollment::DiscardReason < ActiveRecord::Base
  has_paper_trail
  validates :name, presence: true, uniqueness: true
  validates :description, presence: true, uniqueness: true
  has_one :trigger_close_course_discard_reason, class_name: Trigger::CloseCourseDiscardReason, as: :resourceable
  has_many :discardings, class_name: Enrollment::Discarding, foreign_key: :discard_reason_id
  has_many :enrollments, through: :discardings, class_name: Enrollment::Base

  ROLES_CLASS = [
    {name: :admin, description: I18n.t("discard_reason.roles.class.admin")},
  ]

  before_destroy :destroyable?

  def parse_mailer_content(enrollment)
    if self.mailer_content.present?
      text = self.mailer_content
      text = text.gsub(/\[\[candidato\]\]/, enrollment.applicant.name_or_email.capitalize)
                 .gsub(/\[\[cargo\]\]/, enrollment.course.company_position.try(:name).to_s.downcase)
                 .gsub(/\[\[proceso\]\]/, enrollment.course.title.downcase)
      if enrollment.course.vacancies.present?
        text = text.gsub(/\[\[gerencia\]\]/, enrollment.course.vacancies.first.company_positions_cenco.try(:company_cenco_name).to_s.downcase)
      else
        text = text.gsub(/\[\[gerencia\]\]/, "gerencia")
      end
      return text.html_safe
    end
  end

  private
  def destroyable?
    if trigger_close_course_discard_reason.nil?
      return true
    end
    self.errors.add(:trigger_close_course_discard_reason, :is_default)
    return false
  end
end
