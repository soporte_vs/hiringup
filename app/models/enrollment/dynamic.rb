# == Schema Information
#
# Table name: enrollment_bases
#
#  id                  :integer          not null, primary key
#  type                :string
#  course_id           :integer
#  created_at          :datetime
#  updated_at          :datetime
#  applicant_id        :integer
#  stage_id            :integer
#  current_stage_index :integer
#

class Enrollment::Dynamic < Enrollment::Base
  __elasticsearch__.document_type 'enrollments'
  __elasticsearch__.index_name "#{PREFIX_INDEX}"

  WORK_FLOW = []

  def next_stage(type_stage, name, args = {},first_stage = false)
    type_stage = type_stage.is_a?(Class) ? type_stage : type_stage.constantize
    step = first_stage ? self.current_stage_index.to_i : self.current_stage_index.to_i + 1
    new_stage = self.stages.new(type: type_stage.to_s, name: name, args: args)

    stage_kind = new_stage.type.constantize.model_name.human
    if self.course.closed?
      self.errors.add(:stage, :enrollment_course_closed, stage: stage_kind)
    elsif self.discarding.present?
      self.errors.add(:stage, :enrollment_disposed, stage: stage_kind )
    elsif new_stage.save
      self.reload
    else
      self.errors.add(:stage, :stage_already_created, stage: stage_kind )
    end
    return new_stage
  end

  def next_stage_class
    return nil
  end

  def is_dynamic?
    true
  end
end
