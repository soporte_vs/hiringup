# coding: utf-8
# == Schema Information
#
# Table name: enrollment_bases
#
#  id                  :integer          not null, primary key
#  type                :string
#  course_id           :integer
#  created_at          :datetime
#  updated_at          :datetime
#  applicant_id        :integer
#  stage_id            :integer
#  current_stage_index :integer
#

class Enrollment::Base < ActiveRecord::Base
  has_paper_trail
  resourcify
  include ResourcifyAdapter
  include EnrollmentSearchable

  ROLES_CLASS = [
    {name: :admin, description: I18n.t("enrollment/base.roles.class.admin")}
  ]

  def roles_instance
    [
      {name: :admin, description: I18n.t("enrollment/base.roles.instance.admin")},
      {name: :discard, description: I18n.t("enrollment/base.roles.instance.discard")},
      {name: :invite, description: I18n.t("enrollment/base.roles.instance.invite")}
    ]
  end
  after_commit :course_index_document, on: [:create, :destroy]
  # Se indexa el applicant con el objetivo de actualizar
  # is_enrolled el cual permite saber si está participando en
  # un proceso abierto
  after_commit :applicant_index_document

  belongs_to :course
  belongs_to :applicant, class_name: Applicant::Base
  belongs_to :stage, class_name: 'Stage::Base'

  has_many :video_interview_answers, class_name: 'VideoInterview::Answer', foreign_key: :enrollment_id
  has_many :stages, class_name: 'Stage::Base', foreign_key: :enrollment_id, dependent: :destroy
  delegate :document_group, to: :course
  delegate :document_descriptors, to: :document_group
  delegate :document_records, to: :applicant
  delegate :index_document, to: :course, allow_nil: true, prefix: true
  delegate :index_document, to: :applicant, allow_nil: true, prefix: true

  has_one :discarding, class_name: Enrollment::Discarding, foreign_key: :enrollment_id, dependent: :destroy
  has_one :course_invitation, class_name: Enrollment::CourseInvitation, foreign_key: :enrollment_id, dependent: :destroy

  validates :course, presence: true
  validates :applicant, presence: true
  validates :applicant_id, uniqueness: { scope: :course_id }

  LEGACY_TYPES = [
    {name: "Flujo R&S", enrollment_class: Enrollment::Base},
    {name: "Flujo Práctica",enrollment_class: Enrollment::Simple},
    {name: "Flujo Administración y Servicios", enrollment_class: Enrollment::Administrative}
  ]

  TYPES = [
    # Segun lo solicitado por INTEGRA, después en la actualización
    # el Flujo R&S debe quedar vinculado al flujo flexible
    {name: "Flujo R&S", enrollment_class: Enrollment::Dynamic},
    # {name: "Flujo R&S", enrollment_class: Enrollment::Base},
    {name: "Flujo de Promociones y Traslados",enrollment_class: Enrollment::Internal},
    {name: "Flujo de Movilidad Interna", enrollment_class: Enrollment::InternalTransfer}
    # {name: "Flujo Práctica",enrollment_class: Enrollment::Simple},
    # {name: "Flujo Administración y Servicios", enrollment_class: Enrollment::Administrative}
  ]

  WORK_FLOW = [
    {type: :module, class_name: 'ManagerInterview::Stage', name: 'Entrevista Inicial'},
    {type: :module, class_name: 'TechnicalTest::Stage', name: 'Prueba Técnica'},
    {type: :module, class_name: 'GroupInterview::Stage', name: 'Entrevista Grupal'},
    {type: :module, class_name: 'Test::Stage', name: 'Test'},
    {type: :module, class_name: 'PsycholaboralEvaluation::Stage', name: 'Evaluación por competencias / Psicolaboral'},
    {type: :module, class_name: 'FinalInterview::Stage', name: 'Entrevista Final'},
    {type: :module, class_name: 'Offer::Stage', name: 'Seleccionado'},
    {type: :module, class_name: 'Onboarding::Stage', name: 'Documentación'},
    {type: :module, class_name: 'Engagement::Stage', name: 'Ingreso'}
  ]

  def discard(observations=nil, discard_reason=nil)
    if self.disposable?
      self.build_discarding(discard_reason: discard_reason, observations: observations)
      return self.discarding.save
    end
    return false
  end

  def revert_discard
    if self.discarding.present?

      triggers = Trigger::BlacklistDiscardReason.joins(discard_reason: :enrollments)
                  .where(enrollment_bases: {applicant_id: self.applicant.id})
                  .where.not(enrollment_bases: {course_id: self.course.id})

      self.discarding.destroy
      Enrollment::DiscardingMailer.delay.revert_discard(self)
      #revert_blacklisted
      if triggers.empty?
        self.applicant.update(blacklisted: false)
      end

      self.applicant.touch
    else
      self.errors.add(:discarding, :no_discarding_present)
    end

  end


  # Metodo de objeto que permite cambiar de etapa a un Enrollment::Base.
  # Este metodo es llamado por el metodo de clase self.next_stage
  def next_stage
    if self.course.closed?
      self.errors.add(:stage, :enrollment_course_closed)
      return nil
    elsif self.discarding.present?
      self.errors.add(:stage, :enrollment_disposed)
      return nil
    end
    next_index = self.stages.empty? ? 0 : self.current_stage_index.to_i + 1
    next_stage = self.course.work_flow[next_index]

    if next_stage.present?
      if next_stage[:type] == :module
        new_stage = next_stage[:class_name].constantize.new(enrollment_id: self.id, name: next_stage[:name])
        # When a stage is created, it automaticaly is releated to enrollment and set curren_stage_index
        # for enrollment
        if new_stage.save
          self.reload
        end
      end
      return self.stage
    elsif self.work_flow_finished?
      self.errors.add(:stage, :imposible_next_stage_work_flow_finished)
    else
      self.stage = nil
      self.save
    end
    return nil
  end

  # Este metodo de clase, permite cambiar de etapa a un conjunto de enrollments (ActiveRecord::Relation)
  # Ejemplo: @enrollments.next_stage(params)
  # El metodo se encarga de diferenciar si el enrollment es Dynamic o Base, cambiandolo de etapa segun corresponda
  def self.next_stage(type_stage, name_stage, args = {})
    current_user = args.try(:[], :current_user)
    current_scope.each do |enrollment|
      course = enrollment.course

      previous_stage = enrollment.stage
      if previous_stage.presence && previous_stage.is_approved.nil?
        enrollments_status_stage = args[:enrollments_status_stage].try(:values)
        is_approved = enrollment.get_status_current_stage(enrollments_status_stage)
        previous_stage.update(is_approved: is_approved)
      end

      new_stage = (type_stage.present? && name_stage.present? && enrollment.is_dynamic?) ? enrollment.next_stage(type_stage, name_stage, args) : enrollment.next_stage
      if new_stage.present? && new_stage.persisted?
        if enrollment.work_flow_finished? && current_user.present?
          key_activity = 'enrollment.work_flow_finished'
          course.create_activity key: 'course.'.concat(key_activity), owner: current_user, recipient: enrollment
        elsif current_user.present?
          key_activity = enrollment.stage.class.to_s.tableize.singularize.gsub('/','_').gsub('/','_').concat('.created')
          course.create_activity key: 'course.'.concat(key_activity), owner: current_user, recipient: enrollment
        end
      end
    end
  end

  def finish_work_flow?
    self.stage.nil? and self.stages.any?
  end

  def work_flow_finished?
    self.finish_work_flow?
  end

  def invitable?
    self.stage.nil? && self.stages.empty?
  end

  def disposable?
    if self.discarding.present?
      self.errors.add(:discarding, :already_discarted)
      return false
    elsif self.stages.empty?
      return true
    else
      self.stages.each do |stage|
        if !stage.disposable?
          stage.enrollment.errors[:discarding].each do |error|
            self.errors[:discarding].push(error) unless self.errors[:discarding].include?(error)
          end
          return false
        end
      end
      return true
    end
  end

  def to_s
    "#{self.applicant.user.email}"
  end

  def work_flow_started?
    self.stages.any?
  end

  def in_screening?
    self.invitable?
  end

  def in_progress?
    !self.finish_work_flow?
  end

  def next_stage_class
    return self.course.work_flow.first if self.stages.empty?
    next_index = self.current_stage_index.to_i + 1
    next_stage = self.course.work_flow[next_index]
    if next_stage.present?
      return next_stage
    else
      return nil
    end
  end

  def has_documents?
    self.course.document_group.document_descriptors.each do |desc|
       if self.document_records.find_by(:document_descriptor => desc) != nil
         return true
       end
    end if self.course.document_group != nil
    return false
  end

  def pending_documents
    self.course.document_group.remaining_document_records(document_records)
  end

  def is_hired?
    engagement_stage = Engagement::Stage.find_by(enrollment: self)
    if engagement_stage.present? && engagement_stage.is_hired?
      return true
    else
      return false
    end
  end

  def stage_text
    if self.stage.present?
      current_stage = self.stage.class.parent.to_s
    elsif self.work_flow_finished?
      current_stage = I18n.t('enrollment/base.finished')
    else
      current_stage = I18n.t('enrollment/base.assigned')
    end
    if self.discarding.present?
      current_stage = "#{current_stage} (#{I18n.t('enrollment/base.discarted')})"
    end
    return current_stage
  end

  def is_dynamic?
    false
  end

  def current_status
  end

  def invited?
    self.reload
    self.course_invitation.present?
  end

  def invite
    if !self.invitable?
      self.errors.add(:course_invitation, :not_invitable)
      return false
    elsif self.discarding.present?
      self.errors.add(:course_invitation, :disposed)
      return false
    end
    invitation = Enrollment::CourseInvitation.new(enrollment: self)
    if invitation.save
      self.reload
      return true
    else
      self.errors.add(:course_invitation, :already_invited)
      return false
    end
  end

  def mark_invitation_accepted
    unless self.invited?
      self.errors.add(:course_invitation, :not_invited)
      return false
    end
    if self.course_invitation.update(accepted: true)
      self.reload
      return true
    end
  end

  def mark_invitation_rejected
    unless self.invited?
      self.errors.add(:course_invitation, :not_invited)
      return false
    end
    if self.course_invitation.update(accepted: false)
      self.reload
      return true
    end
  end

  def invitation_accepted?
    self.course_invitation.try(:accepted) == true
  end

  def invitation_rejected?
    self.course_invitation.try(:accepted) == false
  end

  def postulation
    CoursePostulation.find_by(course_id: self.course.id, applicant_id: self.applicant.id).try(:postulationable)
  end

  def postulation_origin
    postulation = self.postulation
    publication = case postulation.class.to_s
    when Laborum::Postulation.to_s
      postulation.laborum_publication
    when Minisite::Postulation.to_s
      postulation.minisite_publication
    else
      nil
    end
    return publication.present? ? publication : self.course
  end

  # Método que recibe un hash indicando el estado para la etapa a la etapa actual de este
  # y retorna solamente el estado de la etapa
  def get_status_current_stage(enrollments_status_stage)
    is_approved = enrollments_status_stage.try(:find){|value| value[:enrollment_id].to_i.eql?(id)}.try(:[], :is_approved)

    is_approved = case is_approved
    when "true", true
      true
    when "false", false
      false
    else
      nil
    end

    return is_approved
  end

  def unenroll
    if self.stages.any? || self.postulation.present?
      self.errors.add(:unenroll, 'no puede ser removido(a), verifique que no hay etapas o postulaciones asociadas.')
    else
      return true if self.destroy
    end
    false
  end

end
