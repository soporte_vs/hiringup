# == Schema Information
#
# Table name: enrollment_course_invitations
#
#  id            :integer          not null, primary key
#  enrollment_id :integer
#  accepted      :boolean
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class Enrollment::CourseInvitation < ActiveRecord::Base
  belongs_to :enrollment, class_name: Enrollment::Base, touch: true

  validates :enrollment, presence: true
  validates :enrollment, uniqueness: true
end
