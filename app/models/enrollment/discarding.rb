# == Schema Information
#
# Table name: enrollment_discardings
#
#  id                :integer          not null, primary key
#  enrollment_id     :integer
#  discard_reason_id :integer
#  observations      :text
#  created_at        :datetime
#  updated_at        :datetime
#

class Enrollment::Discarding < ActiveRecord::Base
  belongs_to :enrollment, class_name: Enrollment::Base
  belongs_to :discard_reason,
             class_name: Enrollment::DiscardReason,
             foreign_key: :discard_reason_id

  validates :enrollment, presence: true
  validates :enrollment_id, uniqueness: true
  validates :discard_reason, presence: true

  after_save :set_blacklisted
  after_commit :touch_enrollment

  private

  def set_blacklisted
    if Trigger::BlacklistDiscardReason.find_by(resourceable: discard_reason).present?
      self.enrollment.applicant.blacklisted = true
      self.enrollment.applicant.save
    end
  end

  def touch_enrollment
    if self.enrollment.present?
      self.enrollment.touch
    end
  end
end
