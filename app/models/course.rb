# coding: utf-8
# == Schema Information
#
# Table name: courses
#
#  id                  :integer          not null, primary key
#  type_enrollment     :string
#  title               :string
#  description         :text
#  created_at          :datetime
#  updated_at          :datetime
#  work_flow           :string
#  territory_city_id   :integer
#  company_position_id :integer
#  document_group_id   :integer
#  aasm_state          :string
#  user_id             :integer
#

class Course < ActiveRecord::Base
  has_paper_trail
  resourcify
  include CourseSearchable
  include ResourcifyAdapter
  include PublicActivity::Model
  include AASM

  aasm do
    state :opened, initial: true
    state :closed

    event :close do
      transitions from: :opened, to: :closed, guards: [:closeable?],
                  after: [
                    :discard_enrollments,
                    :clear_vacancies,
                    :close_minisite_publications,
                    :close_trabajando_publications
                  ]
    end
  end

  MAX_COURSE_DAYS = 30.0
  ROLES_CLASS = [
    {name: :create, description: I18n.t("course.roles.class.create")},
    {name: :search, description: I18n.t("course.roles.class.search")},
    {name: :new, description: I18n.t("course.roles.class.new")},
    {name: :update, description: I18n.t("course.roles.class.update")},
    {name: :admin, description: I18n.t("course.roles.class.admin")},
    {name: :show, description: I18n.t("course.roles.class.show")},
    {name: :enroll, description: I18n.t("course.roles.class.enroll")},
    {name: :admin_publications, description: I18n.t("course.roles.class.admin_publications")}
  ]

  def roles_instance
    roles = [
      {name: :admin, description: I18n.t("course.roles.instance.admin")},
      {name: :show, description: I18n.t("course.roles.instance.show")},
      {name: :enroll, description: I18n.t("course.roles.instance.enroll")},
      {name: :discard_enrollment, description: I18n.t("course.roles.instance.discard_enrollment")},
      {name: :invite_enrollment, description: I18n.t("course.roles.instance.invite_enrollment")},
      {name: :update, description: I18n.t("course.roles.instance.update")},
      {name: :admin_publications, description: I18n.t("course.roles.instance.admin_publications")}
    ]
    self.work_flow.each do |w|
      roles.push({name: w[:class_name].tableize.to_sym, description: w[:name]})
    end
    roles.uniq
  end

  # overwrites PublicActivity::Model#has_many
  # from: https://github.com/chaps-io/public_activity/wiki/%5BHow-To%5D-Dependent-destroy-of-associated-activity-in-Rails

  has_many :activities, as: :trackable, class_name: 'PublicActivity::Activity', dependent: :destroy

  has_many :enrollments, class_name: Enrollment::Base, dependent: :destroy
  has_many :applicants, through: :enrollments
  has_many :used_tags, class_name: Tag::UsedTag, as: :taggable, dependent: :destroy
  has_many :tags, through: :used_tags
  has_many :vacancies, dependent: :nullify, inverse_of: :course
  has_many :course_careers, class_name: CourseCareer, dependent: :destroy
  has_many :careers, through: :course_careers, source: :education_career
  has_many :minisite_publications, class_name: Minisite::Publication, dependent: :destroy
  has_many :laborum_publications, class_name: Laborum::Publication, dependent: :destroy
  has_many :trabajando_publications, class_name: Trabajando::Publication, dependent: :destroy
  has_many :course_postulations
  has_and_belongs_to_many :video_interview_questions, class_name: VideoInterview::Question, join_table: :courses_video_interview_questions
  has_one :evaluar_course, class_name: Evaluar::Course, foreign_key: :course_id, dependent: :destroy

  belongs_to :user
  belongs_to :territory_city, class_name: Territory::City
  belongs_to :company_position, class_name: Company::Position
  belongs_to :document_group, class_name: 'Document::Group'

  belongs_to :contract_type, class_name: Company::ContractType
  belongs_to :request_reason, class_name: Company::VacancyRequestReason
  belongs_to :timetable, class_name: Company::Timetable
  belongs_to :engagement_origin, class_name: Company::EngagementOrigin
  belongs_to :study_level, class_name: Education::StudyLevel
  belongs_to :applicant_profile_type, class_name: Applicant::ProfileType
  belongs_to :applicant_software_level, class_name: Applicant::SoftwareLevel
  belongs_to :area, class_name: Company::Area

  validates :user, presence: true
  validates :type_enrollment, presence: true
  validates :document_group, presence: true
  validates :title, presence: true
  validates :description, presence: true
  validates :work_flow, presence: true, if: 'self.type_enrollment != Enrollment::Dynamic.to_s'
  validates :territory_city, presence: true
  validates :company_position, presence: true

  validates :start, presence: true
  validates :timetable, presence: true
  validates :study_type, presence: true
  validates :study_level, presence: true
  validates :applicant_software_level, presence: true
  validates :minimun_requirements, presence: true
  validates :area, presence: true
  validates :applicant_profile_type, presence: true

  attr_readonly :type_enrollment

  validate :validate_type_enrollment, on: [:create,:update]

  serialize :work_flow

  accepts_nested_attributes_for :vacancies, allow_destroy: true, reject_if: :reject_vacancy?
  accepts_nested_attributes_for :evaluar_course

  before_validation { self.finish = self.start + MAX_COURSE_DAYS.days if self.start }
  before_validation :set_work_flow, unless: "self.persisted?"

  delegate :name, to: :study_level, allow_nil: true, prefix: true
  delegate :name, to: :contract_type, allow_nil: true, prefix: true
  delegate :name, to: :request_reason, allow_nil: true, prefix: true
  delegate :name, to: :timetable, allow_nil: true, prefix: true
  delegate :name, to: :engagement_origin, allow_nil: true, prefix: true

  # Devuelve las vacantes con la cantidad de vacantes según los parámetros con
  # los que se agrupan
  def grouped_vacancies
    gv = []
    counter = Vacancy.where(course: self).group(
      :company_positions_cenco_id,
      :company_vacancy_request_reason_id
    ).count
    counter.each do |parameters, count|
      my_vacancy = Vacancy.where(
        company_positions_cenco_id: parameters.first,
        company_vacancy_request_reason_id: parameters.second
      ).first
      attrs = {}
      attrs[:attributes] = my_vacancy.attributes.except(:id)
      attrs[:attributes][:qty] = count
      attrs[:errors] ||= []
      attrs[:errors] << my_vacancy.errors
      gv << attrs
    end
    gv
  end

  # Retorna todas las preguntas de cada método agrupadas por id de publicación,
  # Ej para un proceso con 2 publicaciones minisite y 1 trabajando:
  # grouped_questions =>
  #  {
  #    :minisite =>
  #    {id_pub_minisite1 => [pregunta1, pregunta 2], id_pub_minisite2=> [pregunta1]}
  #   :trabajando =>
  #    {id_pub_trabajando => [pregunta1, pregunta2, pregunta3]}
  #  }
  def grouped_questions
    {
      minisite: minisite_publications.each_with_object({}){|pub, result| result[pub.id] = pub.questions.map(&:description)},
      trabajando: trabajando_publications.each_with_object({}){|pub, result| result[pub.id] = pub.questions}
    }
  end

  def multiply_vacancies_attributes(received_attributes)
    attributes = []

    received_attributes.each do |_, _vacancy_attrs|
      _vacancy_attrs ||= _
      qty = _vacancy_attrs[:qty]
      vacancy_ids = _vacancy_attrs[:vacancy_ids]
      received_vacancy_attrs = _vacancy_attrs.except(:qty, :vacancy_ids)

      current_vacancy_attrs = [] # las vacantes de este grupo de parámetros

      if vacancy_ids.is_a?(String)
        vacancy_ids = vacancy_ids.split(",")
      end

      if vacancy_ids.is_a?(Array) && vacancy_ids.any?
        if qty.present?
          qty = qty.to_i
        else
          qty = vacancy_ids.count
        end

        # le pongo los ids a los atributos que recibí con ese grupo de ids
        vacancy_ids.each do |vacancy_id|
          attrs = {}.merge(received_vacancy_attrs)
          attrs['id'] = vacancy_id
          attrs.delete('_destroy') # estos los manipulo después
          current_vacancy_attrs << attrs
        end

        # igual actualizo las existentes...
        current_vacancy_attrs.each do |attribute|
          attributes << attribute
        end

        # ahora veo qué viene en qty, para ver si agrego o saco vacantes
        vacancies_with_vacancy_attrs_count = current_vacancy_attrs.count

        # si vienen n menos vacantes le agrego _destroy a las n últimas
        # si vienen n más vacantes, agrego esas n (la diferencia entre las actuales y las que vienen)
        # si son iguales no hago nada
        qty_diff = qty - vacancies_with_vacancy_attrs_count

        if qty_diff < 0
          # tengo que ver cuántas no son destroyables, para no agregarlas al each
          destroyable_vacancy_attrs = []
          vacancy_ids.each do |v_id|
            if Vacancy.find(v_id).destroyable?
              the_destroyable_one = attributes.find{ |v| v['id'] == v_id }
              destroyable_vacancy_attrs << the_destroyable_one
              attributes.delete_if { |c| c['id'] == the_destroyable_one['id'] }
            end
          end

          # tengo vacancies_with_vacancy_attrs_count, me mandaron qty
          # por lo tanto tengo que agregar destroy a (qty - vacancies_with_vacancy_attrs_count)
          destroyable_vacancy_attrs.last(qty_diff.abs).each do |dva|
            # a los destruibles los marco para destrucción a todos
            dva['_destroy'] = 1
            # ahora que ya marqué las que podía destruir para destrucción, las vuelvo a agregar
            # a los atributos que tenía
            attributes << dva
          end
        elsif qty_diff > 0
          # vienen más de las que tenía, entonces tengo que agregar (qty - vacancies_with_vacancy_attrs_count)
          qty_diff.times do
            attributes << received_vacancy_attrs
          end
        end
      else
        # si no vienen vacancy_ids, son todas nuevas, así que se agregan todas no más
        if qty.present?
          qty = qty.to_i # porque puede venir como string
          qty.times do
            attributes << received_vacancy_attrs
          end
        else
          attributes << received_vacancy_attrs
        end
      end
    end # end of received_attributes.each
    attributes
  end

  def vacancies_attributes=(v_attributes)
    attributes = multiply_vacancies_attributes(v_attributes)
    _attributes = []
    attributes.each do |_, received_vacancy_attrs|
      received_vacancy_attrs ||= _
      vacancy = Vacancy.find_by(id: received_vacancy_attrs['id'] || received_vacancy_attrs[:id])
      _destroy = received_vacancy_attrs['_destroy'] || received_vacancy_attrs[:_destroy]

      # En caso de que la vacante este marcada para destrucción y este asociada a una solicitud
      # simplemente se desasocia del proceso
      if vacancy.present? && _destroy.present? && vacancy.hiring_request_record.present?
        received_vacancy_attrs.delete('_destroy')
        received_vacancy_attrs['course_id'] = nil
        _attributes << received_vacancy_attrs
      # En caso de que la vacante no este marcada para destrucción y se encuentre se asocia
      # al proceso
      elsif vacancy.present? && vacancy.course.nil? && !_destroy.present?
        self.vacancies << vacancy
      # En caso que la vacante esté asociada a otro proceso, es decir, si la estamos asociando a otro proceso
      elsif vacancy.present? && vacancy.course.present? && received_vacancy_attrs['course_id'].present? && self.id != received_vacancy_attrs['course_id'] && !_destroy.present?
        self.vacancies << vacancy
      # En otro caso simplemente se almacena en una valiable y se delega la actualización
      # al metodo padre
      else
        _attributes << received_vacancy_attrs
      end
    end
    super(_attributes)
  end

  def enroll(applicant = nil, validate_postulation = true)
    return false unless self.opened?
    #self.enrollments << Kernel.const_get(self.type_enrollment).create(course: self, applicant: applicant)
    enrollment = self.type_enrollment.constantize.new(course: self, applicant: applicant)
    if validate_postulation && applicant.has_postulation?(self.id)
      false
    elsif enrollment.save
      true
    else
      false
    end
  end

  def safe_enrollment_type
    type_constant = type_enrollment.safe_constantize
    type = Enrollment::Base::TYPES.find{|t| t[:enrollment_class] == type_constant}
    type = Enrollment::Base::LEGACY_TYPES.find{|t| t[:enrollment_class] == type_constant} unless type
    type
  end

  def enroll!(applicant = nil)
    return enroll(applicant, validate_postulation = false)
  end

  def to_s
    self.title
  end

  def step_present?(klass=nil)
    begin
      postfix = '::Stage'
      klass = klass.concat(postfix) unless klass.match(postfix)
      klass = klass.constantize
      return self.work_flow.select{|step| step[:class_name].constantize == klass}.any?
    rescue Exception => e
      return false
    end
  end

  def work_flow_classes
    work_flow.map{|step| step[:class_name]}
  end

  def total_pending_postulations
    self.course_postulations.select do |postulation|
      postulation.is_accepted.eql?(nil)
    end.count
  end

  def health
    elapsed_days = (Time.zone.now - self.created_at).to_i / 1.day
    [(elapsed_days/MAX_COURSE_DAYS * 100), 100].min.to_i
  end

  def is_internal?
    type_enrollment.constantize == Enrollment::Internal
  end

  def hired_candidates
    _hired_candidates = []
    if is_internal?
      _hired_candidates = enrollments.select(&:is_hired?).map(&:applicant)
    else
      self.vacancies.each do |vacancy|
        if vacancy.agreement.present? && vacancy.agreement.engagement_stage.enrollment
          _hired_candidates << vacancy.agreement.engagement_stage.enrollment.applicant
        end
      end
    end

    return _hired_candidates
  end

  def hired_candidates?
    if hired_candidates.any?
      return true
    end
    return false
  end

  #  Se reemplaza el caracter de viñeta en ISO-8859-1 por "- " tanto en
  # `Minisite::Publication` como en `Course`
  def description
    _description = super
    if _description.present?
      # reemplaza la viñeta por un "- "
      _description.gsub(" ", "- ")
    end
    return _description
  end

  private

    # Ignore new vacancy with observations blank
    # or vacancy already was occupied
    def reject_vacancy?(attributed)
      attributed['id'].blank? and
      attributed['observations'].blank? and
      attributed['cenco_id'].blank? and
      attributed['company_vacancy_request_reason_id'].blank? and
      attributed['company_positions_cenco_id'].blank?
    end

    def validate_type_enrollment
      begin
        Kernel.const_get(self.type_enrollment) if self.work_flow.nil?
      rescue Exception => e
        errors.add(:type_enrollment, I18n.t('activerecord.errors.models.course.attributes.type_enrollment.corrupt'))
      end
    end

    def set_work_flow
      begin
        self.work_flow = self.type_enrollment.constantize::WORK_FLOW unless self.work_flow != nil
      rescue Exception => e
        errors.add(:work_flow, I18n.t('activerecord.errors.models.course.attributes.work_flow.empty'))
      end
    end

    def clear_vacancies
      unless self.type_enrollment.eql?('Enrollment::Internal')
        target_diasociate_vacancies = []
        target_destroy_vacancies = []
        self.vacancies.each do |vacancy|
          unless Offer::Letter.find_by(vacancy_id: vacancy.id)
            if vacancy.hiring_request_record.nil?
              target_destroy_vacancies.push(vacancy)
            else
              target_diasociate_vacancies.push(vacancy)
            end
          else
            vacancy.closed_at = Time.now
            vacancy.save
          end
        end
        # De esta manera solo se hace solo dos queries una para desasociar y otra para eliminar vacantes
        updated = Vacancy.where(id: target_diasociate_vacancies.map(&:id)).update_all(course_id: nil)
        destroyed = Vacancy.destroy_all(id: target_destroy_vacancies.map(&:id))
      end
    end

    def close_minisite_publications
      self.minisite_publications.each do |publication|
        unless publication.closed?
          publication.close!
        end
      end
      # Minisite::Postulation
      #   .joins(minisite_publication: :course)
      #   .where(is_accepted: nil)
      #   .where(courses: { id: self.id })
      #   .update_all(is_accepted: false)
    end

    def close_trabajando_publications
      self.trabajando_publications.each do |publication|
        unless publication.closed?
          publication.deactivate
        end
      end
    end

    def discard_enrollments
      discard_reason = Trigger::CloseCourseDiscardReason.last.try(:resourceable)
      self.enrollments.each do |enrollment|
        unless enrollment.is_hired?
          Enrollment::Discarding.create(discard_reason: discard_reason, enrollment: enrollment)
        end
      end
    end

    def closeable?
      return true if self.vacancies.empty?
      case self.type_enrollment
      when 'Enrollment::Base'
        vacancies_associated_agreement = Engagement::Agreement.joins(:vacancy).where(vacancies: { course_id: self.id }).count
        closeable = self.vacancies.size == vacancies_associated_agreement
        self.errors.add(:aasm_state, 'No se han cubierto todas las vacantes') unless closeable
        return closeable
      when 'Enrollment::Internal'
        vacancies_associated_email_promotion = Stage::Base.joins(:enrollment).where(type: Promotion::Stage, enrollment_bases: { course_id: self.id}).count
        # closeable = self.vacancies.size == vacancies_associated_email_promotion
        closeable = self.vacancies.size == self.vacancies.where.not(closed_at: nil).count
        self.errors.add(:aasm_state, 'No se han cubierto todas las vacantes') unless closeable
        return closeable
      else
        return true
      end
    end
end
