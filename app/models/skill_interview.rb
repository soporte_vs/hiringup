module SkillInterview
  def self.table_name_prefix
    'skill_interview_'
  end

  def self.to_s
    "Entrevista por Competencia y/o Entrevista para Informe Social"
  end
end
