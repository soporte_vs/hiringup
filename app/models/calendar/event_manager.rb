# coding: utf-8
# == Schema Information
#
# Table name: calendar_event_managers
#
#  id         :integer          not null, primary key
#  event_id   :integer
#  name       :string
#  email      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Calendar::EventManager < ActiveRecord::Base
  belongs_to :event,
             class_name: Calendar::Event,
             inverse_of: :manager

  validates :event, presence: true

  after_create :send_create_notification
  after_destroy :send_remove_notification
  after_touch :send_update_notification

  private
    def send_create_notification
      event = self.event
      Calendar::EventMailer.delay.send_create_notification_to_manager(event)
    end

    def send_remove_notification
      event = self.event
      event.prev_manager = self
      Calendar::EventMailer.delay.send_removed_notification_to_manager(event, self.name, self.email)
    end

    def send_update_notification

      event = self.event

      if event.resourceables_changed?
        # notifico con candidatos actualizados
        resourceables_type = event.event_resourceables.first.resourceable_type
        case resourceables_type
        when "Applicant::Base"
          return Calendar::EventMailer.delay.send_updates_notification_to_manager_with_updated_applicants(event)
        when "Stage::Base"
          return Calendar::EventMailer.delay.send_updates_notification_to_manager_with_updated_stages(event)
        else
          return nil
        end
      else
        # notifico sólo cambios en datos del evento
        Calendar::EventMailer.delay.send_updates_notification_to_manager(event)
      end
    end
end
