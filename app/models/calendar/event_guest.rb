# == Schema Information
#
# Table name: calendar_event_guests
#
#  id         :integer          not null, primary key
#  event_id   :integer
#  name       :string
#  email      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Calendar::EventGuest < ActiveRecord::Base
  belongs_to :event,
             class_name: Calendar::Event,
             inverse_of: :guests

  validates :event, presence: true

  after_create :send_create_notification
  after_destroy :send_remove_notification
  after_touch :send_update_notification

  attr_accessor :is_new

  private
    def send_create_notification
      event = self.event
      guest = self

      self.is_new = true
      event.guests_changed = true

      Calendar::EventMailer.delay.send_create_notification_to_guest(event, guest)
    end

    def send_remove_notification
      guest_name = self[:name]
      guest_email = self[:email]
      event.guests_changed = true
      Calendar::EventMailer.delay.send_removed_notification_to_guest(self.event, guest_name,guest_email)
    end

    def send_update_notification
      event = self.event
      guest = self

      if event.resourceables_changed?
         resourceables_type = event.event_resourceables.first.resourceable_type
        case resourceables_type
        when "Applicant::Base"
          return Calendar::EventMailer.delay.send_updates_notification_to_guest_with_updated_applicants(event, guest)
        when "Stage::Base"
          return Calendar::EventMailer.delay.send_updates_notification_to_guest_with_updated_stages(event, guest)
        else
          return nil
        end
      else
        Calendar::EventMailer.delay.send_updates_notification_to_guest(event, guest)
      end
    end
end
