# == Schema Information
#
# Table name: calendar_event_resourceables
#
#  id                :integer          not null, primary key
#  resourceable_id   :integer
#  resourceable_type :string
#  event_id          :integer
#  status            :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class Calendar::EventResourceable < ActiveRecord::Base
  belongs_to :resourceable, polymorphic: true
  belongs_to :event,
             class_name: Calendar::Event,
             inverse_of: :event_resourceables

  validates :event, presence: true

  after_create :send_create_notification
  after_destroy :send_remove_notification
  after_touch :send_update_notification
  after_commit :reindex

  attr_accessor :is_new

  private
    def send_create_notification
      self.is_new = true
      event = self.event
      event.resourceables_changed = true

      resourceable_type = self.resourceable_type

      case resourceable_type
      when "Applicant::Base"
        applicant = self.resourceable
        return Calendar::EventMailer.delay.send_create_notification_to_applicant(event, applicant)
      when "Stage::Base"
        stage = self.resourceable
        return Calendar::EventMailer.delay.send_create_notification_to_stage(event, stage)
      else
        return nil
      end
    end

    def send_remove_notification
      event = self.event
      event.resourceables_changed = true
      resourceable_type = self.resourceable_type
      now = Time.now
      if now <= event.starts_at
        case resourceable_type
        when "Applicant::Base"
          applicant = self.resourceable
          return Calendar::EventMailer.delay.send_removed_notification_to_applicant(event, applicant)
        when "Stage::Base"
          stage = self.resourceable
          return Calendar::EventMailer.delay.send_removed_notification_to_stage(event, stage)
        else
          return nil
        end
      else
        return nil
      end
    end

    def send_update_notification
      event = self.event
      resourceable_type = self.resourceable_type
      case resourceable_type
      when "Applicant::Base"
        applicant = self.resourceable
        return Calendar::EventMailer.delay.send_updates_notification_to_applicant(event, applicant)
      when "Stage::Base"
        stage = self.resourceable
        return Calendar::EventMailer.delay.send_updates_notification_to_stage(event, stage)
      else
        return nil
      end
    end

    def reindex
      self.resourceable.try(:touch)
    end
end
