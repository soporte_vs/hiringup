# coding: utf-8
# == Schema Information
#
# Table name: calendar_events
#
#  id            :integer          not null, primary key
#  starts_at     :datetime
#  ends_at       :datetime
#  address       :string
#  lat           :float
#  lng           :float
#  guests        :text
#  created_by_id :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class Calendar::Event < ActiveRecord::Base
  has_many :event_resourceables,
           class_name: Calendar::EventResourceable,
           inverse_of: :event,
           after_remove: :set_event_resourceables_changed

  has_many :stages,
           through: :event_resourceables,
           source: :resourceable,
           source_type: 'Stage::Base',
           dependent: :destroy

  has_many :applicants,
           through: :event_resourceables,
           source: :resourceable,
           source_type: 'Applicant::Base',
           dependent: :destroy

  has_many :guests,
           class_name: Calendar::EventGuest,
           inverse_of: :event,
           dependent: :destroy

  has_one :manager,
          class_name: Calendar::EventManager,
          inverse_of: :event,
          dependent: :destroy

  belongs_to :created_by, class_name: User

  validates :address, presence: true
  validates :manager, presence: true
  validates :starts_at, presence: true
  validate :starts_at_before_ends_at

  after_create :notify_creation
  after_update :notify_updates


  after_initialize :set_olds

  attr_accessor :prev_manager
  attr_accessor :old_guests
  attr_accessor :old_stages

  attr_accessor :guests_changed
  attr_accessor :resourceables_changed

  accepts_nested_attributes_for :guests, allow_destroy: true
  accepts_nested_attributes_for :manager, allow_destroy: true


  def resourceables_changed?
    self.resourceables_changed
  end

  def guests_changed?
    self.guests_changed ||
      self.guests.map(&:id).count(nil) > 0 ||
      self.guests.map(&:marked_for_destruction?).count(true) > 0 ||
      self.guests.map(&:changed?).count(true) > 0
  end

  def manager_changed?
    if self.prev_manager.present?
      return self.prev_manager.changed? || self.prev_manager != self.manager
    else
      if self.manager.changed?
        return true
      else
        return false
      end
    end
  end

  def manager_attributes=(attributes)
    # Si cambia el correo, elimino el existente
    # para que se gatille el correo de notificación de eliminación
    if self.manager.present?
      if attributes[:email] != self.manager.email
        attributes.delete(:id)
        self.manager.mark_for_destruction
      end
    end
    super(attributes)
  end

  def guests_attributes=(attributes)
    new_attributes = []
    attributes = attributes.map{ |_, a| a || _ }
    attributes.each do |_, guest_attributes|
      guest_attributes ||= _
      next if guest_attributes[:_destroy].present?
      if guest_attributes[:id].present? && guest_attributes[:email].present?
        updating_guest = self.guests.find(guest_attributes[:id])
        if updating_guest[:email] != guest_attributes[:email]
          new_attributes.push({
            name: guest_attributes[:name],
            email: guest_attributes[:email]
          })
          guest_attributes[:name] = updating_guest.name
          guest_attributes[:email] = updating_guest.email
          guest_attributes[:_destroy] = true
        end
      end
    end

    attributes += new_attributes
    super(attributes)
  end

  def as_json
    event = self
    calendar_event_attributes = {
      id: event.id,
      manager_id: event.manager.id,
      manager_name: event.manager.name,
      manager_email: event.manager.email,
      starts_at: event.starts_at,
      ends_at: event.ends_at,
      address: { name: event.address, lat: event.lat, lng: event.lng },
      guests: event.guests.map{ |guest| { id: guest.id, name: guest.name, email: guest.email } },
      enrollment_ids: event.stages.map(&:enrollment_id),
      applicant_ids: event.applicant_ids
    }
    calendar_event_attributes.to_json
  end

  private
    def starts_at_before_ends_at
      if self.ends_at.present?
        self.errors.add(:starts_at, "Fecha de inicio después de la fecha de fin") if self.starts_at > self.ends_at
      end
    end

    def set_olds
      self.prev_manager = self.manager
      self.resourceables_changed = false
      self.guests_changed = false
      self.old_stages = self.stages
      self.old_guests = self.guests
    end

    def notify_creation
      # notify stages
      # lo hace al crear cada stage en Calendar::EventResourceable

      # notify manager
      # lo hace al crear cada manager en Calendar::EventManager

      # notify guests
      # lo hace al crear cada guest en Calendar::EventGuest
    end

    def notify_updates
      objects_to_notify = self.guests + self.event_resourceables
      attributes = self.attributes.except('lat', 'lng',  'updated_at')
      self_changed = attributes.reduce(false){|value, key_value| value || self.send("#{key_value.first}_changed?")}

      if self.manager_changed?
        objects_to_notify.each do |object|
          # para gatillar acciones sólo en los que estaban antes
          # notificar a los stages menos a los nuevos, o sea, a los que quedaron
          object.touch if object.persisted? && !object.is_new && !object.marked_for_destruction?
        end
      elsif self_changed
        objects_to_notify.each do |object|
          object.touch if object.persisted? && !object.is_new && !object.marked_for_destruction?
        end
        self.manager.touch
      elsif self.resourceables_changed? && self.guests_changed?
        # notifico sólo a los guests y al manager
        self.guests.each do |object|
          object.touch if object.persisted? && !object.is_new && !object.marked_for_destruction?
        end
        self.manager.touch
      elsif self.resourceables_changed? || self.guests_changed?
        # notifico sólo a los guests y al manager
        self.guests.each do |object|
          object.touch if object.persisted? && !object.is_new && !object.marked_for_destruction?
        end
        self.manager.touch
      end
    end
end
