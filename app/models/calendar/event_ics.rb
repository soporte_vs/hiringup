class Calendar::EventIcs
  def self.draw(event)
    require 'icalendar'

    if event.stages.any?
      course = event.stages.first.enrollment.course
    else
      course = nil
    end
    ical = Icalendar::Calendar.new
    time = Time.zone.name
    ical.timezone.tzid = time
    ical_event = Icalendar::Event.new
    ical_event.created = Icalendar::Values::DateTime.new(event.created_at)
    ical_event.dtstart = Icalendar::Values::DateTime.new(event.starts_at, tzid: "#{time}")
    ical_event.dtend = event.ends_at ? Icalendar::Values::DateTime.new(event.ends_at, tzid: "#{time}") : Icalendar::Values::DateTime.new((event.starts_at + 3600), tzid: "#{time}")
    ical_event.organizer = Icalendar::Values::CalAddress.new("#{event.created_by.email}", cn: "#{event.created_by.first_name} #{event.created_by.last_name}")

    # guests
    event.guests.each do |guest|
      attendee_params = {
        "CUTYPE"   => "INDIVIDUAL",
        "ROLE"     => "REQ-PARTICIPANT",
        "PARTSTAT" => "NEEDS-ACTION",
        "RSVP"     => "TRUE",
        "CN"       => "#{guest.name}",
        #"X-NUM-GUESTS" => "0"
      }
      attendee_value = (Icalendar::Values::CalAddress.new(
        "mailto:#{guest.email}", attendee_params
      ))
      ical_event.append_attendee(attendee_value)
    end

    # enrollments
    enrollments = event.stages.map(&:enrollment)
    enrollments.each do |enrollment|
      attendee_params = {
        "CUTYPE"   => "INDIVIDUAL",
        "ROLE"     => "REQ-PARTICIPANT",
        "PARTSTAT" => "NEEDS-ACTION",
        "RSVP"     => "TRUE",
        "CN"       => "#{enrollment.applicant.first_name} #{enrollment.applicant.last_name}",
        #"X-NUM-GUESTS" => "0"
      }
      attendee_value = (Icalendar::Values::CalAddress.new(
        "mailto:#{enrollment.applicant.email}", attendee_params
      ))
      ical_event.append_attendee(attendee_value)
    end

    # manager
    attendee_params = {
      "CUTYPE"   => "INDIVIDUAL",
      "ROLE"     => "REQ-PARTICIPANT",
      "PARTSTAT" => "NEEDS-ACTION",
      "RSVP"     => "TRUE",
      "CN"       => "#{event.manager.name}",
      #"X-NUM-GUESTS" => "0"
    }
    attendee_value = (Icalendar::Values::CalAddress.new(
      "mailto:#{event.manager.email}", attendee_params
    ))
    ical_event.append_attendee(attendee_value)

    if course.present?
      ical_event.summary = course.title
      ical_event.description = course.title
    else
      ical_event.summary = "Entrevista en #{$COMPANY[:name]}"
      ical_event.description = "Entrevista en #{$COMPANY[:name]}"
    end

    ical_event.location = event.try(:address)
    ical_event.status = "CONFIRMED"
    ical_event.sequence = 0
    ical_event.transp = "OPAQUE"
    ical_event.ip_class = "PRIVATE"
    ical_event.geo = ["#{event.lat}", "#{event.lng}"]

    ical_event.uid = "event-#{event.id}-#{event.created_at}"
    # Alarma
    ical_event.alarm do |alarm|
      alarm.action = "DISPLAY"
      alarm.summary = "Recordatorio"
      if course.present?
        alarm.description = course.title
      else
        alarm.description = "Entrevista en #{$COMPANY[:name]}"
      end
      alarm.trigger = "-PT15M"
    end
    ical.add_event(ical_event)
    ical.ip_method = "REQUEST"
    return ical
  end
end
