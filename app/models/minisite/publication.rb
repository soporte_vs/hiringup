# coding: utf-8
# == Schema Information
#
# Table name: minisite_publications
#
#  id                       :integer          not null, primary key
#  course_id                :integer
#  description              :text
#  title                    :string
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  company_contract_type_id :integer
#  aasm_state               :string
#

class Minisite::Publication < ActiveRecord::Base
  include AASM
  include PublicationDeactivatable

  aasm do
    state :opened, initial: true
    state :closed

    event :close do
      transitions from: :opened, to: :closed
    end

    event :open do
      transitions from: :closed, to: :opened, guards: [:course_opened?]
    end
  end

  belongs_to :course
  belongs_to :company_contract_type,
             class_name: Company::ContractType,
             foreign_key: :company_contract_type_id

  has_many :postulations,
           class_name: Minisite::Postulation,
           foreign_key: :minisite_publication_id,
           dependent: :destroy

  has_many :applicants, through: :postulations

  has_many :questions, -> { order('id ASC') },
           class_name: Minisite::Question,
           foreign_key: :minisite_publication_id,
           inverse_of: :minisite_publication,
           dependent: :destroy

  has_many :minisite_category_publications,
           class_name: Minisite::CategoryPublication,
           foreign_key: :minisite_publication_id,
           inverse_of: :minisite_publication,
           dependent: :destroy

  has_many :minisite_categories,
            through: :minisite_category_publications

  validates :course, presence: true
  validates :title, presence: true
  validates :description, presence: true
  validates :desactivate_at, presence: true

  accepts_nested_attributes_for :questions, allow_destroy: true, reject_if: :reject_question?

  def is_new
    currDate = Date.today
    createdTime = self.created_at.to_date
    return (currDate - createdTime) <= 7
  end

  def title=(value)
    self[:title] = value.to_s.strip
  end

  def seo_url
    Rails.application.config.action_mailer.default_url_options[:host] + '/' + seo_path
  end

  def seo_path
    "#{self.id}-#{self.title.gsub(/\W/, '-')}"
  end

  def pdf_name
    "#{self.created_at.strftime('%Y_%m_%d')}-#{self.title.strip.gsub(/[^a-zA-Z0-9áéíóúñÁÉÍÓÚÑÜü]/, '-')}"
  end

  def shortened_url
    ::Shortener::ShortenedUrl.generate(self.seo_path)
  end

  def description
    _description = super
    if _description.present?
      # reemplaza la viñeta por un "- "
      _description.gsub(" ", "- ")
    end
    return _description
  end

  def desactivation_scheduled
    if self.opened?
      self.close!
    end
  end

  private
    def reject_question?(attributed)
      attributed['id'].blank? && attributed['description'].blank?
    end

    def course_opened?
      course.opened?
    end
end
