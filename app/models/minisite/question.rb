# == Schema Information
#
# Table name: minisite_questions
#
#  id                      :integer          not null, primary key
#  minisite_publication_id :integer
#  description             :text
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#

class Minisite::Question < ActiveRecord::Base
  belongs_to :minisite_publication,
             class_name: Minisite::Publication,
             inverse_of: :questions

  has_many :answers,
           class_name: Minisite::Answer,
           foreign_key: :minisite_question_id,
           inverse_of: :minisite_question

  validates :minisite_publication, presence: true
  validates :description, presence: true, uniqueness: { scope: [:minisite_publication_id] }
end
