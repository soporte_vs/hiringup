class Minisite::Banner < ActiveRecord::Base
  mount_uploader :image, MinisiteBannerUploader

  after_save :set_active_image
  validates :title, presence: true
  validates_presence_of :image, on: :create
end

  private
  def set_active_image
    if self.active_image == true
      Minisite::Banner.where.not(id: self.id).where(active_image: true).update_all(active_image: false)
    end
end
