# coding: utf-8
# == Schema Information
#
# Table name: minisite_postulations
#
#  id                      :integer          not null, primary key
#  minisite_publication_id :integer
#  applicant_id            :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  is_accepted             :boolean
#  accepted_at             :date
#

class Minisite::Postulation < ActiveRecord::Base
  include CoursePostulationConnector

  belongs_to :minisite_publication, class_name: Minisite::Publication
  belongs_to :applicant, class_name: Applicant::Base, foreign_key: :applicant_id


  has_many :answers,
           class_name: Minisite::Answer,
           foreign_key: :minisite_postulation_id,
           inverse_of: :minisite_postulation,
           dependent: :destroy

  has_many :questions, through: :minisite_publication

  validates :minisite_publication, presence: true
  validates :applicant, presence: true

  validates :applicant_id, uniqueness: { scope: :minisite_publication_id }
  validate :applicant_not_enrolled!, on: :create
  validate :all_answers_present!, on: :create
  validate :applicant_white_rut?
  delegate :course, to: :minisite_publication, allow_nil: true
  delegate :accept, :accept!, :reject, :reject!, to: :course_postulation, allow_nil: true

  accepts_nested_attributes_for :answers

  after_create :notification_update_profile
  after_create :send_internal_postulation_template

  def publication
    return minisite_publication
  end

  def notification_update_profile
    unless is_internal?
      Minisite::PostulationMailer.delay.update_profile(self)
    end
  end

  def send_internal_postulation_template
    if is_internal?
      course = self.minisite_publication.course
      applicant = self.applicant
      Courses::InternalMailer.delay.send_internal_postulation_template(course, applicant)
    end
  end

  def is_internal?
    self.minisite_publication.course.type_enrollment.constantize == Enrollment::Internal
  end

  def is_base?
    self.minisite_publication.course.type_enrollment.constantize == Enrollment::Base
  end

  def is_simple?
    self.minisite_publication.course.type_enrollment.constantize == Enrollment::Simple
  end

  def answers_with_hupe_format
    # En producción ordena correctamente colocando
    # este order, es raro pero funciona :/
    self.answers.order(id: :asc).map do |answer|
      {
        question: answer.minisite_question.description,
        answer: answer.description
      }
    end
  end

  def postulation_date
    self.created_at
  end

  private

    def applicant_not_enrolled!
      course = self.minisite_publication.try(:course)
      return false if !course
      all_applicant = (course.applicants + self.minisite_publication.applicants).uniq
      if all_applicant.include? self.applicant
        errors.add(:publication_enroll, I18n.t('active_record.errors.model.minisite.publication.enroll.already'))
        false
      else
        true
      end
    end

    def all_answers_present!
      if self.questions.count != self.answers.length
        errors.add(:answers, :incomplete)
        false
      end
    end

    def applicant_white_rut?
      if self.minisite_publication.try(:course).try(:type_enrollment).try(:constantize) == Enrollment::Internal || self.minisite_publication.try(:course).try(:type_enrollment).try(:constantize) == Enrollment::InternalTransfer
      #if self.minisite_publication.try(:course).try(:type_enrollment).try(:constantize) == Enrollment::Internal
        white_rut = Company::WhiteRut.find_by_rut(self.applicant.try(:get_rut))
        unless white_rut.present?
          self.errors.add(:applicant, :white_rut_invalid)
          return false
        end
      end
      return true
    end

end
