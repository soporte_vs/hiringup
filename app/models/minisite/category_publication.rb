class Minisite::CategoryPublication < ActiveRecord::Base

  belongs_to :minisite_category, class_name: Minisite::Category, inverse_of: :minisite_category_publications
  belongs_to :minisite_publication, class_name: Minisite::Publication, inverse_of: :minisite_category_publications

  validates :minisite_category, presence: true
  validates :minisite_publication, presence: true

end
