# == Schema Information
#
# Table name: minisite_answers
#
#  id                      :integer          not null, primary key
#  description             :string
#  minisite_question_id    :integer
#  minisite_postulation_id :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#

class Minisite::Answer < ActiveRecord::Base
  belongs_to :minisite_question, class_name: Minisite::Question, inverse_of: :answers
  belongs_to :minisite_postulation, class_name: Minisite::Postulation, inverse_of: :answers

  validates :minisite_question, presence: true, uniqueness: { scope: [:minisite_postulation] }
  validates :minisite_postulation, presence: true
  validates :description, presence: true

  def ouc
    puts 'dss'
  end
end
