class Minisite::Category < ActiveRecord::Base
  mount_uploader :image, ImageUploader

  has_many :minisite_category_publications,
           class_name: Minisite::CategoryPublication,
           foreign_key: :minisite_category_id,
           inverse_of: :minisite_category,
           dependent: :destroy

  has_many :minisite_publications,
            through: :minisite_category_publications

  validates :name, presence: true
  validates :description, presence: true

end
