# coding: utf-8
# == Schema Information
#
# Table name: laborum_postulations
#
#  id                     :integer          not null, primary key
#  laborum_publication_id :integer
#  applicant_id           :integer
#  user_portal_id         :integer
#  answer1                :string
#  answer2                :string
#  answer3                :string
#  answer4                :string
#  answer5                :string
#  information            :text
#  cv                     :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  is_accepted            :boolean
#  accepted_at            :date
#

class Laborum::Postulation < ActiveRecord::Base
  include CoursePostulationConnector

  belongs_to :laborum_publication, class_name: Laborum::Publication
  belongs_to :applicant, class_name: Applicant::Base, foreign_key: :applicant_id

  validates :laborum_publication, presence: true
  validates :user_portal_id, presence: true
  validates :applicant, presence: true

  serialize :information

  validates :user_portal_id, uniqueness: { scope: :laborum_publication_id }
  validates :applicant_id, uniqueness: { scope: :laborum_publication_id }

  before_validation :set_applicant_and_info, unless: 'self.persisted?'

  delegate :full_name, to: :applicant
  delegate :first_name, to: :applicant
  delegate :last_name, to: :applicant
  delegate :course, to: :laborum_publication, allow_nil: true
  delegate :accept, :accept!, :reject, :reject!, to: :course_postulation, allow_nil: true

  def answers_with_hupe_format
   answers = []
   (1..5).each do |index|
     question = laborum_publication.send("question#{index}")
     if question.try(:present?)
       answers << { question: question, answer: send("answer#{index}") }
     end
   end
   answers
  end

  def publication
    return laborum_publication
  end

  def postulation_date
    self.created_at
  end

  def get_identification_document_type(country_name, type_name)
    hupe_country = Territory::Country.find_by(name: country_name)
    # se define el codename para buscar según el tipo de documento
    document_codename = type_name.downcase
    if type_name == 'R.U.T.'
      document_codename = 'rut'
    elsif type_name == 'Pasaporte'
      document_codename = 'pasaporte' # así lo tenemos generado con el seed
    end

    identification_document_type = People::IdentificationDocumentType.find_by(
      country: hupe_country,
      codename: document_codename
    )
    identification_document_type
  end

  private
    def set_applicant_and_info
      data = Bumeran.get_postulacion(user_portal_id)
      self.information = data
      email = data['curriculum']['email']

      user = nil

      country_name = ["curriculum"]["pais"].present?
      if data['curriculum']['numeroDocumento'].present? &&
         data["curriculum"]['tipoDocumento'].present? &&
         country_name
        document_number = data['curriculum']['numeroDocumento']


        identification_document_type = Laborum::Postulation.get_identification_document_type(
          country_name,
          data["curriculum"]['tipoDocumento']
        )

        user = User.joins(:personal_information).find_by(
          people_personal_informations: {
            identification_document_number: document_number,
            identification_document_type_id: identification_document_type.id
          }
        ) if identification_document_type.present?
      end

      # si no se encontró el usuario por RUT, entonces se busca por email
      user ||= User.find_by(email: email.downcase)
      if user.present?
        unless user.applicant.present?
          create_applicant(user)
        end
      else
        user = create_user(data)
      end
      self.applicant = user.applicant
      set_answers(data)
    end

    def create_user(data)
      user = User.new
      user.email = data['curriculum']['email']
      user.personal_information = People::PersonalInformation.new(
        first_name: data['curriculum']['nombre'],
        last_name: data['curriculum']['apellido'],
        sex: (data['curriculum']['genero'] == "masculino" ? 'M' : 'F'),
        landline: data['curriculum']['telefonoFijo'],
        cellphone: data['curriculum']['telefonoCelular'],
        birth_date: data['curriculum']['fechaNacimiento'],
        address: data['curriculum']['direccion']
      )
      user.applicant = Laborum::Applicant.new(user: user)
      user.invite! { |u| u.skip_invitation = true }
      return user
    end

    def create_applicant(user)
      Laborum::Applicant.create(user: user)
    end

    def set_answers(data)
      answers = {}
      ['question1', 'question2', 'question3', 'question4', 'question5' ].each do |q|
        data['respuestas'].each do |laborum_question|
          if laborum_question['pregunta'] == laborum_publication.send(q)
            answers[q.gsub('question', 'answer')] = laborum_question['respuesta']
          end
        end
      end
      self.attributes = answers
    end

end
