# == Schema Information
#
# Table name: applicant_bases
#
#  id          :integer          not null, primary key
#  type        :string
#  user_id     :integer
#  created_at  :datetime
#  updated_at  :datetime
#  blacklisted :boolean
#

class Laborum::Applicant < Applicant::Base
  __elasticsearch__.document_type 'applicants'
  __elasticsearch__.index_name "#{PREFIX_INDEX}"

   after_create :invite_user

   private

    def invite_user
     if self.valid? and !self.user.worker.present?
       self.user.delay.invite!
     end
   end

end
