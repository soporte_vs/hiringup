# == Schema Information
#
# Table name: laborum_publications
#
#  id                :integer          not null, primary key
#  service_id        :integer
#  title             :string
#  description       :text
#  state_id          :integer
#  city_id           :integer
#  area_id           :integer
#  subarea_id        :integer
#  pay_id            :integer
#  language_id       :integer
#  language_level_id :integer
#  type_job_id       :integer
#  study_area_id     :integer
#  study_type_id     :integer
#  study_status_id   :integer
#  salary            :integer
#  experience        :integer
#  question1         :string
#  question2         :string
#  question3         :string
#  question4         :string
#  question5         :string
#  course_id         :integer
#  status            :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  address           :string
#

class Laborum::Publication < ActiveRecord::Base
  include PublicationDeactivatable
  belongs_to :course

  has_many :postulations, class_name: Laborum::Postulation, foreign_key: :laborum_publication_id, dependent: :destroy
  has_many :applicants, through: :postulations

  STATUS = ['publishing', 'published', 'unpublished']

  validates :course, presence: true
  validates :title, presence: true
  validates :description, presence: true
  validates :pay_id, presence: true
  validates :area_id, presence: true
  validates :subarea_id, presence: true
  validates :study_status_id, presence: true
  validates :study_type_id, presence: true

  validates :service_id, presence: true, on: :update
  validates :status, inclusion: {
              in: STATUS,
              message: "%{value} is not a valid status"
            }
  validates :desactivate_at, presence: true

  before_validation { self.status = 'publishing' if self.status.nil? }
  after_create :publish
  after_update :update_bumeran_publication
  before_destroy :unpublish

  DEFAULT_COUNTRY = Rails.env == 'development' ? 1 : 1007

  def get_laborum_publication_path
    "#{Laborum.get_publication_url}/#{service_id}.html"
  end

  def get_publication_data
    publication = Bumeran::Publication.new

    publication.body[:denominacion][:id] = denomination_id
    publication.body[:titulo] = title
    publication.body[:descripcion] = description
    publication.body[:tipoTrabajoId] = type_job_id if type_job.present?

    publication.body[:preguntas] = []
    questions.each do |q|
      publication.body[:preguntas].push(
        { simple: { texto: q }}
      )
    end

    publication.body[:lugarTrabajo][:paisId] = DEFAULT_COUNTRY

    if state_id.present?
      publication.body[:lugarTrabajo][:zonaId] = state_id
    else
      publication.body[:lugarTrabajo].delete :zonaId
    end

    if city_id.present?
      publication.body[:lugarTrabajo][:localidadId] = city_id
    else
      publication.body[:lugarTrabajo].delete :localidadId
    end

    publication.body[:lugarTrabajo][:direccion] = address

    if area_id.present?
      publication.body[:areaId] = area_id
    else
      publication.body.delete :areaId
    end

    if subarea_id.present?
      publication.body[:subAreaId] = subarea_id
    else
      publication.body.delete :subAreaId
    end

    if language_id.present?
      publication.body[:requisitos][:idiomas][0][:idiomaId] = language_id
      if language_id.present? and language_level_id.present?
        publication.body[:requisitos][:idiomas][0][:nivelId] = language_level_id
      end
    else
      publication.body[:requisitos].delete :idiomas
    end

    publication.body[:requisitos][:salario][:tipo] = 'neto'
    if pay_id.present?
      publication.body[:requisitos][:salario][:frecuenciaPagoId] = pay_id
    else
      publication.body[:requisitos][:salario].delete :frecuenciaPagoId
    end

    publication.body[:requisitos][:salario][:salarioMaximo] = salary || 0
    publication.body[:requisitos][:salario][:solicitarCandidato] = true

    publication.body[:requisitos][:experiencia][:minimo] = experience || 0
    if study_type_id.present?
      publication.body[:requisitos][:educacion][:tipoEstudioId] = study_type_id
    else
      publication.body[:requisitos][:educacion].delete :tipoEstudioId
    end

    if study_status_id.present?
      publication.body[:requisitos][:educacion][:estadoEstudioId] = study_status_id
    else
      publication.body[:requisitos][:educacion].delete :estadoEstudioId
    end

    publication.body[:requisitos].delete :edad
    publication.body[:requisitos].delete :genero
    publication.body[:requisitos].delete :residencia

    publication
  end

  def publish
    # plan de publicaciones 30 por default
    response = Bumeran.publish(self.get_publication_data.body.to_json, DEFAULT_COUNTRY, 30)
    self.update(service_id: response.body.to_i, status: 'published')
  end
  handle_asynchronously :publish

  def unpublish
    Bumeran.destroy_publication(service_id)
    self.update(status: 'unpublished')
  end
  handle_asynchronously :unpublish

  def update_bumeran_publication
    Bumeran.update_publication(service_id, self.get_publication_data.body.to_json)
  end
  handle_asynchronously :update_bumeran_publication

  def get_postulations
    data = Bumeran.get_postulations_in_publication(
      (Rails.env == 'development' or Rails.env == 'test') ?  1002561903 : service_id
    )
    data['content'].each do |p|
      if p['id'].present?
        postulation = Laborum::Postulation.where(
          laborum_publication_id: self.id,
          user_portal_id: p['id']
        ).first_or_create
      end
    end
  end
  handle_asynchronously :get_postulations

  def desactivation_scheduled
    if self.status = 'publishing' || self.status = 'published'
      self.unpublish
    end
  end

  # overwrite getters
  def state
    md = Rails.cache.fetch("bumeran/regiones/#{Laborum::Publication::DEFAULT_COUNTRY}", expires_in: 6.hours) do
      Bumeran.get_zonas_in(Laborum::Publication::DEFAULT_COUNTRY)
    end
    md[self.state_id]['nombre'] if md[self.state_id].present?
  end

  def city
    if state_id and city_id
      md = Rails.cache.fetch("bumeran/comunas/#{state_id}", expires_in: 6.hours) do
        Bumeran.localidad(city_id)
      end
      md[city_id]['nombre'] if md[city_id].present?
    end
  end

  def area
    md = meta_data('areas')
    md[area_id]['nombre'] if md[area_id].present?
  end

  def subarea
    md = meta_data('subareas')
    md[subarea_id]['nombre'] if md[subarea_id].present?
  end

  def pay
    md = meta_data('frecuencias_pago')
    md[pay_id]['nombre'] if md[pay_id].present?
  end

  def language
    md = meta_data('idiomas')
    md[language_id]['nombre'] if md[language_id].present?
  end

  def language_level
    md = meta_data('niveles_idiomas')
    md[language_level_id]['nombre'] if md[language_level_id].present?
  end

  def type_job
    md = meta_data('tipos_trabajo')
    md[type_job_id]['nombre'] if md[type_job_id].present?
  end

  def study_area
    md = meta_data('areas_estudio')
    md[study_area_id]['nombre'] if md[study_area_id].present?
  end

  def study_type
    md = meta_data('tipos_estudio')
    md[study_type_id]['nombre'] if md[study_type_id].present?
  end

  def study_status
    md = meta_data('estados_estudio')
    md[study_status_id]['nombre'] if md[study_status_id].present?
  end

  def denomination
    md = meta_data('denominaciones')
    md[denomination_id]['nombre'] if md[denomination_id].present?
  end

  def questions
    [
      'question1', 'question2', 'question3', 'question4', 'question5'
    ].reject { |q| !send(q).present? }
     .map { |q| self.send(q) }
  end

  def self.has_account?
    return (
      Bumeran.client_id.present? and
      Bumeran.username.present? and
      Bumeran.email.present? and
      Bumeran.password.present?
    )
  end

  #en el caso de bancochile el confidencial es -1,
  # se debe verificar bien con los otros clientes
  def is_confidential?
    denomination_id.eql?(-1) ? true : false
  end

  private
    def meta_data(data)
      Rails.cache.fetch("bumeran/#{data}", expires_in: 6.hours) do
        Bumeran.send(data)
      end
    end
end
