module CourseSearchable

  extend ActiveSupport::Concern

  included do
    include Elasticsearch::Model

    __elasticsearch__.document_type 'courses'
    __elasticsearch__.index_name "#{PREFIX_INDEX}"
    # ES callbacks
    after_commit on: :create do
      index_document
    end

    after_commit on: :update do
      index_document
    end

    after_commit on: :destroy do
      delete_document ignore: 404
    end

    RAW = { raw: { type: "string", index: "not_analyzed" } }
    RAW_LIKE = { raw_like: { type: 'string', analyzer: 'rawlike' } }


    settings ELASTIC_SETTINGS_OPTIONS do
      mappings dynamic: 'false' do
        indexes :id, type: :integer
        indexes :title, analyzer: :first_name, boost: 2.0, fields: RAW
        indexes :position, analyzer: :spanish, fields: RAW
        indexes :contract_type, analyzer: :spanish, fields: RAW
        indexes :engagement_origin, analyzer: :spanish, fields: RAW
        indexes :city, analyzer: :full_name, fields: RAW
        indexes :state, analyzer: :full_name, fields: RAW
        indexes :country, analyzer: :full_name, fields: RAW
        indexes :type_enrollment, analyzer: :full_name, fields: RAW
        indexes :tags, fields: RAW
        indexes :aasm_state, fields: RAW
        indexes :created_by, analyzer: :rawlike, fields: RAW
        indexes :updated_at, type: :date
        indexes :created_at, type: :date
      end
    end

    def self.inrefresh
      self.__elasticsearch__.update_mapping!
      self.__elasticsearch__.import type: 'courses', batch_size: 100
    end

    def self.query(query)
      self.__elasticsearch__.search(query)
    end
  end

  def delete_document(options={})
    __elasticsearch__.delete_document(options) rescue nil
  end

  def index_document
    __elasticsearch__.index_document
  end

  def as_indexed_json(options={})
    course_type_enrollment = safe_enrollment_type
    course_type_enrollment = course_type_enrollment[:name] if course_type_enrollment.present?
    course_info = {
      id: id,
      title: title.try(:strip),
      type_enrollment: course_type_enrollment,
      position: company_position.try(:name),
      city: territory_city.try(:name), fields: RAW,
      state: territory_city.try(:territory_state).try(:name),
      country: territory_city.try(:territory_state).try(:territory_country).try(:name),
      health: self.health,
      contract_type: contract_type.try(:name),
      engagement_origin: engagement_origin.try(:name),
      applicants_count: self.applicants.count,
      pending_postulations_count: self.total_pending_postulations,
      hired_applicants_count: self.hired_candidates.length,
      aasm_state: self.aasm_state,
      created_by: self.user.try(:name_or_email),
      created_at: created_at,
      updated_at: updated_at
    }

    course_info[:tags] = (self.tags.map(&:name) || [])

    course_info.as_json
  end
end
