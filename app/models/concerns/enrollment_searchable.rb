# coding: utf-8
module EnrollmentSearchable

  extend ActiveSupport::Concern

  included do
    include Elasticsearch::Model
    __elasticsearch__.document_type 'enrollments'
    __elasticsearch__.index_name "#{PREFIX_INDEX}"
    # ES callbacks
    after_commit on: :create do
      index_document
    end

    after_commit on: :update do
      index_document
    end

    after_commit on: :destroy do
      delete_document
    end

    RAW = { raw: { type: "string", index: "not_analyzed" } }
    RAW_LIKE = { raw_like: { type: 'string', analyzer: 'rawlike' } }


    settings ELASTIC_SETTINGS_OPTIONS do
      mappings dynamic: 'false' do
        indexes :id, type: :integer
        indexes :course_id, type: :integer
        indexes :full_name, analyzer: :full_name, fields: RAW
        indexes :first_name, analyzer: :first_name
        indexes :last_name, analyzer: :first_name
        indexes :email, analyzer: :emails
        indexes :current_stage, analyzer: :full_name, fields: RAW
        indexes :discarded
        indexes :is_employee, fields: RAW
        indexes :updated_at, type: :date, fields: RAW
        indexes :identification_document_number, analyzer: :identification_document_numbers
        indexes :has_references
      end
    end

    def self.inrefresh
      self.__elasticsearch__.update_mapping!
      self.__elasticsearch__.import type: 'enrollments', batch_size: 100
    end

    def self.query(query)
      self.__elasticsearch__.search(query)
    end

  end

  def delete_document
    __elasticsearch__.delete_document
  end

  def index_document
    __elasticsearch__.index_document
  end


  def stage_url(stage)
    return nil unless stage.type.present?
    begin
      underscore = "course_#{stage.type.split('::').first.underscore}_stage_path"
      return Rails.application.routes.url_helpers.send(underscore, id: self.course.id, stage_id: stage.id)
    rescue Exception => e
      ErrorsMailer.delay.errors_developers("Error al tratar de indexar la url del siguiente objecto", stage, e)
    end
  end

  def as_indexed_json(options={})

    return {} unless self.applicant
    # avatar = self.applicant.user.avatar
    # avatar = "/assets/#{avatar}" unless avatar.split('/').count > 1
    current_stage = self.stage.present? ? self.stage.name : nil
    current_stage ||= self.invitable? ? 'Screening' : 'Finalizó proceso'
    next_stage = next_stage_class.present? ? next_stage_class[:name] : nil
    next_stage ||= self.stage.present? ? 'Finalizó proceso' : nil
    calendar_event_attributes = {}
    event_resourceable = self.stage.present? ? self.stage.event_resourceables.first : nil
    event = event_resourceable.try(:event)
    if event.present?
      calendar_event_attributes = {
        id: event.id,
        manager_id: event.manager.id,
        manager_name: event.manager.name,
        manager_email: event.manager.email,
        starts_at: event.starts_at,
        ends_at: event.ends_at,
        observations: event.observations,
        address: { name: event.address, lat: event.lat, lng: event.lng },
        guests: event.guests.map{ |guest| { id: guest.id, name: guest.name, email: guest.email } },
        enrollment_ids: event.stages.map(&:enrollment_id),
        event_resourceable: event_resourceable
      }
    end
    stages = self.stages.order("created_at ASC") # asegura el orden correcto
    answers = self.postulation.try(:answers_with_hupe_format) || []

    enrollment_info = {
      answers: answers,
      id: id,
      identification_document_number: self.applicant.user.personal_information.identification_document_number,
      identification_document_type: self.applicant.user.personal_information.try(:identification_document_type).try(:name) || "",
      identification_document_country: self.applicant.user.personal_information.try(:identification_document_type).try(:country).try(:name) || "",
      course_id: self.course_id,
      applicant_id: self.applicant_id,
      email: self.applicant.try(:email).try(:strip),
      cellphone: self.applicant.try(:cellphone),
      full_name: "#{applicant.name_or_email}".try(:strip),
      first_name: "#{applicant.first_name}".try(:strip),
      last_name: "#{applicant.last_name}".try(:strip),
      current_stage: current_stage,
      current_stage_approved: self.try(:stage).try(:is_approved?),
      calendar_event: calendar_event_attributes,
      next_stage: next_stage,
      discarded: self.discarding.present? ? true : false,
      blacklisted: self.applicant.blacklisted ? true : false,
      is_employee: self.applicant.user.is_employee?,
      internal_image: ActionController::Base.helpers.image_url("favicon.png"),
      info_request_count: self.applicant.update_info_requests.count,
      stages: stages.map{ |stage|
        {
          id: stage.id,
          name: stage.name,
          type: stage.type,
          url: stage_url(stage),
          created_at: stage.created_at,
          is_approved: stage.is_approved?
        }
      },
      avatar_url: ActionController::Base.helpers.image_path(self.applicant.avatar),
      discarding: {
        reason: self.discarding.try(:discard_reason).try(:name),
        observations: self.discarding.try(:observations),
        errors: self.discarding.try(:errors)
      },
      course_invitation: {
        exists: self.invited?,
        accepted: self.course_invitation.try(:accepted),
        errors: self.course_invitation.try(:errors)
      },
      updated_at: self.updated_at,
      has_references: self.applicant.has_references?,
      errors: self.errors
    }

    enrollment_info.as_json
  end
end
