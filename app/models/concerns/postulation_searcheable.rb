# coding: utf-8
module PostulationSearcheable

  extend ActiveSupport::Concern

  included do
    include Elasticsearch::Model
    __elasticsearch__.document_type 'postulations'
    __elasticsearch__.index_name "#{PREFIX_INDEX}"
    # ES callbacks
    after_commit on: :create do
      index_document
    end

    after_commit on: :update do
      index_document
    end

    after_commit on: :destroy do
      delete_document
    end

    settings ELASTIC_SETTINGS_OPTIONS do
      mappings dynamic: 'false' do
        indexes :id, type: :integer
        indexes :course_id, type: :integer
        indexes :portal, analyzer: :full_name, fields: RAW
        indexes :state, analyzer: :full_name, fields: RAW
        indexes :is_employee, fields: RAW
        indexes :full_name, analyzer: :full_name, fields: RAW
        indexes :first_name, analyzer: :first_name
        indexes :last_name, analyzer: :first_name
        indexes :created_at, type: :date
        indexes :identification_document_number, analyzer: :identification_document_numbers
        indexes :has_references
      end
    end

    def self.inrefresh
      self.__elasticsearch__.update_mapping!
      self.__elasticsearch__.import type: 'postulations', batch_size: 100
    end

    def self.query(query)
      self.__elasticsearch__.search(query)
    end

  end

  def delete_document
    __elasticsearch__.delete_document
  end

  def index_document
    __elasticsearch__.index_document
  end

  def stage_url(stage)
    return nil unless stage.type.present?
    "#{self.course_id}/#{stage.type.split('::').first.underscore}/stages/#{stage.id}"
  end

  def as_indexed_json(options={})
    return {} unless self.applicant
    publication = self.publication
    state = self.is_accepted ? 'accepted' : (self.is_accepted == nil ? 'pending' : 'rejected')

    postulation_info = {
      id: id,
      postulationable_id: postulationable_id,
      course_id: course_id,
      applicant_id: self.applicant_id,
      avatar_url: ActionController::Base.helpers.image_path(self.applicant.avatar),
      # se usan los try en este porque cuando el usuario se registra, user ese momento no tiene personal_information
      identification_document_number: self.applicant.user.try(:personal_information).try(:identification_document_number),
      identification_document_type: self.applicant.user.personal_information.try(:identification_document_type).try(:name) || "",
      identification_document_country: self.applicant.user.personal_information.try(:identification_document_type).try(:country).try(:name) || "",
      full_name: "#{applicant.name_or_email.try(:strip)}",
      first_name: "#{applicant.first_name.try(:strip)}",
      last_name: "#{applicant.last_name.try(:strip)}",
      email: self.applicant.try(:email),
      cellphone: self.applicant.try(:cellphone),
      portal: self.postulationable.class.parent.to_s,
      state: state,
      answers: self.answers_with_hupe_format,
      errors: self.errors,
      blacklisted: self.applicant.blacklisted ? true : false,
      is_employee: self.applicant.user.is_employee?,
      internal_image: ActionController::Base.helpers.image_url("favicon.png"),
      info_request_count: self.applicant.update_info_requests.count,
      created_at: postulationable.created_at,
      has_references: self.applicant.has_references?
    }

    postulation_info.as_json
  end
end
