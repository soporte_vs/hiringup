module CoursePostulationConnector
  extend ActiveSupport::Concern

  included do
    has_one :course_postulation,
        as: :postulationable,
        foreign_key: :postulationable_id,
        dependent: :destroy

    after_commit on: :create do
      generate_course_postulation
    end

    after_commit on: :update do
      course_postulation.index_document
    end

    after_commit on: :destroy do
      course_postulation.delete_document
    end
  end

  def accept
  end

  def reject
  end

  def answers_with_hupe_format
  end

  def generate_course_postulation
    CoursePostulation.find_or_create_by(course: self.course, postulationable: self)
  end
end
