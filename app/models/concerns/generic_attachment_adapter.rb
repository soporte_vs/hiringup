module GenericAttachmentAdapter
  extend ActiveSupport::Concern

  def attachment_attribute=(attachment)
    self.build_attachment
    self.attachment.attachment = attachment
  end
end
