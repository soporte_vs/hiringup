module PublicationDeactivatable
  extend ActiveSupport::Concern

  included do
    after_create :schedule_deactivation
    before_update :update_deactivation, if: :desactivate_at_changed?
  end

  def schedule_deactivation
    delayed_job = self.delay(run_at: self.desactivate_at).desactivation_scheduled
    self.update_column(:delayed_job_id, delayed_job.id) # evita callbacks
  end

  def update_deactivation
    job = Delayed::Job.find_by(id: delayed_job_id)

    job.update(run_at: self.desactivate_at) if job.presence
  end

end
