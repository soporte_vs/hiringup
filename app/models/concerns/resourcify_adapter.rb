module ResourcifyAdapter
  extend ActiveSupport::Concern
  
  module ClassMethods
    def filter_by_role(role_name, user)
      if user.has_role? :admin
        self.all
      else
        self.with_role([role_name, :admin], user).uniq
      end
    end
  end

  def users_by_role(role_name)
    User.joins(:roles).where(
      'roles.name IN (\'admin\', :role_name) AND (roles.resource_type = :resource_type OR roles.resource_type is NULL) AND (roles.resource_id IS NULL OR roles.resource_id = :resource_id)',
       {role_name: role_name, resource_id: self.id, resource_type: self.class.to_s}
    ).uniq
  end
end