module ApplicantSearchable

  extend ActiveSupport::Concern

  included do
    include Elasticsearch::Model

    __elasticsearch__.document_type 'applicants'
    __elasticsearch__.index_name "#{PREFIX_INDEX}"
    # ES callbacks
    after_commit on: :create do
      index_document
    end

    after_commit on: :update do
      index_document
    end

    after_commit on: :destroy do
      delete_document
    end

    RAW = { raw: { type: "string", index: "not_analyzed" } }
    RAW_LIKE = { raw_like: { type: 'string', analyzer: 'rawlike' } }

    settings ELASTIC_SETTINGS_OPTIONS do
      mappings dynamic: 'false' do
        indexes :id, type: :integer
        indexes :first_name, analyzer: :first_name
        indexes :last_name, analyzer: :first_name
        indexes :full_name, analyzer: :full_name, fields: RAW
        indexes :email, analyzer: :emails
        indexes :sex, analyzer: :rawlike, fields: RAW
        indexes :blacklisted
        indexes :birth_date
        indexes :city, analyzer: :full_name, fields: RAW
        indexes :state, analyzer: :full_name, fields: RAW
        indexes :country, analyzer: :full_name, fields: RAW
        indexes :profile_progress, type: :integer
        indexes :comments, type: :nested do
          indexes :content, analyzer: :spanish
        end
        indexes :institutions, fields: RAW
        indexes :careers, fields: RAW
        indexes :study_type, fields: RAW
        indexes :availability_replacements, fields: RAW
        indexes :availability_work_other_residence, fields: RAW
        indexes :has_cv, fields: RAW
        indexes :related_courses, fields: RAW
        indexes :has_references
        # indexes :courses, type: :nested do
        #   indexes :id, type: :integer, index: :no
        #   indexes :title, analyzer: :spanish
        #   indexes :description, analyzer: :spanish
        #   indexes :position, analyzer: :spanish
        # end
        # indexes :degrees, type: :nested do
        #   indexes :institution, analyzer: :institutions, fields: RAW
        #   indexes :career, analyzer: :spanish, fields: RAW
        #   indexes :last_year, type: :integer
        # end
        # indexes :experience, type: :nested do
        #   indexes :position, analyzer: :spanish
        #   indexes :company, analyzer: :spanish
        # end
        indexes :created_at, type: :date
        indexes :is_employee, fields: RAW
        indexes :identification_document_number, analyzer: :identification_document_numbers
      end
    end

    def self.inrefresh
      self.__elasticsearch__.update_mapping!
      self.__elasticsearch__.import type: 'applicants', batch_size: 100
    end

    def self.query(query)
      self.__elasticsearch__.search(query)
    end

  end

  def delete_document
    __elasticsearch__.delete_document
  end

  def index_document
    __elasticsearch__.index_document
  end

  def as_indexed_json(options={})
    higher_study_level = People::Degree::TYPES.find{|type| type[0] == self.user.try(:personal_information).try(:study_type) }.try(:last)
    postulant_info = {
      id: id,
      # se usan los try en este porque cuando el usuario se registra, user ese momento no tiene personal_information
      identification_document_number: user.try(:personal_information).try(:identification_document_number),
      identification_document_type: user.personal_information.try(:identification_document_type).try(:name) || "",
      identification_document_country: user.personal_information.try(:identification_document_type).try(:country).try(:name) || "",
      first_name: "#{first_name.try(:strip)}",
      last_name: "#{last_name.try(:strip)}",
      full_name: "#{name_or_email.try(:strip)}",
      email: email.try(:strip),
      created_at: created_at,
      birth_date: birth_date,
      sex: user.sex_word,
      cellphone: user.personal_information.try(:cellphone),
      landline: user.personal_information.try(:landline),
      city: user.personal_information.try(:city).try(:name),
      state: user.personal_information.try(:city).try(:territory_state).try(:name),
      country: user.personal_information.try(:city).try(:territory_state).try(:territory_country).try(:name),
      profile_progress: profile_progress,
      avatar: ActionController::Base.helpers.image_path(avatar),
      blacklisted: blacklisted ? true : false,
      is_employee: user.is_employee?,
      internal_image: ActionController::Base.helpers.image_url("favicon.png"),
      comments: comments.map{|comment| { content: comment.content } },
      info_request_count: self.update_info_requests.count,
      study_type: higher_study_level,
      availability_replacements: user.try(:personal_information).try(:availability_replacements),
      availability_work_other_residence: user.try(:personal_information).try(:availability_work_other_residence),
      has_cv: self.has_document('cv'),
      related_courses: self.associated_courses.present? ? true : false,
      already_enrolled: self.is_enrolled?,
      has_references: self.has_references?
    }

    # postulant_info[:courses] = courses.map do |course|
    #   {
    #     id: course.id,
    #     title: course.title,
    #     description: course.description,
    #     position: course.try(:company_position).try(:name)
    #   }
    # end

    # postulant_info[:degrees] = (user.try(:professional_information).try(:degrees) || []).map do |degree|
    #   {
    #     institution: degree.institution.name,
    #     career: degree.career.name,
    #     last_year: degree.culmination_date.try(:year)
    #   }
    # end
    postulant_info[:institutions] = (self.user.try(:professional_information).try(:degrees) || []).map(&:institution).select{|i| i.present?}.map(&:name)
    postulant_info[:careers] = (self.user.try(:professional_information).try(:degrees) || []).map(&:career).select{|c| c.present?}.map(&:name)
    # postulant_info[:experience] = (user.try(:professional_information).try(:laboral_experiences) || []).map do |e|
    #   {
    #     position: e.position,
    #     company: e.company,
    #     since_date: e.since_date,
    #     until_date: e.until_date
    #   }
    # end

    postulant_info[:errors] = self.errors

    postulant_info[:events] = self.events.map { |event| {
        id: event.id,
        manager_id: event.manager.id,
        manager_name: event.manager.name,
        manager_email: event.manager.email,
        starts_at: event.starts_at,
        ends_at: event.ends_at,
        address: { name: event.address, lat: event.lat, lng: event.lng },
        observations: event.observations,
        guests: event.guests.map{ |guest| { id: guest.id, name: guest.name, email: guest.email } },
        applicant_ids: event.applicant_ids
      }
    }
    postulant_info.as_json
  end
end
