# coding: utf-8
module HiringRequestRecordSearchable

  extend ActiveSupport::Concern

  included do
    include Elasticsearch::Model
    __elasticsearch__.document_type 'hiring-request-records'
    __elasticsearch__.index_name "#{PREFIX_INDEX}"
    # ES callbacks
    after_commit on: :create do
      index_document
    end

    after_commit on: :update do
      index_document
    end

    after_commit on: :destroy do
      delete_document
    end

    RAW = { raw: { type: "string", index: "not_analyzed" } }
    RAW_LIKE = { raw_like: { type: 'string', analyzer: 'rawlike' } }


    settings ELASTIC_SETTINGS_OPTIONS do
      mappings dynamic: 'false' do
        indexes :id, type: :integer
        indexes :required_by, analyzer: :rawlike, boost: 2.0, fields: RAW
        indexes :request_reason, analyzer: :spanish, fields: RAW
        indexes :position, analyzer: :spanish, fields: RAW
        indexes :company, analyzer: :spanish, fields: RAW
        indexes :cenco, analyzer: :rawlike, fields: RAW
        indexes :business_unit, analyzer: :rawlike, fields: RAW
        indexes :created_by, analyzer: :rawlike, fields: RAW
        indexes :aasm_state, fields: RAW
        indexes :created_at, type: :date
      end
    end

    def self.inrefresh
      # Version antigua donde habia un indice por cada estructura de datos
      # self.__elasticsearch__.create_index! force: true
      # self.__elasticsearch__.import batch_size: 100

      self.__elasticsearch__.update_mapping!
      self.__elasticsearch__.import type: 'hiring-request-records', batch_size: 100
    end

    def self.query(query)
      self.__elasticsearch__.search(query)
    end

  end

  def delete_document
    __elasticsearch__.delete_document
  end

  def index_document
    __elasticsearch__.index_document
  end

  def as_indexed_json(options={})
    required_by = self.required_by_name || self.required_by_email || "" # se agrega string vacío o no muestra "Sistema" en el filtro
    return {
      id: self.id,
      request_reason: self.company_vacancy_request_reason_name,
      position: self.company_positions_cenco.try(:company_position).try(:name),
      company: self.company_positions_cenco.try(:company_cenco).try(:company_management).try(:company_record).try(:name),
      business_unit: self.company_positions_cenco.try(:company_cenco).try(:company_management).try(:company_business_unit).try(:name),
      cenco: self.company_positions_cenco.try(:company_cenco).try(:name),
      aasm_state: self.aasm_state,
      required_by: required_by,
      required_by_name: self.required_by_name || "",
      required_by_email: self.required_by_email || "",
      request_date: self.request_date,
      created_by: self.try(:created_by).try(:email) || "", # se agrega string vacío o no muestra "Sistema" en el filtro
      created_at: self.created_at,
      entry_date: self.entry_date,
      vacancies: {
        assigned: self.vacancies.where.not(course: nil).count,
        total: self.vacancies.count
      }
    }
  end
end
