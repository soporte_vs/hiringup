# == Schema Information
#
# Table name: comments
#
#  id          :integer          not null, primary key
#  content     :text
#  user_id     :integer
#  object_id   :integer
#  object_type :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Comment < ActiveRecord::Base
  belongs_to :user
  belongs_to :object, polymorphic: true

  validates :user, presence: true
  validate :user_personal_information_exists?, if: "user.present?"
  validates :content, presence: true
  validates :object_type, presence: true
  validates :object_id, presence: true

  def user_personal_information_exists?
    if self.user.personal_information.nil?
      self.errors.add(:user,:no_personal_information)
      return false
    else
      return true
    end
  end
end
