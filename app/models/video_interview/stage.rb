# == Schema Information
#
# Table name: stage_bases
#
#  id            :integer          not null, primary key
#  enrollment_id :integer
#  type          :string
#  created_at    :datetime
#  updated_at    :datetime
#  step          :integer
#  name          :string
#

class VideoInterview::Stage <  Stage::Base
  validates :type, uniqueness: { scope: [:enrollment_id]}
  after_create :load_video_interview_questions

  ROLES_CLASS = [
    {name: :admin, description: I18n.t("video_interview.stage.roles.class.admin")}
  ]

  def roles_instance
    [
      {name: :admin, description: I18n.t("video_interview.stage.roles.instance.admin")},
    ]
  end

  def load_video_interview_questions
    questions = self.enrollment.course.video_interview_questions
    user = self.enrollment.applicant.user
    if not questions.empty?
      questions.each do |question|
        user.video_interview_answers.create(:question => question.content, :time_limit => 60, :enrollment_id => self.enrollment_id, :user_id => user.id)
      end
      VideoInterview::AnswerMailer.delay.link_to_interview(user)
    end
  end

  def is_approved?
    answers = self.enrollment.video_interview_answers
    return nil if answers.empty?
    answers.each do |answer|
      return nil if answer.video_state == :not_uploaded
    end
    return true
  end
end
