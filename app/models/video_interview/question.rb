# == Schema Information
#
# Table name: video_interview_questions
#
#  id         :integer          not null, primary key
#  content    :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class VideoInterview::Question < ActiveRecord::Base
  validates :content, presence: true
  has_and_belongs_to_many :courses
end
