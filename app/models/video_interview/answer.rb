# == Schema Information
#
# Table name: video_interview_answers
#
#  id            :integer          not null, primary key
#  time_limit    :integer
#  question      :string
#  enrollment_id :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  user_id       :integer
#

class VideoInterview::Answer < ActiveRecord::Base
  belongs_to :user
  belongs_to :enrollment, class_name: Enrollment::Base
  #f4_cameraized
end
