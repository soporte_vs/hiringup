# == Schema Information
#
# Table name: stage_bases
#
#  id            :integer          not null, primary key
#  enrollment_id :integer
#  type          :string
#  created_at    :datetime
#  updated_at    :datetime
#  step          :integer
#  name          :string
#

class Engagement::Stage <  Stage::Base
  validates :type, uniqueness: { scope: [:enrollment_id]}

  has_one :agreement, class_name: Engagement::Agreement, foreign_key: :engagement_stage_id, dependent: :destroy

  ROLES_CLASS = [
    {name: :admin, description: I18n.t("engagement.stage.roles.class.admin")}
  ]

  def roles_instance
    [
      {name: :admin, description: I18n.t("engagement.stage.roles.instance.admin")},
    ]
  end

  def disposable?
    if self.agreement.nil?
      return true
    else
      error = I18n.t('activerecord.errors.models.engagement/stage.attributes.enrollment.undisposable')
      self.enrollment.errors.add(:discarding, error)
      return false
    end
  end

  def is_hired?
    self.agreement.present?
  end

  def can_not_apply?
    return false if is_hired?
    super
  end

  def is_approved?
    agreement.present? ? true : nil
  end
end
