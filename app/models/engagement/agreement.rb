# == Schema Information
#
# Table name: engagement_agreements
#
#  id                       :integer          not null, primary key
#  revenue                  :string
#  observations             :text
#  engagement_stage_id      :integer
#  created_at               :datetime
#  updated_at               :datetime
#  vacancy_id               :integer
#  company_contract_type_id :integer
#

class Engagement::Agreement < ActiveRecord::Base
  after_commit :touch_enrollment, on: [:create, :update]
  belongs_to :engagement_stage, class_name: Engagement::Stage
  belongs_to :vacancy

  belongs_to :company_contract_type,
             class_name: Company::ContractType,
             foreign_key: :company_contract_type_id

  has_many :documents,
           class_name: Stage::Document,
           as: :resource,
           dependent: :destroy,
           inverse_of: :resource

  validates :engagement_stage, presence: true
  validates :company_contract_type, presence: true
  validates :revenue, presence: true
  validates :vacancy, presence: true
  validates :vacancy, uniqueness: true
  validates :revenue, length: { maximum: 8 }

  validate :duplicate_agreement, on: [:create]
  validate :vacancy_course_valid?

  after_save :assign_vacancy_closed_at
  after_create :assign_company_employee

  delegate :name, to: :company_contract_type, allow_nil: true, prefix: true

  def touch_enrollment
    self.engagement_stage.touch_enrollment
  end

  private
    def assign_vacancy_closed_at
      vacancy.update(closed_at: self.created_at)
    end

    def assign_company_employee
      user = self.engagement_stage.enrollment.applicant.user
      if user.company_employee.present?
        company_employee = user.company_employee
      else
        company_employee = Company::Employee.create(user: user)
      end

      company_positions_cenco = self.vacancy.company_positions_cenco

      company_employee_position_cenco = Company::EmployeePositionsCenco.create(company_positions_cenco: company_positions_cenco, company_employee: company_employee)

      company_employee.update(current_employee_position_cenco: company_employee_position_cenco)

    end

    def duplicate_agreement
      if Engagement::Agreement.where(engagement_stage: self.engagement_stage).count != 0
        self.errors.add(:duplicate_agreement, I18n.t('activerecord.errors.models.engagement.agreement.duplicate'))
      end
    end

    def vacancy_course_valid?
      if vacancy and vacancy.course != engagement_stage.enrollment.course
        message = I18n.t('activerecord.errors.models.engagement.agreement.vacancy_course_invalid')
        self.errors.add(:vacancy_course_invalid, message)
      end
    end

end
