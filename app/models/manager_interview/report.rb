class ManagerInterview::Report < ActiveRecord::Base
  after_commit :touch_enrollment, on: [:create, :update]
  belongs_to :manager_interview_stage,
             class_name: ManagerInterview::Stage,
             foreign_key: :manager_interview_stage_id

  has_many :documents,
            class_name: Stage::Document,
            as: :resource,
            dependent: :destroy

  validates :observations, presence: true
  validates :manager_interview_stage, presence: true

  def touch_enrollment
    self.manager_interview_stage.touch_enrollment
  end
end
