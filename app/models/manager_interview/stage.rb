class ManagerInterview::Stage <  Stage::Base
  has_one :manager_interview_report,
          class_name: ManagerInterview::Report,
          foreign_key: :manager_interview_stage_id,
          dependent: :destroy

  ROLES_CLASS = [
    {name: :admin, description: I18n.t("manager_interview.stage.roles.class.admin")}
  ]

  def roles_instance
    [
      {name: :admin, description: I18n.t("manager_interview.stage.roles.instance.admin")},
    ]
  end

  def can_not_apply?
    return false if self.manager_interview_report.present?
    super
  end
end
