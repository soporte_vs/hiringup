# == Schema Information
#
# Table name: tag_base_tags
#
#  id         :integer          not null, primary key
#  name       :string
#  type       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Tag::BaseTag < ActiveRecord::Base
  validates :name, presence: true, uniqueness: true
  validates :type, presence: true
  
  has_many :used_tags, foreign_key: :tag_id
end
