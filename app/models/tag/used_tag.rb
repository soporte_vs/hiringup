# == Schema Information
#
# Table name: tag_used_tags
#
#  id            :integer          not null, primary key
#  tag_id        :integer
#  taggable_id   :integer
#  taggable_type :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class Tag::UsedTag < ActiveRecord::Base
  belongs_to :tag, class_name: Tag::BaseTag
  belongs_to :taggable, polymorphic: true

  validates :tag, presence: true
  validates :taggable_type, presence: true
  
  validates_uniqueness_of :taggable_id, scope: [:taggable_type ,:tag_id]
end
