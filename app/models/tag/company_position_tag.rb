# == Schema Information
#
# Table name: tag_base_tags
#
#  id         :integer          not null, primary key
#  name       :string
#  type       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Tag::CompanyPositionTag < Tag::BaseTag
  has_many :company_positions, through: :used_tags, source: :taggable, source_type: Company::Position
end
