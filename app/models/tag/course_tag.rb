# == Schema Information
#
# Table name: tag_base_tags
#
#  id         :integer          not null, primary key
#  name       :string
#  type       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Tag::CourseTag < Tag::BaseTag
  has_many :courses, through: :used_tags, source: :taggable, source_type: Course
end
