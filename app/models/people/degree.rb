# coding: utf-8
# == Schema Information
#
# Table name: people_degrees
#
#  id                          :integer          not null, primary key
#  career_id                   :integer
#  professional_information_id :integer
#  condition                   :string
#  institution_id               :integer
#  culmination_date            :date
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#  name                        :string
#

class People::Degree < ActiveRecord::Base
  CONDITIONS = ['EGRESADO', 'EN CURSO', 'TITULADO', 'INCOMPLETA']
  TYPES=[['People::PrimarySchoolDegree','Educación Primaria'],
         ['People::HighSchoolDegree','Enseñanza Media'],
         ['People::TechnicalDegree','Liceo Técnico'],
         ['People::TechnicalProfessionalDegree', 'Técnico Profesional'],
         ['People::ProfessionalDegree','Universitaria'],
         ['People::PostgraduateDegree', 'Postgrado'],
         ['People::MagisterDegree','Magister'],
         ['People::PhdDegree','Doctorado']]

  belongs_to :professional_information, class_name: People::ProfessionalInformation
  belongs_to :career, class_name: Education::Career
  belongs_to :institution, class_name: Education::Institution

  validates :career, presence: true
  validates :institution, presence: true
  validates :name, presence: true
  validates :condition,
            presence: true,
            inclusion: {
              in: CONDITIONS,
              message: "%{value} #{I18n.t('activerecord.errors.models.people/degree.attributes.condition.invalid')}"
            }

  validate :culmination_date_valid?
  validates :professional_information_id, uniqueness: { scope: [:institution_id, :career_id, :type] }
  delegate :name, to: :institution, prefix: true, allow_nil: true
  delegate :type, to: :institution, prefix: true, allow_nil: true

  def serializable_hash options=nil
    super.merge type: type
  end

  def self.translated_degree_type degree_class
    TYPES.find{|type| type.first == degree_class}.try(:last) || ''
  end

  private

    def culmination_date_valid?
      if condition == 'EGRESADO' && culmination_date.nil?
        errors.add(:culmination_date, I18n.t('activerecord.errors.models.people/degree.attributes.culmination_date.blank'))
        false
      else
        true
      end
    end
end
