class People::PersonalReferral < ActiveRecord::Base
  belongs_to :personal_information, class_name: People::PersonalInformation, inverse_of: :personal_referrals

  validates :full_name, presence: true
  validates :email, presence: true, email: true, uniqueness: { scope: :personal_information_id }
  validates :phone, presence: true

  validates :personal_information, presence: true
end
