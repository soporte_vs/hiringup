class People::SoftwareSkill < ActiveRecord::Base
  belongs_to :education_software, class_name: Education::Software
  belongs_to :professional_information, class_name: People::ProfessionalInformation

  LEVELS = ['Básico', 'Intermedio', 'Avanzado']

  validates :level,
            presence: true,
            inclusion: {
              in: LEVELS,
              message: "%{value} #{I18n.t('activerecord.errors.models.people/software_skill.attributes.level.invalid')}"
            }

  validates :education_software_id, presence: true, uniqueness: { scope: :professional_information_id }
  validates :professional_information_id, presence: true
end
