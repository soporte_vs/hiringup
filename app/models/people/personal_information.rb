# coding: utf-8
# == Schema Information
#
# Table name: people_personal_informations
#
#  id                        :integer          not null, primary key
#  first_name                :string
#  last_name                 :string
#  birth_date                :date
#  landline                  :string
#  cellphone                 :string
#  sex                       :string
#  user_id                   :integer
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  territory_city_id         :integer
#  avatar                    :string
#  address                   :string
#  nationality_id            :integer
#  company_marital_status_id :integer
#

class People::PersonalInformation < ActiveRecord::Base
  # Reindexing applicant on ElasticSearch
  after_commit on: :update do
    if self.user.try(:applicant).present?
      self.user.applicant.save
    end
  end

  mount_uploader :avatar, AvatarUploader

  belongs_to :user, inverse_of: :personal_information
  belongs_to :city, class_name: Territory::City, foreign_key: :territory_city_id
  belongs_to :nationality, class_name: Territory::Country, foreign_key: :nationality_id
  belongs_to :company_marital_status, class_name: Company::MaritalStatus
  belongs_to :company_recruitment_source, class_name: Company::RecruitmentSource, foreign_key: :recruitment_source_id
  belongs_to :identification_document_type,
             class_name: People::IdentificationDocumentType,
             foreign_key: :identification_document_type_id

  has_many :personal_referrals, class_name: People::PersonalReferral, dependent: :destroy, inverse_of: :personal_information

  SEX_OPTIONS = [ "Male", "Female" ]

  validates_inclusion_of :sex, :in => SEX_OPTIONS, :allow_nil => true
  validates :user, presence: true

  validates :first_name, presence: true, length: { minimum: 3, maximum: 25 }
  validates :last_name, presence: true, length: { minimum: 3, maximum: 25 }
  validates :landline, format: { with: /\A[\d\s\+]{5,20}\z/ }, length: { minimum: 5, maximum: 20 }, if: "landline.present?"
  validates :cellphone, format: { with: /\A[\d\s\+]{5,20}\z/ }, length: { minimum: 5, maximum: 20 }, if: "cellphone.present?"

  validates :identification_document_type, presence: true
  validates :identification_document_number, presence: true
  validate :validate_identification_document_number, if: "self.identification_document_type.present? && identification_document_number.present?"
  validate :validate_identification_document_number_uniqueness, if: "self.identification_document_type.present? && self.identification_document_type.validate_uniqueness == true"

  validate :birth_date_more_than_18
  validate :rut_validator, if: "has_rut?"
  delegate :name, to: :nationality, prefix: true, allow_nil: true
  accepts_nested_attributes_for :personal_referrals, allow_destroy: true

  validates_inclusion_of :availability_replacements,:in => [true, false], if: "availability_replacements.present?"
  validates_inclusion_of :availability_work_other_residence,:in => [true, false], if: "availability_work_other_residence.present?"

  VALIDATE_PRESENCE_ATTRIBUTES = [
    "landline",
    "cellphone",
    "birth_date",
    "sex",
    "nationality_id",
    "city",
    "address",
    "company_marital_status_id",
    "study_type"
  ]

  [
    :identification_document_number,
    :first_name,
    :last_name,
    :birth_date,
    :landline,
    :cellphone,
    :sex,
    :address
  ].each do |attribute|
    define_method("#{attribute}_html") do
      if !self.send(attribute).blank?
        self.send(attribute)
      else
        I18n.t('activerecord.models.people.field_html_default')
      end
    end
  end

  def active_custom_presence_validators!
    VALIDATE_PRESENCE_ATTRIBUTES.each do |attribute|
      self.singleton_class.validates_presence_of attribute
    end
  end

  def deactive_custom_presence_validators!
    VALIDATE_PRESENCE_ATTRIBUTES.each do |attribute|
      self.singleton_class._validators[attribute.to_sym] = self._validators[attribute.to_sym].reject{ |validator|
        validator.class == ActiveRecord::Validations::PresenceValidator
      }
    end
  end

  def identification_document_number=(value)
    write_attribute(:identification_document_number, value.try(:strip))
  end

  def sex_word
    case self.sex
    when "Male"
      return I18n.t("activerecord.models.people/personal_information.genders.male")
    when "Female"
      return I18n.t("activerecord.models.people/personal_information.genders.female")
    else
      return I18n.t("activerecord.models.people/personal_information.genders.undefined")
    end
  end

  def self.gender_options_for_select
    SEX_OPTIONS.map{|opt| [ I18n.t("activerecord.models.people/personal_information.genders.#{opt.downcase}"), opt]}
  end

  def has_rut?
    self.identification_document_type.present? && self.identification_document_type.codename.downcase == 'rut' && self.identification_document_type.country.present? && self.identification_document_type.country.name.downcase == "chile" && self.identification_document_number.present?
  end

  def get_rut
    if has_rut?
      self.identification_document_number
    end
  end

  private
    def rut_validator
      rut = get_rut.strip.gsub('K', 'k')
      _rut = rut.split('-').first.to_i
      dv = rut.split('-').last
      v = 1
      sum = 0
      for i in (2..9)
        i == 8 ? v = 2 : v += 1
        sum += v * (_rut % 10)
        _rut /= 10
      end
      sum = 11 - sum % 11
      valid_rut = false
      if sum == 11
        valid_rut = '0' == dv
      elsif sum == 10
        valid_rut = 'k' == dv
      else
        valid_rut = sum.to_s == dv
      end
      errors.add(:identification_document_number, 'Rut inválido, utilice el siguiente formato: 11111111-1') unless valid_rut
    end

    def birth_date_more_than_18
      if self.birth_date.present?
        if self.birth_date > (Date.today - 18.years)
          self.errors.add(:birth_date, :not_adult)
          return false
        end
      end
    end

    def validate_identification_document_number
      if identification_document_type.validation_regex.present?
        if identification_document_number.at(Regexp.new(identification_document_type.validation_regex)).nil?
          errors.add(:identification_document_number, :not_valid)
          return false
        end
      end
    end

    def validate_identification_document_number_uniqueness
      # Se busca otra persona que posiblemente esté usando
      # este documento de identidad distinto a si mismo
      people_personal_information = People::PersonalInformation.where(
        identification_document_type: self.identification_document_type,
        identification_document_number: self.identification_document_number
      ).where.not(id: self.id).first

      # si está presente y no es este mismo agrega un error
      if people_personal_information.present?
        errors.add(:identification_document_number, :taken)
        return false
      end
    end

    def rut_validator
      self.identification_document_number = identification_document_number.to_s.strip.gsub('K', 'k')
      _rut = identification_document_number.split('-').first.to_i
      dv = identification_document_number.split('-').last
      v = 1
      sum = 0
      for i in (2..9)
        i == 8 ? v = 2 : v += 1
        sum += v * (_rut % 10)
        _rut /= 10
      end
      sum = 11 - sum % 11
      valid_rut = false
      if sum == 11
        valid_rut = '0' == dv
      elsif sum == 10
        valid_rut = 'k' == dv
      else
        valid_rut = sum.to_s == dv
      end
      errors.add(:identification_document_number, 'Rut inválido, utilice el siguiente formato: 11111111-1') unless valid_rut
    end

end
