class People::IdentificationDocumentType < ActiveRecord::Base
  belongs_to :country,
             class_name: 'Territory::Country',
             foreign_key: :country_id

  has_many :people_personal_informations ,
           class_name: 'People::PersonalInformation',
           foreign_key: :identification_document_type_id,
           dependent: :nullify # si se elimina, en People::PersonalInformation el campo debe quedar nil

  validates :country, presence: true
  validates :name, presence: true
  validates :codename, presence: true
  validates :name, uniqueness: { scope: [:country_id] }
  validates :codename, uniqueness: { scope: [:country_id] }

  before_validation :set_codename

  def self.countries
    country_ids = People::IdentificationDocumentType.pluck(:country_id)
    Territory::Country.where(id: country_ids)
  end

  private
  def set_codename
    self[:codename] = name.squish.parameterize if name.present?
  end
end
