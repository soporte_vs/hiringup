# == Schema Information
#
# Table name: people_language_skills
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class People::LanguageSkill < ActiveRecord::Base
  LEVELS = ['Básico', 'Intermedio', 'Avanzado']

  belongs_to :education_language, class_name: Education::Language
  belongs_to :professional_information, class_name: People::ProfessionalInformation

  validates :level,
            presence: true,
            inclusion: {
              in: LEVELS,
              message: "%{value} #{I18n.t('activerecord.errors.models.people/language_skill.attributes.level.invalid')}"
            }
  validates :education_language, presence: true, uniqueness: {scope: :professional_information_id}
  validates :professional_information, presence: true

end
