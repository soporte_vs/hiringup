# == Schema Information
#
# Table name: people_professional_informations
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class People::ProfessionalInformation < ActiveRecord::Base
  belongs_to :user, inverse_of: :professional_information
  has_many :degrees, dependent: :destroy
  has_many :laboral_experiences, dependent: :destroy
  has_many :language_skills, dependent: :destroy
  has_many :software_skills, dependent: :destroy

  accepts_nested_attributes_for :degrees, allow_destroy: true, reject_if: :reject_degree?
  accepts_nested_attributes_for :laboral_experiences, allow_destroy: true, reject_if: :reject_laboral_experience?
  accepts_nested_attributes_for :language_skills, allow_destroy: true, reject_if: :reject_language_skill?
  accepts_nested_attributes_for :software_skills, allow_destroy: true, reject_if: :all_blank

  validates :user, presence: true

  # Ignore new degrees with all attributes empty and id not present
  def reject_degree?(attributed)
    !attributed['id'].present? and attributed['institution_id'].blank? and attributed['name'].blank? and attributed['career_id'].blank? and attributed['condition'].blank? and attributed['culmination_date'].blank?
  end

  def reject_laboral_experience?(attributed)
    !attributed['id'].present? and attributed['company'].blank? and attributed['description'].blank? and attributed['position'].blank? and attributed['since_date'].blank? and attributed['until_date'].blank?
  end

  def reject_language_skill?(attributed)
    !attributed['id'].present? and attributed['education_language_id'].blank? and attributed['level'].blank?
  end
end
