# == Schema Information
#
# Table name: people_laboral_experiences
#
#  id                          :integer          not null, primary key
#  position                    :string
#  company                     :string
#  since_date                  :date
#  until_date                  :date
#  professional_information_id :integer
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#  description                 :text
#

class People::LaboralExperience < ActiveRecord::Base
  belongs_to :professional_information, class_name: People::ProfessionalInformation, foreign_key: :professional_information_id

  validates :company, presence: true
  validates :position, presence: true
  validates :since_date, presence: true
end
