# == Schema Information
#
# Table name: roles
#
#  id            :integer          not null, primary key
#  name          :string
#  resource_id   :integer
#  resource_type :string
#  created_at    :datetime
#  updated_at    :datetime
#

class Role < ActiveRecord::Base
  has_and_belongs_to_many :users, :join_table => :users_roles
  belongs_to :resource, :polymorphic => true

  before_validation :check_if_known

  scopify

  GLOBAL_ROLES = [
    {name: :admin, description: 'Todos los permisos del sistema'},
    {name: :admin_reports, description: 'Tiene permiso para acceder a reportes'}
  ]

  def to_s
    resource = self.resource_type.try(:constantize)
    if self.resource_id != nil
      resource = resource.find(self.resource_id)
      return resource.roles_instance.select { |global_role| global_role[:name] == self.name.to_sym }.first[:description] + " " +resource.to_s
    elsif resource != nil
      return resource::ROLES_CLASS.select { |global_role| global_role[:name] == self.name.to_sym }.first[:description]
    else
      return GLOBAL_ROLES.select { |global_role| global_role[:name] == self.name.to_sym }.first[:description]
    end
  end

  def check_if_known
    if (self.resource_id and self.resource_type.try(:constantize)) != nil
      resource = self.resource_type.try(:constantize).find(self.resource_id)
      if !resource.roles_instance.map{ |r| r[:name] }.include?(self.name.to_sym)
        return false
      else
        return true
      end
    elsif self.resource_type.try(:constantize) != nil
      if !self.resource_type.try(:constantize)::ROLES_CLASS.map{ |r| r[:name] }.include?(self.name.to_sym)
        return false
      else
        return true
      end
    else
      if !Role::GLOBAL_ROLES.map{ |r| r[:name] }.include?(self.name.to_sym)
        return false
      else
        return true
      end
    end
  end
end
