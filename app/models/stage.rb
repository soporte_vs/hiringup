module Stage
  def self.table_name_prefix
    'stage_'
  end

  def self.to_s
    "Etapa"
  end
end
