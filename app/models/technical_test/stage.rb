class TechnicalTest::Stage <  Stage::Base
  has_one :technical_test_report,
          class_name: TechnicalTest::Report,
          foreign_key: :technical_test_stage_id,
          dependent: :destroy

  ROLES_CLASS = [
    {name: :admin, description: I18n.t("technical_test.stage.roles.class.admin")}
  ]

  def roles_instance
    [
      {name: :admin, description: I18n.t("technical_test.stage.roles.instance.admin")},
    ]
  end

  def can_not_apply?
    return false if self.technical_test_report.present?
    super
  end
end
