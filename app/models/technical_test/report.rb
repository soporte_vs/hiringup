class TechnicalTest::Report < ActiveRecord::Base
  belongs_to :technical_test_stage,
             class_name: TechnicalTest::Stage,
             foreign_key: :technical_test_stage_id

  has_many :documents,
    class_name: Stage::Document,
    as: :resource,
    dependent: :destroy

  validates :observations, presence: true
  validates :notified_to, email: true
  validates :technical_test_stage, presence: true
end
