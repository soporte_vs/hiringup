# == Schema Information
#
# Table name: territory_countries
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Territory::Country < ActiveRecord::Base
  has_paper_trail
  validates :name, presence: true, uniqueness: true

  has_many :territory_states, class_name: Territory::State, foreign_key: :territory_country_id, dependent: :destroy
end
