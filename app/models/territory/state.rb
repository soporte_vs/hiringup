# == Schema Information
#
# Table name: territory_states
#
#  id                   :integer          not null, primary key
#  name                 :string
#  territory_country_id :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#

class Territory::State < ActiveRecord::Base
  has_paper_trail
  belongs_to :territory_country, class_name: Territory::Country

  has_many :territory_cities, class_name: Territory::City, foreign_key: :territory_state_id, dependent: :destroy

  validates :name, presence: true, uniqueness: { scope: :territory_country_id }
  validates :territory_country, presence: true
end
