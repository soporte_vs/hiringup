# == Schema Information
#
# Table name: territory_cities
#
#  id                 :integer          not null, primary key
#  name               :string
#  territory_state_id :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

class Territory::City < ActiveRecord::Base
  has_paper_trail
  belongs_to :territory_state, class_name: Territory::State, foreign_key: :territory_state_id

  validates :name, presence: true, uniqueness: { scope: :territory_state_id }
  validates :territory_state, presence: true
end
