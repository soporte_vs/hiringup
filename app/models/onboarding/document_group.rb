class Onboarding::DocumentGroup < ActiveRecord::Base
  belongs_to :onboarding_stage,
             class_name: Onboarding::Stage,
             foreign_key: :onboarding_stage_id

  belongs_to :document_group,
             class_name: Document::Group,
             foreign_key: :document_group_id

  validates :document_group, presence: true
  validates :onboarding_stage, presence: true
end
