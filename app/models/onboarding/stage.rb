# == Schema Information
#
# Table name: stage_bases
#
#  id            :integer          not null, primary key
#  enrollment_id :integer
#  type          :string
#  created_at    :datetime
#  updated_at    :datetime
#  step          :integer
#  name          :string
#

class Onboarding::Stage < Stage::Base
  after_create :create_onboarding_document_group
  after_create :send_upload_requested_documents_email
  validates :type, uniqueness: { scope: [:enrollment_id]}

  has_one :onboarding_document_group,
          class_name: Onboarding::DocumentGroup,
          foreign_key: :onboarding_stage_id,
          dependent: :destroy


  delegate :document_group, to: :onboarding_document_group, allow_nil: true

  ROLES_CLASS = [
    {name: :admin, description: I18n.t("onboarding.stage.roles.class.admin")}
  ]

  def roles_instance
    [
      {name: :admin, description: I18n.t("onboarding.stage.roles.instance.admin")}
    ]
  end

  def send_upload_requested_documents_email
    offer_letter = self.enrollment.stages.find_by(type: Offer::Stage).try(:offer_letter)

    if  offer_letter && offer_letter.accepted
      Onboarding::StageMailer.delay.send_upload_documentation_request(self)
    end
  end

  private
    def create_onboarding_document_group
      Onboarding::DocumentGroup.create(onboarding_stage: self, document_group_id: (self.args.try(:[], :document_group) || self.enrollment.course.document_group.id))
    end

end
