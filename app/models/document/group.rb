# == Schema Information
#
# Table name: document_groups
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Document::Group < ActiveRecord::Base
  has_paper_trail
  has_many :document_descriptors, :class_name => "Document::Descriptor", :through => :document_descriptor_memberships
  has_many :document_descriptor_memberships, :class_name => "Document::DescriptorMembership", :foreign_key => :document_group_id
  accepts_nested_attributes_for :document_descriptor_memberships, allow_destroy: true
  validates :name, presence: true

  def remaining_document_records(document_records)
    self.document_descriptors.size - document_records.joins([{:document_descriptor => :document_groups}]).where('document_groups.id' => self.id).size
  end
end
