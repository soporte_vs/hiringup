# == Schema Information
#
# Table name: document_descriptors
#
#  id                 :integer          not null, primary key
#  name               :string           not null
#  details            :string
#  allowed_extensions :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  internal           :boolean
#  only_onboarding    :boolean          default(FALSE)
#

class Document::Descriptor < ActiveRecord::Base
  include GenericAttachmentAdapter
  has_many :document_groups, :class_name => "Document::Group", :through => :document_descriptor_memberships
  has_many :document_descriptor_memberships, :class_name => "Document::DescriptorMembership", :foreign_key => :document_descriptor_id

  validates :name, uniqueness: true, presence: true
  validates :details, presence: true

  #CareerWave GenericAttachment
  has_one :attachment, :class_name => "GenericAttachment", as: :resourceable, dependent: :destroy
  accepts_nested_attributes_for :attachment

  before_destroy :destroy_cv

  DEFAULT_EXTENSIONS = %w(pdf doc docx jpg jpeg)



  def to_sym
    self.name.to_sym
  end

  def name=(new_name)
    if self.name.nil? && new_name != nil
      _name = new_name.downcase.strip.gsub(/[_\s]+/, ' ').gsub(/[^a-zA-Z0-9]/, '_')
      super(_name)
    end
  end

  private
    def destroy_cv
      if self.name.eql?("cv")
        errors.add :name, :required_system
        return false
      end
    end
end
