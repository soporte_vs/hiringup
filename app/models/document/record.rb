# == Schema Information
#
# Table name: document_records
#
#  id                     :integer          not null, primary key
#  applicant_id           :integer
#  document_descriptor_id :integer
#  document               :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  enrollment_id          :integer
#

class Document::Record < ActiveRecord::Base
  has_paper_trail
  belongs_to :applicant, :class_name => Applicant::Base
  belongs_to :document_descriptor, :class_name => "Document::Descriptor"
  belongs_to :enrollment, :class_name => "Enrollment::Base"

  mount_uploader :document, RecordUploader
  validates :document, presence:true, file_size: { maximum: 5.megabytes.to_i }
  validates :document_descriptor, presence: true

  after_create :email_callbacks

  def is_internal?
    self.document_descriptor.internal
  end

  private

    def email_callbacks
      if self.applicant.present?
        enrollment = self.applicant.enrollments.where(type: Enrollment::Internal).last
        if enrollment.present? && self.document_descriptor.to_sym == :internal_postulation_template
          Courses::InternalMailer.delay.notify_internal_postulation(enrollment)
        end
      end
    end

end
