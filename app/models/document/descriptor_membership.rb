# == Schema Information
#
# Table name: document_descriptor_memberships
#
#  id                     :integer          not null, primary key
#  document_descriptor_id :integer
#  document_group_id      :integer
#  document_required      :boolean
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#

class Document::DescriptorMembership < ActiveRecord::Base
  belongs_to :document_descriptor, :class_name => "Document::Descriptor"
  belongs_to :document_group, :class_name => "Document::Group"
end
