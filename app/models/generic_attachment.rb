class GenericAttachment < ActiveRecord::Base
  mount_uploader :attachment, AttachmentUploader
  belongs_to :resourceable, polymorphic: true

  before_save :rename_file

  private
  def rename_file
    self.name = self.attachment.model.attachment.to_s.split("/").last
  end
end
