class CourseCareer < ActiveRecord::Base
  belongs_to :course
  belongs_to :education_career, class_name: Education::Career
  validates_uniqueness_of :education_career_id, scope: :course_id
end
