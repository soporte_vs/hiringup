class Test::Stage <  Stage::Base
  has_one :test_report,
          class_name: Test::Report,
          foreign_key: :test_stage_id,
          dependent: :destroy

  ROLES_CLASS = [
    {name: :admin, description: I18n.t("test.stage.roles.class.admin")}
  ]

  def roles_instance
    [
      {name: :admin, description: I18n.t("test.stage.roles.instance.admin")},
    ]
  end

  def can_not_apply?
    return false if self.test_report.present?
    super
  end

end
