class Test::Report < ActiveRecord::Base
  after_commit :touch_enrollment, on: [:create, :update]
  belongs_to :test_stage,
             class_name: Test::Stage,
             foreign_key: :test_stage_id

  has_many :documents,
    class_name: Stage::Document,
    as: :resource,
    dependent: :destroy

  validates :observations, presence: true
  validates :test_stage, presence: true

  def touch_enrollment
    self.test_stage.touch_enrollment
  end
end
