# == Schema Information
#
# Table name: trigger_bases
#
#  id                :integer          not null, primary key
#  type              :string
#  resourceable_id   :integer
#  resourceable_type :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class Trigger::Base < ActiveRecord::Base
  belongs_to :resourceable, polymorphic: true

  validates :resourceable, presence: true
  validates :type, presence: true
end
