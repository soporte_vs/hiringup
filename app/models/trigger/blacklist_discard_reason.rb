# == Schema Information
#
# Table name: trigger_bases
#
#  id                :integer          not null, primary key
#  type              :string
#  resourceable_id   :integer
#  resourceable_type :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class Trigger::BlacklistDiscardReason < Trigger::Base
  validate :check_resourceable_type
  belongs_to :discard_reason, class_name: Enrollment::DiscardReason, foreign_key: :resourceable_id

  private

  def check_resourceable_type
    if self.resourceable.instance_of? Enrollment::DiscardReason
      return true
    else
      self.errors.add(:resourceable, :resourceable_type_check)
      return false
    end
  end

end
