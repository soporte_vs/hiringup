# == Schema Information
#
# Table name: trigger_bases
#
#  id                :integer          not null, primary key
#  type              :string
#  resourceable_id   :integer
#  resourceable_type :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class Trigger::CloseCourseDiscardReason < Trigger::Base

  validates :resourceable_type, uniqueness: true
  validate :check_resourceable_type

  def self.new(attributes = nil, options = {})
    last = self.last
    if last.nil?
      super(attributes, options)
    else
      last.attributes = attributes unless attributes.nil?
      last
    end
  end

  def self.create(attributes = nil, options = {}, &block)
    new = self.new(attributes, options)
    new.save
    new
  end

  private

  def check_resourceable_type
    if self.resourceable.instance_of? Enrollment::DiscardReason
      return true
    else
      self.errors.add(:resourceable, :resourceable_type_check)
      return false
    end
  end

end
