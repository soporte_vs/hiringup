module Test
  def self.table_name_prefix
    'test_'
  end

  def self.to_s
    "Test"
  end
end
