module FinalInterview
  def self.table_name_prefix
    'final_interview_'
  end

  def self.to_s
    "Entrevista Final"
  end
end
