class FinalInterview::Stage <  Stage::Base
  has_one :final_interview_report,
          class_name: FinalInterview::Report,
          foreign_key: :final_interview_stage_id,
          dependent: :destroy

  ROLES_CLASS = [
    {name: :admin, description: I18n.t("final_interview.stage.roles.class.admin")}
  ]

  def roles_instance
    [
      {name: :admin, description: I18n.t("final_interview.stage.roles.instance.admin")},
    ]
  end

  def can_not_apply?
    return false if self.final_interview_report.present?
    super
  end

end
