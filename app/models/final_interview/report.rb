class FinalInterview::Report < ActiveRecord::Base
  belongs_to :final_interview_stage,
             class_name: FinalInterview::Stage,
             foreign_key: :final_interview_stage_id

  has_many :documents,
           class_name: Stage::Document,
           as: :resource,
           dependent: :destroy

  validates :observations, presence: true
  validates :notified_to, email: true
  validates :final_interview_stage, presence: true
end
