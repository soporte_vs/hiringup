class Trabajando::EducationState < ActiveRecord::Base
  has_many :publications,
           class: Trabajando::Publication,
           foreign_key: :education_state_id

  belongs_to :hupe_study_level, class: Education::StudyLevel
  validates :name, presence: true, uniqueness: true
  validates :trabajando_id, presence: true, uniqueness: true
end
