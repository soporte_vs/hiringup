class Trabajando::Country < ActiveRecord::Base
  belongs_to :country, class: Territory::Country

  validates :name, presence: true, uniqueness: true
  validates :trabajando_id, presence: true
end
