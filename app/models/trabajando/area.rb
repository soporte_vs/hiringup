class Trabajando::Area < ActiveRecord::Base
  has_many :publications,
           class: Trabajando::Publication,
           foreign_key: :area_id
  belongs_to :hupe_area, class: Company::Area

  validates :name, presence: true, uniqueness: true
  validates :trabajando_id, presence: true, uniqueness: true
end
