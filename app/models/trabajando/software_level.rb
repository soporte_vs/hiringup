class Trabajando::SoftwareLevel < ActiveRecord::Base
  has_many :publications,
           class: Trabajando::Publication,
           foreign_key: :software_level_id

  belongs_to :hupe_software_level, class: Applicant::SoftwareLevel
  validates :name, presence: true, uniqueness: true
  validates :trabajando_id, presence: true, uniqueness: true
end
