class Trabajando::City < ActiveRecord::Base
  belongs_to :hupe_city, class_name: Territory::City

  validates :name, presence: true, uniqueness: true
  validates :trabajando_id, presence: true
end
