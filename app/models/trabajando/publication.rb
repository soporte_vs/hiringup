class Trabajando::Publication < ActiveRecord::Base
  include AASM
  include PublicationDeactivatable

  aasm do
    state :opened, initial: true
    state :closed

    event :close do
      transitions from: :opened, to: :closed
    end

    event :open do
      transitions from: :closed, to: :opened, guards: [:course_opened?]
    end
  end

  belongs_to :course
  belongs_to :area, class_name: Trabajando::Area
  belongs_to :job_type, class_name: Trabajando::JobType
  belongs_to :company_activity, class_name: Trabajando::CompanyActivity
  belongs_to :work_day, class_name: Trabajando::WorkDay
  belongs_to :applicant_profile, class_name: Trabajando::ApplicantProfile
  belongs_to :education_level, class_name: Trabajando::EducationLevel
  belongs_to :education_state, class_name: Trabajando::EducationState
  belongs_to :software_level, class_name: Trabajando::SoftwareLevel
  belongs_to :region, class_name: Trabajando::State
  belongs_to :city, class_name: Trabajando::City
  has_many :postulations,
            class_name: Trabajando::Postulation,
            foreign_key: :trabajando_publication_id,
            dependent: :destroy

  validates :course, presence: true
  validates :title, presence: true
  validates :description, presence: true
  validates :number_vacancies, presence: true
  validates :minimum_requirements, presence: true
  validates :area, presence: true
  validates :salary, presence: true
  validates :job_type, presence: true
  validates :company_activity, presence: true
  validates :work_day, presence: true
  validates :workplace, length: { maximum: 50 }
  validates :applicant_profile, presence: true
  validates :education_level, presence: true
  validates :education_state, presence: true
  validates :software_level, presence: true
  validates :region, presence: true
  validates :city, presence: true
  validates :desactivate_at, presence: true

  delegate :name, to: :area, allow_nil: true, prefix: true
  delegate :trabajando_id, to: :area, allow_nil: true, prefix: true
  delegate :trabajando_id, to: :job_type, allow_nil: true, prefix: true
  delegate :trabajando_id, to: :company_activity, allow_nil: true, prefix: true
  delegate :trabajando_id, to: :work_day, allow_nil: true, prefix: true
  delegate :trabajando_id, to: :software_level, allow_nil: true, prefix: true
  delegate :trabajando_id, to: :applicant_profile, allow_nil: true, prefix: true
  delegate :trabajando_id, to: :education_level, allow_nil: true, prefix: true
  delegate :trabajando_id, to: :education_state, allow_nil: true, prefix: true


  def publication
    return trabajando_publication
  end

  def deactivate
    return false unless self.opened?
    json_response = JSON.parse(call_deactivate_service.body)
    success = json_response['data'] && json_response['data']['success']
    return success && self.close!
  end

  def activate
    return false unless self.closed?
    json_response = JSON.parse(call_activate_service.body)
    success = json_response['data'] && json_response['data']['success']
    return success && self.open!
  end

  def questions
    [q1, q2, q3, q4, q5].compact.reject(&:blank?)
  end

  def publish
    return nil unless self.valid?
    if self.trabajando_id.present?
      service_url = TRABAJANDO_UPDATE_JOB_URL
      service_method = 'put'
    else
      service_url = TRABAJANDO_PUBLISH_JOB_URL
      service_method = 'post'
    end
    service_params = {
      body: json_trabajando_schema.to_json,
      headers: {'Content-Type' => 'application/json'}
    }
    HTTParty.send(service_method, service_url, service_params)
  end

  def save_and_publish
    response = self.publish
    if response["status"] == 'NOK'
      puts "Error: respuesta servicio trabajando"
      puts response.inspect
      return false
    end
    response_json = JSON.parse(response.body)

    trabajando_id = response_json['data'].nil? ? nil : response_json['data']['id']
    self.trabajando_id = trabajando_id
    if self.trabajando_id.nil?
      self.errors.add(:trabajando_id, 'Problema en la comunicación con el servicio de Trabajando')
      return false
    else
      return save
    end
  end

  def get_remote_url
    "https://www.trabajando.cl/empleos/ofertas/#{self.trabajando_id}"
  end

  def json_trabajando_schema
    {
      "domainId": TRABAJANDO_DOMAIN_ID,
      "companyId": TRABAJANDO_COMPANY_ID,
      "confidential": (self.is_confidential ? 1 : 0),
      "jobId": self.trabajando_id,
      "description": {
        "jobTitle": self.title,
        "jobDescription": sanitized_description,
        "jobAreaId": self.area_trabajando_id,
        "jobValidity": 30,
        "companyName": $COMPANY[:name],
        "availabilityId": (self.opened? ? 1 : 0),
        "vacancies": self.course.try(:vacancies).try(:count).to_i,
        "contractTime": self.contract_time,
        "jobTypeId": self.job_type_trabajando_id,
        "salary": self.salary,
        "showSalary": (self.show_salary ? 1 : 0),
        "companyActivityId": self.company_activity_trabajando_id,
        "workDayId": self.work_day_trabajando_id,
        "salaryComment": self.salary_comment,
        "workTime": self.work_time
      },
      "questions": {
        "question1": self.q1,
        "question2": self.q2,
        "question3": self.q3,
        "question4": self.q4,
        "question5": self.q5
       },
      "location": {
        "regionId": self.region.try(:trabajando_id),
        "workAbroad": 0,
        "foreignCountryId": 1,
        "countryId": 1,
        "workplace": self.workplace,
        "commune": {
         "id": 0
        },
        "city": {
          "id": self.city.try(:trabajando_id),
          "description": self.city.try(:name)
        }
      },
      "requisites": {
        "minimumRequirements": self.minimum_requirements,
        "workExperience": self.work_experience,
        "genderId": -1,
        "computerSkillId": self.software_level_trabajando_id,
        "ownTransport": (self.own_transport ? 1 : 0),
        "applicantProfile": self.applicant_profile_trabajando_id,
        "workExperienceOperator": 0,
        "educationalLevelId": self.education_level_trabajando_id,
        "academicSituationId": self.education_state_trabajando_id
      },
      "token": TRABAJANDO_TOKEN,
      "client": TRABAJANDO_CLIENT,
      "country": "CL"
    }
  end

  def desactivation_scheduled
    deactivate
  end

  private
    def call_deactivate_service
      call_service_swtch_trabajando_state(TRABAJANDO_DEACTIVATE_JOB_URL)
    end

    def call_activate_service
      call_service_swtch_trabajando_state(TRABAJANDO_ACTIVATE_JOB_URL)
    end

    def call_service_swtch_trabajando_state(url)
      response = HTTParty.post(url,
        :body => {
          "domainId": TRABAJANDO_DOMAIN_ID,
          "jobId": self.trabajando_id,
          "token": TRABAJANDO_TOKEN,
          "client": TRABAJANDO_CLIENT,
          "country": "CL"
        }.to_json,
        :headers => {'Content-Type' => 'application/json'}
      )
    end

    def sanitized_description
      _description = self.description
      # se eliminan los saltos de línea
      _description = _description.gsub("\r\n", "")

      # se 'sanitiza' el texto, y saca todos los tags, y reemplaza
      # los whitespace_elements con un "\n"
      _sanitized = ::Sanitize.fragment(
        _description,
        whitespace_elements: {
          'br' => { before: "\n" },
          'li' => { before: "", after: "\n"},
          'p' => { before: "", after: "\n"},
          'div' => { before: "", after: "\n"},
        }
        # luego de eso, reemplaza los "\n" con un "<br />"
      ).gsub("\n", "<br />")
      return _sanitized
    end

    def course_opened?
      course.opened?
    end

 end
