class Trabajando::Career < ActiveRecord::Base
  belongs_to :hupe_career, class: Education::Career

  validates :name, presence: true
end
