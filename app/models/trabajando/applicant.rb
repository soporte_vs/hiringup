# coding: utf-8
class Trabajando::Applicant < Applicant::Base
  __elasticsearch__.document_type 'applicants'
  __elasticsearch__.index_name "#{PREFIX_INDEX}"

  after_create :invite_user
  before_validation :assign_user_applicant, if: "self.user.present?"
  before_validation :generate_password, if: "self.user.present?"

  attr_accessor :trabajando_id
  attr_accessor :user_is_new

  def self.get_trabajando_applicant_url(trabajando_id)
    "#{TRABAJANDO_GET_INFO_APPLICANT}&country=CL&identifier=#{trabajando_id}"
  end

  def self.get_trabajando_applicant(trabajando_id)
    response = HTTParty.get(self.get_trabajando_applicant_url(trabajando_id))
    json_response = JSON.parse(response.body)
    data = json_response['data']
    personal_info_data = data['personalInfo']
    laboral_experiences_data = data['workExperience']
    academics_background = data['studies']
    degrees_data = academics_background['higherEducation']

    rut = personal_info_data['idNumberFormat']
    user = self.get_user(personal_info_data['emails'], rut)
    applicant = Applicant::Base.find_by(user: user)
    applicant ||= Trabajando::Applicant.new(user: user)
    user = applicant.user

    self.extract_personal_information(user, personal_info_data)
    self.extract_laboral_experiences(user, laboral_experiences_data)
    self.extract_universities_degrees(user, degrees_data)

    if applicant.is_a? Trabajando::Applicant
      applicant.set_integration_data(trabajando_id, json_response)
      applicant.user_is_new = !user.persisted?
    end
    return applicant
  end


  def set_integration_data(source_id, source_data)
    _data = {
      source: 'trabajando', # cambiar este string según la fuente (Trabajando, Laborum, etc)
      source_id: source_id,
      data: source_data,
      imported_at: Time.current
    }
    self.integration_data = _data
  end


  private
    # Método para extraer o generar un usuario asociado
    # a un candidato de trabajando.
    #
    # Parametros:
    # emails_data -> {primaryEmail: '', secondaryEmail: ''}
    # rut -> RUT con formato xx.xxx.xxx-x
    #
    # Se define de esa manera el parámetro emails_data, debido
    # a que es la forma en que vienen dentro de la información
    # personal del json que entrega como respuesta trabajando
    def self.get_user(emails_data, rut=nil)
      rut = rut.try(:gsub, '.', '').to_s.downcase
      primary_email = emails_data['primaryEmail'].to_s.downcase
      secondary_email = emails_data['secondaryEmail'].to_s.downcase

      chile = Territory::Country.find_by(name: "Chile")
      chile_rut_identification_document_type = People::IdentificationDocumentType.find_by(
        country: chile,
        codename: "rut"
      )

      user = User.joins(:personal_information).find_by(
        people_personal_informations: {
          identification_document_number: rut,
          identification_document_type_id: chile_rut_identification_document_type.id
        }
      ) if rut.present? && chile_rut_identification_document_type.present?

      user ||= User.find_for_authentication(email: primary_email)
      user ||= User.find_for_authentication(email: secondary_email)
      unless user
	puts "correo p: #{primary_email}"
        puts "correo s: #{secondary_email}"
	primary_email = primary_email == "" ? nil : primary_email
	secondary_email = secondary_email == "" ? nil : secondary_email
	puts "rut:  #{rut}"
	puts "correo p: #{primary_email}"
	puts "correo s: #{secondary_email}"
	puts "Ambos emails son nulos para rut  #{rut}" if primary_email == nil && secondary_email == nil
        user = User.new(email: primary_email || secondary_email)
      end
      return user
    end

    # Método para extraer la información personal envíada por
    # trabajando y asociarla a un usuario en específico.
    # En caso de que haya algún dato que no este se actualiza
    # con los datos en personal_info_data
    #
    # Parametros:
    # user -> Usuario sobre el cual se debe asociar el información personal
    # personal_information_data -> {firstName: '', lastName: '', ...}
    #
    # Se define de esa manera el parámetro emails_data, debido
    # a que es la forma en que vienen dentro de la información
    # personal del json que entrega como respuesta trabajando
    def self.extract_personal_information(user, personal_info_data)
      first_name = personal_info_data['firstName']
      last_name = personal_info_data['lastName']
      birth_date = personal_info_data['birthDate'].try(:to_date)
      address = personal_info_data['address']
      marital_status_data = personal_info_data['maritalStatus']
      gender_data = personal_info_data['gender']
      city_data = personal_info_data['commune']
      phones_data = personal_info_data['phoneNumbers']
      identification_document_number = personal_info_data['idNumberFormat'] || personal_info_data['idNumber']


      personal_info = user.personal_information
      personal_info ||= user.build_personal_information
      personal_info.first_name = first_name if personal_info.first_name.blank?
      personal_info.last_name = last_name if personal_info.last_name.blank?
      personal_info.address = address if personal_info.address.blank?
      personal_info.sex = get_gender(gender_data) if personal_info.sex.blank?
      personal_info.birth_date = birth_date.try(:to_date) if personal_info.birth_date.blank?
      personal_info.company_marital_status = get_marital_status(marital_status_data) if personal_info.company_marital_status.blank?
      personal_info.city = get_city(city_data) if personal_info.city.blank?
      personal_info.landline = get_landline(phones_data) if personal_info.landline.blank?
      personal_info.cellphone = get_cellphone(phones_data) if personal_info.cellphone.blank?

      if personal_info.identification_document_number.blank?
        personal_info.identification_document_number = identification_document_number.gsub(".", "")
        chile_rut = People::IdentificationDocumentType.find_by(
          country: Territory::Country.find_by(name: 'Chile'),
          name: 'RUT'
        )
        personal_info.identification_document_type = chile_rut
      end

      return personal_info
    end

    def self.extract_laboral_experiences(user, laboral_experiences_data)
      professional_information = user.professional_information
      professional_information ||= user.build_professional_information
      laboral_experiences_data.each do |le|
        position = le['jobPosition']
        company = le['companyName']
        description = le['achievements']
        since_date = le['fromDate'].try(:to_date)
        until_date = le['toDate'].try(:to_date)

        laboral_experience = professional_information.laboral_experiences.find_by(
          position: position,
          company: company
        )
        unless laboral_experience
          laboral_experience = professional_information.laboral_experiences.new(
            position: position,
            company: company,
            description: description,
            since_date: since_date,
            until_date: until_date
          )
        end
      end
    end

    def self.extract_universities_degrees(user, degrees_data)
      degrees = []
      degrees_data.each do |degree_data|
        degrees << extract_university_degree(user, degree_data)
      end
      return degrees
    end

    def self.extract_university_degree(user, degree_data)
      professional_information = user.professional_information
      professional_information ||= user.build_professional_information

      institution = get_institution(degree_data)
      career = get_career(degree_data)

      culmination_year = degree_data['graduationyear']
      culmination_date = culmination_year ? "#{culmination_year}-12-31".to_date : nil
      name = get_degree_name(degree_data)
      degree = professional_information.degrees
        .create_with(
          culmination_date: culmination_date,
          type: 'People::ProfessionalDegree',
          name: name
        )
        .find_or_initialize_by(
          career: career,
          institution: institution
        )
      professional_information.degrees << degree unless degree.persisted?
      degree.update(culmination_date: culmination_date) if degree.culmination_date != culmination_date
      return degree
    end

    # Método helper para integrar las instituciones
    # enviadas por trabajando. En caso de no encontrarla
    # se registra como custom
    def self.get_institution(degree_data)
      trabajando_id = degree_data['country'].try(:[], 'id')
      trabajando_country = Trabajando::Country.find_by(trabajando_id: trabajando_id)
      institution_name = degree_data.try(:[],'institution').try(:[], 'name')
      institution = institution_name ? Education::Institution
          .create_with(custom: true)
          .find_or_create_by(
            name: institution_name,
            country: trabajando_country.try(:country)
          ) : nil
    end

    # Método helper para integrar las carreras
    # enviadas por trabajando. En caso de no encontrarla
    # se registra
    def self.get_career(degree_data)
      career_name = degree_data['career'].try(:[], 'nameAlias')
      career_name ||= degree_data['career'].try(:[], 'name')
      career_name ||= degree_data['minor']
      career = career_name ? Education::Career
          .find_or_create_by(name: career_name) : nil
    end

    def self.get_degree_name(degree_data)
      if degree_data.try(:[],'career').try(:[], 'nameAlias').present?
        return degree_data["career"]["nameAlias"]
      elsif degree_data.try(:[],'career').try(:[], 'name').present?
        return degree_data["career"]["name"]
      else
        return degree_data['minor']
      end
    end

    def self.get_cellphone(phones_data = [])
      cellphone_data = phones_data.select{ |phone_data|
        phone_data['place'] == 'mobile'
      }.first
      cellphone_data.present? ? cellphone_data['number'] : ''
    end

    def self.get_landline(phones_data = [])
      landline = phones_data.select{ |phone_data|
        phone_data['place'] == 'home'
      }.first
      landline.present? ? landline['number'] : ''
    end

    def self.get_gender(gender_data = {})
      trabajando_description = gender_data['description']
      gender = case trabajando_description.try(:first).try(:upcase)
      when 'M'
        'Male'
      when 'F'
        'Female'
      else
        ''
      end
    end

    def self.get_city(city_data = {})
      trabajando_city_id = city_data['id']
      trabajando_city = Trabajando::City.find_by(trabajando_id: trabajando_city_id)

      return trabajando_city.try(:hupe_city)
    end

    def self.get_marital_status(marital_status_data = {})
      return nil unless marital_status_data
      trabajando_id = marital_status_data['id']
      trabajando_name = marital_status_data['name']
      trabajando_marital_status = Trabajando::MaritalStatus
                                    .create_with(name: trabajando_name)
                                    .find_or_create_by(trabajando_id: trabajando_id)
      return trabajando_marital_status.company_marital_status
    end

    def invite_user
      if self.user_is_new
        self.user.delay.invite!
      end
    end

    def generate_password
      if !self.user.id.present?
        generate_password = Devise.friendly_token.first(8)
        self.user.password = generate_password
        self.user.password_confirmation = generate_password
      end
    end

    def assign_user_applicant
      if !self.user.valid? and self.user.errors.messages[:email].present?
        user = User.find_by(email: self.user.email)
        if user.try(:worker).present? and !user.try(:applicant).present?
          self.user = user
        end
      end
    end

end
