class Trabajando::Postulation < ActiveRecord::Base
  include CoursePostulationConnector

  belongs_to :trabajando_publication, class_name: Trabajando::Publication
  belongs_to :applicant, class_name: Applicant::Base

  validates :trabajando_publication, presence: true
  validates :applicant, presence: true
  validates :applicant_id, uniqueness: { scope: :trabajando_publication_id }
  validates :postulation_date, presence: true

  serialize :answers
  delegate :course, to: :trabajando_publication, allow_nil: true
  delegate :accept, :accept!, :reject, :reject!, to: :course_postulation, allow_nil: true

  def self.get_remote_postulations_url(from_time, page=1)
    from_date = I18n.l(from_time, format: '%Y%m%d')
    from_hour_date = I18n.l(from_time, format: '%H:%M')
    #puts from_time
    #puts from_date
    #puts from_hour_date
    "#{TRABAJANDO_GET_POSTULATIONS}&country=CL&from=#{from_date}&hour=#{from_hour_date}&pageNumber=#{page}"
  end

  def self.get_remote_postulations(from_time=Time.now, page=1)
    response = self.call_service_get_postulations(from_time, page)
    json_response = JSON.parse(response.body)
    json_response_data = json_response['data']
    exclusive_postulations = json_response_data['exclusivesPostulations'] || []
    shared_postulations = json_response_data['sharedPostulations'] || []
    all_postulations = exclusive_postulations + shared_postulations
    self.load_trabajando_postulations(all_postulations)

    total_pages = json_response_data['totalPages']
    if page < total_pages
      next_number_page = page + 1
      self.get_remote_postulations(from_time, next_number_page)
    end
  end

  def publication
    return trabajando_publication
  end

  def answers_with_hupe_format
   answers || []
  end

  def created_at
    self.postulation_date
  end

  private
    def self.call_service_get_postulations(from_time, page)
      service_url = get_remote_postulations_url(from_time, page)
      #service_url = "http://wsbcknd.trabajando.com/v1.3.5-CL/rest/Applications/json/forPortal?client=FUNDACIONINTEGRA&portalId=3621&token=c43db60d6df09df7faf65a8601b7e5ed&api_key=TRABAJANDO_TOKEN&country=CL&country=CL&from=20180401&hour=00:00&pageNumber=#{page}"
      puts service_url
      response = HTTParty.get(service_url)
    end

    def self.load_trabajando_postulations(all_postulations_json)
      all_postulations_json.each do |postulation_data|
        remote_publication_id = postulation_data['jobId']
        trabajando_publication = Trabajando::Publication.find_by(trabajando_id: remote_publication_id)
        if trabajando_publication.present?
          remote_applicant_id = postulation_data['applicantId']
          questions = postulation_data['questions']	
          postulation_date = postulation_data['date'].to_time
          applicant = Trabajando::Applicant.get_trabajando_applicant(remote_applicant_id)
          applicant.save(validate: false)
          trabajando_postulation = Trabajando::Postulation.new
          trabajando_postulation.trabajando_publication = trabajando_publication
          trabajando_postulation.trabajando_applicant_id = remote_applicant_id
          trabajando_postulation.applicant = applicant
          trabajando_postulation.answers = []
          trabajando_postulation.postulation_date = postulation_date

          # Se extraen y almacenan las respuestas en trabajando_postulation
          questions.map { |k,v| v}.each_slice(2){ |term|
            trabajando_postulation.answers << {
              question: term[0], answer: term[1]
            }
          }
          trabajando_postulation.save
        end
      end
    end
end
