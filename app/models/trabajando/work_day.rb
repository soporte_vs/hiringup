class Trabajando::WorkDay < ActiveRecord::Base
  has_many :publications, class: Trabajando::Publication

  has_many :company_timetable_trabajando_workdays,
           class_name: Company::TimetableTrabajandoWorkday,
           foreign_key: :trabajando_work_day_id

  has_many :company_timetables,
           through: :company_timetable_trabajando_workdays,
           source: :company_timetable

  validates :name, presence: true, uniqueness: true
  validates :trabajando_id, presence: true, uniqueness: true
end
