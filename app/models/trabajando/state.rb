class Trabajando::State < ActiveRecord::Base
  belongs_to :hupe_state, class: Territory::State

  validates :name, presence: true
  validates :trabajando_id, presence: true
end
