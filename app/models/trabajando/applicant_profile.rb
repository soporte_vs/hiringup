class Trabajando::ApplicantProfile < ActiveRecord::Base
  has_many :publications,
           class: Trabajando::Publication,
           foreign_key: :applicant_profile_id

  belongs_to :hupe_profile_type, class: Applicant::ProfileType
  validates :name, presence: true, uniqueness: true
  validates :trabajando_id, presence: true, uniqueness: true
end
