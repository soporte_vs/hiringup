class Trabajando::EducationLevel < ActiveRecord::Base
  has_many :publications,
           class: Trabajando::Publication

  validates :name, presence: true, uniqueness: true
  validates :trabajando_id, presence: true, uniqueness: true
end
