class Trabajando::MaritalStatus < ActiveRecord::Base
  belongs_to :company_marital_status, class: Company::MaritalStatus

  validates :name, presence: true, uniqueness: true
  validates :trabajando_id, presence: true, uniqueness: true
end
