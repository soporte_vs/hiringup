class Trabajando::JobType < ActiveRecord::Base
  has_many :publications,
           class: Trabajando::Publication,
           foreign_key: :job_type_id

  has_many :company_position_trabajando_job_types,
           class_name: Company::PositionTrabajandoJobType,
           foreign_key: :trabajando_job_type_id,
           dependent: :destroy

  has_many :company_positions,
           through: :company_position_trabajando_job_types,
           source: :company_position

  validates :name, presence: true, uniqueness: true
  validates :trabajando_id, presence: true, uniqueness: true
end
