class Stage::NotApplyReason < ActiveRecord::Base
  validates :name, presence: true

  ROLES_CLASS = [
    {name: :admin, description: I18n.t("discard_reason.roles.class.admin")},
  ]

end
