
#
# Table name: stage_bases
#
#  id            :integer          not null, primary key
#  enrollment_id :integer
#  type          :string
#  created_at    :datetime
#  updated_at    :datetime
#  step          :integer
#  name          :string
#

class Stage::Base < ActiveRecord::Base
  after_create :set_as_current_on_enrollment
  after_commit :touch_enrollment
  before_validation :set_step_and_name!, on: :create
  belongs_to :enrollment, class_name: Enrollment::Base
  has_many :event_resourceables,
           class_name: Calendar::EventResourceable,
           as: :resourceable,
           dependent: :destroy

  has_many :events,
           class_name: Calendar::Event,
           through: :event_resourceables

  has_one :not_applying,
          class_name: Stage::NotApplying,
          foreign_key: :stage_id,
          dependent: :destroy

  validates :enrollment, presence: true
  validates :step, presence: true
  validates :name, presence: true

  attr_accessor :args

  def roles_instance
    [
      {name: :admin, description: I18n.t("engagement.stage.roles.instance.admin")},
    ]
  end

  def is_approved?
    unless self.is_approved.nil?
      self.is_approved
    end
  end

  def disposable?
    true
  end

  def finished?
    self.enrollment.stages.include?(self) && self.enrollment.stage != self
  end

  def progress
    if finished?
      1.0
    else
      0.0
    end
  end

  def set_as_current_on_enrollment
    self.enrollment.reload
    self.enrollment.stage = self
    self.enrollment.current_stage_index = self.step
    self.enrollment.save
  end

  def touch_enrollment
    self.enrollment.try(:touch)
  end

  def agendable?
    self.events.empty?
  end

  def not_apply(not_apply_reason, observations = nil)
    not_applying = self.not_applying

    unless not_applying.present?
      if can_not_apply?
        not_applying = Stage::NotApplying.new(
          stage: self,
          not_apply_reason: not_apply_reason,
          observations: observations
        )
        if not_applying.save
          self.enrollment.next_stage
        end
      else
        self.errors.add(:not_applying, :can_not_apply?)
      end
    end

    return not_applying
  end

  # Se le puede agregar un not_apply a la etapa?
  def can_not_apply?
    true
  end


  def to_s
    name
  end

  private

    def set_step_and_name!
      self.name ||= self.type.try(:constantize).try(:parent).try(:to_s) || Stage.to_s
      if self.step.nil? && self.enrollment.present?
        current_class = self.class.to_s

        positions_step = []
        self.enrollment.course.work_flow_classes.each_with_index do |klass,index|
          if klass == current_class
            positions_step << index
          end
        end

        if positions_step.empty?
          self.step = -1
          return true
        else
          if self.class == Stage::Base
            next_step = self.class.where(enrollment: self.enrollment, type: nil).count
          else
            next_step = self.class.where(enrollment: self.enrollment).count
          end
          self.step = positions_step[next_step]
          return true
        end
      end
    end
end
