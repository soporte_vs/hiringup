class Stage::NotApplying < ActiveRecord::Base
  belongs_to :stage, class_name: Stage::Base
  belongs_to :not_apply_reason, class_name: Stage::NotApplyReason

  validates :stage, presence: true, uniqueness: true
  validates :not_apply_reason, presence: true

end
