class Stage::Document < ActiveRecord::Base
  mount_uploader :document, GenericReportStageUploader

  belongs_to :resource, polymorphic: true

  validates :document, presence: true
  validates :document, file_size: { maximum: 5.megabytes.to_i }
  validates :resource, presence: true
end
