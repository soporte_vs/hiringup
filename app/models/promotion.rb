module Promotion
  def self.table_name_prefix
    'promotion_'
  end

  def self.to_s
    "Aprobación de la Promoción y/o traslado"
  end
end
