module GroupInterview
  def self.table_name_prefix
    'group_interview_'
  end

  def self.to_s
    "Entrevista Grupal"
  end
end
