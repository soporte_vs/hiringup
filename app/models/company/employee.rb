class Company::Employee < ActiveRecord::Base
  has_paper_trail
  belongs_to :user
  belongs_to :boss, class_name: Company::Employee
  has_many :subordinates,
           class_name: Company::Employee,
           foreign_key: :boss_id,
           dependent: :nullify
  has_many :company_employee_positions_cencos,
           class_name: Company::EmployeePositionsCenco,
           foreign_key: :company_employee_id,
           dependent: :destroy
  has_many :company_positions_cencos, through: :company_employee_positions_cencos
  has_many :company_references, class_name: Company::Reference, foreign_key: :company_employee_id, dependent: :destroy

  belongs_to :current_employee_position_cenco, class_name: Company::EmployeePositionsCenco

  validates :user, presence: true, uniqueness: true

  after_create :create_applicant
  after_commit :touch_applicant


  def self.load_massive_from_file(file_path, notify_to)
    sheet = Roo::Spreadsheet.open(file_path)
    fila = 2
    (fila..sheet.last_row).each do |row|
      employee_email = sheet.cell(row, 1).downcase.strip
      is_active = sheet.cell(row, 2) == "No" ? false : true

      if is_active
        # Proceso de creación de jefe
        boss_email = sheet.cell(row, 8).downcase.strip
        boss_password = Devise.friendly_token.first(8)
        boss_user = User.create_with(password: boss_password).find_or_create_by(email: boss_email)
        boss = Company::Employee.find_or_create_by(user: boss_user)
        boss.update(is_boss: true)
        # Proceso de creación de empleado
        employee_password = sheet.cell(row, 3)
        employee_password = Devise.friendly_token.first(8) if employee_password.blank?
        employee_user = User.create_with(password: employee_password).find_or_create_by(email: employee_email)

        first_name = sheet.cell(row, 4).downcase.strip
        last_name = sheet.cell(row, 5).downcase.strip
        sex = sheet.cell(row, 6).downcase.strip.capitalize
        sex = sex == 'M' ? 'Male' : (sex == 'F' ? 'Female' : nil)
        cellphone = sheet.cell(row, 7)
        rut = sheet.cell(row, 9)
        chile = Territory::Country.where(name: "Chile").first_or_create   
        document_type = People::IdentificationDocumentType.where(
          country: chile,
          name: "RUT",
          validation_regex: "^([0-9]+-[0-9Kk])$"
        ).first_or_create
        
        People::PersonalInformation.create_with(
          first_name: first_name,
          last_name: last_name,
          sex: sex,
          cellphone: cellphone,
          identification_document_type_id: document_type.id,
          identification_document_number: rut
        ).find_or_create_by(user: employee_user)

        employee = Company::Employee.find_or_create_by(user: employee_user)
        employee.update(boss: boss)
      else
        employee_user = User.find_by(email: employee_email)
        employee_user.try(:company_employee).try(:destroy)
      end
    end

    Company::EmployeeMailer.delay.report_load_employees(notify_to)
    File.delete(file_path)
  end

  private

    def touch_applicant
      self.try(:user).try(:applicant).try(:touch)
    end

    def create_applicant
      if user.present? && user.applicant.nil?
        Company::ApplicantEmployee.create(user: user)
        user.reload
      end
      user.applicant.touch
    end

end
