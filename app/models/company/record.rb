# == Schema Information
#
# Table name: company_records
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Company::Record < ActiveRecord::Base
  has_paper_trail
  has_many :managements,
           class_name: Company::Management,
           foreign_key: :company_record_id,
           inverse_of: :company_record,
           dependent: :destroy

  has_many :company_business_units,
           class_name: Company::BusinessUnit,
           through: :managements

  validates :name, presence: true, uniqueness: true
  validates :email_domain, presence: true, uniqueness: true
end
