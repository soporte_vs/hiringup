# == Schema Information
#
# Table name: company_positions
#
#  id          :integer          not null, primary key
#  name        :string
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  cod         :string
#

class Company::Position < ActiveRecord::Base
  has_paper_trail
  has_many :company_positions_cencos,
           class_name: Company::PositionsCenco,
           foreign_key: :company_position_id,
           dependent: :destroy,
           inverse_of: :company_position

  has_many :company_cencos, through: :company_positions_cencos
  has_many :used_tags, class_name: Tag::UsedTag, as: :taggable, dependent: :destroy
  has_many :tags, through: :used_tags
  has_many :company_positions_references,
            class_name: Company::PositionsReference,
            foreign_key: :company_position_id

  has_many :company_references, through: :company_positions_references

  validates :name, presence: true, uniqueness: true
  validates :description, presence: true
  validates :cod, numericality: { only_integer: true }, allow_blank: true
end
