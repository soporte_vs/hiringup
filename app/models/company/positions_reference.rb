class Company::PositionsReference < ActiveRecord::Base
  belongs_to :company_reference, class_name: Company::Reference, inverse_of: :company_positions_references
  belongs_to :company_position, class_name: Company::Position

  validates :company_reference, presence: true
  validates :company_position, presence: true
end
