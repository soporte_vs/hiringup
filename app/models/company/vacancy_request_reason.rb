# == Schema Information
#
# Table name: company_vacancy_request_reasons
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Company::VacancyRequestReason < ActiveRecord::Base
  has_paper_trail
  validates :name, presence: true, uniqueness: true
end
