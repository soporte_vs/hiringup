class Company::RecruitmentSource < ActiveRecord::Base
  validates :name, presence: true, uniqueness: true
   has_many :applicants, class_name: Applicant::Base, foreign_key: :company_recruitment_source_id
end
