# == Schema Information
#
# Table name: company_cencos
#
#  id                    :integer          not null, primary key
#  name                  :string
#  address               :string
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  cod                   :string
#  company_management_id :integer
#

class Company::Cenco < ActiveRecord::Base
  has_paper_trail
  belongs_to :company_management,
             class_name: Company::Management,
             foreign_key: :company_management_id,
             inverse_of: :company_cencos

  has_many :company_positions_cencos,
           class_name: Company::PositionsCenco,
           foreign_key: :company_cenco_id,
           dependent: :destroy,
           inverse_of: :company_cenco

  has_many :company_positions, through: :company_positions_cencos

  validates :name, presence: true, uniqueness: true
  validates :cod, numericality: { only_integer: true }, allow_blank: true
  validates :company_management, presence: true

  delegate :company_business_unit, to: :company_management

  def to_s
    "#{company_management} - #{name}"
  end

  def self.get_with_large_name
    Company::Cenco.find_by_sql("
      select
        cenco.id,
        cenco.name,
        cenco.address,
        (rd.name || ' - ' || bu.name || ' - ' || cenco.name || ' - ' || COALESCE(cenco.cod, '')) as name
      from
        company_cencos as cenco,
        company_managements as mg,
        company_records as rd,
        company_business_units as bu
      where
        cenco.company_management_id = mg.id and
        mg.company_record_id = rd.id and
        mg.company_business_unit_id = bu.id
    ")
  end
end
