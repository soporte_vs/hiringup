class Company::TimetableTrabajandoWorkday < ActiveRecord::Base
  belongs_to :company_timetable, class_name: Company::Timetable
  belongs_to :trabajando_work_day, class_name: Trabajando::WorkDay

  validates :company_timetable, presence: true
  validates :trabajando_work_day, presence: true

  validates :trabajando_work_day_id, uniqueness: { scope: :company_timetable_id }
end
