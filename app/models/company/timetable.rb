# == Schema Information
#
# Table name: company_timetables
#
#  id          :integer          not null, primary key
#  name        :string
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Company::Timetable < ActiveRecord::Base

  validates :name, presence: true, uniqueness: true
end
