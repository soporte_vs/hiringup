class Company::EmployeePositionsCenco < ActiveRecord::Base
  has_paper_trail
  belongs_to :company_employee, class_name: Company::Employee
  belongs_to :company_positions_cenco, class_name: Company::PositionsCenco


  validates :company_employee, presence: true
  validates :company_positions_cenco, presence: true
end
