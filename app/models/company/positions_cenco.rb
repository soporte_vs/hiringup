# == Schema Information
#
# Table name: company_positions_cencos
#
#  id                  :integer          not null, primary key
#  company_position_id :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  company_cenco_id    :integer
#

class Company::PositionsCenco < ActiveRecord::Base
  has_paper_trail
  belongs_to :company_position,
             class_name: Company::Position,
             inverse_of: :company_positions_cencos

  belongs_to :company_cenco,
             class_name: Company::Cenco,
             inverse_of: :company_positions_cencos

  has_many :company_employee_positions_cencos, class_name: Company::EmployeePositionsCenco, foreign_key: :company_positions_cenco_id
  has_many :company_employees , through: :company_employee_positions_cencos

  validates :company_position, presence: true
  validates :company_cenco, presence: true

  validates :company_position_id, uniqueness: { scope: :company_cenco_id }

  delegate :name, to: :company_cenco, allow_nil: true, prefix: true

  def fullname
    "#{self.company_cenco.company_management.company_record.name} - #{self.company_cenco.company_management.company_business_unit.name} - #{self.company_cenco.name} - #{self.company_cenco.cod}"
  end

  def to_s
    self.fullname
  end

  def self.get_with_large_name(company_position_id = nil)
    filter_by = company_position_id.present? ? " and cp.company_position_id = #{company_position_id}" : ''
    self.find_by_sql("
      select
        cp.id,
        cp.company_position_id,
        cp.company_cenco_id,
        ( rd.name || ' - ' || bu.name || ' - ' || cenco.name || ' - ' || COALESCE(cenco.cod, '') ) as name
      from
        company_positions_cencos as cp,
        company_cencos as cenco,
        company_managements as mg,
        company_records as rd,
        company_business_units as bu
      where
        cp.company_cenco_id = cenco.id and
        cenco.company_management_id = mg.id and
        mg.company_record_id = rd.id and
        mg.company_business_unit_id = bu.id
    " + filter_by)
  end
end
