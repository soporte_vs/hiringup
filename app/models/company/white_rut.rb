class Company::WhiteRut < ActiveRecord::Base
  validates :rut, presence: true, uniqueness: true
  validate :rut_valid?, if: 'self.rut.present?'

  def self.load_white_list_from_file(file_path, notify_to)
    errors, success = [], []
    sheet = Roo::Spreadsheet.open(file_path)
    sheet.column(1).each do |rut|
      white_rut = Company::WhiteRut.new(rut: rut)
      unless white_rut.save
        errors << [white_rut.rut, white_rut.errors[:rut]]
      else
        success << [white_rut.rut, 'OK']
      end
    end
    Company::WhiteRutMailer.delay.report_load_white_list(notify_to, errors, success)
    File.delete(file_path)
  end

  private

    def rut_valid?
      self.rut = rut.strip.gsub('K', 'k')
      _rut = rut.split('-').first.to_i
      dv = rut.split('-').last
      v = 1
      sum = 0
      for i in (2..9)
        i == 8 ? v = 2 : v += 1
        sum += v * (_rut % 10)
        _rut /= 10
      end
      sum = 11 - sum % 11
      valid_rut = false
      if sum == 11
        valid_rut = '0' == dv
      elsif sum == 10
        valid_rut = 'k' == dv
      else
        valid_rut = sum.to_s == dv
      end
      errors.add(:rut, :invalid) unless valid_rut
    end

end
