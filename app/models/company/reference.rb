class Company::Reference < ActiveRecord::Base
  after_create :send_notifications
  before_validation :set_email

  belongs_to :company_employee, class_name: Company::Employee
  belongs_to :applicant, class_name: Applicant::Base

  has_many :company_positions_references,
           class_name: Company::PositionsReference,
           foreign_key: :company_reference_id,
           dependent: :destroy,
           inverse_of: :company_reference

  has_many :positions,
           class_name: Company::Position,
           through: :company_positions_references,
           source: :company_position

  validates :applicant, presence: true
  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :position, presence: true
  validates :email, presence: true, email: true
  validates :email_name, presence: true, on: [:create]
  validates :email_domain, presence: true, on: [:create]

  accepts_nested_attributes_for :applicant, reject_if: :applicant_empty?

  attr_accessor :email_name
  attr_accessor :email_domain

  def applicant_empty?(attributes)
    !attributes[:user_attributes][:email].present?
  end

  def confirm
    unless self.confirmed
      password = Devise.friendly_token.first(8)
      user = User.create_with(
        password: password,
        paasword_confirmation: password,
        personal_information_attributes: {
          first_name: self.first_name,
          last_name: self.last_name
        }
      ).find_or_create_by(email: self.email)
      company_employee = Company::Employee.find_or_create_by(user: user)

      self.update(confirmed: true, company_employee_id: company_employee.id)
      Company::ReferenceMailer.delay.send_notice_to_applicant(self.applicant)
      self.applicant.touch
    else
      return false
    end
  end

  def generate_confirm_token
    string = "reference_token" + self.email + self.id.to_s
    Digest::MD5.hexdigest(string)
  end

  private
    def set_email
      self.email ||= "#{self.email_name}@#{self.email_domain}"
    end

    def send_notifications
      Company::ReferenceMailer.delay.send_notice_to_employee(self)
    end
end
