# == Schema Information
#
# Table name: company_managements
#
#  id                       :integer          not null, primary key
#  company_record_id        :integer
#  company_business_unit_id :integer
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#

class Company::Management < ActiveRecord::Base
  has_paper_trail
  belongs_to :company_record,
              class_name: Company::Record,
              inverse_of: :managements

  belongs_to :company_business_unit,
             class_name: Company::BusinessUnit,
              inverse_of: :managements

  has_many :company_cencos,
           class_name: Company::Cenco,
           foreign_key: :company_management_id,
           inverse_of: :company_management,
           dependent: :destroy

  validates :company_record, presence: true
  validates :company_business_unit, presence: true, uniqueness: { scope: :company_record_id }

  def to_s
    "#{self.company_record.name} - #{self.company_business_unit.name}"
  end

  def name
    self.to_s
  end
end
