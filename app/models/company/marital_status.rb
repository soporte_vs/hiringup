# == Schema Information
#
# Table name: company_marital_statuses
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Company::MaritalStatus < ActiveRecord::Base
  has_paper_trail
  has_many :people_personal_informations, class_name: People::PersonalInformation,
            foreign_key: :company_marital_status_id,
            dependent: :nullify

  validates :name, presence: true, uniqueness: true
end
