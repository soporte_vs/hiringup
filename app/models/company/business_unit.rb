# == Schema Information
#
# Table name: company_business_units
#
#  id          :integer          not null, primary key
#  name        :string
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Company::BusinessUnit < ActiveRecord::Base
  has_paper_trail
  has_many :managements,
           class_name: Company::Management,
           foreign_key: :company_business_unit_id,
           inverse_of: :company_business_unit,
           dependent: :destroy

  has_many :company_records,
           class_name: Company::Record,
           through: :managements

  delegate :company_positions_cencos, to: :company_cencos
  delegate :company_positions, to: :company_cencos

  validates :name, presence: true, uniqueness: true
  validates :description, presence: true

end
