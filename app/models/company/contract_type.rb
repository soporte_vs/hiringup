# == Schema Information
#
# Table name: company_contract_types
#
#  id          :integer          not null, primary key
#  name        :string
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Company::ContractType < ActiveRecord::Base
  has_paper_trail
  validates :name, presence: true, uniqueness: true
  validates :description, presence: true, uniqueness: true
end
