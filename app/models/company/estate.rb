# == Schema Information
#
# Table name: company_estates
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Company::Estate < ActiveRecord::Base

  validates :name, presence: true, uniqueness: true
  
end
