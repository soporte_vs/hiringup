class Company::PositionTrabajandoJobType < ActiveRecord::Base
  belongs_to :company_position, class_name: Company::Position
  belongs_to :trabajando_job_type, class_name: Trabajando::JobType
  validates :trabajando_job_type_id, uniqueness: { scope: :company_position_id }
end
