class Company::ApplicantEmployee < Applicant::Base
  __elasticsearch__.document_type 'applicants'
  __elasticsearch__.index_name "#{PREFIX_INDEX}"
end
