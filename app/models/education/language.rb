class Education::Language < ActiveRecord::Base
  validates :name, presence: true, uniqueness: true
end
