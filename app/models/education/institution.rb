# coding: utf-8
class Education::Institution < ActiveRecord::Base
  belongs_to :country,
             class_name: Territory::Country

  validates :name, presence: true
  validates :name, uniqueness: { scope: [:type, :country_id] }

  delegate :name, to: :country, allow_nil: true, prefix: true

  after_commit :notify_to_maintainer, on: :create, if: "self.custom == true"

  TYPES = [
    "Education::University",
    "Education::TechnicalInstitute",
    "Education::ProfessionalInstitute"
  ].map{ |t|
    [I18n.t("education/institution.types.#{t}"), t]
  }

  # Este método es para definir si la institución fue ingresada por el usuario
  # o si se creó por sistema.
  # Si fue creada por un usuario entonces custom es true, en otro caso es false
  def custom=(value)
    # asigna true sólo si recibe true, en otro caso es false
    if self.class != Education::Institution
      super(value == true)
    else
      self[:custom] = value == true
    end
  end

  def serializable_hash options=nil
    super.merge "type" => type
  end

  def self.translated_degree_type degree_class
    TYPES.find{|type| type.last == degree_class}.try(:first) || ''
  end

  private
  def notify_to_maintainer
    Education::InstitutionMailer.delay.notify_to_maintainer(self)
  end
end
