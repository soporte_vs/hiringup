# == Schema Information
#
# Table name: education_careers
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Education::Career < ActiveRecord::Base
  validates :name, presence: true

end
