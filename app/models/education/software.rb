class Education::Software < ActiveRecord::Base
  validates :name, presence: :true, uniqueness: :true
  has_many :professional_informations, through: :software_skills

end
