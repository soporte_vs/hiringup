class Offer::RejectReason < ActiveRecord::Base
  has_many :offer_letters, class_name: Offer::Letter, foreign_key: :offer_reject_reason_id
  validates :name, uniqueness: true, presence: true
end
