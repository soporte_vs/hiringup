# coding: utf-8

# == Schema Information
#
# Table name: offer_letters
#
#  id                       :integer          not null, primary key
#  company_position_id      :integer
#  company_contract_type_id :integer
#  vacancy_id               :integer
#  enrollment_id            :integer
#  direct_boss              :string
#  net_remuneration         :integer
#  contract_start_date      :date
#  accepted                 :boolean
#  token                    :string
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#

class Offer::Letter < ActiveRecord::Base
  belongs_to :company_position, class_name: Company::Position
  belongs_to :company_contract_type, class_name: Company::ContractType
  belongs_to :vacancy, inverse_of: :offer_letters
  belongs_to :offer_reject_reason, class_name: Offer::RejectReason

  # no aplica para integra
  #validates :company_contract_type_id, presence: true
  belongs_to :stage, class_name: Offer::Stage

  validates :address, presence: true
  validates :direct_boss, presence: true
  validates :net_remuneration, presence: true
  validates :contract_start_date, presence: true
  validates :company_position_id, presence: true
  validates :zone_assignation, numericality: {
    only_integer: true, greater_than_or_equal_to: 0,
    less_than_or_equal_to: 100
  }, allow_nil: true
  validates :vacancy, presence: true
  validates :stage, presence: true

  validates :token, presence: true
  validates :token, uniqueness: true

  before_validation :set_token
  delegate :enrollment, to: :stage, allow_nil: true

  def mark_accepted
    # si ya está aceptada retorna true
    return true if self.accepted

    ActiveRecord::Base.transaction do
      if self.accepted.nil? && self.update(accepted: true)
        # se cierra etapa sólo si se pudo actualizar
        self.stage.enrollment.next_stage unless enrollment.type.eql? "Enrollment::Dynamic"
        # se envía notificación a la psicóloga
        self.send_notification_to_psychologist(self)

        self.enrollment.index_document
        return true
      else
        return false
      end
    end
  end

  def mark_rejected(offer_reject_reason_id=nil)
    ActiveRecord::Base.transaction do
      if self.accepted.nil? && self.update(accepted: false, offer_reject_reason_id: offer_reject_reason_id)

        # se envía notificación a la psicóloga
        self.send_notification_to_psychologist(self)
        self.enrollment.index_document
        return true
      else
        return false
      end
    end
  end

  def send_notification_to_psychologist(offer_letter)
    Offer::StageMailer.delay.send_notification_to_psychologist(offer_letter)
  end

  def send_offer
    Offer::StageMailer.delay.send_offer_letter(self)
  end

  def generate_pdf
    action_controller = ActionController::Base.new
    action_controller.instance_variable_set("@offer_letter", self)

    pdf = WickedPdf.new.pdf_from_string(
      action_controller.render_to_string('offer/stages/show.pdf.slim', layout: false)
    )

    pdf_path = "#{Rails.root}/tmp/offer_letter-#{self.id}-#{DateTime.now.to_i}.pdf"
    pdf = File.open(pdf_path, 'wb') do |file|
      file << pdf
    end
    return pdf
  end

  private
  def set_token
    if not self.persisted?
      begin
        self.token = SecureRandom.hex
      end while self.class.exists?(token: token)
    end
  end

end
