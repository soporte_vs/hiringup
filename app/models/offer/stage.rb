# == Schema Information
#
# Table name: stage_bases
#
#  id            :integer          not null, primary key
#  enrollment_id :integer
#  type          :string
#  created_at    :datetime
#  updated_at    :datetime
#  step          :integer
#  name          :string
#

class Offer::Stage <  Stage::Base
  has_one :offer_letter, class_name: Offer::Letter, dependent: :destroy

  validates :type, uniqueness: { scope: [:enrollment_id]}

  ROLES_CLASS = [
    {name: :admin, description: I18n.t("offer.stage.roles.class.admin")}
  ]

  def roles_instance
    [
      {name: :admin, description: I18n.t("offer.stage.roles.instance.admin")},
    ]
  end

  def can_not_apply?
    offer_letter = Offer::Letter.find_by(stage: self)
    return false if offer_letter.present?
    super
  end

  def is_approved?
    offer_letter.present? ? offer_letter.accepted : nil
  end
end
