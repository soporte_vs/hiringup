  # coding: utf-8
class Promotion::Stage <  Stage::Base
  ROLES_CLASS = [
    {name: :admin, description: I18n.t("promotion.stage.roles.class.admin")}
  ]

  def roles_instance
    [
      {name: :admin, description: I18n.t("promotion.stage.roles.instance.admin")},
    ]
  end

  def send_acceptance_email
    close_vacancy
    Promotion::StageMailer.delay.send_acceptance_email(self)
  end

  def to_s
    return "Promoción"
  end

  private

  def close_vacancy
    self.enrollment.course.vacancies.last.update(closed_at: self.created_at)
  end
end
