class Transparency::Report
  require 'tempfile'
  require 'zip'

  def initialize(year, month)
    @year = year
    @month = month
    @zipfile_path = "#{Rails.root}/tmp/#{self.name}.zip"
  end

  def name
    "#{@month}-#{@year}-reporte-transparencia-#{DateTime.now.to_i}"
  end

  def zipfile_path
    @zipfile_path
  end

  def generate_zip
    publications = Minisite::Publication.where(created_at: date_range)

    Zip::File.open(@zipfile_path, Zip::File::CREATE) do |zipfile|
      publications.each do |p|
        action_controller = ActionController::Base.new
        action_controller.instance_variable_set("@minisite_publication", Minisite::Publication.find(p.id))
        action_controller.instance_variable_set("@minisite_postulation", Minisite::Postulation.new(minisite_publication: @minisite_publication))
        action_controller.instance_variable_get("@minisite_publication").questions.each do |question|
          action_controller.instance_variable_get("@minisite_postulation").answers.new(minisite_question: question)
        end

        pdf = WickedPdf.new.pdf_from_string(
          action_controller.render_to_string('minisite/publications/job.pdf.slim', layout: false)
        )

        pdf_path = "#{Rails.root}/tmp/#{self.name}-#{DateTime.now.to_i}.pdf"
        pdf = File.open(pdf_path, 'wb') do |file|
          file << pdf
        end
        zipfile.add("#{p.seo_path}.pdf", pdf)
      end
    end
  end

  def date_range
    datetime_start = DateTime.new(@year, @month, 1, 00, 00, 0)
    datetime_end = datetime_start + 1.months - 1.seconds
    datetime_start..datetime_end
  end
end
