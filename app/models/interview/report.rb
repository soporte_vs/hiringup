# == Schema Information
#
# Table name: interview_reports
#
#  id                 :integer          not null, primary key
#  observations       :text
#  document           :string
#  is_approved        :boolean
#  interview_stage_id :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

class Interview::Report < ActiveRecord::Base
  mount_uploader :document, GenericReportStageUploader
  after_commit :touch_enrollment, on: [:create, :update]
  belongs_to :interview_stage,
             class_name: Interview::Stage,
             foreign_key: :interview_stage_id

  validates :observations, presence: true
  validates :interview_stage, presence: true
  validates :document, file_size: { maximum: 5.megabytes.to_i }

  def touch_enrollment
    self.interview_stage.touch_enrollment
  end
end
