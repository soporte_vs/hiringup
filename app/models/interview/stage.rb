# == Schema Information
#
# Table name: stage_bases
#
#  id            :integer          not null, primary key
#  enrollment_id :integer
#  type          :string
#  created_at    :datetime
#  updated_at    :datetime
#  step          :integer
#  name          :string
#

class Interview::Stage <  Stage::Base
  has_one :interview_report,
          class_name: Interview::Report,
          foreign_key: :interview_stage_id,
          dependent: :destroy

  ROLES_CLASS = [
    {name: :admin, description: I18n.t("interview.stage.roles.class.admin")}
  ]

  def roles_instance
    [
      {name: :admin, description: I18n.t("interview.stage.roles.instance.admin")},
    ]
  end

  def to_s
    'Entrevista'
  end

end
