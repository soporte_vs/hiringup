module Laborum
  def self.table_name_prefix
    'laborum_'
  end

  def self.get_publication_url
    'http://www.laborum.cl/empleos/'
  end
end
