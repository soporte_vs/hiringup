module AssigningWeighting
  def self.table_name_prefix
    'assigning_weighting_'
  end

  def self.to_s
    "Asignación de ponderación"
  end
end
