module Document::RecordsHelper
  #
  def support_react_document_uploader?
    # FormData support starts from following desktop browsers versions. IE 10+, Firefox 4.0+, Chrome 7+, Safari 5+, Opera 12+
    # React supports IE 8+
    if (browser.ie? and browser.version.to_i <= 11) or
      (browser.firefox? and browser.version.to_i < 4) or
      (browser.chrome? and browser.version.to_i < 7) or
      (browser.safari? and browser.version.to_i < 5) or
      (browser.opera? and browser.version.to_i < 12)
      return false
    else
      return true
    end
  end
end
