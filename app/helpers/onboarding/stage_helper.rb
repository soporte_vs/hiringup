module Onboarding::StageHelper
  class << self
    def html_preview(view, onboarding_stage)
      return "" if onboarding_stage.nil?

      if onboarding_stage.enrollment.has_documents?
        message = 'Ver Documentos'
      else
        message = 'Subir Documentos'
      end

      link = view.link_to(
        message,
        view.polymorphic_path([onboarding_stage.enrollment.course,onboarding_stage]),
        class: 'btn-link'
      )
      html = <<-HTML
      <div>
        #{link}
      </div>
      HTML

      html.html_safe
    end
  end
end
