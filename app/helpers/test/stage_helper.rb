module Test::StageHelper
  class << self
    def html_preview(view, test_stage)
      return "" if test_stage.nil?

      message = view.t('test.stage.title')

      link = view.link_to(
        message,
        view.polymorphic_path([test_stage.enrollment.course,test_stage]),
        class: 'btn-link'
      )
      html = <<-HTML
      <div>
        #{link}
      </div>
      HTML

      html.html_safe
    end
  end
end