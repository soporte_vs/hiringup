module PsycholaboralEvaluation::StageHelper
  class << self
    def html_preview(view, psycholaboral_evaluation_stage)
      return "" if psycholaboral_evaluation_stage.nil?

      message = view.t('psycholaboral_evaluation.stage.title')

      link = view.link_to(
        message,
        view.polymorphic_path([psycholaboral_evaluation_stage.enrollment.course,psycholaboral_evaluation_stage]),
        class: 'scaffold_link __blue'
      )
      html = <<-HTML
      <div>
        #{link}
      </div>
      HTML

      html.html_safe
    end
  end
end