module ApplicationHelper
  def flash_box_printer(styles = "col-md-offset-2 col-md-8")
    if flash.present?
      content_tag(:div, class: "#{styles} flash-box flash-type-#{flash.discard.keys.join(', ')} ") do
        content_tag(:div, class: 'flash-wrapper') do
          flash.collect do |key, val|
            if val.class == Array
              val.collect do |k,v|
                content_tag(:div, "#{k}: #{val[k].join(', ')}", class: "flash_item flash_#{key}")

              end.join.html_safe
              #val-each
            elsif val.class == Hash
              val.collect do |k,v|
                if !v.empty?
                  content_tag(:div, "#{val[k].join(', ')}", class: "flash_item flash_#{key}")
                end
              end.join.html_safe

            else
              content_tag(:div, "#{val}", class: "flash_item flash_#{key}")
            end
          end.join.html_safe
        end #flash-each
      end
      #flash-box
    else
      return nil
    end

  end
  #flash_box_printer

  def field_errors_wrapper(errors=[])
     return "" if errors.empty?

    messages = errors.map { |e| content_tag(:li, e) }.join

    html = <<-HTML
    <div class="field_with_errors" role="alert">
       <ul>#{messages}</ul>
    </div>
    HTML

    html.html_safe
  end
end
#ApplicationHelper
