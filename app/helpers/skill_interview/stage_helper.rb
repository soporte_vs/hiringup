module SkillInterview::StageHelper
  class << self
    def html_preview(view, skill_interview_stage)
      return "" if skill_interview_stage.nil?

      message = view.t('skill_interview.stage.title')

      link = view.link_to(
        message,
        view.polymorphic_path([skill_interview_stage.enrollment.course,skill_interview_stage]),
        class: 'btn-link'
      )
      html = <<-HTML
      <div>
        #{link}
      </div>
      HTML

      html.html_safe
    end
  end
end