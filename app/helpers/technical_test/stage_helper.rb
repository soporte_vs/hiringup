module TechnicalTest::StageHelper
  class << self
    def html_preview(view, technical_test_stage)
      return "" if technical_test_stage.nil?

      message = view.t('technical_test.stage.title')

      link = view.link_to(
        message,
        view.polymorphic_path([technical_test_stage.enrollment.course,technical_test_stage]),
        class: 'scaffold_link __blue'
      )
      html = <<-HTML
      <div>
        #{link}
      </div>
      HTML

      html.html_safe
    end
  end
end