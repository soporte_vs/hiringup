module AssigningWeighting::StageHelper
  class << self
    def html_preview(view, assigning_weighting_stage)
      return "" if assigning_weighting_stage.nil?

      message = view.t('assigning_weighting.stage.title')

      link = view.link_to(
        message,
        view.polymorphic_path([assigning_weighting_stage.enrollment.course,assigning_weighting_stage]),
        class: 'btn-link'
      )
      html = <<-HTML
      <div>
        #{link}
      </div>
      HTML

      html.html_safe
    end
  end
end