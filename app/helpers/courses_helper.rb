module CoursesHelper
  def health_bar_class(health)
    if health < 50
      return "progress-bar-success"
    elsif health < 100
      return "progress-bar-warning"
    else
      return "progress-bar-danger"
    end
  end
end
