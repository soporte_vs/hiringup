module People::PersonalInformationHelper
  def get_gender(user=@user)
    if user.present?
      user.sex_word
    else
      I18n.t("activerecord.models.people/personal_information.genders.undefined")
    end
  end
end
