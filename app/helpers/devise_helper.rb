module DeviseHelper
  def devise_error_messages!
    sentence = I18n.t("errors.messages.not_saved",
                      :count => resource.errors.count,
                      :resource => resource.class.model_name.human.downcase)

    flash[:error] = resource.errors.messages
  end

  def devise_error_messages?
    resource.errors.empty? ? false : true
  end

  def resource_name
    :user
  end

  def resource_class
    User
  end

  def resource
    @resource ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end
end
