module Promotion::StageHelper
  class << self
    def html_preview(view, promotion_stage)
      return "" if promotion_stage.nil?

      message = view.t('promotion.stage.title')

      link = view.link_to(
        message,
        view.polymorphic_path([promotion_stage.enrollment.course,promotion_stage]),
        class: 'btn-link'
      )
      html = <<-HTML
      <div>
        #{link}
      </div>
      HTML

      html.html_safe
    end
  end
end