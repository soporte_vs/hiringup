module ManagerInterview::StageHelper
  class << self
    def html_preview(view, manager_interview_stage)
      return "" if manager_interview_stage.nil?

      message = view.t('manager_interview.stage.title')

      link = view.link_to(
        message,
        view.polymorphic_path([manager_interview_stage.enrollment.course,manager_interview_stage]),
        class: 'btn-link'
      )
      html = <<-HTML
      <div>
        #{link}
      </div>
      HTML

      html.html_safe
    end
  end
end