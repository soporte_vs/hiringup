module Stage::BaseHelper
  # Cuando se haga una generalizacion con el uso de Stage:Document en clases del tipo Stage::Base, este modulo no sera necesario.
  # Esta generalizacion aun no es posible porque las stages estan bajo cambios continuos.
  def get_report_documents(enrollment)
    documents = {}
    enrollment.stages.each do |stage| 
      method_sym = (stage.type.tableize.split('/')[0]+'_report').to_sym
      if stage.methods.include? method_sym
        report = stage.send method_sym
        if report.present? and not report.documents.empty?
          documents[stage] = report.documents
        end
      end
    end
    documents
  end
  
  def file_download_link(stage, document)
    helper_sym = "download_course_#{stage.type.tableize.split('/')[0]}_report_path"
    course = stage.enrollment.course
    report = document.resource
    self.send helper_sym, id: course, stage_id: stage.id, report_id: report.id, stage_document_id: document.id
  end
  
end