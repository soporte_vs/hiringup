module Enrollment::StageHelper
  def current_stage(enrollment)
    if enrollment.discarding.present?
      "#{t('enrollment/base.discarted')}: #{enrollment.discarding.discard_reason.name}"
    else
      if enrollment.stage.present?
        if enrollment.stage.class == Engagement::Stage
          enrollment.stage.to_s(true)
        else
          enrollment.stage.to_s
        end
      else
        "Asignado"
      end
    end
  end
end
