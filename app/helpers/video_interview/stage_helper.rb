module VideoInterview::StageHelper
  class << self
    def html_preview(view, video_interview_stage)
      return "" if video_interview_stage.nil?

      message = view.t('video_interview.stage.title')

      link = view.link_to(
        message,
        view.polymorphic_path([video_interview_stage.enrollment.course,video_interview_stage]),
        class: 'btn-link'
      )
      html = <<-HTML
      <div>
        #{link}
      </div>
      HTML

      html.html_safe
    end
  end
end