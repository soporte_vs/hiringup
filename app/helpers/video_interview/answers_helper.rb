module VideoInterview::AnswersHelper
  def show_time_limit(answer)
    ret = ""
    if answer.time_limit / 60 > 0
      if answer.time_limit / 60 == 1
        ret = "un minuto "
      else
        ret = (answer.time_limit / 60).to_s + " minutos "
      end
    end

    if answer.time_limit % 60 != 0
      if answer.time_limit % 60 == 1
        ret +=  "un segundo "
      else
        ret += (answer.time_limit % 60).to_s + " segundos "
      end
    end
    ret
  end
end
