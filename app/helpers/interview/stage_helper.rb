module Interview::StageHelper
  class << self
    def html_preview(view, interview_stage)
      return "" if interview_stage.nil?

      message = view.t('interview.stage.title')

      link = view.link_to(
        message,
        view.polymorphic_path([interview_stage.enrollment.course,interview_stage]),
        class: 'btn-link'
      )
      html = <<-HTML
      <div>
        #{link}
      </div>
      HTML

      html.html_safe
    end
  end
end