module GroupInterview::StageHelper
  class << self
    def html_preview(view, group_interview_stage)
      return "" if group_interview_stage.nil?

      message = view.t('group_interview.stage.title')

      link = view.link_to(
        message,
        view.polymorphic_path([group_interview_stage.enrollment.course,group_interview_stage]),
        class: 'scaffold_link __blue'
      )
      html = <<-HTML
      <div>
        #{link}
      </div>
      HTML

      html.html_safe
    end
  end
end