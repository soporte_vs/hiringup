module IntegraSpreadsheet::ReportHelper

  def base_columns
    [
      "Título del\n Proceso",
      "Rut",
      "DV",
      "Nombres",
      "Apellidos",
      "Fecha de Nacimiento",
      "Correo Electrónico",
      "Teléfono",
      "Celular",
      "Nivel Máximo\n  de Estudios",
      "a. Título de\n Enseñanza Media",
      "a. Institución\n Enseñanza Media",
      "b. Título\n Liceo Técnico",
      "b. Institución\n Liceo Técnico",
      "c. Título\n Técnico Profesional",
      "c. Institución\n Técnico Profesional",
      "d. Título\n Universitario",
      "d. Institución\n Universitario",
      "Último Cargo",
      "Última\n Empresa",
      "Fecha de recepción\n de postulación",
      "Portal de \n postulación"
    ]
  end

  # Retorna porción de la fila con información básica del aplicante, va
  # a la izquierda de las respuestas en reporte de enrollments.
  def applicant_part(enrollment)
    return [] unless enrollment.applicant.presence
    applicant = enrollment.applicant
    applicant_personal_info =  applicant.try(:user).try(:personal_information)

    last_laboral_experience = applicant.user.professional_information.laboral_experiences.order("since_date DESC").first rescue nil
    max_study_level = People::Degree.translated_degree_type(applicant_personal_info.try(:study_type))
    degrees = applicant.user.professional_information.degrees rescue nil
    ed_media = degrees.where(type: People::HighSchoolDegree).first rescue nil
    ed_liceo = degrees.where(type: People::TechnicalDegree).first rescue nil
    ed_tecnica = degrees.where(type: People::TechnicalProfessionalDegree).first rescue nil
    ed_universitaria = degrees.where(type:People::ProfessionalDegree).first rescue nil
    postulation_date = l enrollment.postulation.try(:created_at).try(:to_date) rescue ''

    if enrollment.postulation.present?
      postulation_class = enrollment.postulation_origin.try(:class)
      postulation_origin = postulation_class == Minisite::Publication ? 'Minisite' : 'Trabajando'
    else
      postulation_origin = ''
    end
    [
      enrollment.course.title,
      applicant.get_rut.split('-').first,
      applicant.get_rut.split('-').last, # DV
      applicant.first_name,
      applicant.last_name,
      applicant.birth_date,
      applicant.email,
      applicant.landline,
      applicant.cellphone,
      max_study_level,
      ed_media.try(:name),
      ed_media.try(:institution).try(:name),
      ed_liceo.try(:name),
      ed_liceo.try(:institution).try(:name),
      ed_tecnica.try(:name),
      ed_tecnica.try(:institution).try(:name),
      ed_universitaria.try(:name),
      ed_universitaria.try(:institution).try(:name),
      last_laboral_experience.try(:position),
      last_laboral_experience.try(:company),
      postulation_date,
      postulation_origin
    ]
  end

  # Retorna un array con las respuestas de cada publicación de un tipo en específico,
  # relleno con nils en el lugar de las preguntas sin respuesta por que el candidato no
  # entró(se enroló) mediante esa publicación.
  # Ej. group_answers_by_type(postulación minisite, Minisite::Postulation, preguntas minisite)
  #  => [respuestaminisite1, respuestaminisite2, respuestaminisite3, nil, nil, nil]
  # Que calza con las preguntas que están en el reporte así:
  # [publicación minisite1 pregunta 1, 2, 3, publicación minisite2 pregunta 1, 2, 3]
  def group_answers_by_type(postulation, postulation_class, type_questions)
    row_part = []
    answers = postulation.answers_with_hupe_format
    questions = type_questions.map{ |pub_id, pub_q| pub_q}.reduce(&:+) || []
    return row_part unless questions.presence
    if postulation.class == postulation_class
      type_questions.each do |pub_id, pub_questions|
        if postulation.publication.id != pub_id
          row_part += [nil] * pub_questions.length
        else
          row_part += pub_questions.map do |question|
            answers.find{|a| a[:question] == question}.try(:[], :answer)
          end
        end
      end
    else
      row_part += [nil] * questions.length
    end
    row_part
  end

  # Retorna la porción de la fila de una postulation de las respuestas,
  # con una respuesta por celda que calza con las preguntas en formato del reporte
  # de question headers más abajo.
  # Ej. si hay dos publicaciones para un proceso, una de minisite y otra trabajando,
  # el encabezado es:
  # [pregunta minisite 1, minisite 2, minisite 3, pregunta trabajando 1, 2, 3]
  # Para un enrollment que entró por trabajando esto retorna:
  # [nil, nil, nil, respuesta trabajando 1, respuesta trab. 2, respuesta trab. 3]
  def answers_part(postulation, grouped_questions)
    return [] unless postulation.presence

    answers_row = group_answers_by_type(postulation, Minisite::Postulation, grouped_questions[:minisite])
    answers_row += group_answers_by_type(postulation, Trabajando::Postulation, grouped_questions[:trabajando])
    answers_row
  end

  # Retorna un encabezado de una con una celda por cada pregunta. De haber multiples publicaciones
  # y por ende múltiples preguntas las agrupa todas en un mismo array de esta forma:
  # question_headers(course.grouped_questions) =>
  #   ['Pregunta minsitio 1, Pregunta minisitio 2, Pregunta ministio 3, Pregunta trabajando 1']
  def question_headers(questions)
    mst_questions = questions[:minisite].map{ |pub, pub_q| pub_q}.reduce(&:+) || []
    mst_question_columns = mst_questions.each_with_index.map{|q, i| "Pregunta Minisitio #{i + 1}: \n '#{q}'"}

    trb_questions = questions[:trabajando].map{ |pub, pub_q| pub_q}.reduce(&:+) || []
    trb_question_columns = trb_questions.each_with_index.map{|q, i| "Pregunta Trabajando #{i + 1}: \n '#{q}'"}

    mst_question_columns + trb_question_columns
  end

end
