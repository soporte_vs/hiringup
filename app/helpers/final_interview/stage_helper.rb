module FinalInterview::StageHelper
  class << self
    def html_preview(view, final_interview_stage)
      return "" if final_interview_stage.nil?

      message = view.t('final_interview.stage.title')

      link = view.link_to(
        message,
        view.polymorphic_path([final_interview_stage.enrollment.course,final_interview_stage]),
        class: 'scaffold_link __blue'
      )
      html = <<-HTML
      <div>
        #{link}
      </div>
      HTML

      html.html_safe
    end
  end
end