module CustomFormHelper
  def field_required?(form, field)
    form.object.singleton_class._validators[field.to_sym].select{ |validator|
      validator.class == ActiveRecord::Validations::PresenceValidator
    }.any?
  end
end