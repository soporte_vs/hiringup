# encoding: utf-8

class RecordUploader < CarrierWave::Uploader::Base
  storage :file

  def store_dir
    "#{Rails.root}/private/uploads/#{model.class.to_s.underscore}/#{model.document_descriptor.name.to_sym.to_s}/#{model.id}"
  end

  def extension_white_list
    if model.try(:document_descriptor).try(:allowed_extensions).present?
      model.document_descriptor.allowed_extensions.split
    else
      Document::Descriptor::DEFAULT_EXTENSIONS
    end
  end
end
