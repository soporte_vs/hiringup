
var Modal = function (buttons, close_button) {
  this.currentModal;

  this.descarte = _.getClasses(buttons);
  this.closeButton = _.getClasses(close_button);

  this.init();
}

Modal.prototype.show = function (e, self) {
  // Mostrar
  if (this.currentModal){
    if (this.currentModal.className.indexOf('__hide') < 0) {
      this.currentModal.className += ' __hide';
      this.currentModal.id = '';
    }
  }
  this.currentModal = _.getClasses('course_candidate-discard-modal', self.parentNode)[0];
  this.currentModal.id = 'active-modal';
  _.removeClass(this.currentModal, '__hide');
}

Modal.prototype.close = function (e, self) {
  // Event listener a boton cerrar
  self.parentNode.parentNode.className += ' __hide';
  if (this.currentModal) {
    this.currentModal.id = '';
    this.currentModal = false;
  }
}

Modal.prototype.externalClose = function (e) {
  // Detectar click externo
  if (this.currentModal) {
    var level = 0;
    for (var element = e.target; element; element = element.parentNode) {
      if (element) {
        if (element.id === 'active-modal') {
          return;
        }
      }
      level++;
    }
    this.currentModal.className += ' __hide';
    this.currentModal.id = '';
    this.currentModal = false;
  }
}

Modal.prototype.init = function () {
  var self = this;

  for (var j = 0; j < this.descarte.length; j++) {
    // Event listener a boton descartar (llama el modal)
    this.descarte[j].addEventListener('click', function(e) {
      self.show(e, this);
    }, true);


    this.closeButton[j].addEventListener('click', function(e) {
      self.close(e, this);
    }, false);
  }

  document.addEventListener('click', function (e) {
    self.externalClose(e);
  }, true);
}

