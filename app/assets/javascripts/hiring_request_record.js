(function () {

  $(document).ready(function () {
    $('.js-datepicker').datetimepicker({
      lazyInit:     true,
      timepicker: false,
      format:'d/m/Y'
    });

    $(".chosen-select").chosen({
      allow_single_deselect: true,
      placeholder_text_single: "Seleccione una opción",
      no_results_text: "No hay coincidencias con"
    });
  })

})();
