(function () {

  $(document).ready(function () {	
    $('.js-datepicker').datetimepicker({
      lazyInit:     true,
      timepicker: false,
      format:'d/m/Y'
    });
  })

})();
