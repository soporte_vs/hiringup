
var OptionSelect = React.createClass({
  render: function() {
    return (
      <option value={this.props.value}>{this.props.text}</option>
    );
  }
});

var VacancyForm = React.createClass({
  propTypes:{
    vacancy: React.PropTypes.shape({
      observations: React.PropTypes.string.isRequired,
      _destroy: React.PropTypes.string
    })
  },
  handlerChangeObservations: function (event) {
    this.state.vacancy.observations = event.target.value;
    this.setState({vacancy: this.state.vacancy});
  },
  handlerChangeCenco: function (event) {
    this.state.vacancy.cenco_id = event.target.value;
    this.setState({vacancy: this.state.vacancy});
  },
  handlerDelete: function (event) {
    this.state.vacancy._destroy = event.target.value;
    this.setState({vacancy: this.state.vacancy});
  },
  getInitialState: function () {
    // Channel.subscribe('company/cencos', this.update_company_cencos);
    return {
      vacancy: this.props.vacancy,
      company_cencos: this.props.company_cencos ? this.props.company_cencos : []
    }
  },
  update_company_cencos: function (company_cencos) {
    this.setState({company_cencos: company_cencos});
    $(".js-select").trigger("chosen:updated");
  },
  render: function () {
    var input_vacancy_id, button_delete_degre;
    if (this.state.vacancy.id) {
      input_vacancy_id = <input type='hidden' value={this.state.vacancy.id} name={this.props.name + '[id]'} />
    }
    button_delete_degre = (
      <div>
        <input type='checkbox' name={this.props.name + '[_destroy]'} onClick={this.handlerDelete} defaultChecked={this.state.vacancy._destroy} />
        <label>&nbsp;Eliminar</label>
      </div>
    )
    if (this.state.vacancy._destroy == 'on'){
      return (
        <div className="create-course_row"  style={{display: 'none'}}>
          {button_delete_degre}
          {input_vacancy_id}
        </div>
      )
    }else{
      return (
        <div className="create-course_vacancy-element">
          <FieldWrapper errors={this.props.errors.course_id}>

            <FieldWrapper errors={this.props.errors.company_positions_cenco}>
              <div className="create-course_row">
                <label htmlFor={React.sanitizeName(this.props.name + '[cenco_id]')}
                       className="create-course_label">Centro de Costo</label>
                <select id={React.sanitizeName(this.props.name + '[cenco_id]')}
                        name={this.props.name + '[cenco_id]'}
                        value={this.state.vacancy.cenco_id}
                        onChange={this.handlerChangeCenco}
                        className='form_element __select js-select'
                        data-placeholder='Seleccion opción'
                        required='true'
                        >

                  <option value=''></option>
                  {this.state.company_cencos.map(function (company_cenco, index){
                    return <OptionSelect value={company_cenco.id} text={company_cenco.name}  key={'cenco_' + index} />
                  })}
                </select>
              </div>
            </FieldWrapper>

            <FieldWrapper errors={this.props.errors.company_vacancy_request_reason_id}>
              <div className="create-course_row">
                <label htmlFor={React.sanitizeName(this.props.name + '[company_vacancy_request_reason_id]')}
                       className="create-course_label">Motivo de la solicitud</label>

                <select id={React.sanitizeName(this.props.name + '[company_vacancy_request_reason_id]')}
                        name={this.props.name + '[company_vacancy_request_reason_id]'}
                        value={this.state.vacancy.company_vacancy_request_reason_id}
                        onChange={this.handlerChangeCenco}
                        className='form_element __select js-select'
                        data-placeholder='Seleccion opción'>

                  <option value=''></option>
                  {this.props.vacancy_request_reasons.map(function (company_cenco, index){
                    return <OptionSelect value={company_cenco.id} text={company_cenco.name}  key={'cenco_' + index} />
                  })}
                </select>
              </div>
            </FieldWrapper>

            <FieldWrapper errors={this.props.errors.observations}>
              <div className="create-course_row">
                <label htmlFor={React.sanitizeName(this.props.name + '[observations]')}
                       className="create-course_label">Observaciones</label>
                <input className="create-course_input"
                       id={React.sanitizeName(this.props.name + '[observations]')}
                       name={this.props.name + '[observations]'}
                       value={this.state.vacancy.observations}
                       onChange={this.handlerChangeObservations} />
              </div>
            </FieldWrapper>

            <FieldWrapper errors={this.props.errors.agreement}>
              <div className="create-course_row">
                {button_delete_degre}
                {input_vacancy_id}
              </div>
            </FieldWrapper>

          </FieldWrapper>
        </div>
      )
    }
  }
});
