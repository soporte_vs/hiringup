
var OptionSelect = React.createClass({
  render: function() {
    return (
      <option value={this.props.value}>{this.props.text}</option>
    );
  }
});

var MultipliedVacancyForm = React.createClass({
  propTypes:{
    vacancy: React.PropTypes.shape({
      observations: React.PropTypes.string.isRequired,
      _destroy: React.PropTypes.string
    })
  },
  handlerChangeObservations: function (event) {
    this.state.vacancy.observations = event.target.value;
    this.setState({vacancy: this.state.vacancy});
  },
  handlerChangeCenco: function (event) {
    this.state.vacancy.cenco_id = event.target.value;
    this.setState({vacancy: this.state.vacancy});
  },
  handlerChangeVacancyRequestReason: function (event) {
    this.state.vacancy.company_vacancy_request_reason_id = event.target.value;
    this.setState({vacancy: this.state.vacancy});
  },
  handlerChangeQty: function (event) {
    this.state.qty = event.target.value;
    this.setState({qty: this.state.qty});
  },
  handlerDelete: function (event) {
    var minimumQty = this.state.not_destroyable_vacancies_qty;
    if(minimumQty == 0) {
      // si no hay vacantes que no pueda destruir, o sea, si las puedo destruir todas
      // marco _destroy con true para que oculte el div
      this.state.vacancy._destroy = event.target.checked;

    }
    this.setState({vacancy: this.state.vacancy, qty: minimumQty});
  },
  getInitialState: function () {
    // Channel.subscribe('company/cencos', this.update_company_cencos);
    return {
      vacancy: this.props.vacancy,
      vacancy_ids: this.props.vacancy_ids,
      not_destroyable_vacancies_qty: this.props.not_destroyable_vacancies_qty || 0,
      qty: this.props.qty || 0,
      company_cencos: this.props.company_cencos ? this.props.company_cencos : []
    };
  },
  update_company_cencos: function (company_cencos) {
    this.setState({company_cencos: company_cencos});
    $(".js-select").trigger("chosen:updated");
  },
  renderNotDestroyableVacanciesQtyWarning: function() {
    if (this.state.not_destroyable_vacancies_qty > 0) {
      return (
        <div className="not-destroyable-vacancies-warning">
          <strong>
            {"Hay " + this.state.not_destroyable_vacancies_qty + " vacante"
             + (this.state.not_destroyable_vacancies_qty > 1 ? "s" : "")
             + " asignada"
             + (this.state.not_destroyable_vacancies_qty > 1 ? "s" : "")
             +", que no puede"
             + (this.state.not_destroyable_vacancies_qty > 1 ? "n" : "")
             +" eliminarse"}
          </strong>
        </div>
      )
    }
  },
  renderDeleteButton: function() {
    var button;
    var destroyable_vacancies_qty = this.state.qty - this.state.not_destroyable_vacancies_qty;
    button = (
      <div>
        <input type='checkbox' name={this.props.name + '[_destroy]'}
               onClick={this.handlerDelete}
               defaultChecked={this.state.vacancy._destroy}
               style={{marginRight: '5px'}}
        />
        <label>{"Eliminar " + destroyable_vacancies_qty + " vacantes"}</label>
      </div>
    );
    if(this.state.not_destroyable_vacancies_qty < this.state.qty) {
      return button;
    }
  },
  render: function () {
    var displayStyle = {};
    var isCencoRequired = true;

    if (this.state.vacancy._destroy) {
      isCencoRequired = false;

      if (this.state.qty === 0) {
        // sólo no muestro el form cuando la marqué para destruir
        // y cuando al cambiar el qty queda en 0
        displayStyle = {display: 'none'};
      }
    }


    return (
      <div className="create-course_vacancy-element" style={displayStyle}>
        <FieldWrapper errors={this.props.errors.course_id}>
          <FieldWrapper errors={this.props.errors.company_positions_cenco}>
            <div className="create-course_row">
              <label htmlFor={React.sanitizeName(this.props.name + '[cenco_id]')}
                     className="create-course_label">Centro de Costo</label>
              <select id={React.sanitizeName(this.props.name + '[cenco_id]')}
                      name={this.props.name + '[cenco_id]'}
                      defaultValue={this.state.vacancy.cenco_id}
                      onChange={this.handlerChangeCenco}
                      className='form_element __select js-select'
                      data-placeholder='Seleccione opción'
                      required={isCencoRequired}
              >
                <option value=''></option>
                {this.state.company_cencos.map(function (company_cenco, index){
                   return <OptionSelect value={company_cenco.id}
                                        text={company_cenco.name}
                                        key={'cenco_' + index} />;
                 })}
              </select>
            </div>
          </FieldWrapper>

          <FieldWrapper errors={this.props.errors.company_vacancy_request_reason_id}>
            <div className="create-course_row">
              <label htmlFor={React.sanitizeName(this.props.name + '[company_vacancy_request_reason_id]')}
                     className="create-course_label">Motivo de la solicitud</label>

              <select id={React.sanitizeName(this.props.name + '[company_vacancy_request_reason_id]')}
                      name={this.props.name + '[company_vacancy_request_reason_id]'}
                      defaultValue={this.state.vacancy.company_vacancy_request_reason_id}
                      onChange={this.handlerChangeVacancyRequestReason}
                      className='form_element __select js-select'
                      data-placeholder='Seleccione opción'
              >
                <option value=''></option>
                {this.props.vacancy_request_reasons.map(function (vacancy_request_reason, index){
                   return <OptionSelect value={vacancy_request_reason.id}
                                        text={vacancy_request_reason.name}
                                        key={'vacancy_request_reason_' + index} />;
                 })}
              </select>
            </div>
          </FieldWrapper>

          <FieldWrapper errors={this.props.errors.observations}>
            <div className="create-course_row">
              <label htmlFor={React.sanitizeName(this.props.name + '[observations]')}
                     className="create-course_label">Observaciones</label>
              <input className="create-course_input"
                     id={React.sanitizeName(this.props.name + '[observations]')}
                     name={this.props.name + '[observations]'}
                     value={this.state.vacancy.observations}
                     onChange={this.handlerChangeObservations} />
            </div>
          </FieldWrapper>

          <FieldWrapper errors={this.props.errors.qty}>
            <div className="create-course_row">
              <label htmlFor={React.sanitizeName(this.props.name + '[qty]')}
                     className="create-course_label">Cantidad de vacantes</label>
              <input className="create-course_input"
                     id={React.sanitizeName(this.props.name + '[qty]')}
                     name={this.props.name + '[qty]'}
                     value={this.state.qty}
                     onChange={this.handlerChangeQty} />
            </div>
          </FieldWrapper>

          <input className="create-course_input"
                 type="hidden"
                 id={React.sanitizeName(this.props.name + '[vacancy_ids]')}
                 name={this.props.name + '[vacancy_ids]'}
                 value={this.state.vacancy_ids}
          />

          <div className="create-course_row">
            {this.renderDeleteButton()}
            {this.renderNotDestroyableVacanciesQtyWarning()}
          </div>
        </FieldWrapper>
      </div>
    );
  }
});
