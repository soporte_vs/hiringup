var MultipleVacancyForm = React.createClass({
  handlerClick: function (event) {
    this.state.vacancies.push({
      object: {
        id: null,
        observations: "",
        company_positions_cenco_id: null,
        cenco_id: null,
        company_vacancy_request_reason_id: null,
        destroyable: true
      },
      errors: []});
    var new_grouped_vacancies = this.getGroupedVacancies(this.state.vacancies);
    this.setState({
      vacancies: this.state.vacancies,
      grouped_vacancies: new_grouped_vacancies
    });
  },
  getInitialState: function () {
    Channel.subscribe('company/cencos', this.update_company_cencos);
    var _grouped_vacancies = this.getGroupedVacancies(this.props.vacancies) || {};
    return {
      vacancies: this.props.vacancies,
      grouped_vacancies: _grouped_vacancies,
      company_cencos: this.props.company_cencos
    };
  },
  update_company_cencos: function (company_cencos) {
    this.setState({company_cencos: company_cencos});
  },
  applyChosen: function (argument) {
    $('.js-select').chosen({
      no_results_text: "No se encontró",
      allow_single_deselect: true
    });
  },
  componentDidMount: function(){
    this.applyChosen();
  },
  componentDidUpdate: function (argument) {
    this.applyChosen();
  },
  getGroupedVacancies: function (vacancies) {
    var _vacancies = vacancies || [];
    var grouped_vacancies = {};

    for(var i=0; i<_vacancies.length; i++) {
      var vacancy = _vacancies[i];
      var index = "z-" + i; // z- al principio para que quede siempre al final
      if(vacancy.object.company_positions_cenco_id && vacancy.object.company_vacancy_request_reason_id) {
        index = vacancy.object.company_positions_cenco_id + "-" + vacancy.object.company_vacancy_request_reason_id;
      }
      grouped_vacancies[index] = grouped_vacancies[index] || [];
      grouped_vacancies[index].push(vacancy);
    }
    return grouped_vacancies;
  },
  render: function () {
    var _name = this.props.name;
    var _company_cencos = this.state.company_cencos;
    var vacancy_request_reasons = this.props.vacancy_request_reasons;
    var _grouped_vacancies = this.state.grouped_vacancies;
    clase = 'create-course_vacancy-container';

    if (this.state.vacancies && this.state.vacancies.length > 0) {
      clase = 'create-course_vacancy-container __content';
    }

    return (
      <div className={clase}>
        <input type="button" name="add_vacancy" onClick={this.handlerClick} value="Añadir Vacante" className="create-course_vacancy __abs btn btn-primary" />

        {
          Object.keys(_grouped_vacancies).map(function (key, index) {
            var vacancy = _grouped_vacancies[key][0];
            var not_destroyable_vacancies_qty = 0;
            var vacancy_qty = _grouped_vacancies[key].length;
            var vacancy_ids = _grouped_vacancies[key].map(function(v) {
              if (v.object.destroyable !== undefined && v.object.destroyable === false) {
                not_destroyable_vacancies_qty += 1;
              }
              return v.object.id;
            });
            var __name = _name+'[vacancies_attributes]['+index+']';
            return <MultipliedVacancyForm key={__name} name={__name}
                                          vacancy={vacancy.object}
                                          errors={vacancy.errors}
                                          qty={vacancy_qty}
                                          not_destroyable_vacancies_qty={not_destroyable_vacancies_qty}
                                          vacancy_ids={vacancy_ids}
                                          company_cencos={_company_cencos}
                                          vacancy_request_reasons={vacancy_request_reasons} />;
          })
        }

      </div>
    );
  }
});
