//= require components/hiring_request/record/hit
//= require components/hiring_request/record/not_hit
//= require components/common/error_hit
var SearchkitProvider = Searchkit.SearchkitProvider;
var SearchBox = Searchkit.SearchBox;
var SelectedFilters = Searchkit.SelectedFilters;
var ResetFilters = Searchkit.ResetFilters;
var RefinementListFilter = Searchkit.RefinementListFilter;
var Hits = Searchkit.Hits;
var NoHits = Searchkit.NoHits;
var Pagination = Searchkit.Pagination;
var SortingSelector = Searchkit.SortingSelector;
var HitsStats = Searchkit.HitsStats;

var HRRStateItem = React.createClass({
  render: function() {
    switch(this.props.label){
      case ('active'): label = 'Activa'; break;
      case ('rejected'): label = 'Rechazada'; break;
      case ('accepted'): label = 'Aceptada'; break;
      default: label = 'Abierto'; break;
    }
    return (
      <div className={this.props.bemBlocks.option().state({selected: this.props.active}).mix(this.props.bemBlocks.container("item"))} onClick={this.props.onClick}>
        <input type="checkbox" data-qa="checkbox" readOnly="" checked={this.props.active} className={this.props.bemBlocks.option("checkbox")}/>
        <div className={this.props.bemBlocks.option("text")}>{label}</div>
        <div className={this.props.bemBlocks.option("count")}>{this.props.count}</div>
      </div>
    );
  }
});

var HRRBySystemItem = React.createClass({
  render: function() {
    var label = 'Sistema';
    if (this.props.label) {
      label = this.props.label;
    }
    return (
      <div className={this.props.bemBlocks.option().state({selected: this.props.active}).mix(this.props.bemBlocks.container("item"))} onClick={this.props.onClick}>
        <input type="checkbox" data-qa="checkbox" readOnly="" checked={this.props.active} className={this.props.bemBlocks.option("checkbox")}/>
        <div className={this.props.bemBlocks.option("text")}>{label}</div>
        <div className={this.props.bemBlocks.option("count")}>{this.props.count}</div>
      </div>
    );
  }
});

var HiringRequestRecord = React.createClass({
  render: function() {
    var facets = {
      'facets.view_more': 'Ver más',
      'facets.view_less': 'Ver menos',
      'facets.view_all': 'Ver todos'
    };
    var searchkit = HupeSearchkitRoutes.getHiringRequestRecordsSearchRoute();
    return (
      <SearchkitProvider searchkit={searchkit}>
        <div>
          <div className="col-md-3 margin-bottom-20-px">
            <div className="col-xs-12 __white-background __with_shadow" style={{padding: '20px'}}>
              <RefinementListFilter
                  id="request_reason"
                  title="Razones de solicitud"
                  field="request_reason.raw"
                  size={5}
                  translations={facets}
              />
              <br/>

              <RefinementListFilter
                  id="company"
                  title="Empresa"
                  field="company.raw"
                  size={5}
                  translations={facets}
              />
              <br/>

              <RefinementListFilter
                  id="business_unit"
                  title="Gerencia"
                  field="business_unit.raw"
                  size={5}
                  translations={facets}
              />
              <br/>

              <RefinementListFilter
                  id="cenco"
                  title="Centro de costos"
                  field="cenco.raw"
                  size={5}
                  translations={facets}
              />
              <br/>

              <RefinementListFilter
                  id="required_by"
                  title="Solicitantes"
                  field="required_by.raw"
                  size={5}
                  translations={facets}
                  itemComponent={HRRBySystemItem}
              />
              <br/>

              <RefinementListFilter
                id="created_by"
                title="Creadores"
                field="created_by.raw"
                size={5}
                translations={facets}
                itemComponent={HRRBySystemItem}
              />
              <br/>

              <RefinementListFilter
                id="positions"
                title="Cargos"
                field="position.raw"
                size={5}
                translations={facets}
              />
              <br/>

              <RefinementListFilter
                id="aasm_state"
                title="Estado de Solicitud"
                field="aasm_state.raw"
                size={5}
                translations={facets}
                itemComponent={HRRStateItem}
              />

              <ResetFilters translations={{'reset.clear_all': 'Limpiar filtros'}}/>
            </div>
          </div>

          <div className="col-md-9">
            <div className="col-xs-12">

              <div className='row panel __with_shadow' style={{padding: '10px'}}>
                <div className='col-xs-9'>
                </div>

                <div className='col-xs-3 text-right'>

                  <SortingSelector options={[
                    {label:"Fecha Reciente", field:"created_at", order:"desc"},
                    {label:"Fecha Antigua", field:"created_at", order:"asc"}
                  ]}/>
                </div>
              </div>

              <div className='row margin-top-10-px'>
                <SelectedFilters/>
              </div>

              <div className='row margin-top-10-px'>
                <Hits hitsPerPage={10} itemComponent={HRRhit}/>

                <NoHits
                  component={NoHitsHiringRequestRecordDisplay}
                  errorComponent={NoHitsErrorDisplay}/>

                <Pagination translations={
                  {
                  "pagination.previous":"Anterior",
                  "pagination.next":"Siguiente",
                  }
                }
                  showNumbers={true}  className='__white-background __with_shadow' style={{'padding': '10px'}}/>

              </div>
            </div>
          </div>
        </div>
      </SearchkitProvider>
    );
  }
});

