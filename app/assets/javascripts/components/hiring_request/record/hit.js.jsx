var HRRhit = React.createClass({
  drawState: function (aasm_state) {
    aasm_state_style = '';
    aasm_state_text = '';
    switch(aasm_state) {
      case 'active':
        aasm_state_style = 'label-primary';
        aasm_state_text = 'Activa';
        break;
      case 'accepted':
        aasm_state_style = 'label-success';
        aasm_state_text = 'Aceptada';
        break;
      case 'rejected':
        aasm_state_style = 'label-danger';
        aasm_state_text = 'Rechazada';
        break;
      default:
        aasm_state_style = 'label-default';
        aasm_state_text = 'Pendiente';
        break;
    }
    return(
      <span className={'label ' + aasm_state_style}>{aasm_state_text}</span>
    );
  },
  render: function () {
    request_tag = "collapseRequest-" + this.props.result._source.id
    return(
      <div className={"____course __progress-top-bar margin-bottom-10-px process_element-container clearfix" + this.props.result._source.health+ "-percent"}>
        <div className='row padding-bottom-10-px padding-top-10-px '>
          { /* Collapse */ }
          <div className='col-xs-12 __with_collapsible-icons'>
            <a data-toggle='collapse' className='__request collapsed'
               href={"#"+request_tag}
               aria-expanded='false'
               aria-controls={request_tag}>
              { /* Principal Content */ }
              <div className='media'>
                <div className="media-body">
                  <div className="row">
                    <div className="col-xs-10">
                      <h3 className="media-heading margin-bottom-10-px">
                        <strong>Solicitud de Personal N-{this.props.result._source.id}</strong>
                      </h3>
                    </div> 
                    <div className='col-xs-2'>
                      <div className='margin-top-10-px'>
                        {this.drawState(this.props.result._source.aasm_state)}
                      </div>
                    </div>
                  </div> 
                  <div className='row'>
                    <div className='col-xs-12'>
                      <p className='text-base  __date'>
                        <span className="glyphicon glyphicon-time" style={{marginRight: "5px"}} ></span>
                        <strong>Fecha de creación de la solicitud:&nbsp;</strong>
                        <span>{moment(this.props.result._source.created_at).format('LLL')}</span>
                      </p>
                      <p className='text-base __date'>
                        <span className="glyphicon glyphicon-calendar" style={{marginRight: "5px"}} ></span>
                        <strong>Fecha estimada de ingreso:&nbsp;</strong>
                        {moment(this.props.result._source.entry_date , "DD-MM-YYYY").format('LL')}
                      </p>
                    </div>
                  </div>
                </div>
                { /* Fin Media Body */ }
              </div>
              { /* Fin Media */ }
            </a>
          </div>
          { /* Fin Collapse */ }
        </div>

        { /* Collapse Content */ }
        <div className="row collapse" id={request_tag}>
          <div className="col-xs-12">
            <div className='row'>
              <div className="col-sm-6">
                <div className='row margin-bottom-10-px'>
                  <div className='col-xs-12'>
                    <p className='text-request'>
                      <span className="glyphicon glyphicon-info-sign" style={{marginRight: "5px"}} ></span>
                      <strong>Razón de la solicitud:&nbsp;</strong>
                      <br />
                      <span className='__textsearch'>
                        {this.props.result._source.request_reason}
                      </span>
                    </p>
                  </div>
                </div>

                <div className='row margin-bottom-10-px'>
                  <div className='col-xs-12'>
                    <p className='text-request'>
                      <span className="glyphicon glyphicon-home" style={{marginRight: "5px"}} ></span>
                      <strong>Empresa:&nbsp;</strong>
                      <br />
                      <span className='__textsearch'>
                        {this.props.result._source.company}
                      </span>
                    </p>
                  </div>
                </div>

                <div className='row margin-bottom-10-px'>
                  <div className='col-xs-12'>
                    <p className='text-request'>
                      <span className="glyphicon glyphicon-user" style={{marginRight: "5px"}} ></span>
                      <strong>Solicitado por:&nbsp;</strong>
                      <br />
                      <span className='__textsearch'>
                        {this.props.result._source.required_by ? this.props.result._source.required_by : 'Sistema'}
                      </span>
                    </p>
                  </div>
                </div>

                <div className='row margin-bottom-10-px'>
                  <div className='col-xs-12'>
                    <p className='text-request'>
                      <span className="glyphicon glyphicon-user" style={{marginRight: "5px"}} ></span>
                      <strong>Creado por:&nbsp;</strong>
                      <br />
                      <span className='__textsearch'>
                        {this.props.result._source.created_by ? this.props.result._source.created_by : 'Sistema'}
                      </span>
                    </p>
                  </div>
                </div>
              </div> 
              <div className="col-sm-6">
                <div className='row margin-bottom-10-px'>
                  <div className='col-xs-12'>
                    <p className='text-request'>
                      <span className="glyphicon glyphicon-briefcase" style={{marginRight: "5px"}} ></span>
                      <strong>Gerencia:&nbsp;</strong>
                      <br />
                      <span className='__textsearch'>
                        {this.props.result._source.business_unit}
                      </span>
                    </p>
                  </div>
                </div>

                <div className='row margin-bottom-10-px'>
                  <div className='col-xs-12'>
                    <p className='text-request'>
                      <span className="glyphicon glyphicon-briefcase" style={{marginRight: "5px"}} ></span>
                      <strong>Cargo:&nbsp;</strong>
                      <br />
                      <span className='__textsearch'>
                        {this.props.result._source.position}
                      </span>
                    </p>
                  </div>
                </div>

                <div className='row margin-bottom-10-px'>
                  <div className='col-xs-12'>
                    <p className='text-request'>
                      <span className="glyphicon glyphicon-briefcase" style={{marginRight: "5px"}} ></span>
                      <strong>Centro de costo:&nbsp;</strong>
                      <br />
                      <span className='__textsearch'>
                        {this.props.result._source.cenco}
                      </span>
                    </p>
                  </div>
                </div>

              </div>
            </div>
            <div className='row margin-bottom-10-px'>
              <div className='col-xs-6'>
                <p className='text-base __date'>
                  <span className="glyphicon glyphicon-ok-sign" style={{marginRight: "5px"}} ></span>
                  <strong>Total de vacantes:&nbsp;</strong>
                  {this.props.result._source.vacancies.total}
                </p>
              </div>

              <div className='col-xs-6'>
                <p className='text-base __date'>
                  <span className="glyphicon glyphicon-question-sign" style={{marginRight: "5px"}} ></span>
                  <strong>Vacantes asignadas:&nbsp;</strong>
                  {this.props.result._source.vacancies.assigned}
                </p>
              </div>
            </div>
            <div className="row">
              <div className="col-xs-12">
                <a target='_blank' href={'/app/hiring_request/records/' + this.props.result._source.id}>
                  <p>Ver Solicitud completa</p>
                </a>
              </div>
            </div>
          </div>
        </div>
        { /* End Collapse Content */ }

      </div>
    );
  },
});
