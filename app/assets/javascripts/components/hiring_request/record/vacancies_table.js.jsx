var VacancyTableRow = React.createClass({
  drawCourselink: function (){
    course = this.state.vacancy.course
    if(course){
      return (
        <a href={course.url}>{course.title}</a>
      )
    }
  },
  drawErrorButton: function () {
    vacancy = this.state.vacancy
    if(!$.isEmptyObject(vacancy.errors)){
      messages = '';
      $.each(vacancy.errors, function (key, value) {
        for (var i = value.length - 1; i >= 0; i--) {
          messages += '<li>' + value[i] + '</li>';
        };
      })
      messages = '<ul class="__force-text_break-word" style="padding-left: 15px">' + messages + '</ul>';
      return (
        <button type="button" className="btn btn-default btn-xs"
                data-container="body" data-toggle="popover"
                data-placement="left" data-content={messages}>
            <span className="glyphicon glyphicon-question-sign" aria-hidden="true"></span>
        </button>
      )
    }
  },
  handleCheckBox: function (event) {
    if (typeof(this.props.onChange) == 'function') {
      this.props.onChange(event);
    }
  },
  componentDidUpdate: function () {
    $('[data-toggle="popover"]').popover({
      trigger: 'hover',
      html: true
    });
  },
  getInitialState: function () {
    return { vacancy: this.props.vacancy }
  },
  render: function () {
    vacancy = this.state.vacancy
    style = $.isEmptyObject(vacancy.errors) ? '' : 'danger'
    return (
      <tr id={"vacancy-" + vacancy.id} className={style}>
        <td>
          <input type="checkbox" value={vacancy.id}
            onChange={this.handleCheckBox} checked={this.props.checked}/>
        </td>
        <td>{vacancy.id}</td>
        <td>{vacancy.created_at}</td>
        <td>{this.drawCourselink()}</td>
        <td>{this.drawErrorButton()}</td>
      </tr>
    )
  }
});


var VacanciesTable = React.createClass({
  updateVacancies: function (vacancies_updates) {
    for (var i = vacancies_updates.length - 1; i >= 0; i--) {
      vacancy = vacancies_updates[i]
      this.refs['vacancy_' + vacancy.id].setState({vacancy: vacancy});
    };
  },
  getInitialState: function () {
    return { vacancies_selected: [] }
  },
  getVacanciesSelected: function () {
    return this.state.vacancies_selected
  },
  handleCheckBoxMaster: function (event) {
    this.state.vacancies_selected = [];
    if(event.target.checked){
      this.state.vacancies_selected = this.props.vacancies.map(function (vacancy) {
        return vacancy.id.toString();
      });
    };
    this.setState({vacancies_selected: this.state.vacancies_selected})
  },
  handleCheckBoxSlave: function (event) {
    vacancies_selected = this.state.vacancies_selected
    if (event.target.checked){
      vacancies_selected.push(event.target.value)
    }else{
      vacancies_selected = $(vacancies_selected).not([event.target.value]).get();
    }
    this.setState({vacancies_selected: vacancies_selected});
  },
  drawFormAssignCourse: function () {
    if (this.state.vacancies_selected.length > 0){
      return (
        <SetterCourseVacancies
              name="course_id" options={this.props.courses}
              callbackParent={this.updateVacancies}
              getVacanciesSelected={this.getVacanciesSelected}
              auth_token={this.props.auth_token} />
      )
    }
  },
  drawFormAssignNewCourse: function () {
    if(this.state.vacancies_selected.length > 0) {
      return (
        <SetterNewCourseVacancies
          name="new_course" options={this.props.courses}
          callbackParent={this.updateVacancies}
          getVacanciesSelected={this.getVacanciesSelected}
          auth_token={this.props.auth_token}
          url_new_course={this.props.url_new_course}
        />
      )
    }
  },
  render: function () {
    _this = this;
    checked = this.state.vacancies_selected.length == this.props.vacancies.length ? true : false
    return (
      <div>
        <div className='clearfix margin-bottom-20-px'>
          <div className='col-sm-5'>
            {this.drawFormAssignCourse()}
          </div>
          <div className='col-sm-4'>
            {this.drawFormAssignNewCourse()}
          </div>
        </div>
        <table className="table table-responsive table-striped">
          <thead>
            <th>
              <input type="checkbox" onChange={this.handleCheckBoxMaster} checked={checked}/>
            </th>
            <th>#</th>
            <th>{this.props.locales.created_at}</th>
            <th>{this.props.locales.course_id}</th>
          </thead>

          <tbody>
            {this.props.vacancies.map(function (vacancy) {
              vacancy_key = 'vacancy_' + vacancy.id
              checked = _this.state.vacancies_selected.indexOf(vacancy.id.toString()) != -1 ? true : false
              vacancyProps = {
                key: vacancy_key,
                ref: vacancy_key,
                vacancy: vacancy,
                checked: checked,
                onChange: _this.handleCheckBoxSlave
              }
              return <VacancyTableRow {...vacancyProps} />
            })}
          </tbody>
        </table>
      </div>
    )
  }
});
