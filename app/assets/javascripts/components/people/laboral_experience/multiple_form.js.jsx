var MultipleLaboralExperienceForm = React.createClass({
  handlerClick: function (event) {
    this.state.laboral_experiences.push({object: {id: null, position: null, company: null, since_date: null, until_date: null}, errors: []});
    this.setState({laboral_experiences: this.state.laboral_experiences});
  },
  getInitialState: function () {
    return {
      laboral_experiences: this.props.laboral_experiences,
    }
  },
  render: function () {
    var _name = this.props.name;
    return (
      <div className='postulant_table-container'>
        <div className='row postulant_table-container-header'>
          <div className="col-sm-6">
            <h3>
              Información profesional
            </h3>
          </div>
          <div className="col-sm-6">
            <button type="button" onClick={this.handlerClick}
                    name="add_laboral_experience"
                    className='form_element __context-button btn btn-primary btn-inverted margin-top-10-px'>
              Agregar nueva experiencia laboral
            </button>
          </div>
        </div>
        <div className='table-responsive'>
          <table className='table table-bordered'>
            <thead>
              <tr className=''>
                <th className='' style={{minWidth: '200px'}}>Cargo</th>
                <th className='' style={{minWidth: '200px'}}>Empresa - Institución</th>
                <th className='' style={{minWidth: '200px'}}>Desde</th>
                <th className='' style={{minWidth: '200px'}}>Hasta</th>
                <th className='' style={{minWidth: '200px'}}>Descripción</th>
                <th className='' style={{minWidth: '200px'}}>Acciones</th>
              </tr>
            </thead>
            <tbody>
              {this.state.laboral_experiences.map(function (laboral_experience, index){
                __name = _name+'[laboral_experiences_attributes]['+index+']';
                return <LaboraExperienceForm  key={__name}
                                              name={__name}
                                              laboral_experience={laboral_experience.object}
                                              errors ={laboral_experience.errors} />
              })}
            </tbody>
          </table>
        </div>
      </div>
    )
  }
});
