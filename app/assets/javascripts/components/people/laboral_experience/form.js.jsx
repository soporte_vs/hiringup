
var LaboraExperienceForm = React.createClass({
  getInitialState: function () {
    return {
      laboral_experience: this.props.laboral_experience
    }
  },
  handlerChangePosition: function (event) {
    this.state.laboral_experience.position = event.target.value;
    this.setState({laboral_experience: this.state.laboral_experience});
  },
  handlerChangeCompany: function (event) {
    this.state.laboral_experience.company = event.target.value;
    this.setState({laboral_experience: this.state.laboral_experience});
  },
  handlerChangeSinceDate: function (event) {
    this.state.laboral_experience.since_date = event.target.value;
    this.setState({laboral_experience: this.state.laboral_experience});
  },
  handlerChangeUntilDate: function (event) {
    this.state.laboral_experience.until_date = event.target.value;
    this.setState({laboral_experience: this.state.laboral_experience});
  },
  handlerChangeDescription: function (event) {
    this.state.laboral_experience.description = event.target.value;
    this.setState({laboral_experience: this.state.laboral_experience});
  },
  handlerDelete: function (event) {
    this.state.laboral_experience.position = '';
    this.state.laboral_experience.company = '';
    this.state.laboral_experience.since_date = '';
    this.state.laboral_experience.until_date = '';
    this.state.laboral_experience._destroy = event.target.value;
    this.setState({laboral_experience: this.state.laboral_experience});
  },
  componentDidMount: function (event) {
    if (typeof $ === 'function') {
      $(ReactDOM.findDOMNode(this)).find('.js-reactpicker').datetimepicker({
        timepicker: false,
        format:'d/m/Y'
      });
    }
  },
  render: function () {
    var style;
    if (this.state.laboral_experience._destroy == 'on'){
      style = {display: 'none'}
    }
    return (
      <tr className='' style={style}>
        <td className=''>
          <FieldWrapper errors={this.props.errors.position} hide_message={ true } >
            <div className='col-xs-12 padding-top-10-px padding-bottom-10-px'>
              <input type='hidden' name={this.props.name + '[id]'}
                      value={this.state.laboral_experience.id}
                      className='form-control' />
              <input type='text' name={this.props.name + '[position]'}
                     value={this.state.laboral_experience.position}
                     onChange={this.handlerChangePosition}
                     className='form-control' />
            </div>
          </FieldWrapper>
        </td>
        <td className=''>
          <FieldWrapper errors={this.props.errors.company} hide_message={ true } >
            <div className='col-xs-12 padding-top-10-px padding-bottom-10-px'>
              <input type='text' name={this.props.name + '[company]'}
                     value={this.state.laboral_experience.company}
                     onChange={this.handlerChangeCompany}
                     className='form-control' />
            </div>
          </FieldWrapper>
        </td>
        <td className=''>
          <FieldWrapper errors={this.props.errors.since_date} hide_message={ true } >
            <div className='col-xs-12 padding-top-10-px padding-bottom-10-px'>
              <input type='text' name={this.props.name + '[since_date]'}
                     value={this.state.laboral_experience.since_date}
                     onChange={this.handlerChangeSinceDate}
                     className='form-control js-reactpicker' />
            </div>
          </FieldWrapper>
        </td>
        <td className=''>
          <div className='col-xs-12 padding-top-10-px padding-bottom-10-px'>
            <input type='text' name={this.props.name + '[until_date]'}
                   defaultValue={this.state.laboral_experience.until_date}
                   onChange={this.handlerChangeUntilDate}
                   className='form-control js-reactpicker' />
          </div>
        </td>
        <td className=''>
          <div className='col-xs-12 padding-top-10-px padding-bottom-10-px'>
            <textarea name={this.props.name + '[description]'}
                   value={this.state.laboral_experience.description}
                   onChange={this.handlerChangeDescription}
                   className='form-control' />
          </div>
        </td>
        <td className=''>
          <div className='col-xs-12 padding-top-10-px padding-bottom-10-px'>
            <input type='checkbox' name={this.props.name + '[_destroy]'}
                   onClick={this.handlerDelete}
                   defaultChecked={this.state.laboral_experience._destroy} />
            <label>&nbsp; Eliminar</label>
          </div>
        </td>
      </tr>
    )
  }
});
