var OptionSelect = React.createClass({
  render: function() {
    return (
      <option value={this.props.value}>{this.props.text}</option>
    );
  }
});

var DegreeForm = React.createClass({
  propTypes:{
    degree: React.PropTypes.shape({
      type: React.PropTypes.string,
      career_id: React.PropTypes.number,
      institution_id: React.PropTypes.number,
      condition: React.PropTypes.string,
      culmination_date: React.PropTypes.string,
      _destroy: React.PropTypes.string,
    }),

    careers: React.PropTypes.arrayOf(React.PropTypes.shape({
      id: React.PropTypes.number,
      name: React.PropTypes.string
    })),

    institutions: React.PropTypes.arrayOf(React.PropTypes.shape({
      id: React.PropTypes.number,
      name: React.PropTypes.string
    })),

    study_types: React.PropTypes.arrayOf(React.PropTypes.shape({
      id: React.PropTypes.string.isRequired,
      name: React.PropTypes.string.isRequired
    })).isRequired,

    conditions: React.PropTypes.arrayOf(React.PropTypes.string),
    types: React.PropTypes.arrayOf(React.PropTypes.array),
    institution_types: React.PropTypes.arrayOf(React.PropTypes.array)
  },
  handlerChangeType: function (event) {
    this.state.degree.object.type = event.target.value;
    this.setState({degree: this.state.degree});
  },
  handlerChangeCareer: function (event) {
    this.state.degree.object.career_id = event.target.value;
    this.setState({degree: this.state.degree});
  },
  handlerChangeInstitution: function (event) {
    this.state.degree.object.institution_id = event.target.value;
    this.setState({degree: this.state.degree});
  },
  handlerChangeInstitutionType: function (event) {
    target = event.target;
    var type = target.value;
    this.state.degree.object.institution_type = type;
    this.setState({degree: this.state.degree});
    this.reloadInstitutions(type);
  },
  handlerChangeCondition: function (event) {
    this.state.degree.object.condition = event.target.value;
    this.setState({degree: this.state.degree});
  },
  handlerChangeName: function (event) {
    this.state.degree.object.name = event.target.value;
    this.setState({degree: this.state.degree});
  },
  handlerChangeCulminationDate: function (event) {
    this.state.degree.object.culmination_date = event.target.value;
    this.setState({degree: this.state.degree});
  },
  handlerDelete: function (event) {
    this.state.degree.object._destroy = event.target.value;
    this.setState({degree: this.state.degree});
  },
  reloadInstitutions: function(type) {
    _this = this;

    // voy a buscar la lista de institutions del tipo dado, que no sean custom
    EducationInstitution.getBy({type: type}).then(function(data){
      _this.setState({institutions: data});
    });
  },
  getInitialState: function () {
    var _institutions = this.props.institutions;
    _degree = this.props.degree;

    if(_degree.institution_id) { // si tiene institution cargada de antes

      // cargo el institution_type en el degree
      _institution = this.props.institutions.filter(function(institution) { return institution.id == _degree.institution_id; } );

      if(_institution.length > 0) {
        institution = _institution[0];
        _degree.institution_type = institution.type;
      }

      // además de cargar el institution_type al degree, limito las instituciones mostradas a las de ese tipo
      _institutions = this.props.institutions.filter(function(institution) { return institution.type == _degree.institution_type; } );
    }

    return {
      degree: _degree,
      institutions: _institutions,
      careers: this.props.careers,
      conditions: this.props.conditions,
      types: this.props.types,
      institution_types: this.props.institution_types
    };
  },
  componentDidMount: function (event) {
    if (typeof $ === 'function') {
      $(ReactDOM.findDOMNode(this)).find('.js-reactpicker').datetimepicker({
        timepicker: false,
        format:'d/m/Y'
      });
    }
  },
  render: function () {
    var input_degree_id, button_delete_degree, style;
    if (this.state.degree.object._destroy){
      style = {display: 'none'};
    }
    if (this.state.degree.object.id) {
      input_degree_id = <input type='hidden' value={this.state.degree.object.id} name={this.props.name + '[id]'} />
      button_delete_degree = (
        <div>
          <br />
          <input type='checkbox' name={this.props.name + '[_destroy]'} onClick={this.handlerDelete} defaultChecked={this.state.degree.object._destroy} />
          <label>&nbsp;Eliminar</label>
        </div>
      );
    }
    return (
      <tr className=''  style={style}>
        <td className=''>
          <FieldWrapper errors={this.props.errors.type} hide_message={ true } >
            <div className='col-xs-12 padding-top-10-px padding-bottom-10-px'>
              <select value={this.state.degree.object.type} name={this.props.name + '[type]'} onChange={this.handlerChangeType} className='form-control'>
                <option value=''></option>
                {this.state.types.map(function (type, index) {
                  return <OptionSelect value={type[0]} text={type[1]}  key={'type_' + index} />
                })}
              </select>
            </div>
          </FieldWrapper>
        </td>
        <td className=''>
          <FieldWrapper errors={this.props.errors.name} hide_message={ true } >
            <div className='col-xs-12 padding-top-10-px padding-bottom-10-px'>
              <input value={this.state.degree.object.name} type='text' name={this.props.name + '[name]'} onChange={this.handlerChangeName}
              className="form-control"/>
            </div>
          </FieldWrapper>
        </td>

        <td className=''>
          <div className='col-xs-12 padding-top-10-px padding-bottom-10-px'>
            <select value={this.state.degree.object.career_id} name={this.props.name + '[career_id]'} onChange={this.handlerChangeCareer} className='form-control'>
              <option value=''></option>
              {this.state.careers.map(function (career, index) {
                return <OptionSelect value={career.id} text={career.name}  key={'career_' + index} />;
              })}
            </select>
          </div>
        </td>
        <td className=''>
          <div className='col-xs-6 padding-top-10-px padding-bottom-10-px'>
            <select value={this.state.degree.object.institution_type} onChange={this.handlerChangeInstitutionType} className='form-control'>
              <option value=''></option>
              {this.state.institution_types.map(function (institution_type, index) {
                 return <OptionSelect value={institution_type[1]} text={institution_type[0]}  key={'institution_type_' + index} />;
               })}
            </select>
          </div>

          <div className='col-xs-6 padding-top-10-px padding-bottom-10-px'>
            <select value={this.state.degree.object.institution_id} name={this.props.name + '[institution_id]'} onChange={this.handlerChangeInstitution} className='form-control'>
              <option value=''></option>
              {this.state.institutions.map(function (institution, index) {
                return <OptionSelect value={institution.id} text={institution.name}  key={'institution_' + index} />;
              })}
            </select>
          </div>
        </td>


        <td className='' >
          <FieldWrapper errors={this.props.errors.condition} hide_message={ true } >
            <div className='col-xs-12 padding-top-10-px padding-bottom-10-px'>
              <select value={this.state.degree.object.condition} name={this.props.name + '[condition]'} onChange={this.handlerChangeCondition} className='form-control'>
                <option value=''></option>
                {this.state.conditions.map(function (condition, index) {
                  return <OptionSelect value={condition} text={condition}  key={'condition_' + index} />
                })}
              </select>
            </div>
          </FieldWrapper>
        </td>

        <td className='' >
          <FieldWrapper errors={this.props.errors.culmination_date} hide_message={ true } >
            <div className='col-xs-12 padding-top-10-px padding-bottom-10-px'>
              <input value={this.state.degree.object.culmination_date} type='text' name={this.props.name + '[culmination_date]'} onChange={this.handlerChangeCulminationDate}
              className='form-control js-reactpicker'/>
            </div>
          </FieldWrapper>
        </td>

        <td className='' >
          <div className='col-xs-12 padding-top-10-px padding-bottom-10-px'>
            <input type='checkbox' name={this.props.name + '[_destroy]'}
                   onClick={this.handlerDelete}
                   defaultChecked={this.state.degree._destroy} />
            <label>&nbsp; Eliminar</label>
            {input_degree_id}
          </div>
        </td>
      </tr>
    )
  }
});
