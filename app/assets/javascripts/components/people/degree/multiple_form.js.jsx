var MultipleDegreeForm = React.createClass({
  propTypes:{
    degrees: React.PropTypes.arrayOf(React.PropTypes.shape({
      institution_id: React.PropTypes.number.isRequired,
      condition: React.PropTypes.string.isRequired,
      culmination_date: React.PropTypes.string.isRequired,
      _destroy: React.PropTypes.string,
    })),

    careers: React.PropTypes.arrayOf(React.PropTypes.shape({
      id: React.PropTypes.number.isRequired,
      name: React.PropTypes.string.isRequired
    })).isRequired,

    institutions: React.PropTypes.arrayOf(React.PropTypes.shape({
      id: React.PropTypes.number.isRequired,
      name: React.PropTypes.string.isRequired
    })).isRequired,

    conditions: React.PropTypes.arrayOf(React.PropTypes.string),
    types: React.PropTypes.arrayOf(React.PropTypes.array),
    institution_types: React.PropTypes.arrayOf(React.PropTypes.array)
  },
  handlerClick: function (event) {
    this.state.degrees.push({object: {type: null, career_id: null, condition: null, culmination_date: null, institution_id: null}, errors: []});
    this.setState({degrees: this.state.degrees});
  },
  getInitialState: function () {
    return {
      degrees: this.props.degrees
    }
  },
  render: function () {
    var _institutions = this.props.institutions,
    _careers = this.props.careers,
    _conditions = this.props.conditions,
    _types = this.props.types,
    _institution_types = this.props.institution_types,
    _name = this.props.name;

    return (
      <div className='postulant_table-container'>
        <div className='row postulant_table-container-header'>
          <div className="col-sm-6">
            <h3>
              Antecedentes académicos
            </h3>
          </div>
          <div className="col-sm-6">
            <button type="button" onClick={this.handlerClick} name="add_degree" className='form_element __context-button btn btn-primary btn-inverted margin-top-10-px'>Agregar nuevo antecedente académico</button>
          </div>
        </div>
        <div className='postulant_table-container-body table-responsive'>
          <table className='table table-bordered'>
            <thead>
              <tr className=''>
                <th className='' style={{minWidth: '200px'}}>Tipo</th>
                <th className='' style={{minWidth: '200px'}}>Título</th>
                <th className='' style={{minWidth: '200px'}}>Área de estudio</th>
                <th className='' style={{minWidth: '200px'}}>Institución</th>
                <th className='' style={{minWidth: '200px'}}>Estado</th>
                <th className='' style={{minWidth: '200px'}}>Fecha de término</th>
                <th className='' style={{minWidth: '200px'}}>Acciones</th>
              </tr>
            </thead>
            <tbody>
              {this.state.degrees.map(function (degree, index){
                 __name = _name+'[degrees_attributes]['+index+']';
                 return <DegreeForm key={__name}
                                    name={__name}
                                    degree={degree}
                                    institutions={_institutions}
                                    careers={_careers}
                                    conditions={_conditions}
                                    types={_types}
                                    institution_types={_institution_types}
                                    errors={degree.errors} />;
              })}
            </tbody>
          </table>
        </div>
      </div>
    )
  }
});
