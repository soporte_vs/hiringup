var OptionSelect = React.createClass({
  render: function() {
    return (
      <option value={this.props.value}>{this.props.text}</option>
    );
  }
});

var IdentificationDocumentInput = React.createClass({
  getInitialState: function () {
    // set regex from props
    var _this = this;
    var _validation_regex = this.getValidationRegex(this.props.identification_document_types, this.props.identification_document_type.id);
    return {
      countries: this.props.countries,
      identification_document_number: this.props.identification_document_number,
      identification_document_types: this.props.identification_document_types || [],
      identification_document_type_country_id: this.props.identification_document_type.country_id,
      identification_document_type_id: this.props.identification_document_type.id,
      identification_document_validation_regex: _validation_regex,
      identification_document_number_errors: []
    };
  },
  getValidationRegex: function(identification_document_types, identification_document_type_id) {
    var _selectedType = identification_document_types.filter(function(type){ return type.id == identification_document_type_id; });
    var _validationRegex = "";
    if(_selectedType.length == 1) {
      var selectedType = _selectedType[0];
      if(selectedType.validation_regex) {
        _validationRegex = selectedType.validation_regex;
      }
    }
    return _validationRegex;
  },
  handleChangeIdentificationDocumentNumber: function(event) {
    var target = event.target;
    var number = target.value;
    this.setState({identification_document_number: number}, function(data){
      this.checkIsNumberValid(number);
    });
  },
  handleChangeCountryId: function(event) {
    var target = event.target;
    var country_id = target.value;
    this.setState({identification_document_type_country_id: country_id}, function(data){
      this.checkIsNumberValid(this.state.identification_document_number);
    });
    this.reloadIdentificationDocumentTypes(country_id);
  },
  handleChangeIdentificationDocumentId: function(event) {
    var target = event.target;
    this.setState({identification_document_type_id: target.value});

    // set regex from selected type
    var validationRegex = this.getValidationRegex(this.state.identification_document_types, target.value);
    this.setState({identification_document_validation_regex: validationRegex}, function(data){
      this.checkIsNumberValid(this.state.identification_document_number);
    });
  },
  checkIsNumberValid: function(number) {
    var validNumber = this.isNumberValid(number);
    var errors = [];
    if(validNumber) {
      errors = [];
    } else {
      if(number.length > 8){
        errors = ['Formato no válido'];
      }
    }
    this.setState({identification_document_number_errors: errors});
  },
  isNumberValid: function(number) {
    var validNumber = true;
    if(this.state.identification_document_validation_regex) {
      var re = new RegExp(this.state.identification_document_validation_regex);
      if(number) {
        validNumber = re.test(number);
      }
    }
    return validNumber;
  },
  reloadIdentificationDocumentTypes: function(country_id) {
    _this = this;

    if(country_id) {
      PeopleIdentificationDocumentType.getBy({country_id: country_id}).then(function(data){
        _this.setState({identification_document_types: data});
      });
    } else {
      this.setState({identification_document_types: []});
    }
  },
  render: function() {
    return (
      <div className="form-horizontal">
        <div className="col-xs-12 margin-top-20-px">
          <div className="form-group">
            <label htmlFor="identification_document_type_country_id" className="col-sm-2 control-label">
              País
            </label>
            <div className="col-sm-10">
              <select value={this.state.identification_document_type_country_id}
                      name={this.props.name + '[identification_document_type][country_id]'}
                      id={'identification_document_type_country_id'}
                      onChange={this.handleChangeCountryId}
                      className='form-control'>
                <option value=''></option>
                {this.state.countries.map(function (country, index) {
                   return <OptionSelect value={country.id} text={country.name}  key={'country_' + index} />;
                 })}
              </select>
            </div>
          </div>
          <div className="form-group">
            <label className="col-sm-2 control-label" htmlFor="identification_document_type_id">
              Tipo
            </label>
            <div className="col-sm-10">
              <select value={this.state.identification_document_type_id}
                      name={this.props.name + '[identification_document_type_id]'}
                      id={'identification_document_type_id'}
                      onChange={this.handleChangeIdentificationDocumentId}
                      className='form-control'>
                <option value=''></option>
                {this.state.identification_document_types.map(function (identification_document_type, index) {
                   return <OptionSelect value={identification_document_type.id} text={identification_document_type.name}  key={'identification_document_type' + index} />;
                 })}
              </select>
            </div>
          </div>
          <FieldWrapper errors={this.state.identification_document_number_errors}>
            <div className="form-group">
              <label className="col-sm-2 control-label" htmlFor="identification_document_number">
                Número de documento
              </label>
              <div className="col-sm-10">
                <input value={this.state.identification_document_number}
                       type='text'
                       name={this.props.name + '[identification_document_number]'}
                       id={'identification_document_number'}
                       onChange={this.handleChangeIdentificationDocumentNumber}
                       autoComplete="off"
                       className="form-control"/>
              </div>
              <div className="col-sm-10 field_info">Formato del RUT 11111111-1</div>
            </div>
          </FieldWrapper>
        </div>
      </div>
    );
  }
});
