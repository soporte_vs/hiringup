var EventBox = React.createClass({
  getInitialState: function() {
    return {};
  },
  callBackEdit: function (response) {
    if (typeof this.props.callBackActions == 'function'){
      this.props.callBackActions(response)
    }else{
      event = response[0].calendar_event;
      this.setProps({event: event});
    }
  },
  render: function() {
    event = this.props.event;
    return(
      <div className="event-box">
        <div className="row">
          <div className="col-sm-8">
            <EventShow
                course_id={this.props.course_id}
                event={event}
                callBackActions={this.callBackEdit}
            />
          </div>
          <div className="col-sm-2">
            <EventMarkWillAttend
                resourceable_id={this.props.resourceable_id}
                resourceable_type={this.props.resourceable_type}
                will_attend={this.props.will_attend}
                event={event}
            />
          </div>
          <div className="col-sm-2">
            <EventMarkAttended
                resourceable_id={this.props.resourceable_id}
                resourceable_type={this.props.resourceable_type}
                attended={this.props.attended}
                event={event}
            />
          </div>
        </div>
      </div>
    );
  }
});
