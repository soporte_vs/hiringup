var EventMarkAttended = React.createClass({
  getInitialState: function() {
    return {
      event: {},
      resourceable_id: '',
      resourceable_type: '',
      attended: false
    };
  },
  componentWillMount: function() {
    var _event;
    if(typeof(this.props.event) == "string") {
      _event = JSON.parse(this.props.event);
    } else {
      _event = this.props.event;
    }
    this.setState({event: _event, attended: this.props.attended});
  },
  markAttended: function(attended) {
    // si no viene attended, es true por defecto
    if (attended === undefined) {
      attended = true;
    } else {
      // si es cualquier otra cosa que no sea true, es false
      if (attended !== true) {
        attended = false;
      }
    }

    confirm_question = attended === true ? "¿Está seguro que desea marcar que asistió?" : "¿Está seguro que desea marcar que no asistió?";
    if(confirm(confirm_question)) {
      _this = this;
      event_id = this.state.event.id;
      resourceable_id = this.props.resourceable_id;
      resourceable_type = this.props.resourceable_type;
      Calendar.markAttended(event_id, resourceable_id, resourceable_type, attended).then(function(event_resourceable){
        if(event_resourceable.attended === true) {
          _this.setState({attended: true});
        } else if(event_resourceable.attended === false) {
          _this.setState({attended: false});
        } else {
          // error
        }
      });
    } else {
      return false;
    }
  },
  setAttended: function () {
    return this.markAttended(true);
  },
  setUnattended: function () {
    return this.markAttended(false);
  },
  drawAttendedButtonContent: function() {
    text = this.state.attended === true ? "Asistió" : "Marcar que asistió";
    icon_class = this.state.attended === true ? 'glyphicon glyphicon-ok-sign' : 'glyphicon glyphicon-info-sign';
    return(
      <span>
        <span className={icon_class} style={{marginRight: "5px"}}></span>
        {text}
      </span>
    );
  },
  render: function() {
    event = this.state.event;
    btn_class = this.state.attended === true ? 'btn-success' : 'btn-default';
    return(
      <div>
        <div className="radio">
          <label>
            <input type="radio"
                   name={"attended_event-" + this.state.event.id}
                   value={true}
                   checked={this.state.attended === true}
                   onChange={this.setAttended}
            />
            Asistió
          </label>
        </div>
        <div className="radio">
          <label>
            <input type="radio"
                   name={"attended_event-" + this.state.event.id}
                   value={false}
                   checked={this.state.attended === false}
                   onChange={this.setUnattended}
            />
            No asistió
          </label>
        </div>
      </div>
    );
  }
});
