var MapShow = React.createClass({
  initGoogleMaps: function () {
    var center = {lat: this.props.lat, lng: this.props.lng};
    var map = new google.maps.Map(document.getElementById('map'), {
      center: center,
      zoom: 14,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      disableDefaultUI: false,
      draggable: true,
      scrollwheel: true
    });
    return map;
  },
  componentDidMount: function() {
    map = this.initGoogleMaps();
  },
  render: function(){
    return(
      <div className='modal-body'>
        <div id='map' style={{height: '400px'}}></div>
      </div>
    );
  }
});
