var EventMarkWillAttend = React.createClass({
  getInitialState: function() {
    return {
      event: {},
      resourceable_id: '',
      resourceable_type: '',
      will_attend: null
    };
  },
  componentWillMount: function() {
    var _event;
    if(typeof(this.props.event) == "string") {
      _event = JSON.parse(this.props.event);
    } else {
      _event = this.props.event;
    }
    this.setState({event: _event, will_attend: this.props.will_attend});
  },
  markWillAttend: function(will_attend) {
    // si no viene will_attend, es null por defecto
    if (will_attend === undefined) {
      will_attend = null;
    } else {
      // si es cualquier otra cosa que no sea true, es false
      if (will_attend !== true) {
        will_attend = false;
      }
    }

    confirm_question = will_attend === true ? "¿Está seguro que desea marcar que asistirá?" : "¿Está seguro que desea marcar que no asistirá?";
    if(confirm(confirm_question)) {
      _this = this;
      event_id = this.state.event.id;
      resourceable_id = this.props.resourceable_id;
      resourceable_type = this.props.resourceable_type;
      Calendar.markWillAttend(event_id, resourceable_id, resourceable_type, will_attend).then(function(event_resourceable){
        if(event_resourceable.will_attend === true) {
          _this.setState({will_attend: true});
        } else if(event_resourceable.will_attend === false) {
          _this.setState({will_attend: false});
        } else {
          // error
        }
      });
    } else {
      return false;
    }
  },
  setWillAttend: function () {
    return this.markWillAttend(true);
  },
  setWontAttend: function () {
    return this.markWillAttend(false);
  },
  drawWillAttendButtonContent: function() {
    text = this.state.will_attend === true ? "Asistirá" : "Marcar que asistirá";
    icon_class = this.state.will_attend === true ? 'glyphicon glyphicon-ok-sign' : 'glyphicon glyphicon-info-sign';
    return(
      <span>
        <span className={icon_class} style={{marginRight: "5px"}}></span>
        {text}
      </span>
    );
  },
  render: function() {
    event = this.state.event;
    btn_class = this.state.will_attend === true ? 'btn-success' : 'btn-default';
    return(
      <div>
        <div className="radio">
          <label>
            <input type="radio"
                   name={"will_attend_event-" + this.state.event.id}
                   value={true}
                   checked={this.state.will_attend === true}
                   onChange={this.setWillAttend}
            />
            Asistirá
          </label>
        </div>
        <div className="radio">
          <label>
            <input type="radio"
                   name={"will_attend_event-" + this.state.event.id}
                   value={false}
                   checked={this.state.will_attend === false}
                   onChange={this.setWontAttend}
            />
            No asistirá
          </label>
        </div>
      </div>
    );
  }
});
