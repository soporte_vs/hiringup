var AddressMapShow = React.createClass({
  showMap: function () {
    modal = document.getElementById('modal');
    modal_content = modal.getElementsByClassName('modal-content')[0];
    latLng = { lat: this.props.address.lat, lng: this.props.address.lng };
    map_show = React.createElement(MapShow, latLng);

    ReactDOM.render(map_show, modal_content);
    var marker = new google.maps.Marker({});
    $('#modal').modal().on('shown.bs.modal', function () {
      google.maps.event.trigger(map, "resize");
      map.setCenter(latLng);
      marker.setMap(map);
      marker.setPosition(latLng);
    });
  },
  render: function(){
    return(
      <span>
        <a style={{cursor: 'pointer'}} onClick={this.showMap}>{this.props.address.name}</a>
      </span>
    );
  }
});
