var EventShow = React.createClass({
  contextTypes: {
    applicants: React.PropTypes.array
  },
  editForm: function () {
    starts_at = moment(this.props.event.starts_at);
    ends_at = this.props.event.ends_at ? moment(this.props.event.ends_at) : null;
    address = this.props.event.address;
    address_name = address.name;
    address_lat = address.lat;
    address_lng = address.lng;
    observations = this.props.event.observations;
    manager_id = this.props.event.manager_id;
    manager_email = this.props.event.manager_email;
    manager_name = this.props.event.manager_name;
    guests = this.props.event.guests.map(function (guest) {
      return { id: guest.id, name: guest.name, email: guest.email};
    });
    event = {
      id: this.props.event.id,
      starts_at:  starts_at,
      ends_at: ends_at,
      address: { name: address_name, lat: address_lat, lng: address_lng},
      observations: observations,
      guests: guests,
      manager_id: manager_id,
      manager_email: manager_email,
      manager_name: manager_name,
      enrollment_ids: this.props.event.enrollment_ids,
      applicant_ids: this.props.event.applicant_ids,
    };

    modal = document.getElementById('modal');
    modal_content = modal.getElementsByClassName('modal-content')[0];

    if (this.props.course_id){
      event_form = React.createElement(StagesEventForm, {
        course_id: this.props.course_id,
        onAddAppointment: this.props.callBackActions,
        event: event,
      });
    }else{
      event_form = React.createElement(ApplicantsEventForm, {
        course_id: this.props.course_id,
        onAddAppointment: this.props.callBackActions,
        applicants: this.context.applicants,
        applicant_selected_ids: event.applicant_ids,
        event: event,
      });
    }

    ReactDOM.render(event_form, modal_content);
    $('#modal').modal().on('shown.bs.modal', function () {
      $('.chosen').chosen();
      google.maps.event.trigger(map, "resize");
      map.setCenter({lat: address.lat, lng: address.lng});
    });
  },
  render: function(){
    return(
      <div>
        <span>
          Tiene un evento agendado para el dia <strong>{moment(this.props.event.starts_at).format('LLLL')}</strong>, en&nbsp;
      <AddressMapShow  address={this.props.event.address}/>
      &nbsp;con&nbsp;
      <strong>{this.props.event.manager_name + ' (' + this.props.event.manager_email + ')'}</strong>
      <small>&nbsp;<a style={{cursor: 'pointer'}} onClick={this.editForm}>(editar)</a></small>
        </span>
      </div>
    );
  }
});
