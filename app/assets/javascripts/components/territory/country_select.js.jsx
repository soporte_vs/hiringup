var CountryOption = React.createClass({
  render: function(){
    return <option value={this.props.value}>{this.props.text}</option>
  }
})

var CountrySelect = React.createClass({
  getCountries: function () {
    var _this = this;
    reqwest({
      url: '/app/territory/countries.json',
      success: function(countries){
        var selected = '';
        for (var i = 0; i < countries.length; i++) {
          // Compare only value NOT use ===
          // Because is posible that web service return id as integer
          // and selected value is a string
          if (countries[i].id == _this.props.selected){
            selected = _this.props.selected;
            break;
          }
        };
        _this.setState({countries: countries, value: selected});
      },
      error: function (err, data) {
        _this.setState({countries: [], value: ''});
      },
      complete: function(){
        _this.handleChange({target: {value: _this.state.value}})
      }
    })
  },
  handleChange: function (event) {
    this.setState({value: event.target.value});
    typeof this.props.onChange === 'function' ? this.props.onChange(event) : null;
  },
  componentDidMount: function () {
    $('.chosen-country').chosen({
      no_results_text: "No se encontró",
      allow_single_deselect: true
    }).change(function (event) {
      this.handleChange(event);
    }.bind(this));
    this.getCountries();
  },
  componentDidUpdate: function () {
    $(".chosen-country").trigger("chosen:updated");
  },
  getInitialState: function () {
    return { countries: [], value: this.props.selected }
  },
  render: function(){
    if (this.props.context === 'scaffold') {
      return (
        <div className='form_row'>
          <label htmlFor={React.sanitizeName(this.props.name)} className='scaffold_label'>País</label>
          <select id={React.sanitizeName(this.props.name)} value={this.state.value} name={this.props.name} onChange={this.handleChange}
                  className='form_element __select scaffold_select chosen-country' data-placeholder='Seleccione una opción'>
            <option value=''></option>
            {this.state.countries.map(function(country){
              return <CountryOption key={country.id} value={country.id} text={country.name} />
            })}
          </select>
        </div>
      )
    } else {
      return (
        <div className='postulant_inner-row'>
          <label htmlFor={React.sanitizeName(this.props.name)} className='postulant_inner-row_key'>País</label>
          <div className='postulant_inner-row_value'>
            <select id={React.sanitizeName(this.props.name)} value={this.state.value} name={this.props.name} onChange={this.handleChange}
                    className='form_element __select chosen-country' data-placeholder='Seleccione una opción' required={this.props.required}>
              <option value=''></option>
              {this.state.countries.map(function(country){
                return <CountryOption key={country.id} value={country.id} text={country.name} />
              })}
            </select>
          </div>
        </div>
      )
    }
  }
});
