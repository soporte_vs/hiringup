
var CountryStateCitySelect = React.createClass({
  handlerChangeCountry: function (event) {
    try{
      // this._reactInternalInstance._renderedComponent._renderedComponent._renderedChildren['.1']._instance.getStatesByCountry(event.target.value);
      this.refs.state_select.getStatesByCountry(event.target.value);
    }catch(err){ console.warn(err) }
    typeof this.props.country_select.onChange === 'function' ? this.props.country_select.onChange(event) : null
  },
  handlerChangeState: function (event) {
    try{
      this.refs.city_select.getCitiesByState(event.target.value);
    }catch(err){ console.warn(err) }
    typeof this.props.state_select.onChange === 'function' ? this.props.state_select.onChange(event) : null
  },
  handlerChangeCity: function (event) {
    typeof this.props.city_select.onChange === 'function' ? this.props.city_select.onChange(event) : null
  },
  render: function () {
    var countrySelectProps = {
          name: this.props.country_select.name,
          selected: this.props.country_select.selected,
          context: this.props.country_select.context,
          onChange: this.handlerChangeCountry,
          required: this.props.country_select.required
        },
        stateSelectProps = {
          name: this.props.state_select.name,
          selected: this.props.state_select.selected,
          context: this.props.state_select.context,
          onChange: this.handlerChangeState,
          required: this.props.state_select.required
        },
        citySelectProps = {
          name: this.props.city_select.name,
          selected: this.props.city_select.selected,
          errors: this.props.city_select.errors,
          context: this.props.city_select.context,
          onChange: this.handlerChangeCity,
          required: this.props.city_select.required
        }
    return (
      <div>
        <CountrySelect {...countrySelectProps}/>
        <StateSelect ref='state_select' {...stateSelectProps}/>
        <CitySelect ref='city_select' {...citySelectProps}/>
      </div>
    )
  }
});

