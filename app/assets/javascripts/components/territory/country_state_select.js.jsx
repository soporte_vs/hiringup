
var CountryStateSelect = React.createClass({
  handlerChangeCountry: function(event){
    try{
      this.refs.state_select.getStatesByCountry(event.target.value);
    }catch(err){ console.warn(err) }
    typeof this.props.country_select.onChange === 'function' ? this.props.country_select.onChange(event) : null
  },
  handlerChangeState: function(event) {
    typeof this.props.state_select.onChange === 'function' ? this.props.state_select.onChange(event) : null
  },
  render: function () {
    var countrySelectProps = {
      name: this.props.country_select.name,
      selected: this.props.country_select.selected,
      context: this.props.country_select.context,
      onChange: this.handlerChangeCountry,
      required: this.props.country_select.required
    },
    stateSelectProps = {
      name: this.props.state_select.name,
      selected: this.props.state_select.selected,
      context: this.props.state_select.context,
      onChange:this.handlerChangeState,
      required: this.props.state_select.required
    };
    return (
      <div>
        <CountrySelect {...countrySelectProps} />
        <StateSelect ref='state_select' {...stateSelectProps} />
      </div>
    )
  }
});

