var StateCitySelect = React.createClass({
  handlerChangeState: function(event){
    try{
      this.refs.city_select.getCitiesByState(event.target.value);
    }catch(err){ console.warn(err) }
    typeof this.props.state_select.onChange === 'function' ? this.props.country_select.onChange(event) : null
  },
  handlerChangeCity: function(event) {
    typeof this.props.state_select.onChange === 'function' ? this.props.state_select.onChange(event) : null
  },
  render: function () {
    var stateSelectProps = {
      name: this.props.state_select.name,
      selected: this.props.state_select.selected,
      context: this.props.state_select.context,
      country_id: this.props.state_select.country_id,
      onChange: this.handlerChangeState,
      required: this.props.state_select.required
    },
    citySelectProps = {
      name: this.props.city_select.name,
      selected: this.props.city_select.selected,
      context: this.props.city_select.context,
      onChange:this.handlerChangeCity,
      required: this.props.city_select.required
    };
    return (
      <div>
        <StateSelect {...stateSelectProps} />
        <CitySelect ref='city_select' {...citySelectProps} />
      </div>
    )
  }
});
