var StateOnlySelect = React.createClass({
  render: function () {
    var stateSelectProps = {
      name: this.props.state_select.name,
      selected: this.props.state_select.selected,
      context: this.props.state_select.context,
      country_id: this.props.state_select.country_id,
    };
    return (
      <div>
        <StateSelect {...stateSelectProps} />
      </div>
    )
  }
});

