var OptionSelect = React.createClass({
  render: function() {
    return (
      <option value={this.props.value}>{this.props.text}</option>
    )
  }
});

var CitySelect = React.createClass({
  getCitiesByState: function (state_id) {
    var _this = this;
    // if state_id is undefined or empty or null , dont do ajax reqwest
    if(state_id)
      reqwest({
        url: '/app/territory/states/'.concat(state_id).concat('.json'),
        success: function (state) {
          var value = '';
          for (var i = 0; i < state.territory_cities.length; i++) {
            if (state.territory_cities[i].id == _this.props.selected) {
              value = _this.props.selected;
              break;
            }
          };
          _this.setState({cities: state.territory_cities, value: value});
        },
        error: function (err, data) {
          _this.setState({value: '', cities: []});
        },
        complete: function(){
          _this.handleChange({target: {value: _this.state.value }});
        }
      });
    else{
      _this.setState({cities: []});
      _this.handleChange({target: {value: ''}});
    }
  },
  handleChange: function (event) {
    this.setState({value: event.target.value});
    typeof this.props.onChange === 'function' ? this.props.onChange(event) : null ;
  },
  getInitialState: function () {
    return { cities: [], value: this.props.selected }
  },
  componentDidMount: function () {
    $('.chosen-city').chosen({
      no_results_text: "No se encontró",
      allow_single_deselect: true
    }).change(function (event) {
      this.handleChange(event)
    }.bind(this));
  },
  componentDidUpdate: function () {
    $('.chosen-city').trigger("chosen:updated");
  },
  render: function () {
    if (this.props.context === 'scaffold') {
      return (
        <FieldWrapper errors={this.props.errors}>
          <div className='form_row'>
            <label htmlFor={this.props.name} className='scaffold_label'>Comuna</label>
            <select id={React.sanitizeName(this.props.name)}
                    name={this.props.name} value={this.state.value}
                    onChange={this.handleChange}
                    className='form_element __select scaffold_select chosen-city'
                    data-placeholder='Selecciones una opción'>
              <option value=''></option>
              {this.state.cities.map(function (city) {
                return <OptionSelect key={'city_' + city.id} value={city.id} text={city.name} />
              })}
            </select>
          </div>
        </FieldWrapper>
      )
    } else {
      return (
        <FieldWrapper errors={this.props.errors}>
          <div className='postulant_inner-row'>
            <label htmlFor={React.sanitizeName(this.props.name)} className='postulant_inner-row_key'>Comuna</label>
            <div className='postulant_inner-row_value'>
              <select id={React.sanitizeName(this.props.name)}
                      name={this.props.name} value={this.state.value}
                      onChange={this.handleChange}
                      className='form_element __select chosen-city'
                      data-placeholder='Selecciones una opción'
                      required={this.props.required}>
                <option value=''></option>
                {this.state.cities.map(function (city) {
                  return <OptionSelect key={'city_' + city.id} value={city.id} text={city.name} />
                })}
              </select>
            </div>
          </div>
        </FieldWrapper>
      )
    }
  }
});

