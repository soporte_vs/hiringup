
var OptionSelect = React.createClass({
  render: function() {
    return (
      <option value={this.props.value}>{this.props.text}</option>
    );
  }
});

var StateSelect = React.createClass({
  getStatesByCountry: function (country_id) {
    var _this = this;
    // if country_id is undefined or empty or null not do ajax reqwes
    if(country_id)
      reqwest({
        url: '/app/territory/countries/'.concat(country_id).concat('.json'),
        success: function (country) {
          var value = '';
          for (var i = 0; i < country.territory_states.length; i++) {
            if (country.territory_states[i].id == _this.props.selected) {
              value = _this.props.selected;
              break;
            }
          };
          _this.setState({states: country.territory_states, value: value});
        },
        error: function (err, data) {
          _this.setState({value: '', states: []});
        },
        complete: function(){
          _this.handleChange({target: {value: _this.state.value }});
        }
      });
    else{
      _this.setState({states: []});
      _this.handleChange({target: {value: ''}});
    }
  },
  handleChange: function (event) {
    this.setState({value: event.target.value});
    typeof this.props.onChange === 'function' ? this.props.onChange(event) : null ;
  },
  getInitialState: function () {
    return { states: [], value: this.props.selected }
  },
  componentDidMount: function () {
    $('.chosen-state').chosen({
      no_results_text: "No se encontró",
      allow_single_deselect: true
    }).change(function (event) {
      this.handleChange(event)
    }.bind(this));
    if (this.props.country_id){
      this.getStatesByCountry(this.props.country_id);
    }
  },
  componentDidUpdate: function () {
    $('.chosen-state').trigger("chosen:updated");
  },
  render: function () {
    if (this.props.context === 'scaffold') {
      return (
        <div className='form_row'>
          <label htmlFor={React.sanitizeName(this.props.name)} className='scaffold_label'>Región</label>
          <select id={React.sanitizeName(this.props.name)} name={this.props.name} value={this.state.value} onChange={this.handleChange}
                  className='form_element __select scaffold_select chosen-state' data-placeholder='Seleccione una opción'>
            <option value=''></option>
            {this.state.states.map(function (state) {
              return <OptionSelect value={state.id} text={state.name} key={'state_' + state.id} />
            })}
          </select>
        </div>
      )
    } else {
      return (
        <div className='postulant_inner-row'>
          <label htmlFor={React.sanitizeName(this.props.name)} className='postulant_inner-row_key'>Región</label>
          <div className='postulant_inner-row_value'>
            <select id={React.sanitizeName(this.props.name)} name={this.props.name} value={this.state.value} onChange={this.handleChange}
                    className='form_element __select chosen-state' data-placeholder='Seleccione una opción' required={this.props.required}>
              <option value=''></option>
              {this.state.states.map(function (state) {
                return <OptionSelect value={state.id} text={state.name} key={'state_' + state.id} />
              })}
            </select>
          </div>
        </div>
      )
    }
  }
});

