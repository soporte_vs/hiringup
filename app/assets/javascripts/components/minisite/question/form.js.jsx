
var OptionSelect = React.createClass({
  render: function() {
    return (
      <option value={this.props.value}>{this.props.text}</option>
    );
  }
});

var QuestionForm = React.createClass({
  propTypes:{
    question: React.PropTypes.shape({
      description: React.PropTypes.string.isRequired,
      _destroy: React.PropTypes.string
    })
  },
  handlerChangeDescription: function (event) {
    this.state.question.description = event.target.value;
    this.setState({question: this.state.question});
  },
  handlerDelete: function (event) {
    this.state.question._confirm = event.target.value;
    this.state.checked = true;

    this.setState({question: this.state.question}, function () {
      if (confirm('¿Seguro que deseas eliminar esta pregunta?')) {
        this.state.question.description = '';
        this.state.question._destroy = 'on';
      } else {
        this.state.question._confirm = false;
        this.state.checked = false;
      }
      this.setState({question: this.state.question})
    });
  },
  getInitialState: function () {
    return {
      question: this.props.question,
    }
  },
  render: function () {
    var style = {};
    if (this.state.question._destroy == 'on') {
      style['display'] = 'none';
    }

    if (this.state.question._confirm == 'on') {
      style['border'] = '2px solid #4785c9';
    } else {
      style['border'] = 'none';
    }

    return (
      <li style={style} className='create-course_list-element'>
        <div className='create-course_row'>
          <input type='hidden' name={this.props.name + '[id]'}
                 value={this.state.question.id} />

          <label className='create-course_label'>Pregunta</label>
          <div className='create-course_delete-question'>
            <div className='checkbox'>
              <label>
                <input type='checkbox' name={this.props.name + '[_destroy]'}
                       onChange={this.handlerDelete}
                       checked={this.state.checked || this.state.question._destroy}
                       className=''/>

                Eliminar
              </label>
            </div>
          </div>
          <input type='text'
                 name={this.props.name + '[description]'}
                 value={this.state.question.description}
                 onChange={this.handlerChangeDescription}
                 className='form-control' />
        </div>
      </li>
    )
  }
});
