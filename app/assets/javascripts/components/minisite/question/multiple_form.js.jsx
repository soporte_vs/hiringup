var MultipleQuestionForm = React.createClass({
  handlerClick: function (event) {
    this.state.questions.push({id: null, description: null});
    this.setState({questions: this.state.questions});
  },
  getInitialState: function () {
    return {
      questions: this.props.questions,
    }
  },
  render: function () {
    var _name = this.props.name;

    return (
      <div>
        <ol>
          {this.state.questions.map(function (question, index){
            __name = _name+'[questions_attributes]['+index+']';
            return <QuestionForm key={__name} name={__name} question={question} />
          })}
        </ol>
        <input type='button'
               name = 'add_question'
               onClick={this.handlerClick}
               value='Agregar Nueva Pregunta'
               className='btn btn-primary' />
      </div>
    )
  }
});
