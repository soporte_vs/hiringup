
var SetterNewCourseVacancies = React.createClass({
  notifyParent: function (vacancies_updates) {
    this.setState({value: ''});
    typeof this.props.callbackParent == 'function' ? this.props.callbackParent(vacancies_updates) : null
  },
  handleChange: function (event) {
    this.setState({value: event.target.value});
    typeof this.props.onChange === 'function' ? this.props.onChange(event) : null ;
  },
  handleClick: function (event) {
    console.log(this.props.getVacanciesSelected());
    _vacancy_ids = this.props.getVacanciesSelected();
    vacancy_ids = []
    for (i in _vacancy_ids) {
      vacancy_ids.push('vacancy_ids[]=' + _vacancy_ids[i])
    }
    url_encoded_vacancies = vacancy_ids.join('&');
    window.location = this.props.url_new_course + '?' + url_encoded_vacancies;
    return;
  },
  getInitialState: function () {
    return {
      options: this.props.options ? this.props.options : [],
      value: this.props.selected ? this.props.selected : ''
    }
  },
  drawButton: function () {
    style = 'btn btn-primary'
    message = 'Asignar a un nuevo proceso'
    loading = 'Asignando...'
    return (
      <button className={style} data-loading-text={loading} onClick={this.handleClick}>{message}</button>
    )
  },
  render: function () {
    return (
      <div className="form-inline">
        {this.drawButton()}
      </div>
    )
  }
});
