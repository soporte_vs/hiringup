var NoHitsErrorDisplay = React.createClass({
  render: function () {
    return (
      <div className='text-center col-xs-12 __white-background padding-top-20-px padding-bottom-20-px __with_shadow'>
        <strong>
          Ha occurido un error al realizar la búsqueda
        </strong>
      </div>
    )
  }
})
