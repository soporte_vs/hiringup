var EmailValidator = {
  isValidEmail: function (email) {
    email = (typeof(email) === 'undefined') ? this.state.email : email;
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }
};

var EmailField = React.createClass({
  mixins: [EmailValidator],
  setEmail: function(event){
    email = event.target.value.toLowerCase();
    this.setState({email: email, valid: this.isValidEmail(email)}, function () {
      this.props.valueLink.requestChange(this.state.email);
    });
  },
  getInitialState: function () {
    return { email: this.props.valueLink.value || '', valid: null }
  },
  clearField: function () {
    this.setState({email: '', valid: null});
  },
  render: function() {
    var classNameError = '';
    var glyphicon = '';
    if (this.state.valid != null){
      classNameError = this.state.valid ? 'has-success' : 'has-error';
      glyphicon = this.state.valid ? 'glyphicon-ok' : 'glyphicon-remove';
    }
    classNameRequired = this.props.required ? 'input_required' : '';
    return(
      <div className={classNameError + " has-feedback"}>
        <label className={classNameRequired + " control-label"}>{this.props.label}</label>
        <input type="text" className="form-control" value={this.state.email} onChange={this.setEmail}/>
        <span className={"glyphicon " + glyphicon + " form-control-feedback"} aria-hidden="true"></span>
        <span className="sr-only">{this.state.valid}</span>
      </div>
    )
  }
});

