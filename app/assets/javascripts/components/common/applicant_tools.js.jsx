var ApplicantTools = {
  drawCandidateType: function(candidate){
    if(candidate.is_employee){
      return(
        <img src={candidate.internal_image} alt="Internal" style={{height:17}}/>
      )
    }
  },
  drawBlacklisted: function(blacklisted){
    if (blacklisted){
      return(
        <span className="inline-item label label-danger">{'No recomendado'}</span>
      )
    }
  },
  drawStatus: function (is_approved) {
    if (is_approved == null){
      styleClass = "glyphicon-time";
      tooltip = 'Etapa pendiente'
    }else if (is_approved){
      styleClass = "text-success glyphicon-ok";
      tooltip = 'Etapa aprobada'
    }else{
      styleClass = "text-danger glyphicon-remove";
      tooltip = 'Etapa rechazada'
    }
    return(
      <span data-toggle="tooltip"
            data-placement="left"
            title={tooltip}
            className={"glyphicon " + styleClass}
            aria-hidden="true"></span>
    )
  },
  drawStatusLabeled: function(is_approved){
    if (is_approved == null){
      colorClass = "info";
      iconClass = 'time'
      tooltip = 'Etapa pendiente'
    }else if (is_approved){
      colorClass = "success";
      iconClass = 'ok-circle'
      tooltip = 'Etapa aprobada'
    }else{
      colorClass = "danger";
      iconClass = 'remove-circle'
      tooltip = 'Etapa rechazada'
    }

    return(
      <span data-toggle="tooltip"
            data-placement="left"
            title={tooltip}
            className={"label label-" + colorClass}>
        <span className={"glyphicon glyphicon-" + iconClass} aria-hidden="true"></span>
      </span>
    )
  },
  drawUpdateInfoRequestCount: function(request_count){
    if (request_count > 0){
      if (request_count == 1){
        mensaje = ' actualización de datos enviada'
      }else if (request_count > 1){
        mensaje = ' actualizaciones de datos enviadas'
      }
      return(
        <span title={'Número de peticiones de actualización enviadas'} className="inline-item label label-info">{request_count + mensaje}</span>
        )
    }
  },
  drawReferenced: function(candidate){
    if (candidate.has_references){
      return(
        <span className='glyphicon glyphicon-star-empty' style={{top:3}} > </span>
      )
    }
  }
}
