var DateTimeRangeValidator = {
  isDateTimeRangeValid: function(){
    is_starts_at_valid = moment().isBefore(this.state.starts_at);
    if (this.state.ends_at){
      return is_starts_at_valid && this.state.ends_at.isAfter(this.state.starts_at)
    }
    return is_starts_at_valid;
  }
}

var DateTimePicker = React.createClass({
  onChange: function () {
    this.props.onChange(this.state.date);
  },
  defaultMomentFormat: function (moment) {
    return (moment ? moment.format('LLLL') : '');
  },
  getInitialState: function () {
    return { date: this.props.date || null }
  },
  setDate: function (moment) {
    this.setState({date: moment}, this.onChange);
  },
  clearDate: function () {
    this.setState({date: null}, this.onChange);
  },
  render: function () {
    glyphicon = this.props.valid != null ? (this.props.valid ? 'glyphicon-ok' : 'glyphicon-remove') : '';
    required_class = this.props.required ? ' input_required ' : '';
    return (
      <div className='row'>
        <div className='col-xs-10'>
          <label className={'control-label' + required_class}>{this.props.label}</label>
          <Datetime inputProps={{readOnly: true, value: this.defaultMomentFormat(this.state.date)}}
                    onChange={this.setDate}
                    defaultValue={this.state.date}
                    isValidDate={this.props.isValidDate} />
          <span className={"glyphicon " + glyphicon + " form-control-feedback"} aria-hidden="true"></span>
        </div>
        <div className='col-xs-2'>
          <label>&nbsp;</label>
          <button type='button' className='btn btn-default btn-block' onClick={this.clearDate}>
            <span className='glyphicon glyphicon-ban-circle'></span>
          </button>
        </div>
      </div>
    )
  }
});

var DateTimeRangePicker = React.createClass({
  mixins: [React.addons.LinkedStateMixin, DateTimeRangeValidator],
  isValidStartsAt: function (current_date) {
    now = moment().startOf('day');
    if(current_date.isSameOrAfter(now)){
      return true;
    }
    return false;
  },
  isValidEndsAt: function(current_date){
    starts_at = this.state.starts_at ? this.state.starts_at.clone().startOf('day') : this.state.starts_at;
    if (current_date.isSameOrAfter(starts_at)){
      return true;
    }
    return false;
  },
  setStartsAt: function (moment_date) {
    this.setState({starts_at: moment_date}, function () {
      this.props.valueLinkStartsAt.requestChange(this.state.starts_at);
    });
  },
  setEndsAt: function (moment_date) {
    this.setState({ends_at: moment_date}, function () {
      this.props.valueLinkEndsAt.requestChange(this.state.ends_at);
    });
  },
  getInitialState: function(){
    starts_at = this.props.valueLinkStartsAt.value || null;
    ends_at = this.props.valueLinkEndsAt.value || null;
    return { starts_at: starts_at, ends_at: ends_at }
  },
  clearDates: function () {
    this.refs.since_date.clearDate();
    this.refs.until_date.clearDate();
  },
  render: function() {
    now = moment();
    style_ends_at = ''; style_starts_at = '';
    is_ends_at_valid = null; is_starts_at_valid = null;

    if (this.state.starts_at != null){
      style_starts_at = this.state.starts_at.isAfter(now) ? 'has-success' : 'has-error'
      is_starts_at_valid =  this.state.starts_at.isAfter(now) ? true : false;
    }
    if (this.state.ends_at != null){
      style_ends_at = this.state.ends_at.isAfter(this.state.starts_at) ? 'has-success' : 'has-error'
      is_ends_at_valid =  this.state.ends_at.isAfter(this.state.starts_at) ? true : false;
    }

    return(
      <div className='form-group row clearfix'>
        <div className={'col-xs-6 ' + style_starts_at + ' has-feedback'}>
          <DateTimePicker
            label='Fecha y hora de incio'
            required={true}
            ref='since_date'
            date={this.state.starts_at}
            onChange={this.setStartsAt}
            valid={is_starts_at_valid}
            isValidDate={this.isValidStartsAt}/>
        </div>
        <div className={'col-xs-6 ' + style_ends_at + ' has-feedback'}>
          <DateTimePicker
            label='Fecha y hora de finalización'
            required={false}
            ref='until_date'
            date={this.state.ends_at}
            onChange={this.setEndsAt}
            valid={is_ends_at_valid}
            isValidDate={this.isValidEndsAt}/>
        </div>
      </div>
    )
  }
});
