var FieldWrapper = React.createClass({
  drawErrorMessages:function() {
    if (this.props.hide_message != true){
      return (
        <ul className="errors_list clearfix">
          {this.props.errors.map(function (error) {
            return <li>{error}</li>
          })}
        </ul>
      )
    }
  },
  render: function() {
    errors = this.props.errors || [];
    if(errors.length > 0){
      return (
        <div className="field_with_errors">
          {this.props.children}
          { this.drawErrorMessages()}
        </div>
      );
    }else{
      return (
        <div>
          {this.props.children}
        </div>
      );
    }
  }
});

