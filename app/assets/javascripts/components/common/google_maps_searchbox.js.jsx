var GoogleMapsSearchBox = React.createClass({
  handlerChangeAddreesName: function (event) {
    this.props.valueLink.requestChange({
      name: event.target.value,
    });
  },
  placeChange: function (map, autocomplete, marker) {
    var place = autocomplete.getPlace();
    if (place.geometry.viewport) {
      map.fitBounds(place.geometry.viewport);
    } else {
      map.setCenter(place.geometry.location);
      map.setZoom(15);
    }
    this.props.valueLink.requestChange({
      name: place.formatted_address,
      lat: place.geometry.location.lat(),
      lng: place.geometry.location.lng()
    });
    marker.setPosition(place.geometry.location);
  },
  initGoogleMaps: function () {
    addres = this.props.valueLink.value;
    lat = address && address.lat ? address.lat : -33.4205956;
    lng = address && address.lng ? address.lng : -70.6737828;
    var map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: lat, lng: lng},
      zoom: 14,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      disableDefaultUI: true,
      draggable: true,
      scrollwheel: false
    });
    return map;
  },
  componentDidMount: function(){
    map = this.initGoogleMaps()
    var input = document.getElementById('pac-input');
    var searchBox = new google.maps.places.SearchBox(input);
    var autocomplete = new google.maps.places.Autocomplete(input);
    map.addListener('bounds_changed', function() {
      searchBox.setBounds(map.getBounds());
    });

    var marker = new google.maps.Marker({
      map: map,
      position: {lat: map.center.lat(), lng: map.center.lng() }
    });
    autocomplete.bindTo("bounds", map);
    google.maps.event.addListener(
      autocomplete,
      "place_changed",
      this.placeChange.bind(this, map, autocomplete, marker)
    );
  },
  getInitialState: function () {
    address = this.props.valueLink.value;
    address_name =  address && address.name ? address.name : '';
    return { address_name: address_name}
  },
  render: function() {
    return (
      <div>
        <label className="input_required">Ubicación</label>
        <input id='pac-input' type="text"
                className="form-control"
                placeholder='Dirección'
                onChange={this.handlerChangeAddreesName}
                defaultValue={this.state.address_name} />
        <br/>
        <div id="map" style={{height: '400px'}}></div>
      </div>
    )
  }
});
