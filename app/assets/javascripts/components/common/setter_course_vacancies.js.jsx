
var OptionSelect = React.createClass({
  render: function() {
    return (
      <option value={this.props.value}>{this.props.text}</option>
    )
  }
});

var SetterCourseVacancies = React.createClass({
  notifyParent: function (vacancies_updates) {
    this.setState({value: ''});
    typeof this.props.callbackParent == 'function' ? this.props.callbackParent(vacancies_updates) : null
  },
  handleChange: function (event) {
    this.setState({value: event.target.value});
    typeof this.props.onChange === 'function' ? this.props.onChange(event) : null ;
  },
  handleClick: function (event) {
    _this = this;
    button_target = $(event.target);
    if (typeof this.props.getVacanciesSelected == 'function')
      $(event.target).button('loading');
      Vacancy.assignCourse(
        this.props.getVacanciesSelected(),
        this.state.value,
        this.props.auth_token
      ).then(function (vacancies_updates) {
        _this.notifyParent(vacancies_updates);
        button_target.button('reset');
      }, function (err, data) {
        button_target.button('reset');
      });
  },
  getInitialState: function () {
    return {
      options: this.props.options ? this.props.options : [],
      value: this.props.selected ? this.props.selected : ''
    }
  },
  drawButton: function () {
    style = 'btn btn-success'
    message = 'Asignar'
    loading = 'Asignando...'
    return (
      <button className={style} data-loading-text={loading} onClick={this.handleClick}>{message}</button>
    )
  },
  render: function () {
    return (
      <div className="form-inline">
        <div className="form-group" style={{marginRight: '10px'}}>
          <select id={React.sanitizeName(this.props.name)}
                  name={this.props.name} value={this.state.value}
                  onChange={this.handleChange} className="form-control"
                  style={{width: '352px'}}>
            <option value=''>Ningun proceso (Desasignar)</option>
            {this.state.options.map(function (option) {
              text = option.name || option.title
              return <OptionSelect key={'option_' + option.id} value={option.id} text={text} />
            })}
          </select>
        </div>
        {this.drawButton()}
      </div>
    )
  }
});
