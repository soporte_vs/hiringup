var ParentChildSelect = React.createClass({
  handlerChangeParent: function(event) {
    try {
      this.refs.child_select.getChildrenByParent(event.target.value);
    } catch(err) {
      console.warn(err);
    }

    if(typeof this.props.parent_select.onChange === 'function') {
      this.props.parent_select.onChange(event);
    }
  },
  handlerChangeChild: function(event) {
    if(typeof this.props.child_select.onChange === 'function') {
      this.props.child_select.onChange(event);
    }
  },
  render: function () {
    var parentSelectProps = {
      label: this.props.parent_select.label,
      required: this.props.parent_select.required,
      errors: this.props.parent_select.errors,
      url: this.props.parent_select.url,
      name: this.props.parent_select.name,
      selected: this.props.parent_select.selected,
      context: this.props.parent_select.context,
      onChange: this.handlerChangeParent
    },
    childSelectProps = {
      label: this.props.child_select.label,
      required: this.props.child_select.required,
      errors: this.props.child_select.errors,
      url: this.props.child_select.url,
      name: this.props.child_select.name,
      selected: this.props.child_select.selected,
      context: this.props.child_select.context,
      onChange:this.handlerChangeChild
    };
    return (
      <div>
        <ParentSelect {...parentSelectProps} />
        <ChildSelect ref='child_select' {...childSelectProps} />
      </div>
    )
  }
});
