var ParentOption = React.createClass({
  render: function(){
    return <option value={this.props.value}>{this.props.text}</option>
  }
})

var ParentSelect = React.createClass({
  getParents: function () {
    var _this = this;
    this.setState({spinnerShow: true});
    reqwest({
      url: _this.props.url,
      success: function(parents){
        var selected = '';
        for(key in parents) {
          if (key == _this.props.selected) {
            selected = _this.props.selected;
            break;
          }
        };
        _this.setState({parents: parents, value: selected});
      },
      error: function (err, data) {
        _this.setState({parents: [], value: ''});
      },
      complete: function(){
        _this.handleChange({target: {value: _this.state.value}})
        _this.setState({spinnerShow: false});
      }
    })
  },
  handleChange: function (event) {
    this.setState({value: event.target.value});
    if(typeof this.props.onChange === 'function') {
      this.props.onChange(event);
    }
  },
  componentDidMount: function () {
    var class_name = '.chosen-laborum-parent-' + this.props.label.replace(/ /g,'')
    $(class_name).chosen({
      no_results_text: "No se encontró",
      allow_single_deselect: true
    }).change(function (event) {
      this.handleChange(event);
    }.bind(this));
    this.getParents();
  },
  componentDidUpdate: function () {
    var class_name = '.chosen-laborum-parent-' + this.props.label.replace(/ /g,'')
    $(class_name).trigger("chosen:updated");
  },
  getInitialState: function () {
    return { parents: {}, value: this.props.selected, spinnerShow: false}
  },
  render: function(){
    var _this = this;
    var requiredSpan;
    var class_name = 'form_element __select chosen-laborum-parent-' + this.props.label.replace(/ /g,'');
    if(this.props.required) {
      requiredSpan = <span className='form_required'>  *</span>
    }
    return (
      <FieldWrapper errors={this.props.errors}>
        <div className='postulant_inner-row'>
          <label className='postulant_inner-row_key'>{this.props.label}{requiredSpan}<Spinner show={this.state.spinnerShow} /></label>
          <div className='postulant_inner-row_value'>
            <select value={this.state.value} name={this.props.name} disabled={this.state.spinnerShow} onChange={this.handleChange} className={class_name} data-placeholder='Seleccione una opción'>
              <option value=''></option>
              {Object.keys(this.state.parents).map(function(val, index){
                return <ParentOption key={val} value={val} text={_this.state.parents[val].nombre} />
              })}
            </select>
          </div>
        </div>
      </FieldWrapper>
    )
  }
});
