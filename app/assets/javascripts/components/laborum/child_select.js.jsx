var OptionSelect = React.createClass({
  render: function() {
    return (
      <option value={this.props.value}>{this.props.text}</option>
    );
  }
});

var ChildSelect = React.createClass({
  getChildrenByParent: function (parent_id) {
    var _this = this;
    // if country_id is undefined or empty or null not do ajax reqwes
    if(parent_id) {
      _this.setState({spinnerShow: true});
      reqwest({
        url: _this.props.url.concat('?md=').concat(parent_id),
        success: function (parent) {
          var value = '';
          for(key in parent) {
            if (key == _this.props.selected) {
              value = _this.props.selected;
              break;
            }
          };
          _this.setState({children: parent, value: value});
        },
        error: function (err, data) {
          _this.setState({value: '', children: []});
        },
        complete: function(){
          _this.handleChange({target: {value: _this.state.value }});
          _this.setState({spinnerShow: false});
        }
      });
    } else {
      _this.setState({children: []});
      _this.handleChange({target: {value: ''}});
    }
  },
  handleChange: function (event) {
    this.setState({value: event.target.value});
    if(typeof this.props.onChange === 'function') {
      this.props.onChange(event);
    }
  },
  componentDidMount: function () {
    $('.chosen-laborum-child').chosen({
      no_results_text: "No se encontró",
      allow_single_deselect: true
    }).change(this.handleChange(event))
    this.getChildrenByParent();
  },
  componentDidUpdate: function () {
    $(".chosen-laborum-child").trigger("chosen:updated");
  },
  getInitialState: function () {
    return { children: [], value: this.props.selected, spinnerShow: false }
  },
  render: function () {
    _this = this;
    var requiredSpan;
    if(this.props.required) {
      requiredSpan = <span className='form_required'>  *</span>
    }

    return (
      <FieldWrapper errors={this.props.errors}>
        <div className='postulant_inner-row'>
          <label className='postulant_inner-row_key'>{this.props.label}{requiredSpan}<Spinner show={this.state.spinnerShow} /></label>
          <div className='postulant_inner-row_value'>
            <select name={this.props.name} value={this.state.value} disabled={this.state.spinnerShow} onChange={this.handleChange} className='form_element __select chosen-laborum-child' data-placeholder='Seleccione una opción'>
              <option value=''></option>
              { Object.keys(this.state.children).map(function(val, index) {
                return <OptionSelect value={val} text={_this.state.children[val].nombre} key={'child_' + val} />
              })}
            </select>
          </div>
        </div>
      </FieldWrapper>
    )
  }
});
