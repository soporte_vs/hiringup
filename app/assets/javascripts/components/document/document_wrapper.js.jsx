var DocumentWrapper = React.createClass({
    getInitialState: function(){
      return {
        document_descriptor: this.props.document_descriptor,
        document_records: this.props.document_records,
        auth_token: this.props.authenticity_token,
        document_uploading: null,
        hasErrors: false,
        show_origin: this.props.show_origin != 'undefined' ? this.props.show_origin : false,
        is_admin: this.props.is_admin,
        current_user_id: this.props.current_user_id,
        show_document_request: this.props.show_document_request || false
      }
    },
    componentDidMount: function() {
      $('[data-toggle="tooltip"]').tooltip();
    },
    onDrop: function (files) {
      this.clearRecordsWithErrors();
      this.setState({document_uploading: true})
      var data = new FormData();
      _this = this;
      fileDescriptor = this.state.document_descriptor.name;
      data.append( 'applicant_documents[' + fileDescriptor+ ']', files[0] );

      //params: applicant_id, applicant_documents, auth_token
      Document.sendFile(this.props.upload_path, data, this.state.auth_token).then(function(data){
        _this.state.document_records.push(data[0])
        _this.clearRecordsWithErrors();
        if ( data[0].errors.document && data[0].errors.document.length > 0 ) {
          _this.setState({hasErrors: true})
        } else {
          _this.setState({hasErrors: false})
        }
        _this.setState({document_records: _this.state.document_records})
        // Actualizamos los iconos
        _this.drawIcon();
        _this.setState({document_uploading: null})

      });
    },
    clearRecordsWithErrors: function() {
      $('li.document-record.error').fadeOut();
    },
    drawIcon: function() {
      if (this.state.hasErrors == false && this.state.document_records.length > 0 ) {
        return  (
          <div className='document_icon col-xs-4'>
            <span className='uxpin-like size-24-px'></span>
          </div>
        )
      } else if ( this.state.hasErrors == true ) {
        return  (
          <div className='document_icon col-xs-4'>
            <span className='glyphicon glyphicon-floppy-remove size-20-px'></span>
          </div>
        )
      } else {
        return  (
          <div className='document_icon col-xs-4'>
            <span className=''></span>
          </div>
        )
      }
    },
    drawStatusClass: function(){
      if (this.state.hasErrors == false && this.state.document_records.length > 0 ) {
        return 'saved'
      } else if ( this.state.hasErrors == true ) {
        return 'error'
      } else {
        return ''
      }
    },
    deleteCallback: function(deleted_document_id){
      _this = this;
      for (var i=0;i<_this.state.document_records.length; i++){
        if(_this.state.document_records[i].id===deleted_document_id){
          _this.state.document_records.splice(i, 1);
          _this.setState({document_records: _this.state.document_records});
          break;
        }
      }
      //_this.state.document_records.map(function(record,index){
      //})

    },
    drawRecords: function(){
      _this = this;
      show_origin = _this.state.show_origin;
      is_admin = _this.state.is_admin;
      current_user_id = _this.state.current_user_id;

      if (this.state.document_records.length > 0) {
        return (
          <ul>
            { _this.state.document_records.map(function(record,index){
              return <DocumentRecord document_record={record} show_origin={show_origin} is_admin={is_admin} current_user_id={current_user_id} deleteCallback={_this.deleteCallback}/>
            })}
          </ul>
        )
      } else {
        extensions = this.state.document_descriptor.allowed_extensions ? this.state.document_descriptor.allowed_extensions : "jpg, jpeg, doc, docx y pdf"
        return (
          <div>
            <p className="text-center">
              { "En este momento no tienes documentos de " + this.state.document_descriptor.details + " subidos"}
            </p>
            <p className="text-center">
              Haz un click en el botón o arrastra y suelta el archivo en el botón, para subirlo.
            </p>
            <p className="text-center">
              Solo puedes subir archivos de máximo 5MB de peso
            </p>
            <p className="text-center">

            { "Se permiten archivos con la extension: " + extensions }
            </p>
          </div>
        )
      }
    },
    drawUploadingDocument: function() {

      if (this.state.document_uploading != null) {
        return (
          <div className="col-xs-12">
            <div className="progress">
              <div className="progress-bar progress-bar-success progress-bar-striped active" role="progressbar"
              aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style={{ width: '100%'}}>
                Subiendo...
              </div>
            </div>
          </div>

        )
      }
    },
    drawCheckBoxRequestDocument: function(document_descriptor){
      if (this.props.show_document_request){
        return(
          <div className="col-sm-1">
            <input type="checkbox" readOnly onClick={this.props.onClickDescriptor} value={document_descriptor.name}/>
          </div>
        )
      }
    },
    render: function () {
      // errors = this.state.record.errors.document ? this.state.record.errors.document : []
      documentID = this.state.document_descriptor.id
      return (
        <div className={ this.drawStatusClass() }>
          {this.drawCheckBoxRequestDocument(this.state.document_descriptor)}
          <div className={"document-header clearfix  __with_collapsible-icons " + (this.props.show_document_request ? "col-sm-11" : "col-sm-12")}>
            <a href={'#document-body' + documentID} data-toggle='collapse' aria-expanded='false' aria-controls={"document-body" + documentID} className='collapsed'>
              <div className='col-sm-6'>
                { this.drawIcon()}

                <div className='document_descriptor col-xs-8'>
                  <p>{ this.state.document_descriptor.details }</p>
                </div>

              </div>

              <div className='col-sm-6 text-right'>

                <div className='fake-input text-center'>
                  <span className='glyphicon glyphicon-floppy-open'></span>
                  <span className='small'> &nbsp;subir archivos </span>
                </div>

              </div>
            </a>
          </div>

          <div id={"document-body" + documentID} className='collapse clearfix document-body col-sm-12'>
            <div className='row margin-top-10-px'>

              { this.drawRecords() }
            </div>
            <div className='row margin-top-10-px'>
              { this.drawUploadingDocument() }
              <hr />
              <div className="col-xs-12 text-center">
                <div className='fake-input margin-bottom-10-px text-center'>
                  <Dropzone onDrop={this.onDrop} multiple={false} className={'document-dropzone'} disablePreview={true}>
                    <span className='glyphicon glyphicon-floppy-open'></span>
                    <span className='small'> &nbsp;subir archivos </span>
                  </Dropzone>
                </div>
              </div>
            </div>
          </div>

        </div>
      );
    }
});
