var DocumentWrapperAction = React.createClass({
  getInitialState: function(){
    return { document_descriptors_checked: [] }
  },
  filter_records_by_descriptor: function(records, descriptor){
    new_records = [];

    records.map(function(record){
      if (record.document_descriptor_id == descriptor.id){
        new_records.push(record)
      }
    })
    return new_records;
  },
  onClickDescriptor: function(event){
    document_descriptor_name = event.target.value;
    if (event.target.checked){
      this.state.document_descriptors_checked.push(document_descriptor_name);
    }else{
      index = this.state.document_descriptors_checked.indexOf(document_descriptor_name);
      this.state.document_descriptors_checked.splice(index, 1)
    }
    this.setState({document_descriptors_checked: this.state.document_descriptors_checked})
  },
  documentRequest: function(){
    enrollments = this.props.enrollments;
    modal = document.querySelector('#modal .modal-content');

    invite_form = React.createElement(DocumentRequestForm, {
      document_descriptors: this.props.document_descriptors,
      document_descriptors_checked: this.state.document_descriptors_checked,
      request_path: this.props.request_path
    });

    ReactDOM.render(invite_form, modal);
    $('#modal').modal().on('shown.bs.modal', function () {
      $('.chosen').chosen();
    });

    $('#modal').on('hide.bs.modal', function () {
      modal_content = this.querySelector('#modal .modal-content');
      if(modal_content && modal_content.children[0])
        modal_content.removeChild(modal_content.children[0]);
    });
  },


  render: function() {
    _this = this;
    return (
      <div>
        <div id="document_request">
          <div className="row">
            <div className="col-xs-12">
              <button className="btn btn-success" onClick={_this.documentRequest}>Solicitar nuevamente</button>
            </div>
          </div>
        </div>
        <div id="document_wrapper">
          {_this.props.document_descriptors.map(function (document_descriptor, index) {
            return(
              <li className="document-item-wrapper clearfix">
                <DocumentWrapper
                  applicant_id={_this.props.applicant_id}
                  auth_token={_this.props.auth_token}
                  document_descriptor={document_descriptor}
                  document_records = {_this.filter_records_by_descriptor(_this.props.document_records, document_descriptor)}
                  upload_path={_this.props.upload_path}
                  is_admin={_this.props.is_admin}
                  show_origin={_this.props.show_origin}
                  show_document_request= {_this.props.show_document_request}
                  onClickDescriptor= {_this.onClickDescriptor}
                />
              </li>
            )
          })}
        </div>
      </div>
    );
  }
});

