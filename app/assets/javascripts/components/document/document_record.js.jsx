var DocumentRecord = React.createClass({
    getInitialState: function(){
      return {
        document_record: this.props.document_record
      }
    },
    handlerDelete: function (files) {
      _this = this;
      if (confirm('¿Seguro que deseas eliminar este archivo?')){
        id = _this.state.document_record.id;
        Document.deleteFile(
          _this.state.document_record.id
        ).then(function(data){
          _this.props.deleteCallback(id);
          _this.setState({document_record: null});
        });
      }
    },
    drawDeleteLink: function(){
      documentUserId = this.state.document_record.user_id
      currentUserId = this.props.current_user_id
      if (this.props.is_admin == true || documentUserId == currentUserId) {
        return (
          <a onClick={this.handlerDelete} className='uxpin-trash size-25-px'></a>
        )
      }
    },
    drawDownloadLink: function(){
      if (this.state.document_record.url) {
        return (
          <a href={this.state.document_record.url} download className='uxpin-download size-25-px'></a>
        )
      }
    },
    drawStatusClass: function(){
      errors = this.state.document_record.errors;
      if (errors.document && errors.document.length > 0) {
        return 'error'
      } else {
        return ''
      }
    },
    drawIcon: function() {
      var extension = ''
      if (this.state.document_record.document) {
        doc = this.state.document_record.document
        file_name_tokenized = doc.split('.')
        extension = file_name_tokenized[file_name_tokenized.length - 1 ] ? file_name_tokenized[file_name_tokenized.length - 1 ] : ''
        extension = extension.toLowerCase()
      }
      return  (
        <div className={'document_icon col-xs-4 ' + extension}>
          <span className='doc-type'>{ extension }</span>
        </div>
      )
    },
    drawFileNameLink: function() {
      return (
        <a href={this.state.document_record.url} target="_blank" className="text-success">{ this.state.document_record.document }</a>
      )
    },
    drawCourseOrLink: function(){
      linkToProfile = this.state.document_record.profile_url
      applicantName = this.state.document_record.applicant

      docCourse = this.state.document_record.course

      if (docCourse) {
        courseTitle = docCourse.title
        coursePath = this.state.document_record.course_path
        return (
          <a href={coursePath} className="course-link">{ 'proceso:' + courseTitle }</a>
        )
      } else {
        return (
          <a href={this.state.document_record.profile_url} className="applicant-link">{ applicantName }</a>
        )
      }
    },
    drawDocumentOrigin: function() {
      if ( this.props.show_origin == true ) {
        return (
          <span>
            <strong>origen de documento: </strong>
            <span>{ this.drawCourseOrLink() } </span>
          </span>
        )
      }
    },
    drawDetailsOrErrors: function() {
      errors = this.state.document_record.errors;
      if (errors.document && errors.document.length > 0) {
        return (
          <FieldWrapper errors={errors.document} />
        )
      } else {
        return (
          <p>
            <strong>Subido </strong>
            <span className="small">{ this.state.document_record.created_at} </span>
            { this.drawDocumentOrigin() }
          </p>
        )
      }
    },
    render: function () {
      if (this.state.document_record.errors.length >= 1) {
        errors = this.state.document_record.errors.document ? this.state.document_record.errors.document : []
      }
      return (
          <li className={ "clearfix document-record " + this.drawStatusClass() }>
            <div className='col-sm-10'>
              <div className='col-sm-1'>
                { this.drawIcon() }

              </div>
              <div className='col-sm-11 document_record-data'>
                  <p> { this.drawFileNameLink() } </p>
                  { this.drawDetailsOrErrors() }
              </div>
            </div>

            <div className="col-sm-2 doc-controls text-right">
              <div className="document_linkto-download">
                { this.drawDownloadLink() }
                &nbsp;
                &nbsp;
                { this.drawDeleteLink() }
              </div>
            </div>

          </li>
        )
    }
});
