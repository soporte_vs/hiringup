var DocumentRequestForm = React.createClass({
  mixins: [React.addons.LinkedStateMixin],
  getInitialState: function () {
    return {
      document_descriptors_checked: this.props.document_descriptors_checked || [],
      document_descriptors: this.props.document_descriptors || [],
      message: "",
      response : "",
    }
  },

  getDescriptorsSelected: function(){
    document_descriptors_checked = [];
    selectedOptions = this.refs.document_descriptor_selected.selectedOptions;
    for (var i = 0; i < selectedOptions.length; i++) {
      document_descriptors_checked.push(selectedOptions[i].value);
    }
    return document_descriptors_checked;
  },

  changeDescriptors: function (event) {
    this.setState({document_descriptors_checked: this.getDescriptorsSelected()})
  },

  componentDidMount: function () {
    $('.chosen').change(this.changeDescriptors);
  },
  componentDidUpdate: function(){
    $('.chosen').trigger("chosen:updated");
  },

  accept: function(event){
    if (this.isFormCompleted){
      descriptors_checked = this.state.document_descriptors_checked;
      Document.documentRequest(this.props.request_path, descriptors_checked, this.state.message).then(function(data){
         event.target.setAttribute('disabled', false);
        _this.setState({response: data, document_descriptors_checked: [], message: ""});
      })
    }
  },

  isFormCompleted: function(){
    return (
      this.state.document_descriptors_checked.length > 0
    )
  },

  renderResponse: function(){
    response = this.state.response;
    if (response){
      style = response.success ? "alert-success" : (response.error ? "alert-danger" : "")
      return (
        <div className={'alert ' + style} role="alert">
          {function () {
            if(response.success){
              return response.success;
            }else{
              return response.error
            }
          }()}
        </div>
      );
    }
  },

  render: function(){
    _this = this;

    button_disabled = !this.isFormCompleted()

    return(
      <div>
        <div className='modal-header'>
          <span>Solicitar Documentos</span>
        </div>
        <div className='modal-body'>
          {this.renderResponse()}
          <div className='form-group'>
            <label className='input_required'>Documentos</label>
            <select ref='document_descriptor_selected' data-placeholder="Seleccione documentos"
                    className='form-control chosen'
                    multiple={true}
                    onChange= {this.changeDescriptors}
                    value={this.state.document_descriptors_checked}
            >
              {this.state.document_descriptors.map(function(descriptor, index) {
                 return (
                   <option key={'descriptor-selected' + index} value={descriptor.name}>{descriptor.details}</option>
                 )
               })}
            </select>
          </div>

          <div className="form-group">
            <label>Observaciones</label>
            <small> Estas observaciones serán enviadas al candidato</small>
            <textarea className="form-control" valueLink={this.linkState('message')}></textarea>
          </div>
          <div className='form-group text-right'>
            <button type="button" className="btn btn-default" data-dismiss="modal" style={{marginRight: '5px'}}>Cerrar</button>
            <button type='button'  className='btn btn-success' disabled={button_disabled} onClick={this.accept}>Aceptar</button>
          </div>
        </div>
      </div>
    )
  }
})

