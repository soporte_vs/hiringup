var NoHitsCourseDisplay = React.createClass({
  drawSearchURI: function () {
    params = [];
    uri = '/app/courses/new'
    if (this.props.query.length > 0)
      params.push("course_title=" + this.props.query)
    if (params.length > 0){
      uri_params = params.join('&')
      uri += '?' + uri_params
    }
    return uri;
  },
  render: function () {
    return (
      <div className='text-center col-xs-12 __white-background padding-top-20-px padding-bottom-20-px __with_shadow'>
        <p>
          <strong>
            No se encontraron procesos para la búsqueda '{this.props.query}'!
          </strong>
        </p>
        <p>
          <a href={this.drawSearchURI()} className="btn btn-success">
            Crear proceso
          </a>
        </p>
      </div>
    )
  }
});
