var CourseHit = React.createClass({
  drawState: function (aasm_state) {
    style = (aasm_state == 'opened' ? 'label-success' : 'label-danger')
    aasm_state = (aasm_state == 'opened' ? 'Abierto' : 'Cerrado')
    return(
      <span className={'label ' + style}>{aasm_state}</span>
    )
  },
  render: function () {
    return(
      <div className={"____course __progress-top-bar margin-bottom-10-px _"+ this.props.result._source.health+ "-percent"}>
        <a href={'/app/courses/' + this.props.result._source.id + '/?discard[0]=false'} className="process_element-container clearfix">
          <div className='col-xs-10'>
            <h2 className='process_title'>{this.props.result._source.title}</h2>
            <div className='row margin-bottom-10-px'>
              <span className=' process_col process_icons __date'>
                {moment(this.props.result._source.created_at).format('LLL')}
              </span>
              <span className='process_col process_icons __postulants'>
                {"Candidatos: " + this.props.result._source.applicants_count}
              </span>
              <span className='process_col process_icons __postulants'>
                {"Postulaciones pendientes: " + this.props.result._source.pending_postulations_count}
              </span>
              <span className='process_col process_icons __postulants'>
                {"Candidatos contratados: " + this.props.result._source.hired_applicants_count}
              </span>
            </div>
            <div className='row margin-bottom-10-px'>
              <span className='process_col process_icons __created'>
                Creado por:&nbsp;
                <span className='text-primary'>
                  {this.props.result._source.created_by ? this.props.result._source.created_by : 'Sistema'}
                </span>
              </span>
            </div>
          </div>
          <div className='col-xs-2'>
            <div className='margin-top-30-px'>
              {this.drawState(this.props.result._source.aasm_state)}
            </div>
          </div>
        </a>
      </div>
    );
  },
});
