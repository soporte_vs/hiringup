//= require components/course/postulations/hits
//= require components/course/postulations/not_hits
//= require components/common/error_hit

var SearchkitProvider = Searchkit.SearchkitProvider;
var RefinementListFilter = Searchkit.RefinementListFilter;
var Hits = Searchkit.Hits;
var NoHits = Searchkit.NoHits;
var Pagination = Searchkit.Pagination;
var ResetFilters = Searchkit.ResetFilters;

var PostulationStateItem = React.createClass({
  render: function() {
    switch(this.props.label){
      case ('accepted'): label = 'Aceptadas'; break;
      case ('rejected'): label = 'Rechazadas'; break;
      default: label = 'Pendientes'; break;
    }
    return (
      <div className={this.props.bemBlocks.option().state({selected: this.props.active}).mix(this.props.bemBlocks.container("item"))} onClick={this.props.onClick}>
        <input type="checkbox" data-qa="checkbox" readOnly="" checked={this.props.active} className={this.props.bemBlocks.option("checkbox")}/>
        <div className={this.props.bemBlocks.option("text")}>{label}</div>
        <div className={this.props.bemBlocks.option("count")}>{this.props.count}</div>
      </div>
    )
  }
});

var PostulationIsEmployeeItem = React.createClass({
  render: function() {
    switch(this.props.label){
      case ('true'): label = 'Sí'; break;
      case ('false'): label = 'No'; break;
      default: label = 'No'; break;
    }
    return (
      <div className={this.props.bemBlocks.option().state({selected: this.props.active}).mix(this.props.bemBlocks.container("item"))} onClick={this.props.onClick}>
        <input type="checkbox" data-qa="checkbox" readOnly="" checked={this.props.active} className={this.props.bemBlocks.option("checkbox")}/>
        <div className={this.props.bemBlocks.option("text")}>{label}</div>
        <div className={this.props.bemBlocks.option("count")}>{this.props.count}</div>
      </div>
    )
  }
});


var PostulationsSearch = React.createClass({
  childContextTypes: {
    searchkit: React.PropTypes.object,
    course_id: React.PropTypes.number,
    is_admin: React.PropTypes.bool
  },
  getChildContext: function() {
    return {
      searchkit: this.state.searchkit,
      course_id: this.state.course_id,
      is_admin: this.state.is_admin
    };
  },
  getInitialState: function () {
    var searchkit  = HupeSearchkitRoutes.getCoursePostulationsSearchRoute(this.props.course_id);
    return {
      searchkit: searchkit,
      course_id: this.props.course_id,
      is_admin: this.props.is_admin
    }
  },
  render: function() {
    var facets = {
      'facets.view_more': 'Ver más',
      'facets.view_less': 'Ver menos',
      'facets.view_all': 'Ver todos'
    }
    return (
      <SearchkitProvider searchkit={this.state.searchkit}>
        <div className='row'>
          <div className="col-md-12">
            <div className="col-xs-12">
              <div className='row panel __with_shadow' style={{padding: '10px'}}>
                <div className='col-xs-9'>
                  <div className='col-md-7'>
                    <SearchBox
                      autofocus={true}
                      searchOnChange={true}
                      queryOptions={{minimum_should_match: '100%'}}
                      translations={{'searchbox.placeholder': 'Buscar postulante'}}
                      queryFields={["full_name", "last_name", "first_name", "email"]}
                    />
                  </div>
                </div>

                <div className='col-xs-3 text-right'>
                  <SortingSelector options={[
                    {label:"Nombre A-Z", field:"full_name", order:"asc", },
                    {label:"Nombre Z-A", field:"full_name", order:"desc"},
                    {label:"Fecha Reciente", field:"created_at", order:"desc", defaultOption: true},
                    {label:"Fecha Antigua", field:"created_at", order:"asc"}
                  ]} className='pull-right'/>
                </div>
              </div>
            </div>
          </div>

          <div className='col-md-3'>
            <div className='col-xs-12 __white-background __with_shadow' style={{padding: '10px'}}>
            <hr/>
              <MenuFilter
                id="portals"
                title="Portal de Trabajo"
                field="portal"
                translations={{"minisite":"Minisitio", "All":"Todas"}}
              />
              <hr/>
              <MenuFilter
                id="state"
                title="Estado"
                field="state"
                translations={{"pending":"Pendiente", "accepted":"Aceptado", "rejected":"Rechazado","All":"Todos"}}
              />

              <hr/>
              <MenuFilter
                id="is_employee"
                title="Colaborador"
                field="is_employee"
                translations={{"true":"Sí", "false":"No", "All":"Todos"}}
              />

              <hr/>
              <MenuFilter
                id="references"
                title="Posee referencias"
                field="has_references"
                translations={{"All":"Todas", "true":"Sí", "false":"No"}}

              />

              <ResetFilters translations={{'reset.clear_all': 'Limpiar filtros'}}/>
            </div>
          </div>

          <div className="col-md-9">
            <Hits hitsPerPage={10}
              highlightFields={["title"]}
              sourceFilter={["title"]}
              listComponent={PostulationHits}/>

            <NoHits
              component={NoHitsPostulationsDisplay}
              errorComponent={NoHitsErrorDisplay}/>

            <Pagination translations={
              {
              "pagination.previous":"Anterior",
              "pagination.next":"Siguiente",
              }
            }
              showNumbers={true}  className='__white-background __with_shadow' style={{'padding': '10px'}}/>

          </div>
        </div>
      </SearchkitProvider>
    );
  }
});

