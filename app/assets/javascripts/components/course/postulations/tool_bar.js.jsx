//= require components/course/postulations/accept_form
//= require components/course/postulations/reject_form

var PostulationActions = React.createClass({
  contextTypes: {
    is_admin: React.PropTypes.bool,
  },
  accept: function () {
    postulations = this.props.postulations;
    modal = document.querySelector('#modal .modal-content');

    accept_form = React.createElement(AcceptForm, {
      course_id: this.props.course_id,
      postulation_selected_ids: this.props.postulation_ids_checked,
      onAccept: this.props.callBackActions
    });

    ReactDOM.render(accept_form, modal);
    $('#modal').modal().on('shown.bs.modal', function () {
      $('.chosen').chosen();
    });;

    $('#modal').on('hide.bs.modal', function () {
      modal_content = this.querySelector('#modal .modal-content');
      if(modal_content && modal_content.children[0])
        modal_content.removeChild(modal_content.children[0]);
    });
  },
  reject: function () {
    postulations = this.props.postulations;
    modal = document.querySelector('#modal .modal-content');

    accept_form = React.createElement(RejectForm, {
      course_id: this.props.course_id,
      postulation_selected_ids: this.props.postulation_ids_checked,
      onReject: this.props.callBackActions
    });

    ReactDOM.render(accept_form, modal);
    $('#modal').modal().on('shown.bs.modal', function () {
      $('.chosen').chosen();
    });;

    $('#modal').on('hide.bs.modal', function () {
      modal_content = this.querySelector('#modal .modal-content');
      if(modal_content && modal_content.children[0])
        modal_content.removeChild(modal_content.children[0]);
    });
  },
  update_request: function(){
    modal = document.querySelector('#modal .modal-content');

    applicants = this.props.postulations.map(function (postulation) {
      return applicant = {id: postulation.applicant_id, full_name: postulation.full_name };
    });
    ai = this.props.postulation_ids_checked;
    applicant_ids_checked = this.props.postulations.map(function (postulation) {
      if (String(ai).includes(String(postulation.id)))
        return postulation.applicant_id
    });

    update_form = React.createElement(UpdateApplicantDataRequestForm, {
      applicant_selected_ids: applicant_ids_checked,
      applicants: applicants,
      onUpdate: this.props.callBackActions
    });

    ReactDOM.render(update_form, modal);
    $('#modal').modal().on('shown.bs.modal', function () {
      $('.chosen').chosen();
      $('.chosen-course').chosen({
        allow_single_deselect: true
      });
    });

     $('#modal').on('hide.bs.modal', function () {
       modal_content = this.querySelector('#modal .modal-content');
       if(modal_content && modal_content.children[0])
         modal_content.removeChild(modal_content.children[0]);
     });
  },
  send_mail: function () {
    modal = document.querySelector('#modal .modal-content');

    applicants = this.props.postulations.map(function (postulation) {
      return applicant = {id: postulation.applicant_id, full_name: postulation.full_name };
    });
    ai = this.props.postulation_ids_checked;
    applicant_ids_checked = this.props.postulations.map(function (postulation) {
      if (String(ai).includes(String(postulation.id)))
        return postulation.applicant_id
    });

    send_email_form = React.createElement(SendMassiveApplicantForm, {
      applicant_selected_ids: applicant_ids_checked,
      applicants: applicants,
      onSendEmail: this.props.callBackActions,
    });

    ReactDOM.render(send_email_form, modal);
    $('#modal').modal().on('shown.bs.modal', function () {
      $('.chosen').chosen();
      $('.chosen-course').chosen({
        allow_single_deselect: true
      });
    });

     $('#modal').on('hide.bs.modal', function () {
       modal_content = this.querySelector('#modal .modal-content');
       if(modal_content && modal_content.children[0])
         modal_content.removeChild(modal_content.children[0]);
     });
  },
  reset: function () {
    postulations = this.props.postulations;
    modal = document.querySelector('#modal .modal-content');

    reset_postulation_form = React.createElement(ResetPostulationForm, {
      course_id: this.props.course_id,
      postulation_selected_ids: this.props.postulation_ids_checked,
      onResetPostulation: this.props.callBackActions
    });

    ReactDOM.render(reset_postulation_form, modal);
    $('#modal').modal().on('shown.bs.modal', function () {
      $('.chosen').chosen();
    });;

    $('#modal').on('hide.bs.modal', function () {
      modal_content = this.querySelector('#modal .modal-content');
      if(modal_content && modal_content.children[0])
        modal_content.removeChild(modal_content.children[0]);
    });
  },
  drawResetPostulation: function(){
    if (this.context.is_admin){
      return(
        <li>
          <a className='mouse-pointer' onClick={_this.reset}>
            <span className='glyphicon glyphicon-refresh' style={{marginRight: '5px'}}></span>
            Reiniciar postulación
          </a>
        </li>
      )
    }
  },
  export: function () {
    postulations = this.props.postulations;
    modal = document.querySelector('#modal .modal-content');

    export_form = React.createElement(CoursePostulationExportForm, {
      course_id: this.props.course_id,
      postulation_selected_ids: this.props.postulation_ids_checked,
      onExport: this.props.callBackActions
    });

    ReactDOM.render(export_form, modal);

    $('#modal').modal().on('shown.bs.modal', function(){
      $('.chosen').chosen();
    })
  },
  export_cv: function () {
    modal = document.querySelector('#modal .modal-content');

    applicants = this.props.postulations.map(function (postulation) {
      return applicant = {id: postulation.applicant_id, full_name: postulation.full_name };
    });
    ai = this.props.postulation_ids_checked;
    applicant_ids_checked = this.props.postulations.map(function (postulation) {
      if (String(ai).includes(String(postulation.id)))
        return postulation.applicant_id
    });

    export_cv_form = React.createElement(ExportCvApplicantForm, {
      applicant_selected_ids: applicant_ids_checked,
      applicants: applicants
    });

    ReactDOM.render(export_cv_form, modal);
    $('#modal').modal().on('shown.bs.modal', function () {
      $('.chosen').chosen();
      $('.chosen-course').chosen({
        allow_single_deselect: true
      });
    });

     $('#modal').on('hide.bs.modal', function () {
       modal_content = this.getElementsByClassName('modal-content')[0];
       if(modal_content && modal_content.children[0])
         modal_content.removeChild(modal_content.children[0]);
     });
  },
  render: function () {
    _this = this;
    return(
      <div className='btn-group'>
        <button type='button' className='btn btn-default' onClick={_this.accept}>
          <span className='glyphicon glyphicon-thumbs-up' style={{marginRight: '5px'}}></span>
          Aceptar
        </button>
        <button type='button' className='btn btn-default' onClick={_this.reject}>
          <span className='glyphicon glyphicon-thumbs-down' style={{marginRight: '5px'}}></span>
          Rechazar
        </button>

        <div className='btn-group'>
          <button type="button" className="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Más acciones <span className="caret"></span>
          </button>
          <ul className="dropdown-menu">
            <li>
              <a className='mouse-pointer' onClick={_this.update_request}>
                <span className='glyphicon glyphicon-envelope' style={{marginRight: '5px'}}></span>
                Solicitar actualización de datos
              </a>
            </li>
            <li>
              <a className='mouse-pointer' onClick={_this.send_mail}>
                <span className='glyphicon glyphicon-send' style={{marginRight: '5px'}}></span>
                Enviar correo
              </a>
            </li>
            <li>
              <a className='mouse-pointer' onClick={_this.export}>
                <span className='glyphicon glyphicon-download-alt' style={{marginRight: '5px'}}></span>
                Exportar Excel
              </a>
            </li>
            <li>
              <a className='mouse-pointer' onClick={_this.export_cv}>
                <span className='glyphicon glyphicon-download-alt' style={{marginRight: '5px'}}></span>
                Exportar CV
              </a>
            </li>
            { _this.drawResetPostulation() }
          </ul>
        </div>
      </div>
    )
  }
});
