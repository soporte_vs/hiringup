var AcceptForm = React.createClass({
  getInitialState: function () {
    if (!this.props.postulations) {
      _this = this;
      CoursePostulation.getPostulations(this.props.course_id).then(function (postulations) {
        _this.setState({postulations: postulations})
      });
    }
   return {
      response_accept: null,
      postulation_selected_ids: this.props.postulation_selected_ids || [],
      postulations: this.props.postulations || []
    }
  },
  accept: function (event) {
    _this = this;
    ids = this.getPostulationSelectedIds();
    course_id = this.props.course_id;
    if(ids.length > 0){
      event.target.setAttribute('disabled', true);
      CoursePostulation.accept(course_id, ids).then(function(data){
        event.target.setAttribute('disabled', false);
        _this.setState({response_accept: data, postulation_selected_ids: []});
        _this.props.onAccept(data)
      });
    }
  },
  getPostulationSelectedIds: function(){
    postulation_selected_ids = [];
    selectedOptions = this.refs.postulation_selected_ids.selectedOptions;
    for (var i = 0; i < selectedOptions.length; i++) {
      postulation_selected_ids.push(selectedOptions[i].value);
    }
    return postulation_selected_ids;
  },
  renderResponseAccept: function () {
    response_accept = this.state.response_accept;
    if(response_accept){
      errors = [];
      success = 0;
      for (var i = 0; i < response_accept.length; i++) {
        postulation = response_accept[i]
        if (postulation.errors.accept){
          errors.push('La postulación de ' + postulation.full_name + ' no pudo ser aceptada: ' + postulation.errors.accept);
        }else
          success++;
      }
      style = success == response_accept.length ? 'alert-success' : ( errors.length == response_accept.length ? 'alert-danger' : 'alert-warning' );
      return (
        <div className={'alert ' + style} role="alert">
          {function () {
             if(success == response_accept.length){
               return 'Postulaciones aceptadas exitosamente'
             }else if (success > 0 && errors.length > 0){
               return(
                 <div>
                   <span>Las postulaciones fueron aceptadas exitosamente, excepto las siguientes:</span>
                   <ul>
                     {function () {
                        return (
                          errors.map(function (error) {
                            return <li>{error}</li>
                          })
                        )
                      }()}
                   </ul>
                 </div>
               )
             }else{
               return(
                 <ul>
                   {function () {
                      return (
                        errors.map(function (error, index) {
                          return <li key={'postulation-error-' + index}>{error}</li>
                        })
                      )
                    }()}
                 </ul>
               )
             }
           }()}
        </div>
      )
    }
  },
  changePostulations: function (event) {
    this.setState({postulation_selected_ids: this.getPostulationSelectedIds()})
  },
  componentDidMount: function () {
    $('.chosen').change(this.changePostulations);
  },
  componentDidUpdate: function(){
    $('.chosen').trigger("chosen:updated");
  },
  render: function(){
    button_accept_disabled = false
    if(this.state.postulation_selected_ids.length == 0){
      button_accept_disabled = true;
    }
    return (
      <div>
        <div className='modal-header'>
          <span>Acceptar Postulaciones</span>
        </div>
        <div className='modal-body'>
          {this.renderResponseAccept()}
          <div className='form-group'>
            <label className='input_required'>Postulaciones</label>
            <select ref='postulation_selected_ids' data-placeholder="Seleccione postulaciones"
                    className='form-control chosen'
                    onChange={this.changePostulations}
                    multiple={true}
                    value={this.state.postulation_selected_ids}
            >
              {this.state.postulations.map(function(postulation, index) {
                 return (
                   <option key={'postulation-selected' + index} value={postulation.id}>{postulation.full_name}</option>
                 )
               })}
            </select>
          </div>
          <div className='form-group text-right'>
            <button type="button" className="btn btn-default" data-dismiss="modal" style={{marginRight: '5px'}}>Cerrar</button>
            <button type='button' disabled={button_accept_disabled} className='btn btn-success' onClick={this.accept}>Aceptar</button>
          </div>
        </div>
      </div>
    )
  }
})
