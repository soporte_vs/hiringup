var CoursePostulationExportForm = React.createClass({
  getInitialState: function(){
    if (!this.props.postulations) {
      _this = this;
      CoursePostulation.getPostulations(this.props.course_id).then(function (postulations) {
        _this.setState({postulations: postulations})
      });
    }
    return {
      postulation_selected_ids: this.props.postulation_selected_ids || [],
      postulations: this.props.postulations || [],
      response_export: null
    }
  },

  getPostulationSelectedIds: function(){
    postulation_selected_ids = [],
    selectedOptions = this.refs.postulation_selected_ids.selectedOptions;
    for (var i = 0; i < selectedOptions.length; i++){
      postulation_selected_ids.push(selectedOptions[i].value);
    }
    return postulation_selected_ids;
  },

  changePostulations: function(){
    this.setState({postulation_selected_ids: this.getPostulationSelectedIds()});
    if (this.getPostulationSelectedIds().length < this.state.postulations.length){
      this.refs.checkbox_select_all.checked = false;
    }else if (this.getPostulationSelectedIds().length == this.state.postulations.length){
      this.refs.checkbox_select_all.checked = true;
    }
  },
  accept: function(event){
    _this = this;
    ids = _this.getPostulationSelectedIds();

    postulations_ids_url = [];
    if (ids.toString() == this.getAllPostulationIds().toString()){
      postulations_ids_url.push("course_postulation_ids=*");
    }else if(ids.length > 0){
      for(var i = 0; i < ids.length; i++){
        postulations_ids_url.push("course_postulation_ids[]=" + ids[i]);
      }
    }
    url_encoded_postulation = postulations_ids_url.join("&");
    course_id = this.props.course_id;
    window.location.href = "/app/courses/"+ course_id +"/course_postulations/export?" + url_encoded_postulation;
  },
  componentDidMount: function () {
    $('.chosen').change(this.changePostulations);
  },
  handlerAll: function(event){
    if(event.target.checked){
      this.setState({postulation_selected_ids: this.getAllPostulationIds()});
    }else{
      this.setState({postulation_selected_ids: []});
    }
  },
  getAllPostulationIds: function () {
    return this.state.postulations.map(function(p, index){ return p.id });
  },
  componentDidUpdate: function(){
    $('.chosen').trigger("chosen:updated");
  },
  render: function(){
    _this = this;
    button_accept_disabled = false;
    if (this.state.postulation_selected_ids.length == 0)
      button_accept_disabled = true;


    return(
      <div>
        <div className="modal-header">
          <span>Exportar</span>
        </div>
        <div className="modal-body">
          <div className="form-group">
            <label className='input_required'>Postulaciones</label>
            <select ref='postulation_selected_ids'
                    data-placeholder='Seleccione postulaciones'
                    className='form-control chosen'
                    onChange={this.changePostulations}
                    multiple={true}
                    value={this.state.postulation_selected_ids}
            >
              {this.state.postulations.map(function(postulation, index){
                return(
                  <option key={'postulation-selected' + index} value={postulation.id}>{postulation.full_name}</option>
                )
              })}
            </select>
            <div className='form-group'>
              {function(){
                if (_this.state.postulations.length > 0){
                  return (
                    <div>
                      <input type="checkbox" onClick={_this.handlerAll} ref="checkbox_select_all" id="select-all"/> <label htmlFor="select-all">Seleccionar todos en el proceso</label>
                    </div>
                  )
                }else{
                  return <p>Cargando postulantes...</p>
                }
              }()}
            </div>
          </div>
          <div className="form-group text-right">
          <button type="button" className="btn btn-default" data-dismiss="modal" style={{marginRight: '5px'}}>Cerrar</button>
            <button type='button' disabled={button_accept_disabled} onClick={this.accept} className='btn btn-success'>Exportar</button>
          </div>
        </div>
      </div>
    )
  },
});
