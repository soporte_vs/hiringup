var PostulationHit = React.createClass({
  mixins: [ApplicantTools],
  drawPostulationAnswers: function () {
    postulation = this.props.postulation_hit._source;
    answers = postulation.answers;
    return(
      answers.map(function (answer, index) {
        return (
          <div className='row margin-top-20-px' key={answer.id + '_'+ index}>
            <div className='col-xs-12'>
              <strong className='__force-text_break-word'>{answer.question}</strong>
              <p className='__force-text_break-word'>{answer.answer}</p>
            </div>
          </div>
        )
      })
    )
  },
  drawPostulationStatus: function () {
    if (this.props.postulation_hit._source.state == 'accepted') {
      invitation_label = 'success';
      invitation_status = 'Aceptada';
    }else if(this.props.postulation_hit._source.state == 'rejected'){
      invitation_label = 'danger';
      invitation_status = 'Rechazada';
    }else {
      invitation_label = 'warning';
      invitation_status = 'Pendiente';
    }
    return (
      <span className={"inline-item label label-" + invitation_label}>{invitation_status}</span>
    )
  },
  drawLinkApplicant: function(){
    portal = this.props.postulation_hit._source.portal;
    postulationable_id = this.props.postulation_hit._source.postulationable_id;
    if (portal == 'Laborum'){
      return(
        <div className='row'>
          <div className='col-xs-12'>
            <a href={'/app/laborum/postulations/' + postulationable_id} target='_blank'>Ver postulación</a>
          </div>
        </div>
      )
    }else{
      return(
        <div className='row'>
          <div className='col-xs-12'>
            <a href={'/app/applicant/bases/' + postulation.applicant_id} target='_blank'>Ver perfil completo</a>
          </div>
        </div>
      )
    }
  },
  render: function(){
    _this = this;
    postulation = this.props.postulation_hit._source;
    tag_id = "collapsePostulation-" + postulation.id;
    blacklisted = this.props.postulation_hit._source.blacklisted;
    info_request_count = this.props.postulation_hit._source.info_request_count;
    identification_document_number = this.props.postulation_hit._source.identification_document_number || "No ingresó";
    identification_document_type = this.props.postulation_hit._source.identification_document_type || "No ingresó";
    identification_document_country = this.props.postulation_hit._source.identification_document_country || "No ingresó";
    return (
      <div className='enrollment_item clearfix padding-bottom-10-px' style={blacklisted ? {backgroundColor: '#F9C8C8'} : {}}>
        <div className='row padding-bottom-10-px padding-top-10-px '>
          <div className='col-xs-1 text-center padding-top-20-px'>
            <input type='checkbox' readOnly
                   checked={this.props.checked}
                   onClick={this.props.onClick}
                   value={postulation.id} />
          </div>
          <div className='col-xs-11 __with_collapsible-icons'>
            <div className='col-xs-12'>
              <a data-toggle="collapse" className='collapsed'
                 href={"#"+tag_id}
                 aria-expanded="false"
                 aria-controls={tag_id}>

                <div className="media">
                  <div className="media-left">
                    <img className="media-object img-rounded img-thumbnail" data-src="holder.js/64x64"
                          alt="64x64" data-holder-rendered="true"
                          style={{width: '64px', height: '64px'}}
                          src={postulation.avatar_url} />
                  </div>
                  <div className="media-body">
                    <h3 className="media-heading">
                      {this.drawCandidateType(postulation)}
                      &nbsp;
                      {this.drawReferenced(postulation)}
                      &nbsp;
                      {postulation.full_name}
                    </h3>
                    <span className="inline-item label label-primary">{postulation.portal}</span>
                    &nbsp;
                    {this.drawPostulationStatus()}
                    &nbsp;
                    {this.drawBlacklisted(blacklisted)}
                    &nbsp;
                    {this.drawUpdateInfoRequestCount(info_request_count)}
                  </div>
                </div>
              </a>
            </div>
          </div>
        </div>

        <div className="row collapse" id={tag_id}>
          <div className='col-xs-offset-1 col-xs-11'>
            <div className='col-xs-12'>
              <div className='row'>
                <div className="col-xs-12">
                  <div className="pull-left margin-bottom-5-px">
                    <strong>Fecha de Postulación: </strong>
                    <span>{moment(postulation.created_at).format('llll')}</span>
                  </div>
                </div>

                <div className='col-xs-6'>
                  <div className="pull-left margin-bottom-5-px">
                    <strong>Documento de identificación: </strong>
                    <span className="textsearch">{identification_document_number}</span>
                  </div>

                  <div className="pull-left margin-bottom-5-px">
                    <strong>Tipo de documento de identificación: </strong>
                    <span className="textsearch">{identification_document_type}</span>
                  </div>

                  <div className="pull-left margin-bottom-5-px">
                    <strong>País del documento de identificación: </strong>
                    <span className="textsearch">{identification_document_country}</span>
                  </div>

                </div>
                <div className='col-xs-6'>
                  <div className="pull-left margin-bottom-5-px">
                    <strong>Email: </strong>
                    <span>{postulation.email}</span>
                  </div>

                  <div className="pull-left margin-bottom-5-px">
                    <strong>Teléfono celular: </strong>
                    <span>{postulation.cellphone}</span>
                  </div>
                </div>
              </div>

              {this.drawLinkApplicant()}

              {this.drawPostulationAnswers()}
            </div>
          </div>
        </div>
      </div>
    )
  }
});
