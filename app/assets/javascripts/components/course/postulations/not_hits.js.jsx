var NoHitsPostulationsDisplay = React.createClass({
  render: function () {
    return (
      <div className='text-center col-xs-12 __white-background padding-top-20-px padding-bottom-20-px __shadow'>
        <p>
          <strong>
            Aún no han llegado postulaciones de algún portal de trabajo
          </strong>
        </p>
      </div>
    )
  }
});

