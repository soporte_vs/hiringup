var ResetPostulationForm = React.createClass({
  getInitialState: function () {
    if (!this.props.postulations) {
      _this = this;
      CoursePostulation.getPostulations(this.props.course_id).then(function (postulations) {
        _this.setState({postulations: postulations})
      });
    }
   return {
      response_reset: null,
      postulation_selected_ids: this.props.postulation_selected_ids || [],
      postulations: this.props.postulations || []
    }
  },
  resetPostulation: function (event) {
    _this = this;
    ids = this.getPostulationSelectedIds();
    course_id = this.props.course_id;
    if(ids.length > 0){
      event.target.setAttribute('disabled', true);
      CoursePostulation.reset(course_id, ids).then(function(data){
        event.target.setAttribute('disabled', false);
        _this.setState({response_reset: data, postulation_selected_ids: []});
        _this.props.onResetPostulation(data)
      });
    }
  },
  getPostulationSelectedIds: function(){
    postulation_selected_ids = [];
    selectedOptions = this.refs.postulation_selected_ids.selectedOptions;
    for (var i = 0; i < selectedOptions.length; i++) {
      postulation_selected_ids.push(selectedOptions[i].value);
    }
    return postulation_selected_ids;
  },
  renderResponseResetPostulation: function () {
    response_reset = this.state.response_reset;
    if(response_reset){
      errors = [];
      success = 0;
      for (var i = 0; i < response_reset.length; i++) {
        postulation = response_reset[i]
        if (postulation.errors.reset)
          errors.push(postulation.full_name + ' no pudo ser reiniciado: ' + postulation.errors.reset);
        else
          success++;
      }
      style = success == response_reset.length ? 'alert-success' : ( errors.length == response_reset.length ? 'alert-danger' : 'alert-warning' );
      return (
        <div className={'alert ' + style} role="alert">
          {function () {
             if(success == response_reset.length){
               return 'Postulaciones reiniciadas exitosamente'
             }else if (success > 0 && errors.length > 0){
               return(
                 <div>
                   <span>Las postulaciones fueron reiniciadas exitosamente, excepto las siguientes:</span>
                   <ul>
                     {function () {
                        return (
                          errors.map(function (error) {
                            return <li>{error}</li>
                          })
                        )
                      }()}
                   </ul>
                 </div>
               )
             }else{
               return(
                 <ul>
                   {function () {
                      return (
                        errors.map(function (error, index) {
                          return <li key={'postulation-error-' + index}>{error}</li>
                        })
                      )
                    }()}
                 </ul>
               )
             }
           }()}
        </div>
      )
    }
  },
  changePostulations: function (event) {
    this.setState({postulation_selected_ids: this.getPostulationSelectedIds()})
  },
  componentDidMount: function () {
    $('.chosen').change(this.changePostulations);
  },
  componentDidUpdate: function(){
    $('.chosen').trigger("chosen:updated");
  },
  render: function(){
    button_reset_disabled = false
    if(this.state.postulation_selected_ids.length == 0){
      button_reset_disabled = true;
    }
    return (
      <div>
        <div className='modal-header'>
          <span>Reiniciar Postulaciones</span>
        </div>
        <div className='modal-body'>
          {this.renderResponseResetPostulation()}
          <div className='form-group'>
            <label className='input_required'>Postulaciones</label>
            <select ref='postulation_selected_ids' data-placeholder="Seleccione postulaciones"
                    className='form-control chosen'
                    onChange={this.changePostulations}
                    multiple={true}
                    value={this.state.postulation_selected_ids}
            >
              {this.state.postulations.map(function(postulation, index) {
                 return (
                   <option key={'postulation-selected' + index} value={postulation.id}>{postulation.full_name}</option>
                 )
               })}
            </select>
          </div>
          <div className='form-group text-right'>
            <button type="button" className="btn btn-default" data-dismiss="modal" style={{marginRight: '5px'}}>Cerrar</button>
            <button type='button' disabled={button_reset_disabled} className='btn btn-danger' onClick={this.resetPostulation}>Aceptar</button>
          </div>
        </div>
      </div>
    )
  }
})
