//= require components/course/postulations/hit
//= require components/course/postulations/tool_bar
var SortingSelector = Searchkit.SortingSelector;

var PostulationHits = React.createClass({
  contextTypes: {
    searchkit: React.PropTypes.object,
    course_id: React.PropTypes.number,
  },
  getInitialState: function(){
    return { postulation_ids_checked: [] }
  },
  onClickChild: function (event) {
    postulation_id = event.target.value;
    if(event.target.checked){
      this.state.postulation_ids_checked.push(postulation_id);
    }else{
      index = this.state.postulation_ids_checked.indexOf(postulation_id);
      this.state.postulation_ids_checked.splice(index, 1);
    }
    this.setState({postulation_ids_checked: this.state.postulation_ids_checked});
  },
  onClick: function (event) {
    this.state.postulation_ids_checked = event.target.checked ? this.props.hits.map(function (hit) {
      return hit._source.id.toString();
    }) : [];
    this.setState({postulation_ids_checked: this.state.postulation_ids_checked});
  },
  onChecked: function () {
    a2 = this.props.hits;
    a1 = this.state.postulation_ids_checked;
    return a1.length == a2.length && a1.every(function(v,i) { return v == a2[i]._source.id})
  },
  callBackActions: function (data) {
    searchkit = this.context.searchkit

    delete searchkit.query;
    setTimeout(function(){
      searchkit.performSearch();
    }, 1000);

    // Se busca la ruta con la cual el componete de enrollments del proceso acutal
    // esta usando para hacerle una especie de touch y que el componete enrollments
    // refresque lo que muestra
    enrollments_route = HupeSearchkitRoutes.getCourseEnrollmentsSearchRoute(this.context.course_id)
    delete enrollments_route.query;
    setTimeout(function(){
      enrollments_route.performSearch();
    }, 1000);
  },
  render: function(){
    _this = this;
    checked = _this.onChecked();
    postulations = this.props.hits.map(function (hit) {
      return hit._source;
    });
    return (
      <div>
        <div className='panel __with_shadow'  style={{padding: '10px 0'}}>
          <div className='row'>
            <div className='col-xs-1 text-center'>
              <input type='checkbox' checked={checked} onClick={this.onClick}/>
            </div>
            <div className='col-xs-11'>
              <div className='col-xs-10'>
                <PostulationActions
                  course_id={this.context.course_id}
                  postulations={postulations}
                  postulation_ids_checked={this.state.postulation_ids_checked}
                  callBackActions={this.callBackActions}/>
              </div>
            </div>
          </div>
        </div>
        <div className='panel __with_shadow clearfix '>
          {this.props.hits.map(function (postulation_hit, index) {
            checked = _this.state.postulation_ids_checked.includes(postulation_hit._source.id.toString());
            return <PostulationHit
                      key={'PostulationHit-' + index}
                      postulation_hit={postulation_hit}
                      checked={checked}
                      onClick={_this.onClickChild}
                      callBackActions={_this.callBackActions}/>
          })}
        </div>
      </div>
    )
  }
});
