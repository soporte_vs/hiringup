//= require components/course/hit
//= require components/course/not_hit
//= require components/common/error_hit
var SearchkitProvider = Searchkit.SearchkitProvider;
var SearchBox = Searchkit.SearchBox;
var SelectedFilters = Searchkit.SelectedFilters;
var ResetFilters = Searchkit.ResetFilters;
var RefinementListFilter = Searchkit.RefinementListFilter;
var Hits = Searchkit.Hits;
var NoHits = Searchkit.NoHits;
var Pagination = Searchkit.Pagination;
var SortingSelector = Searchkit.SortingSelector;
var HitsStats = Searchkit.HitsStats;

var CourseStateItem = React.createClass({
  render: function() {
    switch(this.props.label){
      case ('opened'): label = 'Abierto'; break;
      case ('closed'): label = 'Cerrado'; break;
      default: label = 'Abierto'; break;
    }
    return (
      <div className={this.props.bemBlocks.option().state({selected: this.props.active}).mix(this.props.bemBlocks.container("item"))} onClick={this.props.onClick}>
        <input type="checkbox" data-qa="checkbox" readOnly="" checked={this.props.active} className={this.props.bemBlocks.option("checkbox")}/>
        <div className={this.props.bemBlocks.option("text")}>{label}</div>
        <div className={this.props.bemBlocks.option("count")}>{this.props.count}</div>
      </div>
    )
  }
});
var CourseSearch = React.createClass({
  render: function() {
    var facets = {
      'facets.view_more': 'Ver más',
      'facets.view_less': 'Ver menos',
      'facets.view_all': 'Ver todos'
    }
    var searchkit = HupeSearchkitRoutes.getCoursesSearchRoute();
    return (
      <SearchkitProvider searchkit={searchkit}>
        <div>
          <div className="col-md-3 margin-bottom-20-px">
            <div className="col-xs-12 __white-background __with_shadow" style={{padding: '20px'}}>
              <RefinementListFilter
                id="positions"
                title="Cargos"
                field="position.raw"
                operator="AND"
                size={5}
                translations={facets}
              />

              <br/>
              <RefinementListFilter
                id="regions"
                title="Regiones"
                field="state.raw"
                operator="AND"
                size={5}
                translations={facets}
              />

              <br/>
              <RefinementListFilter
                id="cities"
                title="Ciudades"
                field="city.raw"
                operator="AND"
                size={5}
                translations={facets}
              />

              <br/>
              <RefinementListFilter
                id="aasm_state"
                title="Estado del proceso"
                field="aasm_state.raw"
                operator="AND"
                size={5}
                translations={facets}
                itemComponent={CourseStateItem}
              />

              <br/>
              <RefinementListFilter
                id="tags"
                title="Etiquetas de proceso"
                field="tags.raw"
                operator="AND"
                size={5}
                translations={facets}
              />

              <br/>
              <RefinementListFilter
                id="type_enrollment"
                title="Tipo de flujo"
                field="type_enrollment.raw"
                operator="OR"
                size={5}
                translations={facets}
              />
              <br/>
              <RefinementListFilter
                id="contract_type"
                title="Tipo de contrato"
                field="contract_type.raw"
                operator="AND"
                size={5}
                translations={facets}
              />
              <br/>
              <RefinementListFilter
                id="engagement_origin"
                title="Origen de la contratación"
                field="engagement_origin.raw"
                operator="AND"
                size={5}
                translations={facets}
              />

              <ResetFilters translations={{'reset.clear_all': 'Limpiar filtros'}}/>
            </div>
          </div>

          <div className="col-md-9">
            <div className="col-xs-12">

              <div className='row panel __with_shadow' style={{padding: '10px'}}>
                <div className='col-xs-9'>
                  <div className='col-md-7'>
                    <SearchBox
                      autofocus={true}
                      searchOnChange={true}
                      queryOptions={{minimum_should_match: '100%'}}
                      translations={{'searchbox.placeholder': 'Buscar proceso'}}
                      queryFields={["title^10"]}
                      />
                  </div>

                  <div className='col-xs-5 padding-top-10-px'>
                    <HitsStats translations={
                        {'hitstats.results_found': '{hitCount} procesos encontrados en {timeTaken} ms'}
                      }
                    />
                  </div>
                </div>

                <div className='col-xs-3 text-right'>

                  <SortingSelector options={[
                    {label:"Título A-Z", field:"title", order:"asc"},
                    {label:"Título Z-A", field:"title", order:"desc"},
                    {label:"Fecha Reciente", field:"created_at", order:"desc", defaultOption: true},
                    {label:"Fecha Antigua", field:"created_at", order:"asc"}
                  ]}/>
                </div>
              </div>

              <div className='row margin-top-10-px'>
                <SelectedFilters/>
              </div>

              <div className='row margin-top-10-px'>
                <Hits hitsPerPage={10}
                  highlightFields={["title"]}
                  sourceFilter={["title"]}
                  itemComponent={CourseHit}/>

                <NoHits
                  component={NoHitsCourseDisplay}
                  errorComponent={NoHitsErrorDisplay}/>

                <Pagination translations={
                  {
                  "pagination.previous":"Anterior",
                  "pagination.next":"Siguiente",
                  }
                }
                  showNumbers={true}  className='__white-background __with_shadow' style={{'padding': '10px'}}/>

              </div>
            </div>
          </div>
        </div>
      </SearchkitProvider>
    )
  }
});

