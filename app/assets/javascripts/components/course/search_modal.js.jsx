var Hits = Searchkit.Hits;
var HitsStats = Searchkit.HitsStats;

var CourseSearchModal = React.createClass({
  render: function() {
    var facets = {
      'facets.view_more': 'Ver más',
      'facets.view_less': 'Ver menos',
      'facets.view_all': 'Ver todos'
    }
    var searchkit = HupeSearchkitRoutes.getCoursesSearchRoute();
    return (
      <SearchkitProvider searchkit={searchkit}>
        <div className="modal-header">
          <div className="modal-header">
            <div className='row'>
              <SearchBox
                searchOnChange={true}
                translations={{'searchbox.placeholder': 'Buscar proceso'}}
                queryFields={["title^10", "description^10"]}
                />
            </div>
            <div className='row margin-top-10-px'>
              <HitsStats translations={
                    {'hitstats.results_found': '{hitCount} procesos encontrados en {timeTaken} ms'}
                  }
                />
            </div>
          </div>

          <div className="modal-body">
            <Hits hitsPerPage={3}
              highlightFields={["title"]}
              sourceFilter={["title"]}
              itemComponent={CourseHit}/>
          </div>
        </div>
      </SearchkitProvider>
    )
  }
});

