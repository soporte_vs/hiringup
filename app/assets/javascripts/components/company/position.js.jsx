var OptionSelect = React.createClass({
  render: function() {
    return (
      <option value={this.props.value}>{this.props.text}</option>
    );
  }
});

var CompanyPositionSelect = React.createClass({
  getCencosByPosition: function (position_id) {
    var _this = this;
    // if position_id is undefined or empty or null not do ajax reqwes
    if(position_id)
      reqwest({
        url: '/app/company/positions/'.concat(position_id).concat('.json'),
        success: function (position) {
          _this.setState({company_cencos: position.company_positions_cencos});
        },
        error: function (err, data) {
          _this.setState({company_cencos: []});
        },
        complete: function(){
          Channel.publish('company/cencos', _this.state.company_cencos);
        }
      });
    else{
      _this.setState({company_cencos: [], position_id: ''});
      Channel.publish('company/cencos', []);
    }
  },
  handlerChange: function (event) {
    this.state.position_id = event.target.value;
    this.setState({position_id: this.state.position_id});
    this.getCencosByPosition(event.target.value);
  },
  getInitialState: function () {
    Channel.create('company/cencos');
    return {
      position_id: this.props.position_id,
      positions: this.props.positions ? this.props.positions : [],
      company_cencos: this.props.company_cencos
    }
  },
  componentDidMount: function () {
    $('.chosen-position').chosen({
      no_results_text: "No se encontró",
      allow_single_deselect: true,
      width: '100%'
    }).change(function (event) {
      this.handlerChange(event);
    }.bind(this));
  },
  componentDidUpdate: function () {
    $('.chosen-position').trigger("chosen:updated");
  },
  render: function () {
    label = this.props.label ? this.props.label : 'Cargo'
    return (
      <FieldWrapper errors={this.props.errors}>
        <label htmlFor={React.sanitizeName(this.props.name + '[company_position_id]')}>{label}</label>
        <select id={React.sanitizeName(this.props.name + '[company_position_id]')}
                name={this.props.name + '[company_position_id]'} value={this.state.position_id}
                onChange={this.handlerChange} className='chosen-position' data-placeholder='Seleccione cargo'
                required={this.props.required}>
          <option value=''></option>
          {this.state.positions.map(function (position, index){
            return <OptionSelect value={position.id} text={position.name}  key={'position_' + index} />
          })}
        </select>
      </FieldWrapper>
    )
  }
});
