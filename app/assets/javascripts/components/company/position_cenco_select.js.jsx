var OptionSelect = React.createClass({
  render: function() {
    return (
      <option value={this.props.value}>{this.props.text}</option>
    );
  }
});

var PositionCencoSelect = React.createClass({
  handlerChangeCenco: function (event) {
  },
  getInitialState: function () {
    return {
      value: this.props.selected,
      cencos: this.props.cencos ? this.props.cencos : []
    }
  },
  update_cenco: function (cencos) {
    this.setState({cencos: cencos});
    $(".chosen-select").trigger("chosen:updated");
  },
  componentDidMount: function () {
    Channel.subscribe('company/cencos', this.update_cenco);
  },
  render: function () {
    return (
      <div>
        <FieldWrapper errors={this.props.errors}>
          <div className='col-md-6'>
            <CompanyPositionSelect position_id={this.props.position_id}
                                 positions={this.props.positions}
                                 company_cencos={this.props.cencos}
                                 name={this.props.name + '[company_position_id]'}
                                 required={this.props.required} />
          </div>
          <div className='col-md-6'>
            <label htmlFor={React.sanitizeName(this.props.name)}>{this.props.label}</label>
            <select id={React.sanitizeName(this.props.name)}
                    name={this.props.name}
                    value={this.props.selected}
                    onChange={this.handlerChangeCenco}
                    className='form-control chosen-select'
                    data-placeholder='Seleccion centro de costo'
                    required={this.props.required}
                    >

              <option value=''></option>
              {this.state.cencos.map(function (cenco, index){
                return <OptionSelect value={cenco.id} text={cenco.name}  key={'cenco_' + index} />
              })}
            </select>
          </div>
        </FieldWrapper>
      </div>
    )
  }
});
