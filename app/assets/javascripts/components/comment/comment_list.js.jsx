var CommentList = React.createClass({
  getInitialState: function(){
    return {
      objectType: this.props.objectType,
      objectId: this.props.objectId,
      comments: this.props.comments || [],
      page: 1,
      isLast: false
    }
  },

  pushComment: function(comment){
    this.setState({comment: this.state.comments.unshift(comment)});
  },

  getComments: function(){
    _this = this;
    objectType = this.props.objectType;
    objectId = this.props.objectId;
    page = this.state.page;
    Comment.getByObject(objectType, objectId, page).then(function(comments){
      _this.setState({comments: comments});
    });
  },

  componentDidMount: function(){
    this.getComments();
  },

  changePage: function(event){
    _this = this;
    Comment.getByObject(objectType, objectId, this.state.page + 1 ).then(function(comments){
      newState = {};
      if (comments.length < 6){
        newState["isLast"] = true;
      }
      var comments = _this.state.comments.concat(comments);
      newState["comments"] = comments;
      newState["page"] = _this.state.page + 1;
      _this.setState(newState);
    });
  },

  drawViewMore: function(){
    if (!this.state.isLast && this.state.comments.length >= 6){
      var buttonViewMore = {
        marginTop: 10
      };
      return(
        <div>
          <button onClick={this.changePage} className="btn btn-block btn-info" disabled={this.state.isLast} style={buttonViewMore}>
            Ver más&nbsp;&nbsp;
            <span className="glyphicon glyphicon-arrow-down"></span>
          </button>
          <hr />
        </div>
      )
    }
  },

  renderList: function(){
    _this = this;
    comments = this.state.comments;

    var  avatarProfile = {
      height: 64,
      width: 64,
      borderRadius: 32,
      margin: "0 auto"
    };
    if(comments.length >= 1){
      return(
        <div className="margin-top-10-px">
          {function(){
            return(
              comments.map(function(comment, index){
                return (
                  <div key={'comment-' + index}>
                    <div className="media" >
                      <div className="media-body">
                        <div className="col-xs-2">
                          <a href={comment.user.profile_path}>
                            <img className="media-object" style={avatarProfile} src={comment.user.avatar} alt="..." />
                          </a>
                        </div>
                        <div className="col-xs-10 __no-padding">
                          <h4 className="media-heading">
                            <strong>
                              {comment.user.personal_information.first_name ? comment.user.personal_information.first_name + " " + comment.user.personal_information.last_name : comment.user.email}
                              <small>
                                &nbsp;({moment(comment.created_at).format("lll")})
                              </small>
                            </strong>
                          </h4>
                          <div className="text-justify __force-text_break-word">
                            {comment.content}
                          </div>
                        </div>
                      </div>
                    </div>
                    <hr />
                  </div>
                )
              })
            )
          }()}
          {this.drawViewMore()}
        </div>
      )
    } else {
      return (
        <div>
          <p className="small margin-top-10-px"><i>No existen comentarios, puedes crear uno utilizando la caja de más abajo</i></p>
          <hr />
        </div>
      )
    }
  },

  render: function(){
    return this.renderList();
  }
});
