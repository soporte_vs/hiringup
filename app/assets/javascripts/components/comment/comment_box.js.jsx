var CommentBox = React.createClass({
  mixins: [React.addons.LinkedStateMixin],
  getInitialState: function(){
    return {
      content: this.props.content,
      objectType: this.props.objectType,
      objectId: this.props.objectId,
      comment: null,
    }
  },

  accept: function(event){
    _this = this;
    if(this.isFormCompleted()){
      content = this.state.content;
      objectType = this.state.objectType;
      objectId = this.state.objectId;
      Comment.addComment(objectType, objectId, content).then(function(comment){
        _this.refs.commentText.value="";
        _this.refs.commentText.focus();
        _this.props.onComment(comment);
        _this.setState({content: ''})
      });
    }
  },
  isFormCompleted: function(){
    _this = this;
    content = this.state.content;
    return(
      content != null && content.trim().length > 0
    )
  },
  render: function(){
    buttonAcceptDisabled = true;
    if (this.isFormCompleted()){
      buttonAcceptDisabled = false;
    }
    return (
      <div>
        <div id="comment-box"></div>
        <div className='form-group'>
          <label className='input_required small'>Nuevo comentario</label>
          <textarea ref="commentText"
                    className="form-control"
                    valueLink={this.linkState('content')}
          ></textarea>
        </div>
        <div className="form-group text-right">
          <button type='button' className="btn btn-success btn-block" onClick={this.accept} disabled={buttonAcceptDisabled}>Comentar</button>
        </div>
      </div>
    )
  }
});
