var CommentAction = React.createClass({
  onComment: function(comment){
    this.refs.commentLists.pushComment(comment);
    this.setState({comment: null})
  },
  render: function() {
    return (
      <div id="comments">
        <div className="panel panel-default">
          <div className="panel-heading">Comentarios</div>
          <div className="panel-body">
            <div id="comment-list">
              <CommentList ref="commentLists"
                objectType={this.props.objectType}
                objectId={this.props.objectId}
              />
            </div>
            <div id="comment-box">
              <CommentBox
                objectType={this.props.objectType}
                objectId={this.props.objectId}
                onComment={this.onComment}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
});
