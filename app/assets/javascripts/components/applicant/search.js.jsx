//= require components/applicant/hit
//= require components/applicant/hits
//= require components/applicant/not_hit
//= require components/common/error_hit
var SearchkitProvider = Searchkit.SearchkitProvider;
var SearchBox = Searchkit.SearchBox;
var SelectedFilters = Searchkit.SelectedFilters;
var ResetFilters = Searchkit.ResetFilters;
var RefinementListFilter = Searchkit.RefinementListFilter;
var Hits = Searchkit.Hits;
var NoHits = Searchkit.NoHits;
var Pagination = Searchkit.Pagination;
var SortingSelector = Searchkit.SortingSelector;
var HitsStats = Searchkit.HitsStats;

var ApplicantGenderItem = React.createClass({
  render: function() {
    switch(this.props.label){
      case ('female'): label = 'Femenino'; break;
      case ('male'): label = 'Masculino'; break;
      case ('no_entry'): label = 'No ingresó'; break;
      default: label = this.props.label; break;
    }
    return (
      <div className={this.props.bemBlocks.option().state({selected: this.props.active}).mix(this.props.bemBlocks.container("item"))} onClick={this.props.onClick}>
        <input type="checkbox" data-qa="checkbox" readOnly="" checked={this.props.active} className={this.props.bemBlocks.option("checkbox")}/>
        <div className={this.props.bemBlocks.option("text")}>{label}</div>
        <div className={this.props.bemBlocks.option("count")}>{this.props.count}</div>
      </div>
    )
  }
});

var ApplicantBlacklistedItem = React.createClass({
  render: function() {
    switch(this.props.label){
      case ('true'): label = 'Sí'; break;
      case ('false'): label = 'No'; break;
      default: label = 'Sí'; break;
    }
    return (
      <div className={this.props.bemBlocks.option().state({selected: this.props.active}).mix(this.props.bemBlocks.container("item"))} onClick={this.props.onClick}>
        <input type="checkbox" data-qa="checkbox" readOnly="" checked={this.props.active} className={this.props.bemBlocks.option("checkbox")}/>
        <div className={this.props.bemBlocks.option("text")}>{label}</div>
        <div className={this.props.bemBlocks.option("count")}>{this.props.count}</div>
      </div>
    )
  }
});
var ApplicantIsEmployeeItem = React.createClass({
  render: function() {
    switch(this.props.label){
      case ('true'): label = 'Sí'; break;
      case ('false'): label = 'No'; break;
      default: label = 'No'; break;
    }
    return (
      <div className={this.props.bemBlocks.option().state({selected: this.props.active}).mix(this.props.bemBlocks.container("item"))} onClick={this.props.onClick}>
        <input type="checkbox" data-qa="checkbox" readOnly="" checked={this.props.active} className={this.props.bemBlocks.option("checkbox")}/>
        <div className={this.props.bemBlocks.option("text")}>{label}</div>
        <div className={this.props.bemBlocks.option("count")}>{this.props.count}</div>
      </div>
    )
  }
});

var ApplicantReferencesItem = React.createClass({
  render: function() {
    switch(this.props.label){
      case ('true'): label = 'Sí'; break;
      case ('false'): label = 'No'; break;
      default: label = 'No'; break;
    }
    return (
      <div className={this.props.bemBlocks.option().state({selected: this.props.active}).mix(this.props.bemBlocks.container("item"))} onClick={this.props.onClick}>
        <input type="checkbox" data-qa="checkbox" readOnly="" checked={this.props.active} className={this.props.bemBlocks.option("checkbox")}/>
        <div className={this.props.bemBlocks.option("text")}>{label}</div>
        <div className={this.props.bemBlocks.option("count")}>{this.props.count}</div>
      </div>
    )
  }
});

var ApplicantSearch = React.createClass({
  childContextTypes: {
    searchkit: React.PropTypes.object,
    course_id: React.PropTypes.number,
  },
  getChildContext: function() {
    return {
      searchkit: this.state.searchkit,
      course_id: this.state.course_id
    };
  },
  getInitialState: function () {
    var searchkit = HupeSearchkitRoutes.getApplicantsSearchRoute();
    return {
      searchkit: searchkit,
      course_id: this.props.course_id
    }
  },
  render: function() {
    var facets = {
      'facets.view_more': 'Ver más',
      'facets.view_less': 'Ver menos',
      'facets.view_all': 'Ver todos'
    }
    return (
      <SearchkitProvider searchkit={this.state.searchkit}>
        <div>
          <div className="col-md-3 margin-bottom-20-px">
            <div className="col-xs-12 __white-background __with_shadow" style={{padding: '20px'}}>
              <SearchBox
                autofocus={true}
                searchOnChange={true}
                queryOptions={{minimum_should_match: '100%'}}
                translations={{'searchbox.placeholder': 'Buscar candidato'}}
                queryFields={["full_name", "first_name", "last_name", "email", "identification_document_number"]}
              />

              <RefinementListFilter
                id="careers"
                title="Areas de estudio"
                field="careers.raw"
                operator="AND"
                size={5}
                translations={facets}
              />
              <br/>
              <RefinementListFilter
                id="institutions"
                title="Instituciones educacionales"
                field="institutions.raw"
                operator="AND"
                size={5}
                translations={facets}
              />

              <br/>
              <RefinementListFilter
                id="genders"
                title="Géneros"
                field="sex"
                operator="AND"
                size={5}
                translations={facets}
                itemComponent={ApplicantGenderItem}
              />

              <br/>
              <RefinementListFilter
                id="country"
                title="Nacionalidad"
                field="country.raw"
                operator="AND"
                size={5}
                translations={facets}
              />

              <br/>
              <RefinementListFilter
                id="regions"
                title="Regiones"
                field="state.raw"
                operator="AND"
                size={5}
                translations={facets}
              />

              <br/>
              <RefinementListFilter
                id="cities"
                title="Ciudades"
                field="city.raw"
                operator="AND"
                size={5}
                translations={facets}
              />

              <br/>
              <RefinementListFilter
                id="blacklist"
                title="En lista negra"
                field="blacklisted"
                operator="AND"
                size={5}
                translations={facets}
                itemComponent={ApplicantBlacklistedItem}
              />

              <br/>
              <RefinementListFilter
                id="study_type"
                title="Nivel de estudios"
                field="study_type"
                operator="OR"
                size={5}
                translations={facets}
              />

              <br/>
              <RefinementListFilter
                id="has_cv"
                title="¿Posee CV?"
                field="has_cv"
                operator="OR"
                size={5}
                translations={{"true":"Sí", "false":"No"}}
              />

              <br/>
              <RefinementListFilter
                id="related_courses"
                title="¿Tiene procesos asociados?"
                field="related_courses"
                operator="OR"
                size={5}
                translations={{"true":"Sí", "false":"No"}}
              />

              <br/>
              <RefinementListFilter
                id="availability_replacements"
                title="Disponibilidad para reemplazos"
                field="availability_replacements"
                operator="OR"
                size={5}
                translations={{"true":"Sí", "false":"No"}}
              />

              <br/>
              <RefinementListFilter
                id="availability_work_other_residence"
                title="Disponibilidad para trabajar en región distinta a la de residencia"
                field="availability_work_other_residence"
                operator="OR"
                size={5}
                translations={{"true":"Sí", "false":"No"}}
              />

              {/*<hr/>
                            <RefinementListFilter
                              id="references"
                              title="Posee referencias"
                              field="has_references"
                              operator="AND"
                              size={5}
                              translations={facets}
                              itemComponent={ApplicantReferencesItem}

                            />*/}

              <ResetFilters translations={{'reset.clear_all': 'Limpiar filtros'}}/>
            </div>
          </div>

          <div className="col-md-9">
            <div className="col-xs-12">
              <div className='row'>
                <Hits hitsPerPage={10}
                  highlightFields={["full_name"]}
                  sourceFilter={["full_name"]}
                  listComponent={ApplicantHits}/>

                <NoHits
                  component={NoHitsApplicantDisplay}
                  errorComponent={NoHitsErrorDisplay}/>

                <Pagination translations={
                    {
                        "pagination.previous":"Anterior",
                        "pagination.next":"Siguiente",
                    }
                  }
                  showNumbers={true}  className='__white-background __with_shadow' style={{'padding': '10px'}}/>

              </div>
            </div>
          </div>
        </div>
      </SearchkitProvider>
    )
  }
});

