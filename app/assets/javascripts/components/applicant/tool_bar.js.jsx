var ApplicantActions = React.createClass({
  enroll: function () {
    applicants = this.props.applicants;
    modal = document.querySelector('#modal .modal-content');

    invite_form = React.createElement(InviteApplicants, {
      applicant_selected_ids: this.props.applicants_ids_checked,
      course_id: this.props.course_id,
      applicants: applicants,
      onEnroll: this.props.callBackActions
    });

    ReactDOM.render(invite_form, modal);
    $('#modal').modal().on('shown.bs.modal', function () {
      $('.chosen').chosen();
      $('.chosen-course').chosen({
        allow_single_deselect: true
      });
    });

     $('#modal').on('hide.bs.modal', function () {
       modal_content = this.querySelector('#modal .modal-content');
       if(modal_content && modal_content.children[0])
         modal_content.removeChild(modal_content.children[0]);
     });
    //console.info(this.props.applicants_ids_checked)
    //console.log("ENROLL APPLICANT");
  },
  createEvent: function () {
    applicants = this.props.applicants;

    modal = document.querySelector('#modal .modal-content');
    event_form = React.createElement(ApplicantsEventForm, {
      course_id: this.props.course_id,
      applicant_selected_ids: this.props.applicants_ids_checked,
      applicants: applicants,
      onAddAppointment: this.props.callBackActions
    });

    ReactDOM.render(event_form, modal);
    $('#modal').modal().on('shown.bs.modal', function () {
      $('.chosen').chosen();
      google.maps.event.trigger(map, "resize");
    });

    $('#modal').on('hide.bs.modal', function () {
      modal_content = this.querySelector('#modal .modal-content');
      if(modal_content && modal_content.children[0])
        modal_content.removeChild(modal_content.children[0]);
    });
  },
  update: function () {
    applicants = this.props.applicants;
    modal = document.querySelector('#modal .modal-content');

    update_form = React.createElement(UpdateApplicantDataRequestForm, {
      applicant_selected_ids: this.props.applicants_ids_checked,
      applicants: applicants,
      onUpdate: this.props.callBackActions
    });

    ReactDOM.render(update_form, modal);
    $('#modal').modal().on('shown.bs.modal', function () {
      $('.chosen').chosen();
      $('.chosen-course').chosen({
        allow_single_deselect: true
      });
    });

     $('#modal').on('hide.bs.modal', function () {
       modal_content = this.querySelector('#modal .modal-content');
       if(modal_content && modal_content.children[0])
         modal_content.removeChild(modal_content.children[0]);
     });
    //console.info(this.props.applicants_ids_checked)
    //console.log("ENROLL APPLICANT");
  },
  send_mail: function () {
    applicants = this.props.applicants;
    modal = document.querySelector('#modal .modal-content');

    send_email_form = React.createElement(SendMassiveApplicantForm, {
      applicant_selected_ids: this.props.applicants_ids_checked,
      applicants: applicants,
      onSendEmail: this.props.callBackActions,
    });

    ReactDOM.render(send_email_form, modal);
    $('#modal').modal().on('shown.bs.modal', function () {
      $('.chosen').chosen();
      $('.chosen-course').chosen({
        allow_single_deselect: true
      });
    });

     $('#modal').on('hide.bs.modal', function () {
       modal_content = this.querySelector('#modal .modal-content');
       if(modal_content && modal_content.children[0])
         modal_content.removeChild(modal_content.children[0]);
     });
  },
  render: function () {
    _this = this;
/*        <button type='button' className='btn btn-default' onClick={_this.createEvent}>
          <span className='glyphicon glyphicon-share' style={{marginRight: '5px'}}></span>
          Agendar cita
        </button>*/
    return (
      <div className='btn-group'>
        <div className='btn-group'>
          <button type='button' className='btn btn-default' onClick={_this.enroll}>
            <span className='glyphicon glyphicon-share' style={{marginRight: '5px'}}></span>
              Asignar Proceso
          </button>

          <button type='button' className='btn btn-default' onClick={_this.update}>
            <span className='glyphicon glyphicon-envelope' style={{marginRight: '5px'}}></span>
              Solicitar actualización de datos
          </button>
          <button type='button' className='btn btn-default' onClick={_this.send_mail}>
            <span className='glyphicon glyphicon-send' style={{marginRight: '5px'}}></span>
              Enviar correo
          </button>
        </div>
      </div>

    )
  }
});
