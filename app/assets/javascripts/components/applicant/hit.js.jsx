var ApplicantHit = React.createClass({
  mixins: [ApplicantTools],
  drawEvents: function () {
    events = this.props.applicant_hit._source.events;
    callBackActions = this.props.callBackActions;
    if (events.length > 0){
      return (
        <div>
          <h3>Eventos agendados</h3>
          <ul>
            {events.map(function (event, index) {
              return <li key={'applicant-event-' + index} className='margin-bottom-10-px'><EventShow key={'applicant-event-'+index} event={event} callBackActions={callBackActions}/></li>
            })}
          </ul>
        </div>
      )
    }
  },
  render: function () {
    tag_id = "collapseApplicant-" + this.props.applicant_hit._source.id;
    birth_date = this.props.applicant_hit._source.birth_date ? moment(this.props.applicant_hit._source.birth_date).format("DD-MM-YYYY") : 'No ingresó';
    blacklisted = this.props.applicant_hit._source.blacklisted;
    info_request_count = this.props.applicant_hit._source.info_request_count;
    identification_document_number = this.props.applicant_hit._source.identification_document_number || "No ingresó";
    identification_document_type = this.props.applicant_hit._source.identification_document_type || "No ingresó";
    identification_document_country = this.props.applicant_hit._source.identification_document_country || "No ingresó";
    return (
      <div className='applicant_item clearfix padding-bottom-10-px padding-top-10-px' style={blacklisted ? {backgroundColor: '#F9C8C8'} : {}}>
        <div className='row padding-bottom-10-px padding-top-10-px '>
          { /* Primera Columna */ }
          <div className='col-xs-1 text-center padding-top-20-px'>
            <input type='checkbox' readOnly checked={this.props.checked} onClick={this.props.onClick} value={this.props.applicant_hit._source.id}/>
          </div>
          { /* Fin Primera Columna */ }
          { /* Segunda Columna */ }
          <div className='col-xs-11 __with_collapsible-icons'>
            <div className='col-xs-12'>
              <a data-toggle='collapse' className='collapsed'
                 href={"#"+tag_id}
                 aria-expanded='false'
                 aria-controls={tag_id}>

                <div className='media'>
                  <div className='media-left'>
                    <img className="media-object img-rounded img-thumbnail" data-src="holder.js/64x64" alt="64x64" src={this.props.applicant_hit._source.avatar} data-holder-rendered="true" style={{width: '64px', height: '64px'}}/>
                  </div>
                  <div className="media-body">
                    <h3 className="media-heading">
                      {this.drawCandidateType(this.props.applicant_hit._source)}
                      &nbsp;
                      {this.drawReferenced(this.props.applicant_hit._source)}
                      &nbsp;
                      {this.props.applicant_hit._source.full_name}
                    </h3>
                    &nbsp;
                    {this.drawBlacklisted(blacklisted)}
                    &nbsp;
                    {this.drawUpdateInfoRequestCount(info_request_count)}
                    <div className="row">
                      <div className='col-xs-10'>
                        <div className="progress margin-top-10-px margin-bottom-0-px">
                          <div aria-valuemax="100"
                               aria-valuemin="0"
                               aria-valuenow="12"
                               className="progress-bar progress-bar-primary progress-bar-striped"
                               role="progressbar"
                               style={{width: this.props.applicant_hit._source.profile_progress + "%"}}>
                            <span>{this.props.applicant_hit._source.profile_progress}% completado</span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  { /* Fin Media Body */ }
                </div>
                { /* Fin Media */ }
              </a>
            </div>
          </div>
          { /* Fin Segunda Columna */ }
        </div>

        <div className="row collapse" id={tag_id}>
          <div className='col-xs-offset-1 col-xs-10'>
            <div className='row'>

              <div className="pull-left margin-bottom-5-px">
                <span className="processtext">Documento de identificación</span>
                <span className="textsearch">{identification_document_number}</span>
              </div>

              <div className="pull-left margin-bottom-5-px">
                <span className="processtext">Tipo de documento de identificación</span>
                <span className="textsearch">{identification_document_type}</span>
              </div>

              <div className="pull-left margin-bottom-5-px">
                <span className="processtext">País del documento de identificación</span>
                <span className="textsearch">{identification_document_country}</span>
              </div>

              <div className="pull-left margin-bottom-5-px">
                <span className="processtext">Correo</span>
                <span className="textsearch">{this.props.applicant_hit._source.email}</span>
              </div>
              <div className="pull-left margin-bottom-5-px">
                <span className="processtext">Género</span>
                <span className="textsearch">{this.props.applicant_hit._source.sex}</span>
              </div>
              <div className="pull-left margin-bottom-5-px">
                <span className="processtext">Fecha de nacimiento</span>
                <span className="textsearch">{ birth_date }</span>
              </div>
              <div className="pull-left margin-bottom-5-px">
                <span className="processtext">Teléfono</span>
                <span className="textsearch">{ this.props.applicant_hit._source.landline ? this.props.applicant_hit._source.landline : 'No ingresó'}</span>
              </div>
              <div className="pull-left margin-bottom-5-px">
                <span className="processtext">Celular</span>
                <span className="textsearch">{this.props.applicant_hit._source.cellphone ? this.props.applicant_hit._source.cellphone : 'No ingresó'}</span>
              </div>
              <div className="pull-left margin-bottom-5-px">
                <span className="processtext">País</span>
                <span className="textsearch">{this.props.applicant_hit._source.country}</span>
              </div>
              <div className="pull-left margin-bottom-5-px">
                <span className="processtext">Región</span>
                <span className="textsearch">{this.props.applicant_hit._source.state}</span>
              </div>
              <div className="pull-left margin-bottom-5-px">
                <span className="processtext">Comuna</span>
                <span className="textsearch">{this.props.applicant_hit._source.city}</span>
              </div>
            </div>
            <div className="row">
              <div className="col-xs-11">
                <div className="col-xs-12">
                  <a className="clearfix margin-bottom-10-px" href={'/app/applicant/bases/' + this.props.applicant_hit._source.id}>
                    <p style={{'paddingLeft': '11px'}}>Ver perfil completo</p>
                  </a>
                </div>
              </div>
            </div>

            <div className="row">
              <div className="col-xs-12">
                {this.drawEvents()}
              </div>
            </div>

          </div>
        </div>
        { /* End Collapse */ }

      </div>
    )
  }
});
