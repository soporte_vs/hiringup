//= require components/applicant/tool_bar
var ApplicantHits = React.createClass({
  contextTypes: {
    searchkit: React.PropTypes.object,
    course_id: React.PropTypes.number,
  },
  childContextTypes: {
    applicants: React.PropTypes.array
  },
  getChildContext: function() {
    applicants = this.props.hits.map(function (applicant_hit) {
      return applicant_hit._source;
    })
    return {
      applicants: applicants,
    };
  },
  getInitialState: function(){
    return { applicants_ids_checked: [] }
  },
  onClickChild: function (event) {
    applicant_id = event.target.value;
    if(event.target.checked){
      this.state.applicants_ids_checked.push(applicant_id);
    } else{
      index = this.state.applicants_ids_checked.indexOf(applicant_id);
      this.state.applicants_ids_checked.splice(index, 1);
    }
    this.setState({applicants_ids_checked: this.state.applicants_ids_checked});
  },
  onClick: function (event) {
    this.state.applicants_ids_checked = event.target.checked ? this.props.hits.map(function (hit) {
      return hit._source.id.toString();
    }) : [];
    this.setState({applicants_ids_checked: this.state.applicants_ids_checked});
  },
  onChecked: function () {
    a2 = this.props.hits;
    a1 = this.state.applicants_ids_checked;
    return a1.length == a2.length && a1.every(function(v,i) { return v == a2[i]._source.id})
  },
  callBackActions: function (data) {
    searchkit = this.context.searchkit
    delete searchkit.query;
    setTimeout(function(){
      searchkit.performSearch();
     }, 1000);
  },
  render: function(){
    _this = this;
    checked = _this.onChecked();
    applicants = this.props.hits.map(function (hit) {
      return hit._source;
    });
    return (
      <div>
        <div className='row'>
          <div className='col-xs-12'>
            <div className='col-xs-12'>
              { /* Filtros y Orden */}
              <div className='row __white-background padding-top-15-px padding-bottom-15-px __with_shadow'>
                <div className='row'>
                  <div className='col-xs-1 text-center padding-top-5-px'>
                    <input type='checkbox' checked={checked} onClick={this.onClick}/>
                  </div>
                  <div className='col-xs-11'>
                    <div className='col-xs-8'>
                      <ApplicantActions
                        course_id={this.context.course_id}
                        applicants={applicants}
                        applicants_ids_checked={this.state.applicants_ids_checked}
                        callBackActions={this.callBackActions}
                      />

                    </div>
                    <div className='col-xs-4'>
                      <SortingSelector options={[
                        {label:"Nombre A-Z", field:"full_name", order:"asc"},
                        {label:"Nombre Z-A", field:"full_name", order:"desc"},
                        {label:"Fecha Reciente", field:"created_at", order:"desc", defaultOption: true},
                        {label:"Fecha Antigua", field:"created_at", order:"asc"}
                      ]} className='pull-right'/>
                    </div>
                  </div>
                </div>
              </div>

              <div className='row margin-top-10-px'>
                <SelectedFilters/>
              </div>

              { /* Hit */ }
              <div className="row __white-background margin-top-10-px __with_shadow">
                {this.props.hits.map(function (applicant_hit, index) {
                  checked = _this.state.applicants_ids_checked.includes(applicant_hit._source.id.toString());
                  return <ApplicantHit
                            key={'ApplicantHit-' + index}
                            applicant_hit={applicant_hit}
                            checked={checked}
                            onClick={_this.onClickChild}
                            callBackActions={_this.callBackActions}/>
                })}
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
});
