var NoHitsApplicantDisplay = React.createClass({
  render: function () {
    return (
      <div className='text-center col-xs-12 __white-background padding-top-20-px padding-bottom-20-px __with_shadow'>
        <p>
          <strong>
            No se encontraron candidatos para la búsqueda '{this.props.query}'!
          </strong>
        </p>
      </div>
    )
  }
});
