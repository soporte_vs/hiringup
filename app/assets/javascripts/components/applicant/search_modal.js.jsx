var Hits = Searchkit.Hits;
var HitsStats = Searchkit.HitsStats;

var ApplicantSearchModal = React.createClass({
  render: function() {
    var facets = {
      'facets.view_more': 'Ver más',
      'facets.view_less': 'Ver menos',
      'facets.view_all': 'Ver todos'
    }
    var searchkit = HupeSearchkitRoutes.getApplicantsSearchRoute();
    return (
      <SearchkitProvider searchkit={searchkit}>
        <div>
          <div className="modal-header">
            <div className='row'>
              <SearchBox
                searchOnChange={true}
                translations={{'searchbox.placeholder': 'Buscar candidato'}}
                queryFields={["full_name^10", "email"]}
                />
            </div>
            <div className='row margin-top-10-px'>
              <HitsStats translations={
                    {'hitstats.results_found': '{hitCount} candidatos encontrados en {timeTaken} ms'}
                  }
                />
            </div>
          </div>
          <div className="modal-body">
            <Hits hitsPerPage={3}
              highlightFields={["full_name"]}
              sourceFilter={["full_name"]}
              itemComponent={ApplicantHit}/>
          </div>
        </div>
      </SearchkitProvider>
    )
  }
});

