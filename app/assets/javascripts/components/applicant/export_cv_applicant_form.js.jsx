var ExportCvApplicantForm = React.createClass({

  mixins: [React.addons.LinkedStateMixin],
  getInitialState: function(){
    return {
      applicant_selected_ids: this.props.applicant_selected_ids || [],
      applicants: this.props.applicants || []
    }
  },
  getApplicantSelectedIds: function(){
    applicant_selected_ids = [];
    selectedOptions = this.refs.applicant_selected_ids.selectedOptions;
    for (var i = 0; i < selectedOptions.length; i++) {
      applicant_selected_ids.push(selectedOptions[i].value);
    }
    return applicant_selected_ids;
  },
  updateApplicantIds: function (event) {
    this.setState({applicant_selected_ids: this.getApplicantSelectedIds()})
  },
  componentDidMount: function () {
    $('.chosen').change(this.updateApplicantIds);
  },
  componentDidUpdate: function(){
    $('.chosen').trigger("chosen:updated");
  },
  accept: function(event){
    if(this.isFormCompleted()){
      _this = this;
      ids = _this.getApplicantSelectedIds();
      applicants_ids_url = [];
      for(var i = 0; i < ids.length; i++){
        applicants_ids_url.push("ids[]=" + ids[i]);
      }
      url_encoded_applicant = applicants_ids_url.join("&");
      window.location.href = "/app/applicant/bases/generate_massive_cvs.json?" + url_encoded_applicant
    }
  },
  isFormCompleted: function(){
    _this = this;
    applicants_id = this.state.applicant_selected_ids;
    return(
      applicants_id.length > 0
    )
  },
  render: function(){
    button_accept_disabled = false;
    if(!this.isFormCompleted()){
      button_accept_disabled = true;
    }
    return (
      <div>
        <div className='modal-header'>
          <span>Exportar CV</span>
        </div>
        <div className='modal-body'>
          <div className='form-group'>
            <label className='input_required'>Candidatos</label>
            <select ref="applicant_selected_ids" data-placeholder="Seleccione candidatos"
                    className='form-control chosen'
                    onChange={this.updateApplicantIds}
                    multiple={true}
                    value={this.state.applicant_selected_ids}>
              {this.state.applicants.map(function(applicant, index) {
                 return (
                   <option key={'applicant-select' + index} value={applicant.id}>{applicant.full_name}</option>
                 )
               })}
            </select>
          </div>
          <div className='form-group text-right'>
            <button type='button' className='btn btn-default' data-dismiss='modal' style={{marginRight: '5px'}}>Cerrar</button>
            <button type='button' className='btn btn-success' disabled={button_accept_disabled} onClick={this.accept}>Aceptar</button>
          </div>
        </div>
      </div>
    )
  }
});
