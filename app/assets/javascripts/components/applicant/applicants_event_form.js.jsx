var ApplicantsEventForm = React.createClass({
  mixins: [React.addons.LinkedStateMixin, EmailValidator, DateTimeRangeValidator],
  searchOtherApplicants: function(){
    other_applicant_ids = [];
    applicant_selected_ids = this.props.applicant_selected_ids;
    applicant_ids = this.props.applicants.map(function (applicant) {
      return applicant.id;
    });
    for (var i = 0; i < applicant_selected_ids.length; i++) {
      if(applicant_ids.indexOf(applicant_selected_ids[i]) == -1){
        other_applicant_ids.push(applicant_selected_ids[i]);
      }
    }
    if (other_applicant_ids.length > 0)
      Applicant.getByIds(other_applicant_ids).then(function (applicants) {
        for (var i = 0; i < applicants.length; i++) {
          if (applicant_ids.indexOf(applicants[i].id) == -1)
            _this.state.applicants.push(applicants[i]);
        }
        _this.setState({applicants: _this.state.applicants});
      });
  },
  getInitialState: function () {
    this.searchOtherApplicants();
    event = this.props.event;
    id = event ? event.id : '';
    starts_at = event ? event.starts_at : '';
    ends_at = event ? event.ends_at : '';
    address = event && event.address ? event.address : { name: '', lat: '', lng: ''};
    guests = event ? event.guests : [];
    manager_id = event ? event.manager_id : '';
    manager_name = event ? event.manager_name : '';
    manager_email = event ? event.manager_email : '';
    applicant_selected_ids = event ? event.applicant_ids : this.props.applicant_selected_ids || [];
    return {
      id: id,
      starts_at: starts_at,
      ends_at: ends_at,
      address: address,
      guests: guests,
      manager_id: manager_id,
      manager_name: manager_name,
      manager_email: manager_email,
      response_add_appointment: null,
      applicant_selected_ids: applicant_selected_ids,
      applicants: this.props.applicants || []
    }
  },
  accept: function (event) {
    if(this.isFormCompleted()){
      serialized_manager = { id: this.state.manager_id, name: this.state.manager_name, email: this.state.manager_email };
      ends_at = this.state.ends_at ? this.state.ends_at.format() : '';
      _this = this;
      if (this.state.id)
        Calendar.updateEventForApplicants(
          this.state.id,
          this.state.applicant_selected_ids,
          this.state.starts_at.format(),
          ends_at,
          this.state.address,
          this.state.guests,
          serialized_manager
        ).then(function (data) {
          _this.setState({response_add_appointment: data}, function () {
            $('#message-box').focus();
          });
          // _this.refs.manager_email_picker.clearField();
          // _this.refs.date_range_picker.clearDates();
          _this.props.onAddAppointment(data);
        });
      else
        Calendar.createEventForApplicants(
          this.state.applicant_selected_ids,
          this.state.starts_at.format(),
          ends_at,
          this.state.address,
          this.state.guests,
          serialized_manager
        ).then(function (data) {
          _this.setState({response_add_appointment: data}, function () {
            $('#message-box').focus();
          });
          // _this.refs.manager_email_picker.clearField();
          // _this.refs.date_range_picker.clearDates();
          _this.props.onAddAppointment(data);
        });
    }
  },
  renderResponseAddAppointment: function () {
    response_add_appointment = this.state.response_add_appointment;
    if(response_add_appointment) {
      errors = [];
      success = 0;
      for (var i = 0; i < response_add_appointment.length; i++) {
        applicant = response_add_appointment[i];
        if (applicant.errors.base)
          errors.push(applicant.full_name + ' no pudo ser agendado: ' + applicant.errors.course);
        else
          success++;
      }
      style = success == response_add_appointment.length ? 'alert-success' : ( errors.length == response_add_appointment.length ? 'alert-danger' : 'alert-warning' );
      return (
        <div className={'alert ' + style} role="alert">
          {function () {
             if(success == response_add_appointment.length){
               return 'Cita creada para los candidatos exitosamente'
             } else if (success > 0 && errors.length > 0) {
               return(
                 <div>
                   <span>Los candidatos fueron agendados exitosamente, excepto los siguientes:</span>
                   <ul>
                     {function () {
                        return (
                          errors.map(function (error) {
                            return <li>{error}</li>
                          })
                        )
                      }()}
                   </ul>
                 </div>
               )
             }else{
               return(
                 <ul>
                   {function () {
                      return (
                        errors.map(function (error, index) {
                          return <li key={'applicant-error-' + index}>{error}</li>
                        })
                      )
                    }()}
                 </ul>
               )
             }
           }()}
        </div>
      )
    }
  },
  changeApplicants: function (event) {
    this.state.applicant_selected_ids = [];
    for (var i = 0; i < event.target.selectedOptions.length; i++) {
      this.state.applicant_selected_ids.push(event.target.selectedOptions[i].value)
    }
    this.setState({applicant_selected_ids: this.state.applicant_selected_ids});
  },
  addGuest: function() {
    this.state.guests.push({id: '', name: '', email: ''});
    this.setState({guests: this.state.guests});
  },
  componentDidUpdate: function(){
    $('.chosen').trigger("chosen:updated");
  },
  componentDidMount: function () {
    $('.chosen').change(this.changeApplicants);
  },
  isFormCompleted: function () {
    _this = this;
    ids = this.state.applicant_selected_ids;
    address = this.state.address;
    manager_name = this.state.manager_name;
    manager_email = this.state.manager_email;
    guestFormsCompleted = this.state.guests.map(function(guest){
      return (guest.name != '' && _this.isValidEmail(guest.email));
    }).indexOf(false) == -1;
    return (
      ids.length > 0 &&
      address.name &&
      address.lat &&
      address.lng &&
      manager_name &&
      this.isDateTimeRangeValid() &&
      this.isValidEmail(manager_email) &&
      guestFormsCompleted
    );
  },
  nameGuestChange: function (i, value) {
    this.state.guests[i].name = value
    this.setState({ guests: this.state.guests })
  },
  emailGuestChange: function (i, value) {
    this.state.guests[i].email = value
    this.setState({ guests: this.state.guests })
  },
  removeGuest: function (index) {
    guest = this.state.guests[index];
    if(guest.id){
      this.state.guests[index]['_destroy'] = 1;
    }else{
      this.state.guests.splice(index, 1);
    }
    this.setState({guests: this.state.guests});
  },
  renderHeader: function () {
    title =  this.state.id ? 'Actualizar evento' : 'Agendar evento';
    return (
      <div className='modal-header'>
        <span>{title}</span>
      </div>
    )
  },
  render: function() {
    return (
      <div>
        {this.renderHeader()}
        <div className='modal-body'>
          <div id='message-box' tabIndex='0'>
            {this.renderResponseAddAppointment()}
          </div>
          <div className='form-group'>
            <label className='input_required'>Candidatos</label>
            <select ref="applicant_selected_ids" data-placeholder="Seleccione candidatos"
                    className='form-control chosen'
                    multiple={true}
                    value={this.state.applicant_selected_ids}>
              {this.state.applicants.map(function(applicant, index) {
                 return (
                   <option key={'applicant-selected' + index} value={applicant.id}>{applicant.full_name}</option>
                 )
               })}
            </select>
          </div>

          <div className='form-group row clearfix'>
            <div className='col-xs-6'>
              <label className='input_required'>Nombre del encargado</label>
              <input type='name' valueLink={this.linkState('manager_name')} className='form-control'/>
            </div>
            <div className='col-xs-6'>
              <EmailField
                  ref='manager_email_picker'
                  label={'Email del encargado'}
                  valueLink={this.linkState('manager_email')}
                  required={true} />
            </div>
          </div>

          <DateTimeRangePicker ref='date_range_picker' valueLinkStartsAt={this.linkState('starts_at')}
                               valueLinkEndsAt={this.linkState('ends_at')}/>

          <div className="form-group">
            <GoogleMapsSearchBox valueLink={this.linkState('address')}/>
          </div>

          <div className='form-group'>
            <button className="btn btn-primary margin-bottom-10-px" onClick={this.addGuest}>
              Agregar invitado
            </button>
            {this.state.guests.map(function (guest, index) {
              var nameGuestChange = {
                name: guest.name,
                requestChange: this.nameGuestChange.bind(null, index)
              }
              var emailGuestChange = {
                value: guest.email,
                requestChange: this.emailGuestChange.bind(null, index)
              }
              if (!guest._destroy)
                return (
                  <div key={'form-guest-' + index} className='form-group clearfix'>
                    <div className='col-xs-1 text-center' style={{paddingTop: '5px'}}>
                        <strong>{index + 1}</strong>
                    </div>
                    <div className='col-xs-5'>
                      <label className='input_required'>Nombre</label>
                      <input type='text' valueLink={nameGuestChange} defaultValue={guest.name} className='form-control' placeholder='Nombre'/>
                    </div>
                    <div className='col-xs-5'>
                      <EmailField label={'Email'} valueLink={emailGuestChange} email={guest.email} required={true}/>
                    </div>
                    <div className='col-xs-1'>
                      <label>&nbsp;</label>
                      <button type='button' className='btn btn-default btn-block' onClick={this.removeGuest.bind(null, index)}>
                        <span className='glyphicon glyphicon-trash'></span>
                      </button>
                    </div>
                  </div>
                );
            }, this)}
          </div>

          <div className='form-group text-right'>
            <button type="button" className="btn btn-default" data-dismiss="modal" style={{marginRight: '5px'}}>Cerrar</button>
            <button type='button' disabled={!this.isFormCompleted()} className='btn btn-success' onClick={this.accept}>Aceptar</button>
          </div>
        </div>
      </div>
    )
  }
});
