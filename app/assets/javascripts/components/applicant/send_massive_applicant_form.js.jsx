var SendMassiveApplicantForm = React.createClass({

  mixins: [React.addons.LinkedStateMixin],

  searchOtherApplicants: function(){
    other_applicant_ids = [];
    applicant_selected_ids = this.props.applicant_selected_ids;
    applicant_ids = this.props.applicants.map(function (applicant) {
      return applicant.id;
    });
    for (var i = 0; i < applicant_selected_ids.length; i++) {
      if(applicant_ids.indexOf(applicant_selected_ids[i]) == -1){
        other_applicant_ids.push(applicant_selected_ids[i]);
      }
    }
    if (other_applicant_ids.length > 0)
      Applicant.getByIds(other_applicant_ids).then(function (applicants) {
        for (var i = 0; i < applicants.length; i++) {
          if (applicant_ids.indexOf(applicants[i].id) == -1)
            _this.state.applicants.push(applicants[i]);
        }
        _this.setState({applicants: _this.state.applicants});
      });
  },
  getInitialState: function(){
    this.searchOtherApplicants();
    return {
      response: null,
      applicant_selected_ids: this.props.applicant_selected_ids || [],
      applicants: this.props.applicants || [],
      subject: '',
      message: ''
    }
  },
  getApplicantSelectedIds: function(){
    applicant_selected_ids = [];
    selectedOptions = this.refs.applicant_selected_ids.selectedOptions;
    for (var i = 0; i < selectedOptions.length; i++) {
      applicant_selected_ids.push(selectedOptions[i].value);
    }
    return applicant_selected_ids;
  },
  updateApplicantIds: function (event) {
    this.setState({applicant_selected_ids: this.getApplicantSelectedIds()})
  },
  componentDidMount: function () {
    $('.chosen').change(this.updateApplicantIds);
  },
  componentDidUpdate: function(){
    $('.chosen').trigger("chosen:updated");
  },
  changeSubject: function (event) {
    this.setState({subject: event.target.value});
  },
  changeMessage: function (event) {
    this.setState({message: event.target.value});
  },
  accept: function(event){
    if(this.isFormCompleted()){
      _this = this;
      event.target.setAttribute('disabled', true);
      Applicant.sendMailApplicants(
        this.getApplicantSelectedIds(),
        this.state.subject,
        this.state.message
      ).then(function(data){
        event.target.removeAttribute('disabled', false);
        _this.setState({
          response: data,
          applicant_selected_ids: [],
          subject: '',
          message: ''
        });
        _this.props.onSendEmail(data);
      });
    }
  },
  isFormCompleted: function(){
    _this = this;
    applicants_id = this.state.applicant_selected_ids;
    subject = this.state.subject;
    message = this.state.message;
    return(
      applicants_id.length > 0 &&
      subject.trim() != "" &&
      message.trim() != ""
    )
  },
  renderResponse: function(){
    _this = this;
    response_invite = this.state.response;
    if (response_invite){
      errors = [];
      success = 0;
      for (var i = 0; i < response_invite.length; i++){
        applicant = response_invite[i];
        if (applicant.errors.update){
          errors.push(applicant.full_name)
        }else{
          success++;
        }
      }
      style = success == response_invite.length ? 'alert-success' : (errors.length == response_invite.length ? 'alert-danger' : 'alert-warning');
      return(
        <div className={'alert ' + style} role="alert">
          {function() {
            if (success == response_invite.length){
              return (
                <div>
                  <span>Correo enviado exitosamente</span>
                </div>
              )
            }else if (success > 0 && errors.length > 0){
              return (
                <div>
                  <span>Correo enviado exitosamente, excepto a los siguientes candidatos:</span>
                  <ul>
                    {function(){
                      return (
                        errors.map(function(error){
                          return <li>{error}</li>
                        })
                      )
                    }()}
                  </ul>
                </div>
              )
            }else{
              return(
                <ul>
                  {function(){
                    return(
                      errors.map(function(error, index){
                        return <li key={'applicant-error-'+index}>{error}</li>
                      })
                    )
                  }()}
                </ul>
              )
            }
          }()}
        </div>
      )
    }
  },

  render: function(){
    button_accept_disabled = false;
    if(!this.isFormCompleted()){
      button_accept_disabled = true;
    }
    return (
      <div>
        <div className='modal-header'>
          <span>Enviar correo</span>
        </div>
        <div className='modal-body'>
          <div id="message-box" tabIndex='0'>
            {this.renderResponse()}
          </div>
          <div className='form-group'>
            <label className='input_required'>Candidatos</label>
            <select ref="applicant_selected_ids" data-placeholder="Seleccione candidatos"
                    className='form-control chosen'
                    onChange={this.updateApplicantIds}
                    multiple={true}
                    value={this.state.applicant_selected_ids}>
              {this.state.applicants.map(function(applicant, index) {
                 return (
                   <option key={'applicant-select' + index} value={applicant.id}>{applicant.full_name}</option>
                 )
               })}
            </select>
          </div>
          <div className='form-group'>
            <label className='input_required small'>Asunto</label>
            <input type='text'
                   className='form-control'
                   value={this.state.subject}
                   onChange={this.changeSubject}/>
          </div>
          <div className='form-group'>
            <label className='input_required small'>Mensaje</label>
              <textarea ref="commentText"
                    className='form-control'
                    value={this.state.message}
                    onChange={this.changeMessage}
              ></textarea>
          </div>
          <div className='form-group text-right'>
            <button type='button' className='btn btn-default' data-dismiss='modal' style={{marginRight: '5px'}}>Cerrar</button>
            <button type='button' className='btn btn-success' disabled={button_accept_disabled} onClick={this.accept}>Aceptar</button>
          </div>
        </div>
      </div>
    )
  }
});
