var DisposeForm = React.createClass({
  getInitialState: function () {
    if (!this.props.discard_reasons) {
      _this = this;
      Enrollment.getDiscardReasons().then(function (discard_reasons) {
        _this.setState({discard_reasons: discard_reasons})
      });
    }
    if (!this.props.enrollments) {
      _this = this;
      Course.getCandidates(this.props.course_id).then(function (enrollments) {
        _this.setState({enrollments: enrollments})
      });
    }
    return {
      response_dispose: null,
      discard_reasons: this.props.discard_reasons || [],
      enrollments: this.props.enrollments || [],
      enrollment_selected_ids: this.props.enrollment_selected_ids || [],
      observations: '',
      discard_reason_id: '',
      send_discarding_email: true
    }
  },
  getEnrollmentSelectedIds: function(){
    enrollment_selected_ids = []
    selectedOptions = this.refs.enrollment_selected_ids.selectedOptions;
    for (var i = 0; i < selectedOptions.length; i++) {
      enrollment_selected_ids.push(selectedOptions[i].value)
    }
    return enrollment_selected_ids;
  },
  changeDiscardReason: function (event) {
    this.setState({discard_reason_id: event.target.value});
  },
  changeObservations: function (event) {
    this.setState({observations: event.target.value});
  },
  changeSendDiscardingEmail: function (event) {
    this.setState({send_discarding_email: event.target.checked});
  },
  accept: function (event) {
    _this = this;
    if (this.enabledToAccept()){
      event.target.setAttribute('disabled', true);
      Enrollment.dispose(
        this.state.enrollment_selected_ids,
        this.state.discard_reason_id,
        this.state.observations,
        this.state.send_discarding_email
      ).then(function(data){
        event.target.setAttribute('disabled', false);
        _this.setState({
          response_dispose: data,
          enrollment_selected_ids: [],
          discard_reason_id: '',
          observations: '',
          send_discarding_email: true
        });
        _this.props.onDispose(data);
      });
      this.removeSelectedIds();
    }
  },
  enabledToAccept: function () {
    ids = this.state.enrollment_selected_ids;
    discard_reason_id = this.state.discard_reason_id;
    return (ids.length > 0 && discard_reason_id != '')
  },
  removeSelectedIds: function(){
    selected_ids = this.state.enrollment_selected_ids;
    size = selected_ids.length;
    selected_ids.splice(0, size);
    this.setState(
      {enrollment_selected_ids: selected_ids},
      this.changeEnrollments
    )
  },
  renderResponseDispose: function () {
    response_dispose = this.state.response_dispose;
    if(response_dispose){
      errors = []
      success = 0
      for (var i = 0; i < response_dispose.length; i++) {
        enrollment = response_dispose[i]
        if (enrollment.errors.discarding)
          errors.push(enrollment.full_name + ' no puede ser descartado: ' + enrollment.errors.discarding);
        else
          success++;
      }
      style = success == response_dispose.length ? 'alert-success' : ( errors.length == response_dispose.length ? 'alert-danger' : 'alert-warning' );
      return (
        <div className={'alert ' + style} role="alert">
          {function () {
            if(success == response_dispose.length){
              return 'Candidatos descartados exitosamente'
            }else if (success > 0 && errors.length > 0){
              return(
                <div>
                  <span>Los candidatos fueron descartados exitosamente, excepto los siguientes:</span>
                  <ul>
                    {function () {
                      return (
                        errors.map(function (error) {
                          return <li>{error}</li>
                        })
                      )
                    }()}
                  </ul>
                </div>
              )
            }else{
              return(
                <ul>
                  {function () {
                    return (
                      errors.map(function (error) {
                        return <li>{error}</li>
                      })
                    )
                  }()}
                </ul>
              )
            }
          }()}
        </div>
      )
    }
  },
  changeEnrollments: function (event) {
    this.setState({enrollment_selected_ids: this.getEnrollmentSelectedIds()})
  },
  componentDidMount: function () {
    $('.chosen').change(this.changeEnrollments);
    $('.chosen-discard').chosen({
      no_results_text: "No se encontró",
      allow_single_deselect: true,
      width: '100%'
    }).change(this.changeDiscardReason);
  },
  componentDidUpdate: function(){
    $('.chosen').trigger("chosen:updated");
    $('.chosen-discard').trigger("chosen:updated");
  },
  render: function(){
    var disabled_accept_button = !this.enabledToAccept();
    return(
      <div>
        <div className='modal-header'>
          <span>Descartar candidatos</span>
        </div>
        <div className='modal-body'>
          {this.renderResponseDispose()}
          <div className='form-group'>
            <label className='input_required'>Candidatos</label>
            <select ref='enrollment_selected_ids' data-placeholder="Seleccione candidatos"
                    className='form-control chosen'
                    onChange={this.changeEnrollments}
                    multiple={true}
                    value={this.state.enrollment_selected_ids}>
              {this.state.enrollments.map(function(enrollment, index) {
                return (
                  <option key={'enrollment-sected' + index} value={enrollment.id}>{enrollment.full_name}</option>
                )
              })}
            </select>
          </div>
          <div className='form-group'>
            <label className='input_required'>Razón de descarte</label>
            <select data-placeholder="Seleccione la razón de descarte"
                    className='form-control chosen-discard'
                    value={this.state.discard_reason_id}
                    onChange={this.changeDiscardReason}>
              <option value=''></option>
              {this.state.discard_reasons.map(function (discard_reason, index) {
                return <option
                          key={'discard_reason-option-' + index}
                          value={discard_reason.id}>
                          {discard_reason.name}
                        </option>
              })}
            </select>
          </div>
          <div className='form-group'>
            <label>Observaciones</label>
            <input type='text' className='form-control' value={this.state.observations} onChange={this.changeObservations}/>
          </div>

          <div className='form-group'>
            <label>
              <input style={{marginRight: '5px'}} type='checkbox' checked={this.state.send_discarding_email} onChange={this.changeSendDiscardingEmail}/>
              Enviar email de descarte
            </label>
          </div>
          <div className='form-group text-right'>
            <button type="button" className="btn btn-default" data-dismiss="modal" style={{marginRight: '5px'}}>Cerrar</button>
            <button type='button' disabled={disabled_accept_button} className='btn btn-danger' onClick={this.accept}>Aceptar</button>
          </div>
        </div>
      </div>
    )
  }
})
