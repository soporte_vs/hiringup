var UnEnrollForm = React.createClass({
  getInitialState: function(){
    if (!this.props.enrollments) {
      _this = this;
      Course.getCandidates(this.props.course_id).then(function (enrollments) {
        _this.setState({enrollments: enrollments})
      });
    }
    return {
      response_unenroll: null,
      enrollments: this.props.enrollments || [],
      enrollment_selected_ids: this.props.enrollment_selected_ids || [],

    }
  },

  getEnrollmentSelectedIds: function(){
    enrollment_selected_ids = [],
    selectedOptions = this.refs.enrollment_selected_ids.selectedOptions;
    for (var i = 0; i < selectedOptions.length; i++){
      enrollment_selected_ids.push(selectedOptions[i].value);
    }
    return enrollment_selected_ids;
  },

  accept: function (event) {
    _this = this;
    ids = this.getEnrollmentSelectedIds();
    if(ids.length > 0){
      event.target.setAttribute('disabled', true);
      Enrollment.unEnroll(ids).then(function(data){
        event.target.setAttribute('disabled', false);
        _this.setState({
          response_unenroll: data,
          enrollment_selected_ids: []
        });
        _this.props.onUnEnroll(data);
      });
    }
  },


  changeEnrollments: function(){
    this.setState({enrollment_selected_ids: this.getEnrollmentSelectedIds()});
  },

  renderResponseUnEnroll: function () {
    response_unenroll = this.state.response_unenroll;
    if(response_unenroll){
      errors = [];
      success = 0;
      for (var i = 0; i < response_unenroll.length; i++) {
        enrollment = response_unenroll[i]
        if (enrollment.errors.unenroll)
          errors.push(enrollment.full_name + ' ' + enrollment.errors.unenroll);
        else
          success++;
      }
      selected_enrollments = this.getEnrollmentSelectedIds().length
      unenrolled = selected_enrollments - errors.length
      style = success == response_unenroll.length ? 'alert-success' : ( errors.length == response_unenroll.length && unenrolled == 0 ? 'alert-danger' : 'alert-warning' );
      return (
        <div className={'alert ' + style} role="alert">
          {function () {
              if (unenrolled > 0 && errors.length > 0){
               return(
                 <div>
                   <span>Los candidatos fueron removidos exitosamente, excepto los siguientes:</span>
                   <ul>
                     {function () {
                        return (
                          errors.map(function (error) {
                            return <li>{error}</li>
                          })
                        )
                      }()}
                   </ul>
                 </div>
               )
             }else if(success == response_unenroll.length){
               return 'Los candidatos fueron removidos exitosamente'
             }else{
               return(
                 <ul>
                   {function () {
                      return (
                        errors.map(function (error, index) {
                          return <li key={'enrollment-error-' + index}>{error}</li>
                        })
                      )
                    }()}
                 </ul>
               )
             }
           }()}
        </div>
      )
    }
  },
  componentDidMount: function () {
    $('.chosen').change(this.changeEnrollments);
  },
  componentDidUpdate: function(){
    $('.chosen').trigger("chosen:updated");
  },
  render: function(){
    button_accept_disabled = false;
    if (this.state.enrollment_selected_ids.length == 0)
      button_accept_disabled = true;
    return(
      <div>
        <div className="modal-header">
          <span>Remover Candidato</span>
        </div>
        <div className="modal-body">
          {this.renderResponseUnEnroll()}
          <div className="form-group">
            <label className='input_required'>Candidatos</label>
            <select ref='enrollment_selected_ids'
                    data-placeholder='Seleccione candidatos'
                    className='form-control chosen'
                    onChange={this.changeEnrollments}
                    multiple={true}
                    value={this.state.enrollment_selected_ids} >
              {this.state.enrollments.map(function(enrollment, index){
                return(
                  <option key={'enrollment-selected' + index} value={enrollment.id}>{enrollment.full_name}</option>
                )
              })}
            </select>
          </div>
          <div className="form-group text-right">
          <button type="button" className="btn btn-default" data-dismiss="modal" style={{marginRight: '5px'}}>Cerrar</button>
            <button type='button' disabled={button_accept_disabled} onClick={this.accept} className='btn btn-success'>Remover Candidato</button>
          </div>
        </div>
      </div>
    )
  },
});
