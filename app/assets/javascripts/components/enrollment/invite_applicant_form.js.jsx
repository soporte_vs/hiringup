var InviteApplicants = React.createClass({

  mixins: [React.addons.LinkedStateMixin],

  searchOtherApplicants: function(){
    other_applicant_ids = [];
    applicant_selected_ids = this.props.applicant_selected_ids;
    applicant_ids = this.props.applicants.map(function (applicant) {
      return applicant.id;
    });
    for (var i = 0; i < applicant_selected_ids.length; i++) {
      if(applicant_ids.indexOf(applicant_selected_ids[i]) == -1){
        other_applicant_ids.push(applicant_selected_ids[i]);
      }
    }
    if (other_applicant_ids.length > 0)
      Applicant.getByIds(other_applicant_ids).then(function (applicants) {
        for (var i = 0; i < applicants.length; i++) {
          if (applicant_ids.indexOf(applicants[i].id) == -1)
            _this.state.applicants.push(applicants[i]);
        }
        _this.setState({applicants: _this.state.applicants});
      });
  },
  getInitialState: function(){
    this.searchOtherApplicants();
    if(!this.props.courses){
      _this = this;
      Course.getAll().then(function(courses){
        _this.setState({courses: courses});
      });
    }
    return {
      applicant_selected_ids: this.props.applicant_selected_ids || [],
      applicants: this.props.applicants || [],
      courses: this.props.courses || [],
      course_id: this.props.course_id,
      response: null
    }
  },
  getApplicantSelectedIds: function(){
    applicant_selected_ids = [];
    selectedOptions = this.refs.applicant_selected_ids.selectedOptions;
    for (var i = 0; i < selectedOptions.length; i++) {
      applicant_selected_ids.push(selectedOptions[i].value);
    }
    return applicant_selected_ids;
  },
  changeApplicants: function (event) {
    this.setState({applicant_selected_ids: this.getApplicantSelectedIds()})
  },
  changeCourse:function(event){
    this.setState({course_id: event.target.value})
  },
  componentDidMount: function () {
    $('.chosen').change(this.changeApplicants);
    $('.chosen-course').change(this.changeCourse);
  },
  componentDidUpdate: function(){
    $('.chosen').trigger("chosen:updated");
    $('.chosen-course').trigger("chosen:updated");
  },
  accept: function(event){
    if(this.isFormCompleted()){
      _this = this;
      applicantsIds = this.getApplicantSelectedIds();
      event.target.setAttribute('disabled', true);
      course_id = this.state.course_id;
      Applicant.enrollApplicants(applicantsIds, course_id).then(function(data){
        event.target.removeAttribute('disabled');
        _this.setState({response: data}, _this.props.onEnroll);
      })
    }
  },
  isFormCompleted: function(){
    _this = this;
    applicants_id = this.state.applicant_selected_ids;
    course_id = this.state.course_id
    return(
      applicants_id.length > 0 &&
      course_id
    )
  },
  drawCourseLink: function () {
    if (this.state.course_id){
      course_name = this.refs.course_select.selectedOptions[0].innerText;
      return (
        <div>
          <a href={'/app/courses/' + this.state.course_id}>{'Ir al proceso '+ course_name}</a>
        </div>
      )
    }
  },
  renderResponse: function(){
    _this = this;
    response_invite = this.state.response
    if (response_invite){
      errors = [];
      success = 0;
      for (var i = 0; i < response_invite.length; i++){
        applicant = response_invite[i];
        if (applicant.errors.enroll){
          errors.push(applicant.full_name + " : " + applicant.errors.enroll)
        }else{
          success++;
        }
      }
      style = success == response_invite.length ? 'alert-success' : (errors.length == response_invite.length ? 'alert-danger' : 'alert-warning');
      return(
        <div className={'alert ' + style} role="alert">
          {function() {
            if (success == response_invite.length){
              return (
                <div>
                  <span>Candidatos asignados al proceso exitosamente</span>
                  {_this.drawCourseLink()}
                </div>
              )
            }else if (success > 0 && errors.length > 0){
              return (
                <div>
                  <span>Candidatos asignados al proceso exitosamente, excepto los siguientes:</span>
                  <ul>
                    {function(){
                      return (
                        errors.map(function(error){
                          return <li>{error}</li>
                        })
                      )
                    }()}
                  </ul>
                  {_this.drawCourseLink()}
                </div>
              )
            }else{
              return(
                <ul>
                  {function(){
                    return(
                      errors.map(function(error, index){
                        return <li key={'applicant-error-'+index}>{error}</li>
                      })
                    )
                  }()}
                </ul>
              )
            }
          }()}
        </div>
      )
    }
  },

  deleteApplicant: function(event){
    applicant_id = event.currentTarget.dataset.applicantId;
    applicant_selected = this.state.applicant_selected_ids;

    index = applicant_selected.indexOf(applicant_id);
    if (index >= 0){
      applicant_selected.splice(index, 1)
    }
    this.setState(
      {applicant_selected_ids: applicant_selected},
      this.changeApplicants
    )
  },

  drawEnrolledApplicants: function () {
    var _this = this;
    var ids = _this.state.applicant_selected_ids
    var enrolled_applicant = this.state.applicants.filter(function(a){
      if(a.already_enrolled == true && ids.indexOf(a.id.toString()) >= 0 )
        return a;
    });

    if(enrolled_applicant.length > 0)
      return (
        <div>
          <div className="alert alert-danger">
            Los siguientes candidatos ya se encuentran participando en un proceso
          </div>
          <table className="table">
            <thead>
              <tr>
                <th>Candidatos</th>
                <th className="text-center">No asignar a este proceso</th>
              </tr>
            </thead>
            <tbody>
                {enrolled_applicant.map(function (applicant, index) {
                return(
                <tr>
                  <td>
                    {applicant.full_name}
                  </td>
                  <td className="col-md-4 text-center">
                    <button
                       onClick={_this.deleteApplicant}
                       className="btn btn-danger"
                       data-applicant-id={applicant.id}>
                       <span className="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </button>
                  </td>
                </tr>
                )
              })}
            </tbody>
          </table>
        </div>
      )
   },

  render: function(){
    button_accept_disabled = false;
    if(!this.isFormCompleted()){
      button_accept_disabled = true;
    }
    return (
      <div>
        <div className='modal-header'>
          <span>Asignar Proceso</span>
        </div>
        <div className='modal-body'>
          <div id="message-box" tabIndex='0'>
            {this.renderResponse()}
          </div>
          <div className='form-group'>
            <label className='input_required'>Candidatos</label>
            <select ref="applicant_selected_ids" data-placeholder="Seleccione candidatos"
                    className='form-control chosen'
                    onChange={this.changeApplicants}
                    multiple={true}
                    value={this.state.applicant_selected_ids}>
              {this.state.applicants.map(function(applicant, index) {
                 return (
                   <option key={'applicant-select' + index} value={applicant.id}>{applicant.full_name}</option>
                 )
               })}
            </select>
          </div>

          <div className='form-group'>
            <label className='input_required'>Proceso</label>
            <select data-placeholder='Seleccione Proceso'
                    onChange={this.changeCourse}
                    value={this.state.course_id}
                    ref='course_select'
                    className='chosen-course form-control'><option></option>{this.state.courses.map(function(course, index){
                return(
                  <option key={'course-select' + index} value={course.id}>{course.title}</option>
                )
              })}
            </select>
          </div>
          {this.drawEnrolledApplicants()}
          <div className='form-group text-right'>
            <button type='button' className='btn btn-default' data-dismiss='modal' style={{marginRight: '5px'}}>Cerrar</button>
            <button type='button' className='btn btn-success' disabled={button_accept_disabled} onClick={this.accept}>Aceptar</button>
          </div>
        </div>
      </div>
    )
  }

});
