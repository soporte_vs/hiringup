var NoHitsEnrollmentDisplay = React.createClass({
  contextTypes: {
    course_id: React.PropTypes.number
  },
  drawSearchURI: function () {
    params = [];
    uri = '/app/applicant/bases/search'
    if (this.props.query.length > 0)
      params.push("q=" + this.props.query)
    if (this.context.course_id)
      params.push("course_id=" + this.context.course_id)
    if (params.length > 0){
      uri_params = params.join('&')
      uri += '?' + uri_params
    }
    return uri;
  },
  render: function () {
    return (
      <div className='text-center col-xs-12 __white-background padding-top-20-px padding-bottom-20-px __shadow'>
        <p>
          <strong>
            No se encontraron candidatos en este proceso para la búsqueda {this.props.query ? "\'" + this.props.query + "\'" : null}!
          </strong>
        </p>
        <p>
          <a href={this.drawSearchURI()} className="btn btn-success">
            Buscar y agregar candidatos
          </a>
        </p>
      </div>
    )
  }
});

