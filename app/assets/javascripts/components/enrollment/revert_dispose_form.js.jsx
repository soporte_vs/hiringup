var RevertDisposeForm = React.createClass({
  getInitialState: function () {
    if (!this.props.enrollments) {
      _this = this;
      Course.getCandidates(this.props.course_id).then(function (enrollments) {
        _this.setState({enrollments: enrollments})
      });
    }
    return {
      response_revert_dispose: null,
      enrollments: this.props.enrollments || [],
      enrollment_selected_ids: this.props.enrollment_selected_ids || []
    }
  },
  getEnrollmentSelectedIds: function(){
    enrollment_selected_ids = []
    selectedOptions = this.refs.enrollment_selected_ids.selectedOptions;
    for (var i = 0; i < selectedOptions.length; i++) {
      enrollment_selected_ids.push(selectedOptions[i].value)
    }
    return enrollment_selected_ids;
  },
  accept: function (event) {
    _this = this;
    if (this.enabledToAccept()){
      event.target.setAttribute('disabled', true);
      Enrollment.revert_dispose(
        this.state.enrollment_selected_ids
      ).then(function(data){
        event.target.setAttribute('disabled', false);
        _this.setState({
          response_revert_dispose: data,
          enrollment_selected_ids: []
        });
        _this.props.onRevertDispose(data);
      });
    }
  },
  enabledToAccept: function () {
    ids = this.state.enrollment_selected_ids;
    return (ids.length > 0)
  },
  renderResponseRevertDispose: function () {
    response_revert_dispose = this.state.response_revert_dispose;
    if(response_revert_dispose){
      errors = []
      success = 0
      for (var i = 0; i < response_revert_dispose.length; i++) {
        enrollment = response_revert_dispose[i]
        if (enrollment.errors.discarding)
          errors.push(enrollment.full_name + ' no es posible revertir el descarte ' + enrollment.errors.discarding);
        else
          success++;
      }
      style = success == response_revert_dispose.length ? 'alert-success' : ( errors.length == response_revert_dispose.length ? 'alert-danger' : 'alert-warning' );
      return (
        <div className={'alert ' + style} role="alert">
          {function () {
            if(success == response_revert_dispose.length){
              return 'Reverso de descarte realizado con exito'
            }else if (success > 0 && errors.length > 0){
              return(
                <div>
                  <span>Los descartes fueron revertidos, excepto los siguientes:</span>
                  <ul>
                    {function () {
                      return (
                        errors.map(function (error) {
                          return <li>{error}</li>
                        })
                      )
                    }()}
                  </ul>
                </div>
              )
            }else{
              return(
                <ul>
                  {function () {
                    return (
                      errors.map(function (error) {
                        return <li>{error}</li>
                      })
                    )
                  }()}
                </ul>
              )
            }
          }()}
        </div>
      )
    }
  },
  changeEnrollments: function (event) {
    this.setState({enrollment_selected_ids: this.getEnrollmentSelectedIds()})
  },
  componentDidMount: function () {
    $('.chosen').change(this.changeEnrollments);
  },
  componentDidUpdate: function(){
    $('.chosen').trigger("chosen:updated");
  },
  render: function(){
    var disabled_accept_button = !this.enabledToAccept();
    return(
      <div>
        <div className='modal-header'>
          <span>Revertir descarte de candidatos</span>
        </div>
        <div className='modal-body'>
          {this.renderResponseRevertDispose()}
          <div className='form-group'>
            <label className='input_required'>Candidatos</label>
            <select ref='enrollment_selected_ids' data-placeholder="Seleccione candidatos"
                    className='form-control chosen'
                    onChange={this.changeEnrollments}
                    multiple={true}
                    value={this.state.enrollment_selected_ids}>
              {this.state.enrollments.map(function(enrollment, index) {
                return (
                  <option key={'enrollment-sected' + index} value={enrollment.id}>{enrollment.full_name}</option>
                )
              })}
            </select>
          </div>
          <div className='form-group text-right'>
            <button type="button" className="btn btn-default" data-dismiss="modal" style={{marginRight: '5px'}}>Cerrar</button>
            <button type='button' disabled={disabled_accept_button} className='btn btn-success' onClick={this.accept}>Aceptar</button>
          </div>
        </div>
      </div>
    )
  }
})
