var OnboardingTools = {
  changeDocumentGroup:function(event){
    this.state.onboarding.document_group_id = event.target.value;
    this.setState({onboarding: this.state.onboarding})
  },
  enabledToAcceptOnboardingForm: function () {
    document_group_id = this.state.onboarding.document_group_id;
    return document_group_id && document_group_id != ''
  },
  drawOnboardingForm: function(){
    return (
      <div className="form-group">
        <label className='input_required'>Grupo de Documentos</label>
          <select className="form-control"
                  defaultValue={this.props.onboarding.default_selected}
                  onChange={this.changeDocumentGroup}>
            <option value=''></option>
            {this.props.onboarding.document_groups.map(function(group, index){
              return <option
                key={'type_document_descriptor-' + index}
                value={group.id}>
                {group.name}
              </option>;
            })}
        </select>
      </div>
    )
  }
}
