var NextStageDynamicForm = React.createClass({
  mixins: [
    React.addons.LinkedStateMixin,
    EnrollmentTools,
    OnboardingTools
  ],
  getRemoteEnrollments: function () {
    _this = this;
    Course.getCandidates(this.props.course_id).then(function (enrollments) {
      _this.setState({enrollments: enrollments}, _this.updateSelectedEnrollments);
    });
  },
  getInitialState: function () {
    if (!this.props.enrollments) {
      this.getRemoteEnrollments();
    }
    return {
      response_next_stage: null,
      enrollment_selected_ids: this.props.enrollment_selected_ids || [],
      enrollments_status_stage: [],
      types_stage: [
        {name: 'Entrevista', value: 'Interview::Stage', editable: true},
        {name: 'Entrevista Inicial', value: 'ManagerInterview::Stage', editable: false},
        {name: 'Prueba Técnica', value: 'TechnicalTest::Stage', editable: false},
        {name: 'Entrevista Grupal', value: 'GroupInterview::Stage', editable: false},
        {name: 'Test', value: 'Test::Stage', editable: false},
        {name: 'Evaluación por competencias / Psicolaboral', value: 'PsycholaboralEvaluation::Stage', editable: false},
        {name: 'Entrevista Final', value: 'FinalInterview::Stage', editable: false},
        {name: 'Seleccionado', value: 'Offer::Stage', editable: false},
        {name: 'Documentación', value: 'Onboarding::Stage', editable: false},
        {name: 'Ingreso', value: 'Engagement::Stage', editable: false}
      ],
      type_stage: '',
      name_stage: '',
      is_editable: false,
      enrollments: this.props.enrollments || [],
      onboarding: {document_group_id: this.props.onboarding.default_selected}
    };
  },
  accept: function (event) {
    _this = this;
    if(this.enabledToAccept()){
      name_stage = this.state.name_stage;
      type_stage = this.state.type_stage;
      event.target.setAttribute('disabled', true);
      args = this.getArgsStage();
      ids = this.getEnrollmentSelectedIds();
      Enrollment.nextStage(ids, args, name_stage, type_stage).then(function(data){
        event.target.setAttribute('disabled', false);
        _this.setState({
          response_next_stage: data,
          enrollment_selected_ids: [],
          type_stage: '',
          name_stage: '',
          enrollments_status_stage: []
        });
        _this.getRemoteEnrollments();
        _this.props.onNextStage(data);
     });
    }
  },
  getEnrollmentSelectedIds: function(){
    enrollment_selected_ids = [];
    selectedOptions = this.refs.enrollment_selected_ids.selectedOptions;
    for (var i = 0; i < selectedOptions.length; i++) {
      enrollment_selected_ids.push(selectedOptions[i].value);
    }
    return enrollment_selected_ids;
  },
  renderResponseNextStage: function () {
    response_next_stage = this.state.response_next_stage;
    if(response_next_stage){
      errors = [];
      success = 0;
      for (var i = 0; i < response_next_stage.length; i++) {
        enrollment = response_next_stage[i];
        if (enrollment.errors.stage)
          errors.push(enrollment.full_name + ' no puedo ser cambiado de etapa: ' + enrollment.errors.stage);
        else
          success++;
      }
      style = success == response_next_stage.length ? 'alert-success' : ( errors.length == response_next_stage.length ? 'alert-danger' : 'alert-warning' );
      return (
        <div className={'alert ' + style} role="alert">
          {function () {
            if(success == response_next_stage.length){
              return 'Candidatos cambiados de etapa exitosamente';
            }else if (success > 0 && errors.length > 0){
              return(
                <div>
                  <span>Los candidatos fueron cambiados de etapa exitosamente, excepto los siguientes:</span>
                  <ul>
                    {function () {
                      return (
                        errors.map(function (error) {
                          return <li>{error}</li>;
                        })
                      )
                    }()}
                  </ul>
                </div>
              )
            }else{
              return(
                <ul>
                  {function () {
                    return (
                      errors.map(function (error, index) {
                        return <li key={'enrollment-error-' + index}>{error}</li>
                      })
                    );
                  }()}
                </ul>
              );
            }
          }()}
        </div>
      );
    }
  },
  changeEnrollments: function (event) {
    this.setState({
      enrollment_selected_ids: this.getEnrollmentSelectedIds()
    }, this.updateSelectedEnrollments);
  },
  getArgsStage: function(stage){
    _this = this;
    // Se obtienen los argumentos dentro de un hash el cual es enviado al controlador
    args = {};
    args["enrollments_status_stage"] = this.getEnrollmentStatusStage();

    switch(this.state.type_stage) {
    case "Onboarding::Stage":
      args["document_group"] =  _this.state.onboarding.document_group_id;
    }
    return args;
  },
  changeTypeStage: function(event){
    name_stage = event.target.selectedOptions[0].innerText;
    is_editable = event.target.selectedOptions[0].dataset.isEditable;
    this.setState({type_stage: event.target.value, name_stage: name_stage, is_editable: is_editable});
  },
  componentDidMount: function () {
    $('.chosen').change(this.changeEnrollments);
  },
  componentDidUpdate: function(){
    $('.chosen').trigger("chosen:updated");
  },
  enabledToAccept: function () {
    ids = this.state.enrollment_selected_ids;
    type_stage = this.state.type_stage;
    name_stage = this.state.name_stage;
    enrollment_stages = this.state.enrollments_status_stage;
    return (
      ids.length > 0 &&
      type_stage != '' &&
      name_stage &&
      enrollment_stages.length > 0 &&
      this.enabledToAcceptStageForm()
    )
  },
  enabledToAcceptStageForm: function () {
    if (this.state.type_stage == "Onboarding::Stage"){
      return this.enabledToAcceptOnboardingForm();
    }
    return true
  },
  drawStageForm: function(){
    if (this.state.type_stage == "Onboarding::Stage"){
      return this.drawOnboardingForm();
    }
  },
  render: function () {
    _this = this;
    button_accept_disabled = !this.enabledToAccept();
    errors_type_stage = [];
    if (this.state.type_stage == '' && this.state.enrollment_selected_ids.length > 0){
      errors_type_stage.push('Debe Seleccionar la etapa a donde pasarán los candidatos seleccionados');
    }
    showClass = this.state.type_stage == '' || this.state.is_editable == 'false' ? 'hide' : '';
    return (
      <div>
        <div className='modal-header'>
          <span>Cambiar de etapa</span>
        </div>
        <div className='modal-body'>
          {this.renderResponseNextStage()}
          <div className='form-group'>
            <label className='input_required'>Candidatos</label>
            <select ref='enrollment_selected_ids' data-placeholder="Seleccione candidatos"
                    className='form-control chosen'
                    onChange={this.changeEnrollments}
                    multiple={true}
                    value={this.state.enrollment_selected_ids}>
              {this.state.enrollments.map(function(enrollment, index) {
              if (enrollment.next_stage == null){
                  next_stage = "(" + enrollment.current_stage + ")";
                }
                else{
                  next_stage = "(De: " + enrollment.current_stage + " A: " + _this.state.name_stage + ")";
                }
                return (
                  <option key={'enrollment-sected' + index} value={enrollment.id}>{enrollment.full_name + ' ' + next_stage}</option>
                );
              })}
            </select>
          </div>
          {this.drawApproveStageForm()}
          <FieldWrapper errors={errors_type_stage}>
            <div className='form-group'>
              <label className='input_required'>Siguiente etapa</label>
              <select className='form-control' value={this.state.type_stage} onChange={this.changeTypeStage}>
                <option value=''></option>
                {this.state.types_stage.map(function (type_stage, index) {
                  return <option
                            key={'type_stage-option-' + index}
                            value={type_stage.value}
                            data-is-editable={type_stage.editable}>
                            {type_stage.name}
                          </option>;
                })}
              </select>
            </div>
          </FieldWrapper>
          {this.drawStageForm()}
          <div className={'form-group ' + showClass}>
            <label htmlFor='enrollment_stage_name'>Nombre de la etapa</label>
            <input type='text' id='enrollment_stage_name_' className='form-control' valueLink={this.linkState('name_stage')} autoComplete='true'></input>
          </div>
          <div className='form-group text-right margin-top-10-px'>
            <button type="button" className="btn btn-default" data-dismiss="modal" style={{marginRight: '5px'}}>Cerrar</button>
            <button type='button' disabled={button_accept_disabled} className='btn btn-success' onClick={this.accept}>Aceptar</button>
          </div>
        </div>
      </div>
    );
  }
});
