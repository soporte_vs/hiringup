var NextStageForm = React.createClass({
  mixins: [EnrollmentTools],
  getInitialState: function () {
    if (!this.props.enrollments) {
      this.updateEnrollments();
    }
    return {
      response_next_stage: null,
      enrollment_selected_ids: this.props.enrollment_selected_ids || [],
      enrollments_status_stage: [],
      enrollments: this.props.enrollments || [],
    };
  },
  accept: function (event) {
    _this = this;
    ids = this.getEnrollmentSelectedIds();
    args = this.getArgsStage();
    enrollments_status_stage = this.getEnrollmentStatusStage();
    if(enrollments_status_stage.length > 0){
      event.target.setAttribute('disabled', true);
      Enrollment.nextStage(ids, args).then(function(data){
        event.target.setAttribute('disabled', false);
        _this.setState({
          response_next_stage: data,
          enrollment_selected_ids: [],
          enrollments_status_stage: []
        });
        _this.props.onNextStage(data);
      });
      this.updateEnrollments();
    }
  },
  getArgsStage: function(){
    _this = this;
    args = {}
    args["enrollments_status_stage"] = this.getEnrollmentStatusStage();

    return args
  },
  updateEnrollments: function () {
    _this = this;
    Course.getCandidates(this.props.course_id).then(function (enrollments) {
      _this.setState({enrollments: enrollments}, _this.updateSelectedEnrollments);
    });
  },
  getEnrollmentSelectedIds: function(){
    enrollment_selected_ids = [];
    selectedOptions = this.refs.enrollment_selected_ids.selectedOptions;
    for (var i = 0; i < selectedOptions.length; i++) {
      enrollment_selected_ids.push(selectedOptions[i].value);
    }
    return enrollment_selected_ids;
  },
  renderResponseDispose: function () {
    response_next_stage = this.state.response_next_stage;
    if(response_next_stage){
      errors = [];
      success = 0;
      for (var i = 0; i < response_next_stage.length; i++) {
        enrollment = response_next_stage[i];
        if (enrollment.errors.stage)
          errors.push(enrollment.full_name + ' no puedo ser cambiado de etapa: ' + enrollment.errors.stage);
        else
          success++;
      }
      style = success == response_next_stage.length ? 'alert-success' : ( errors.length == response_next_stage.length ? 'alert-danger' : 'alert-warning' );
      return (
        <div className={'alert ' + style} role="alert">
          {function () {
            if(success == response_next_stage.length){
              return 'Candidatos cambiados de etapa exitosamente';
            }else if (success > 0 && errors.length > 0){
              return(
                <div>
                  <span>Los candidatos fueron cambiados de etapa exitosamente, excepto los siguientes:</span>
                  <ul>
                    {function () {
                      return (
                        errors.map(function (error) {
                          return <li>{error}</li>;
                        })
                      );
                    }()}
                  </ul>
                </div>
              );
            }else{
              return(
                <ul>
                  {function () {
                    return (
                      errors.map(function (error, index) {
                        return <li key={'enrollment-error-' + index}>{error}</li>;
                      })
                    );
                  }()}
                </ul>
              );
            }
          }()}
        </div>
      );
    }
  },
  changeEnrollments: function (event) {
    this.setState({enrollment_selected_ids: this.getEnrollmentSelectedIds()}, this.updateSelectedEnrollments);
  },
  componentDidMount: function () {
    $('.chosen').change(this.changeEnrollments);
  },
  componentDidUpdate: function(){
    $('.chosen').trigger("chosen:updated");
  },
  render: function () {
    button_accept_disabled = false;
    if(this.getEnrollmentStatusStage().length == 0){
      button_accept_disabled = true;
    }
    return (
      <div>
        <div className='modal-header'>
          <span>Cambiar de etapa</span>
        </div>
        <div className='modal-body'>
          {this.renderResponseDispose()}
          <div className='form-group'>
            <label className='input_required'>Candidatos</label>
            <select ref='enrollment_selected_ids' data-placeholder="Seleccione candidatos"
                    className='form-control chosen'
                    onChange={this.changeEnrollments}
                    multiple={true}
                    value={this.state.enrollment_selected_ids}>
              {this.state.enrollments.map(function(enrollment, index) {
                if (enrollment.next_stage == null){
                  next_stage = "(" + enrollment.current_stage + ")";
                }
                else{
                  next_stage = "(De: " + enrollment.current_stage + " A: " + enrollment.next_stage + ")";
                }
                return (
                  <option key={'enrollment-sected' + index} value={enrollment.id}>{enrollment.full_name + ' ' + next_stage}</option>
                );
              })}
            </select>
          </div>
          {this.drawApproveStageForm()}
          <div className='form-group text-right margin-top-10-px'>
            <button type="button" className="btn btn-default" data-dismiss="modal" style={{marginRight: '5px'}}>Cerrar</button>
            <button type='button' disabled={button_accept_disabled} className='btn btn-success' onClick={this.accept}>Aceptar</button>
          </div>
        </div>
      </div>
    );
  }
});
