var EnrollmentExportForm = React.createClass({
  getInitialState: function(){
    if (!this.props.enrollments) {
      _this = this;
      Course.getCandidates(this.props.course_id).then(function (enrollments) {
        _this.setState({enrollments: enrollments});
      });
    }
    return {
      enrollment_selected_ids: this.props.enrollment_selected_ids || [],
      enrollments: this.props.enrollments || [],
      response_export: null,
    }
  },

  getEnrollmentSelectedIds: function(){
    enrollment_selected_ids = [],
    selectedOptions = this.refs.enrollment_selected_ids.selectedOptions;
    for (var i = 0; i < selectedOptions.length; i++){
      enrollment_selected_ids.push(selectedOptions[i].value);
    }
    return enrollment_selected_ids;
  },

  changeEnrollments: function(){
    this.setState({enrollment_selected_ids: this.getEnrollmentSelectedIds()});
    if (this.getEnrollmentSelectedIds().length < this.state.enrollments.length){
      this.refs.checkbox_select_all.checked = false;
    }else if (this.getEnrollmentSelectedIds().length == this.state.enrollments.length){
      this.refs.checkbox_select_all.checked = true;
    }
  },
  accept: function(event){
    _this = this;
    ids = _this.getEnrollmentSelectedIds();
    if(ids.length > 0){
      enrollments_ids_url = [];
      for(var i = 0; i < ids.length; i++){
        enrollments_ids_url.push("ids[]=" + ids[i]);
      }
      url_encoded_enrollment = enrollments_ids_url.join("&");
      window.location.href = "/app/enrollment/bases/export?" + url_encoded_enrollment
    }
  },
  componentDidMount: function () {
    $('.chosen').change(this.changeEnrollments);
  },
  handlerAll: function(event){
    if(event.target.checked){
      this.setState({enrollment_selected_ids: this.state.enrollments.map(function(e, index){ return e.id })});
    }else{
      this.setState({enrollment_selected_ids: []});
    }
  },
  componentDidUpdate: function(){
    $('.chosen').trigger("chosen:updated");
  },
  render: function(){
    _this = this;
    button_accept_disabled = false;
    if (this.state.enrollment_selected_ids.length == 0)
      button_accept_disabled = true;


    return(
      <div>
        <div className="modal-header">
          <span>Exportar</span>
        </div>
        <div className="modal-body">
          <div className="form-group">
            <label className='input_required'>Candidatos</label>
            <select ref='enrollment_selected_ids'
                    data-placeholder='Seleccione candidatos'
                    className='form-control chosen'
                    onChange={this.changeEnrollments}
                    multiple={true}
                    value={this.state.enrollment_selected_ids}
            >
              {this.state.enrollments.map(function(enrollment, index){
                return(
                  <option key={'enrollment-selected' + index} value={enrollment.id}>{enrollment.full_name}</option>
                )
              })}
            </select>
          </div>
          <div className='form-group'>
            {function(){
              if (_this.state.enrollments.length > 0){
                return (
                  <div>
                    <input type="checkbox" onClick={_this.handlerAll} ref="checkbox_select_all" id="select-all"/> <label htmlFor="select-all">Seleccionar todos en el proceso</label>
                  </div>
                )
              }else{
                return <p>Cargando candidatos...</p>
              }
            }()}
          </div>
          <div className="form-group text-right">
          <button type="button" className="btn btn-default" data-dismiss="modal" style={{marginRight: '5px'}}>Cerrar</button>
            <button type='button' disabled={button_accept_disabled} onClick={this.accept} className='btn btn-success'>Exportar</button>
          </div>
        </div>
      </div>
    )
  },
});
