var InviteForm = React.createClass({
  getInitialState: function () {
    if (!this.props.enrollments) {
      _this = this;
      Course.getCandidates(this.props.course_id).then(function (enrollments) {
        _this.setState({enrollments: enrollments})
      });
    }
    return {
      response_invite: null,
      enrollment_selected_ids: this.props.enrollment_selected_ids || [],
      enrollments: this.props.enrollments || []
    }
  },
  accept: function (event) {
    _this = this;
    ids = this.getEnrollmentSelectedIds();
    if(ids.length > 0){
      event.target.setAttribute('disabled', true);
      Enrollment.invite(ids).then(function(data){
        event.target.setAttribute('disabled', false);
        _this.setState({response_invite: data, enrollment_selected_ids: []});
        _this.props.onInvite(data)
      });
    }
  },
  getEnrollmentSelectedIds: function(){
    enrollment_selected_ids = [];
    selectedOptions = this.refs.enrollment_selected_ids.selectedOptions;
    for (var i = 0; i < selectedOptions.length; i++) {
      enrollment_selected_ids.push(selectedOptions[i].value);
    }
    return enrollment_selected_ids;
  },
  renderResponseInvite: function () {
    response_invite = this.state.response_invite;
    if(response_invite){
      errors = [];
      success = 0;
      for (var i = 0; i < response_invite.length; i++) {
        enrollment = response_invite[i]
        if (enrollment.errors.course_invitation)
          errors.push(enrollment.full_name + ' no pudo ser invitado: ' + enrollment.errors.course_invitation);
        else
          success++;
      }
      style = success == response_invite.length ? 'alert-success' : ( errors.length == response_invite.length ? 'alert-danger' : 'alert-warning' );
      return (
        <div className={'alert ' + style} role="alert">
          {function () {
             if(success == response_invite.length){
               return 'Candidatos invitados exitosamente'
             }else if (success > 0 && errors.length > 0){
               return(
                 <div>
                   <span>Los candidatos invitados exitosamente, excepto los siguientes:</span>
                   <ul>
                     {function () {
                        return (
                          errors.map(function (error) {
                            return <li>{error}</li>
                          })
                        )
                      }()}
                   </ul>
                 </div>
               )
             }else{
               return(
                 <ul>
                   {function () {
                      return (
                        errors.map(function (error, index) {
                          return <li key={'enrollment-error-' + index}>{error}</li>
                        })
                      )
                    }()}
                 </ul>
               )
             }
           }()}
        </div>
      )
    }
  },
  changeEnrollments: function (event) {
    this.setState({enrollment_selected_ids: this.getEnrollmentSelectedIds()})
  },
  componentDidMount: function () {
    $('.chosen').change(this.changeEnrollments);
  },
  componentDidUpdate: function(){
    $('.chosen').trigger("chosen:updated");
  },
  render: function(){
    button_accept_disabled = false
    if(this.state.enrollment_selected_ids.length == 0){
      button_accept_disabled = true;
    }
    return (
      <div>
        <div className='modal-header'>
          <span>Invitar</span>
        </div>
        <div className='modal-body'>
          {this.renderResponseInvite()}
          <div className='form-group'>
            <label className='input_required'>Candidatos</label>
            <select ref='enrollment_selected_ids' data-placeholder="Seleccione candidatos"
                    className='form-control chosen'
                    onChange={this.changeEnrollments}
                    multiple={true}
                    value={this.state.enrollment_selected_ids}
            >
              {this.state.enrollments.map(function(enrollment, index) {
                 return (
                   <option key={'enrollment-selected' + index} value={enrollment.id}>{enrollment.full_name}</option>
                 )
               })}
            </select>
          </div>
          <div className='form-group text-right'>
            <button type="button" className="btn btn-default" data-dismiss="modal" style={{marginRight: '5px'}}>Cerrar</button>
            <button type='button' disabled={button_accept_disabled} className='btn btn-success' onClick={this.accept}>Aceptar</button>
          </div>
        </div>
      </div>
    )
  }
})
