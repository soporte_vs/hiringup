var EnrollmentTools = {
  drawApproveStageForm: function(){
    _this = this;
    enrollments_status_stage = this.state.enrollments_status_stage;
    if (enrollments_status_stage.length > 0){
      return(
        <table className="table">
          <thead>
            <tr>
              <th>Candidatos</th>
              <th>Etapa actual</th>
              <th>Estado etapa actual</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {enrollments_status_stage.map(function(enrollment_status_stage, index){
              var status_stage_change = {
                is_approved:enrollment_status_stage.is_approved,
                requestChange: _this.changeStatusStage.bind(null, index)
              }

              enrollment = enrollment_status_stage.enrollment;
              return (
                <tr key={"enrollment-list-" + index}>
                  <td>{enrollment.first_name}</td>
                  <td>{enrollment.current_stage}</td>
                  <td>
                    {function(){
                      if(enrollment.stages.length > 0 && enrollment.current_stage_approved == null){
                        return (
                          <select
                          className="form-control"
                          valueLink={status_stage_change}>
                            <option value="true">Aprobada</option>
                            <option value="">Pendiente</option>
                          </select>)
                      }else if(enrollment.stages.length > 0){
                        return <p>{enrollment.current_stage_approved ? "Aprobada" : "Rechazada"}</p>
                      }else{
                        return <p>Sin Etapa</p>
                      }
                    }()}
                  </td>
                  <td className="col-xs-1">
                    <button
                       onClick={_this.deleteEnrollment}
                       className="btn btn-danger"
                       data-enrollment-id={enrollment.id}>
                       <span className="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </button>
                  </td>
                </tr>
              )
            })}
          </tbody>
        </table>
      )
    }
  },
  changeStatusStage: function(i, value){
    this.state.enrollments_status_stage[i].is_approved = value
    this.setState({enrollments_status_stage: this.state.enrollments_status_stage})
  },
  updateSelectedEnrollments: function(){
    _this = this;
    enrollments = _this.state.enrollments.filter(function(enrollment){
      return _this.state.enrollment_selected_ids.indexOf(String(enrollment.id)) >= 0
    });

    _this.state.enrollments_status_stage = enrollments.map(function(enrollment){
      // si el valor del current_stage_approved es nulo, es porque la etapa no se encuentra revisada y por defecto se debe dejar en true
      // a menos que el usuario lo cambie mediante el select
      is_approved = enrollment.current_stage_approved ==  null ? "true" : enrollment.current_stage_approved;
      return {enrollment: enrollment, is_approved: is_approved}
    });

    _this.setState({
      enrollments_status_stage: _this.state.enrollments_status_stage
    });
  },
  getEnrollmentStatusStage: function(){
    _this = this;
    return _this.state.enrollments_status_stage.map(function(enrollment_status_stage){
      return {enrollment_id: enrollment_status_stage.enrollment.id, is_approved: enrollment_status_stage.is_approved}
    })
  },
  deleteEnrollment: function(event){
    enrollment_id = event.currentTarget.dataset.enrollmentId;
    enrollment_selected = this.state.enrollment_selected_ids;

    index = enrollment_selected.indexOf(enrollment_id);
    if (index >= 0){
      // Eliminamos el enrollment del arreglo
      enrollment_selected.splice(index, 1)
    }
    this.setState(
      {enrollment_selected_ids: enrollment_selected},
      this.changeEnrollments
    )
  }
}
