//= require components/enrollment/not_hit
//= require components/common/error_hit

var SearchkitProvider = Searchkit.SearchkitProvider;
var SearchBox = Searchkit.SearchBox;
var ResetFilters = Searchkit.ResetFilters;
var RefinementListFilter = Searchkit.RefinementListFilter;
var MenuFilter = Searchkit.MenuFilter;
var Hits = Searchkit.Hits;
var NoHits = Searchkit.NoHits;
var Pagination = Searchkit.Pagination;
var SortingSelector = Searchkit.SortingSelector;

var EnrollmentSearch  = React.createClass({
  childContextTypes: {
    searchkit: React.PropTypes.object,
    course_id: React.PropTypes.number,
    type_enrollment: React.PropTypes.string,
    is_admin: React.PropTypes.bool,
    onboarding: React.PropTypes.object,
  },
  getChildContext: function() {
    return {
      searchkit: this.state.searchkit,
      course_id: this.state.course_id,
      type_enrollment: this.state.type_enrollment,
      is_admin: this.state.is_admin,
      onboarding: this.props.onboarding
    };
  },
  getInitialState: function () {
    var searchkit = HupeSearchkitRoutes.getCourseEnrollmentsSearchRoute(this.props.course_id);
    return {
      searchkit: searchkit,
      course_id: this.props.course_id,
      type_enrollment: this.props.type_enrollment,
      is_admin: this.props.is_admin
    };
  },
  render: function() {
    return (
      <SearchkitProvider searchkit={this.state.searchkit}>
        <div >

          <div className='row'>
            <div className='col-xs-12'>
              <div className='col-xs-12'>
                <div className='row panel __with_shadow padding-top-10-px padding-bottom-10-px' >
                  <div className='col-xs-8 col-sm-6'>
                    <SearchBox
                      autofocus={true}
                      searchOnChange={true}
                      queryOptions={{minimum_should_match: '100%'}}
                      translations={{'searchbox.placeholder': 'Buscar candidato'}}
                      queryFields={["full_name", "last_name", "first_name", "email", "identification_document_number"]}
                    />
                  </div>

                  <div className='col-xs-4 col-sm-6 text-right'>
                    <SortingSelector options={[
                      {label:"Actualizados", field:"updated_at", order:"desc"},
                      {label:"Nombre A-Z", field:"full_name", order:"asc", },
                      {label:"Nombre Z-A", field:"full_name", order:"desc"}
                    ]} className='pull-right'/>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className='row'>
            <div className='col-md-3 clearfix'>
              <div className='col-xs-12 __white-background __with_shadow padding-top-10-px padding-bottom-10-px'>

                <MenuFilter
                  id="current_stages"
                  title="Etapas del proceso"
                  field="current_stage.raw"
                  translations={{"All":"Todas"}}
                />

                <hr/>
                <MenuFilter
                  id="discard"
                  title="Descartado"
                  field="discarded"
                  translations={{"false":"No", "true":"Sí", "All":"Todos"}}
                />

                <hr/>
                <MenuFilter
                  id="is_employee"
                  title="Colaboradores"
                  field="is_employee"
                  translations={{"true":"Sí", "false":"No", "All":"Todos"}}
                />

                <hr/>
                <MenuFilter
                  id="references"
                  title="Posee referencias"
                  field="has_references"
                  translations={{"All":"Todas", "true":"Sí", "false":"No"}}

                />
              </div>
            </div>

            <div className='col-md-9 clearfix'>
              <Hits hitsPerPage={10}
                highlightFields={["title"]}
                sourceFilter={["title"]}
                listComponent={EnrollmentHits}/>

              <NoHits
                  component={NoHitsEnrollmentDisplay}
                  errorComponent={NoHitsErrorDisplay}/>

              <Pagination translations={
                    {
                    "pagination.previous":"Anterior",
                    "pagination.next":"Siguiente",
                    }
                  }
                showNumbers={true}  className='__white-background __with_shadow' style={{'padding': '10px'}}/>
            </div>
          </div>
        </div>
      </SearchkitProvider>
    );
  }
});
