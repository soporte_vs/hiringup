var EnrollmentHits = React.createClass({
  contextTypes: {
    searchkit: React.PropTypes.object,
  },
  getInitialState: function(){
    return { enrollment_ids_checked: [] }
  },
  onClickChild: function (event) {
    enrollment_id = event.target.value;
    if(event.target.checked){
      this.state.enrollment_ids_checked.push(enrollment_id);
    }else{
      index = this.state.enrollment_ids_checked.indexOf(enrollment_id);
      this.state.enrollment_ids_checked.splice(index, 1);
    }
    this.setState({enrollment_ids_checked: this.state.enrollment_ids_checked});
  },
  onClick: function (event) {
    this.state.enrollment_ids_checked = event.target.checked ? this.props.hits.map(function (hit) {
      return hit._source.id.toString();
    }) : [];
    this.setState({enrollment_ids_checked: this.state.enrollment_ids_checked});
  },
  onChecked: function () {
    a2 = this.props.hits;
    a1 = this.state.enrollment_ids_checked;
    return a1.length == a2.length && a1.every(function(v,i) { return v == a2[i]._source.id})
  },
  callBackActions: function (data) {
    searchkit = this.context.searchkit
    delete searchkit.query;
    setTimeout(function(){
      searchkit.performSearch();
     }, 1000);
  },
  render: function(){
    _this = this;
    checked = _this.onChecked();
    enrollments = this.props.hits.map(function (hit) {
      return hit._source;
    });
    return (
      <div>
        <div className='panel __with_shadow' style={{padding: '10px 0'}}>
          <div className='row'>
            <div className='col-xs-1 text-center'>
              <input type='checkbox' checked={checked} onClick={this.onClick}/>
            </div>
            <div className='col-xs-11'>
              <div className='col-xs-10'>
                <EnrollmentActions
                  enrollments={enrollments}
                  enrollment_ids_checked={this.state.enrollment_ids_checked}
                  callBackActions={this.callBackActions}/>
              </div>
            </div>
          </div>
        </div>
        <div className='panel __with_shadow clearfix '>
          {this.props.hits.map(function (enrollment_hit, index) {
            checked = _this.state.enrollment_ids_checked.includes(enrollment_hit._source.id.toString());
            return <EnrollmentHit
                      key={'EnrollmentHit-' + index}
                      enrollment_hit={enrollment_hit}
                      checked={checked}
                      onClick={_this.onClickChild}
                      callBackActions={_this.callBackActions}/>
          })}
        </div>
      </div>
    )
  }
});
