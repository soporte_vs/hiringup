var EnrollmentActions = React.createClass({
  contextTypes: {
    course_id: React.PropTypes.number,
    type_enrollment: React.PropTypes.string,
    is_admin: React.PropTypes.bool,
    onboarding: React.PropTypes.object
  },
  dispose: function() {
    enrollments = this.props.enrollments;
    modal = document.querySelector('#modal .modal-content');

    dispose_form = React.createElement(DisposeForm, {
      course_id: this.context.course_id,
      enrollment_selected_ids: this.props.enrollment_ids_checked,
      onDispose: this.props.callBackActions
    });

    ReactDOM.render(dispose_form, modal);
    $('#modal').modal().on('shown.bs.modal', function () {
      $('.chosen').chosen();
    });

    $('#modal').on('hide.bs.modal', function () {
      modal_content = this.querySelector('#modal .modal-content');
      if(modal_content && modal_content.children[0])
        modal_content.removeChild(modal_content.children[0]);
    });
  },
  revert_dispose:function(){
    enrollments = this.props.enrollments;
    modal = document.querySelector('#modal .modal-content');

    revert_dispose_form = React.createElement(RevertDisposeForm, {
      enrollment_selected_ids: this.props.enrollment_ids_checked,
      course_id: this.context.course_id,
      onRevertDispose: this.props.callBackActions
    });

    ReactDOM.render(revert_dispose_form, modal);
    $('#modal').modal().on('shown.bs.modal', function () {
      $('.chosen').chosen();
    });

    $('#modal').on('hide.bs.modal', function () {
      modal_content = this.querySelector('#modal .modal-content');
      if(modal_content && modal_content.children[0])
        modal_content.removeChild(modal_content.children[0]);
    });
  },
  next_stage: function () {
    enrollments = this.props.enrollments;
    modal = document.querySelector('#modal .modal-content');
    next_stage_form_type = (this.context.type_enrollment == 'Enrollment::Dynamic' ? NextStageDynamicForm : NextStageForm);
    next_stage_form = React.createElement(next_stage_form_type, {
      course_id: this.context.course_id,
      enrollment_selected_ids: this.props.enrollment_ids_checked,
      onNextStage: this.props.callBackActions,
      onboarding: this.context.onboarding
    });

    ReactDOM.render(next_stage_form, modal);
    $('#modal').modal().on('shown.bs.modal', function () {
      $('.chosen').chosen();
    });

    $('#modal').on('hide.bs.modal', function () {
      modal_content = this.querySelector('#modal .modal-content');
      if(modal_content && modal_content.children[0])
        modal_content.removeChild(modal_content.children[0]);
    });
  },
  invite: function () {
    enrollments = this.props.enrollments;
    modal = document.querySelector('#modal .modal-content');

    invite_form = React.createElement(InviteForm, {
      course_id: this.context.course_id,
      enrollment_selected_ids: this.props.enrollment_ids_checked,
      onInvite: this.props.callBackActions
    });

    ReactDOM.render(invite_form, modal);
    $('#modal').modal().on('shown.bs.modal', function () {
      $('.chosen').chosen();
    });

    $('#modal').on('hide.bs.modal', function () {
      modal_content = this.querySelector('#modal .modal-content');
      if(modal_content && modal_content.children[0])
        modal_content.removeChild(modal_content.children[0]);
    });
  },

  unEnroll: function () {
    enrollments = this.props.enrollments;
    modal = document.querySelector('#modal .modal-content');

    unenroll_form = React.createElement(UnEnrollForm, {
      course_id: this.context.course_id,
      enrollment_selected_ids: this.props.enrollment_ids_checked,
      onUnEnroll: this.props.callBackActions
    });

    ReactDOM.render(unenroll_form, modal);
    $('#modal').modal().on('shown.bs.modal', function () {
      $('.chosen').chosen();
    });

    $('#modal').on('hide.bs.modal', function () {
      modal_content = this.querySelector('#modal .modal-content');
      if(modal_content && modal_content.children[0])
        modal_content.removeChild(modal_content.children[0]);
    });
  },

  update:function(){
    modal = document.querySelector('#modal .modal-content');

    applicants = this.props.enrollments.map(function (enrollment) {
      return applicant = {id: enrollment.applicant_id, full_name: enrollment.full_name };
    });
    ai = this.props.enrollment_ids_checked;
    applicant_ids_checked = this.props.enrollments.map(function (enrollment) {
      if (String(ai).includes(String(enrollment.id)))
        return enrollment.applicant_id
    });

    update_form = React.createElement(UpdateApplicantDataRequestForm, {
      applicant_selected_ids: applicant_ids_checked,
      applicants: applicants,
      onUpdate: this.props.callBackActions
    });

    ReactDOM.render(update_form, modal);
    $('#modal').modal().on('shown.bs.modal', function () {
      $('.chosen').chosen();
      $('.chosen-course').chosen({
        allow_single_deselect: true
      });
    });

     $('#modal').on('hide.bs.modal', function () {
       modal_content = this.querySelector('#modal .modal-content');
       if(modal_content && modal_content.children[0])
         modal_content.removeChild(modal_content.children[0]);
     });
  },
  send_mail: function () {
    modal = document.querySelector('#modal .modal-content');

    applicants = this.props.enrollments.map(function (enrollment) {
      return applicant = {id: enrollment.applicant_id, full_name: enrollment.full_name };
    });
    ai = this.props.enrollment_ids_checked;
    applicant_ids_checked = this.props.enrollments.map(function (enrollment) {
      if (String(ai).includes(String(enrollment.id)))
        return enrollment.applicant_id
    });

    send_email_form = React.createElement(SendMassiveApplicantForm, {
      applicant_selected_ids: applicant_ids_checked,
      applicants: applicants,
      onSendEmail: this.props.callBackActions,
    });

    ReactDOM.render(send_email_form, modal);
    $('#modal').modal().on('shown.bs.modal', function () {
      $('.chosen').chosen();
      $('.chosen-course').chosen({
        allow_single_deselect: true
      });
    });

     $('#modal').on('hide.bs.modal', function () {
       modal_content = this.querySelector('#modal .modal-content');
       if(modal_content && modal_content.children[0])
         modal_content.removeChild(modal_content.children[0]);
     });
  },
  add_appointment: function () {
    enrollments = this.props.enrollments;
    modal = document.querySelector('#modal .modal-content');
    event_form = React.createElement(StagesEventForm, {
      course_id: this.context.course_id,
      enrollment_selected_ids: this.props.enrollment_ids_checked,
      onAddAppointment: this.props.callBackActions
    });

    ReactDOM.render(event_form, modal);
    $('#modal').modal().on('shown.bs.modal', function () {
      $('.chosen').chosen();
      google.maps.event.trigger(map, "resize");
    });

    $('#modal').on('hide.bs.modal', function () {
      modal_content = this.querySelector('#modal .modal-content');
      if(modal_content && modal_content.children[0])
        modal_content.removeChild(modal_content.children[0]);
    });
  },
  export: function () {
    enrollments = this.props.enrollments;
    modal = document.querySelector('#modal .modal-content');

    export_form = React.createElement(EnrollmentExportForm, {
      course_id: this.context.course_id,
      enrollment_selected_ids: this.props.enrollment_ids_checked,
      onExport: this.props.callBackActions
    });

    ReactDOM.render(export_form, modal);

    $('#modal').modal().on('shown.bs.modal', function(){
      $('.chosen').chosen();
    })
  },
  export_cv: function () {
    modal_content = document.querySelector('#modal .modal-content');

    applicants = this.props.enrollments.map(function (enrollment) {
      return applicant = {id: enrollment.applicant_id, full_name: enrollment.full_name };
    });
    ai = this.props.enrollment_ids_checked;
    applicant_ids_checked = this.props.enrollments.map(function (enrollment) {
      if (String(ai).includes(String(enrollment.id)))
        return enrollment.applicant_id
    });

    export_cv_form = React.createElement(ExportCvApplicantForm, {
      applicant_selected_ids: applicant_ids_checked,
      applicants: applicants
    });

    ReactDOM.render(export_cv_form, modal_content);
    $('#modal').modal().on('shown.bs.modal', function () {
      $('.chosen').chosen();
      $('.chosen-course').chosen({
        allow_single_deselect: true
      });
    });

     $('#modal').on('hide.bs.modal', function () {
       modal_content = document.querySelector('#modal .modal-content');
       if(modal_content && modal_content.children[0])
         modal_content.removeChild(modal_content.children[0]);
     });
  },
  drawRevertDisposeButton: function () {
    if (_this.context.is_admin){
      return (
        <li>
          <a className='mouse-pointer' onClick={_this.revert_dispose}>
            <span className='glyphicon glyphicon-repeat' style={{marginRight: '5px'}}></span>
            Revertir descarte
          </a>
        </li>
      )
    }
  },
  drawUnEnrollButton: function () {
    if (_this.context.is_admin){
      return (
        <li>
          <a className='mouse-pointer' onClick={_this.unEnroll}>
            <span className='glyphicon glyphicon-trash' style={{marginRight: '5px'}}></span>
            Remover candidato
          </a>
        </li>
      )
    }
  },
  render: function () {
    _this = this;
    return(
      <div className='btn-group'>
        <button type='button' className='btn btn-default' onClick={_this.next_stage}>
          <span className='glyphicon glyphicon-share' style={{marginRight: '5px'}}></span>
          Cambiar de etapa
        </button>
        <button type='button' className='btn btn-default' onClick={_this.add_appointment}>
          <span className='glyphicon glyphicon-calendar' style={{marginRight: '5px'}}></span>
          Agendar evento
        </button>
        <button type='button' className='btn btn-default' onClick={_this.dispose}>
          <span className='glyphicon glyphicon-thumbs-down' style={{marginRight: '5px'}}></span>
          Descartar
        </button>
        <div className='btn-group'>
          <button type="button" className="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Más acciones <span className="caret"></span>
          </button>
          <ul className="dropdown-menu">
            <li>
              <a className='mouse-pointer' onClick={_this.send_mail}>
                <span className='glyphicon glyphicon-send' style={{marginRight: '5px'}}></span>
                Enviar correo
              </a>
            </li>
            <li>
              <a className='mouse-pointer' onClick={_this.export}>
                <span className='glyphicon glyphicon-download-alt' style={{marginRight: '5px'}}></span>
                Exportar Excel
              </a>
            </li>
            <li>
              <a className='mouse-pointer' onClick={_this.export_cv}>
                <span className='glyphicon glyphicon-download-alt' style={{marginRight: '5px'}}></span>
                Exportar CV
              </a>
            </li>
            <li>
              <a className='mouse-pointer' onClick={_this.invite}>
                <span className='glyphicon glyphicon-thumbs-up' style={{marginRight: '5px'}}></span>
                Invitar
              </a>
            </li>
            <li>
              <a className='mouse-pointer' onClick={_this.update}>
                <span className='glyphicon glyphicon-envelope' style={{marginRight: '5px'}}></span>
                Solicitar actualización de datos
              </a>
            </li>
            { _this.drawRevertDisposeButton() }
            { _this.drawUnEnrollButton() }
          </ul>
        </div>

      </div>
    );
  }
});


