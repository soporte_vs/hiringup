var EnrollmentHit = React.createClass({
  mixins: [ApplicantTools],
  drawDiscardReason: function () {
    discard_reason = this.props.enrollment_hit._source.discarding.reason;
    if (discard_reason){
      return (
        <span className="inline-item label label-danger">{'Descartado por: ' + discard_reason}</span>
      );
    }
  },
  drawPostulationAnswers: function () {
    var answers = this.props.enrollment_hit._source.answers;
    var enrollmentAnswersId = "enrollmentAnswersId-" + this.props.enrollment_hit._source.id;
    if(answers.length > 0){
      return(
        <div className="col-xs-12 __with_collapsible-caret">
          <a className="btn-link collapsed answers-collapse-link" role="button" data-toggle="collapse" href={"#"+enrollmentAnswersId} aria-expanded="false" aria-controls={enrollmentAnswersId}>
            <small className="__with_caret">Respuestas de la postulación&nbsp;&nbsp;</small>
          </a>
          <div className="margin-top-10-px collapse" id={enrollmentAnswersId}>
            <ol>
              {answers.map(function (answer, index) {
                return (
                  <li key={index}>
                    <strong className='__force-text_break-word'>{answer.question}</strong>
                    <p className='__force-text_break-word'>{answer.answer}</p>
                  </li>
                )
               })
              }
            </ol>
          </div>
        </div>
      )
    }
  },
  drawInvitationStatus: function () {
    invited = this.props.enrollment_hit._source.course_invitation.exists;
    invitation_status = "";
    invitation_label = "info";
    if (invited) {
      invitation = this.props.enrollment_hit._source.course_invitation;
      switch(invitation.accepted) {
        case true:
          invitation_label = 'success';
          invitation_status = "Invitación aceptada";
          break;
        case false:
          invitation_label = 'danger';
          invitation_status = "Invitación rechazada";
          break;
        default:
          invitation_status = "Esperando respuesta de la invitación";
          break;
      }
      return (
        <span className={"inline-item label label-" + invitation_label}>{invitation_status}</span>
      )
    }
  },
  drawStages: function () {
    _this = this;
    if (this.props.enrollment_hit._source.stages.length > 0)
      return (
        <div className='row'>
          <div className='col-xs-12'>
            <strong>Etapas del candidato</strong>
            <ol>
              {this.props.enrollment_hit._source.stages.map(function (stage) {
                return (
                  <li key={'Enrollment-' + _this.props.enrollment_hit._source.id + 'stage-' + stage.id}>
                    {_this.drawStatus(stage.is_approved)}
                    &nbsp;
                    <a href={stage.url} data-toggle="tooltip" data-placement="right" target='_blank' title={'Fecha de creación: ' + moment(stage.created_at).format('LLL')}>{stage.name}</a>&nbsp;
                  </li>
                )
              }
              )}
            </ol>
          </div>
        </div>
      )
  },
  drawCalendarEvent: function () {
    event = this.props.enrollment_hit._source.calendar_event;
    event_resourceable = event ? event.event_resourceable : {};
    if(event['starts_at'])
      return (
        <div>
          <div className='row'>
            <div className='col-xs-12'>
              <EventBox
                  course_id={_this.props.enrollment_hit._source.course_id}
                  event={event}
                  {...event_resourceable}
                  callBackActions={this.props.callBackActions}/>
            </div>
          </div>
          <br/>
        </div>
      );
  },
  render: function(){
    _this = this;
    tag_id = "collapseEnrollment-" + this.props.enrollment_hit._source.id;
    email = this.props.enrollment_hit._source.email;
    identification_document_number = this.props.enrollment_hit._source.identification_document_number || "No ingresó";
    identification_document_type = this.props.enrollment_hit._source.identification_document_type || "No ingresó";
    identification_document_country = this.props.enrollment_hit._source.identification_document_country || "No ingresó";
    cellphone = this.props.enrollment_hit._source.cellphone;
    if(cellphone == '' || cellphone == undefined) cellphone = 'No ingresó';
    blacklisted = this.props.enrollment_hit._source.blacklisted;
    info_request_count = this.props.enrollment_hit._source.info_request_count;

    return (
      <div className='enrollment_item clearfix padding-bottom-10-px' style={blacklisted ? {backgroundColor: '#F9C8C8'} : {}}>
        <div className='row padding-bottom-10-px padding-top-10-px '>
          <div className='col-xs-1 text-center padding-top-20-px'>
            <input type='checkbox' readOnly checked={this.props.checked} onClick={this.props.onClick} value={this.props.enrollment_hit._source.id}/>
          </div>
          <div className='col-xs-11 __with_collapsible-icons'>
            <div className='col-xs-12'>
              <a data-toggle="collapse" className='collapsed enrollment-item-link'
                 href={"#"+tag_id}
                 aria-expanded="false"
                 aria-controls={tag_id}>

                <div className="media">
                  <div className="media-left">
                    <img className="media-object img-rounded img-thumbnail" data-src="holder.js/64x64" alt="64x64" src={this.props.enrollment_hit._source.avatar_url} data-holder-rendered="true" style={{width: '64px', height: '64px'}}/>
                  </div>
                  <div className="media-body">
                    <h3 className="media-heading">
                      {this.drawCandidateType(this.props.enrollment_hit._source)}
                      &nbsp;
                      {this.drawReferenced(this.props.enrollment_hit._source)}
                      &nbsp;
                      {this.props.enrollment_hit._source.full_name}
                    </h3>
                    {this.drawStatusLabeled(this.props.enrollment_hit._source.current_stage_approved)}
                    &nbsp;
                    <span className="inline-item label label-primary">{this.props.enrollment_hit._source.current_stage}</span>
                    &nbsp;
                    {this.drawUpdateInfoRequestCount(info_request_count)}
                    &nbsp;
                    {this.drawInvitationStatus()}
                    &nbsp;
                    {this.drawBlacklisted(blacklisted)}
                    &nbsp;
                    {this.drawDiscardReason()}
                  </div>
                </div>
              </a>
            </div>
          </div>
        </div>

        <div className="row collapse" id={tag_id}>
          <div className='col-xs-offset-1 col-xs-11'>
            <div className='row'>
              <div className='row'>
                <div className='col-xs-6'>
                  <strong>Documento de identificación: </strong>
                  <span>{identification_document_number}</span><br></br>

                  <strong>Tipo de documento de identificación: </strong>
                  <span>{identification_document_type}</span><br></br>

                  <strong>País del documento de identificación: </strong>
                  <span>{identification_document_country}</span><br></br>
                </div>

                <div className='col-xs-6'>
                  <strong>Email: </strong>
                  <span>{email}</span><br></br>

                  <strong>Teléfono celular: </strong>
                  <span>{cellphone}</span>
                </div>
              </div>
              <div className='row'>
                <div className='col-xs-12'>
                  <a href={'/app/applicant/bases/' + this.props.enrollment_hit._source.applicant_id} target='_blank'>Ver perfil completo</a>
                </div>
              </div>
            </div>
            <br/>

            <div className='row'>
              {this.drawCalendarEvent()}
            </div>

            <div className='row'>
              {this.drawPostulationAnswers()}
            </div>

            <br/>
            <div className='row'>
              {this.drawStages()}
           </div>
          </div>
        </div>
      { /* End Collapse */ }
      </div>
    );
  }
});
