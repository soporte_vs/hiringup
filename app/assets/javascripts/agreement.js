(function () {

    $(document).ready(function(){
      $('.money-mask').mask("#.##0", {reverse: true});
    });

    $(document).submit(function() {
    $(".money-mask").unmask();
    });

})();
