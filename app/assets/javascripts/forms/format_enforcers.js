if ( $('form').length > 0 ){
  // Enforce Digits only , K allowed.
  $('input.enforce_rut').keydown(function(e) {
    // Allow: -, k, backspace, delete, tab, escape, enter, . and -
    if($.inArray(e.keyCode, [
      189, // dash
      173, // minus (firefox) / mute/unmute
      75, // k
      46, // delete
      8, // backspace
      9, // tab
      27, // escape
      13, // enter
      110, // decimal point
      190, // period
      109 // substract
    ]) !== -1 ||
      // Allow: Ctrl+A
      (e.keyCode == 65 && e.ctrlKey === true) ||
      // Allow: Ctrl+C
      (e.keyCode == 67 && e.ctrlKey === true) ||
      // Allow: Ctrl+X
      (e.keyCode == 88 && e.ctrlKey === true) ||
      // Allow: home, end, left, right
      (e.keyCode >= 35 && e.keyCode <= 39)) {
      // let it happen, don't do anything
        return;
    }
    // Ensure that it is a number and stop the keypress
    if((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
      e.preventDefault();
    }
  }).attr('placeholder','12345678-k');

  // Enforce letters only
  $('input.enforce_text').keydown(function(e) {
    // Allow: -, k, backspace, delete, tab, escape, enter and .
    if($.inArray(e.keyCode, [
      189, // dash
      173, // minus (firefox) / mute/unmute
      75, // k
      46, // delete
      8, // backspace
      9, // tab
      27, // escape
      13, // enter
      110, // decimal point
      190, // period
      109 // substract
    ]) !== -1 ||
       // Allow: Ctrl+A
       (e.keyCode == 65 && e.ctrlKey === true) ||
       // Allow: Ctrl+C
       (e.keyCode == 67 && e.ctrlKey === true) ||
       // Allow: Ctrl+X
       (e.keyCode == 88 && e.ctrlKey === true) ||
       // Allow: home, end, left, right
       (e.keyCode >= 35 && e.keyCode <= 39)) {
      // let it happen, don't do anything
      return;
    }
    // Ensure that it is NOT a number and stop the keypress
    if( (e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) ) {
      e.preventDefault();
    } else {
      return;
    }
  })

  // Enforce Numbers only
  $('input.enforce_num').keydown(function(e) {
    // Allow: -, k, backspace, delete, tab, escape, enter and .
    if($.inArray(e.keyCode,[
      189, // dash
      173, // minus (firefox) / mute/unmute
      46, // delete
      8, // backspace
      9, // tab
      27, // escape
      13, // enter
      110, // decimal point
      190, // period
      109 // substract
    ]) !== -1 ||
       // Allow: Ctrl+A
       (e.keyCode == 65 && e.ctrlKey === true) ||
       // Allow: Ctrl+C
       (e.keyCode == 67 && e.ctrlKey === true) ||
       // Allow: Ctrl+X
       (e.keyCode == 88 && e.ctrlKey === true) ||
       // Allow: home, end, left, right
       (e.keyCode >= 35 && e.keyCode <= 39)) {
      // let it happen, don't do anything
      return;
    }
    // Ensure that it is NOT a number and stop the keypress
    if( (e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) ) {
      return;
    } else {
      e.preventDefault();
    }
  })

  // Enforce Numbers only
  $('input.enforce_phone').keydown(function(e) {
    // Allow: space, -, backspace, delete, tab, escape, enter, + and .
    if($.inArray(e.keyCode, [
      32, // spacebar
      189, // dash
      173, // minus (firefox) / mute/unmute
      46, // delete
      8, // backspace
      9, // tab
      27, // escape
      13, // enter
      110, // decimal point
      190, // period
      109, // substract
      107, // add
    ]) !== -1 ||
      // Allow: Ctrl+A
      (e.keyCode == 65 && e.ctrlKey === true) ||
      // Allow: Ctrl+C
      (e.keyCode == 67 && e.ctrlKey === true) ||
      // Allow: Ctrl+X
      (e.keyCode == 88 && e.ctrlKey === true) ||
      // Allow: home, end, left, right
      (e.keyCode >= 35 && e.keyCode <= 39)) {
      // let it happen, don't do anything
        return;
    } else if ( (e.keyCode == 56 && e.shiftKey === true) || (e.keyCode == 57 && e.shiftKey === true) ) {
      e.preventDefault();
    }

    // Ensure that it is NOT a number and stop the keypress
    if( (e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) ) {
      return;
    } else {
      e.preventDefault();
    }
  })
}

