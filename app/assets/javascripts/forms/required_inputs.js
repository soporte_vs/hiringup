// Espera a que esté todo cargado... incluso los assets
_.everythingLoaded(function(){

  // Assosiates required inputs with his label via classes.
  form = $('form');
  formReqInputs = form.find('[required]');

  // Si existe un form y ese form tiene campos requireds...
  if ( form.length > 0 && formReqInputs.length > 0) {

    function reqInputs() {
      // Update vars.
      form = $('form');
      formReqInputs = form.find('[required]');
      if (formReqInputs.length > 0) {
        // Iteramos por cada input...
        for ( i = 0; i < formReqInputs.length; i++ ) {
          element = formReqInputs[i];
          id = $(element).attr('id');
          // Agregamos la clase 'input_required' al label asociado a la ID del input.
          $('label[for="'+ id +'"]').addClass('input_required');
        }
        reqInputs.called = true;
      };
    };

    reqInputs();

    // onDOMChange
    var target = document.querySelector('form');
      _.DOMChanged(target,function(){
        console.log('DOM Changed!')
        reqInputs();
    },{  attributes: false, childList: true, characterData: false, subtree: true })
  };


}); // DOMLoaded






