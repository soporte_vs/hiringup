$(document).ready(function(){
  form = $('form');

  if ( form.length > 0 ) {
    form.each(function(){
      // Inicializamos el validador

      $(this).validate({
        errorElement: 'em',
        ignore: '.ignore',
        errorPlacement: function(error, element) {
          attrName = element.attr("name")
          if ( $('input[name="'+attrName+'"]').length > 1 ) {
            error.insertAfter($('input[name="'+attrName+'"]').last().parent());
          } else {
            error.insertAfter(element);
          }
        }
      });

      // Agrega la regla required (validación) a los campos con required(html)
      if ( $(this).find('[required]').length > 0 ) {
        $(this).find('[required]').rules('add', {
          required: true
        });
      };

      // Agrega la regla email (validación) a los campos con input[type=email]
      if ( $(this).find('input[type=email]').length > 0 ) {
        $(this).find('input[type=email]').rules('add', {
          email: true
        });
      };

      // Agrega la validacion de rut a los campos con la clase enforce_rut
      if ( $(this).find('input.enforce_rut').length > 0 ) {
        $(this).find('input.enforce_rut').rules('add', {
          rutCL: true
        });
      };

      if ( $(this).find('select.chosen').length > 0 ) {
        $('.chosen').chosen({
          allow_single_deselect: true
        });
      };
    });
  };
});
