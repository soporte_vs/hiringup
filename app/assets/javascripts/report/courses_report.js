$(document).ready(function(){
  // Some Variables
  checkBox = $('input[name="select_all_positions"]');
  selectBox = $('select[name="position_ids[]"]');

  options = $(selectBox).find('option');
  optionsArray = $(options).map(function(){
                  return $(this).val()
                })
  // Initializes select chosen with the right width
  // otherwise it wont calculate the width properly
  $('select[name="position_ids[]"]').chosen({width: "99%"});


  // Adds the event listener and some functions
  $(checkBox).click(function(){
    if ( $(checkBox).prop('checked') == false ) {
      $('.company_positions_select').removeClass('hidden');
      $(selectBox).val([]).trigger("chosen:updated");
    } else {
      $('.company_positions_select').addClass('hidden');
      $(selectBox).val(optionsArray).trigger("chosen:updated");
    }
  });

});
