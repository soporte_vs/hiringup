(function () {
  var current,
      buttons = document.getElementsByClassName('js-see_answers');

  for (var i = 0; i < buttons.length; i++) {
    current = buttons[i];

    var mostrarRespuestas = function (e) {
      self = e.currentTarget;
      self.innerText = 'Esconder Respuestas';

      var parent = self.parentElement.parentElement.parentElement;
      var answers = parent.getElementsByClassName('js-answers')[0];

      answers.className = 'js-answers';

      self.removeEventListener('click', mostrarRespuestas, false);
      self.addEventListener('click', esconderRespuestas, false);
    }

    var esconderRespuestas = function (e) {
      self = e.currentTarget;
      self.innerText = 'Ver Respuestas';

      var parent = self.parentElement.parentElement.parentElement;
      var answers = parent.getElementsByClassName('js-answers')[0];

      answers.className = 'js-answers hide';

      self.removeEventListener('click', esconderRespuestas, false);
      self.addEventListener('click', mostrarRespuestas, false);
    }

    current.addEventListener('click', mostrarRespuestas, false);
  }
})();