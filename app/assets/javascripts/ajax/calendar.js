var Calendar = {

  createEvent: function(enrollment_ids, starts_at, ends_at, address, observations, serialized_guests, serialized_manager) {
    return new Promise(function (resolve, reject) {
      _reject = reject
      $.ajax({
        url: '/app/calendar/events.json',
        method: 'post',
        data: {
          calendar_event: {
            starts_at: starts_at,
            ends_at: ends_at,
            address: address.name,
            observations: observations,
            lat: address.lat,
            lng: address.lng,
            guests_attributes: serialized_guests,
            manager_attributes: serialized_manager,
          },
          enrollment_ids: enrollment_ids
        },
        success: function(data, textStatus, jqXHR) {
          resolve(data);
          if(typeof data.error === 'undefined') {
            // Success so call function to process the form
          } else {
            // Handle errors here
            console.log('ERRORS: ' + data.error);
          }
        }, error: function(jqXHR, textStatus, errorThrown) {
          // Handle errors here
          console.log('ERRORS: ' + textStatus);
          propsErr = { position: 'bottom-right', className: 'error'};
        }
      })
    })
  },
  createEventForApplicants: function(applicant_ids, starts_at, ends_at, address, observations, serialized_guests, serialized_manager) {
    return new Promise(function (resolve, reject) {
      _reject = reject
      $.ajax({
        url: '/app/calendar/events/create_for_applicants.json',
        method: 'post',
        data: {
          calendar_event: {
            starts_at: starts_at,
            ends_at: ends_at,
            address: address.name,
            observations: observations,
            lat: address.lat,
            lng: address.lng,
            guests_attributes: serialized_guests,
            manager_attributes: serialized_manager,
          },
          applicant_ids: applicant_ids
        },
        success: function(data, textStatus, jqXHR) {
          resolve(data);
          if(typeof data.error === 'undefined') {
            // Success so call function to process the form
          } else {
            // Handle errors here
            console.log('ERRORS: ' + data.error);
          }
        }, error: function(jqXHR, textStatus, errorThrown) {
          // Handle errors here
          console.log('ERRORS: ' + textStatus);
          propsErr = { position: 'bottom-right', className: 'error'};
        }
      })
    })
  },
  updateEvent: function(id, enrollment_ids, starts_at, ends_at, address, observations, serialized_guests, serialized_manager) {
    return new Promise(function (resolve, reject) {
      _reject = reject
      $.ajax({
        url: '/app/calendar/events/'+id+'.json',
        method: 'patch',
        data: {
          calendar_event: {
            starts_at: starts_at,
            ends_at: ends_at,
            address:address.name,
            observations: observations,
            lat: address.lat,
            lng: address.lng,
            guests_attributes: serialized_guests,
            manager_attributes: serialized_manager,
          },
          enrollment_ids: enrollment_ids
        },
        success: function(data, textStatus, jqXHR) {
          resolve(data);
          if(typeof data.error === 'undefined') {
            // Success so call function to process the form
          } else {
            // Handle errors here
            console.log('ERRORS: ' + data.error);
          }
        }, error: function(jqXHR, textStatus, errorThrown) {
          // Handle errors here
          console.log('ERRORS: ' + textStatus);
          propsErr = { position: 'bottom-right', className: 'error'};
        }
      })
    })
  },
  updateEventForApplicants: function(id, applicant_ids, starts_at, ends_at, address, observations, serialized_guests, serialized_manager) {
    return new Promise(function (resolve, reject) {
      _reject = reject
      $.ajax({
        url: '/app/calendar/events/'+id+'/update_for_applicants.json',
        method: 'put',
        data: {
          calendar_event: {
            starts_at: starts_at,
            ends_at: ends_at,
            address:address.name,
            observations: observations,
            lat: address.lat,
            lng: address.lng,
            guests_attributes: serialized_guests,
            manager_attributes: serialized_manager,
          },
          applicant_ids: applicant_ids
        },
        success: function(data, textStatus, jqXHR) {
          resolve(data);
          if(typeof data.error === 'undefined') {
            // Success so call function to process the form
          } else {
            // Handle errors here
            console.log('ERRORS: ' + data.error);
          }
        }, error: function(jqXHR, textStatus, errorThrown) {
          // Handle errors here
          console.log('ERRORS: ' + textStatus);
          propsErr = { position: 'bottom-right', className: 'error'};
        }
      })
    })
  },
  markAttended: function(id, resourceable_id, resourceable_type, attended) {
    // si no viene attended, es true por defecto
    if (attended === undefined) {
      attended = true;
    } else {
      // si es cualquier otra cosa que no sea true, es false
      if (attended !== true) {
        attended = false;
      }
    }
    return new Promise(function (resolve, reject) {
      $.ajax({
        url: '/app/calendar/events/'+id+'/mark_attended.json',
        method: 'put',
        data: {
          resourceable_id: resourceable_id,
          resourceable_type: resourceable_type,
          attended: attended
        },
        success: function(data, textStatus, jqXHR) {
          resolve(data);
          if(typeof data.error === 'undefined') {
            // Success so call function to process the form
          } else {
            // Handle errors here
            console.log('ERRORS: ' + data.error);
          }
        }, error: function(jqXHR, textStatus, errorThrown) {
          // Handle errors here
          console.log('ERRORS: ' + textStatus);
          propsErr = { position: 'bottom-right', className: 'error'};
        }
      })
    })
  },
  markWillAttend: function(id, resourceable_id, resourceable_type, will_attend) {
    // si no viene will_attend, es null por defecto
    if (will_attend === undefined) {
      will_attend = null;
    } else {
      // si es cualquier otra cosa que no sea true, es false
      if (will_attend !== true) {
        will_attend = false;
      }
    }
    return new Promise(function (resolve, reject) {
      $.ajax({
        url: '/app/calendar/events/'+id+'/mark_will_attend.json',
        method: 'put',
        data: {
          resourceable_id: resourceable_id,
          resourceable_type: resourceable_type,
          will_attend: will_attend
        },
        success: function(data, textStatus, jqXHR) {
          resolve(data);
          if(typeof data.error === 'undefined') {
            // Success so call function to process the form
          } else {
            // Handle errors here
            console.log('ERRORS: ' + data.error);
          }
        }, error: function(jqXHR, textStatus, errorThrown) {
          // Handle errors here
          console.log('ERRORS: ' + textStatus);
          propsErr = { position: 'bottom-right', className: 'error'};
        }
      })
    })
  }
};
