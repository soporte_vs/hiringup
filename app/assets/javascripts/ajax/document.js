var Document = {
  sendFile: function(upload_path, applicant_documents, auth_token) {
    return new Promise(function (resolve, reject) {
      _reject = reject
      $.ajax({
        url: upload_path,
        type: 'POST',
        data: applicant_documents,
        cache: false,
        dataType: 'json',
        processData: false, // Don't process the files
        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
        success: function(data, textStatus, jqXHR) {
          resolve(data);
          if(typeof data.error === 'undefined') {
            // Success so call function to process the form
          } else {
            // Handle errors here
            console.log('ERRORS: ' + data.error);
          }
        }, error: function(jqXHR, textStatus, errorThrown) {
          // Handle errors here
          console.log('ERRORS: ' + textStatus);
          propsErr = { position: 'botom-right', className: 'error'};
        }
      })
    })
  },
  deleteFile: function(record_id) {
    return new Promise(function (resolve, reject) {
    _reject = reject
    $.ajax({
      url: '/app/document/records/'+record_id+'.json',
      method: 'DELETE',
      data: {id: record_id},
        success: function(data) {
          resolve(data);
        }, error: function(err) {
          propsErr = { position: 'bottom-right', className: 'error'};
          if(err.status == 401)
            $.notify('No tiene permisos para tal acción', propsErr);
          else if(err.status == 500){
            $.notify('Ha ocurrido un problema', propsErr)
          }else{
            $.notify('Verifique que está conectado a internet', propsErr)
          }
          if(typeof(reject) == 'function'){
            reject(err)
          }
        }
      });
    });
  },
  documentRequest: function(request_path, document_descriptors_name, message){
    return new Promise(function (resolve, reject) {
      _reject = reject
      $.ajax({
        url: request_path,
        type: 'POST',
        data: {document_descriptors: document_descriptors_name, message: message},
        success: function(data, textStatus, jqXHR) {
          resolve(data);
          if(typeof data.error === 'undefined') {
            // Success so call function to process the form
          } else {
            // Handle errors here
            console.log('ERRORS: ' + data.error);
          }
        }, error: function(jqXHR, textStatus, errorThrown) {
          // Handle errors here
          console.log('ERRORS: ' + textStatus);
          propsErr = { position: 'botom-right', className: 'error'};
        }
      })
    })
  }
};
