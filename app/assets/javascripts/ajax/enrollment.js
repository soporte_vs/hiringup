var Enrollment = {
  nextStage: function(enrollment_ids, args, name_stage, type_stage){

    return new Promise(function (resolve, reject) {
      reqwest({
        url: '/app/enrollment/bases/next_stage.json',
        method: 'post',
        data: { ids: enrollment_ids, name_stage: name_stage, type_stage: type_stage, args: args},
        dataType: "json",
        success: function (enrollments) {
          resolve(enrollments)
        },
        error: function (err) {
          propsErr = { position: 'bottom-right', className: 'error'};
          if(err.status == 401)
            document.location.href="/";
          else if(err.status == 500){
            $.notify('Ha ocurrido un problema', propsErr)
          }else{
            $.notify('Verifique que está conectado a internet', propsErr)
          }
          if(typeof(reject) == 'function'){
            reject(err)
          }
        }
      });
    });
  },
  dispose: function(enrollment_ids, discard_reason_id, observations, send_discarding_email){
    return new Promise(function (resolve, reject) {
      reqwest({
        url: '/app/enrollment/bases/dispose.json',
        method: 'post',
        data: { ids: enrollment_ids, discard_reason_id: discard_reason_id, observations: observations, send_discarding_email: send_discarding_email},
        success: function (enrollments) {
          resolve(enrollments)
        },
        error: function (err) {
          propsErr = { position: 'bottom-right', className: 'error'};
          if(err.status == 401)
            document.location.href="/";
          else if(err.status == 500){
            $.notify('Ha ocurrido un problema', propsErr)
          }else{
            $.notify('Verifique que está conectado a internet', propsErr)
          }
          if(typeof(reject) == 'function'){
            reject(err)
          }
        }
      });
    });
  },
  revert_dispose: function(enrollment_ids){
    return new Promise(function (resolve, reject) {
      reqwest({
        url: '/app/enrollment/bases/revert_dispose.json',
        method: 'post',
        data: { ids: enrollment_ids },
        success: function (enrollments) {
          resolve(enrollments)
        },
        error: function (err) {
          propsErr = { position: 'bottom-right', className: 'error'};
          if(err.status == 401)
            document.location.href="/";
          else if(err.status == 500){
            $.notify('Ha ocurrido un problema', propsErr)
          }else if(err.status == 403){
            $.notify('No posee permisos suficientes para realizar esta acción', propsErr)
          }else{
            $.notify('Verifique que está conectado a internet', propsErr)
          }
          if(typeof(reject) == 'function'){
            reject(err)
          }
        }
      });
    });
  },

  unEnroll: function(enrollment_ids){
    return new Promise(function (resolve, reject) {
      reqwest({
        url: '/app/enrollment/bases/unenroll.json',
        method: 'post',
        data: { ids: enrollment_ids },
        success: function (enrollments) {
          resolve(enrollments)
        },
        error: function (err) {
          propsErr = { position: 'bottom-right', className: 'error'};
          if(err.status == 401)
            document.location.href="/";
          else if(err.status == 500){
            $.notify('Ha ocurrido un problema', propsErr)
          }else if(err.status == 403){
            $.notify('No posee permisos suficientes para realizar esta acción', propsErr)
          }else{
            $.notify('Verifique que está conectado a internet', propsErr)
          }
          if(typeof(reject) == 'function'){
            reject(err)
          }
        }
      });
    });
  },

  invite: function(enrollment_ids){
    return new Promise(function (resolve, reject) {
      reqwest({
        url: '/app/enrollment/bases/invite.json',
        method: 'post',
        data: { ids: enrollment_ids },
        success: function (enrollments) {
          resolve(enrollments)
        },
        error: function (err) {
          propsErr = { position: 'bottom-right', className: 'error'};
          if(err.status == 401)
            document.location.href="/";
          else if(err.status == 500){
            $.notify('Ha ocurrido un problema', propsErr)
          }else{
            $.notify('Verifique que está conectado a internet', propsErr)
          }
          if(typeof(reject) == 'function'){
            reject(err)
          }
        }
      });
    });
  },
  getDiscardReasons: function(){
    return new Promise(function (resolve, reject) {
      reqwest({
        url: '/app/enrollment/discard_reasons.json',
        method: 'get',
        success: function (discard_reasons) {
          resolve(discard_reasons)
        },
        error: function (err) {
          propsErr = { position: 'bottom-right', className: 'error'};
          if(err.status == 401)
            document.location.href="/";
          else if(err.status == 500){
            $.notify('Ha ocurrido un problema', propsErr)
          }else{
            $.notify('Verifique que está conectado a internet', propsErr)
          }
          if(typeof(reject) == 'function'){
            reject(err)
          }
        }
      });
    });
  }
};
