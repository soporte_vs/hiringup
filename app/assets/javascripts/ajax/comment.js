var Comment = {
  getByObject: function(object_type, object_id, page){
    return new Promise(function (resolve, reject) {
      reqwest({
        url: "/app/comment.json?",
        data: {object_type: object_type, object_id: object_id, page: page},
        method: "get",
        success: function (data) {
          resolve(data);
        },
        error: function (err) {
          propsErr = { position: "bottom-right", className: "error"};
          if(err.status == 401) {
            document.location.href = "/";
          } else if(err.status == 500){
            $.notify("Ha ocurrido un problema", propsErr);
          } else {
            $.notify("Verifique que está conectado a internet", propsErr);
          }

          if(typeof(reject) == "function"){
            reject(err);
          }
        }
      });
    });
  },

  addComment: function(object_type, object_id, content){
    return new Promise(function (resolve, reject) {
      reqwest({
        url: "/app/comment",
        method: "post",
        data: {object_type: object_type, object_id: object_id, content: content},
        success: function(data){
          resolve(data);
        },
        error: function(err){
          propsErr = { position: "bottom-right", className: "error"};
          if(err.status == 401) {
            $.notify("Necesita estar logueado para comentar");
          } else if(err.status == 500){
            $.notify("Ha ocurrido un problema", propsErr)
          } else if(err.status == 422){
            errors = JSON.parse(err.response)
            if (errors) {
              $.each(errors.errors, function(key, value) {
                $.notify(value, propsErr)
              });
            }
          } else {
            $.notify("Verifique que está conectado a internet", propsErr)
          }
          if(typeof(reject) == "function"){
            reject(err);
          }
        }
      });
    });
  }
};
