var CoursePostulation = {
  getPostulations: function(course_id){
    return new Promise(function (resolve, reject) {
      reqwest({
        url: '/app/courses/'+ course_id+'/course_postulations.json',
        method: 'get',
        success: function (discard_reasons) {
          resolve(discard_reasons)
        },
        error: function (err) {
          propsErr = { position: 'botom-right', className: 'error'};
          if(err.status == 401)
            document.location.href="/";
          else if(err.status == 500){
            $.notify('Ha ocurrido un problema', propsErr)
          }else{
            $.notify('Verifique que esta conectado a internet', propsErr)
          }
          if(typeof(reject) == 'function'){
            reject(err)
          }
        }
      });
    });
  },

  accept: function(course_id, course_postulation_ids){
    return new Promise(function (resolve, reject) {
      reqwest({
        url: '/app/courses/' + course_id + '/course_postulations/accept.json',
        method: 'post',
        data: { course_postulation_ids: course_postulation_ids},
        success: function (enrollments) {
          resolve(enrollments)
        },
        error: function (err) {
          propsErr = { position: 'bottom-right', className: 'error'};
          if(err.status == 401)
            document.location.href="/";
          else if(err.status == 500){
            $.notify('Ha ocurrido un problema', propsErr)
          }else{
            $.notify('Verifique que está conectado a internet', propsErr)
          }
          if(typeof(reject) == 'function'){
            reject(err)
          }
        }
      });
    });
  },

  reject: function(course_id, course_postulation_ids){
    return new Promise(function (resolve, reject) {
      reqwest({
        url: '/app/courses/' + course_id + '/course_postulations/reject.json',
        method: 'post',
        data: { course_postulation_ids: course_postulation_ids},
        success: function (enrollments) {
          resolve(enrollments)
        },
        error: function (err) {
          propsErr = { position: 'bottom-right', className: 'error'};
          if(err.status == 401)
            document.location.href="/";
          else if(err.status == 500){
            $.notify('Ha ocurrido un problema', propsErr)
          }else{
            $.notify('Verifique que está conectado a internet', propsErr)
          }
          if(typeof(reject) == 'function'){
            reject(err)
          }
        }
      });
    });
  },

  reset: function(course_id, course_postulation_ids){
    return new Promise(function (resolve, reject) {
      reqwest({
        url: '/app/courses/' + course_id + '/course_postulations/reset.json',
        method: 'post',
        data: { course_postulation_ids: course_postulation_ids},
        success: function (enrollments) {
          resolve(enrollments)
        },
        error: function (err) {
          propsErr = { position: 'bottom-right', className: 'error'};
          if(err.status == 401)
            document.location.href="/";
          else if(err.status == 500){
            $.notify('Ha ocurrido un problema', propsErr)
          }else{
            $.notify('Verifique que está conectado a internet', propsErr)
          }
          if(typeof(reject) == 'function'){
            reject(err)
          }
        }
      });
    });
  }
};
