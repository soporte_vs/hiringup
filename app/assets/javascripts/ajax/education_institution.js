var EducationInstitution = {
  getBy: function(params){
    var type = params['type'] || '';
    var country_id = params['country_id'] || '';
    return new Promise(function (resolve, reject) {
      $.ajax({
        url: '/app/education/institutions.json?custom=false&type=' + type + '&country_id=' + country_id,
        method: 'GET',
        success: function(data) {
          resolve(data);
        }, error: function(err) {
          propsErr = { position: 'bottom-right', className: 'error'};
          if(err.status == 401)
            $.notify('No tiene permisos para tal acción', propsErr);
          else if(err.status == 500){
            $.notify('Ha ocurrido un problema', propsErr);
          }else{
            $.notify('Verifique que está conectado a internet', propsErr);
          }
          if(typeof(reject) == 'function'){
            reject(err);
          }
        }
      });
    });
  }

};
