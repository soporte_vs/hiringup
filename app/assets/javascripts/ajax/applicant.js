var Applicant = {
  getAll: function(){
    return new Promise(function (resolve, reject) {
      reqwest({
        url: '/app/applicant/bases.json',
        method: 'get',
        success: function (data) {
          resolve(data)
        },
        error: function (err) {
          propsErr = { position: 'bottom-right', className: 'error'};
          if(err.status == 401)
            document.location.href="/";
          else if(err.status == 500){
            $.notify('Ha ocurrido un problema', propsErr)
          }else{
            $.notify('Verifique que está conectado a internet', propsErr)
          }
          if(typeof(reject) == 'function'){
            reject(err)
          }
        }
      });
    });
  },
  getByIds: function(ids){
    return new Promise(function (resolve, reject) {
      params_ids = ''
      for (var i = 0; i < ids.length; i++) {
        params_ids += 'ids[]=' + ids[i] + '&';
      }
      reqwest({
        url: '/app/applicant/bases.json?'+params_ids,
        method: 'get',
        success: function (data) {
          resolve(data)
        },
        error: function (err) {
          propsErr = { position: 'bottom-right', className: 'error'};
          if(err.status == 401)
            document.location.href="/";
          else if(err.status == 500){
            $.notify('Ha ocurrido un problema', propsErr)
          }else{
            $.notify('Verifique que está conectado a internet', propsErr)
          }
          if(typeof(reject) == 'function'){
            reject(err)
          }
        }
      });
    });
  },

  enrollApplicants: function(applicants_ids, course_id) {
    return new Promise(function (resolve, reject) {
    _reject = reject
    $.ajax({
      url: '/app/applicant/bases/enroll.json',
      method: 'POST',
      data: {applicants_ids: applicants_ids, course_id: course_id},
        success: function(data) {
          resolve(data);
        }, error: function(err) {
          propsErr = { position: 'bottom-right', className: 'error'};
          if(err.status == 401)
            $.notify('No tiene permisos para tal acción', propsErr);
          else if(err.status == 500){
            $.notify('Ha ocurrido un problema', propsErr)
          }else{
            $.notify('Verifique que está conectado a internet', propsErr)
          }
          if(typeof(reject) == 'function'){
            reject(err)
          }
        }
      });
    });
  },

  sendUpdatedApplicants: function(applicants_ids) {
    return new Promise(function (resolve, reject) {
    _reject = reject
    $.ajax({
      url: '/app/applicant/bases/send_update_request.json',
      method: 'POST',
      data: {applicants_ids: applicants_ids},
        success: function(data) {
          resolve(data);
        }, error: function(err) {
          propsErr = { position: 'bottom-right', className: 'error'};
          if(err.status == 401)
            $.notify('No tiene permisos para tal acción', propsErr);
          else if(err.status == 500){
            $.notify('Ha ocurrido un problema', propsErr)
          }else{
            $.notify('Verifique que está conectado a internet', propsErr)
          }
          if(typeof(reject) == 'function'){
            reject(err)
          }
        }
      });
    });
  },

  sendMailApplicants: function(applicants_ids, subject, message) {
    return new Promise(function (resolve, reject) {
      _reject = reject
      $.ajax({
        url: '/app/applicant/bases/massive_mailer.json',
        method: 'POST',
        data: {applicants_ids: applicants_ids, subject: subject, message: message},
        success: function(data) {
          resolve(data);
          console.log(data);
        }, error: function(err) {
          propsErr = { position: 'bottom-right', className: 'error'};
          if(err.status == 401)
            $.notify('No tiene permisos para tal acción', propsErr);
          else if(err.status == 500){
            $.notify('Ha ocurrido un problema', propsErr)
          }else{
            $.notify('Verifique que está conectado a internet', propsErr)
          }
          if(typeof(reject) == 'function'){
            reject(err)
          }
        }
      });
    });
  }

};
