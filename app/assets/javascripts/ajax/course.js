var Course = {
  getCandidates: function(course_id){
    return new Promise(function (resolve, reject) {
      reqwest({
        url: '/app/courses/'+ course_id+'/candidates.json',
        method: 'get',
        success: function (discard_reasons) {
          resolve(discard_reasons)
        },
        error: function (err) {
          propsErr = { position: 'botom-right', className: 'error'};
          if(err.status == 401)
            document.location.href="/";
          else if(err.status == 500){
            $.notify('Ha ocurrido un problema', propsErr)
          }else{
            $.notify('Verifique que esta conectado a internet', propsErr)
          }
          if(typeof(reject) == 'function'){
            reject(err)
          }
        }
      });
    });
  },

  getAll: function(){
    return new Promise(function (resolve, reject) {
      reqwest({
        url: '/app/courses.json',
        method: 'get',
        success: function (data) {
          resolve(data)
        },
        error: function (err) {
          propsErr = { position: 'bottom-right', className: 'error'};
          if(err.status == 401)
            document.location.href="/";
          else if(err.status == 500){
            $.notify('Ha ocurrido un problema', propsErr)
          }else{
            $.notify('Verifique que está conectado a internet', propsErr)
          }
          if(typeof(reject) == 'function'){
            reject(err)
          }
        }
      });
    });
  }

};
