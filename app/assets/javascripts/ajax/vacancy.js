var Vacancy = {
  assignCourse: function(vacancy_ids, course_id, auth_token){
    return new Promise(function (resolve, reject) {
      reqwest({
        url: '/app/vacancies/assign_course.json',
        method: 'put',
        data: { ids: vacancy_ids, course_id: course_id, authenticity_token: auth_token },
        success: function (vacancies) {
          resolve(vacancies)
        },
        error: function (err) {
          propsErr = { position: 'botom-right', className: 'error'};
          if(err.status == 401)
            document.location.href="/";
          else if(err.status == 500){
            $.notify('Ha ocurrido un problema', propsErr)
          }else{
            $.notify('Verifique que esta conectado a internet', propsErr)
          }
          if(typeof(reject) == 'function'){
            reject(err)
          }
        }
      });
    });
  }
};
