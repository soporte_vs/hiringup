var PeopleIdentificationDocumentType = {
  getBy: function(params){
    var country_id = params['country_id'] || '';
    return new Promise(function (resolve, reject) {
      $.ajax({
        url: '/app/people/identification_document_types.json?country_id=' + country_id,
        method: 'GET',
        success: function(data) {
          resolve(data);
        }, error: function(err) {
          propsErr = { position: 'bottom-right', className: 'error'};
          if(err.status == 401)
            $.notify('No tiene permisos para tal acción', propsErr);
          else if(err.status == 500){
            $.notify('Ha ocurrido un problema', propsErr);
          }else{
            $.notify('Verifique que está conectado a internet', propsErr);
          }
          if(typeof(reject) == 'function'){
            reject(err);
          }
        }
      });
    });
  }

};
