/*
  Info: https://github.com/4Talent/hiringUpEnterprise/wiki/Acciones-Masivas
*/
(function () {

  _.everythingLoaded(function () {
    getIds = function (type) {
      var current, data_value, hidden,
          elements = [],
          checks = _.qS('input[type="checkbox"]');

      for (var i = 0; i < checks.length; i++) {
        current = checks[i];
        if (current.checked && current.attributes['data-value']) {
          data_value = current.attributes['data-value'].value;
          hidden = document.createElement('input');
          hidden.type = 'hidden';
          hidden.name = type+'_ids[]';
          hidden.value = data_value;

          elements.push(hidden);
        }
      }
      if (elements.length > 0) {
        return elements;
      } else {
        return false;
      }
    }

    var forms = _.getClasses('massive-form');

    if (forms.length < 0) {
      return false;
    } else {
      var current, type, submit;

      for (var i = 0; i < forms.length; i++) {
        current = forms[i];

        if (typeof current === 'object') {
          type = current.attributes['data-type'].value;

          current.addEventListener('submit', function (e) {
            e.preventDefault();
            var elements = getIds(type);

            if (elements) {
              for (var i = 0; i < elements.length; i++) {
                this.appendChild(elements[i]);
              }
              this.submit();
            } else {
              alert('Debe seleccionar al menos un candidato.');
              return false;
            }
          }, false);
        }
      }
    }
  });
})();
