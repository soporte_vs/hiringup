(function () {

  $('#course_header_tabs a').click(function (e) {
    var x = window.pageXOffset,
        y = window.pageYOffset;
    $(window).one('scroll', function () {
      window.scrollTo(x, y);
    })
    $(this).tab('show');
  });

  // store the currently selected tab in the hash value
  $("#course_header_tabs a").on("shown.bs.tab", function (e) {
      var id = $(e.target).attr("href").substr(1);
      $("#course_header_tabs a").each(function () {
        $(this).removeClass('active');
      });
      $(this).addClass('active');
      window.location.hash = id;
  });

  var hash = window.location.hash;
  $('#course_header_tabs a[href="' + hash + '"]').tab('show');

  // Cargar helpers
  var currentModal, descarte;

  if (_ && typeof _ === 'object') {
    _.everythingLoaded( function () {
      // Descarte masivo
      new Modal('js-massive-discard', 'js-massive-discard-close');
      // Descarte individual
      new Modal('js-single-discard', 'js-single-discard-close');
    });
  } else {
    throw ': Error cargando helpers. Intente denuevo, revise su conexión o ponganse en contacto con soporte.';
  }

  $("a[data-remote]").on("ajax:success", function(e, data, status, xhr) {
    alert("Se ha enviado la solicitud de forma correcta.");
  });


  $('#btn-evaluar').on('click', function() {
    $(this).addClass('disabled');
  });

  $('[data-toggle="popover"]').popover();

})();
