// En este archivo se agruparan la construcción de todas las rutas que usa
// Searchkit para conectarse con el servidor y este a su vez con elasticsearch.
// Como se puede notar todas se construyen usando el patron singleton, el cual
// antes de construir la misma route verifica si ya se construyo para no repetirla
// y de esta manera se use la misma que se este usando en el contexto.

var SearchkitManager = Searchkit.SearchkitManager;

var HupeSearchkitRoutes = {
  coursesSearchRoute: null,
  applicantsSearchRoute: null,
  hiringRequestRecordsSearchRoute: null,
  courseEnrollmentsSearchRoute: null,
  coursePostulationsSearchRoute: null,
  getCoursesSearchRoute: function () {
    route_name = 'coursesSearchRoute';
    route_path = '/app/courses';
    return HupeSearchkitRoutes.applySingletonPatternOfRoute(route_name, route_path);
  },
  getApplicantsSearchRoute: function () {
    route_name = 'applicantsSearchRoute';
    route_path = '/app/applicant/bases';
    return HupeSearchkitRoutes.applySingletonPatternOfRoute(route_name, route_path);
  },
  getHiringRequestRecordsSearchRoute: function () {
    route_name = 'hiringRequestRecordsSearchRoute';
    route_path = '/app/hiring_request/records';
    return HupeSearchkitRoutes.applySingletonPatternOfRoute(route_name, route_path);
  },
  // Esta además de usarla el componente de renderizarción de enrollments
  // también es usado por el componente de postulaciones ya que al aceptar
  // postulaciones se enrolan nuevos applicants al proceso, por lo cual es
  // necesario actualizar los candidatos mostrados, aquí es donde se evidencia
  // la necesidad de usar el patrón singleton, ya que de otra manera se haría
  // la consulta al server por los candidatos actuales del proceso pero no
   // refrescaria el componente debido a que no es el mismo route.
  getCourseEnrollmentsSearchRoute: function (course_id) {
    route_name = 'courseEnrollmentsSearchRoute';
    route_path = '/app/courses/'+ course_id;
    return HupeSearchkitRoutes.applySingletonPatternOfRoute(route_name, route_path)
  },
  getCoursePostulationsSearchRoute: function (course_id) {
    route_name = 'coursePostulationsSearchRoute';
    route_path = '/app/courses/' + course_id + '/course_postulations/';
    return HupeSearchkitRoutes.applySingletonPatternOfRoute(route_name, route_path)
  },
  applySingletonPatternOfRoute: function (route_name, route_path) {
    route = HupeSearchkitRoutes[route_name];
    if (route){
      return route
    }else{
      route = new SearchkitManager(route_path);
      HupeSearchkitRoutes[route_name] = route;
      return HupeSearchkitRoutes[route_name];
    }
  }
}
