//Helper de estilos para vista de candidatos
setTableStyles = function(){

  // Container
  candidateContainer = $('.course_candidates-container').parent();
  // Table Body
  tableBody = $('.course_candidates-container > tbody');

  if ( tableBody.children().length == 0 ) {
    $('.course_candidates-container > thead th').addClass('empty_body');
  }

  // Si el ancho del contenido del body de la tabla es mayor al ancho del contenedor de la tabla..
  if ( tableBody.innerWidth() > candidateContainer.innerWidth() ) {
    $('.course_candidates-container').css({
      'overflow-x':'scroll',
      'width' : '100%'
      });

    candidateContainer.css('overflow','hidden');
    console.log('Se aplica scroll');
  } else {
    theadTrWidth = $('.course_candidates-container > thead tr').innerWidth();
    $('.course_candidates-container').innerWidth(theadTrWidth);
    console.log('Seteando el ancho...');

  }
}
// Llama a la función al cargar la página
$(window).ready(setTableStyles)

$('.course_candidates-container').on('DOMNodeInserted',function(){
  setTableStyles()
});
