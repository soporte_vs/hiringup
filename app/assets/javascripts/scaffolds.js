$(document).ready(function() {
  var windowPath = window.location.pathname
  var elements = $("a.panel-link")

  $(elements).each( function(index, element) {

    var elementPath = element.getAttribute('href')
    if (windowPath == elementPath) {
      panelGroup = element.closest('.panel-group')
      $(panelGroup).find('.collapse').addClass('in')
      $(panelGroup).find('.panel-heading').addClass('active-panel-heading')
      $(element).addClass('active')

      if ($(panelGroup).find('h4 a').hasClass('collapsed')){
        $(panelGroup).find('h4 a').removeClass('collapsed')
      }
    }

  });

})