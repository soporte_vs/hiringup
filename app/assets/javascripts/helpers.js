// /*
//   TODO:
//     - removeClass: Permitir remove más de una clase pasando un arreglo de clases
// */

var _ = {

//   /*******************************
//     Availability
//   ********************************/

//   /*
//     MutationObserver, callback que se activa cuando se produce una mutación del DOM
//     Se le debe pasar un target, que es el elemento al que se le va a hacer escucha.
//     Tambien se le puede pasar options como parametros de objeto para configurar el observer.
//     Soporta callback si existe una de las mutaciones solicitadas.
//   */

  DOMChanged: function (target,callback,options) {
    var targeted = target;
    var observer = new MutationObserver(function(mutations) {
      if ( mutations.length > 0 ) {
        if (callback) {
          callback()
        } else {
          console.log('Mutated!, but no callback')
        };
      };
    });

    // configuration of the observer:
    if (options) {
      var config = options
    } else {
      var config = { attributes: true, childList: true, characterData: true, subtree: true };
    };

    // pass in the target node, as well as the observer options
    observer.observe(targeted, config);
  },


  /*
    Abstracción de DOMContentLoaded, callback se activa cuando el HTML se ha
    cargado (no toma en cuenta ningun otro tipo de asset, solo html).

    params:   callback
    returns:
  */

  DOMLoaded: function (callback) {
    if (document.addEventListener) {
      document.addEventListener('DOMContentLoaded', function () {
        callback();
      }, false);
    } else {
      console.log('no event listener');
    }
  },

  /*
    Abstracción de onload o load, callback se activa cuando todos los assets
    se han cargado.

    params:   callback
    returns:
  */

  everythingLoaded: function (callback) {
    if (window.onload) {
      window.onload = function () {
        callback();
      }
    } else {
      window.addEventListener('load', function () {
        callback();
      }, false);
    }
  },

  /******************************
    HTMLElement Handling
  *******************************/

  /*
    Abstracción de document.getElementById

    params:   id
    returns:  HTMLElement
  */
  getId: function (id) {
    return document.getElementById(id);
  },

  /*
    Remover una clase especifica de un elemento
    params:   HTMLElement, clase a remover
    returns: element or false
  */

  getClasses: function (klass, parent) {
    var state;
    if (document.getElementsByClassName) {
      if (parent) {
        state = parent.getElementsByClassName(klass);
      } else {
        state = document.getElementsByClassName(klass);
      }
    } else {
      console.log('no document.getElementsByClassName');
      state = false;
    }
    return state;
  },

  qS: function (query) {
    if (document.querySelectorAll) {
      return document.querySelectorAll(query);
    } else {
      console.log('no document.querySelectorAll');
      return false;
    }
  },

  removeClass: function (element, clase) {
    // Guardar clases originales del elemento
    var oldClass = element.className;

    /*
      Si existe la clase que se quiere eliminar, se reemplaza por '',
      si no existe retorna false
    */

    if (oldClass.indexOf(clase) > 0) {
      var newClass = oldClass.replace(clase, '');
      element.className = newClass;
      return element;
    } else {
      return false;
    }
  },

   /* Simulador de Rails Flashes */
  createFlash: function (type,msg) {
    var daFlash =
    '<div class="col-md-offset-2 col-md-8 flash-box flash-type-'+ type +'">'+
      '<div class="flash-wrapper">'+
        '<div class="flash_item flash_'+ type +'">'+
          msg +
        '</div>'+
      '</div><div class="close">x</div>'+
    '</div>';

    $('#floating_box').append(daFlash);

    $('.flash-box .close').click(function(){
      $(this).parent().fadeOut();
    });
  },
}
