// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.

//= require promise
//= require jquery
//= require jquery_ujs
//= require bootsy
//= require helpers
//= require moment.min

//  *BOOTSTRAP*
//= require bootstrap-sprockets
//  Por componentes
//= require bootstrap/scrollspy
//= require bootstrap/modal
//= require bootstrap/dropdown
//= require bootstrap/transition
//= require bootstrap/tab
//= require bootstrap/tooltip
//= require bootstrap/popover
//= require bootstrap/alert
//= require bootstrap/button
//= require bootstrap/collapse
//= require bootstrap/carousel
//= require bootstrap/affix

//= require chartkick

// Forms
//= require jquery.validation/jquery.validation.min
//= require jquery.validation/messages_es
//= require jquery.validation/custom.validations
//= require forms/format_enforcers
//= require jquery.mask/jquery.mask
//= require forms/validaemons
//= require forms/required_inputs



//= require jquery.datetimepicker
//= require chosen.jquery.min
//= require notify.min
//= require SocialShare/SocialShare

//= require react
//= require react_ujs
//= require searchkit
//= require components

//= require mousetrap.min/mousetrap.min
//= require keyboard_shortcuts

//= require errors
//= require company_references

//= require cocoon
