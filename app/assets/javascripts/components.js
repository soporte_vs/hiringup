//= require reqwest.min
//= require observer
//= require channel
//= require react-dropzone
//= require datetimepicker-react.min
//= require searchkit_routes

//= require ajax/document
//= require ajax/vacancy
//= require ajax/enrollment
//= require ajax/course
//= require ajax/course_postulation
//= require ajax/applicant
//= require ajax/calendar
//= require ajax/comment
//= require ajax/education_institution
//= require ajax/people_identification_document_type

//= require components/territory/country_state_select
//= require components/territory/state_select
//= require components/territory/country_select
//= require components/territory/city_select
//= require components/territory/country_state_city_select
//= require components/territory/state_city_select

//= require components/common/field_wrapper
//= require components/common/email_field
//= require components/common/setter_course_vacancies
//= require components/common/setter_new_course_vacancies
//= require components/common/spinner
//= require components/common/google_maps_searchbox
//= require components/common/date_tools
//= require components/common/applicant_tools

//= require components/calendar/event_box
//= require components/calendar/event_mark_attended
//= require components/calendar/event_mark_will_attend
//= require components/calendar/event_show
//= require components/calendar/address_map_show
//= require components/calendar/map_show

//= require components/course/search
//= require components/course/search_modal
//= require components/course/postulations/search
//= require components/course/postulations/reset_postulation_form
//= require components/course/postulations/export_form
//= require components/applicant/search
//= require components/applicant/search_modal
//= require components/applicant/applicants_event_form
//= require components/applicant/update_applicant_data_request_form
//= require components/applicant/send_massive_applicant_form
//= require components/applicant/export_cv_applicant_form

//= require components/enrollment/tools
//= require components/enrollment/onboarding_tools
//= require components/enrollment/tool_bar
//= require components/enrollment/invite_form
//= require components/enrollment/dispose_form
//= require components/enrollment/revert_dispose_form
//= require components/enrollment/stages_event_form
//= require components/enrollment/invite_applicant_form
//= require components/enrollment/next_stage_form
//= require components/enrollment/next_stage_dynamic_form
//= require components/enrollment/hit
//= require components/enrollment/hits
//= require components/enrollment/search
//= require components/enrollment/export_form
//= require components/enrollment/unenroll_form

//= require components/laborum/nested_select
//= require components/laborum/parent_select
//= require components/laborum/child_select
//= require components/territory/country_select
//= require components/territory/state_select
//= require components/territory/country_state_select
//= require components/territory/country_state_city_select
//= require components/territory/state_city_select
//= require components/territory/state_only_select


//= require components/people/degree/form
//= require components/people/degree/multiple_form

//= require components/vacancy/form
//= require components/vacancy/multiplied_form
//= require components/vacancy/multiple_form

//= require components/company/position
//= require components/company/position_cenco_select

//= require components/people/laboral_experience/form
//= require components/people/laboral_experience/multiple_form

//= require components/people/identification_document_type/input

//= require components/minisite/question/form
//= require components/minisite/question/multiple_form

//= require components/hiring_request/record/vacancies_table
//= require components/hiring_request/record/search

//= require components/document/document_record
//= require components/document/document_wrapper
//= require components/document/document_wrapper_action
//= require components/document/documents_request_form

//= require components/comment/comment
//= require components/comment/comment_box
//= require components/comment/comment_list

React.sanitizeName = function (name) {
  return name.replace(/\]\[|[^\w]/g, '_').replace().replace(/_$/, "");
}

React.truncateText = function(text, length) {
  if (text.length > length) {
    text = text.substring(0,length) + '...'
  }
  return text
}

moment.locale('es');
$.datetimepicker.setLocale('es');

datetimepickerOptions = {
  // overwrite datepicker scroll default settings
  scrollMonth: false,
  scrollTime: false,
  scrollInput: false
}
$.extend($.fn.datetimepicker.defaults, datetimepickerOptions);

$('body').append(' \
  <div id="modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel"> \
    <div class="modal-dialog modal-lg" role="document">\
      <div class="modal-content"> \
      </div>\
    </div>\
  </div>');

$('#modal').on("hidden.bs.modal", function(){
  $("#modal .modal-content").html("");
});

$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
