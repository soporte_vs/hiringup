(function () {
  Mousetrap.prototype.stopCallback = function () {
     return false;
  };
  //Crear proceso
  Mousetrap.bind(['command+k', 'ctrl+k'], function() {
    window.location.href = "/app/courses/new";
    return false;
  });
  //Crear proceso en nueva pestaña
  Mousetrap.bind(['command+shift+k', 'ctrl+shift+k'], function() {
    window.open(href = "/app/courses/new", "_blank");
    return false;
  });
  //Crear candidato
  Mousetrap.bind(['command+u', 'ctrl+u'], function() {
    window.location.href = "/app/applicant/catcheds/new";
    return false;
  });
  //Crear proceso en nueva pestaña
  Mousetrap.bind(['command+shift+u', 'ctrl+shift+u'], function() {
    window.open(href = "/app/applicant/catcheds/new", "_blank");
    return false;
  });

  Mousetrap.bind(['command+shift+b', 'ctrl+shift+b'], function() {
    modal = document.querySelector('#modal .modal-content');
    ReactDOM.render(React.createElement(ApplicantSearchModal, null), modal);
    $('#modal').modal().on('shown.bs.modal', function (e) {
      e.target.getElementsByClassName('sk-search-box__text')[0].focus()
    });
    return false;
  });

  Mousetrap.bind(['command+shift+v', 'ctrl+shift+v'], function() {
    modal = document.querySelector('#modal .modal-content');
    ReactDOM.render(React.createElement(CourseSearchModal, null), modal);
    $('#modal').modal().on('shown.bs.modal', function (e) {
      e.target.getElementsByClassName('sk-search-box__text')[0].focus()
    });
    return false;
  });
})();
