(function () {
  $(document).ready(function(){
    var _yearEnd = moment().year() - 18;
    var _startDate = moment().subtract(18, 'years');
    $('.js-datepicker').datetimepicker({
      format: 'Y-m-d',
      lazyInit:     true,
      timepicker:   false,
      startDate:    _startDate.format("YYYY-MM-DD"),
      yearEnd:      _yearEnd,
      yearStart:    1900
    });

    // Alert before leaving for if any field changed
    $('.edit_user input, .edit_applicant input').not('.postulant_upload-avatar', '.submit_profile').change(function(){
      mssg ='Hay modificaciones no guardadas, se perderá la información si no la guardas. ';
      window.onbeforeunload = confirmExit1;
      function confirmExit1() {
        return mssg
      }

      $('input[name="commit"], button.submit').click(function(){
        window.onbeforeunload = null;
      });
    });

    // Alert before leaving for if avatar changed
    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
          $('.postulant_avatar').attr('style','background-image: url('+e.target.result+')').addClass('preview');
          msg = 'Recuerda hacer clic en "Guardar Información" para guardar tu imagen de perfil';
          _.createFlash('alert', msg);
        }

        reader.readAsDataURL(input.files[0]);
      }
    }

    $('.postulant_upload-avatar').change(function(){
      readURL(this);
    });
    //end Image Preview

    //end Alert before leaving for if avatar changed

    // Alert before leaving for if Documents added
    $('#edit_applicant_documents input').change(function(){
      console.log('Doc about to upload');
      window.onbeforeunload = confirmExit;
      btnValue = $('#edit_applicant_documents input[type="submit"]').val();
      function confirmExit() {
        return 'Has seleccionado archivos para subir pero el proceso no se ha completado, para confirmar la subida de dichos archivos debes hacer click en "' + btnValue + '".';
      };
    });
    // End Alert before leaving for if Documents added

    $(document).ready(function(){
      $('.rut-mask').mask("###0-A", {reverse: true});
    });

    //end Image Preview
    $('input.validate_rut').keydown(function(e) {
      // Allow: -, k, backspace, delete, tab, escape, enter and .
      if($.inArray(e.keyCode, [189, 75, 46, 8, 9, 27, 13, 110, 190]) !== -1 ||
         // Allow: Ctrl+A
         (e.keyCode == 65 && e.ctrlKey === true) ||
         // Allow: Ctrl+C
         (e.keyCode == 67 && e.ctrlKey === true) ||
         // Allow: Ctrl+X
         (e.keyCode == 88 && e.ctrlKey === true) ||
         // Allow: home, end, left, right
         (e.keyCode >= 35 && e.keyCode <= 39)) {
        // let it happen, don't do anything
        return;
      }
      // Ensure that it is a number and stop the keypress
      if((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
      }
    });

  })
})();
