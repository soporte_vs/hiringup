(function () {

  _.everythingLoaded( function () {
    var check = _.getId('js-check_everything');
    var checkBoxes = document.getElementsByClassName('js-checkbox');

    if (check && checkBoxes) {
      check.addEventListener('click', function () {
        var state = this.checked;

        for (var i = 0; i < checkBoxes.length; i++) {
          checkBoxes[i].checked = state;
        };

      }, false);
    } else {
      if (console && console.error) {
        // console.error('No se encontraron checkboxes');
      }
    }
  });
})();
