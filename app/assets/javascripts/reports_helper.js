(function () {
  $('.js-datepicker').datetimepicker({
    lazyInit:     true,
    timepicker:   false,
    format:'d/m/Y'
  });
  $('.js-select').chosen({
    no_results_text: "No se encontró ningún centro de costo",
    allow_single_deselect: true
  });
})();
