(function () {
  $('#profile_header_tabs a').click(function (e) {
    var x = window.pageXOffset,
        y = window.pageYOffset;
    $(window).one('scroll', function () {
      window.scrollTo(x, y);
    })
    $(this).tab('show');
  });

  // store the currently selected tab in the hash value
  $("#profile_header_tabs a").on("shown.bs.tab", function (e) {
      var id = $(e.target).attr("href").substr(1);
      $("#profile_header_tabs a").each(function () {
        $(this).removeClass('active');
      });
      $(this).addClass('active');
      window.location.hash = id;
  });

  var hash = window.location.hash;
  $('#profile_header_tabs a[href="' + hash + '"]').tab('show');

  //Chosen para agregar candidatos a un course
  $('.choosen-courses').chosen({
    width: '100%',
    no_results_text: "No se encontró ningún proceso",
    allow_single_deselect: true
  }).change(function () {
    answer = confirm('¿Estás seguro que deseas agregar al postulante a ese proceso?')
    if(answer) {
      f = this.selectedOptions[0];
      url = $(f).attr('data-link');
      form = $('form#enroll-form');
      form.attr('action', url);
      form.submit();
    }else{
      $('[name=enroll-select]').val('').trigger("chosen:updated");
    }
  });
})();
