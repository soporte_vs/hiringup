+function () {
	/*
		Private
	*/

	// Agregar clases para animar un formulario y esconder el otro
	var animateForms = function (showForm, duration) {

		// Si no se pasa duration, por defecto se asigna 1000
		if (!duration) {
			var duration = 1000;
		}

		/*
			Esconder formulario quitandole la clase __fade-in y agregando __fade-out
			y __hide (despues de 1000ms para evitar 'spontaneous combustion')
		*/
		_.removeClass(current, '__fade-in');
		_.removeClass(current, '__fade-out');

		current.className += ' __fade-out';

		window.setTimeout( function () {
			_.removeClass(current, '__hide');
			current.className += ' __hide';
			current = showForm;
		}, duration);

		/*
			Mostrar formulario removiendo clase '__hide' y '__fade-out' y
			agregando __fade-in
		*/

		_.removeClass(showForm, ' __hide');
		_.removeClass(showForm, ' __fade-out');
		showForm.className += ' __fade-in';
	}



	/*
		Actions
	*/

	// Setear variables en root scope para reutilizar elementos
	var login_container, login_form, register_form, logo, current,
	loginTrigger, registerTrigger, changeTrigger;

	/*
		Cuando el DOM (HTML) carge, se setean los elementos a sus respectivas
		variables, así solo se llaman una vez
	*/

	_.DOMLoaded( function() {
		login_form = _.getId('new_user');
		register_form = _.getId('new_applicant_external');
		changePass_form = _.getId('new_pass');

		login_container = _.getId('login_container');

		logo = _.getId('logo');

		loginTrigger = _.getId('logear');
		registerTrigger = _.getId('registrar');
		changeTrigger = _.getId('cambiar');
		passRegistrarTrigger = _.getId('password_registrar');
		passIngresarTrigger = _.getId('password_ingresar');

		// Setear current a login form
		current = login_form;

	});

	_.everythingLoaded( function () {
		/*
			Se muestra el contenedor al estar todo cargado para evitar flash de
			imagen no cargada y se agrega clase para animacion
		*/
		login_container.className += ' __show __fade-in';

		// Se agrega clase de animación a logo
		logo.className += ' __fade-in';

		// Se agrega clase de animación a formulario login
		login_form.className += ' __fade-in';

		window.setTimeout( function () {
			_.removeClass(login_form, ' __fade-in');
		}, 1000);

		/*
			Se agregan triggers a botones para activar animaciones de formularios,
			esconder uno y mostrar el otro.
		*/

		if (loginTrigger) {
			loginTrigger.addEventListener('click', function () {
				animateForms(login_form);
			}, false);
		}

		if (registerTrigger) {
			registerTrigger.addEventListener('click', function () {
				animateForms(register_form);
			}, false);
		}

		if (changeTrigger) {
			changeTrigger.addEventListener('click', function (e) {
				e.preventDefault();
				animateForms(changePass_form);
			}, false);
		}

		if (passIngresarTrigger) {
			passIngresarTrigger.addEventListener('click', function () {
				animateForms(login_form);
			}, false);
		}

		if (passRegistrarTrigger) {
			passRegistrarTrigger.addEventListener('click', function () {
				animateForms(register_form);
			}, false);
		}
	});
}();
