(function () {
  $('.js-select').chosen({
    no_results_text: "No se encontró ningún centro de costo",
    allow_single_deselect: true
  });
  $('[data-toggle="tooltip"]').tooltip()

  var minDate = new Date();
  minDate.setDate(minDate.getDate() - 7);

  $('.js-datepicker-start').datetimepicker({
    format:     'd/m/Y',
    lang:       'es',
    minDate:    minDate,
    timepicker: false,
    onChangeDateTime: function () {
      // Vaciar el valor del calendario end-date si se cambia la fecha de start_date
      var end = $('.js-datepicker-end');
      end.val() ? end.val('') : true
    }
  });

})();
