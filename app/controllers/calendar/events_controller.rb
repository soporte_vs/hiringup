# coding: utf-8
class Calendar::EventsController < ApplicationController
  before_action :authenticate_user!, except: [:confirm_will_attend, :marked_will_attend]
  before_action :catch_enrollments, only: [:create, :update]
  before_action :catch_applicants, only: [:create_for_applicants, :update_for_applicants]
  before_action :catch_event, only: [:update, :update_for_applicants, :mark_attended, :mark_will_attend, :confirm_will_attend, :marked_will_attend]
  before_action :catch_event_resourceable, only: [:mark_attended, :mark_will_attend, :confirm_will_attend, :marked_will_attend]
  before_action :can_mark_attended, only: [:mark_attended, :mark_will_attend]
  before_action :check_token, only: [:confirm_will_attend]
  before_action :check_attended_param, only: [:mark_attended]
  before_action :check_will_attend_param, only: [:mark_will_attend]

  def create
    _stages = []
    _event_errors = []
    _enrollments_errors = 0
    @enrollments.each do |enrollment|
      if !current_user.can?(:admin, enrollment.course)
        enrollment.errors.add(:course, :enough_permission)
        _enrollments_errors += 1
      elsif enrollment.discarding.present?
        enrollment.errors.add(:course, :enrollment_disposed)
        _enrollments_errors += 1
      elsif enrollment.invited? && enrollment.invitation_rejected?
        enrollment.errors.add(:course, :course_invitation_rejected)
        _enrollments_errors += 1
      elsif !enrollment.stage.present?
        enrollment.errors.add(:course, :cant_schedule_without_stage)
        _enrollments_errors += 1
      elsif !enrollment.stage.agendable?
        enrollment.errors.add(:course, :stage_already_scheduled)
        _enrollments_errors += 1
      else
        _stages << enrollment.stage
      end
    end
    event = Calendar::Event.new(calendar_event_params)
    if _stages.any?
      event.stages = _stages
      event.save
      _event_errors = event.errors unless event.persisted?
    end

    respond_to do |format|
      if _event_errors.empty?
        format.json {
          render json: @enrollments.map{ |enrollment| enrollment.as_indexed_json }
        }
      else
        format.json {
          render json: _event_errors, status: :unprocessable_entity
        }
      end
    end
  end

  def update
    _stages = []
    _event_errors = []
    _enrollments_errors = 0
    @enrollments.each do |enrollment|
      if !current_user.can?(:admin, enrollment.course)
        enrollment.errors.add(:course, :enough_permission)
        _enrollments_errors += 1
      elsif enrollment.discarding.present?
        enrollment.errors.add(:course, :enrollment_disposed)
        _enrollments_errors += 1
      elsif enrollment.invited? && enrollment.invitation_rejected?
        enrollment.errors.add(:course, :course_invitation_rejected)
        _enrollments_errors += 1
      elsif !enrollment.stage.present?
        enrollment.errors.add(:course, :cant_schedule_without_stage)
        _enrollments_errors += 1
      else
        _stages << enrollment.stage
      end
    end

    if _stages.any?
      @event.stages = _stages
    end

    if _enrollments_errors == 0
      @event.attributes = calendar_event_params
      unless @event.save
        _event_errors = @event.errors if @event.errors.any?
      end
    end

    respond_to do |format|
      if _event_errors.empty?
        format.json {
          render json: @enrollments.map{ |enrollment| enrollment.as_indexed_json }
        }
      else
        format.json {
          render json: _event_errors, status: :unprocessable_entity
        }
      end
    end
  end

  def create_for_applicants
    _applicants = []
    _event_errors = []
    _applicants_errors = 0
    @applicants.each do |applicant|
      if !current_user.can?(:schedule, applicant)
        applicant.errors.add(:base, :enough_permission)
        _applicants_errors += 1
      else
        _applicants << applicant
      end
    end

    event = Calendar::Event.new(calendar_event_params)
    if _applicants.any? && _applicants_errors == 0
      event.applicants = _applicants
      event.save
      _event_errors = event.errors unless event.persisted?
    end

    respond_to do |format|
      if _event_errors.empty?
        format.json {
          render json: @applicants.map{ |applicant| applicant.as_indexed_json }
        }
      else
        format.json {
          render json: _event_errors, status: :unprocessable_entity
        }
      end
    end
  end

  def update_for_applicants
    _applicants = []
    _event_errors = []
    _applicants_errors = 0
    @applicants.each do |applicant|
      if !current_user.can?(:schedule, applicant)
        applicant.errors.add(:base, :enough_permission)
        _applicants_errors += 1
      else
        _applicants << applicant
      end
    end

    if _applicants.any?
      @event.applicants = _applicants
    end

    if _applicants_errors == 0
      @event.attributes = calendar_event_params
      unless @event.save
        _event_errors = @event.errors if @event.errors.any?
      end
    end

    respond_to do |format|
      if _event_errors.empty?
        format.json {
          render json: @applicants.map{ |applicant| applicant.as_indexed_json }
        }
      else
        format.json {
          render json: _event_errors, status: :unprocessable_entity
        }
      end
    end
  end

  # Recibe los parámetros por PUT cuando el worker marca que ya
  # asistió el candidato al evento. Esta acción la hace el worker en la vista
  # de la etapa, desde el componente EventMarkAttended
  def mark_attended
    attended = params[:attended]
    respond_to do |format|
      if @event_resourceable.update(attended: attended)
        format.json {
          render json: @event_resourceable
        }
      else
        format.json {
          render json: {errors: @event_resourceable.errors}
        }
      end
    end
  end

  # Recibe los parámetros por PUT cuando el worker marca que va a
  # asistir o no el candidato. Esta acción la hace el worker en la vista de la
  # etapa, desde el componente EventMarkWillAttend
  def mark_will_attend
    will_attend = params[:will_attend]
    respond_to do |format|
      if @event_resourceable.update(will_attend: will_attend)
        format.json {
          render json: @event_resourceable
        }
      else
        format.json {
          render json: {errors: @event_resourceable.errors}
        }
      end
    end
  end

  # Muestra el mensaje correspondiente al usuario luego de haber
  # hecho click en asistencia (o confirmar que no va) en el email del evento
  def marked_will_attend
  end

  # Recibe los parámetros por GET cuando el usuario hace click en
  # confirmar asistencia en el email del evento, y luego redirige a
  # `#marked_will_attend`
  def confirm_will_attend
    will_attend = params[:will_attend]
    respond_to do |format|
      if @event_resourceable.update(will_attend: will_attend)
        format.html {
          redirect_to marked_will_attend_calendar_event_path(id: @event.to_param, resourceable_id: @event_resourceable.resourceable.id, resourceable_type: @event_resourceable.resourceable.class.to_s)
        }
      else
        format.html {
          render 'errors/404', status: 404, layout: 'application'
        }
      end
    end
  end

  private
    def catch_event
      @event = Calendar::Event.find(params[:id])
      render 'errors/403', status: 403, layout: 'application' unless @event.present?
    end

    def catch_enrollments
      @enrollments = Enrollment::Base.where(id: params[:enrollment_ids])
    end

    def catch_applicants
      @applicants = Applicant::Base.where(id: params[:applicant_ids])
    end

    def catch_event_resourceable
      resourceable = params[:resourceable_type].constantize.find(params[:resourceable_id])
      @event_resourceable = Calendar::EventResourceable.find_by(resourceable: resourceable, event: @event)
      render 'errors/404', status: 404, layout: 'application' unless @event_resourceable.present?
    end

    def can_mark_attended
      if @event_resourceable.resourceable.is_a? Stage::Base
        unless current_user.can?(:admin) ||
               current_user.can?(:admin, @event_resourceable.resourceable.enrollment.course) ||
               current_user.can?(:admin, @event_resourceable.resourceable) ||
               current_user.can?(:admin, @event_resourceable.resourceable.class)
          render 'errors/403', status: 403, layout: 'application'
        end
      else
        unless current_user.can?(:admin) ||
               current_user.can?(:admin, Applicant::Base)
          render 'errors/403', status: 403, layout: 'application'
        end
      end
    end

    def check_attended_param
      unless params[:attended] != nil
        @event_resourceable.errors.add(:attended, :blank)
        respond_to do |format|
          format.json {
            render json: {errors: @event_resourceable.errors.full_messages}
          }
        end
      end
    end

    def check_will_attend_param
      unless params[:will_attend] != nil
        @event_resourceable.errors.add(:will_attend, :blank)
        respond_to do |format|
          format.json {
            render json: {errors: @event_resourceable.errors.full_messages}
          }
        end
      end
    end

    def calendar_event_params
      params.require(:calendar_event).permit(
        :starts_at,
        :ends_at,
        :address,
        :lat,
        :lng,
        :observations,
        guests_attributes: [:id, :name, :email, :_destroy],
        manager_attributes: [:id, :name, :email, :_destroy]
      ).merge(created_by: current_user)
    end

    def check_token
      received_token = params[:token]
      token = Digest::MD5.new
      if @event_resourceable.resourceable.is_a? Stage::Base
        token << @event_resourceable.resourceable.enrollment.applicant.email
        @token = "#{token}-#{@event_resourceable.resourceable.enrollment.id}"
      elsif @event_resourceable.resourceable.is_a? Applicant::Base
        token << @event_resourceable.resourceable.email
        @token = "#{token}-#{@event_resourceable.resourceable.id}"
      end

      unless received_token == @token
        render 'errors/403', status: 403, layout: 'application'
      end
    end
end
