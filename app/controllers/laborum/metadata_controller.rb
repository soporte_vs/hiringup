class Laborum::MetadataController < ApplicationController
  before_action :has_account?

  def areas
    # __method__ return this method name
    # __method__ => 'areas'
    render json: meta_data(__method__)
  end

  def subareas
    data = if params[:md].present?
      Rails.cache.fetch("bumeran/#{__method__}/#{params[:md]}", expires_in: 6.hours) do
        Bumeran.get_subareas_in(params[:md])
      end
    else
      {}
    end
    render json: data
  end

  def frecuencias_pago
    render json: meta_data(__method__)
  end

  def denominaciones
    render json: meta_data(__method__)
  end

  def direcciones
    render json: meta_data(__method__)
  end

  def idiomas
    render json: meta_data(__method__)
  end

  def niveles_idiomas
    render json: meta_data(__method__)
  end

  def industrias
    render json: meta_data(__method__)
  end

  def tipos_trabajo
    render json: meta_data(__method__)
  end

  def areas_estudio
    render json: meta_data(__method__)
  end

  def estados_estudio
    render json: meta_data(__method__)
  end

  def tipos_estudio
    render json: meta_data(__method__)
  end

  def paises
    render json: meta_data(__method__)
  end

  def denominaciones
    render json: meta_data(__method__)
  end

  def regiones
    data = Rails.cache.fetch("bumeran/#{__method__}/#{Laborum::Publication::DEFAULT_COUNTRY}", expires_in: 6.hours) do
      Bumeran.get_zonas_in(Laborum::Publication::DEFAULT_COUNTRY)
    end
    render json: data
  end

  def comunas
    if params[:md].present?
      data = Rails.cache.fetch("bumeran/#{__method__}/#{params[:md]}", expires_in: 6.hours) do
        Bumeran.get_localidades_in(params[:md])
      end
    else
      data = {}
    end
    render json: data
  end

  private
    def meta_data(method)
      Rails.cache.fetch("bumeran/#{method}", expires_in: 6.hours) do
        Bumeran.send method
      end
    end

    def has_account?
      render 'errors/403', status: 403, layout: 'application' unless Laborum::Publication.has_account?
    end
end
