class Laborum::PublicationsController < ApplicationController
  layout 'course'
  before_action :has_account?
  before_action :authenticate_user!
  before_action :set_course
  before_action :authorize_action
  before_action :set_laborum_publication, if: "params[:publication_id].present?"

  def index
    @laborum_publications = @course.laborum_publications.order(created_at: :desc).page(params[:page])
  end

  def new
    @laborum_publication = @course.laborum_publications.new(title: @course.title, description: @course.description)
    last_trabajando_publication = @course.trabajando_publications.select("q1, q2, q3, q4, q5").last
    last_minisite_publication = @course.minisite_publications.order(created_at: :desc).first

    if last_minisite_publication
      last_minisite_publication.questions.first(5).each_with_index do |question, index|
        @laborum_publication.send("question#{index+1}=", question.description)
      end
    elsif last_trabajando_publication
      last_trabajando_publication.attributes.each_with_index do |(attr_name, attr_value), index|
        @laborum_publication.send("question#{index+1}=", attr_value)  if attr_value.present?
      end
    end
  end

  def create
    @laborum_publication = @course.laborum_publications.new(laborum_publication_params)
    if @laborum_publication.save
      flash[:success] = 'Se esta creando la publicacion en laborum, esto puede tardar unos minutos'
      #redirect_to laborum_publications_path(@course)
      redirect_to laborum_publication_path(:publication_id => @laborum_publication)
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @laborum_publication.update(laborum_publication_params)
      flash[:success] = 'Se esta actualizando la publicacion en laborum, esto puede tardar unos minutos'
      redirect_to laborum_publication_path(:publication_id => @laborum_publication)
    else
      flash.now[:error] = @laborum_publication.errors.messages
      render :edit
    end
  end

  def show
    @laborum_postulations = @laborum_publication.postulations.page(params[:page])
  end

  def destroy
    if @laborum_publication.update(status: 'unpublished')
      @laborum_publication.unpublish
      flash[:success] = 'La publicacion ha sido despublicada correctamente.'
      redirect_to laborum_publications_path(@course)
    else
      flash[:error] = 'La publicacion ha sido despublicada correctamente.'
      redirect_to laborum_publications_path(@course)
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_course
      @course = Course.find_by(id: params[:id])
      render 'errors/404', status: 404, layout: 'application' unless @course
    end

    def authorize_action
      unless current_user.can?(:admin_publications, @course)
        render 'errors/403', status: 403, layout: 'application'
      end
    end

    def set_laborum_publication
      @laborum_publication = @course.laborum_publications.find(params[:publication_id])
      render 'errors/404', status: 404, layout: 'application' unless @laborum_publication
    end

    def laborum_publication_params
      params.require(:laborum_publication).permit(
        :title,
        :description,
        :desactivate_at,
        :state_id,
        :city_id,
        :address,
        :area_id,
        :subarea_id,
        :pay_id,
        :language_id,
        :language_level_id,
        :type_job_id,
        :study_area_id,
        :study_type_id,
        :study_status_id,
        :denomination_id,
        :question1,
        :question2,
        :question3,
        :question4,
        :question5
      )
    end

    def has_account?
      render 'errors/403', status: 403, layout: 'application' unless Laborum::Publication.has_account?
    end
end
