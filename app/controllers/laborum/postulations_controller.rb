# coding: utf-8
class Laborum::PostulationsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_postulation
  before_action :authorize_action

  def accept
    if @postulation.accept
      flash[:success] = "Se ha aceptado la postulación de #{@postulation.applicant.first_name}, y se le ha enviado un correo de notificación"
      @course.enroll!(@postulation.applicant)
    else
      flash.now[:notice] = "#{@postulation.errors.full_messages}"
    end
    redirect_to laborum_publication_path(id: @course.id, publication_id: @publication.id)
  end

  def discard
    if @postulation.reject
      flash[:success] = "Se ha descartado la postulación de #{@postulation.applicant.first_name}."
    else
      flash.now[:notice] = "#{@postulation.errors.full_messages}"
    end
    redirect_to laborum_publication_path(id: @course.id, publication_id: @publication.id)
  end

  def show
  end

  private
    def set_postulation
      @postulation = Laborum::Postulation.find_by(id: params[:id])
      if not @postulation
        render 'errors/404', status: 404, layout: 'application'
      else
        @publication = @postulation.laborum_publication
        @course = @publication.course
      end
    end

    def authorize_action
      unless current_user.can?(:admin_publications, @postulation.laborum_publication.course)
        render 'errors/403', status: 403, layout: 'application'
      end
    end

end
