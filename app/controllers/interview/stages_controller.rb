class Interview::StagesController < Stage::BasesController
  layout 'stage'
  before_action :authorize_action

  def show
    @interview_report = Interview::Report.new(interview_stage: @stage)
  end

  private
    def catch_stage
      begin
        @stage = Interview::Stage.find(params[:stage_id])
        raise "" if @stage.enrollment.course.id != @course.id
      rescue Exception => e
        render 'errors/404', status: 404, layout: 'application'
      end
    end

    def authorize_action
      # Customizado para Empressas SB
      if !(valid_action_permission?(@stage))
        render 'errors/403', status: 403, layout: 'application'
      end
    end

    def valid_action_permission?(stage)
      current_user.can?(:admin, @course) ||
      current_user.can?(:admin, Interview::Stage) ||
      current_user.can?(:admin) ||
      (Role.find_by(resource_type: stage.class.to_s, resource_id: stage.id, name: :admin).try(:users) || []).include?(current_user)
    end

end
