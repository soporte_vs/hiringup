class Interview::ReportsController < Interview::StagesController
  def download
    document = @stage.interview_report.document
    send_file document.path, filename: document.filename
  end

  def create
    @interview_report = Interview::Report.new(report_params)

    if @interview_report.save
      flash[:success] = I18n.t('interview.report.created.success')
      redirect_to course_interview_stage_path(id: @course, stage_id: @stage.id)
    else
      render template: 'interview/stages/show'
    end
  end
  private

    def report_params
      params.require(:interview_report).permit(
        :observations,
        :document
      ).merge(interview_stage_id: @stage.id)
    end
end
