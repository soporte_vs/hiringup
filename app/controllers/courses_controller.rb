# coding: utf-8
class CoursesController < ApplicationController
  before_action :authenticate_user!
  before_action :catch_course, if: "params[:id].present?"

  before_action :reject_postulant, except: [:new, :search, :create, :_search]
  before_action :authorize_action, except: [:update, :edit, :clone, :candidates, :publications, :detail, :security, :_search, :search]
  before_action :authorize_action_show, only: [:show, :candidates, :detail, :security]
  before_action :authorize_action_search, only: [:search, :_search]
  before_action :authorize_action_publication, only: [:publications]
  before_action :authorize_action_update, only: [:edit, :update]
  before_action :authorize_action_update_security, only: :update_security
  before_action :authorize_clone, only: :clone
  before_action :course_opened?, only: [:edit, :update]

  def _search_candidates
    attrs = params.except(:action, :controller, :course, :_source, :id)
    attrs[:query] = {
      filtered: {
        query: attrs[:query],
        filter: {
          terms: {
            course_id: [@course.id]
          }
        }
      }
    }
    attrs[:sort].unshift({"_score": "desc"}) if attrs[:sort].present?
    response_elastic = Enrollment::Base.query(attrs)
    render json: response_elastic.response
  end

  def candidates
    @enrollments = @course.enrollments
    respond_to do |format|
      format.html { redirect_to action: 'show' }
      format.json { render json: @enrollments.map{ |enrollment| enrollment.as_indexed_json } }
    end
  end

  # PATCH/PUT /app/courses/records/1/switch_state
  # PATCH/PUT /app/courses/records/1/switch_state.json
  def switch_state
    respond_to do |format|
      begin
        @course.attributes = swicth_state_params
        if @course.send("#{params[:switch]}!")
          @course.create_activity key: "course.close", owner: current_user, recipient: @course if params[:switch] == 'close'
          flash[:success] = I18n.t("courses.actions.#{params[:switch]}.success")
          format.html { redirect_to @course}
          format.json { render :show, status: :ok, location: @course }
        else
          error_message = t("courses.actions.#{params[:switch]}.unsuccess")
          format.html do
              flash[:error] = error_message
              redirect_to course_path(record_id: @course)
            end
          format.json do
            render json: error_message, status: :unprocessable_entity
          end
        end
      rescue Exception => e
        if @course.errors.has_key?(:aasm_state)
          error = @course.errors.messages[:aasm_state].join('; ')
        else
          action = t("courses.actions.#{params[:switch]}.text").downcase
          state = t("courses.states.#{@course.aasm_state}.default")
          error = t("courses.errors.transition", action: action, state: state)
        end
        format.html do
          flash[:error] = error
          redirect_to course_path(record_id: @course)
        end
        format.json { render json: error }
      end
    end
  end

  def publications
    redirect_to action: 'show', anchor: 'publications'
  end

  def statistics
    redirect_to action: 'show', anchor: 'statistics'
  end

  def security
    redirect_to action: 'show', anchor: 'security'
  end

  def clone
    clone = @course.dup
    clone.user = current_user
    clone.aasm_state = :opened
    clone.user_id = current_user.id

    respond_to do |format|
      if clone.save

        current_user.add_role :admin, clone
        #Si es un ejecutivo crea el mismo permiso para los miembros de su grupo
        current_user.groups.each do |group|
          group.users.each do |user|
            user.add_role :admin, clone
          end
        end

        flash[:success] = t("course.clone.create_success")
        clone.create_activity key: 'course.create', owner: current_user
        format.html { redirect_to edit_course_path(clone) }
      else
        flash[:error] = clone.errors.messages
        format.html { render action: 'show' }
        format.json { render json: clone.errors, status: :unprocessable_entity }
      end
    end
  end

  def show
    @activities = @course.activities.page(params[:page])
  end

  def _search
    attrs = params.except(:action, :controller, :course, :_source)
    @courses = Course.filter_by_role(:show, current_user)
    # Se filtran por los que pueda ver el usuario
    attrs[:query] = {
      filtered: {
        query: attrs[:query],
        filter: {
          terms: {
            id: @courses.ids
          }
        }
      }
    }
    attrs[:sort].unshift({"_score": "desc"}) if attrs[:sort].present?
    response_elastic = Course.query(attrs)
    render json: response_elastic.response
  end

  def search
    course_ids = Course.opened.filter_by_role(:enroll, current_user).ids

    response_elastic = Course.query(elastic_query_course_ids(course_ids))
    elastic_courses = response_elastic.response.hits.hits
    respond_to do |format|
      format.html
      format.json { render json: elastic_courses.map{|ec| ec._source } }
    end
  end

  def new
    if params[:vacancy_ids].present?
      vacancies = Vacancy.find(params[:vacancy_ids])
      @course = Course.new(vacancies: vacancies)
      @course.company_position_id = vacancies.first.company_positions_cenco.company_position_id
    else
      @course = Course.new(vacancies: [Vacancy.new])
      @course.title = params[:course_title] if params[:course_title]
    end
  end

  def edit
  end

  def update
    respond_to do |format|
      if @course.errors.empty? && @course.update(course_params)
        flash[:success] = t("course.update_success")
        format.html { redirect_to course_path(@course) }
      else
        format.html { render :edit }
        format.json { render json: @course.errors, status: :unprocessable_entity }
      end
    end
  end

  def update_security
    respond_to do |format|
      security_params[:security].each do |r|
        begin
          role = @course.roles.find_or_create_by(name: r[:role_name])
          role.update(user_ids: r[:users])
        rescue Exception => e
          flash[:error] = t("course.security.update_failure")
          format.html { redirect_to security_course_path(@course) }
        end
      end
      flash[:success] = t("course.security.update_success")
      format.html { redirect_to security_course_path(@course) }
    end
  end

  def create
    @course = Course.new(course_params)
    @course.user = current_user

    respond_to do |format|
      if @course.save
        current_user.add_role :admin, @course

        current_user.groups.each do |group|
          group.users.each do |user|
            user.add_role :admin, @course
          end
        end

        flash[:success] = t("course.create_success")
        @course.create_activity key: 'course.create', owner: current_user
        format.html { redirect_to course_path(@course) }
      else
        format.html { render action: 'new' }
        format.json { render json: @course.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Método para constuir la query que se debe enviar a
    # Elasticsearch para buscar procesos filtrando por id
    def elastic_query_course_ids(course_ids)
      {
        query:{
          filtered: {
            filter: {
              terms: {
                id: course_ids
              }
            }
          }
        },
        size: course_ids.length
      }
    end

    def security_params
      params.require(:course).permit(security: [:role_name, users: []])
    end

    def swicth_state_params
      params.require(:course).permit(:observations_closing)
    end

    def course_params
      params.require(:course).permit(
        :type_enrollment,
        :title,
        :description,
        :company_position_id,
        :document_group_id,
        :timetable_id,
        :contract_type_id,
        :request_reason_id,
        :start,
        :experience_years,
        :is_supervisor,
        :study_type,
        :study_level_id,
        :applicant_profile_type_id,
        :applicant_software_level_id,
        :area_id,
        :engagement_origin_id,
        :minimun_requirements,
        career_ids: [],
        video_interview_question_ids: [],
        vacancies_attributes: [
          :id,
          :company_positions_cenco_id,
          :cenco_id,
          :company_vacancy_request_reason_id,
          :observations,
          :qty, # cuántas de estas vacantes iguales voy a crear
          :_destroy,
          :vacancy_ids, # para saber cuáles vacantes voy a actualizar
          vacancy_ids: [] # pueden venir como string o como array también
        ]
      ).merge(
        territory_city_id: params[:course][:territory_city],
        tag_ids: params[:course][:tags],
      )
    end

    def catch_course
      @course = nil
      if params.has_key? :id
        begin
          @course = Course.find(params[:id])
        rescue Exception => e
          render 'errors/404', :status => 404, layout: 'application'
        end
      end
    end

    def course_opened?
      unless @course.opened?
        flash[:error] = I18n.t('courses.errors.update.course_closed')
        redirect_to course_path(@course)
      end
    end

    def authorize_action
      render 'errors/403', :status => 403, layout: 'application' unless current_user.can?(params[:action].to_sym,@course||Course)
    end

    def authorize_action_show
      render 'errors/403', :status => 403, layout: 'application' unless current_user.can?(:show,@course||Course)
    end

    def authorize_action_search
      render 'errors/403', :status => 403, layout: 'application' unless current_user.can?(:search, Course)
    end

    def authorize_action_publication
      render 'errors/403', :status => 403, layout: 'application' unless current_user.can?(:admin_publications, @course||Course)
    end

    def authorize_action_update
      render 'errors/403', :status => 403, layout: 'application' unless current_user.can?(:update,@course||Course)
    end

    def authorize_action_update_security
      if !current_user.can?(:admin, @course || Course)
        render 'errors/403', :status => 403, layout: 'application'
      end
    end

    def authorize_clone
      render 'errors/403', :status => 403, layout: 'application' unless current_user.can?(:create, Course)
    end

    def reject_postulant
      if @course.applicants.include?(current_user.applicant)
        render 'errors/403', :status => 403, layout: 'application'
      end
    end
end
