class ProfilesController < ApplicationController
  include ApplicantDocumentActions
  before_action :authenticate_user!

  around_action :custom_validators, only: [:edit, :update]
  around_action :set_document_records, only: [:edit, :update, :upload_documents]

  def edit
    if !current_user.professional_information
      People::ProfessionalInformation.create(user: current_user)
    end
    current_user.reload
  end

  def update
    respond_to do |format|
      if current_user.update(profile_params)
        flash[:success] = I18n.t('profiles.updated_success')
        format.html { redirect_to edit_profiles_path }
      else
        flash.now[:error] = current_user.errors.messages
        format.html { render :edit }
        format.json { render json: current_user.errors, status: :unprocessable_entity }
      end
    end
  end

  # upload_documents por defecto redirecciona a :back
  def upload_documents
    respond_to do |format|
      format.html do
        do_upload_documents
        redirect_to edit_profiles_path
      end

      format.json do
        @document_records = do_upload_documents_ajax
        render template: 'document/records/index'
      end
    end
  end

  def download_document
    @applicant = current_user.applicant
    super
  end

  private
    def set_document_records
      @document_records = []
      @applicant = current_user.applicant || current_user.create_applicant
      Document::Descriptor.all.each do |descriptor|
        record = @applicant.get_latest_document_record(descriptor.name, nil)
        if record.nil?
          record = Document::Record.new(applicant_id: @applicant.id, document_descriptor: descriptor)
        end
        @document_records << record
      end
      yield
    end

    def profile_params
      params.require(:user).permit(
        :id,
        :email,
        professional_information_attributes: [
          :id,
          degrees_attributes: [
            :id,
            :type,
            :name,
            :career_id,
            :institution_id,
            :culmination_date,
            :condition,
            :_destroy
          ],
          laboral_experiences_attributes: [
            :id,
            :position,
            :description,
            :company,
            :since_date,
            :until_date,
            :_destroy
          ],
          language_skills_attributes: [
            :id,
            :education_language_id,
            :level,
            :_destroy
          ],
          software_skills_attributes: [
            :id,
            :level,
            :education_software_id,
            :_destroy
          ]
        ],
        personal_information_attributes: [
          :id,
          :rut,
          :first_name,
          :last_name,
          :birth_date,
          :study_type,
          :landline,
          :cellphone,
          :sex,
          :territory_city_id,
          :avatar,
          :address,
          :nationality_id,
          :company_marital_status_id,
          :recruitment_source_id,
          :areas_of_interest,
          :identification_document_number,
          :identification_document_type_id,
          :availability_replacements,
          :availability_work_other_residence,
          personal_referrals_attributes: [
            :id,
            :full_name,
            :email,
            :phone,
            :_destroy
          ]
        ]
      )
    end

    def custom_validators
      if !current_user.personal_information
        # Se saltan las validaciones debido a que es necesario crear una instancia de personal_information
        # asociada al User del applicant para que este pueda editar dichos datos. De lo contrario no se muestra
        # el formulario al acceder al perfil de dicho usuario. (Debido a que USER no tiene un atributo
        # que se llame personal_information, es necesario persistirlo referenciando al usuario al cual pertenece).
        personal_information = People::PersonalInformation.new(user: current_user)
        personal_information.save(validate: false)
      end
      current_user.personal_information.active_custom_presence_validators!
      yield
      current_user.personal_information.deactive_custom_presence_validators!
    end
end
