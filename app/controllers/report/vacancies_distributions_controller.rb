class Report::VacanciesDistributionsController < ApplicationController
  before_action :authenticate_user
  before_action :authorize_action

  respond_to :xml, :jasperpdf

  def new
  end

  def report
    starts_at = params[:starts_at] || l(Date.today.at_beginning_of_month)
    ends_at = params[:ends_at] || l(Date.today)
    company_ids = "#{params[:company_ids].present? ? params[:company_ids].join(',') : 'NULL'}"
    company_business_unit_ids = "#{params[:company_business_unit_ids].present? ? params[:company_business_unit_ids].join(',') : 'NULL'}"

    @company_business_units = Company::BusinessUnit.find_by_sql(
      "select crd.name as empresa, bu.name as gerencia,
        (select AVG((CASE WHEN va.closed_at IS NOT NULL THEN va.closed_at ELSE NOW() END) - va.created_at)
          from
            vacancies as va,
            company_positions_cencos as pcenco,
            company_cencos as cenco
          where
            (
              ( va.created_at <= to_date('#{ends_at}', 'DD/MM/YYYY') and va.created_at >= to_date('#{starts_at}', 'DD/MM/YYY') ) or
              ( va.closed_at <= to_date('#{ends_at}', 'DD/MM/YYYY') and va.closed_at >= to_date('#{starts_at}', 'DD/MM/YYY') )
            ) and
            va.company_positions_cenco_id = pcenco.id and
            pcenco.company_cenco_id = cenco.id and
            cenco.company_management_id = cmg.id
        ) as avg_days_closed,
        (select count(*)
          from
            vacancies as va,
            company_positions_cencos as pcenco,
            company_cencos as cenco
          where
            (
              ( va.created_at <= to_date('#{ends_at}', 'DD/MM/YYYY') and va.created_at >= to_date('#{starts_at}', 'DD/MM/YYY') ) or
              ( va.closed_at <= to_date('#{ends_at}', 'DD/MM/YYYY') and va.closed_at >= to_date('#{starts_at}', 'DD/MM/YYY') )
            ) and
            va.company_positions_cenco_id = pcenco.id and
            pcenco.company_cenco_id = cenco.id and
            cenco.company_management_id = cmg.id and
            (va.closed_at IS NOT NULL)
        ) as vacancy_count_closed,
        (select count(*)
          from
            vacancies as va,
            company_positions_cencos as pcenco,
            company_cencos as cenco
          where
            (
              ( va.created_at <= to_date('#{ends_at}', 'DD/MM/YYYY') and va.created_at >= to_date('#{starts_at}', 'DD/MM/YYY') ) or
              ( va.closed_at <= to_date('#{ends_at}', 'DD/MM/YYYY') and va.closed_at >= to_date('#{starts_at}', 'DD/MM/YYY') )
            ) and
            va.company_positions_cenco_id = pcenco.id and
            pcenco.company_cenco_id = cenco.id and
            cenco.company_management_id = cmg.id and
            (va.closed_at IS NULL)
        ) as vacancy_count
        from
          company_business_units as bu,
          company_managements as cmg,
          company_records as crd
        where
          bu.id = cmg.company_business_unit_id and
          cmg.company_record_id = crd.id and
          crd.id in (#{company_ids}) and
          bu.id in (#{company_business_unit_ids})"
    )
    @company_business_units = @company_business_units.reject do |bu|
      (bu.vacancy_count + bu.vacancy_count_closed) == 0
    end
    if @company_business_units.empty?
      flash.now[:notice] = I18n.t('reports.messages.no_result')
      request.format = :html
      respond_to do |format|
        format.html { render :new }
      end
    else
      @REPORT_TITLE = I18n.t('reports.vacancies_distributions')
      @FILTERS_APPLIED = "
        #{I18n.t('reports.filters.starts_at')}: #{starts_at} #{I18n.t('reports.filters.ends_at')}: #{ends_at}
        #{I18n.t('reports.filters.company_ids')}: #{Company::Record.find(params[:company_ids]).map(&:name).join(', ')}
        #{I18n.t('reports.filters.company_business_unit_ids')}: #{Company::BusinessUnit.find(params[:company_business_unit_ids]).map(&:name).join(', ')}
      "
      respond_with @company_business_units
    end
  end

  private

    def authenticate_user
      redirect_to new_user_session_path unless user_signed_in?
    end

    def authorize_action
      render 'errors/403', status: 403, layout: 'application' unless current_user.can?(:admin) || current_user.can?(:admin_reports)
    end
end
