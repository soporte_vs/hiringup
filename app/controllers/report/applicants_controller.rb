# coding: utf-8
class Report::ApplicantsController < ApplicationController
  before_action :authenticate_user
  before_action :authorize_action
  before_action :set_applicant_report, only: [:download]

  def new
  end

  def generate
    report = Applicant::Report.new(report_params)
    if report.save
      flash[:success] = "El reporte será enviado a su correo electrónico #{current_user.email}"
      redirect_to new_report_applicant_path
    else
      flash[:error] = report.errors.messages
      redirect_to new_report_applicant_path
    end
  end

  def download
    send_file(@applicant_report.report_file, disposition: 'inline')
  end

  private

    def authenticate_user
      redirect_to new_user_session_path unless user_signed_in?
    end

    def set_applicant_report
      @applicant_report = Applicant::Report.find_by id: params[:id]
    end

    def authorize_action
      render 'errors/403', status: 403, layout: 'application' unless current_user.can?(:admin) || current_user.can?(:admin_reports)
    end

    def report_params
      report_params = params.permit(
        applicant: [
          :first_name,
          :last_name,
          :sex,
          :country,
          :state,
          :city,
          :study_type,
          :degree_name,
          {:degree_career_ids => []},
          {:degree_university_ids => []},
          :degree_condition,
        ],
        course: [
          :country, # país
          :state, # región
          :city, # comuna
          { :tags => [] },
          { :engagement_origin_ids => []}, # orígenes de la contratación
        ]
      )
      {
        user_id: current_user.id,
        params: report_params
      }
    end
end
