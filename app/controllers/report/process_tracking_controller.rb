# coding: utf-8
class Report::ProcessTrackingController < ApplicationController
  before_action :authenticate_user
  before_action :authorize_action

  def new
  end

  def generate
    report = ProcessTracking::Report.new(report_params)
    if report.valid?
      @columns = ProcessTracking::Report.columns
      @report = report
      respond_to do |format|
        format.xlsx {
          response.headers['Content-Disposition'] = "attachment; filename=seguimiento_de_procesos.xlsx"
        }
        format.html {
          Courses::ReportMailer.delay.send_to_user(current_user, report)
          flash[:success] = "El reporte se ha enviado a su correo electrónico #{current_user.email}"
          redirect_to new_report_process_tracking_path
        }
      end
    else
      flash[:error] = report.errors.messages
      redirect_to new_report_process_tracking_path
    end
  end

  private

    def authenticate_user
      redirect_to new_user_session_path unless user_signed_in?
    end

    def authorize_action
      render 'errors/403', status: 403, layout: 'application' unless current_user.can?(:admin) || current_user.can?(:admin_reports)
    end

    def report_params
      if current_user
        params.permit(
          :title,
          :country, # país
          :state, # región
          :city, # comuna
          {:tags =>[]},
          {:stages => []}, # etapa actual, puede seleccionar muchas
          {:position_ids => []},
          {:engagement_origin_ids => []}, # orígenes de la contratación
        )
      end
    end
end
