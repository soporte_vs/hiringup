class Report::VacanciesEvolutionController < ApplicationController
  before_action :authenticate_user
  before_action :authorize_action

  respond_to :xml, :jasperpdf

  def new
  end

  def report
    starts_at = params[:starts_at] || I18n.l(Date.today.at_beginning_of_month)
    ends_at = params[:ends_at] || I18n.l(Date.today)
    company_ids = "#{params[:company_ids].present? ? params[:company_ids].join(',') : 'NULL'}"
    company_business_unit_ids = "#{params[:company_business_unit_ids].present? ? params[:company_business_unit_ids].join(',') : 'NULL'}"

    @vacancies = Vacancy.find_by_sql(
      "select
        crd.name as company,
        bu.name as business_unit,
        to_char(va.created_at, 'YYYY/MM/DD') as date,
        va.company_positions_cenco_id as company_positions_cenco_id,
        count(*) as vacancy_count
       from
        vacancies as va,
        company_positions_cencos as pcenco,
        company_cencos as cenco,
        company_business_units as bu,
        company_managements as cmg,
        company_records as crd
       where
        va.company_positions_cenco_id = pcenco.id and
        pcenco.company_cenco_id = cenco.id and
        cenco.company_management_id = cmg.id and
        cmg.company_business_unit_id = bu.id and
        cmg.company_record_id = crd.id and
        ( va.created_at >= to_date('#{starts_at}', 'DD/MM/YYYY') and va.created_at <= to_date('#{ends_at}', 'DD/MM/YYYY') ) and
        crd.id in (#{company_ids}) and
        bu.id in (#{company_business_unit_ids})
       group by
        company, business_unit, date, company_positions_cenco_id
       order by
        company ASC
      "
    )
    if @vacancies.empty?
      flash.now[:notice] = I18n.t('reports.messages.no_result')
      request.format = :html
      respond_to do |format|
        format.html { render :new }
      end
    else
      @REPORT_TITLE = I18n.t('reports.vacancies_evolution')
      @FILTERS_APPLIED = "
        #{I18n.t('reports.filters.starts_at')}: #{starts_at} #{I18n.t('reports.filters.ends_at')}: #{ends_at}
        #{I18n.t('reports.filters.company_ids')}: #{Company::Record.find(params[:company_ids]).map(&:name).join(', ')}
        #{I18n.t('reports.filters.company_business_unit_ids')}: #{Company::BusinessUnit.find(params[:company_business_unit_ids]).map(&:name).join(', ')}
      "
      respond_with @vacancies
    end
  end

  private

    def authenticate_user
      redirect_to new_user_session_path unless user_signed_in?
    end

    def authorize_action
      render 'errors/403', status: 403, layout: 'application' unless current_user.can?(:admin) || current_user.can?(:admin_reports)
    end
end
