class Report::GeneralPerMonthController < ApplicationController
  before_action :authenticate_user
  before_action :authorize_action

  def new
  end

  def report
    month = params[:date][:month] rescue Time.new.month
    year = params[:date][:year] rescue Time.new.year
    @date = Date.new(year.to_i,month.to_i,01)
    @month_name = I18n.l(@date, format: "%B").capitalize
    @courses = Course.where(created_at: @date.beginning_of_month..@date.end_of_month)

    if @courses.any?
      request.format = :xlsx
      respond_to do |format|
        format.xlsx {
          response.headers['Content-Disposition'] = "attachment; filename='reporte_general_#{@month_name.downcase}_#{@date.year}.xlsx'"
        }
      end
    else
      request.format = :html
      respond_to do |format|
        format.html {
          flash[:error] = "No hay procesos creados en el mes y año seleccionado"
          redirect_to :back
        }
      end
    end
  end

  def authenticate_user
    redirect_to new_user_session_path unless user_signed_in?
  end

  def authorize_action
    render 'errors/403', status: 403, layout: 'application' unless current_user.can?(:admin)
  end
end
