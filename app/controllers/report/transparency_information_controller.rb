class Report::TransparencyInformationController < ApplicationController
  before_action :authenticate_user
  before_action :authorize_action

  MONTHS_PER_PAGE = 10

  def new
    @months = months(0)
  end

  def generate
    @COLUMNS = [
      "Denominacion o tipologia",
      "Tipo normal",
      "Numero",
      "Nombre o titulo",
      "Fecha",
      "Fecha medio y forma de\npublicacion",
      "Tiene efectos\ngenerales",
      "Fecha ultima actualizacion si corresponde a\nactos y resoluciones con efectos\ngenerales",
      "Breve descripcion del\nobjeto del acto",
      "Enlace aviso"
    ]
    @PREDEFINED_CONTENT = [
      "Llamados a concurso de\npersonal",
      "Instruccion",
      "PO RH216",
      "",
      "",
      "web Integra Trabajando.Com\naviso de concurso",
      "No",
      "No Aplica",
      "Reclutamiento\npersonal",
      "",
      ""
    ]
    @year = params[:year].to_i
    month = params[:month].to_i
    datetime_start = DateTime.new(@year, month, 1, 00, 00, 0)
    datetime_end = DateTime.new(@year, month, 1, 00, 00, 0).beginning_of_month + 1.months - 1.seconds
    @month_string = ["", "ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE"][month]
    @publications = Minisite::Publication.where("created_at >= ? and created_at <= ?", datetime_start, datetime_end)
    request.format = :xlsx
    respond_to do |format|
      format.xlsx
    end
  end

  def download_publications
    transparency_report = Transparency::Report.new(params[:year].to_i, params[:month].to_i)
    zip = transparency_report.generate_zip
    send_file(transparency_report.zipfile_path, filename: transparency_report.name + '.zip')
  end

  private
    def months(page)
      end_date = (DateTime.now - (page*MONTHS_PER_PAGE).months)
      (0..MONTHS_PER_PAGE-1).to_a.map {|x|
        end_date - x.months
      }
    end

    def authenticate_user
      redirect_to new_user_session_path unless user_signed_in?
    end

    def authorize_action
      render 'errors/403', status: 403, layout: 'application' unless current_user.can?(:admin) || current_user.can?(:admin_reports)
    end
end
