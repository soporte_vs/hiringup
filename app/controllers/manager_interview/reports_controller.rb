class ManagerInterview::ReportsController < ManagerInterview::StagesController
  def download
    stage_document = @stage.manager_interview_report.documents.find(params[:stage_document_id])
    send_file stage_document.document.path, filename: stage_document.document.filename
  end

  def create
    @manager_interview_report = ManagerInterview::Report.new(report_params)
    @manager_interview_report.documents.each{ |doc| doc.resource = @manager_interview_report }

    if @manager_interview_report.save
      flash[:success] = I18n.t('manager_interview.report.created.success')
      redirect_to course_manager_interview_stage_path(id: @course, stage_id: @stage.id)
    else
      render template: 'manager_interview/stages/show'
    end
  end

  private

    def report_params
      attrs = params.require(:manager_interview_report).permit(:observations)
      attrs[:documents] = []
      document_attrs = params[:manager_interview_report][:documents] || []
      document_attrs.each do |document_val|
        attrs[:documents] << Stage::Document.new(document: document_val)
      end
      attrs.merge(manager_interview_stage_id: @stage.id)
    end
end
