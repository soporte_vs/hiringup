class GroupsController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize_action
  before_action :set_group, only: [:show, :edit, :update, :destroy]

  # GET /groups
  # GET /groups.json
  def index
    respond_to do |format|
      format.html { @groups = Group.page(params[:page]) }
      format.json { @groups = Group.all }
    end
  end

  # GET /groups/1
  # GET /groups/1.json
  def show
  end

  # GET /groups/new
  def new
    @group = Group.new
  end

  # GET /groups/1/edit
  def edit
  end

  # POST /groups
  # POST /groups.json
  def create
    @group = Group.new(group_params)

    respond_to do |format|
      if @group.save
        flash[:success] = t("resources.groups.create_success")
        format.html { redirect_to @group }
        format.json { render :show, status: :created, location: @group }
      else
        flash.now[:error] = @group.errors.messages
        format.html { render :new }
        format.json { render json: @group.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /groups/1
  # PATCH/PUT /groups/1.json
  def update
    respond_to do |format|
      if @group.update(group_params)
        flash[:success] = t("resources.groups.update_success")
        format.html { redirect_to @group }
        format.json { render :show, status: :ok, location: @group }
      else
        flash.now[:error] = @group.errors.messages
        format.html { render :edit }
        format.json { render json: @group.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /groups/1
  # DELETE /groups/1.json
  def destroy
    @group.destroy
    respond_to do |format|
      flash[:success] = t("resources.groups.destroy_success")
      format.html { redirect_to groups_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_group
      @group = Group.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def group_params
      params.require(:group).permit(:name, :description, user_ids: [])
    end

    def authorize_action
      render 'errors/403', :status => 403, layout: 'application' unless current_user.can?(:admin)
    end
end
