class PsycholaboralEvaluation::StagesController < Stage::BasesController
  def show
    @psycholaboral_evaluation_report = PsycholaboralEvaluation::Report.new(psycholaboral_evaluation_stage: @stage)
  end

  private
    def catch_stages
      @stages = PsycholaboralEvaluation::Stage.joins(:enrollment).where(enrollment_bases: {course_id: @course.id}).joins(enrollment: :course).page(params[:page])
    end

    def authorize_action
      unless has_permission?
        render 'errors/403', status: 403, layout: 'application'
      end
    end

    def has_permission?(stage=nil)
      current_user.can?(:admin, @stage || stage) ||
      current_user.can?(PsycholaboralEvaluation::Stage.to_s.tableize.to_sym, @course) ||
      current_user.can?(:admin, PsycholaboralEvaluation::Stage)
    end

end
