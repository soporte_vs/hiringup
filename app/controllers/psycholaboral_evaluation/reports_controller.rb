class PsycholaboralEvaluation::ReportsController < PsycholaboralEvaluation::StagesController

  def download
    stage_document = @stage.psycholaboral_evaluation_report.documents.find(params[:stage_document_id])
    send_file stage_document.document.path, filename: stage_document.document.filename
  end

  def create
    @psycholaboral_evaluation_report = PsycholaboralEvaluation::Report.new(report_params)
    @psycholaboral_evaluation_report.documents.each{ |doc| doc.resource = @psycholaboral_evaluation_report }


    respond_to do |format|
      if @psycholaboral_evaluation_report.save
          if @psycholaboral_evaluation_report.notified_to.present?
            PsycholaboralEvaluation::ReportMailer.delay.send_report(@psycholaboral_evaluation_report)
          end
        format.html { redirect_to course_psycholaboral_evaluation_stage_path(id: @course, stage_id: @stage.id) }
        flash[:success] = I18n.t('psycholaboral_evaluation.report.created.success')
      else
        flash[:error] = @psycholaboral_evaluation_report.errors.messages
        format.html { render 'psycholaboral_evaluation/stages/show' }
      end
    end
  end

  private

    def report_params
      attrs = params.require(:psycholaboral_evaluation_report).permit(:observations, :notified_to)
      attrs[:documents] = []
      document_attrs = params[:psycholaboral_evaluation_report][:documents] || []
      document_attrs.each do |document_val|
        attrs[:documents] << Stage::Document.new(document: document_val)
      end
      attrs.merge(psycholaboral_evaluation_stage_id: @stage.id)
    end
end
