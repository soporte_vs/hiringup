class CoursePostulationsController < ApplicationController
  helper IntegraSpreadsheet::ReportHelper
  before_action :authenticate_user!
  before_action :catch_course, if: "params[:id]"
  before_action :authorize_action
  before_action :catch_postulations, except: [:_search]

  def _search
    attrs = params.except(:action, :controller, :course_postulation, :_source, :id)
    attrs[:query] = {
      filtered: {
        query: attrs[:query],
        filter: {
          terms: {
            course_id: [@course.id]
          }
        }
      }
    }
    attrs[:sort].unshift({"_score": "desc"}) if attrs[:sort].present?
    response_elastic = CoursePostulation.query(attrs)
    render json: response_elastic.response
  end

  def accept
    @postulations = @course_postulations
    @postulations.each do |postulation|
      if postulation.accept
        @course.enroll!(postulation.applicant)
      end
    end
    respond_to do |format|
      format.json do
        render json: @postulations.map{ |postulation| postulation.as_indexed_json }
      end
    end
  end

  def reject
    @postulations = @course_postulations
    @postulations.each do |postulation|
      postulation.reject
    end
    respond_to do |format|
      format.json do
         render json: @postulations.map{ |postulation| postulation.as_indexed_json }
      end
    end
  end

  def reset
    @postulations = @course_postulations
    @postulations.each do |postulation|
      postulation.reset
    end
    respond_to do |format|
      format.json do
         render json: @postulations.map{ |postulation| postulation.as_indexed_json }
      end
    end
  end

  def index
    # Se hace consulta directa a elasticsearch para evitar tener que reconstruir el json
    # persistido en elasticsearch que es usado en el frontend.
    course_postulations = CoursePostulation.query(elastic_query_params).response.hits.hits
    respond_to do |format|
      format.json{
        render json: course_postulations.map{ |postulation| postulation._source }
      }
    end
  end

  def export
    render xlsx: "export.xlsx", layout: false, filename: "Postulaciones del proceso: #{@course.id}.xlsx"
  end

  private

    def elastic_query_params
      {
        query: {
          filtered: {
            filter: {
              terms: {
                course_id: [@course.id]
              }
            }
          }
        },
        size: CoursePostulation.where(course: @course).count
      }
    end

    def catch_course
      @course = Course.find(params[:id])
    end

    def catch_postulations
      if params[:course_postulation_ids].present? && params[:course_postulation_ids].eql?('*')
        @course_postulations = CoursePostulation.where(course: @course.id)
      elsif params[:course_postulation_ids].present?
        @course_postulations = CoursePostulation.where(course: @course.id, id: params[:course_postulation_ids])
      else
        @course_postulations = CoursePostulation.where(course: @course.id)
      end
    end

    def authorize_action
      if !current_user.can?(:admin) && !current_user.can?(:admin_publications, @course)
        render 'errors/403', status: 403, layout: 'application'
      end
    end

end
