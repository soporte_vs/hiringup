class VacanciesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_vacancies
  before_action :set_course

  def assign_course
    respond_to do |format|
      format.json do
        @vacancies.each do |vacancy|
          old_course = vacancy.course
          vacancy.course = @course
          if has_permission_assing_course?(vacancy)
            unless vacancy.save
              vacancy.course = old_course
            end
          else
            vacancy.course = old_course
            vacancy.errors.add(:update,
              I18n.t('errors.messages.forbiden.f', object_name: Vacancy.model_name.human)
            )
          end
        end
      end
    end
  end

  private

    def set_vacancies
      @vacancies = Vacancy.where(id: params[:ids]) || []
    end

    def set_course
      @course = Course.find_by(id: params[:course_id])
    end

    def has_permission_assing_course?(vacancy = nil)
      vacancy ||= @vacancy
      (vacancy.hiring_request_record.present? and
       current_user.can?(:admin, HiringRequest::Record)) or
       current_user.can?(:admin)
    end
end
