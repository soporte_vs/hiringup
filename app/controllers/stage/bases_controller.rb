# coding: utf-8
class Stage::BasesController < ApplicationController
  layout 'stage'
  before_action :authenticate_user!
  before_action :catch_course
  before_action :catch_stage, if: "params[:stage_id].present?"
  before_action :enrollment_discarted?, except: [:show, :download], if: "params[:stage_id].present?"
  before_action :reject_postulant
  before_action :authorize_action, except: [:massive_finish_stage]

  def show
  end

  private

    def enrollment_discarted?
      if @stage.enrollment.discarding.present?
        flash[:alert] = I18n.t('stage.base.messages.alert.discarted.single_action')
        path = 'course_' + @stage.class.to_s.tableize.singularize.gsub('/','_') + '_path'
        redirect_to send(path)
      end
    end

    def catch_course
      begin
        @course = Course.find(params[:id])
      rescue Exception => e
        render 'errors/404', status: 404, layout: 'application'
      end
    end

    def catch_stage
      begin
        @stage = Stage::Base.find(params[:stage_id])
        raise "" if @stage.enrollment.course.id != @course.id
      rescue Exception => e
        render 'errors/404', status: 404, layout: 'application'
      end
    end

    def authorize_action
      if !(current_user.can?(params[:action], @stage) ||
           current_user.can?(params[:controller].to_sym, @course))
        render 'errors/403', status: 403, layout: 'application'
      end
    end

    def reject_postulant
      if @course.applicants.include?(current_user.applicant)
        render 'errors/403', status: 403, layout: 'application'
      end
    end

end
