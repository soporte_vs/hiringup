class Stage::NotApplyReasonsController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize_action
  before_action :set_stage_not_apply_reason, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    respond_to do |format|
      format.html { @stage_not_apply_reasons = Stage::NotApplyReason.page(params[:page]) }
      format.json { @stage_not_apply_reasons = Stage::NotApplyReason.all }
    end
    respond_with(@enrollment_stage_not_apply_reasons)
  end

  def show
    respond_with(@stage_not_apply_reason)
  end

  def new
    @stage_not_apply_reason = Stage::NotApplyReason.new
    respond_with(@stage_not_apply_reason)
  end

  def edit

  end

  def create
    @stage_not_apply_reason = Stage::NotApplyReason.new(stage_not_apply_reason_params)
    if @stage_not_apply_reason.save
      flash[:success] = t("resources.discard_reasons.create_success")
    else
      flash[:error] = @stage_not_apply_reason.errors.messages
    end
    respond_with(@stage_not_apply_reason)
  end

  def update
    if @stage_not_apply_reason.update(stage_not_apply_reason_params)
      flash[:success] = t("resources.discard_reasons.update_success")
    else
      flash[:error] = @stage_not_apply_reason.errors.messages
    end
    respond_with(@stage_not_apply_reason)
  end

  def destroy
    if @stage_not_apply_reason.destroy
      flash[:success] = t("resources.discard_reasons.destroy_success")
    else
      flash[:error] = @stage_not_apply_reason.errors.messages
    end
    redirect_to :back
  end

  private
    def authorize_action
      render 'errors/403', status: 403, layout: 'application' unless current_user.can?(params[:action].to_sym, Stage::NotApplyReason)
    end

    def set_stage_not_apply_reason
      @stage_not_apply_reason = Stage::NotApplyReason.find(params[:id])
    end

    def stage_not_apply_reason_params
      params.require(:stage_not_apply_reason).permit(:name, :description)
    end
end
