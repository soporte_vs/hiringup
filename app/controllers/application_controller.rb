# coding: utf-8
class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  # protect_from_forgery with: :exception
  before_action :prepare_exception_notifier
  before_action :set_paper_trail_whodunnit
  # before_action :set_raven_context

  def after_sign_in_path_for(resource)
    location = stored_location_for(resource)
    if location != nil
      location
    elsif current_user.is_active == false || current_user.is_active == nil
      flash[:error] =I18n.t("devise.sessions.blocked_acount")
      session.clear
      root_path
    elsif current_user.has_any_role?({ name: :admin, resource: Course }, { name: :search, resource: Course })
      flash[:notice] =I18n.t("devise.sessions.signed_in")
      search_courses_path
    elsif current_user.has_any_role?({ name: :admin, resource: Course }, { name: :search, resource: Applicant::Base })
      flash[:notice] =I18n.t("devise.sessions.signed_in")
      search_applicant_bases_path
    else
      flash[:notice] =I18n.t("devise.sessions.signed_in")
      edit_profiles_path
    end
  end

  rescue_from 'ActiveRecord::InvalidForeignKey', 'ActiveRecord::RecordNotDestroyed' do |record|
    flash[:error] = "No se logró terminar la operación. Se trató de eliminar un registro que está relacionado a otros datos"
    redirect_to :back
  end

  private
    def prepare_exception_notifier
      request.env["exception_notifier.exception_data"] = {
        :current_user => current_user
      }
    end

    # def set_raven_context
    #   Raven.user_context(id: session[:current_user_id]) # or anything else in session
    #   Raven.extra_context(params: params.to_unsafe_h, url: request.url)
    # end
end
