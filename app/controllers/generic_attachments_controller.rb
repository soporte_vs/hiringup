class GenericAttachmentsController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize_action
  before_action :set_generic_attachment, only: [:destroy, :download]

  # DELETE /generic_attachments/1
  # DELETE /generic_attachments/1.json
  def destroy
    @generic_attachment.destroy
    respond_to do |format|
      flash[:success] = t("resources.generic_attachment.descriptor.destroy_success")
      format.html { redirect_to @generic_attachment.resourceable}
      format.json { head :no_content }
    end
  end

  def download
    send_file(@generic_attachment.attachment.path,
              :type => @generic_attachment.attachment.content_type,
              :disposition => 'attachment',
    )
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_generic_attachment
      @generic_attachment = GenericAttachment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def generic_attachment_params
      params.require(:generic_attachment).permit(:attachment, :resourceable_id, :resourceable_type)
    end
    def authorize_action
      render 'errors/403', status: 403, layout: 'application' unless current_user.can?(:admin)
    end
end
