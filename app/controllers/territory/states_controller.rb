class Territory::StatesController < ApplicationController
  before_action :authenticate_user!, except: [:show, :index]
  before_action :authorize_action, except: [:show, :index]
  before_action :set_territory_state, only: [:show, :edit, :update, :destroy]

  # GET /territory/states
  # GET /territory/states.json
  def index
    respond_to do |format|
      format.html { @territory_states = Territory::State.page(params[:page]) }
      format.json { @territory_states = Territory::State.all }
    end
  end

  # GET /territory/states/1
  # GET /territory/states/1.json
  def show
  end

  # GET /territory/states/new
  def new
    @territory_state = Territory::State.new
  end

  # GET /territory/states/1/edit
  def edit
  end

  # POST /territory/states
  # POST /territory/states.json
  def create
    @territory_state = Territory::State.new(territory_state_params)

    respond_to do |format|
      if @territory_state.save
        flash[:success] = t("resources.territory.states.create_success")
        format.html { redirect_to @territory_state }
        format.json { render :show, status: :created, location: @territory_state }
      else
        flash.now[:error] = @territory_state.errors.messages
        format.html { render :new }
        format.json { render json: @territory_state.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /territory/states/1
  # PATCH/PUT /territory/states/1.json
  def update
    respond_to do |format|
      if @territory_state.update(territory_state_params)
        flash[:success] = t("resources.territory.states.update_success")
        format.html { redirect_to @territory_state }
        format.json { render :show, status: :ok, location: @territory_state }
      else
        flash.now[:error] = @territory_state.errors.messages
        format.html { render :edit }
        format.json { render json: @territory_state.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /territory/states/1
  # DELETE /territory/states/1.json
  def destroy
    @territory_state.destroy
    respond_to do |format|
      flash[:success] = t("resources.territory.states.destroy_success")
      format.html { redirect_to territory_states_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_territory_state
      @territory_state = Territory::State.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def territory_state_params
      begin
        territory_country = Territory::Country.find(params[:territory_state][:territory_country])
      rescue Exception => e
        territory_country = nil
      end
      params.require(:territory_state).permit(:name).merge(territory_country: territory_country)
    end

    def authorize_action
      render 'errors/403', :status => 403, layout: 'application' unless current_user.can?(:admin)
    end
end
