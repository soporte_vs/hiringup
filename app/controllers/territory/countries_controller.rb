class Territory::CountriesController < ApplicationController
  before_action :authenticate_user!, except: [:show, :index]
  before_action :authorize_action, except: [:show, :index]
  before_action :set_territory_country, only: [:show, :edit, :update, :destroy]

  # GET /territory/countries
  # GET /territory/countries.json
  def index
    respond_to do |format|
      format.html { @territory_countries = Territory::Country.page(params[:page]) }
      format.json { @territory_countries = Territory::Country.all }
    end
  end

  # GET /territory/countries/1
  # GET /territory/countries/1.json
  def show
  end

  # GET /territory/countries/new
  def new
    @territory_country = Territory::Country.new
  end

  # GET /territory/countries/1/edit
  def edit
  end

  # POST /territory/countries
  # POST /territory/countries.json
  def create
    @territory_country = Territory::Country.new(territory_country_params)

    respond_to do |format|
      if @territory_country.save
        flash[:success] = t("resources.territory.countries.create_success")
        format.html { redirect_to @territory_country }
        format.json { render :show, status: :created, location: @territory_country }
      else
        flash.now[:error] = @territory_country.errors.messages
        format.html { render :new }
        format.json { render json: @territory_country.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /territory/countries/1
  # PATCH/PUT /territory/countries/1.json
  def update
    respond_to do |format|
      if @territory_country.update(territory_country_params)
        flash[:success] = t("resources.territory.countries.update_success")
        format.html { redirect_to @territory_country }
        format.json { render :show, status: :ok, location: @territory_country }
      else
        flash.now[:error] = @territory_country.errors.messages
        format.html { render :edit }
        format.json { render json: @territory_country.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /territory/countries/1
  # DELETE /territory/countries/1.json
  def destroy
    @territory_country.destroy
    respond_to do |format|
      flash[:success] = t("resources.territory.countries.destroy_success")
      format.html { redirect_to territory_countries_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_territory_country
      @territory_country = Territory::Country.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def territory_country_params
      params.require(:territory_country).permit(:name)
    end

    def authorize_action
      render 'errors/403', :status => 403, layout: 'application' unless current_user.can?(:admin)
    end
end
