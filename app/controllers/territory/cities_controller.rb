class Territory::CitiesController < ApplicationController
  before_action :authenticate_user!, except: [:show, :index]
  before_action :authorize_action, except: [:show, :index]
  before_action :set_territory_city, only: [:show, :edit, :update, :destroy]

  # GET /territory/cities
  # GET /territory/cities.json
  def index
    respond_to do |format|
      format.html { @territory_cities = Territory::City.page(params[:page]) }
      format.json { @territory_cities = Territory::City.all }
    end
  end

  # GET /territory/cities/1
  # GET /territory/cities/1.json
  def show
  end

  # GET /territory/cities/new
  def new
    @territory_city = Territory::City.new
  end

  # GET /territory/cities/1/edit
  def edit
  end

  # POST /territory/cities
  # POST /territory/cities.json
  def create
    @territory_city = Territory::City.new(territory_city_params)

    respond_to do |format|
      if @territory_city.save
        flash[:success] = t("resources.territory.cities.create_success")
        format.html { redirect_to @territory_city }
        format.json { render :show, status: :created, location: @territory_city }
      else
        flash.now[:error] = @territory_city.errors.messages
        format.html { render :new }
        format.json { render json: @territory_city.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /territory/cities/1
  # PATCH/PUT /territory/cities/1.json
  def update
    respond_to do |format|
      if @territory_city.update(territory_city_params)
        flash[:success] = t("resources.territory.cities.update_success")
        format.html { redirect_to @territory_city }
        format.json { render :show, status: :ok, location: @territory_city }
      else
        flash.now[:error] = @territory_city.errors.messages
        format.html { render :edit }
        format.json { render json: @territory_city.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /territory/cities/1
  # DELETE /territory/cities/1.json
  def destroy
    @territory_city.destroy
    respond_to do |format|
      flash[:success] = t("resources.territory.cities.destroy_success")
      format.html { redirect_to territory_cities_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_territory_city
      @territory_city = Territory::City.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def territory_city_params
      begin
        territory_state = Territory::State.find(params[:territory_city][:territory_state])
      rescue Exception => e
        territory_state = nil
      end
      params.require(:territory_city).permit(:name).merge(territory_state: territory_state)
    end

    def authorize_action
      render 'errors/403', :status => 403, layout: 'application' unless current_user.can?(:admin)
    end
end
