class GroupInterview::StagesController < Stage::BasesController
  def show
    @group_interview_report = GroupInterview::Report.new(group_interview_stage: @stage)
  end

  private
    def catch_stages
      @stages = GroupInterview::Stage.joins(:enrollment).where(enrollment_bases: {course_id: @course.id}).joins(enrollment: :course).page(params[:page])
    end

    def authorize_action
      unless has_permission?
        render 'errors/403', status: 403, layout: 'application'
      end
    end

    def has_permission?(stage=nil)
      current_user.can?(:admin, stage || @stage) ||
      current_user.can?(GroupInterview::Stage.to_s.tableize.to_sym, @course) ||
      current_user.can?(:admin, GroupInterview::Stage)
    end
end
