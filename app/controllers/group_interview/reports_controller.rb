class GroupInterview::ReportsController < GroupInterview::StagesController
  def download
    stage_document = @stage.group_interview_report.documents.find(params[:stage_document_id])
    send_file stage_document.document.path, filename: stage_document.document.filename
  end

  def create
    @group_interview_report = GroupInterview::Report.new(report_params)
    @group_interview_report.documents.each{ |doc| doc.resource = @group_interview_report }

    respond_to do |format|
      if @group_interview_report.save
        if @group_interview_report.notified_to.present?
          GroupInterview::ReportMailer.delay.send_report(@group_interview_report)
        end
        flash[:success] = I18n.t('group_interview.report.created.success')
        format.html { redirect_to course_group_interview_stage_path(id: @course, stage_id: @stage.id) }
      else
        flash[:error] = @group_interview_report.errors.messages
        format.html { render 'group_interview/stages/show' }
      end
    end
  end

  private

    def report_params
      attrs = params.require(:group_interview_report).permit(:observations, :notified_to)
      attrs[:documents] = []
      document_attrs = params[:group_interview_report][:documents] || []
      document_attrs.each do |document_val|
        attrs[:documents] << Stage::Document.new(document: document_val)
      end
      attrs.merge(group_interview_stage_id: @stage.id)
    end
end
