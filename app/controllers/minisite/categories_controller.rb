class Minisite::CategoriesController < ApplicationController
  before_action :authenticate_user!
  before_action  :authorize_action
  before_action  :set_minisite_category, only: [:show, :edit, :update, :destroy]

  def index
    respond_to do |format|
      format.html { @minisite_categories = Minisite::Category.all.page(params[:page]) }
      format.json { @minisite_categories = Minisite::Category.all }
    end
  end

  def show
  end

  def new
    @minisite_category = Minisite::Category.new
  end

  def edit
  end

  def create
    @minisite_category = Minisite::Category.new(minisite_category_params)

    respond_to do |format|
      if  @minisite_category.save
        flash[:success] = t("resources.minisite.categories.create_success")
        format.html { redirect_to @minisite_category }
        format.json { render :show, status: :created, location: @minisite_category}
      else
        flash.now[:error] = @minisite_category.errors.messages
        format.html { render :new }
        format.json { render json: @minisite_category.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @minisite_category.update(minisite_category_params)
        flash[:success] = t("resources.minisite.categories.update_success")
        format.html { redirect_to @minisite_category }
        format.json { render :show, status: :ok, location: @minisite_category }
      else
        flash.now[:error] = @minisite_category.errors.messages
        format.html { render :edit }
        format.json { render json: @minisite_category.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @minisite_category.destroy
    respond_to do |format|
      flash[:success] = I18n.t("resources.minisite.categories.destroy_success")
      format.html { redirect_to minisite_categories_url }
      format.json { head :no_content }
    end
  end

  private
    def set_minisite_category
        @minisite_category = Minisite::Category.find(params[:id])
    end

    def minisite_category_params
      params.require(:minisite_category).permit(:name, :description, :image)
    end

    def authorize_action
        render 'errors/403', status: 403, layout: 'application' unless current_user.can?(:admin)
    end

end
