class Minisite::SiteController < ApplicationController
  before_action :set_category, only: [:search, :category]
  layout 'site'

  def search
    @minisite_publications = Minisite::Publication.joins(course: :territory_city)
        .opened
        .where(course_id: @category.courses.map(&:id))
        .where(territory_filters)
        .where(query_search)
        .order(created_at: :desc)
        .page(params[:page])

    render :category
  end

  def category
    @minisite_publications = Minisite::Publication
        .opened
        .where(course_id: @category.courses.map(&:id))
        .order(created_at: :desc)
        .page(params[:page])
    @titulo = ''
    if @category.name == 'Promociones y Traslados'
      @titulo = 'Postulaciones por Dirección Regional'
    else
      @titulo = 'Últimas Ofertas'
    end
  end

  def index
    @minisite_publications = Minisite::Publication.opened.order(created_at: :desc).page(params[:page])
    @minisite_banner = Minisite::Banner.find_by(active_image: true)
  end

  def resources
  end

  private

    def set_category
      @category = Tag::CourseTag.find(params[:id])
    end

    def query_search
      if params[:query].present?
        params[:query].split(' ').map { |p|
          "translate(LOWER(minisite_publications.title), 'áéíóúñ', 'aeioun') LIKE LOWER('%#{I18n.transliterate(p)}%') OR
           translate(LOWER(minisite_publications.description), 'áéíóúñ', 'aeioun') LIKE LOWER('%#{I18n.transliterate(p)}%')"
        }.join(' OR ')
      else
        ''
      end
    end

    def territory_filters
      if params[:territory_city_id].present?
        return { courses: { territory_city_id: params[:territory_city_id] } }
      elsif params[:territory_state_id].present?
        return { territory_cities: { territory_state_id: params[:territory_state_id] } }
      else
        return {}
      end
    end
end
