class Minisite::BannersController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize_action
  before_action :set_minisite_banner, only: [:show, :edit, :update, :destroy]

  # GET /minisite/banners
  # GET /minisite/banners.json
  def index
    @minisite_banners = Minisite::Banner.all.order(created_at: :asc).page(params[:page])
  end

  # GET /minisite/banners/1
  # GET /minisite/banners/1.json
  def show
  end

  # GET /minisite/banners/new
  def new
    @minisite_banner = Minisite::Banner.new
  end

  # GET /minisite/banners/1/edit
  def edit
  end

  # POST /minisite/banners
  # POST /minisite/banners.json
  def create
    @minisite_banner = Minisite::Banner.new(minisite_banner_params)

    respond_to do |format|
      if @minisite_banner.save
        flash[:success] = I18n.t("resources.minisite/banner.create_success")
        format.html { redirect_to @minisite_banner }
        format.json { render :show, status: :created, location: @minisite_banner }
      else
        format.html { render :new }
        format.json { render json: @minisite_banner.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /minisite/banners/1
  # PATCH/PUT /minisite/banners/1.json
  def update
    respond_to do |format|
      if @minisite_banner.update(minisite_banner_params)
        flash[:success] = I18n.t("resources.minisite/banner.update_success")
        format.html { redirect_to @minisite_banner }
        format.json { render :show, status: :ok, location: @minisite_banner }
      else
        format.html { render :edit }
        format.json { render json: @minisite_banner.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /minisite/banners/1
  # DELETE /minisite/banners/1.json
  def destroy
    @minisite_banner.destroy
    respond_to do |format|
      flash[:success] = I18n.t("resources.minisite/banner.destroy_success")
      format.html { redirect_to minisite_banners_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_minisite_banner
      @minisite_banner = Minisite::Banner.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def minisite_banner_params
      params.require(:minisite_banner).permit(:title, :image, :active_image)
    end

    def authorize_action
      render 'errors/403', status: 403, layout: 'application' unless current_user.can?(:admin)
    end
end
