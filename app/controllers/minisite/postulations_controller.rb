# coding: utf-8
class Minisite::PostulationsController < ApplicationController
  before_action :authenticate_user!
  before_action :check_publication_open!, only: :create
  before_action :set_postulation, except: :create
  before_action :authorize_action, except: :create

  def create
    applicant = Applicant::Base.find_by(user_id: current_user.id)
    if !applicant.present?
      applicant = Minisite::Applicant.create(user: current_user)
    end

    @minisite_postulation = Minisite::Postulation.new(
      postulation_params.merge(
        applicant_id: current_user.applicant.id,
        minisite_publication_id: params[:id]
      )
    )
    @minisite_publications = Minisite::Publication.opened.page(params[:page]).where.not(id: params[:id])
    if !applicant.postulation_data_completed?
      flash[:error] = "Debes completar la información personal requerida en tu perfil antes de postular"
      render template: 'minisite/publications/job'
    elsif @minisite_postulation.save
      flash[:success] = "Muchas gracias por postular a #{$COMPANY[:name]}"
      redirect_to job_path(@minisite_publication.seo_path)
    else
      render template: 'minisite/publications/job'
    end
  end

  private
    def check_publication_open!
      @minisite_publication = Minisite::Publication.find(params[:id])
      unless @minisite_publication.opened?
        flash[:error] = t('minisite/postulation.actions.create.error.publication_not_open')
        redirect_to :back unless @minisite_publication.opened?
      end
    end

    def set_postulation
      @postulation = Minisite::Postulation.find_by(id: params[:id])
      if !@postulation
        render 'errors/404', status: 404, layout: 'application'
      else
        @publication = @postulation.minisite_publication
        @course = @publication.course
      end
    end

    def authorize_action
      if !current_user.can?(:admin_publications, @postulation.minisite_publication.course)
        render 'errors/403', status: 403, layout: 'application'
      end
    end

    def postulation_params
      if params[:minisite_postulation].present?
        params.require(:minisite_postulation).permit(
          answers_attributes: [
            :minisite_question_id,
            :description
          ]
        )
      else
        {}
      end
    end
end
