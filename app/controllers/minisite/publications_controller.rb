class Minisite::PublicationsController < ApplicationController
  layout 'course', except: :job
  before_action :authenticate_user!, except: :job
  before_action :set_course, except: [:job, :postulate]
  before_action :authorize_action, except: [:job, :postulate]
  before_action :set_minisite_publication, only: [:show, :edit, :update, :destroy, :update_state]

  def job
    @minisite_publication = Minisite::Publication.find(get_id_of_path)
    @minisite_postulation = Minisite::Postulation.new(minisite_publication: @minisite_publication)
    @minisite_publication.questions.each do |question|
      @minisite_postulation.answers.new(minisite_question: question)
    end
    @minisite_publications = Minisite::Publication.opened.page(params[:page]).where.not(id: params[:id])
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: @minisite_publication.pdf_name
      end
    end
  end

  # GET /app/course/:id/minisite/publications
  # GET /app/course/:id/minisite/publications.json
  def index
    @minisite_publications = @course.minisite_publications.order(created_at: :desc).page(params[:page])
  end

  # GET /app/course/:id/minisite/publications/:publication_id
  # GET /app/course/:id/minisite/publications/:publication_id.json
  def show
  end

  # GET /app/course/:id/minisite/publications/new

  def new
    @minisite_publication = @course.minisite_publications.new(
      title: @course.title,
      description: @course.description,
      company_contract_type: @course.contract_type
    )

    laborum_publication = @course.laborum_publications.select("question1, question2, question3, question4, question5").last
    trabajando_publication = @course.trabajando_publications.select("q1, q2, q3, q4, q5").last

    laborum_publication.present? ? last_publication = laborum_publication : last_publication = trabajando_publication

    if last_publication
      last_publication.attributes.each do |attr_name, attr_value|
        @minisite_publication.questions.new(description: attr_value) if attr_value.present?
      end
    end

  end


  # GET /app/course/:id/minisite/publications/:publication_id/edit
  def edit
  end

  # POST /app/course/:id/minisite/publications
  # POST /app/course/:id/minisite/publications.json
  def create
    @minisite_publication = @course.minisite_publications.new(minisite_publication_params)
    respond_to do |format|
      if @minisite_publication.save
        flash[:success] = t("publications.create_success")
        #format.html { redirect_to minisite_publications_path(@course) }
        format.html { redirect_to minisite_publication_path(:publication_id => @minisite_publication) }
        format.json { render :show, status: :created, id: @course.id, publication_id: @minisite_publication.id }
      else
        format.html { render :new }
        format.json { render json: @minisite_publication.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /app/course/:id/minisite/publications/:publication_id
  # PATCH/PUT /app/course/:id/minisite/publications/:publication_id.json
  def update
    respond_to do |format|
      if @minisite_publication.update(minisite_publication_params)
        flash[:success] = t("publications.update_success")
        #format.html { redirect_to minisite_publications_path(@course) }
        format.html { redirect_to minisite_publication_path(:publication_id => @minisite_publication) }
        format.json { render :show, status: :ok, location: @minisite_publication }
      else
        flash.now[:error] = @minisite_publication.errors.messages
        format.html { render :edit }
        format.json { render json: @minisite_publication.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /app/course/:id/minisite/publications/:publication_id
  # DELETE /app/course/:id/minisite/publications/:publication_id.json
  def destroy
    @minisite_publication.destroy
    respond_to do |format|
      flash[:success] = t("publications.destroy_success")
      format.html { redirect_to course_path(@course.id, anchor: 'publications') }
      format.json { head :no_content }
    end
  end

  def update_state
    respond_to do |format|
      begin
        if @minisite_publication.send("#{params[:switch]}!")
          format.html do
            flash[:success] = t("minisite/publication.actions.#{params[:switch]}.success")
            redirect_to minisite_publication_path(publication_id: @minisite_publication)
          end
          format.json { render json: @minisite_publication }
        else
          format.html do
            flash[:error] = t("minisite/publication.actions.#{params[:switch]}.unsuccess")
            redirect_to minisite_publication_path(publication_id: @minisite_publication)
          end
          format.json { render json: @minisite_publication.errors }
        end
      rescue AASM::InvalidTransition => e
        action = t("minisite/publication.actions.#{params[:switch]}.text").downcase
        state = t("minisite/publication.state.#{@minisite_publication.aasm_state}.default")
        error = t("minisite/publication.errors.transition", action: action, state: state)
        format.html do
          flash[:error] = error
          redirect_to minisite_publication_path(publication_id: @minisite_publication)
        end
        format.json { render json: error }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_minisite_publication
      @minisite_publication = @course.minisite_publications.find(params[:publication_id])
      render 'errors/404', status: 404, layout: 'application' unless @minisite_publication
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_course
      @course = Course.find_by(id: params[:id])
      render 'errors/404', status: 404, layout: 'application' unless @course
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def minisite_publication_params
      params.require(:minisite_publication).permit(
        :title,
        :description,
        :desactivate_at,
        :company_contract_type_id,
        minisite_category_ids: [],
        questions_attributes: [
          :id,
          :description,
          :_destroy
        ]

      )
    end

    def authorize_action
      if !current_user.can?(:admin_publications, @course)
        render 'errors/403', status: 403, layout: 'application'
      end
    end

    def get_id_of_path
      params[:id].split('-').try(:first)
    end
end
