class Promotion::StagesController < Stage::BasesController

  def send_acceptance_email
    @stage.send_acceptance_email
    flash[:success] = t('promotion/stage.actions.send_acceptance_email.success')
    redirect_to course_promotion_stage_path(id: @course.id, stage_id: @stage.id)
  end

  private
    def catch_stages
      @stages = Promotion::Stage.joins(:enrollment).where(enrollment_bases: {course_id: @course.id}, step: params[:step]).joins(enrollment: :course).page(params[:page])
    end

    def authorize_action
      unless has_permission?
        render 'errors/403', status: 403, layout: 'application'
      end
    end

    def has_permission?(stage=nil)
      current_user.can?(:admin, @stage || stage) ||
      current_user.can?(Promotion::Stage.to_s.tableize.to_sym, @course) ||
      current_user.can?(:admin, Promotion::Stage)
    end
end

