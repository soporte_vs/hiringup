class Engagement::AgreementsController < Engagement::StagesController
  before_action :catch_agreement,  if: "params[:agreement_id].present?"

  respond_to :jasperpdf

  def download
    stage_document = @agreement.documents.find(params[:stage_document_id])
    send_file stage_document.document.path, filename: stage_document.document.filename
  end

  def preview_agreement
    @engagement_agreement = Engagement::Agreement.new(agreement_params)

    if @engagement_agreement.valid?
      @COMPANY = @engagement_agreement.vacancy.company_positions_cenco.company_cenco.company_business_unit.name
      @CONTRACT_TYPE = @engagement_agreement.company_contract_type_name
      @SALARY = view_context.number_to_currency(@engagement_agreement.revenue)
      @OBSERVATIONS = @engagement_agreement.observations

      respond_with :course, @engagement_agreement, template: 'engagement/agreements/show'
    else
      @company_contract_types = Company::ContractType.all
      flash.now[:error] = @engagement_agreement.errors.messages

      render template: 'engagement/stages/show', :formats => [:html]
    end
  end

  def show
    @COMPANY = @agreement.vacancy.company_positions_cenco.company_cenco.company_business_unit.name
    @CONTRACT_TYPE = @agreement.company_contract_type_name
    @SALARY = view_context.number_to_currency(@agreement.revenue)
    @OBSERVATIONS = @agreement.observations

    respond_with @engagement_agreement
  end

  def new
    @engagement_agreement = Engagement::Agreement.new(engagement_stage_id: @stage.id)
    @company_contract_types = Company::ContractType.all
  end

  def create
    @engagement_agreement = Engagement::Agreement.new(agreement_params)
    @engagement_agreement.documents.each{ |doc| doc.resource = @engagement_agreement }

    respond_to do |format|
      if @stage.enrollment.discarding.nil? && @engagement_agreement.save
        Engagement::AgreementMailer.delay.hire(@engagement_agreement)
        flash[:success] = 'Ficha de ingreso creada exitosamente'
        @course.create_activity key: 'course.engagement_stage.hired', owner: current_user, recipient: @engagement_agreement.engagement_stage.enrollment
        format.html { redirect_to course_engagement_stage_path(id: @course.id, stage_id: @stage.id) }
      else
        @company_contract_types = Company::ContractType.all
        flash.now[:error] = @engagement_agreement.errors.messages
        format.html { render template: 'engagement/stages/show' }
      end
    end
  end

  private

    def authorize_action
      if !(current_user.can?(:admin, @stage) ||
           current_user.can?(Engagement::Stage.to_s.tableize.to_sym, @course))
        render 'errors/403', status: 403, layout: 'application'
      end
    end

    def catch_agreement
      begin
        @agreement = Engagement::Agreement.find(params[:agreement_id])
      rescue Exception => e
        render 'errors/404', status: 404, layout: 'application'
      end
    end

    def agreement_params
      attrs = params.require(:engagement_agreement).permit(
        :revenue,
        :observations,
        :vacancy_id,
        :company_contract_type_id
      )
      attrs[:documents] = []
      document_attrs = params[:engagement_agreement][:documents] || []
      document_attrs.each do |document_val|
        attrs[:documents] << Stage::Document.new(document: document_val)
      end
      attrs.merge(engagement_stage_id: @stage.id)
    end
end
