class Engagement::StagesController < Stage::BasesController
  layout 'stage'

  def show
    @engagement_agreement = Engagement::Agreement.find_by(engagement_stage: @stage) || Engagement::Agreement.new(engagement_stage: @stage)
    @company_contract_types = Company::ContractType.all
  end

  private

   def catch_stage
    begin
      @stage = Engagement::Stage.find(params[:stage_id])
      raise "" if @stage.enrollment.course.id != @course.id
    rescue Exception => e
      render 'errors/404', status: 404, layout: 'application'
    end
  end

    def authorize_action
      if !(current_user.can?(params[:action], @stage) ||
           current_user.can?(params[:controller].to_sym, @course) ||
           current_user.can?(:admin, Engagement::Stage))
        render 'errors/403', status: 403, layout: 'application'
      end
    end
end
