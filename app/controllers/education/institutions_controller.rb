# coding: utf-8
class Education::InstitutionsController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize_action, except: [:index]
  before_action :set_education_institution, only: [:show, :edit, :update, :destroy, :update_custom, :approve_custom]

  # GET /education/institutions
  # GET /education/institutions.json
  def index
    if params[:custom].present? && params[:custom] == 'true'
      @education_institutions = Education::Institution.where(custom: true)
    elsif params[:custom].present? && params[:custom] == 'all'
      @education_institutions = Education::Institution.all
    else
      @education_institutions = Education::Institution.where.not(custom: true)
    end

    if params[:country_id].present? && Territory::Country.find(params[:country_id]).present?
      @education_institutions = @education_institutions.where(country_id: params[:country_id])
    end

    if params[:type].present?
      @education_institutions = @education_institutions.where(type: params[:type])
    end

    # se usa el becomes porque se necesitan con la clase original
    # porque si no me redirige al controlador de la clase que tiene type.
    # Por ejemplo si es un Education::University, cuando use las institutions en la vista,
    # va a querer ir al controlador de University y no al de Institutions
    @education_institutions = @education_institutions.map do |institution|
      institution.becomes(Education::Institution)
    end

    respond_to do |format|
      format.html {
        # Se usa Kaminari porque el map devuelve un Array, y page actua sobre ActiveRecord::Relation,
        # o sobre un Kaminari::PaginatableArray
        @education_institutions = Kaminari.paginate_array(@education_institutions).page(params[:page])
      }
      format.json {}
    end
  end

  # GET /education/institutions/1
  # GET /education/institutions/1.json
  def show
  end

  # GET /education/institutions/new
  def new
    @education_institution = Education::Institution.new
  end

  # GET /education/institutions/1/edit
  def edit
  end

  # POST /education/institutions
  # POST /education/institutions.json
  def create
    @education_institution = Education::Institution.new(education_institution_params)

    respond_to do |format|
      if @education_institution.save
        @education_institution = @education_institution.becomes(Education::Institution)
        flash[:success] = t("resources.education.institutions.create_success")
        format.html { redirect_to @education_institution }
        format.json { render :show, status: :created, location: @education_institution }
      else
        flash.now[:error] = @education_institution.errors.messages
        format.html { render :new }
        format.json { render json: @education_institution.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /education/institutions/1
  # PATCH/PUT /education/institutions/1.json
  def update
    respond_to do |format|
      if @education_institution.update(education_institution_params)
        flash[:success] = t("resources.education.institutions.update_success")
        format.html { redirect_to @education_institution }
        format.json { render :show, status: :ok, location: @education_institution }
      else
        flash.now[:error] = @education_institution.errors.messages
        format.html { render :edit }
        format.json { render json: @education_institution.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /education/institutions/1
  # DELETE /education/institutions/1.json
  def destroy
    @education_institution.destroy
    respond_to do |format|
      flash[:success] = t("resources.education.institutions.destroy_success")
      format.html { redirect_to education_institutions_url }
      format.json { head :no_content }
    end
  end

  def custom
    @education_institutions = Education::Institution.where(custom: true)

    # se usa el becomes porque se necesitan con la clase original
    @education_institutions = @education_institutions.map do |institution|
      institution.becomes(Education::Institution)
    end

    # Se usa Kaminari porque el map devuelve un Array, y page actua sobre ActiveRecord::Relation,
    # o sobre un Kaminari::PaginatableArray
    @education_institutions = Kaminari.paginate_array(@education_institutions).page(params[:page])
  end

  def update_custom
    degrees_to_update = People::Degree.where(institution: @education_institution)
    new_institution = Education::Institution.find_by(id: params[:new_id])

    if new_institution.present?
      if degrees_to_update.update_all(institution_id: new_institution.id) && @education_institution.destroy
        flash[:success] = t("resources.education.institutions.update_success")
        redirect_to custom_education_institutions_path
      else
        flash.now[:error] = "no se pudo actualizar los degrees"
        render :custom
      end
    else
      flash.now[:error] = "no existe la nueva institución"
      render :custom
    end
  end

  def approve_custom
    if @education_institution.update(custom: false)
      flash[:success] = t("resources.education.institutions.update_success")
      redirect_to custom_education_institutions_path
    else
      flash.now[:error] = @education_institution.errors.messages
      render :custom
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_education_institution
      @education_institution = Education::Institution.find_by(id: params[:id])
      if @education_institution.present?
        @education_institution = @education_institution.becomes(Education::Institution)
      else
        render 'errors/404', status: 404, layout: 'application'
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def education_institution_params
      params.require(:education_institution).permit(
        :name,
        :type,
        :country_id
      )
    end

    def authorize_action
      render 'errors/403', status: 403, layout: 'application' unless current_user.can?(:admin)
    end
end
