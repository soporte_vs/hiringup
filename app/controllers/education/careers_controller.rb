class Education::CareersController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize_action
  before_action :set_education_career, only: [:show, :edit, :update, :destroy]

  # GET /education/careers
  # GET /education/careers.json
  def index
    respond_to do |format|
      format.html { @education_careers = Education::Career.page(params[:page]) }
      format.json { @education_careers = Education::Career.all }
    end
  end

  # GET /education/careers/1
  # GET /education/careers/1.json
  def show
  end

  # GET /education/careers/new
  def new
    @education_career = Education::Career.new
  end

  # GET /education/careers/1/edit
  def edit
  end

  # POST /education/careers
  # POST /education/careers.json
  def create
    @education_career = Education::Career.new(education_career_params)

    respond_to do |format|
      if @education_career.save
        flash[:success] = t("resources.education.careers.create_success")
        format.html { redirect_to @education_career }
        format.json { render :show, status: :created, location: @education_career }
      else
        flash.now[:error] = @education_career.errors.messages
        format.html { render :new }
        format.json { render json: @education_career.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /education/careers/1
  # PATCH/PUT /education/careers/1.json
  def update
    respond_to do |format|
      if @education_career.update(education_career_params)
        flash[:success] = t("resources.education.careers.update_success")
        format.html { redirect_to @education_career }
        format.json { render :show, status: :ok, location: @education_career }
      else
        flash.now[:error] = @education_career.errors.messages
        format.html { render :edit }
        format.json { render json: @education_career.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /education/careers/1
  # DELETE /education/careers/1.json
  def destroy
    @education_career.destroy
    respond_to do |format|
      flash[:success] = t("resources.education.careers.destroy_success")
      format.html { redirect_to education_careers_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_education_career
      @education_career = Education::Career.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def education_career_params
      params.require(:education_career).permit(:name)
    end

    def authorize_action
      render 'errors/403', status: 403, layout: 'application' unless current_user.can?(:admin)
    end
end
