class Education::StudyLevelsController < ApplicationController
  before_action :set_education_study_level, only: [:show, :edit, :update, :destroy]

  # GET /education/study_levels
  # GET /education/study_levels.json
  def index
    respond_to do |format|
      format.html { @education_study_levels = Education::StudyLevel.page(params[:page]) }
      format.json { @education_study_levels = Education::StudyLevel.all }
    end
  end

  # GET /education/study_levels/1
  # GET /education/study_levels/1.json
  def show
  end

  # GET /education/study_levels/new
  def new
    @education_study_level = Education::StudyLevel.new
  end

  # GET /education/study_levels/1/edit
  def edit
  end

  # POST /education/study_levels
  # POST /education/study_levels.json
  def create
    @education_study_level = Education::StudyLevel.new(education_study_level_params)

    respond_to do |format|
      if @education_study_level.save
        flash[:success] = t("resources.education.study_levels.create_success")
        format.html { redirect_to @education_study_level }
        format.json { render :show, status: :created, location: @education_study_level }
      else
        format.html { render :new }
        format.json { render json: @education_study_level.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /education/study_levels/1
  # PATCH/PUT /education/study_levels/1.json
  def update
    respond_to do |format|
      if @education_study_level.update(education_study_level_params)
        flash[:success] = t("resources.education.study_levels.update_success")
        format.html { redirect_to @education_study_level }
        format.json { render :show, status: :ok, location: @education_study_level }
      else
        flash.now[:error] = @education_study_level.errors.messages
        format.html { render :edit }
        format.json { render json: @education_study_level.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /education/study_levels/1
  # DELETE /education/study_levels/1.json
  def destroy
    @education_study_level.destroy
    respond_to do |format|
      flash[:success] = t("resources.education.study_levels.destroy_success")
      format.html { redirect_to education_study_levels_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_education_study_level
      @education_study_level = Education::StudyLevel.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def education_study_level_params
      params.require(:education_study_level).permit(:name)
    end
end
