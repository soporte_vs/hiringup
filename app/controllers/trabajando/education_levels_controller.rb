class Trabajando::EducationLevelsController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize_action, except: [:show]
  before_action :set_trabajando_education_level, only: [:show, :edit, :update]

  # GET /trabajando/education_levels
  # GET /trabajando/education_levels.json
  def index
    @trabajando_education_levels = Trabajando::EducationLevel.all.page(params[:page])
  end

  # GET /trabajando/job_types/1
  # GET /trabajando/job_types/1.json
  def show
  end

  # GET /trabajando/job_types/new
  # def new
  #   @trabajando_education_level = Trabajando::EducationLevel.new
  # end

  # GET /trabajando/job_types/1/edit
  def edit
  end

  # POST /trabajando/job_types
  # POST /trabajando/job_types.json
  # def create
  #   @trabajando_education_level = Trabajando::EducationLevel.new(trabajando_education_level_params)

  #   respond_to do |format|
  #     if @trabajando_education_level.save
  #       format.html { redirect_to @trabajando_education_level, notice: 'Job type was successfully created.' }
  #       format.json { render :show, status: :created, location: @trabajando_education_level }
  #     else
  #       format.html { render :new }
  #       format.json { render json: @trabajando_education_level.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  # PATCH/PUT /trabajando/job_types/1
  # PATCH/PUT /trabajando/job_types/1.json
  def update
    respond_to do |format|
      if @trabajando_education_level.update(trabajando_education_level_params)
        format.html { redirect_to @trabajando_education_level, notice: 'Actualizado correctamente' }
        format.json { render :show, status: :ok, location: @trabajando_education_level }
      else
        format.html { render :edit }
        format.json { render json: @trabajando_education_level.errors, status: :unprocessable_entity }
      end
    end
  end

  # # DELETE /trabajando/job_types/1
  # # DELETE /trabajando/job_types/1.json
  # def destroy
  #   @trabajando_education_level.destroy
  #   respond_to do |format|
  #     format.html { redirect_to trabajando_education_levels_url, notice: 'Job type was successfully destroyed.' }
  #     format.json { head :no_content }
  #   end
  # end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_trabajando_education_level
      @trabajando_education_level = Trabajando::EducationLevel.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def trabajando_education_level_params
      params.require(:trabajando_education_level).permit(
        :hupe_study_type
      )
    end

    def authorize_action
      render 'errors/403', status: 403, layout: 'application' unless current_user.can?(:admin)
    end
end
