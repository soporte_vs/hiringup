class Trabajando::MaritalStatusesController < ApplicationController
  before_action :set_trabajando_marital_status, only: [:show, :edit, :update]

  # GET /trabajando/marital_statuses
  # GET /trabajando/marital_statuses.json
  def index
    @trabajando_marital_statuses = Trabajando::MaritalStatus.all.page(params[:page])
  end

  # GET /trabajando/marital_statuses/1
  # GET /trabajando/marital_statuses/1.json
  def show
  end

  # GET /trabajando/marital_statuses/1/edit
  def edit
  end

  # PATCH/PUT /trabajando/marital_statuses/1
  # PATCH/PUT /trabajando/marital_statuses/1.json
  def update
    respond_to do |format|
      if @trabajando_marital_status.update(trabajando_marital_status_params)
        format.html { redirect_to @trabajando_marital_status, notice: 'Actualizado correctamente.' }
        format.json { render :show, status: :ok, location: @trabajando_marital_status }
      else
        format.html { render :edit }
        format.json { render json: @trabajando_marital_status.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_trabajando_marital_status
      @trabajando_marital_status = Trabajando::MaritalStatus.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def trabajando_marital_status_params
      params.require(:trabajando_marital_status).permit(:name, :company_marital_status_id)
    end

    def authorize_action
      render 'errors/403', status: 403, layout: 'application' unless current_user.can?(:admin)
    end
end
