class Trabajando::CitiesController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize_action, except: [:show]
  before_action :set_trabajando_city, only: [:show, :edit, :update, :destroy]

  # GET /trabajando/cities
  # GET /trabajando/cities.json
  def index
    @trabajando_cities = Trabajando::City.all.page(params[:page])
  end

  # GET /trabajando/cities/1
  # GET /trabajando/cities/1.json
  def show
  end

  # GET /trabajando/cities/new
  # def new
  #   @trabajando_city = Trabajando::City.new
  # end

  # GET /trabajando/cities/1/edit
  def edit
  end

  # POST /trabajando/cities
  # POST /trabajando/cities.json
  # def create
  #   @trabajando_city = Trabajando::City.new(trabajando_city_params)

  #   respond_to do |format|
  #     if @trabajando_city.save
  #       format.html { redirect_to @trabajando_city, notice: 'Job type was successfully created.' }
  #       format.json { render :show, status: :created, location: @trabajando_city }
  #     else
  #       format.html { render :new }
  #       format.json { render json: @trabajando_city.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  # PATCH/PUT /trabajando/cities/1
  # PATCH/PUT /trabajando/cities/1.json
  def update
    respond_to do |format|
      if @trabajando_city.update(trabajando_city_params)
        format.html { redirect_to @trabajando_city, notice: 'Actualizado correctamente.' }
        format.json { render :show, status: :ok, location: @trabajando_city }
      else
        format.html { render :edit }
        format.json { render json: @trabajando_city.errors, status: :unprocessable_entity }
      end
    end
  end

  # # DELETE /trabajando/cities/1
  # # DELETE /trabajando/cities/1.json
  # def destroy
  #   @trabajando_city.destroy
  #   respond_to do |format|
  #     format.html { redirect_to trabajando_cities_url, notice: 'Job type was successfully destroyed.' }
  #     format.json { head :no_content }
  #   end
  # end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_trabajando_city
      @trabajando_city = Trabajando::City.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def trabajando_city_params
      params.require(:trabajando_city).permit(:hupe_city_id)
    end

    def authorize_action
      render 'errors/403', status: 403, layout: 'application' unless current_user.can?(:admin)
    end
end
