class Trabajando::StatesController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize_action
  before_action :set_trabajando_state, only: [:show, :edit, :update, :destroy]

  # GET /trabajando/states
  # GET /trabajando/states.json
  def index
    @trabajando_states = Trabajando::State.all.page(params[:page])
  end

  # GET /trabajando/states/1
  # GET /trabajando/states/1.json
  def show
  end


  # GET /trabajando/states/1/edit
  def edit
  end

  # PATCH/PUT /trabajando/states/1
  # PATCH/PUT /trabajando/states/1.json
  def update
    respond_to do |format|
      if @trabajando_state.update(trabajando_state_params)
        format.html { redirect_to @trabajando_state, notice: 'Actualizado exitosamente.' }
        format.json { render :show, status: :ok, location: @trabajando_state }
      else
        format.html { render :edit }
        format.json { render json: @trabajando_state.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_trabajando_state
      @trabajando_state = Trabajando::State.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def trabajando_state_params
      params.require(:trabajando_state).permit(:hupe_state_id)
    end

    def authorize_action
      render 'errors/403', status: 403, layout: 'application' unless current_user.can?(:admin)
    end
end
