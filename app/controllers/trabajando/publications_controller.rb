# coding: utf-8
class Trabajando::PublicationsController < ApplicationController
  layout 'course'
  before_action :authenticate_user!
  before_action :set_course
  before_action :authorize_action
  before_action :set_trabajando_publication, only: [:show, :edit, :update, :destroy, :switch_state, :get_trabajando_postulations]

  # GET /trabajando/publications
  # GET /trabajando/publications.json
  def index
    @trabajando_publications = @course.trabajando_publications.page(params[:page])
  end

  # GET /trabajando/publications/1
  # GET /trabajando/publications/1.json
  def show
  end

  # GET /trabajando/publications/new
  def new
    @trabajando_publication = @course.trabajando_publications.new
    @trabajando_publication.number_vacancies = @course.vacancies.count
    @trabajando_publication.title = @course.title
    @trabajando_publication.description = @course.description
    @trabajando_publication.area = Trabajando::Area.find_by(hupe_area: @course.area)
    @trabajando_publication.company_activity = Trabajando::CompanyActivity.find_by(name: 'Educación / Capacitación')
    @trabajando_publication.applicant_profile = Trabajando::ApplicantProfile.find_by(hupe_profile_type: @course.applicant_profile_type)
    @trabajando_publication.software_level = Trabajando::SoftwareLevel.find_by(hupe_software_level: @course.applicant_software_level)
    @trabajando_publication.minimum_requirements = @course.minimun_requirements
    @trabajando_publication.region = Trabajando::State.find_by(hupe_state: @course.territory_city.territory_state)
    @trabajando_publication.city = Trabajando::City.find_by(hupe_city: @course.territory_city)
    @trabajando_publication.desactivate_at ||= (Date.today + 30.day)

    last_minisite_publication = @course.minisite_publications.order(created_at: :desc).first
    last_laborum_publication = @course.laborum_publications.select("question1, question2, question3, question4, question5").last

    if last_minisite_publication
      last_minisite_publication.questions.order(id: :asc).first(5).each_with_index do |question, index|
        @trabajando_publication.send("q#{index+1}=", question.description)
      end
    elsif last_laborum_publication
      last_laborum_publication.attributes.each_with_index do |(attr_name, attr_value), index|
        @trabajando_publication.send("q#{index+1}=", attr_value)  if attr_value.present?
      end
    end

    if Company::PositionTrabajandoJobType.find_by(company_position: @course.company_position)
      @trabajando_publication.job_type = Company::PositionTrabajandoJobType.find_by(company_position: @course.company_position).try(:trabajando_job_type)
    end
    @trabajando_publication.contract_time = @course.contract_type.try(:name)
    @trabajando_publication.work_experience = @course.experience_years
    @trabajando_publication.work_day = Trabajando::WorkDay.joins(:company_timetables).find_by(company_timetables: {id: @course.timetable_id})
    @trabajando_publication.education_level = Trabajando::EducationLevel.find_by(hupe_study_type: @course.study_type)
    @trabajando_publication.education_state = Trabajando::EducationState.find_by(hupe_study_level: @course.study_level)
  end

  # GET /trabajando/publications/1/edit
  def edit
  end

  # POST /trabajando/publications
  # POST /trabajando/publications.json
  def create
    @trabajando_publication = @course.trabajando_publications.new(trabajando_publication_params)

    respond_to do |format|
      if @trabajando_publication.save_and_publish
        format.html {
          flash[:success] = t("publications.create_success")
          redirect_to trabajando_publication_path(@course, @trabajando_publication)
        }
        format.json { render :show, status: :created, location: @trabajando_publication }
      else
        format.html {
          flash[:error] = @trabajando_publication.errors.messages
          render :new
        }
        format.json { render json: @trabajando_publication.errors, status: :unprocessable_entity }
      end
    end
  end

  # GET /trabajando/publications/1/edit
  def edit
  end

  # PATCH/PUT /trabajando/publications/1
  # PATCH/PUT /trabajando/publications/1.json
  def update
    respond_to do |format|
      @trabajando_publication.attributes = trabajando_publication_params

      if @trabajando_publication.save_and_publish
        format.html {
          flash[:success] = t("publications.update_success")
          redirect_to trabajando_publication_path(@course, @trabajando_publication)
        }
        format.json { render :show, status: :ok, location: @trabajando_publication }
      else
        format.html { render :edit }
        format.json { render json: @trabajando_publication.errors, status: :unprocessable_entity }
      end
    end
  end

  def switch_state
    respond_to do |format|
      begin
        if @trabajando_publication.send("#{params[:switch]}")
          format.html do
            flash[:success] = t("trabajando/publication.actions.#{params[:switch]}.success")
            redirect_to trabajando_publication_path(publication_id: @trabajando_publication)
          end
          format.json { render json: @trabajando_publication }
        else
          format.html do
            flash[:error] = t("trabajando/publication.actions.#{params[:switch]}.unsuccess")
            redirect_to trabajando_publication_path(publication_id: @trabajando_publication)
          end
          format.json { render json: @trabajando_publication.errors }
        end
      rescue AASM::InvalidTransition => e
        action = t("trabajando/publication.actions.#{params[:switch]}.text").downcase
        state = t("trabajando/publication.state.#{@trabajando_publication.aasm_state}.default")
        error = t("trabajando/publication.errors.transition", action: action, state: state)
        format.html do
          flash[:error] = error
          redirect_to trabajando_publication_path(publication_id: @trabajando_publication)
        end
        format.json { render json: error }
      end
    end
  end


  # # DELETE /trabajando/publications/1
  # # DELETE /trabajando/publications/1.json
  # def destroy
  #   @trabajando_publication.destroy
  #   respond_to do |format|
  #     format.html { redirect_to trabajando_publications_url, notice: 'Publication was successfully destroyed.' }
  #     format.json { head :no_content }
  #   end
  # end

  def get_trabajando_postulations
    query_time = @trabajando_publication.created_at
    Trabajando::Postulation.delay.get_postulations(query_time.beginning_of_day, publication_id: @trabajando_publication.id)
    flash[:notice] = t("trabajando/publication.actions.get_trabajando_postulations.success")
    redirect_to trabajando_publication_path(id: @trabajando_publication.course.to_param, publication_id: @trabajando_publication.to_param)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_trabajando_publication
      @trabajando_publication = @course.trabajando_publications.find(params[:publication_id])
      render 'errors/404', status: 404, layout: 'application' unless @trabajando_publication
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_course
      @course = Course.find_by(id: params[:id])
      render 'errors/404', status: 404, layout: 'application' unless @course
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def trabajando_publication_params
      params.require(:trabajando_publication).permit(
        :title,
        :description,
        :desactivate_at,
        :number_vacancies,
        :area_id,
        :job_type_id,
        :company_activity_id,
        :work_day_id,
        :applicant_profile_id,
        :education_level_id,
        :education_state_id,
        :software_level_id,
        :region_id,
        :city_id,
        :q1,
        :q2,
        :q3,
        :q4,
        :q5,
        :salary,
        :contract_time,
        :show_salary,
        :salary_comment,
        :work_time,
        :workplace,
        :minimum_requirements,
        :work_experience,
        :own_transport,
        :is_confidential
      )
    end

    def authorize_action
      if !current_user.can?(:admin_publications, @course)
        render 'errors/403', status: 403, layout: 'application'
      end
    end
end
