class Trabajando::JobTypesController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize_action, except: [:show]
  before_action :set_trabajando_job_type, only: [:show, :edit, :update, :destroy]

  # GET /trabajando/job_types
  # GET /trabajando/job_types.json
  def index
    @trabajando_job_types = Trabajando::JobType.all.page(params[:page])
  end

  # GET /trabajando/job_types/1
  # GET /trabajando/job_types/1.json
  def show
  end

  # GET /trabajando/job_types/new
  # def new
  #   @trabajando_job_type = Trabajando::JobType.new
  # end

  # GET /trabajando/job_types/1/edit
  def edit
  end

  # POST /trabajando/job_types
  # POST /trabajando/job_types.json
  # def create
  #   @trabajando_job_type = Trabajando::JobType.new(trabajando_job_type_params)

  #   respond_to do |format|
  #     if @trabajando_job_type.save
  #       format.html { redirect_to @trabajando_job_type, notice: 'Job type was successfully created.' }
  #       format.json { render :show, status: :created, location: @trabajando_job_type }
  #     else
  #       format.html { render :new }
  #       format.json { render json: @trabajando_job_type.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  # PATCH/PUT /trabajando/job_types/1
  # PATCH/PUT /trabajando/job_types/1.json
  def update
    respond_to do |format|
      if @trabajando_job_type.update(trabajando_job_type_params)
        format.html { redirect_to @trabajando_job_type, notice: 'Actualizado correctamente' }
        format.json { render :show, status: :ok, location: @trabajando_job_type }
      else
        format.html { render :edit }
        format.json { render json: @trabajando_job_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # # DELETE /trabajando/job_types/1
  # # DELETE /trabajando/job_types/1.json
  # def destroy
  #   @trabajando_job_type.destroy
  #   respond_to do |format|
  #     format.html { redirect_to trabajando_job_types_url, notice: 'Job type was successfully destroyed.' }
  #     format.json { head :no_content }
  #   end
  # end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_trabajando_job_type
      @trabajando_job_type = Trabajando::JobType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def trabajando_job_type_params
      params.require(:trabajando_job_type).permit(
        company_position_ids: [],
      )
    end

    def authorize_action
      render 'errors/403', status: 403, layout: 'application' unless current_user.can?(:admin)
    end
end
