class Trabajando::CountriesController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize_action
  before_action :set_trabajando_country, only: [:show, :edit, :update]

  # GET /trabajando/countries
  # GET /trabajando/countries.json
  def index
    @trabajando_countries = Trabajando::Country.all.page(params[:page])
  end

  # GET /trabajando/countries/1
  # GET /trabajando/countries/1.json
  def show
  end


  # GET /trabajando/countries/1/edit
  def edit
  end

  # PATCH/PUT /trabajando/countries/1
  # PATCH/PUT /trabajando/countries/1.json
  def update
    respond_to do |format|
      if @trabajando_country.update(trabajando_country_params)
        format.html { redirect_to @trabajando_country, notice: 'Actualizado correctamente.' }
        format.json { render :show, status: :ok, location: @trabajando_country }
      else
        format.html { render :edit }
        format.json { render json: @trabajando_country.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_trabajando_country
      @trabajando_country = Trabajando::Country.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def trabajando_country_params
      params.require(:trabajando_country).permit(:name, :trabajando_id, :country_id)
    end

    def authorize_action
      render 'errors/403', status: 403, layout: 'application' unless current_user.can?(:admin)
    end
end
