class Trabajando::SoftwareLevelsController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize_action, except: [:show]
  before_action :set_trabajando_software_level, only: [:show, :edit, :update, :destroy]

  # GET /trabajando/software_levels
  # GET /trabajando/software_levels.json
  def index
    @trabajando_software_levels = Trabajando::SoftwareLevel.all.page(params[:page])
  end

  # GET /trabajando/software_levels/1
  # GET /trabajando/software_levels/1.json
  def show
  end

  # GET /trabajando/software_levels/new
  # def new
  #   @trabajando_software_level = Trabajando::SoftwareLevel.new
  # end

  # GET /trabajando/software_levels/1/edit
  def edit
  end

  # POST /trabajando/software_levels
  # POST /trabajando/software_levels.json
  # def create
  #   @trabajando_software_level = Trabajando::SoftwareLevel.new(trabajando_software_level_params)

  #   respond_to do |format|
  #     if @trabajando_software_level.save
  #       format.html { redirect_to @trabajando_software_level, notice: 'Job type was successfully created.' }
  #       format.json { render :show, status: :created, location: @trabajando_software_level }
  #     else
  #       format.html { render :new }
  #       format.json { render json: @trabajando_software_level.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  # PATCH/PUT /trabajando/software_levels/1
  # PATCH/PUT /trabajando/software_levels/1.json
  def update
    respond_to do |format|
      if @trabajando_software_level.update(trabajando_software_level_params)
        format.html { redirect_to @trabajando_software_level, notice: 'Actualizado correctamente' }
        format.json { render :show, status: :ok, location: @trabajando_software_level }
      else
        format.html { render :edit }
        format.json { render json: @trabajando_software_level.errors, status: :unprocessable_entity }
      end
    end
  end

  # # DELETE /trabajando/software_levels/1
  # # DELETE /trabajando/software_levels/1.json
  # def destroy
  #   @trabajando_software_level.destroy
  #   respond_to do |format|
  #     format.html { redirect_to trabajando_software_levels_url, notice: 'Job type was successfully destroyed.' }
  #     format.json { head :no_content }
  #   end
  # end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_trabajando_software_level
      @trabajando_software_level = Trabajando::SoftwareLevel.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def trabajando_software_level_params
      params.require(:trabajando_software_level).permit(
        :hupe_software_level_id
      )
    end

    def authorize_action
      render 'errors/403', status: 403, layout: 'application' unless current_user.can?(:admin)
    end
end
