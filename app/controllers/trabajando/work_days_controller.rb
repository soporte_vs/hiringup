# coding: utf-8
class Trabajando::WorkDaysController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize_action, except: [:show]
  before_action :set_trabajando_work_day, only: [:show, :edit, :update, :destroy]

  # GET /trabajando/work_days
  # GET /trabajando/work_days.json
  def index
    @trabajando_work_days = Trabajando::WorkDay.all.page(params[:page])
  end

  # GET /trabajando/work_days/1
  # GET /trabajando/work_days/1.json
  def show
  end

  # # GET /trabajando/work_days/new
  # def new
  #   @trabajando_work_day = Trabajando::WorkDay.new
  # end

  # GET /trabajando/work_days/1/edit
  def edit
  end

  # POST /trabajando/work_days
  # POST /trabajando/work_days.json
  # def create
  #   @trabajando_work_day = Trabajando::WorkDay.new(trabajando_work_day_params)

  #   respond_to do |format|
  #     if @trabajando_work_day.save
  #       format.html { redirect_to @trabajando_work_day, notice: 'Work day was successfully created.' }
  #       format.json { render :show, status: :created, location: @trabajando_work_day }
  #     else
  #       format.html { render :new }
  #       format.json { render json: @trabajando_work_day.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  # PATCH/PUT /trabajando/work_days/1
  # PATCH/PUT /trabajando/work_days/1.json
  def update
    respond_to do |format|
      if @trabajando_work_day.update(trabajando_work_day_params)
        format.html { redirect_to @trabajando_work_day, notice: 'jornada laboral actualizada correctamentes' }
        format.json { render :show, status: :ok, location: @trabajando_work_day }
      else
        format.html { render :edit }
        format.json { render json: @trabajando_work_day.errors, status: :unprocessable_entity }
      end
    end
  end

  # # DELETE /trabajando/work_days/1
  # # DELETE /trabajando/work_days/1.json
  # def destroy
  #   @trabajando_work_day.destroy
  #   respond_to do |format|
  #     format.html { redirect_to trabajando_work_days_url, notice: 'Work day was successfully destroyed.' }
  #     format.json { head :no_content }
  #   end
  # end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_trabajando_work_day
      @trabajando_work_day = Trabajando::WorkDay.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def trabajando_work_day_params
      params.require(:trabajando_work_day).permit(
        company_timetable_ids: []
      )
    end

    def authorize_action
      render 'errors/403', status: 403, layout: 'application' unless current_user.can?(:admin)
    end
end
