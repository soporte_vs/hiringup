class Trabajando::ApplicantProfilesController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize_action, except: [:show]
  before_action :set_trabajando_applicant_profile, only: [:show, :edit, :update, :destroy]

  # GET /trabajando/applicant_profiles
  # GET /trabajando/applicant_profiles.json
  def index
    @trabajando_applicant_profiles = Trabajando::ApplicantProfile.all.page(params[:page])
  end

  # GET /trabajando/applicant_profiles/1
  # GET /trabajando/applicant_profiles/1.json
  def show
  end

  # GET /trabajando/applicant_profiles/new
  #def new
  #  @trabajando_applicant_profile = Trabajando::ApplicantProfile.new
  #end

  # GET /trabajando/applicant_profiles/1/edit
  def edit
  end

  # POST /trabajando/applicant_profiles
  # POST /trabajando/applicant_profiles.json
  #def create
  #  @trabajando_applicant_profile = Trabajando::ApplicantProfile.new(trabajando_applicant_profile_params)

  #  respond_to do |format|
  #    if @trabajando_applicant_profile.save
  #      format.html { redirect_to @trabajando_applicant_profile, notice: 'Tipo de perfil de candidato creado correctamente.' }
  #      format.json { render :show, status: :created, location: @trabajando_applicant_profile }
  #    else
  #      format.html { render :new }
  #      format.json { render json: @trabajando_applicant_profile.errors, status: :unprocessable_entity }
  #    end
  #  end
  #end

  # PATCH/PUT /trabajando/applicant_profiles/1
  # PATCH/PUT /trabajando/applicant_profiles/1.json
  def update
    respond_to do |format|
      if @trabajando_applicant_profile.update(trabajando_applicant_profile_params)
        format.html { redirect_to @trabajando_applicant_profile, notice: 'Actualizado correctamente' }
        format.json { render :show, status: :ok, location: @trabajando_applicant_profile }
      else
        format.html { render :edit }
        format.json { render json: @trabajando_applicant_profile.errors, status: :unprocessable_entity }
      end
    end
  end

  # # DELETE /trabajando/applicant_profiles/1
  # # DELETE /trabajando/applicant_profiles/1.json
  # def destroy
  #   @trabajando_applicant_profile.destroy
  #   respond_to do |format|
  #     format.html { redirect_to trabajando_applicant_profiles_url, notice: 'Job type was successfully destroyed.' }
  #     format.json { head :no_content }
  #   end
  # end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_trabajando_applicant_profile
      @trabajando_applicant_profile = Trabajando::ApplicantProfile.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def trabajando_applicant_profile_params
      params.require(:trabajando_applicant_profile).permit(
        :hupe_profile_type_id
      )
    end

    def authorize_action
      render 'errors/403', status: 403, layout: 'application' unless current_user.can?(:admin)
    end
end
