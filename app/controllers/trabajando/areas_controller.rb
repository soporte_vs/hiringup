class Trabajando::AreasController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize_action
  before_action :set_trabajando_area, only: [:show, :edit, :update, :destroy]

  # GET /trabajando/areas
  # GET /trabajando/areas.json
  def index
    @trabajando_areas = Trabajando::Area.all.page(params[:page])
  end

  # GET /trabajando/areas/1
  # GET /trabajando/areas/1.json
  def show
  end

  # # GET /trabajando/areas/new
  # def new
  #   @trabajando_area = Trabajando::Area.new
  # end

  # GET /trabajando/areas/1/edit
  def edit
  end

  # # POST /trabajando/areas
  # # POST /trabajando/areas.json
  # def create
  #   @trabajando_area = Trabajando::Area.new(trabajando_area_params)

  #   respond_to do |format|
  #     if @trabajando_area.save
  #       format.html { redirect_to @trabajando_area, notice: 'Area was successfully created.' }
  #       format.json { render :show, status: :created, location: @trabajando_area }
  #     else
  #       format.html { render :new }
  #       format.json { render json: @trabajando_area.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  # PATCH/PUT /trabajando/areas/1
  # PATCH/PUT /trabajando/areas/1.json
  def update
    respond_to do |format|
      if @trabajando_area.update(trabajando_area_params)
        format.html { redirect_to @trabajando_area, notice: 'Area fue actualizada exitosamente' }
        format.json { render :show, status: :ok, location: @trabajando_area }
      else
        format.html { render :edit }
        format.json { render json: @trabajando_area.errors, status: :unprocessable_entity }
      end
    end
  end

  # # DELETE /trabajando/areas/1
  # # DELETE /trabajando/areas/1.json
  # def destroy
  #   @trabajando_area.destroy
  #   respond_to do |format|
  #     format.html { redirect_to trabajando_areas_url, notice: 'Area was successfully destroyed.' }
  #     format.json { head :no_content }
  #   end
  # end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_trabajando_area
      @trabajando_area = Trabajando::Area.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def trabajando_area_params
      params.require(:trabajando_area).permit(:hupe_area_id)
    end

  def authorize_action
      render 'errors/403', status: 403, layout: 'application' unless current_user.can?(:admin)
    end
end
