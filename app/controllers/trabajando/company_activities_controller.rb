class Trabajando::CompanyActivitiesController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize_action
  before_action :set_trabajando_company_activity, only: [:show, :edit, :update, :destroy]

  # GET /trabajando/company_activities
  # GET /trabajando/company_activities.json
  def index
    @trabajando_company_activities = Trabajando::CompanyActivity.all.page(params[:page])
  end

  # GET /trabajando/company_activities/1
  # GET /trabajando/company_activities/1.json
  def show
  end

  # GET /trabajando/company_activities/new
  # def new
  #   @trabajando_company_activity = Trabajando::CompanyActivity.new
  # end

  # GET /trabajando/company_activities/1/edit
  def edit
  end

  # POST /trabajando/company_activities
  # POST /trabajando/company_activities.json
  # def create
  #   @trabajando_company_activity = Trabajando::CompanyActivity.new(trabajando_company_activity_params)

  #   respond_to do |format|
  #     if @trabajando_company_activity.save
  #       format.html { redirect_to @trabajando_company_activity, notice: 'Job type was successfully created.' }
  #       format.json { render :show, status: :created, location: @trabajando_company_activity }
  #     else
  #       format.html { render :new }
  #       format.json { render json: @trabajando_company_activity.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  # PATCH/PUT /trabajando/company_activities/1
  # PATCH/PUT /trabajando/company_activities/1.json
  def update
    respond_to do |format|
      if @trabajando_company_activity.update(trabajando_company_activity_params)
        format.html { redirect_to @trabajando_company_activity, notice: 'Actualizado correctamente' }
        format.json { render :show, status: :ok, location: @trabajando_company_activity }
      else
        format.html { render :edit }
        format.json { render json: @trabajando_company_activity.errors, status: :unprocessable_entity }
      end
    end
  end

  # # DELETE /trabajando/company_activities/1
  # # DELETE /trabajando/company_activities/1.json
  # def destroy
  #   @trabajando_company_activity.destroy
  #   respond_to do |format|
  #     format.html { redirect_to trabajando_company_activities_url, notice: 'Job type was successfully destroyed.' }
  #     format.json { head :no_content }
  #   end
  # end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_trabajando_company_activity
      @trabajando_company_activity = Trabajando::CompanyActivity.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def trabajando_company_activity_params
      params.require(:trabajando_company_activity).permit(:name)
    end

    def authorize_action
      render 'errors/403', status: 403, layout: 'application' unless current_user.can?(:admin)
    end
end
