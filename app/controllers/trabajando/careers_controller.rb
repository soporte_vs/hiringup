class Trabajando::CareersController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize_action, except: [:show]
  before_action :set_trabajando_career, only: [:show, :edit, :update, :destroy]

  # GET /trabajando/careers
  # GET /trabajando/careers.json
  def index
    @trabajando_careers = Trabajando::Career.page(params[:page])
  end

  # GET /trabajando/careers/1
  # GET /trabajando/careers/1.json
  def show
  end

  # GET /trabajando/careers/new
  def new
    @trabajando_career = Trabajando::Career.new
  end

  # GET /trabajando/careers/1/edit
  def edit
  end

  # POST /trabajando/careers
  # POST /trabajando/careers.json
  def create
    @trabajando_career = Trabajando::Career.new(trabajando_career_params)

    respond_to do |format|
      if @trabajando_career.save
        format.html { redirect_to @trabajando_career, notice: 'Career was successfully created.' }
        format.json { render :show, status: :created, location: @trabajando_career }
      else
        format.html { render :new }
        format.json { render json: @trabajando_career.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /trabajando/careers/1
  # PATCH/PUT /trabajando/careers/1.json
  def update
    respond_to do |format|
      if @trabajando_career.update(trabajando_career_params)
        format.html { redirect_to @trabajando_career, notice: 'Career was successfully updated.' }
        format.json { render :show, status: :ok, location: @trabajando_career }
      else
        format.html { render :edit }
        format.json { render json: @trabajando_career.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /trabajando/careers/1
  # DELETE /trabajando/careers/1.json
  def destroy
    @trabajando_career.destroy
    respond_to do |format|
      format.html { redirect_to trabajando_careers_url, notice: 'Career was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_trabajando_career
      @trabajando_career = Trabajando::Career.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def trabajando_career_params
      params.require(:trabajando_career).permit(:name, :trabajando_id, :hupe_career_id)
    end

    def authorize_action
      render 'errors/403', status: 403, layout: 'application' unless current_user.can?(:admin)
    end
end
