class Evaluar::CoursesController < ApplicationController
  before_action :catch_applicant, only: :results
  before_action :catch_evaluar_course, if: "params[:evaluar_course_id].present?"

  def create_in_evaluar
    if @evaluar_course.create_in_evaluar
      flash[:success] = I18n.t("evaluar_course.create_in_evaluar.success")
    else
      flash[:error] =  @evaluar_course.errors.messages
    end
    redirect_to course_path(@evaluar_course.course_id)
  end

  def results
    respond_to do |format|
      format.json do
        message = I18n.t("evaluar_course.request.error")
        status = false
        if params[:processId] && params[:identification] && params[:id]
          evaluar_result = Evaluar::Result.new(data: params,
                            process_id: params[:processId],
                            identification: params[:identification],
                            evaluar_course_id: params[:evaluar_course_id],
                            applicant_id: @applicant.try(:id)
                         )
          if evaluar_result.save
            message = I18n.t("evaluar_course.request.success")
            status = true
          else
            message = evaluar_result.errors.messages
          end
        end
        render json: { "data": status, "messages": message }
      end
    end
  end

  private
    def catch_applicant
      personal_information = People::PersonalInformation.find_by(identification_document_number: params[:identification])
      @applicant = personal_information.try(:user).try(:applicant)
    end

    def catch_evaluar_course
      @evaluar_course = Evaluar::Course.find(params[:evaluar_course_id])
    end
end
