class Company::CencosController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize_action
  before_action :set_company_cenco, only: [:show, :edit, :update, :destroy]

  # GET /company/cencos
  # GET /company/cencos.json
  def index
    respond_to do |format|
      format.html { @company_cencos = Company::Cenco.page(params[:page]) }
      format.json { @company_cencos = Company::Cenco.all }
    end
  end

  # GET /company/cencos/1
  # GET /company/cencos/1.json
  def show
  end

  # GET /company/cencos/new
  def new
    @company_cenco = Company::Cenco.new
  end

  # GET /company/cencos/1/edit
  def edit
  end

  # POST /company/cencos
  # POST /company/cencos.json
  def create
    @company_cenco = Company::Cenco.new(company_cenco_params)

    respond_to do |format|
      if @company_cenco.save
        flash[:success] = t("resources.company.cencos.create_success")
        format.html { redirect_to @company_cenco }
        format.json { render :show, status: :created, location: @company_cenco }
      else
        flash.now[:error] = @company_cenco.errors.messages
        format.html { render :new }
        format.json { render json: @company_cenco.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /company/cencos/1
  # PATCH/PUT /company/cencos/1.json
  def update
    respond_to do |format|
      if @company_cenco.update(company_cenco_params)
        flash[:success] = t("resources.company.cencos.update_success")
        format.html { redirect_to @company_cenco }
        format.json { render :show, status: :ok, location: @company_cenco }
      else
        flash.now[:error] = @company_cenco.errors.messages
        format.html { render :edit }
        format.json { render json: @company_cenco.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /company/cencos/1
  # DELETE /company/cencos/1.json
  def destroy
    @company_cenco.destroy
    respond_to do |format|
      flash[:success] = t("resources.company.cencos.destroy_success")
      format.html { redirect_to company_cencos_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_company_cenco
      @company_cenco = Company::Cenco.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def company_cenco_params
      params.require(:company_cenco).permit(:company_management_id, :name, :address, :cod)
    end


    def authorize_action
      render 'errors/403', status: 403, layout: 'application' unless current_user.can?(:admin)
    end
end
