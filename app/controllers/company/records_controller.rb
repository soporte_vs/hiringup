class Company::RecordsController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize_action
  before_action :set_company_record, only: [:show, :edit, :update, :destroy]

  # GET /company/records
  # GET /company/records.json
  def index
    @company_records = Company::Record.all.page(params[:page])
  end

  # GET /company/records/1
  # GET /company/records/1.json
  def show
  end

  # GET /company/records/new
  def new
    @company_record = Company::Record.new
  end

  # GET /company/records/1/edit
  def edit
  end

  # POST /company/records
  # POST /company/records.json
  def create
    @company_record = Company::Record.new(company_record_params)

    respond_to do |format|
      if @company_record.save
        flash[:success] = t("company/record.messages.created.success")
        format.html { redirect_to @company_record }
        format.json { render :show, status: :created, location: @company_record }
      else
        format.html { render :new }
        format.json { render json: @company_record.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /company/records/1
  # PATCH/PUT /company/records/1.json
  def update
    respond_to do |format|
      if @company_record.update(company_record_params)
        flash[:success] = t("company/record.messages.updated.success")
        format.html { redirect_to @company_record }
        format.json { render :show, status: :ok, location: @company_record }
      else
        format.html { render :edit }
        format.json { render json: @company_record.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /company/records/1
  # DELETE /company/records/1.json
  def destroy
    @company_record.destroy
    respond_to do |format|
      flash[:success] = t("company/record.messages.destroyed.success")
      format.html { redirect_to company_records_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_company_record
      @company_record = Company::Record.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def company_record_params
      params.require(:company_record).permit(:name, :email_domain, company_business_unit_ids: [])
    end

    def authorize_action
      render 'errors/403', status: 403, layout: 'application' unless current_user.can?(:admin)
    end
end
