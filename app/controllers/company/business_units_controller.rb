class Company::BusinessUnitsController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize_action
  before_action :set_company_business_unit, only: [:show, :edit, :update, :destroy]

  # GET /company/business_units
  # GET /company/business_units.json
  def index
    respond_to do |format|
      format.html { @company_business_units = Company::BusinessUnit.page(params[:page]) }
      format.json { @company_business_units = Company::BusinessUnit.all }
    end
  end

  # GET /company/business_units/1
  # GET /company/business_units/1.json
  def show
  end

  # GET /company/business_units/new
  def new
    @company_business_unit = Company::BusinessUnit.new
  end

  # GET /company/business_units/1/edit
  def edit
  end

  # POST /company/business_units
  # POST /company/business_units.json
  def create
    @company_business_unit = Company::BusinessUnit.new(company_business_unit_params)

    respond_to do |format|
      if @company_business_unit.save
        flash[:success] = I18n.t("resources.company.business_units.create_success")
        format.html { redirect_to @company_business_unit }
        format.json { render :show, status: :created, location: @company_business_unit }
      else
        flash.now[:error] = @company_business_unit.errors.messages
        format.html { render :new }
        format.json { render json: @company_business_unit.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /company/business_units/1
  # PATCH/PUT /company/business_units/1.json
  def update
    respond_to do |format|
      if @company_business_unit.update(company_business_unit_params)
        flash[:success] = I18n.t("resources.company.business_units.update_success")
        format.html { redirect_to @company_business_unit }
        format.json { render :show, status: :ok, location: @company_business_unit }
      else
        flash.now[:error] = @company_business_unit.errors.messages
        format.html { render :edit }
        format.json { render json: @company_business_unit.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /company/business_units/1
  # DELETE /company/business_units/1.json
  def destroy
    @company_business_unit.destroy
    respond_to do |format|
      flash[:success] = I18n.t("resources.company.business_units.destroy_success")
      format.html { redirect_to company_business_units_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_company_business_unit
      @company_business_unit = Company::BusinessUnit.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def company_business_unit_params
      params.require(:company_business_unit).permit(:name, :description)
    end

    def authorize_action
      render 'errors/403', status: 403, layout: 'application' unless current_user.can?(:admin)
    end
end
