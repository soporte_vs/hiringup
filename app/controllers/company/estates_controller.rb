class Company::EstatesController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize_action
  before_action :set_company_estate, only: [:show, :edit, :update, :destroy]

  # GET /estates
  # GET /estates.json
  def index
    respond_to do |format|
      format.html { @company_estates = Company::Estate.page(params[:page]) }
      format.json { @company_estates = Company::Estate.all }
    end
  end

  # GET /estates/1
  # GET /estates/1.json
  def show
  end

  # GET /estates/new
  def new
    @company_estate = Company::Estate.new
  end

  # GET /estates/1/edit
  def edit
  end

  # POST /estates
  # POST /estates.json
  def create
    @company_estate = Company::Estate.new(company_estate_params)

    respond_to do |format|
      if @company_estate.save
        flash[:success] = t("resources.company.estates.create_success")
        format.html { redirect_to @company_estate }
        format.json { render :show, status: :created, location: @company_estate }
      else
        format.html { render :new }
        format.json { render json: @company_estate.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /estates/1
  # PATCH/PUT /estates/1.json
  def update
    respond_to do |format|
      if @company_estate.update(company_estate_params)
        flash[:success] = t("resources.company.estates.update_success")
        format.html { redirect_to @company_estate }
        format.json { render :show, status: :ok, location: @company_estate }
      else
        format.html { render :edit }
        format.json { render json: @company_estate.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /estates/1
  # DELETE /estates/1.json
  def destroy
    @company_estate.destroy
    flash[:success] = t("resources.company.estates.destroy_success")
    respond_to do |format|
      format.html { redirect_to company_estates_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_company_estate
      @company_estate = Company::Estate.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def company_estate_params
      params.require(:company_estate).permit(:name)
    end

    def authorize_action
      render 'errors/403', status: 403, layout: 'application' unless current_user.can?(:admin)
    end
end
