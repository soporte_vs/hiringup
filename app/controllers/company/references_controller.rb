class Company::ReferencesController < ApplicationController
  before_action :set_company_reference, only: [:confirm]

  def new
    @company_reference = Company::Reference.new
    @company_reference.applicant = Applicant::Base.new(
      user: User.new(personal_information: People::PersonalInformation.new)
    )
  end

  def create
    @company_reference = Company::Reference.new(company_reference_params)

    respond_to do |format|
      if @company_reference.save
        flash[:success] = t("company/reference.messages.created.success")
        format.html { redirect_to new_company_reference_path }
        format.json { render :new, status: :created, location: new_company_reference_path }
      else
        flash.now[:error] = @company_reference.errors.messages
        format.html { render :new }
        format.json { render json: @company_reference.errors, status: :unprocessable_entity }
      end
    end
  end

  def confirm
    if params[:token] != @company_reference.generate_confirm_token
      render 'errors/404', status: 404, layout: 'application'
    elsif @company_reference.confirm
      true
    else
      redirect_to root_path
      flash[:alert] = "Ya esta referencia fue confirmada"
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_company_reference
      @company_reference = Company::Reference.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def company_reference_params
      arguments = params.require(:company_reference).permit(
        :email_name,
        :email_domain,
        :first_name,
        :last_name,
        :position,
        :observations,
        position_ids: [],
        applicant_attributes: [
          user_attributes: [
            :email,
            personal_information_attributes: [
              :first_name,
              :last_name
            ]
          ]
        ]
      )
      if arguments.present? && arguments[:applicant_attributes].present? && arguments[:applicant_attributes][:user_attributes].present?
        arguments[:applicant_attributes][:type] = Applicant::Catched.to_s
      end
      return arguments
    end

    def authorize_action
      render 'errors/403', status: 403, layout: 'application' unless current_user.company_employee.present?
    end
end
