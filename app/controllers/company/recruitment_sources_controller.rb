class Company::RecruitmentSourcesController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize_action
  before_action :set_company_recruitment_source, only: [:show, :edit, :update, :destroy]

  # GET /company/recruitment_sources
  # GET /company/recruitment_sources.json
  def index
    respond_to do |format|
      format.html { @company_recruitment_sources = Company::RecruitmentSource.page(params[:page]) }
      format.json { @company_recruitment_sources = Company::RecruitmentSource.all }
    end
  end

  # GET /company/recruitment_sources/1
  # GET /company/recruitment_sources/1.json
  def show
  end

  def new
    @company_recruitment_source = Company::RecruitmentSource.new
  end

  # GET /company/recruitment_sources/1/edit
  def edit
  end

  def create
    @company_recruitment_source = Company::RecruitmentSource.new(company_recruitment_source_params)

    respond_to do |format|
      if @company_recruitment_source.save
        flash[:success] = I18n.t("resources.company.recruitment_sources.create_success")
        format.html { redirect_to @company_recruitment_source }
        format.json { render :show, status: :created, location: @company_recruitment_source }
      else
        flash.now[:error] = @company_recruitment_source.errors.messages
        format.html { render :new }
        format.json { render json: @company_recruitment_source.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /company/recruitment_sources/1
  # PATCH/PUT /company/recruitment_sources/1.json
  def update
    respond_to do |format|
      if @company_recruitment_source.update(company_recruitment_source_params)
        flash[:success] = I18n.t("resources.company.recruitment_sources.update_success")
        format.html { redirect_to @company_recruitment_source }
        format.json { render :show, status: :ok, location: @company_recruitment_source }
      else
        flash.now[:error] = @company_recruitment_source.errors.messages
        format.html { render :edit }
        format.json { render json: @company_recruitment_source.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @company_recruitment_source.destroy
    respond_to do |format|
      flash[:success] = I18n.t("resources.company.recruitment_sources.destroy_success")
      format.html { redirect_to company_recruitment_sources_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_company_recruitment_source
      @company_recruitment_source = Company::RecruitmentSource.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def company_recruitment_source_params
      params.require(:company_recruitment_source).permit(:name, :description)
    end

    def authorize_action
      render 'errors/403', status: 403, layout: 'application' unless current_user.can?(:admin)
    end

end
