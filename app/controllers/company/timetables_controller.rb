class Company::TimetablesController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize_action
  before_action :set_company_timetable, only: [:show, :edit, :update, :destroy]

  # GET /timetables
  # GET /timetables.json
  def index
    respond_to do |format|
      format.html { @company_timetables = Company::Timetable.page(params[:page]) }
      format.json { @company_timetables = Company::Timetable.all }
    end
  end

  # GET /timetables/1
  # GET /timetables/1.json
  def show
  end

  # GET /timetables/new
  def new
    @company_timetable = Company::Timetable.new
  end

  # GET /timetables/1/edit
  def edit
  end

  # POST /timetables
  # POST /timetables.json
  def create
    @company_timetable = Company::Timetable.new(company_timetable_params)

    respond_to do |format|
      if @company_timetable.save
        flash[:success] = t("resources.company.timetables.create_success")
        format.html { redirect_to @company_timetable }
        format.json { render :show, status: :created, location: @company_timetable }
      else
        flash.now[:error] = @company_timetable.errors.messages
        format.html { render :new }
        format.json { render json: @company_timetable.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /timetables/1
  # PATCH/PUT /timetables/1.json
  def update
    respond_to do |format|
      if @company_timetable.update(company_timetable_params)
        flash[:success] = t("resources.company.timetables.update_success")
        format.html { redirect_to @company_timetable }
        format.json { render :show, status: :ok, location: @company_timetable }
      else
        flash.now[:error] = @company_timetable.errors.messages
        format.html { render :edit }
        format.json { render json: @company_timetable.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /timetables/1
  # DELETE /timetables/1.json
  def destroy
    @company_timetable.destroy
    respond_to do |format|
      flash[:success] = t("resources.company.timetables.destroy_success")
      format.html { redirect_to company_timetables_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_company_timetable
      @company_timetable = Company::Timetable.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def company_timetable_params
      params.require(:company_timetable).permit(:name, :description)
    end

    def authorize_action
      render 'errors/403', status: 403, layout: 'application' unless current_user.can?(:admin)
    end
end
