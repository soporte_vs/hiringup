class Company::AreasController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize_action, except: [:show]
  before_action :set_company_area, only: [:show, :edit, :update, :destroy]

  # GET /company/areas
  # GET /company/areas.json
  def index
    @company_areas = Company::Area.all.page(params[:page])
  end

  # GET /company/areas/1
  # GET /company/areas/1.json
  def show
  end

  # GET /company/areas/new
  def new
    @company_area = Company::Area.new
  end

  # GET /company/areas/1/edit
  def edit
  end

  # POST /company/areas
  # POST /company/areas.json
  def create
    @company_area = Company::Area.new(company_area_params)

    respond_to do |format|
      if @company_area.save
        format.html { redirect_to @company_area, notice: 'El area fué creada.' }
        format.json { render :show, status: :created, location: @company_area }
      else
        format.html { render :new }
        format.json { render json: @company_area.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /company/areas/1
  # PATCH/PUT /company/areas/1.json
  def update
    respond_to do |format|
      if @company_area.update(company_area_params)
        format.html { redirect_to @company_area, notice: 'El area fué actualizada.' }
        format.json { render :show, status: :ok, location: @company_area }
      else
        format.html { render :edit }
        format.json { render json: @company_area.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /company/areas/1
  # DELETE /company/areas/1.json
  def destroy
    @company_area.destroy
    respond_to do |format|
      format.html { redirect_to company_areas_url, notice: 'El area fué elminada.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_company_area
      @company_area = Company::Area.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def company_area_params
      params.require(:company_area).permit(:name)
    end

    def authorize_action
      render 'errors/403', status: 403, layout: 'application' unless current_user.can?(:admin)
    end
end
