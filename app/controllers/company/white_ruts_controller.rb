class Company::WhiteRutsController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize_action
  before_action :set_company_white_rut, only: [:show, :edit, :update, :destroy]

  # GET /company/white_ruts
  # GET /company/white_ruts.json
  def index
    @company_white_ruts = Company::WhiteRut.all.page(params[:page])
  end

  # GET /company/white_ruts/1
  # GET /company/white_ruts/1.json
  def show
  end

  # GET /company/white_ruts/new
  def new
    @company_white_rut = Company::WhiteRut.new
  end

  # GET /company/white_ruts/1/edit
  def edit
  end

  # POST /company/white_ruts
  # POST /company/white_ruts.json
  def create
    @company_white_rut = Company::WhiteRut.new(company_white_rut_params)

    respond_to do |format|
      if @company_white_rut.save
        flash[:success] = I18n.t('company/white_rut.actions.create.success')
        format.html { redirect_to @company_white_rut }
        format.json { render :show, status: :created, location: @company_white_rut }
      else
        format.html { render :new }
        format.json { render json: @company_white_rut.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /company/white_ruts/1
  # PATCH/PUT /company/white_ruts/1.json
  def update
    respond_to do |format|
      if @company_white_rut.update(company_white_rut_params)
        flash[:success] = I18n.t('company/white_rut.actions.update.success')
        format.html { redirect_to @company_white_rut }
        format.json { render :show, status: :ok, location: @company_white_rut }
      else
        format.html { render :edit }
        format.json { render json: @company_white_rut.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /company/white_ruts/1
  # DELETE /company/white_ruts/1.json
  def destroy
    @company_white_rut.destroy
    respond_to do |format|
      flash[:success] = I18n.t('company/white_rut.actions.destroy.success')
      format.html { redirect_to company_white_ruts_url }
      format.json { head :no_content }
    end
  end

  def upload_white_list
    if params[:file].try(:path).present? && params[:notify_to].present?
      tmp_file = write_temp_file(params[:file])
      Company::WhiteRut.delay.load_white_list_from_file(tmp_file, params[:notify_to])
      flash[:success] = 'Archivo subido exitosamente, al ser procesado sera notificado a través del correo ingresado'
    else
      flash[:error] = 'Debe subir el archivo con los ruts e ingresar un email de notifiación'
    end
    redirect_to company_white_ruts_url
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_company_white_rut
      @company_white_rut = Company::WhiteRut.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def company_white_rut_params
      params.require(:company_white_rut).permit(:rut)
    end

    def authorize_action
      render 'errors/403', status: 403, layout: 'application' unless current_user.can?(:admin)
    end

    def write_temp_file(file)
      file_name = "ruts-#{I18n.l(Time.now, format: '%d_%m_%Y_%H_%M_%S')}"
      tmp_file = "#{Rails.root}/tmp/#{file_name}.xlsx"
      File.open(tmp_file, 'wb') do |f|
        f.write file.read
      end
      return tmp_file
    end
end
