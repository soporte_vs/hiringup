class Company::ContractTypesController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize_action
  before_action :set_company_contract_type, only: [:show, :edit, :update, :destroy]

  # GET /company/contract_types
  # GET /company/contract_types.json
  def index
    @company_contract_types = Company::ContractType.page(params[:page])
  end

  # GET /company/contract_types/1
  # GET /company/contract_types/1.json
  def show
  end

  # GET /company/contract_types/new
  def new
    @company_contract_type = Company::ContractType.new
  end

  # GET /company/contract_types/1/edit
  def edit
  end

  # POST /company/contract_types
  # POST /company/contract_types.json
  def create
    @company_contract_type = Company::ContractType.new(company_contract_type_params)

    respond_to do |format|
      if @company_contract_type.save
        flash[:success] = I18n.t('resources.company.contract_types.create_success')
        format.html { redirect_to @company_contract_type}
        format.json { render :show, status: :created, location: @company_contract_type }
      else
        flash[:error] = @company_contract_type.errors.messages
        format.html { render :new }
        format.json { render json: @company_contract_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /company/contract_types/1
  # PATCH/PUT /company/contract_types/1.json
  def update
    respond_to do |format|
      if @company_contract_type.update(company_contract_type_params)
        flash[:success] = I18n.t('resources.company.contract_types.update_success')
        format.html { redirect_to @company_contract_type}
        format.json { render :show, status: :ok, location: @company_contract_type }
      else
        flash[:error] = @company_contract_type.errors.messages
        format.html { render :edit }
        format.json { render json: @company_contract_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /company/contract_types/1
  # DELETE /company/contract_types/1.json
  def destroy
    @company_contract_type.destroy
    respond_to do |format|
      flash[:success] = I18n.t('resources.company.contract_types.destroy_success')
      format.html { redirect_to company_contract_types_url}
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_company_contract_type
      @company_contract_type = Company::ContractType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def company_contract_type_params
      params.require(:company_contract_type).permit(:name, :description)
    end


    def authorize_action
      render 'errors/403', status: 403, layout: 'application' unless current_user.can?(:admin)
    end
end
