class Company::VacancyRequestReasonsController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize_action
  before_action :set_company_vacancy_request_reason, only: [:show, :edit, :update, :destroy]

  # GET /company/vacancy_request_reasons
  # GET /company/vacancy_request_reasons.json
  def index
    @company_vacancy_request_reasons = Company::VacancyRequestReason.page(params[:page])
  end

  # GET /company/vacancy_request_reasons/1
  # GET /company/vacancy_request_reasons/1.json
  def show
  end

  # GET /company/vacancy_request_reasons/new
  def new
    @company_vacancy_request_reason = Company::VacancyRequestReason.new
  end

  # GET /company/vacancy_request_reasons/1/edit
  def edit
  end

  # POST /company/vacancy_request_reasons
  # POST /company/vacancy_request_reasons.json
  def create
    @company_vacancy_request_reason = Company::VacancyRequestReason.new(company_vacancy_request_reason_params)

    respond_to do |format|
      if @company_vacancy_request_reason.save
        flash[:success] = I18n.t('company/vacancy_request_reason.actions.create.success')
        format.html { redirect_to @company_vacancy_request_reason }
        format.json { render :show, status: :created, location: @company_vacancy_request_reason }
      else
        format.html { render :new }
        format.json { render json: @company_vacancy_request_reason.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /company/vacancy_request_reasons/1
  # PATCH/PUT /company/vacancy_request_reasons/1.json
  def update
    respond_to do |format|
      if @company_vacancy_request_reason.update(company_vacancy_request_reason_params)
        flash[:success] = I18n.t('company/vacancy_request_reason.actions.update.success')
        format.html { redirect_to @company_vacancy_request_reason }
        format.json { render :show, status: :ok, location: @company_vacancy_request_reason }
      else
        format.html { render :edit }
        format.json { render json: @company_vacancy_request_reason.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /company/vacancy_request_reasons/1
  # DELETE /company/vacancy_request_reasons/1.json
  def destroy
    @company_vacancy_request_reason.destroy
    respond_to do |format|
      flash[:success] = I18n.t('company/vacancy_request_reason.actions.destroy.success')
      format.html { redirect_to company_vacancy_request_reasons_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_company_vacancy_request_reason
      @company_vacancy_request_reason = Company::VacancyRequestReason.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def company_vacancy_request_reason_params
      params.require(:company_vacancy_request_reason).permit(:name)
    end

    def authorize_action
      render 'errors/403', status: 403, layout: 'application' unless current_user.can?(:admin)
    end
end
