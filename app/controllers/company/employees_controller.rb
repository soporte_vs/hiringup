class Company::EmployeesController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize_action

  def upload_load_massive
    if params[:file].try(:path).present? && params[:notify_to].present?
      tmp_file = write_temp_file(params[:file])
      Company::Employee.delay.load_massive_from_file(tmp_file, params[:notify_to])
      flash[:success] = 'Archivo subido exitosamente, al ser procesado sera notificado a través del correo ingresado'
    else
      flash[:error] = 'Debe subir el archivo con los datos de los empleados e ingresar un email de notifiación'
    end
    redirect_to company_employees_path
  end

  def download_file_example
    file_content = File.read("#{Rails.root}/app/assets/documents/plantilla-modelo.xlsx")

    if !file_content.nil?
      send_file 'app/assets/documents/plantilla-modelo.xlsx',
      :type=>"application/xlsx",
      :x_sendfile=>true
    end
  end

  private

    def write_temp_file(file)
      file_name = "employees-#{I18n.l(Time.now, format: '%d_%m_%Y_%H_%M_%S')}"
      tmp_file = "#{Rails.root}/tmp/#{file_name}.xlsx"
      File.open(tmp_file, 'wb') do |f|
        f.write file.read
      end
      return tmp_file
    end

    def authorize_action
      render 'errors/403', :status => 403, layout: 'application' unless current_user.can?(:admin)
    end

end
