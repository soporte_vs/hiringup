class Company::MaritalStatusesController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize_action
  before_action :set_company_marital_status, only: [:show, :edit, :update, :destroy]

  # GET /company/marital_statuses
  # GET /company/marital_statuses.json
  def index
    @company_marital_statuses = Company::MaritalStatus.page(params[:page])
  end

  # GET /company/marital_statuses/1
  # GET /company/marital_statuses/1.json
  def show
  end

  # GET /company/marital_statuses/new
  def new
    @company_marital_status = Company::MaritalStatus.new
  end

  # GET /company/marital_statuses/1/edit
  def edit
  end

  # POST /company/marital_statuses
  # POST /company/marital_statuses.json
  def create
    @company_marital_status = Company::MaritalStatus.new(company_marital_status_params)

    respond_to do |format|
      if @company_marital_status.save
        flash[:success] = I18n.t('company/marital_status.actions.create.success')
        format.html { redirect_to @company_marital_status }
        format.json { render :show, status: :created, location: @company_marital_status }
      else
        format.html { render :new }
        format.json { render json: @company_marital_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /company/marital_statuses/1
  # PATCH/PUT /company/marital_statuses/1.json
  def update
    respond_to do |format|
      if @company_marital_status.update(company_marital_status_params)
        flash[:success] = I18n.t('company/marital_status.actions.update.success')
        format.html { redirect_to @company_marital_status }
        format.json { render :show, status: :ok, location: @company_marital_status }
      else
        format.html { render :edit }
        format.json { render json: @company_marital_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /company/marital_statuses/1
  # DELETE /company/marital_statuses/1.json
  def destroy
    @company_marital_status.destroy
    respond_to do |format|
      flash[:success] = I18n.t('company/marital_status.actions.destroy.success')
      format.html { redirect_to company_marital_statuses_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_company_marital_status
      @company_marital_status = Company::MaritalStatus.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def company_marital_status_params
      params.require(:company_marital_status).permit(:name)
    end

    def authorize_action
      render 'errors/403', status: 403, layout: 'application' unless current_user.can?(:admin)
    end
end
