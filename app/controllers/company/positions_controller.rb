class Company::PositionsController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize_action, except: [:show]
  before_action :set_company_position, only: [:show, :edit, :update, :destroy]

  # GET /company/positions
  # GET /company/positions.json
  def index
    respond_to do |format|
      format.html { @company_positions = Company::Position.page(params[:page]) }
      format.json { @company_positions = Company::Position.all }
    end
  end

  # GET /company/positions/1
  # GET /company/positions/1.json
  def show
  end

  # GET /company/positions/new
  def new
    @company_position = Company::Position.new
  end

  # GET /company/positions/1/edit
  def edit
  end

  # POST /company/positions
  # POST /company/positions.json
  def create
    @company_position = Company::Position.new(company_position_params)

    respond_to do |format|
      if @company_position.save
        flash[:success] = t("resources.company.positions.create_success")
        format.html { redirect_to @company_position }
        format.json { render :show, status: :created, location: @company_position }
      else
        flash.now[:error] = @company_position.errors.messages
        format.html { render :new }
        format.json { render json: @company_position.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /company/positions/1
  # PATCH/PUT /company/positions/1.json
  def update
    respond_to do |format|
      if @company_position.update(company_position_params)
        flash[:success] = t("resources.company.positions.update_success")
        format.html { redirect_to @company_position }
        format.json { render :show, status: :ok, location: @company_position }
      else
        flash.now[:error] = @company_position.errors.messages
        format.html { render :edit }
        format.json { render json: @company_position.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /company/positions/1
  # DELETE /company/positions/1.json
  def destroy
    @company_position.destroy
    respond_to do |format|
      flash[:success] = t("resources.company.positions.destroy_success")
      format.html { redirect_to company_positions_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_company_position
      @company_position = Company::Position.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def company_position_params
      params.require(:company_position).permit(
        :name,
        :description,
        :cod,
        company_cenco_ids: []).merge(tag_ids: params[:company_position][:tags])

    end

    def authorize_action
      render 'errors/403', status: 403, layout: 'application' unless current_user.can?(:admin)
    end
end
