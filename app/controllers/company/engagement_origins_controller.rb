class Company::EngagementOriginsController < ApplicationController
  before_action :set_company_engagement_origin, only: [:show, :edit, :update, :destroy]

  # GET /company/engagement_origins
  # GET /company/engagement_origins.json
  def index
    respond_to do |format|
      format.html { @company_engagement_origins = Company::EngagementOrigin.page(params[:page]) }
      format.json { @company_engagement_origins = Company::EngagementOrigin.all }
    end
  end

  # GET /company/engagement_origins/1
  # GET /company/engagement_origins/1.json
  def show
  end

  # GET /company/engagement_origins/new
  def new
    @company_engagement_origin = Company::EngagementOrigin.new
  end

  # GET /company/engagement_origins/1/edit
  def edit
  end

  # POST /company/engagement_origins
  # POST /company/engagement_origins.json
  def create
    @company_engagement_origin = Company::EngagementOrigin.new(company_engagement_origin_params)

    respond_to do |format|
      if @company_engagement_origin.save
        format.html { redirect_to @company_engagement_origin, notice: 'Engagement origin was successfully created.' }
        format.json { render :show, status: :created, location: @company_engagement_origin }
      else
        format.html { render :new }
        format.json { render json: @company_engagement_origin.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /company/engagement_origins/1
  # PATCH/PUT /company/engagement_origins/1.json
  def update
    respond_to do |format|
      if @company_engagement_origin.update(company_engagement_origin_params)
        format.html { redirect_to @company_engagement_origin, notice: 'Engagement origin was successfully updated.' }
        format.json { render :show, status: :ok, location: @company_engagement_origin }
      else
        format.html { render :edit }
        format.json { render json: @company_engagement_origin.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /company/engagement_origins/1
  # DELETE /company/engagement_origins/1.json
  def destroy
    @company_engagement_origin.destroy
    respond_to do |format|
      format.html { redirect_to company_engagement_origins_url, notice: 'Engagement origin was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_company_engagement_origin
      @company_engagement_origin = Company::EngagementOrigin.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def company_engagement_origin_params
      params.require(:company_engagement_origin).permit(:name, :description)
    end
end
