class People::IdentificationDocumentTypesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_people_identification_document_type, only: [:show, :edit, :update, :destroy]

  # GET /people/identification_document_types
  # GET /people/identification_document_types.json
  def index
    if params[:country_id].present?
      @people_identification_document_types = People::IdentificationDocumentType.where(country_id: params[:country_id])
    else
      @people_identification_document_types = People::IdentificationDocumentType.all
    end

    respond_to do |format|
      format.html {
        @people_identification_document_types = @people_identification_document_types.page(params[:page])
      }
      format.json {}
    end
  end

  # GET /people/identification_document_types/1
  # GET /people/identification_document_types/1.json
  def show
  end

  # GET /people/identification_document_types/new
  def new
    @people_identification_document_type = People::IdentificationDocumentType.new
  end

  # GET /people/identification_document_types/1/edit
  def edit
  end

  # POST /people/identification_document_types
  # POST /people/identification_document_types.json
  def create
    @people_identification_document_type = People::IdentificationDocumentType.new(people_identification_document_type_params)

    respond_to do |format|
      if @people_identification_document_type.save
        format.html { redirect_to @people_identification_document_type, notice: 'Identification document type was successfully created.' }
        format.json { render :show, status: :created, location: @people_identification_document_type }
      else
        format.html { render :new }
        format.json { render json: @people_identification_document_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /people/identification_document_types/1
  # PATCH/PUT /people/identification_document_types/1.json
  def update
    respond_to do |format|
      if @people_identification_document_type.update(people_identification_document_type_params)
        format.html { redirect_to @people_identification_document_type, notice: 'Identification document type was successfully updated.' }
        format.json { render :show, status: :ok, location: @people_identification_document_type }
      else
        format.html { render :edit }
        format.json { render json: @people_identification_document_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /people/identification_document_types/1
  # DELETE /people/identification_document_types/1.json
  def destroy
    @people_identification_document_type.destroy
    respond_to do |format|
      format.html { redirect_to people_identification_document_types_url, notice: 'Identification document type was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_people_identification_document_type
      @people_identification_document_type = People::IdentificationDocumentType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def people_identification_document_type_params
      params.require(:people_identification_document_type).permit(
        :name,
        :country_id,
        :validation_regex,
        :validate_uniqueness
      )
    end
end
