class VideoInterview::StagesController < Stage::BasesController
  private
    def authorize_action
      unless has_permission?
        render 'errors/403', status: 403, layout: 'application'
      end
    end

    def has_permission?(stage=nil)
      current_user.can?(:admin, @stage || stage) ||
      current_user.can?(VideoInterview::Stage.to_s.tableize.to_sym, @course) ||
      current_user.can?(:admin, VideoInterview::Stage)
    end
end
