class VideoInterview::QuestionsController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize_action
  before_action :set_video_interview_question, only: [:show, :edit, :update, :destroy]

  # GET /video_interview/questions
  # GET /video_interview/questions.json
  def index
    @video_interview_questions = VideoInterview::Question.page(params[:page])
  end

  # GET /video_interview/questions/1
  # GET /video_interview/questions/1.json
  def show
  end

  # GET /video_interview/questions/new
  def new
    @video_interview_question = VideoInterview::Question.new
  end

  # GET /video_interview/questions/1/edit
  def edit
  end

  # POST /video_interview/questions
  # POST /video_interview/questions.json
  def create
    @video_interview_question = VideoInterview::Question.new(video_interview_question_params)

    respond_to do |format|
      if @video_interview_question.save
        flash[:success] = t("resources.video_interview.question.create_success")
        format.html { redirect_to video_interview_questions_path }
        format.json { render :show, status: :created, location: @video_interview_question }
      else
        format.html { render :new }
        format.json { render json: @video_interview_question.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /video_interview/questions/1
  # PATCH/PUT /video_interview/questions/1.json
  def update
    respond_to do |format|
      if @video_interview_question.update(video_interview_question_params)
        flash[:success] = t("resources.video_interview.question.update_success")
        format.html { redirect_to video_interview_questions_path }
        format.json { render :show, status: :ok, location: @video_interview_question }
      else
        format.html { render :edit }
        format.json { render json: @video_interview_question.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /video_interview/questions/1
  # DELETE /video_interview/questions/1.json
  def destroy
    @video_interview_question.destroy
    respond_to do |format|
      format.html { redirect_to video_interview_questions_url, notice: t("resources.video_interview.question.destroy_success") }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_video_interview_question
      @video_interview_question = VideoInterview::Question.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def video_interview_question_params
      params.require(:video_interview_question).permit(:content)
    end
    
    def authorize_action
      render 'errors/403', status: 403, layout: 'application' unless current_user.can?(:admin)
    end
end
