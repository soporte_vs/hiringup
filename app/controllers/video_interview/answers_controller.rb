class VideoInterview::AnswersController < ApplicationController
  before_action :set_video_interview_answer, only: [:show]
  acts_as_token_authentication_handler_for User

  # GET /video_interview/answers
  # GET /video_interview/answers.json
  def index
    @video_interview_answers = VideoInterview::Answer.all
  end

  # GET /video_interview/answers/1
  # GET /video_interview/answers/1.json
  def show
    if @video_interview_answer.video_state == :uploaded
      redirect_to :root, notice: 'Esta pregunta ya fue respondida.'
    end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_video_interview_answer
      @video_interview_answer = VideoInterview::Answer.find(params[:id])
    end
end
