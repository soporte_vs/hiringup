class CallbacksController < Devise::OmniauthCallbacksController
    def linkedin
      if @user = User.from_omniauth(request.env["omniauth.auth"])
        sign_in_and_redirect @user
      else
        redirect_to new_user_session_path, notice: 'Ya existe un usuario con este email'
      end
    end
end
