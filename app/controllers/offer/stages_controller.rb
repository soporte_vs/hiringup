class Offer::StagesController < Stage::BasesController

  def show
    if @stage.offer_letter
      @offer_letter = @stage.offer_letter
    else
      @offer_letter = @stage.build_offer_letter(
        company_position: @stage.enrollment.course.company_position
      )
    end

    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "file_name"   # Excluding ".pdf" extension.
      end
    end
  end

  def create_letter
    @offer_letter = @stage.build_offer_letter(
      offer_letter_params.merge(company_position: @stage.enrollment.course.company_position)
    )

    if @offer_letter.save
      @offer_letter.send_offer
      @course.create_activity key: "course.offer_letter.created", owner: current_user, recipient: @stage.enrollment
      flash[:success] = t('offer/letter.actions.create_letter.success')
      redirect_to course_offer_stage_path(id: @course.id, stage_id: @stage.id)
    else
      flash[:error] = @offer_letter.errors.messages
      render action: 'show'
    end
  end

  def preview_letter
    @offer_letter = @stage.build_offer_letter(
      offer_letter_params.merge(company_position: @stage.enrollment.course.company_position, created_at: DateTime.now)
    )

    if @offer_letter.valid?
      send_file(@offer_letter.generate_pdf.path, disposition: 'inline')
    else
      render template: '/offer/stages/show'
    end
  end

  def resend_letter
    @offer_letter = @stage.offer_letter

    flash[:success] = t('offer/letter.actions.resend_letter.success')

    # manda el correo
    @offer_letter.send_offer
    redirect_to course_offer_stage_path(id: @course.id, stage_id: @stage.id)
  end

  private
    def catch_stages
      @stages = Offer::Stage.joins(:enrollment).where(enrollment_bases: {course_id: @course.id}, step: params[:step]).joins(enrollment: :course).page(params[:page])
    end

    def authorize_action
      unless has_permission?
        render 'errors/403', status: 403, layout: 'application'
      end
    end

    def has_permission?(stage=nil)
      current_user.can?(:admin, @stage || stage) ||
      current_user.can?(Offer::Stage.to_s.tableize.to_sym, @course) ||
      current_user.can?(:admin, Offer::Stage)
    end

    def offer_letter_params
      params.require(:offer_letter).permit(
        :vacancy_id,
        :direct_boss,
        :net_remuneration,
        :zone_assignation,
        :contract_start_date,
        :address
      )
    end
end
