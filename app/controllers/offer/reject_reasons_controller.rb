class Offer::RejectReasonsController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize_action
  before_action :set_offer_reject_reason, only: [:show, :edit, :update, :destroy]

  # GET /offer/reject_reasons
  # GET /offer/reject_reasons.json
  def index
    @offer_reject_reasons = Offer::RejectReason.page(params[:page])
  end

  # GET /offer/reject_reasons/1
  # GET /offer/reject_reasons/1.json
  def show
  end

  # GET /offer/reject_reasons/new
  def new
    @offer_reject_reason = Offer::RejectReason.new
  end

  # GET /offer/reject_reasons/1/edit
  def edit
  end

  # POST /offer/reject_reasons
  # POST /offer/reject_reasons.json
  def create
    @offer_reject_reason = Offer::RejectReason.new(offer_reject_reason_params)

    respond_to do |format|
      if @offer_reject_reason.save
        format.html { redirect_to @offer_reject_reason, notice: I18n.t('resources.offer.reject_reasons.create_success') }
        format.json { render :show, status: :created, location: @offer_reject_reason }
      else
        flash[:error] = @offer_reject_reason.errors.messages
        format.html { render :new }
        format.json { render json: @offer_reject_reason.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /offer/reject_reasons/1
  # PATCH/PUT /offer/reject_reasons/1.json
  def update
    respond_to do |format|
      if @offer_reject_reason.update(offer_reject_reason_params)
        format.html { redirect_to @offer_reject_reason, notice: I18n.t('resources.offer.reject_reasons.update_success') }
        format.json { render :show, status: :ok, location: @offer_reject_reason }
      else
        flash[:error] = @offer_reject_reason.errors.messages
        format.html { render :edit }
        format.json { render json: @offer_reject_reason.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /offer/reject_reasons/1
  # DELETE /offer/reject_reasons/1.json
  def destroy
    @offer_reject_reason.destroy
    respond_to do |format|
      flash[:success] = I18n.t('resources.offer.reject_reasons.destroy_success')
      format.html { redirect_to offer_reject_reasons_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_offer_reject_reason
      @offer_reject_reason = Offer::RejectReason.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def offer_reject_reason_params
      params.require(:offer_reject_reason).permit(:name, :description)
    end

    def authorize_action
      render 'errors/403', status: 403, layout: 'application' unless current_user.can?(:admin)
    end
end
