class HiringRequest::RecordsController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize_action
  before_action :set_hiring_request_record, except: [:index, :new, :create, :_search]
  before_action :active_required!, only: [:edit, :update]

  def _search
    attrs = params.except(:action, :controller, :record, :_source)
    response_elastic = HiringRequest::Record.query(attrs)
    render json: response_elastic.response
  end


  # GET /hiring_request/records
  # GET /hiring_request/records.json
  def index
    @hiring_request_records = HiringRequest::Record.all.order(aasm_state: :asc, entry_date: :desc).page(params[:page])
  end

  # GET /hiring_request/records/1
  # GET /hiring_request/records/1.json
  def show
  end

  # GET /hiring_request/records/new
  def new
    @hiring_request_record = HiringRequest::Record.new
  end

  # GET /hiring_request/records/1/edit
  def edit
  end

  # POST /hiring_request/records
  # POST /hiring_request/records.json
  def create
    @hiring_request_record = HiringRequest::Record.new(hiring_request_record_params)
    @hiring_request_record.created_by = current_user

    respond_to do |format|
      if @hiring_request_record.save
        flash[:success] = I18n.t('hiring_request/record.actions.created.success')
        format.html { redirect_to @hiring_request_record}
        format.json { render :show, status: :created, location: @hiring_request_record }
      else
        format.html { render :new }
        format.json { render json: @hiring_request_record.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /hiring_request/records/1
  # PATCH/PUT /hiring_request/records/1.json
  def update
    respond_to do |format|
      if @hiring_request_record.update(hiring_request_record_params)
        flash[:success] = I18n.t('hiring_request/record.actions.updated.success')
        format.html { redirect_to @hiring_request_record}
        format.json { render :show, status: :ok, location: @hiring_request_record }
      else
        format.html { render :edit }
        format.json { render json: @hiring_request_record.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /hiring_request/records/1/switch_state
  # PATCH/PUT /hiring_request/records/1/switch_state.json
  def switch_state
    respond_to do |format|
      begin
        if @hiring_request_record.send("#{params[:switch]}!")
          flash[:success] = I18n.t("hiring_request/record.actions.#{params[:switch]}.success")
          format.html { redirect_to @hiring_request_record}
          format.json { render :show, status: :ok, location: @hiring_request_record }
        else
          error_message = t("hiring_request/record.actions.#{params[:switch]}.unsuccess")
          format.html do
              flash[:error] = error_message
              redirect_to hiring_request_record_path(record_id: @hiring_request_record)
            end
          format.json do
            render json: error_message, status: :unprocessable_entity
          end
        end
      rescue Exception => e
        if @hiring_request_record.errors.has_key?(:aasm_state)
          error = @hiring_request_record.errors.messages[:aasm_state].join('; ')
        else
          action = t("hiring_request/record.actions.#{params[:switch]}.text").downcase
          state = t("hiring_request/record.states.#{@hiring_request_record.aasm_state}.default")
          error = t("hiring_request/record.errors.transition", action: action, state: state)
        end
        format.html do
          flash[:error] = error
          redirect_to hiring_request_record_path(record_id: @hiring_request_record)
        end
        format.json { render json: error }
      end
    end
  end

  # # DELETE /hiring_request/records/1
  # # DELETE /hiring_request/records/1.json
  # def destroy
  #   @hiring_request_record.destroy
  #   respond_to do |format|
  #     flash[:success] = I18n.t('hiring_request/record.actions.destroyed.success')
  #     format.html { redirect_to hiring_request_records_url}
  #     format.json { head :no_content }
  #   end
  # end

  private
    def active_required!
      unless @hiring_request_record.active?
        state = t("hiring_request/record.states.#{@hiring_request_record.aasm_state}.default")
        flash[:notice] = t("hiring_request/record.notices.edit.blocked", state: state)
        redirect_to @hiring_request_record
      end
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_hiring_request_record
      @hiring_request_record = HiringRequest::Record.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def hiring_request_record_params
      arguments = params.require(:hiring_request_record).permit(
        :company_positions_cenco_id,
        :request_date,
        :revenue_expected,
        :workplace,
        :company_vacancy_request_reason_id,
        :company_contract_type_id,
        :company_estate_id,
        :company_timetable_id,
        :entry_date,
        :observations,
        :number_vacancies,
        :required_by_name,
        :required_by_email,
        applicant_attributes: [
          user_attributes: [:email]
        ]
      )
      if arguments.present? && arguments[:applicant_attributes].present?
        arguments[:applicant_attributes][:type] = Applicant::Catched.to_s
      end
      return arguments
    end

    def authorize_action
      unless current_user.can?(:admin, HiringRequest::Record)
        render 'errors/403', status: 403, layout: 'application'
      end
    end
end
