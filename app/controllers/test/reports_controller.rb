class Test::ReportsController < Test::StagesController
  def download
    stage_document = @stage.test_report.documents.find(params[:stage_document_id])
    send_file stage_document.document.path, filename: stage_document.document.filename
  end

  def create
    @test_report = Test::Report.new(report_params)
    @test_report.documents.each{ |doc| doc.resource = @test_report }

    if @test_report.save
      flash[:success] = I18n.t('test.report.created.success')
      redirect_to course_test_stage_path(id: @course, stage_id: @stage.id)
    else
      render template: 'test/stages/show'
    end
  end

  private
    def report_params
      attrs = params.require(:test_report).permit(:observations)
      attrs[:documents] = []
      document_attrs = params[:test_report][:documents] || []
      document_attrs.each do |document_val|
        attrs[:documents] << Stage::Document.new(document: document_val)
      end
      attrs.merge(test_stage_id: @stage.id)
    end
end
