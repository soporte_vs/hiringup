class Document::DescriptorsController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize_action
  before_action :set_document_descriptor, only: [:show, :edit, :update, :destroy]

  # GET /document/descriptors
  # GET /document/descriptors.json
  def index
    respond_to do |format|
      format.html { @document_descriptors = Document::Descriptor.page(params[:page]) }
      format.json { @document_descriptors = Document::Descriptor.all }
    end
  end

  # GET /document/descriptors/1
  # GET /document/descriptors/1.json
  def show
  end

  # GET /document/descriptors/new
  def new
    @document_descriptor = Document::Descriptor.new
  end

  # GET /document/descriptors/1/edit
  def edit
  end

  # POST /document/descriptors
  # POST /document/descriptors.json
  def create
    @document_descriptor = Document::Descriptor.new(document_descriptor_params)
    respond_to do |format|
      if @document_descriptor.save
        flash[:success] = t("resources.document.descriptor.create_success")
        format.html { redirect_to @document_descriptor }
        format.json { render :show, status: :created, location: @document_descriptor }
      else
        flash.now[:error] = @document_descriptor.errors.messages
        format.html { render :new }
        format.json { render json: @document_descriptor.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /document/descriptors/1
  # PATCH/PUT /document/descriptors/1.json
  def update
    respond_to do |format|
      if @document_descriptor.update(document_descriptor_params)
        flash[:success] = t("resources.document.descriptor.update_success")
        format.html { redirect_to @document_descriptor }
        format.json { render :show, status: :ok, location: @document_descriptor }
      else
        flash.now[:error] = @document_descriptor.errors.messages
        format.html { render :edit }
        format.json { render json: @document_descriptor.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /document/descriptors/1
  # DELETE /document/descriptors/1.json
  def destroy
    if @document_descriptor.destroy
      flash[:success] = t("resources.document.descriptor.destroy_success")
    else
      flash[:error] = @document_descriptor.errors.messages
    end

    respond_to do |format|
      format.html {redirect_to document_descriptors_url}
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_document_descriptor
      @document_descriptor = Document::Descriptor.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def document_descriptor_params
      params.require(:document_descriptor).permit(
        :name,
        :details,
        :allowed_extensions,
        :internal,
        :only_onboarding,
        :attachment_attribute
      )
    end

    def authorize_action
      render 'errors/403', status: 403, layout: 'application' unless current_user.can?(:admin)
    end
end
