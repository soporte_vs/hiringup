class Document::RecordsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_document_record, only: [:show, :edit, :update, :destroy]
  before_action :validate_destroy, only: [:destroy]


  # GET /document/records
  # GET /document/records.json
  def index
    @document_records = Document::Record.all
  end

  # GET /document/records/1
  # GET /document/records/1.json
  def show
  end

  # GET /document/records/new
  def new
    @document_record = Document::Record.new
  end

  # GET /document/records/1/edit
  def edit
  end

  # POST /document/records
  # POST /document/records.json
  def create
    @document_record = Document::Record.new(document_record_params)

    respond_to do |format|
      if @document_record.save
        format.html { redirect_to @document_record, notice: 'Record was successfully created.' }
        format.json { render :show, status: :created, location: @document_record }
      else
        format.html { render :new }
        format.json { render json: @document_record.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /document/records/1
  # PATCH/PUT /document/records/1.json
  def update
    respond_to do |format|
      if @document_record.update(document_record_params)
        format.html { redirect_to @document_record, notice: 'Record was successfully updated.' }
        format.json { render :show, status: :ok, location: @document_record }
      else
        format.html { render :edit }
        format.json { render json: @document_record.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /document/records/1
  # DELETE /document/records/1.json
  def destroy
    respond_to do |format|
      format.html { redirect_to document_records_url, notice: 'Record was successfully destroyed.' }
      format.json {
        if @document_record.destroy
          render json: { status: :ok, document_record: @document_record }
        else
          render json: { errors: @document_record.errors.messages, document_record: @document_record }
        end
      }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_document_record
      @document_record = Document::Record.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def document_record_params
      params.require(:document_record).permit(:user_id, :document_descriptor_id, :document)
    end

    def validate_destroy
      render json: { error: 'Error 403' } , status: 403 unless current_user.can?(:admin) || current_user.id==@document_record.applicant.user.id
    end
end
