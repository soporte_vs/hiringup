class Document::GroupsController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize_action
  before_action :set_document_group, only: [:show, :edit, :update, :destroy]

  # GET /document/groups
  # GET /document/groups.json
  def index
    respond_to do |format|
      format.html { @document_groups = Document::Group.page(params[:page]) }
      format.json { @document_groups = Document::Group.all }
    end
  end

  # GET /document/groups/1
  # GET /document/groups/1.json
  def show
  end

  # GET /document/groups/new
  def new
    @document_group = Document::Group.new
  end

  # GET /document/groups/1/edit
  def edit
  end

  # POST /document/groups
  # POST /document/groups.json
  def create
    @document_group = Document::Group.new(document_group_params)

    respond_to do |format|
      if @document_group.save
        flash[:success] = t("resources.document.group.create_success")
        format.html { redirect_to @document_group }
        format.json { render :show, status: :created, location: @document_group }
      else
        flash.now[:error] = @document_group.errors.messages
        format.html { render :new }
        format.json { render json: @document_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /document/groups/1
  # PATCH/PUT /document/groups/1.json
  def update
    respond_to do |format|
      # Al eliminar descriptores, afecto la actualizacion en document_descriptor_memberships_attributes debido
      # a que algunos memberships son eliminados.
      # Tenia dos opciones, una era consultar a la DB por los memberships eliminados desde document_descriptor_ids y filtrar 
      # document_descriptor_memberships_attributes y la otra era dividir este update en dos etapas (mentalmente mas economico y tambien con menos consultas)
      group_params = document_group_params
      memberships = group_params['document_descriptor_memberships_attributes']
      group_params.delete('document_descriptor_memberships_attributes')
      
      if @document_group.update(group_params)
        group_params['document_descriptor_memberships_attributes'] = memberships
        begin
          @document_group.update(group_params)
        rescue
          # who cares
        end
        
        flash[:success] = t("resources.document.group.update_success")
        format.html { redirect_to @document_group }
        format.json { render :show, status: :ok, location: @document_group }
      else
        flash.now[:error] = @document_group.errors.messages
        format.html { render :edit }
        format.json { render json: @document_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /document/groups/1
  # DELETE /document/groups/1.json
  def destroy
    @document_group.destroy
    respond_to do |format|
      flash[:success] = t("resources.document.group.destroy_success")
      format.html { redirect_to document_groups_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_document_group
      @document_group = Document::Group.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def document_group_params
      params.require(:document_group).permit(:name, :document_descriptor_ids => [], :document_descriptor_memberships_attributes => [:id, :document_required])
    end

    def authorize_action
      render 'errors/403', status: 403, layout: 'application' unless current_user.can?(:admin)
    end
end
