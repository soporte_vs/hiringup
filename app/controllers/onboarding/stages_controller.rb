class Onboarding::StagesController < Stage::BasesController
  include ApplicantDocumentActions
  layout 'stage'
  before_action :authenticate_user!, except: [:upload_requested_documents, :do_upload_requested_documents]
  before_action :reject_postulant, except: [:upload_requested_documents, :do_upload_requested_documents]
  before_action :authorize_action, except: [:upload_requested_documents, :do_upload_requested_documents]
  before_action :check_token, only: [:upload_requested_documents, :do_upload_requested_documents]

  def show
  end

  def upload_requested_documents
    @applicant = @stage.enrollment.applicant
  end
  #este metodo funciona para los usuarios de la plataforma
  # si no esta logueado te manda al formulario de logueo
  def do_upload_document
    respond_to do |format|
      format.html do
        do_upload_documents
        redirect_to course_onboarding_stage_path(id: @stage.enrollment.course.id, stage_id: @stage.to_param)
      end

      format.json do
        @document_records = do_upload_documents_ajax
        render template: 'document/records/index'
      end
    end
  end

  # upload_documents por defecto redirecciona a :back
  #Este metodo es ocupado para que el candidato pueda subir los documentos mediante un token de acceso
  #si no viene el token, manda error 403
  #Este metodo no se funciono con do_upload_document para que cuando ingrese sin el token
  # envie error 403 y no redireccione al inicio de sesión
  def do_upload_requested_documents
    respond_to do |format|
      format.html do
        do_upload_documents
        redirect_to upload_requested_documents_course_onboarding_stage_path(id: @stage.enrollment.course.id, stage_id: @stage.to_param, token: @token)
      end

      format.json do
        @document_records = do_upload_documents_ajax
        render template: 'document/records/index'
      end
    end
  end

  def documents_request
    document_descriptors = Document::Descriptor.where(name: params[:document_descriptors])
    respond_to do |format|
      format.json do
        if document_descriptors.any?
          Onboarding::StageMailer.delay.documents_request(@stage, document_descriptors, params[:message])
          render json: {success: t("onboarding/stage.messages.documents_request.success")}
        else
          render json: {error: t("onboarding/stage.messages.documents_request.unsuccess")}
        end
      end
    end
  end

  private

    def catch_stage
      begin
        @stage = Onboarding::Stage.find(params[:stage_id])

        # enrollment es necesario para ApplicantDocumentActions
        @enrollment = @stage.enrollment

        @applicant = @enrollment.applicant
        raise "" if @stage.enrollment.course.id != @course.id
      rescue Exception => e
        render 'errors/404', status: 404, layout: 'application'
      end
    end

    def authorize_action
      if !(current_user.can?(params[:action], @stage || Onboarding::Stage) ||
           current_user.can?(Onboarding::Stage.to_s.tableize.to_sym, @course) ||
           current_user.can?(:admin, Onboarding::Stage))
        render 'errors/403', status: 403, layout: 'application'
      end
    end

    def check_token
      received_token = params[:token]
      token = Digest::MD5.new
      token << @stage.enrollment.applicant.email
      @token = "#{token}-#{@stage.enrollment.id}"
      unless received_token == @token
        render 'errors/403', status: 403, layout: 'application'
      end
    end
end
