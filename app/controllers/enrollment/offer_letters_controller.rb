# coding: utf-8
class Enrollment::OfferLettersController < ApplicationController
  before_action :authenticate_user!, except: [:accept, :show_reject, :reject]

  before_action :catch_enrollment, if: "params[:id]"
  before_action :find_offer_letter

  def accept
    return render template: 'enrollment/offer_letters/already_rejected' if @offer_letter.accepted == false
    unless @offer_letter.mark_accepted
      render 'errors/403', status: 403, layout: 'application'
    end
  end

  def show_reject
    return render template: 'enrollment/offer_letters/already_accepted' if @offer_letter.accepted == true
    return render template: 'enrollment/offer_letters/already_rejected' if @offer_letter.accepted == false
  end

  def reject
    return render template: 'enrollment/offer_letters/already_accepted' if @offer_letter.accepted == true
    return render template: 'enrollment/offer_letters/already_rejected' if @offer_letter.accepted == false
    @offer_letter.mark_rejected(reject_reason_params[:offer_reject_reason_id]) if @offer_letter.accepted.nil?
  end

  private
    def find_offer_letter
      @offer_letter = Offer::Letter.find_by(id: params[:offer_letter_id], token: params[:token])
      render 'errors/404', status: 404, layout: 'application' unless @offer_letter
    end

    def catch_enrollment
      @enrollment = Enrollment::Base.find(params[:id])
    end

    def reject_reason_params
      params.require(:offer_letter).permit(:offer_reject_reason_id)
    end
end
