class Enrollment::BasesController < ApplicationController
  helper IntegraSpreadsheet::ReportHelper
  before_action :authenticate_user!
  before_action :catch_enrollment, if: "params[:id]"
  before_action :catch_enrollments, if: "params[:ids]"
  before_action :authorize_mark_invitation, only: [:mark_invitation_accepted, :mark_invitation_rejected]
  before_action :authorize_revert_dispose, only: [:revert_dispose]
  before_action :authorize_export, only: [:export]
  before_action :authorize_unenroll, only: [:unenroll]

  def dispose
    @discard_reason = Enrollment::DiscardReason.find_by(id: params[:discard_reason_id])
    @observations = params[:observations]
    send_discarding_email = (params[:send_discarding_email] == false || params[:send_discarding_email] == 'false') ? false : true

    @enrollments.each do |enrollment|
      if !current_user.can?(:discard_enrollment, enrollment.course) && \
         !current_user.can?(:discard, enrollment)
         enrollment.errors.add(:discarding, :enough_permission)
      elsif enrollment.discard(@observations, @discard_reason)
        Enrollment::DiscardingMailer.delay.discard(enrollment) if send_discarding_email
        enrollment.course.create_activity key: 'course.discarded', owner: current_user, recipient: enrollment
      end
    end

    respond_to do |format|
      format.json do
        render json: @enrollments.map{ |enrollment| enrollment.as_indexed_json }
      end
    end
  end

  def revert_dispose
    @enrollments.each do |enrollment|
      enrollment.revert_discard
    end

    respond_to do |format|
      format.json do
        render json: @enrollments.map{ |enrollment| enrollment.as_indexed_json }
      end
    end
  end

  def unenroll
    @enrollments.each do |enrollment|
      #Se borra el enrollment del arreglo una vez que se ha eliminado con el metodo unenroll
      #con la finalidad de no enviarlo por json y evitar que as_indexed_json haga map de un elemento que ya no existe
     @enrollments -= [enrollment] if enrollment.unenroll
    end

    respond_to do |format|
      format.json do
        render json: @enrollments.map{ |enrollment| enrollment.as_indexed_json }
      end
    end
  end

  def next_stage
    type_stage, name_stage = params[:type_stage], params[:name_stage]
    args = params[:args].present? ? params[:args] : {}

    args[:current_user] = current_user

    enrollments_target = []
    @enrollments.each do |enrollment|
      if can_next_stage?(enrollment)
        enrollments_target << enrollment
        @enrollments -= [enrollment]
      else
        enrollment.errors.add(:stage, :next_stage_unuthorized)
      end
    end

    #Obtenemos los enrollments que pueden pasar de etapa
    enrollments = Enrollment::Base.where(id: enrollments_target.map(&:id))
    # los enrollments que pueden pasar de etapa, los pasamos al metodo next_stage
    enrollments.next_stage(type_stage, name_stage, args)

    @enrollments += enrollments

    respond_to do |format|
      format.json do
        render json: @enrollments.map{ |enrollment| enrollment.as_indexed_json }
      end
    end
  end

  def invite
    @enrollments.each do |enrollment|
      if !current_user.can?(:invite_enrollment, enrollment.course) && \
         !current_user.can?(:invite, enrollment)
        enrollment.errors.add(:course_invitation, :enough_permission)
      elsif enrollment.invite
        Enrollment::CourseInvitationMailer.delay.send_course_invitation(enrollment)
        enrollment.course.create_activity key: 'course.invited', owner: current_user, recipient: enrollment
      end
    end

    respond_to do |format|
      format.json do
        render json: @enrollments.map{ |enrollment| enrollment.as_indexed_json }
      end
    end
  end

  def export
    @columns = ProcessTracking::Report.columns
    @course = @enrollments.first.course
    render xlsx: "export.xlsx", layout: false, filename: "#{@course.id} - Candidatos.xlsx"
  end

  def mark_invitation_accepted
    respond_to do |format|
      if @enrollment.mark_invitation_accepted
        format.html {
          flash[:success] = I18n.t("enrollment/base.course_invitation.accepted")
          render 'enrollment/course_invitations/accepted'
        }
        format.json {
          render json: {
                   enrollment: @enrollment,
                   course_invitation: @enrollment.course_invitation
                 }
        }
      else
        format.html {
          flash[:error] = @enrollment.errors
          # redirect_to edit_profiles_path
          render 'errors/403', status: 403, layout: 'application'
        }
        format.json { render json: @enrollment.errors, status: :unprocessable_entity }
      end
    end
  end

  def mark_invitation_rejected
    respond_to do |format|
      if @enrollment.mark_invitation_rejected
        format.html {
          flash[:success] = I18n.t("enrollment/base.course_invitation.rejected")
          render 'enrollment/course_invitations/rejected'
        }
        format.json {
          render json: {
                   enrollment: @enrollment,
                   course_invitation: @enrollment.course_invitation
                 }
        }
      else
        format.html {
          flash[:error] = I18n.t("activerecord.errors.models.enrollment/base.attributes.course_invitation.not_invited")
          # redirect_to edit_profiles_path
          render 'errors/403', status: 403, layout: 'application'
        }
        format.json { render json: @enrollment.errors, status: :unprocessable_entity }
      end
    end
  end



  private
    def catch_enrollment
      @enrollment = Enrollment::Base.find(params[:id])
    end

    def catch_enrollments
      @enrollments = Enrollment::Base.where(id: params[:ids])
    end

    def can_next_stage?(enrollment)
      if enrollment.invitable?
        return current_user.can?(:invite_enrollment, enrollment.course) ||
               current_user.can?(:invite, enrollment)
      else
        return current_user.can?(:admin, enrollment.stage) ||
               current_user.can?(:admin, enrollment.course) ||
               current_user.can?(enrollment.stage.class.to_s.tableize.to_sym, enrollment.course)
      end
    end

    def authorize_mark_invitation
      unless current_user == @enrollment.applicant.user
        render 'errors/403', status: 403, layout: 'application'
      end
    end

    def authorize_revert_dispose
      unless current_user.can?(:admin)
        render 'errors/403', status: 403, layout: 'application'
      end
    end

    def authorize_export
      unless current_user.can?(:admin) || current_user.can?(:admin, @enrollments.first.course)
        render 'errors/403', status: 403, layout: 'application'
      end
    end

    def authorize_unenroll
      unless current_user.can?(:admin)
        render 'errors/403', status: 403, layout: 'application'
      end
    end
end
