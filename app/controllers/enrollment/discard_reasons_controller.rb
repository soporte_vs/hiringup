class Enrollment::DiscardReasonsController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize_action, except: [:index]
  before_action :set_enrollment_discard_reason, only: [:show, :edit, :update, :destroy, :set_as_default_when_close_course, :toggle_blacklist]

  respond_to :html

  def index
    respond_to do |format|
      format.html { @enrollment_discard_reasons = Enrollment::DiscardReason.page(params[:page]) }
      format.json { @enrollment_discard_reasons = Enrollment::DiscardReason.all }
    end
  end

  def show
    respond_with(@enrollment_discard_reason)
  end

  def new
    @enrollment_discard_reason = Enrollment::DiscardReason.new
    respond_with(@enrollment_discard_reason)
  end

  def edit

  end

  def create
    @enrollment_discard_reason = Enrollment::DiscardReason.new(enrollment_discard_reason_params)
    if @enrollment_discard_reason.save
      flash[:success] = t("resources.discard_reasons.create_success")
    else
      flash[:error] = @enrollment_discard_reason.errors.messages
    end
    respond_with(@enrollment_discard_reason)
  end

  def update
    if @enrollment_discard_reason.update(enrollment_discard_reason_params)
      flash[:success] = t("resources.discard_reasons.update_success")
    else
      flash[:error] = @enrollment_discard_reason.errors.messages
    end
    respond_with(@enrollment_discard_reason)
  end

  def destroy
    if @enrollment_discard_reason.destroy
      flash[:success] = t("resources.discard_reasons.destroy_success")
    else
      flash[:error] = @enrollment_discard_reason.errors.messages
    end
    redirect_to :back
  end

  def set_as_default_when_close_course
    trigger = Trigger::CloseCourseDiscardReason.new
    trigger.resourceable = @enrollment_discard_reason
    trigger.save

    redirect_to :back
  end

  def toggle_blacklist
    trigger = Trigger::BlacklistDiscardReason.find_by(resourceable: @enrollment_discard_reason)
    if trigger.nil?
      trigger = Trigger::BlacklistDiscardReason.create(resourceable: @enrollment_discard_reason)
    else
      trigger.destroy
    end
    redirect_to :back
  end

  private
    def authorize_action
      render 'errors/403', status: 403, layout: 'application' unless current_user.can?(params[:action].to_sym, Enrollment::DiscardReason)
    end

    def set_enrollment_discard_reason
      @enrollment_discard_reason = Enrollment::DiscardReason.find(params[:id])
    end

    def enrollment_discard_reason_params
      params.require(:enrollment_discard_reason).permit(:name, :description, :mailer_content, :subject)
    end
end
