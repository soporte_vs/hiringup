class TechnicalTest::StagesController < Stage::BasesController
  def show
    @technical_test_report = TechnicalTest::Report.new(technical_test_stage: @stage)
  end

  private
    def catch_stages
      @stages = TechnicalTest::Stage.joins(:enrollment).where(enrollment_bases: {course_id: @course.id}).joins(enrollment: :course).page(params[:page])
    end

    def authorize_action
      unless has_permission?
        render 'errors/403', status: 403, layout: 'application'
      end
    end

    def has_permission?(stage=nil)
      current_user.can?(:admin, stage || @stage) ||
      current_user.can?(TechnicalTest::Stage.to_s.tableize.to_sym, @course) ||
      current_user.can?(:admin, TechnicalTest::Stage)
    end
end
