class TechnicalTest::ReportsController < TechnicalTest::StagesController

  def download
    stage_document = @stage.technical_test_report.documents.find(params[:stage_document_id])
    send_file stage_document.document.path, filename: stage_document.document.filename
  end

  def create
    @technical_test_report = TechnicalTest::Report.new(report_params)
    @technical_test_report.documents.each{ |doc| doc.resource = @technical_test_report }


    respond_to do |format|
      if @technical_test_report.save
        if @technical_test_report.notified_to.present?
          TechnicalTest::ReportMailer.delay.send_report(@technical_test_report)
        end
        flash[:success] = I18n.t('technical_test.report.created.success')
        format.html { redirect_to course_technical_test_stage_path(id: @course, stage_id: @stage.id) }
      else
        flash[:error] = @technical_test_report.errors.messages
        format.html { render 'technical_test/stages/show' }
      end
    end
  end

  private

    def report_params
      attrs = params.require(:technical_test_report).permit(:observations, :notified_to)
      attrs[:documents] = []
      document_attrs = params[:technical_test_report][:documents] || []
      document_attrs.each do |document_val|
        attrs[:documents] << Stage::Document.new(document: document_val)
      end
      attrs.merge(technical_test_stage_id: @stage.id)
    end
end
