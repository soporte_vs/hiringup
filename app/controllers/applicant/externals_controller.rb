class Applicant::ExternalsController < ApplicationController
  layout 'devise'
  before_action { redirect_to "/" if user_signed_in? }

  def new
    @rut_error = []
    @applicant_external = Applicant::External.new(user: User.new)
  end

  def create
    rut = applicant_external_params[:user_attributes][:rut]
    rut = rut.gsub(".", "").gsub("-", "").gsub("K", "k") if rut
    rut_exists = People::PersonalInformation.where(identification_document_number: "#{rut[0..rut.length-2]}-#{rut[rut.length-1]}").any?
    @rut_error = ["Rut ya está asociado a otra cuenta existente"] if rut_exists
    @rut_error = ["Rut invalido"] unless rut_valid? rut
    @applicant_external = Applicant::External.new
    @applicant_external.build_user
    @applicant_external.attributes = {user_attributes: applicant_external_params[:user_attributes].except!(:rut)}
    if @rut_error
      render :new
    else
      if @applicant_external.save
        sign_in @applicant_external.user
        redirect_to edit_profiles_path(identification_document_number: "#{rut[0..rut.length-2]}-#{rut[rut.length-1]}")
      else
        render :new
      end
    end
  end

  private
    def applicant_external_params
      params.require(:applicant_external).permit(user_attributes: [:email, :password, :rut])
    end

    def rut_valid? rut
      rut = rut.strip.gsub('K', 'k')
      _rut = rut[0..rut.length-2].to_i
      dv = rut[rut.length-1]
      v = 1
      sum = 0
      for i in (2..9)
        i == 8 ? v = 2 : v += 1
        sum += v * (_rut % 10)
        _rut /= 10
      end
      sum = 11 - sum % 11
      valid_rut = false
      if sum == 11
        valid_rut = '0' == dv
      elsif sum == 10
        valid_rut = 'k' == dv
      else
        valid_rut = sum.to_s == dv
      end
      valid_rut
    end

end