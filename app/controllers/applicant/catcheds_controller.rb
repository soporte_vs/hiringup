class Applicant::CatchedsController < Applicant::BasesController
  before_action :authenticate_user!
  before_action :authorize_action
  before_action :catch_applicant, if: "params[:id].present?"

  def new
    @applicant_catched = Applicant::Catched.new(
      user: User.new(
        personal_information: People::PersonalInformation.new,
        professional_information: People::ProfessionalInformation.new
      )
    )
  end

  def create
    @applicant_catched = Applicant::Catched.new(applicant_catched_params)
    if @applicant_catched.save
      flash[:success] = t('applicant.action.created')
      redirect_to applicant_basis_path(id: @applicant_catched.id)
    else
      respond_to do |format|
        flash[:error] = @applicant_catched.errors.messages
        format.html { render action: 'new' }
      end
    end
  end

  private
    def catch_applicant
      @applicant_catched = Applicant::Catched.find_by(id: params[:id])
      render 'errors/404', status: 404, layout: 'application' unless @applicant_catched
    end

    def authorize_action
      render 'errors/403', status: 403, layout: 'application' unless current_user.can?(params[:action].to_sym, Applicant::Catched)
    end

    def applicant_catched_params
      params.require(:applicant_catched).permit(
        :company_recruitment_source_id,
        user_attributes: [
          :email,
           professional_information_attributes: [
            :id,
            degrees_attributes: [
              :id,
              :name,
              :career_id,
              :type,
              :institution_id,
              :culmination_date,
              :condition,
              :_destroy
            ],
            laboral_experiences_attributes: [
              :id,
              :position,
              :company,
              :description,
              :since_date,
              :until_date,
              :_destroy
            ],
            language_skills_attributes: [
              :id,
              :education_language_id,
              :level,
              :_destroy
            ]
          ],
          personal_information_attributes: [
            :id,
            :rut,
            :first_name,
            :last_name,
            :birth_date,
            :landline,
            :cellphone,
            :sex,
            :territory_city_id,
            :avatar,
            :address,
            :nationality_id,
            :company_marital_status_id,
            :identification_document_number,
            :identification_document_type_id,
            :availability_replacements,
            :availability_work_other_residence,
            personal_referrals_attributes: [
              :id,
              :full_name,
              :email,
              :phone,
              :_destroy
            ]
          ]
        ]
      )
    end
end
