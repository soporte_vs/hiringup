class Applicant::BasesController < ApplicationController
  include ApplicantDocumentActions

  before_action :set_course_context, if: "params[:course_id].present?"
  before_action :authenticate_user!
  before_action :catch_aplicant, if: "params[:id].present?"
  before_action :catch_applicants, if: "params[:ids]"
  before_action :authorize_read, only: [:show, :search]
  before_action :authorize_update, only: [:edit, :update, :upload_documents]

  before_action :catch_course, only: [:enroll]
  before_action :authorize_enroll, only: [:enroll]
  before_action :authorize_send_update_request, only: [:send_update_request]
  before_action :authorize_massive_mailer, only: [:massive_mailer]
  before_action :authorize_update_password, only: [:update_password]
  before_action :authorize_generate_cv, only: [:generate_cv, :generate_massive_cvs]

  around_action :set_document_records, only: [:edit, :update, :show]

  def update
    respond_to do |format|
      if @applicant.update(applicant_params)
        flash[:success] = I18n.t('applicant.action.updated')
        format.html { redirect_to applicant_basis_path(@applicant) }
        format.json { render :show, status: :ok, location: @applicant }
      else
        flash.now[:error] = @applicant.errors.messages
        format.html { render :edit }
        format.json { render json: @applicant.errors, status: :unprocessable_entity }
      end
    end
  end

  def notification_update_profile
    respond_to do |format|
      @applicant.notification_update_profile
      format.json { head :no_content }
    end
  end

  def edit
    flash.now[:notice] = t("helper_text")
    if !@applicant.user.personal_information
      personal_information = People::PersonalInformation.new(user: @applicant.user)
      # Se saltan las validaciones debido a que es necesario crear una instancia de personal_information
      # asociada al User del applicant para que este pueda editar dichos datos. De lo contrario no se muestra
      # el formulario al acceder al perfil de dicho usuario. (Debido a que USER no tiene un atributo
      # que se llame personal_information, es necesario persistirlo referenciando al usuario al cual pertenece).
      personal_information.save(validate: false)
    end
    if !@applicant.user.professional_information
      People::ProfessionalInformation.create(user: @applicant.user)
    end
    @applicant.reload
  end

  def enroll
    respond_to do |format|
      format.html do
        if @course.enroll(@applicant)
          @course.create_activity key: "course.enroll", owner: current_user, recipient: @applicant
          flash.now[:success] = I18n.t("applicant.base.enroll.success")
        else
          flash.now[:error] = I18n.t("applicant.base.enroll.errors.already_enrolled")
        end
        renders_params
        render action: 'show'
      end

      format.json do
        applicants = Applicant::Base.where(id: params[:applicants_ids])
        applicants.each do |applicant|
          if @course.enroll(applicant)
            @course.create_activity key: "course.enroll", owner: current_user, recipient: applicant
          else
            applicant.errors.add(:enroll, I18n.t("applicant.base.enroll.errors.already_enrolled", applicant_name: applicant.name_or_email))
          end
        end
        render json: applicants.map{ |applicant| applicant.as_indexed_json }
      end
    end
  end

  def show
    renders_params
  end

  def _search
    attrs = params.except(:action, :controller, :basis, :_source)
    attrs[:sort].unshift({"_score": "desc"}) if attrs[:sort].present?
    response_elastic = Applicant::Base.query(attrs)
    render json: response_elastic.response
  end

  def search
    response_elastic = Applicant::Base.query(build_elastic_query).page(params[:page])
    @applicants = response_elastic.records
    @aggregations = response_elastic.response.aggregations
    #Objetos a CSV
    respond_to do |format|
      format.html
      format.xls { send_data @applicants.to_csv(@applicants).encode("iso-8859-1").force_encoding("utf-8"), filename: "Excel-#{Date.today}.xls"
      }
      format.json do
        applicants = Applicant::Base.where(id: params[:ids])
        render json: applicants.map{ |applicant| applicant.as_indexed_json }
      end
    end
  end

  # upload_documents por defecto redirecciona a :back
  def upload_documents
    respond_to do |format|
      format.html do
        do_upload_documents
        redirect_to applicant_basis_path(@applicant)
      end

      format.json do
        @document_records = do_upload_documents_ajax
        render template: 'document/records/index'
      end
    end
  end

  # create comment
  def comment
    @applicant = Applicant::Base.find_by(id: params[:id])
    @comment = @applicant.comments.new(comment_params)
    if @comment.save
      flash[:success] = t('comment.create_success')
    else
      flash[:error] = @comment.errors.messages
    end
    redirect_to applicant_basis_path(@applicant)

  end

  #Send massive mail
  def massive_mailer
    respond_to do |format|
      format.json do
        applicants = Applicant::Base.where(id: params[:applicants_ids])
        applicants.each do |applicant|
          Applicant::BaseMailer.delay.massive_mailer(applicant, current_user.email, params[:subject], params[:message])
        end
        render json: applicants.map{ |applicant| applicant.as_indexed_json }
      end
    end
  end

  # send email requesting info update
  def send_update_request
    respond_to do |format|
      format.json do
        applicants = Applicant::Base.where(id: params[:applicants_ids])
        applicants.each do |applicant|
          if Applicant::BaseMailer.delay.send_update_request(applicant)
            applicant.update_info_requests.create
          else
            applicant.errors.add(:update, I18n.t("applicant.base.send_update_request.errors.not_sent", applicant_name: applicant.name_or_email))
          end
        end
        render json: applicants.map{ |applicant| applicant.as_indexed_json }
      end
    end
  end

  def update_password
    if !@applicant.user.update(password: params[:password], password_confirmation: params[:password_confirmation])
      flash[:error] = @applicant.user.errors
    else
      flash[:success] = "Contraseña actualizada correctamente"
    end
    redirect_to applicant_basis_path(@applicant)
  end

  def generate_cv
    send_file(Applicant::Base.generate_pdf(@applicant).path, disposition: 'inline')
  end

  def generate_massive_cvs
    respond_to do |format|
      format.json do
        file_names = []
        zipfile_name = "cvs_exportados_" + Time.now.strftime("%m_%d_%Y_%H_%M_%S") +".zip"
        temp_file = Tempfile.new(zipfile_name)
        Zip::OutputStream.open(temp_file) { |zos| }
        @applicants.each do |applicant|
          Applicant::Base.generate_pdf(applicant).path
          file_names << "cv-#{applicant.id}-#{applicant.full_name}.pdf"
        end
        Zip::File.open(temp_file.path, Zip::File::CREATE) do |zipfile|
          file_names.each do |filename|
            zipfile.add(filename, "#{Rails.root}/tmp/#{filename}")
          end
        end
        send_file(temp_file, disposition: 'inline', filename: zipfile_name)
      end
    end
  end

  private
    def set_document_records
      @document_records = []
      Document::Descriptor.all.each do |descriptor|
        record = @applicant.get_latest_document_record(descriptor.name, nil)
        if record.nil?
          record = Document::Record.new(applicant_id: @applicant.id, document_descriptor: descriptor)
        end
        @document_records << record
      end
      yield
    end

    def applicant_params
      params.require(:applicant).permit(:blacklisted,
      :company_recruitment_source_id,
      user_attributes: [
        :id,
        :email,
        professional_information_attributes: [
          :id,
          degrees_attributes: [
            :id,
            :type,
            :name,
            :career_id,
            :institution_id,
            :culmination_date,
            :condition,
            :_destroy
          ],
          laboral_experiences_attributes: [
            :id,
            :position,
            :description,
            :company,
            :since_date,
            :until_date,
            :_destroy
          ],
          language_skills_attributes: [
            :id,
            :education_language_id,
            :level,
            :_destroy
          ],
          software_skills_attributes: [
            :id,
            :education_software_id,
            :level,
            :_destroy
          ]
        ],
        personal_information_attributes: [
          :id,
          :rut,
          :first_name,
          :last_name,
          :birth_date,
          :landline,
          :cellphone,
          :nationality_id,
          :company_marital_status_id,
          :study_type,
          :recruitment_source_id,
          :areas_of_interest,
          :sex,
          :study_type,
          :territory_city_id,
          :avatar,
          :address,
          :identification_document_number,
          :identification_document_type_id,
          :availability_replacements,
          :availability_work_other_residence,
          personal_referrals_attributes: [
            :id,
            :full_name,
            :email,
            :phone,
            :_destroy
          ]
        ]

        ])
    end

    def comment_params
      params.require(:comment).permit(:content).merge(user_id: current_user.id)
    end

    def build_elastic_query
      query = {
        query: {
          filtered: {
            query: {
              bool: {
                must: { match_all: {} }
              }
            },
            filter: {
              bool: {
                must: []
              }
            }
          }
        },
        aggs: {
          cities: {
            terms: {
              field: 'city.raw',
              size: 0
            }
          },
          genders: {
            terms: {
              field: 'sex',
              size: 0
            }
          },
          institutions: {
            nested: {
              path: 'degrees'
            },
            aggs: {
              institutions: {
                terms: {
                  field: 'degrees.institution.raw',
                  size: 0
                }
              }
            }
          }
        }
      }

      if params[:query].present?
        query[:query][:filtered][:query][:bool][:must] = {
          match: {
            _all: {
              query: ( params[:query].present? ? params[:query] : "*" ),
              type: :phrase_prefix,
              slop: 2
            }
          }
        }
      end
      # Filtro de regiones
      if params[:cities].present?
        query[:query][:filtered][:filter][:bool][:must] << {
          terms: { # Hace un or entre todos los iterms de params[:cities]
            'city.raw' => params[:cities],
            execution: :bool
          }
        }
      end
      # Filtro de generos
      if params[:genders].present?
        query[:query][:filtered][:filter][:bool][:must] << {
          terms: { # Hace un or entre todos los iterms de params[:genders]
            'sex' => params[:genders],
            execution: :bool
          }
        }
      end
      # Filtro de instituciones
      if params[:institutions].present?
        query[:query][:filtered][:filter][:bool][:must] << {
          nested: {
            path: :degrees,
            filter: {
              terms: {
                "degrees.institution.raw" => params[:institutions],
                execution: :bool
              }
            }
          }
        }
      end
      query
    end

    def renders_params
      @courses = Course.opened.filter_by_role(:enroll, current_user) - @applicant.courses
    end

    def catch_course
      @course = Course.find_by(id: params[:course_id])
      if !@course
        flash.now[:error] = I18n.t("applicant.base.enroll.errors.course.not_found")
        renders_params
        respond_to do |format|
          format.html { render action: 'show' }
        end
      elsif !@course.opened?
        flash[:error] = I18n.t("applicant.base.enroll.errors.course.not_opened")
        redirect_to :back
      end
    end

    def catch_aplicant
      @applicant = Applicant::Base.find_by(id: params[:id])
      render 'errors/404', status: 404, layout: 'application' unless @applicant

    end
    def catch_applicants
      @applicants = Applicant::Base.where(id: params[:ids])
    end


    def authorize_enroll
      unless current_user.can?(:enroll, @course || Course)
        if request.format == 'json'
          render json: { error: 'Error 403'}, status: 403
        else
          render 'errors/403', status: 403, layout: 'application'
        end

      end
    end

    def authorize_send_update_request
      unless current_user.can?(:send_update_request, Applicant::Base)
        if request.format == 'json'
          render json: { error: 'Error 403'}, status: 403
        else
          render 'errors/403', status: 403, layout: 'application'
        end
      end
    end

    def authorize_massive_mailer
      unless current_user.can?(:massive_mailer, Applicant::Base)
        if request.format == 'json'
          render json: { error: 'Error 403'}, status: 403
        else
          render 'errors/403', status: 403, layout: 'application'
        end
      end
    end

    def authorize_update_password
      render 'errors/403', status: 403, layout: 'application' unless current_user.can?(:admin)
    end

    def authorize_read
      render 'errors/403', status: 403, layout: 'application' unless current_user.can?(:search, Applicant::Base)
    end

    def authorize_update
      unless current_user.can?(:update, @applicant) or current_user.can?(:update, Applicant::Base)
        if request.format == 'json'
          render json: { error: 'Error 403' } , status: 403
        else
          render 'errors/403', status: 403, layout: 'application'
        end
      end
    end

    def authorize_action
      render 'errors/404', status: 404, layout: 'application' unless current_user.can?(params[:action], @applicant) or current_user.can?(params[:action], Applicant::Base)
    end

    def authorize_generate_cv
      # Si el ejecutivo tiene permiso para admin y para buscar
      render 'errors/403', status: 403, layout: 'application' unless current_user.can?(:admin, Applicant::Base) || current_user.can?(:search, Applicant::Base)
    end

    def set_course_context
      @course = Course.find_by(id: params[:course_id])
    end
end
