class Applicant::ProfileTypesController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize_action
  before_action :set_applicant_profile_type, only: [:show, :edit, :update, :destroy]

  # GET /applicant/profile_types
  # GET /applicant/profile_types.json
  def index
    @applicant_profile_types = Applicant::ProfileType.all.page(params[:page])
  end

  # GET /applicant/profile_types/1
  # GET /applicant/profile_types/1.json
  def show
  end

  # GET /applicant/profile_types/new
  def new
    @applicant_profile_type = Applicant::ProfileType.new
  end

  # GET /applicant/profile_types/1/edit
  def edit
  end

  # POST /applicant/profile_types
  # POST /applicant/profile_types.json
  def create
    @applicant_profile_type = Applicant::ProfileType.new(applicant_profile_type_params)

    respond_to do |format|
      if @applicant_profile_type.save
        format.html { redirect_to @applicant_profile_type, notice: 'Tipo de Perfil de candidato creado exitosamente' }
        format.json { render :show, status: :created, location: @applicant_profile_type }
      else
        format.html { render :new }
        format.json { render json: @applicant_profile_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /applicant/profile_types/1
  # PATCH/PUT /applicant/profile_types/1.json
  def update
    respond_to do |format|
      if @applicant_profile_type.update(applicant_profile_type_params)
        format.html { redirect_to @applicant_profile_type, notice: 'Tipo de Perfil de candidato actualizado exitosamente' }
        format.json { render :show, status: :ok, location: @applicant_profile_type }
      else
        format.html { render :edit }
        format.json { render json: @applicant_profile_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /applicant/profile_types/1
  # DELETE /applicant/profile_types/1.json
  def destroy
    @applicant_profile_type.destroy
    respond_to do |format|
      format.html { redirect_to applicant_profile_types_url, notice: 'Tipo de Perfil de candidato eliminado exitosamente' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_applicant_profile_type
      @applicant_profile_type = Applicant::ProfileType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def applicant_profile_type_params
      params.require(:applicant_profile_type).permit(:name)
    end

    def authorize_action
      render 'errors/403', status: 403, layout: 'application' unless current_user.can?(:admin)
    end
end
