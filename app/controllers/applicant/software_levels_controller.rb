class Applicant::SoftwareLevelsController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize_action
  before_action :set_applicant_software_level, only: [:show, :edit, :update, :destroy]

  # GET /applicant/software_levels
  # GET /applicant/software_levels.json
  def index
    @applicant_software_levels = Applicant::SoftwareLevel.all.page(params[:page])
  end

  # GET /applicant/software_levels/1
  # GET /applicant/software_levels/1.json
  def show
  end

  # GET /applicant/software_levels/new
  def new
    @applicant_software_level = Applicant::SoftwareLevel.new
  end

  # GET /applicant/software_levels/1/edit
  def edit
  end

  # POST /applicant/software_levels
  # POST /applicant/software_levels.json
  def create
    @applicant_software_level = Applicant::SoftwareLevel.new(applicant_software_level_params)

    respond_to do |format|
      if @applicant_software_level.save
        format.html { redirect_to @applicant_software_level, notice: 'Nivel de conocimiento en computación creado exitosamente' }
        format.json { render :show, status: :created, location: @applicant_software_level }
      else
        format.html { render :new }
        format.json { render json: @applicant_software_level.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /applicant/software_levels/1
  # PATCH/PUT /applicant/software_levels/1.json
  def update
    respond_to do |format|
      if @applicant_software_level.update(applicant_software_level_params)
        format.html { redirect_to @applicant_software_level, notice: 'Nivel de conocimiento en computación actualizado exitosamente' }
        format.json { render :show, status: :ok, location: @applicant_software_level }
      else
        format.html { render :edit }
        format.json { render json: @applicant_software_level.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /applicant/software_levels/1
  # DELETE /applicant/software_levels/1.json
  def destroy
    @applicant_software_level.destroy
    respond_to do |format|
      format.html { redirect_to applicant_software_levels_url, notice: 'Nivel de conocimiento en computación eliminado exitosamente' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_applicant_software_level
      @applicant_software_level = Applicant::SoftwareLevel.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def applicant_software_level_params
      params.require(:applicant_software_level).permit(:name)
    end

    def authorize_action
      render 'errors/403', status: 403, layout: 'application' unless current_user.can?(:admin)
    end
end
