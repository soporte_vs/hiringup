class SkillInterview::StagesController < Stage::BasesController
  def show
    @skill_interview_report = SkillInterview::Report.new(skill_interview_stage: @stage)
  end

  private
  def catch_stages
    where_condition = { enrollment_bases: {course_id: @course.id} }
      if params[:step].present?
        where_condition[:step] = params[:step]
      end
      @stages = SkillInterview::Stage.joins(:enrollment).where(where_condition).joins(enrollment: :course).page(params[:page])
    end

    def authorize_action
      unless has_permission?
        render 'errors/403', status: 403, layout: 'application'
      end
    end

    def has_permission?(stage=nil)
      current_user.can?(:admin, @stage || stage) ||
      current_user.can?(SkillInterview::Stage.to_s.tableize.to_sym, @course) ||
      current_user.can?(:admin, SkillInterview::Stage)
    end
end
