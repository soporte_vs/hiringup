class SkillInterview::ReportsController < SkillInterview::StagesController
  def download
    stage_document = @stage.skill_interview_report.documents.find(params[:stage_document_id])
    send_file stage_document.document.path, filename: stage_document.document.filename
  end

  def create
    @skill_interview_report = SkillInterview::Report.new(report_params)
    @skill_interview_report.documents.each{ |doc| doc.resource = @skill_interview_report }

    if @skill_interview_report.save
      flash[:success] = I18n.t('skill_interview.report.created.success')
      redirect_to course_skill_interview_stage_path(id: @course, stage_id: @stage.id)
    else
      render template: 'skill_interview/stages/show'
    end
  end

  private

    def report_params
      attrs = params.require(:skill_interview_report).permit(:observations)
      attrs[:documents] = []
      document_attrs = params[:skill_interview_report][:documents] || []
      document_attrs.each do |document_val|
        attrs[:documents] << Stage::Document.new(document: document_val)
      end
      attrs.merge(skill_interview_stage_id: @stage.id)

      # params.require(:skill_interview_report).permit(:observations, :document).merge(skill_interview_stage_id: @stage.id)
    end
end
