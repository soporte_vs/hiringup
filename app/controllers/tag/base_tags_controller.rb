class Tag::BaseTagsController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize_action
  before_action :set_tag_base_tag, only: [:show, :edit, :update, :destroy]
  before_action :catch_class

  # GET /tag/base_tags
  # GET /tag/base_tags.json
  def index
    respond_to do |format|
      format.html { @tag_base_tags = Tag::BaseTag.where(type: params[:type]).page(params[:page]) }
      format.json { @tag_base_tags = Tag::BaseTag.where(type: params[:type]) }
    end
  end

  # GET /tag/base_tags/1
  # GET /tag/base_tags/1.json
  def show
  end

  # GET /tag/base_tags/new
  def new
    @tag_base_tag = @tag_class.new
  end

  # GET /tag/base_tags/1/edit
  def edit
  end

  # POST /tag/base_tags
  # POST /tag/base_tags.json
  def create
    @tag_base_tag = @tag_class.new(tag_base_tag_params)

    respond_to do |format|
      if @tag_base_tag.save
        flash[:success] = t("resources.tags.create_success")
        format.html { redirect_to @tag_base_tag }
        format.json { render :show, status: :created, location: @tag_base_tag }
      else
        flash.now[:error] = @tag_base_tag.errors.messages
        format.html { render :new }
        format.json { render json: @tag_base_tag.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tag/base_tags/1
  # PATCH/PUT /tag/base_tags/1.json
  def update
    respond_to do |format|
      if @tag_base_tag.update(tag_base_tag_params)
        flash[:success] = t("resources.tags.update_success")
        format.html { redirect_to @tag_base_tag }
        format.json { render :show, status: :ok, location: @tag_base_tag }
      else
        flash.now[:error] = @tag_base_tag.errors.messages
        format.html { render :edit }
        format.json { render json: @tag_base_tag.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tag/base_tags/1
  # DELETE /tag/base_tags/1.json
  def destroy
    @tag_base_tag.destroy

    respond_to do |format|
      flash[:success] = t("resources.tags.destroy_success")
      format.html { redirect_to controller: params[:controller], type: params[:type] }
      format.json { head :no_content }
    end
  end

  private
    def catch_class
      @tag_class = params[:type].try(:constantize) || Tag::BaseTag
    end
    # Use callbacks to share common setup or constraints between actions.
    def set_tag_base_tag
      @tag_base_tag = Tag::BaseTag.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tag_base_tag_params
      params.require(params[:type].tableize.singularize.gsub("/","_").to_sym).permit(:name)
    end

    def authorize_action
      render 'errors/403', :status => 403, layout: 'application' unless current_user.can?(:admin)
    end
end
