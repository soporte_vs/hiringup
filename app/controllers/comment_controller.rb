class CommentController < ApplicationController
  before_action :authenticate_user!, only: [:create]

  def index
    object_id = params[:object_id]
    object_type = params[:object_type]
    @comments = Comment.where(object_type: object_type, object_id: object_id).order('created_at DESC').page(params[:page]).per(6)
  end

  def create
    @comment = current_user.comments.new(comment_params)
    if @comment.save
      @comment
    else
      respond_to do |format|
        format.json { render json: { errors: @comment.errors.messages }, status: :unprocessable_entity }
      end
    end
  end


  private

  def comment_params
    params.permit(:content, :object_type, :object_id)
  end
end
