class FaqsController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize_action
  before_action :set_faq, only: [:show, :edit, :update, :destroy]

  # GET /faqs
  # GET /faqs.json
  def index
    @faqs = Faq.all
  end

  # GET /faqs/1
  # GET /faqs/1.json
  def show
  end

  # GET /faqs/new
  def new
    @faq = Faq.new
  end

  # GET /faqs/1/edit
  def edit
  end

  # POST /faqs
  # POST /faqs.json
  def create
    @faq = Faq.new(faq_params)

    respond_to do |format|
      if @faq.save
        flash[:success] = I18n.t("faqs.create_success")
        format.html { redirect_to @faq }
        format.json { render :show, status: :created, location: @faq }
      else
        flash.now[:error] = @faq.errors.messages
        format.html { render :new }
        format.json { render json: @faq.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /faqs/1
  # PATCH/PUT /faqs/1.json
  def update
    respond_to do |format|
      if @faq.update(faq_params)
        flash[:success] = I18n.t("faqs.update_success")
        format.html { redirect_to @faq }
        format.json { render :show, status: :ok, location: @faq }
      else
        flash.now[:error] = @faq.errors.messages
        format.html { render :edit }
        format.json { render json: @faq.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /faqs/1
  # DELETE /faqs/1.json
  def destroy
    @faq.destroy
    respond_to do |format|
      flash[:success] = I18n.t("faqs.destroy_success")
      format.html { redirect_to faqs_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_faq
      @faq = Faq.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def faq_params
      params.require(:faq).permit(:question, :answer, :bootsy_image_gallery_id)
    end

    def authorize_action
      render 'errors/403', status: 403, layout: 'application' unless current_user.can?(:admin)
    end
end
