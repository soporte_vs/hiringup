class FinalInterview::ReportsController < FinalInterview::StagesController
  def download
    stage_document = @stage.final_interview_report.documents.find(params[:stage_document_id])
    send_file stage_document.document.path, filename: stage_document.document.filename
  end

  def create
    @final_interview_report = FinalInterview::Report.new(report_params)
    @final_interview_report.documents.each{ |doc| doc.resource = @final_interview_report }

    respond_to do |format|
      if @final_interview_report.save
        if @final_interview_report.notified_to.present?
          FinalInterview::ReportMailer.delay.send_report(@final_interview_report)
        end
        flash[:success] = I18n.t('final_interview.report.created.success')
        format.html { redirect_to course_final_interview_stage_path(id: @course, stage_id: @stage.id) }
      else
        flash[:error] = @final_interview_report.errors.messages
        format.html { render 'final_interview/stages/show' }
      end
    end
  end

  private

    def report_params
      attrs = params.require(:final_interview_report).permit(:observations, :notified_to)
      attrs[:documents] = []
      document_attrs = params[:final_interview_report][:documents] || []
      document_attrs.each do |document_val|
        attrs[:documents] << Stage::Document.new(document: document_val)
      end
      attrs.merge(final_interview_stage_id: @stage.id)
    end
end
