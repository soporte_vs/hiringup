class Worker::BasesController < ApplicationController
  before_action :authenticate_user!
  before_action :catch_worker, if: "params[:id].present?"
  before_action :authorize_action
  before_action :query_params, only: [:search]

  def add_role
    UsersRole.where(user_id: @worker.user_id).joins(:role).where(roles: {resource_id: nil}).destroy_all
    begin
      params[:roles].each do |role|
        role_hash = eval(role)
        @worker.user.add_role role_hash[:name], role_hash[:resource_type].try(:constantize)
      end
      flash[:success] = I18n.t("worker/base.roles.messages.update_success")
    rescue Exception => e
      flash[:errors] = I18n.t("worker/base.roles.messages.role_corrupt")
    end
    redirect_to worker_basis_path(@worker)
  end

  def edit
    flash.now[:notice] = t("helper_text")
    if !@worker.user.personal_information
      # Se saltan las validaciones debido a que es necesario crear una instancia de personal_information
      # asociada al User del applicant para que este pueda editar dichos datos. De lo contrario no se muestra
      # el formulario al acceder al perfil de dicho usuario. (Debido a que USER no tiene un atributo
      # que se llame personal_information, es necesario persistirlo referenciando al usuario al cual pertenece).
      personal_information = People::PersonalInformation.new(user: @worker.user)
      personal_information.save(validate: false)
    end
    if !@worker.user.professional_information
      People::ProfessionalInformation.create(user: @worker.user)
    end
    @worker.reload
    @worker.user.professional_information.degrees.new
  end

  def update
    respond_to do |format|
      if @worker.update(worker_base_params)
        flash[:success] = t("worker/base.update_success")
        format.html { redirect_to worker_basis_path(@worker) }
        format.json { render :show, status: :ok, location: @worker }
      else
        flash.now[:error] = @worker.errors.messages
        format.html { render :edit }
        format.json { render json: @worker.errors, status: :unprocessable_entity }
      end
    end
  end

  def show
    @activities = @worker.user.activities.order('created_at DESC').page(params[:page])
  end

  def search
    if params[:query].present?
      @workers = Worker::Base.joins(user: :personal_information)
      .where("LOWER(people_personal_informations.first_name) LIKE LOWER('%#{params[:query]}%') OR
              LOWER(people_personal_informations.last_name) LIKE LOWER('%#{params[:query]}%') OR
              LOWER(users.email) LIKE LOWER('%#{params[:query]}%')
            ")
      .page(params[:page])
    else
      @workers = Worker::Base.page(params[:page])
    end
  end

  def new
    @worker = Worker::Base.new(user: User.new(personal_information: People::PersonalInformation.new))
  end

  def create
    @worker = Worker::Base.new(worker_base_params)
    respond_to do |format|
      if @worker.save
        flash[:success] = t("worker/base.create_success")
        format.html { redirect_to @worker }
      else
        flash.now[:error] = @worker.errors.messages
        format.html { render action: 'new' }
        format.json { render json: @course.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    def catch_worker
      @worker = Worker::Base.find_by(id: params[:id])
      render 'errors/404', status: 404, layout: 'application' unless @worker
    end

    def worker_base_params
      params.require(:worker).permit(
        user_attributes: [
          :id,
          :email,
          professional_information_attributes: [
            :id,
            degrees_attributes: [
              :id,
              :type,
              :name,
              :career_id,
              :institution_id,
              :culmination_date,
              :condition,
              :_destroy
            ],
            laboral_experiences_attributes: [
              :id,
              :position,
              :company,
              :description,
              :since_date,
              :until_date,
              :_destroy
            ],
            language_skills_attributes: [
              :id,
              :education_language_id,
              :level,
              :_destroy
            ],
            software_skills_attributes: [
              :id,
              :level,
              :education_software_id,
              :_destroy
            ]
          ],
          personal_information_attributes: [
            :id,
            :rut,
            :first_name,
            :last_name,
            :birth_date,
            :landline,
            :cellphone,
            :study_type,
            :nationality_id,
            :sex,
            :territory_city_id,
            :avatar,
            :company_marital_status_id,
            :recruitment_source_id,
            :areas_of_interest,
            :identification_document_number,
            :identification_document_type_id,
            :availability_replacements,
            :availability_work_other_residence,
            personal_referrals_attributes: [
              :id,
              :full_name,
              :email,
              :phone,
              :_destroy
            ]
          ]
        ]
      )
    end

    def query_params
      params.permit(:query)
    end

    def authorize_action
      action = case params[:action]
      when "edit"
        "update"
      else
        params[:action]
      end
      render 'errors/403', status: 403, layout: 'application' unless current_user.can?(action.to_sym, @worker || Worker::Base)
    end
end
