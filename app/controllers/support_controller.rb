class SupportController < ApplicationController
  before_action :authenticate_user!, except: [:faqs]


  def faqs
    @faqs = Faq.all
  end
end
