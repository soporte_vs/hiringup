class AssigningWeighting::StagesController < Stage::BasesController
  def show
   @assigning_weighting_report = AssigningWeighting::Report.new(assigning_weighting_stage: @stage)
 end

  private
    def catch_stages
      where_condition = { enrollment_bases: {course_id: @course.id} }
        if params[:step].present?
         where_condition[:step] = params[:step]
        end
      @stages = AssigningWeighting::Stage.joins(:enrollment).where(where_condition).joins(enrollment: :course).page(params[:page])
    end

    def authorize_action
      unless has_permission?
        render 'errors/403', status: 403, layout: 'application'
      end
    end

    def has_permission?(stage=nil)
      current_user.can?(:admin, @stage || stage) ||
      current_user.can?(AssigningWeighting::Stage.to_s.tableize.to_sym, @course) ||
      current_user.can?(:admin, AssigningWeighting::Stage)
    end
end
