class AssigningWeighting::ReportsController < AssigningWeighting::StagesController
  def download
    stage_document = @stage.assigning_weighting_report.documents.find(params[:stage_document_id])
    send_file stage_document.document.path, filename: stage_document.document.filename
  end

  def create
    @assigning_weighting_report = AssigningWeighting::Report.new(report_params)
    @assigning_weighting_report.documents.each{ |doc| doc.resource = @assigning_weighting_report }

    if @assigning_weighting_report.save
      flash[:success] = I18n.t('assigning_weighting.report.created.success')
      redirect_to course_assigning_weighting_stage_path(id: @course, stage_id: @stage.id)
    else
      render template: 'assigning_weighting/stages/show'
    end
  end

  private

    def report_params
      attrs = params.require(:assigning_weighting_report).permit(:observations)
      attrs[:documents] = []
      document_attrs = params[:assigning_weighting_report][:documents] || []
      document_attrs.each do |document_val|
        attrs[:documents] << Stage::Document.new(document: document_val)
      end
      attrs.merge(assigning_weighting_stage_id: @stage.id)
    end
end
