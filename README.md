# HiringUp Enterprise `Cosmic Coipo 0.3` INTEGRA

## Requirements
- Ruby: `v2.2`
- Rails: `v4.1.4`
- Java: `JDK + JRE 6`
- ElasticSearch: `v1.6.0`
- Node: `v0.10.32`
- PG: `v9.3+`
- DynkoPato

###Pasos para levantar el ambiente de desarrollo
 - Correr `bundle install`
 - Despues de correr bundle install exitosamente ejecutar `rake development:build` y listo!

###Pasos manuales para levantar el entorno
Correr `bundle install`, `npm install` y `rake db:setup` para instalar dependencias, excepto Elasticsearch que se instala manualmente.

###En caso de incendio con postgres (pg) rompa el cristal
Cambiar `{version}`por tú versión de postgres

`gem install pg -v '0.17.1' -- --with-pg-config=/Applications/Postgres.app/Contents/Versions/{version}/bin/pg_config`.

                  ____________________________________________________________
     |||||||     /                                                           /
    -  O JO -   /   Java Problems:                                          /
     |  __ |   /   Asegurarse de tener instalado el JDK + JRE de java 6    /
     | |__||  /___________________________________________________________/
     |_____|

###En caso de problemas con openSSL
Se debe actualizar la version de openSSL

###Whenever
whenever permite correr las tareas RAKE con el cron de unix

para crear el schema de cron:
$ whenever -i # dentro de la carpeta del projecto

para revisar que se creo correctamente la tarea en cron
$ crontab -l

## ElasticSearch
### Indexación
##### Aplicantes:
`Applicant::Base.inrefresh`
##### Procesos
`Course.inrefresh`

#####Para eliminar indices:

`curl -XDELETE 'localhost:9200/*'`


## Database
### Datos falsos
Para tener datos placeholder con los cuales trabajar utiliza:

`FactoryGirl.create_list(:modelo, n)`

n puede ser cualquier número

### Tarea para revisar consistencia de datos
Esta tarea revisa que todos los registros de todas las tablas definidas por modelos
de ActiveRecord sean registros validos

`rake models:validate`

### Tarea para revisar consistencia de Permisos
Esta tarea revisa que todos los registros de la tabla de permisos estan
definidos dentro de la aplicación

`rake roles:validate`

#####Modelos
- course
- applicant_basis
- worker_basis

####### [Issue: Generar tarea para hacer esto automatico](#39)

### Para agregar permisos (sobre clase `User`)
`User.find(n).add_role: admin`

##Testing
### Local
Si un test necesita de elasticsearch (tiene el tag `elasticsearch: true`), debes levantar elasticsearch, gil.
ejecutar `rspec` o `rake parallel:spec`

### Terminator
Al hacer push de tu rama a `origin` o hacer un pull request, Jenkins ejecuta los tests. Para revisar, ir a [ci.4talent.cl:8080](ci.4talent.cl:8080)

## Alertas y Notificaciones
Las alertas, notificaciones, errores, etc, deben ser mostradas en las vistas a travez de flash.now[:type] para que se puedan formatear.

  - Formatos actuales:
    * :notice   => notificación "neutral" al usuario, es de color azul, se puede usar para mostrar sugerencias, actividades, etc
    * :alert    => alerta menor, es de color [por definir]
    * :error    => error, es de color rojo, se debe usar para los fallos de los procesos y envio de forms
    * :success  => proceso o actividad realizada con éxito, es de color verde.

### ChromeDriver
http://chromedriver.storage.googleapis.com/index.html
2.16 => Problemas con Features
2.17 + Chrome 44.0.2403.155 (64-bit Linux) => Features OK

## Doorkeeper
### Configuración

En la aplicación de HUPE está instalado doorkeeper que permite la autentificación de la otras app (Ex: Tickets)

- Pasos para configurar la app en HUPE
  * Entrar a la consola de rails "rails c production" de la instancia del cliente
  * Dentro, crear una nueva app en el modulo de doorkeeper con "Doorkeeper::Application.create(name: 'tickets', redirect_uri: 'http://tickets.4talent.cl/auth/callback')"
  * Esto devolvera el client_id y secret para ser ingresados en la app Tickets

Estos pasos son específicos para la herramienta de seguimiento de Tickets de 4talent pero puede ser aplicado a cualquier otra aplicación que quiera autentificar usuarios a través de Oauth con HUPE
